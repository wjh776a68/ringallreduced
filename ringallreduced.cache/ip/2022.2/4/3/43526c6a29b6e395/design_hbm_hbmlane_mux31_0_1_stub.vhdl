-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
-- Date        : Tue Dec  6 16:27:56 2022
-- Host        : dell-Vostro-3910-China-HDD-Protection running 64-bit Ubuntu 22.04.1 LTS
-- Command     : write_vhdl -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_hbm_hbmlane_mux31_0_1_stub.vhdl
-- Design      : design_hbm_hbmlane_mux31_0_1
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xcvu37p-fsvh2892-2L-e
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  Port ( 
    clk : in STD_LOGIC;
    mux_select : in STD_LOGIC_VECTOR ( 1 downto 0 );
    m_HBM_READ_tvalid : out STD_LOGIC;
    m_HBM_READ_tdata : out STD_LOGIC_VECTOR ( 255 downto 0 );
    m_HBM_READ_tready : in STD_LOGIC;
    m_HBM_READ_tlast : out STD_LOGIC;
    m_HBM_READ_tkeep : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_HBM_WRITE_tready : out STD_LOGIC;
    m_HBM_WRITE_tdata : in STD_LOGIC_VECTOR ( 255 downto 0 );
    m_HBM_WRITE_tvalid : in STD_LOGIC;
    m_rd_blockamount : in STD_LOGIC_VECTOR ( 31 downto 0 );
    m_rd_run : in STD_LOGIC;
    m_rd_startaddress : in STD_LOGIC_VECTOR ( 31 downto 0 );
    m_wr_blockamount : in STD_LOGIC_VECTOR ( 31 downto 0 );
    m_wr_run : in STD_LOGIC;
    m_wr_startaddress : in STD_LOGIC_VECTOR ( 31 downto 0 );
    m_busy : out STD_LOGIC;
    s1_HBM_READ_tvalid : in STD_LOGIC;
    s1_HBM_READ_tdata : in STD_LOGIC_VECTOR ( 255 downto 0 );
    s1_HBM_READ_tready : out STD_LOGIC;
    s1_HBM_READ_tlast : in STD_LOGIC;
    s1_HBM_READ_tkeep : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s1_HBM_WRITE_tready : in STD_LOGIC;
    s1_HBM_WRITE_tdata : out STD_LOGIC_VECTOR ( 255 downto 0 );
    s1_HBM_WRITE_tvalid : out STD_LOGIC;
    s1_rd_blockamount : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s1_rd_run : out STD_LOGIC;
    s1_rd_startaddress : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s1_wr_blockamount : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s1_wr_run : out STD_LOGIC;
    s1_wr_startaddress : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s1_busy : in STD_LOGIC;
    s2_HBM_READ_tvalid : in STD_LOGIC;
    s2_HBM_READ_tdata : in STD_LOGIC_VECTOR ( 255 downto 0 );
    s2_HBM_READ_tready : out STD_LOGIC;
    s2_HBM_READ_tlast : in STD_LOGIC;
    s2_HBM_READ_tkeep : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s2_HBM_WRITE_tready : in STD_LOGIC;
    s2_HBM_WRITE_tdata : out STD_LOGIC_VECTOR ( 255 downto 0 );
    s2_HBM_WRITE_tvalid : out STD_LOGIC;
    s2_rd_blockamount : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s2_rd_run : out STD_LOGIC;
    s2_rd_startaddress : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s2_wr_blockamount : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s2_wr_run : out STD_LOGIC;
    s2_wr_startaddress : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s2_busy : in STD_LOGIC;
    s3_HBM_READ_tvalid : in STD_LOGIC;
    s3_HBM_READ_tdata : in STD_LOGIC_VECTOR ( 255 downto 0 );
    s3_HBM_READ_tready : out STD_LOGIC;
    s3_HBM_READ_tlast : in STD_LOGIC;
    s3_HBM_READ_tkeep : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s3_HBM_WRITE_tready : in STD_LOGIC;
    s3_HBM_WRITE_tdata : out STD_LOGIC_VECTOR ( 255 downto 0 );
    s3_HBM_WRITE_tvalid : out STD_LOGIC;
    s3_rd_blockamount : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s3_rd_run : out STD_LOGIC;
    s3_rd_startaddress : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s3_wr_blockamount : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s3_wr_run : out STD_LOGIC;
    s3_wr_startaddress : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s3_busy : in STD_LOGIC
  );

end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture stub of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk,mux_select[1:0],m_HBM_READ_tvalid,m_HBM_READ_tdata[255:0],m_HBM_READ_tready,m_HBM_READ_tlast,m_HBM_READ_tkeep[31:0],m_HBM_WRITE_tready,m_HBM_WRITE_tdata[255:0],m_HBM_WRITE_tvalid,m_rd_blockamount[31:0],m_rd_run,m_rd_startaddress[31:0],m_wr_blockamount[31:0],m_wr_run,m_wr_startaddress[31:0],m_busy,s1_HBM_READ_tvalid,s1_HBM_READ_tdata[255:0],s1_HBM_READ_tready,s1_HBM_READ_tlast,s1_HBM_READ_tkeep[31:0],s1_HBM_WRITE_tready,s1_HBM_WRITE_tdata[255:0],s1_HBM_WRITE_tvalid,s1_rd_blockamount[31:0],s1_rd_run,s1_rd_startaddress[31:0],s1_wr_blockamount[31:0],s1_wr_run,s1_wr_startaddress[31:0],s1_busy,s2_HBM_READ_tvalid,s2_HBM_READ_tdata[255:0],s2_HBM_READ_tready,s2_HBM_READ_tlast,s2_HBM_READ_tkeep[31:0],s2_HBM_WRITE_tready,s2_HBM_WRITE_tdata[255:0],s2_HBM_WRITE_tvalid,s2_rd_blockamount[31:0],s2_rd_run,s2_rd_startaddress[31:0],s2_wr_blockamount[31:0],s2_wr_run,s2_wr_startaddress[31:0],s2_busy,s3_HBM_READ_tvalid,s3_HBM_READ_tdata[255:0],s3_HBM_READ_tready,s3_HBM_READ_tlast,s3_HBM_READ_tkeep[31:0],s3_HBM_WRITE_tready,s3_HBM_WRITE_tdata[255:0],s3_HBM_WRITE_tvalid,s3_rd_blockamount[31:0],s3_rd_run,s3_rd_startaddress[31:0],s3_wr_blockamount[31:0],s3_wr_run,s3_wr_startaddress[31:0],s3_busy";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "hbmlane_mux31,Vivado 2022.2";
begin
end;
