// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
// Date        : Tue Dec  6 16:27:56 2022
// Host        : dell-Vostro-3910-China-HDD-Protection running 64-bit Ubuntu 22.04.1 LTS
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_hbm_hbmlane_mux31_0_1_stub.v
// Design      : design_hbm_hbmlane_mux31_0_1
// Purpose     : Stub declaration of top-level module interface
// Device      : xcvu37p-fsvh2892-2L-e
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "hbmlane_mux31,Vivado 2022.2" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(clk, mux_select, m_HBM_READ_tvalid, 
  m_HBM_READ_tdata, m_HBM_READ_tready, m_HBM_READ_tlast, m_HBM_READ_tkeep, 
  m_HBM_WRITE_tready, m_HBM_WRITE_tdata, m_HBM_WRITE_tvalid, m_rd_blockamount, m_rd_run, 
  m_rd_startaddress, m_wr_blockamount, m_wr_run, m_wr_startaddress, m_busy, 
  s1_HBM_READ_tvalid, s1_HBM_READ_tdata, s1_HBM_READ_tready, s1_HBM_READ_tlast, 
  s1_HBM_READ_tkeep, s1_HBM_WRITE_tready, s1_HBM_WRITE_tdata, s1_HBM_WRITE_tvalid, 
  s1_rd_blockamount, s1_rd_run, s1_rd_startaddress, s1_wr_blockamount, s1_wr_run, 
  s1_wr_startaddress, s1_busy, s2_HBM_READ_tvalid, s2_HBM_READ_tdata, s2_HBM_READ_tready, 
  s2_HBM_READ_tlast, s2_HBM_READ_tkeep, s2_HBM_WRITE_tready, s2_HBM_WRITE_tdata, 
  s2_HBM_WRITE_tvalid, s2_rd_blockamount, s2_rd_run, s2_rd_startaddress, s2_wr_blockamount, 
  s2_wr_run, s2_wr_startaddress, s2_busy, s3_HBM_READ_tvalid, s3_HBM_READ_tdata, 
  s3_HBM_READ_tready, s3_HBM_READ_tlast, s3_HBM_READ_tkeep, s3_HBM_WRITE_tready, 
  s3_HBM_WRITE_tdata, s3_HBM_WRITE_tvalid, s3_rd_blockamount, s3_rd_run, 
  s3_rd_startaddress, s3_wr_blockamount, s3_wr_run, s3_wr_startaddress, s3_busy)
/* synthesis syn_black_box black_box_pad_pin="clk,mux_select[1:0],m_HBM_READ_tvalid,m_HBM_READ_tdata[255:0],m_HBM_READ_tready,m_HBM_READ_tlast,m_HBM_READ_tkeep[31:0],m_HBM_WRITE_tready,m_HBM_WRITE_tdata[255:0],m_HBM_WRITE_tvalid,m_rd_blockamount[31:0],m_rd_run,m_rd_startaddress[31:0],m_wr_blockamount[31:0],m_wr_run,m_wr_startaddress[31:0],m_busy,s1_HBM_READ_tvalid,s1_HBM_READ_tdata[255:0],s1_HBM_READ_tready,s1_HBM_READ_tlast,s1_HBM_READ_tkeep[31:0],s1_HBM_WRITE_tready,s1_HBM_WRITE_tdata[255:0],s1_HBM_WRITE_tvalid,s1_rd_blockamount[31:0],s1_rd_run,s1_rd_startaddress[31:0],s1_wr_blockamount[31:0],s1_wr_run,s1_wr_startaddress[31:0],s1_busy,s2_HBM_READ_tvalid,s2_HBM_READ_tdata[255:0],s2_HBM_READ_tready,s2_HBM_READ_tlast,s2_HBM_READ_tkeep[31:0],s2_HBM_WRITE_tready,s2_HBM_WRITE_tdata[255:0],s2_HBM_WRITE_tvalid,s2_rd_blockamount[31:0],s2_rd_run,s2_rd_startaddress[31:0],s2_wr_blockamount[31:0],s2_wr_run,s2_wr_startaddress[31:0],s2_busy,s3_HBM_READ_tvalid,s3_HBM_READ_tdata[255:0],s3_HBM_READ_tready,s3_HBM_READ_tlast,s3_HBM_READ_tkeep[31:0],s3_HBM_WRITE_tready,s3_HBM_WRITE_tdata[255:0],s3_HBM_WRITE_tvalid,s3_rd_blockamount[31:0],s3_rd_run,s3_rd_startaddress[31:0],s3_wr_blockamount[31:0],s3_wr_run,s3_wr_startaddress[31:0],s3_busy" */;
  input clk;
  input [1:0]mux_select;
  output m_HBM_READ_tvalid;
  output [255:0]m_HBM_READ_tdata;
  input m_HBM_READ_tready;
  output m_HBM_READ_tlast;
  output [31:0]m_HBM_READ_tkeep;
  output m_HBM_WRITE_tready;
  input [255:0]m_HBM_WRITE_tdata;
  input m_HBM_WRITE_tvalid;
  input [31:0]m_rd_blockamount;
  input m_rd_run;
  input [31:0]m_rd_startaddress;
  input [31:0]m_wr_blockamount;
  input m_wr_run;
  input [31:0]m_wr_startaddress;
  output m_busy;
  input s1_HBM_READ_tvalid;
  input [255:0]s1_HBM_READ_tdata;
  output s1_HBM_READ_tready;
  input s1_HBM_READ_tlast;
  input [31:0]s1_HBM_READ_tkeep;
  input s1_HBM_WRITE_tready;
  output [255:0]s1_HBM_WRITE_tdata;
  output s1_HBM_WRITE_tvalid;
  output [31:0]s1_rd_blockamount;
  output s1_rd_run;
  output [31:0]s1_rd_startaddress;
  output [31:0]s1_wr_blockamount;
  output s1_wr_run;
  output [31:0]s1_wr_startaddress;
  input s1_busy;
  input s2_HBM_READ_tvalid;
  input [255:0]s2_HBM_READ_tdata;
  output s2_HBM_READ_tready;
  input s2_HBM_READ_tlast;
  input [31:0]s2_HBM_READ_tkeep;
  input s2_HBM_WRITE_tready;
  output [255:0]s2_HBM_WRITE_tdata;
  output s2_HBM_WRITE_tvalid;
  output [31:0]s2_rd_blockamount;
  output s2_rd_run;
  output [31:0]s2_rd_startaddress;
  output [31:0]s2_wr_blockamount;
  output s2_wr_run;
  output [31:0]s2_wr_startaddress;
  input s2_busy;
  input s3_HBM_READ_tvalid;
  input [255:0]s3_HBM_READ_tdata;
  output s3_HBM_READ_tready;
  input s3_HBM_READ_tlast;
  input [31:0]s3_HBM_READ_tkeep;
  input s3_HBM_WRITE_tready;
  output [255:0]s3_HBM_WRITE_tdata;
  output s3_HBM_WRITE_tvalid;
  output [31:0]s3_rd_blockamount;
  output s3_rd_run;
  output [31:0]s3_rd_startaddress;
  output [31:0]s3_wr_blockamount;
  output s3_wr_run;
  output [31:0]s3_wr_startaddress;
  input s3_busy;
endmodule
