// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
// Date        : Tue Dec  6 16:15:50 2022
// Host        : dell-Vostro-3910-China-HDD-Protection running 64-bit Ubuntu 22.04.1 LTS
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_hbm_channel_channel_ctrl_v5_0_0_stub.v
// Design      : design_hbm_channel_channel_ctrl_v5_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xcvu37p-fsvh2892-2L-e
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "channel_ctrl_v5,Vivado 2022.2" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(AXI_ARADDR, AXI_ARBURST, AXI_ARID, AXI_ARLEN, 
  AXI_ARREADY, AXI_ARSIZE, AXI_ARVALID, AXI_AWADDR, AXI_AWBURST, AXI_AWID, AXI_AWLEN, 
  AXI_AWREADY, AXI_AWSIZE, AXI_AWVALID, AXI_BID, AXI_BREADY, AXI_BRESP, AXI_BVALID, AXI_RDATA, 
  AXI_RID, AXI_RLAST, AXI_RREADY, AXI_RRESP, AXI_RVALID, AXI_WID, AXI_WDATA, AXI_WLAST, AXI_WREADY, 
  AXI_WSTRB, AXI_WVALID, AXI_WDATA_PARITY, FIFO_WRITE_tready, FIFO_WRITE_tdata, 
  FIFO_WRITE_tvalid, FIFO_WRITE_tkeep, FIFO_WRITE_tlast, FIFO_READ_tvalid, FIFO_READ_tdata, 
  FIFO_READ_tready, rd_running, rd_startaddress, rd_blockamount, wr_running, wr_startaddress, 
  wr_blockamount, busy, clk)
/* synthesis syn_black_box black_box_pad_pin="AXI_ARADDR[32:0],AXI_ARBURST[1:0],AXI_ARID[5:0],AXI_ARLEN[3:0],AXI_ARREADY,AXI_ARSIZE[2:0],AXI_ARVALID,AXI_AWADDR[32:0],AXI_AWBURST[1:0],AXI_AWID[5:0],AXI_AWLEN[3:0],AXI_AWREADY,AXI_AWSIZE[2:0],AXI_AWVALID,AXI_BID[5:0],AXI_BREADY,AXI_BRESP[1:0],AXI_BVALID,AXI_RDATA[255:0],AXI_RID[5:0],AXI_RLAST,AXI_RREADY,AXI_RRESP[1:0],AXI_RVALID,AXI_WID[5:0],AXI_WDATA[255:0],AXI_WLAST,AXI_WREADY,AXI_WSTRB[31:0],AXI_WVALID,AXI_WDATA_PARITY[31:0],FIFO_WRITE_tready,FIFO_WRITE_tdata[255:0],FIFO_WRITE_tvalid,FIFO_WRITE_tkeep[31:0],FIFO_WRITE_tlast,FIFO_READ_tvalid,FIFO_READ_tdata[255:0],FIFO_READ_tready,rd_running,rd_startaddress[31:0],rd_blockamount[31:0],wr_running,wr_startaddress[31:0],wr_blockamount[31:0],busy,clk" */;
  output [32:0]AXI_ARADDR;
  output [1:0]AXI_ARBURST;
  output [5:0]AXI_ARID;
  output [3:0]AXI_ARLEN;
  input AXI_ARREADY;
  output [2:0]AXI_ARSIZE;
  output AXI_ARVALID;
  output [32:0]AXI_AWADDR;
  output [1:0]AXI_AWBURST;
  output [5:0]AXI_AWID;
  output [3:0]AXI_AWLEN;
  input AXI_AWREADY;
  output [2:0]AXI_AWSIZE;
  output AXI_AWVALID;
  input [5:0]AXI_BID;
  output AXI_BREADY;
  input [1:0]AXI_BRESP;
  input AXI_BVALID;
  input [255:0]AXI_RDATA;
  input [5:0]AXI_RID;
  input AXI_RLAST;
  output AXI_RREADY;
  input [1:0]AXI_RRESP;
  input AXI_RVALID;
  output [5:0]AXI_WID;
  output [255:0]AXI_WDATA;
  output AXI_WLAST;
  input AXI_WREADY;
  output [31:0]AXI_WSTRB;
  output AXI_WVALID;
  output [31:0]AXI_WDATA_PARITY;
  input FIFO_WRITE_tready;
  output [255:0]FIFO_WRITE_tdata;
  output FIFO_WRITE_tvalid;
  output [31:0]FIFO_WRITE_tkeep;
  output FIFO_WRITE_tlast;
  input FIFO_READ_tvalid;
  input [255:0]FIFO_READ_tdata;
  output FIFO_READ_tready;
  input rd_running;
  input [31:0]rd_startaddress;
  input [31:0]rd_blockamount;
  input wr_running;
  input [31:0]wr_startaddress;
  input [31:0]wr_blockamount;
  output busy;
  input clk;
endmodule
