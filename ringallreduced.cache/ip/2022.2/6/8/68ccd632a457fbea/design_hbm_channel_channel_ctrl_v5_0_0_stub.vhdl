-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
-- Date        : Tue Dec  6 16:15:50 2022
-- Host        : dell-Vostro-3910-China-HDD-Protection running 64-bit Ubuntu 22.04.1 LTS
-- Command     : write_vhdl -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_hbm_channel_channel_ctrl_v5_0_0_stub.vhdl
-- Design      : design_hbm_channel_channel_ctrl_v5_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xcvu37p-fsvh2892-2L-e
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  Port ( 
    AXI_ARADDR : out STD_LOGIC_VECTOR ( 32 downto 0 );
    AXI_ARBURST : out STD_LOGIC_VECTOR ( 1 downto 0 );
    AXI_ARID : out STD_LOGIC_VECTOR ( 5 downto 0 );
    AXI_ARLEN : out STD_LOGIC_VECTOR ( 3 downto 0 );
    AXI_ARREADY : in STD_LOGIC;
    AXI_ARSIZE : out STD_LOGIC_VECTOR ( 2 downto 0 );
    AXI_ARVALID : out STD_LOGIC;
    AXI_AWADDR : out STD_LOGIC_VECTOR ( 32 downto 0 );
    AXI_AWBURST : out STD_LOGIC_VECTOR ( 1 downto 0 );
    AXI_AWID : out STD_LOGIC_VECTOR ( 5 downto 0 );
    AXI_AWLEN : out STD_LOGIC_VECTOR ( 3 downto 0 );
    AXI_AWREADY : in STD_LOGIC;
    AXI_AWSIZE : out STD_LOGIC_VECTOR ( 2 downto 0 );
    AXI_AWVALID : out STD_LOGIC;
    AXI_BID : in STD_LOGIC_VECTOR ( 5 downto 0 );
    AXI_BREADY : out STD_LOGIC;
    AXI_BRESP : in STD_LOGIC_VECTOR ( 1 downto 0 );
    AXI_BVALID : in STD_LOGIC;
    AXI_RDATA : in STD_LOGIC_VECTOR ( 255 downto 0 );
    AXI_RID : in STD_LOGIC_VECTOR ( 5 downto 0 );
    AXI_RLAST : in STD_LOGIC;
    AXI_RREADY : out STD_LOGIC;
    AXI_RRESP : in STD_LOGIC_VECTOR ( 1 downto 0 );
    AXI_RVALID : in STD_LOGIC;
    AXI_WID : out STD_LOGIC_VECTOR ( 5 downto 0 );
    AXI_WDATA : out STD_LOGIC_VECTOR ( 255 downto 0 );
    AXI_WLAST : out STD_LOGIC;
    AXI_WREADY : in STD_LOGIC;
    AXI_WSTRB : out STD_LOGIC_VECTOR ( 31 downto 0 );
    AXI_WVALID : out STD_LOGIC;
    AXI_WDATA_PARITY : out STD_LOGIC_VECTOR ( 31 downto 0 );
    FIFO_WRITE_tready : in STD_LOGIC;
    FIFO_WRITE_tdata : out STD_LOGIC_VECTOR ( 255 downto 0 );
    FIFO_WRITE_tvalid : out STD_LOGIC;
    FIFO_WRITE_tkeep : out STD_LOGIC_VECTOR ( 31 downto 0 );
    FIFO_WRITE_tlast : out STD_LOGIC;
    FIFO_READ_tvalid : in STD_LOGIC;
    FIFO_READ_tdata : in STD_LOGIC_VECTOR ( 255 downto 0 );
    FIFO_READ_tready : out STD_LOGIC;
    rd_running : in STD_LOGIC;
    rd_startaddress : in STD_LOGIC_VECTOR ( 31 downto 0 );
    rd_blockamount : in STD_LOGIC_VECTOR ( 31 downto 0 );
    wr_running : in STD_LOGIC;
    wr_startaddress : in STD_LOGIC_VECTOR ( 31 downto 0 );
    wr_blockamount : in STD_LOGIC_VECTOR ( 31 downto 0 );
    busy : out STD_LOGIC;
    clk : in STD_LOGIC
  );

end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture stub of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "AXI_ARADDR[32:0],AXI_ARBURST[1:0],AXI_ARID[5:0],AXI_ARLEN[3:0],AXI_ARREADY,AXI_ARSIZE[2:0],AXI_ARVALID,AXI_AWADDR[32:0],AXI_AWBURST[1:0],AXI_AWID[5:0],AXI_AWLEN[3:0],AXI_AWREADY,AXI_AWSIZE[2:0],AXI_AWVALID,AXI_BID[5:0],AXI_BREADY,AXI_BRESP[1:0],AXI_BVALID,AXI_RDATA[255:0],AXI_RID[5:0],AXI_RLAST,AXI_RREADY,AXI_RRESP[1:0],AXI_RVALID,AXI_WID[5:0],AXI_WDATA[255:0],AXI_WLAST,AXI_WREADY,AXI_WSTRB[31:0],AXI_WVALID,AXI_WDATA_PARITY[31:0],FIFO_WRITE_tready,FIFO_WRITE_tdata[255:0],FIFO_WRITE_tvalid,FIFO_WRITE_tkeep[31:0],FIFO_WRITE_tlast,FIFO_READ_tvalid,FIFO_READ_tdata[255:0],FIFO_READ_tready,rd_running,rd_startaddress[31:0],rd_blockamount[31:0],wr_running,wr_startaddress[31:0],wr_blockamount[31:0],busy,clk";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "channel_ctrl_v5,Vivado 2022.2";
begin
end;
