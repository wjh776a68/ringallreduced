// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
// Date        : Tue Dec  6 16:16:48 2022
// Host        : dell-Vostro-3910-China-HDD-Protection running 64-bit Ubuntu 22.04.1 LTS
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_qsfp_40G_recv_adjustpacket_0_0_stub.v
// Design      : design_qsfp_40G_recv_adjustpacket_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xcvu37p-fsvh2892-2L-e
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "recv_adjustpacket,Vivado 2022.2" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(m_axis_tready, m_axis_tvalid, m_axis_tdata, 
  m_axis_tkeep, m_axis_tlast, m_axis_tuser, s_axis_tready, s_axis_tvalid, s_axis_tdata, 
  s_axis_tkeep, s_axis_tlast, s_axis_tuser, clk)
/* synthesis syn_black_box black_box_pad_pin="m_axis_tready,m_axis_tvalid,m_axis_tdata[255:0],m_axis_tkeep[31:0],m_axis_tlast,m_axis_tuser[0:0],s_axis_tready,s_axis_tvalid,s_axis_tdata[255:0],s_axis_tkeep[31:0],s_axis_tlast,s_axis_tuser[0:0],clk" */;
  output m_axis_tready;
  input m_axis_tvalid;
  input [255:0]m_axis_tdata;
  input [31:0]m_axis_tkeep;
  input m_axis_tlast;
  input [0:0]m_axis_tuser;
  input s_axis_tready;
  output s_axis_tvalid;
  output [255:0]s_axis_tdata;
  output [31:0]s_axis_tkeep;
  output s_axis_tlast;
  output [0:0]s_axis_tuser;
  input clk;
endmodule
