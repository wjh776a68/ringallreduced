// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
// Date        : Tue Dec  6 16:16:37 2022
// Host        : dell-Vostro-3910-China-HDD-Protection running 64-bit Ubuntu 22.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_qsfp_40G_forward_0_0_sim_netlist.v
// Design      : design_qsfp_40G_forward_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcvu37p-fsvh2892-2L-e
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_qsfp_40G_forward_0_0,forward,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* IP_DEFINITION_SOURCE = "module_ref" *) 
(* X_CORE_INFO = "forward,Vivado 2022.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (m_axis_tready,
    m_axis_tvalid,
    m_axis_tdata,
    m_axis_tkeep,
    m_axis_tlast,
    m_axis_tuser,
    s_axis_tready,
    s_axis_tvalid,
    s_axis_tdata,
    s_axis_tkeep,
    s_axis_tlast,
    s_axis_tuser,
    clk);
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_axis TREADY" *) output m_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_axis TVALID" *) input m_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_axis TDATA" *) input [255:0]m_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_axis TKEEP" *) input [31:0]m_axis_tkeep;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_axis TLAST" *) input m_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_axis TUSER" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m_axis, FREQ_HZ 312500000, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 1, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, PHASE 0, CLK_DOMAIN design_qsfp_40G_l_ethernet_0_0_tx_clk_out_0, LAYERED_METADATA undef, INSERT_VIP 0" *) input [0:0]m_axis_tuser;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s_axis TREADY" *) (* X_INTERFACE_PARAMETER = "FREQ_HZ 390625000" *) input s_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s_axis TVALID" *) output s_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s_axis TDATA" *) output [255:0]s_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s_axis TKEEP" *) output [31:0]s_axis_tkeep;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s_axis TLAST" *) output s_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s_axis TUSER" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s_axis, FREQ_HZ 312500000, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 1, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, PHASE 0, CLK_DOMAIN design_qsfp_40G_l_ethernet_0_0_tx_clk_out_0, LAYERED_METADATA undef, INSERT_VIP 0" *) output [0:0]s_axis_tuser;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 clk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME clk, ASSOCIATED_BUSIF m_axis:s_axis, FREQ_HZ 312500000, FREQ_TOLERANCE_HZ 0, PHASE 0, CLK_DOMAIN design_qsfp_40G_l_ethernet_0_0_tx_clk_out_0, INSERT_VIP 0" *) input clk;

  wire clk;
  wire [255:0]m_axis_tdata;
  wire [31:0]m_axis_tkeep;
  wire m_axis_tlast;
  wire m_axis_tready;
  wire [0:0]m_axis_tuser;
  wire m_axis_tvalid;
  wire [255:0]s_axis_tdata;
  wire [31:31]\^s_axis_tkeep ;
  wire s_axis_tlast;
  wire s_axis_tready;
  wire s_axis_tvalid;

  assign s_axis_tkeep[31] = \^s_axis_tkeep [31];
  assign s_axis_tkeep[30:0] = m_axis_tkeep[30:0];
  assign s_axis_tuser[0] = m_axis_tuser;
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_forward inst
       (.clk(clk),
        .m_axis_tdata(m_axis_tdata),
        .m_axis_tlast(m_axis_tlast),
        .m_axis_tready(m_axis_tready),
        .m_axis_tvalid(m_axis_tvalid),
        .s_axis_tdata(s_axis_tdata),
        .s_axis_tkeep(\^s_axis_tkeep ),
        .s_axis_tlast(s_axis_tlast),
        .s_axis_tready(s_axis_tready),
        .s_axis_tvalid(s_axis_tvalid));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_forward
   (s_axis_tdata,
    s_axis_tlast,
    s_axis_tvalid,
    m_axis_tready,
    s_axis_tkeep,
    m_axis_tvalid,
    m_axis_tdata,
    clk,
    m_axis_tlast,
    s_axis_tready);
  output [255:0]s_axis_tdata;
  output s_axis_tlast;
  output s_axis_tvalid;
  output m_axis_tready;
  output [0:0]s_axis_tkeep;
  input m_axis_tvalid;
  input [255:0]m_axis_tdata;
  input clk;
  input m_axis_tlast;
  input s_axis_tready;

  wire clk;
  wire focus_tkeep;
  wire focus_tkeep_i_1_n_0;
  wire focus_tkeep_i_2_n_0;
  wire focus_tlast0;
  wire focus_tlast_i_2_n_0;
  wire focus_tlast_i_3_n_0;
  wire focus_tlast_i_4_n_0;
  wire focus_tlast_i_5_n_0;
  wire focus_tlast_i_6_n_0;
  wire focus_tlast_reg_n_0;
  wire [255:0]m_axis_tdata;
  wire m_axis_tlast;
  wire m_axis_tready;
  wire m_axis_tvalid;
  wire [15:1]p_0_in;
  wire p_3_in;
  wire packet_size_counter0_carry__0_n_2;
  wire packet_size_counter0_carry__0_n_3;
  wire packet_size_counter0_carry__0_n_4;
  wire packet_size_counter0_carry__0_n_5;
  wire packet_size_counter0_carry__0_n_6;
  wire packet_size_counter0_carry__0_n_7;
  wire packet_size_counter0_carry_n_0;
  wire packet_size_counter0_carry_n_1;
  wire packet_size_counter0_carry_n_2;
  wire packet_size_counter0_carry_n_3;
  wire packet_size_counter0_carry_n_4;
  wire packet_size_counter0_carry_n_5;
  wire packet_size_counter0_carry_n_6;
  wire packet_size_counter0_carry_n_7;
  wire \packet_size_counter[0]_i_1_n_0 ;
  wire \packet_size_counter[15]_i_3_n_0 ;
  wire \packet_size_counter[15]_i_4_n_0 ;
  wire [15:0]packet_size_counter_reg;
  wire [255:0]s_axis_tdata;
  wire [0:0]s_axis_tkeep;
  wire s_axis_tlast;
  wire s_axis_tlast_INST_0_i_1_n_0;
  wire s_axis_tlast_INST_0_i_2_n_0;
  wire s_axis_tlast_INST_0_i_3_n_0;
  wire s_axis_tready;
  wire s_axis_tvalid;
  wire s_axis_tvalid_packetnouf;
  wire s_axis_tvalid_packetnouf_i_1_n_0;
  wire s_axis_tvalid_packetnouf_reg_n_0;
  wire stat_FSM;
  wire stat_FSM_i_2_n_0;
  wire stat_FSM_next;
  wire [7:6]NLW_packet_size_counter0_carry__0_CO_UNCONNECTED;
  wire [7:7]NLW_packet_size_counter0_carry__0_O_UNCONNECTED;

  LUT6 #(
    .INIT(64'h2F222F2F20222020)) 
    focus_tkeep_i_1
       (.I0(focus_tlast_i_3_n_0),
        .I1(focus_tlast_i_2_n_0),
        .I2(focus_tkeep_i_2_n_0),
        .I3(stat_FSM),
        .I4(stat_FSM_i_2_n_0),
        .I5(focus_tkeep),
        .O(focus_tkeep_i_1_n_0));
  LUT6 #(
    .INIT(64'h5555555055555553)) 
    focus_tkeep_i_2
       (.I0(stat_FSM),
        .I1(focus_tlast_i_5_n_0),
        .I2(s_axis_tlast_INST_0_i_3_n_0),
        .I3(s_axis_tlast_INST_0_i_2_n_0),
        .I4(\packet_size_counter[15]_i_4_n_0 ),
        .I5(focus_tlast_i_4_n_0),
        .O(focus_tkeep_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    focus_tkeep_reg
       (.C(clk),
        .CE(1'b1),
        .D(focus_tkeep_i_1_n_0),
        .Q(focus_tkeep),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h000F000044444444)) 
    focus_tlast_i_1
       (.I0(focus_tlast_i_2_n_0),
        .I1(focus_tlast_i_3_n_0),
        .I2(s_axis_tlast_INST_0_i_1_n_0),
        .I3(focus_tlast_i_4_n_0),
        .I4(focus_tlast_i_5_n_0),
        .I5(stat_FSM),
        .O(focus_tlast0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    focus_tlast_i_2
       (.I0(s_axis_tlast_INST_0_i_3_n_0),
        .I1(packet_size_counter_reg[12]),
        .I2(packet_size_counter_reg[13]),
        .I3(packet_size_counter_reg[9]),
        .I4(packet_size_counter_reg[11]),
        .O(focus_tlast_i_2_n_0));
  LUT6 #(
    .INIT(64'h0040000000000000)) 
    focus_tlast_i_3
       (.I0(focus_tlast_i_6_n_0),
        .I1(packet_size_counter_reg[7]),
        .I2(packet_size_counter_reg[10]),
        .I3(packet_size_counter_reg[0]),
        .I4(packet_size_counter_reg[3]),
        .I5(packet_size_counter_reg[5]),
        .O(focus_tlast_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair129" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    focus_tlast_i_4
       (.I0(packet_size_counter_reg[2]),
        .I1(packet_size_counter_reg[1]),
        .I2(packet_size_counter_reg[0]),
        .O(focus_tlast_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair130" *) 
  LUT3 #(
    .INIT(8'h1F)) 
    focus_tlast_i_5
       (.I0(s_axis_tvalid_packetnouf_reg_n_0),
        .I1(m_axis_tvalid),
        .I2(s_axis_tready),
        .O(focus_tlast_i_5_n_0));
  (* SOFT_HLUTNM = "soft_lutpair129" *) 
  LUT2 #(
    .INIT(4'h7)) 
    focus_tlast_i_6
       (.I0(packet_size_counter_reg[1]),
        .I1(packet_size_counter_reg[2]),
        .O(focus_tlast_i_6_n_0));
  FDRE #(
    .INIT(1'b0)) 
    focus_tlast_reg
       (.C(clk),
        .CE(1'b1),
        .D(focus_tlast0),
        .Q(focus_tlast_reg_n_0),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair131" *) 
  LUT2 #(
    .INIT(4'h2)) 
    m_axis_tready_INST_0
       (.I0(s_axis_tready),
        .I1(stat_FSM),
        .O(m_axis_tready));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY8 packet_size_counter0_carry
       (.CI(packet_size_counter_reg[0]),
        .CI_TOP(1'b0),
        .CO({packet_size_counter0_carry_n_0,packet_size_counter0_carry_n_1,packet_size_counter0_carry_n_2,packet_size_counter0_carry_n_3,packet_size_counter0_carry_n_4,packet_size_counter0_carry_n_5,packet_size_counter0_carry_n_6,packet_size_counter0_carry_n_7}),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O(p_0_in[8:1]),
        .S(packet_size_counter_reg[8:1]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY8 packet_size_counter0_carry__0
       (.CI(packet_size_counter0_carry_n_0),
        .CI_TOP(1'b0),
        .CO({NLW_packet_size_counter0_carry__0_CO_UNCONNECTED[7:6],packet_size_counter0_carry__0_n_2,packet_size_counter0_carry__0_n_3,packet_size_counter0_carry__0_n_4,packet_size_counter0_carry__0_n_5,packet_size_counter0_carry__0_n_6,packet_size_counter0_carry__0_n_7}),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_packet_size_counter0_carry__0_O_UNCONNECTED[7],p_0_in[15:9]}),
        .S({1'b0,packet_size_counter_reg[15:9]}));
  LUT1 #(
    .INIT(2'h1)) 
    \packet_size_counter[0]_i_1 
       (.I0(packet_size_counter_reg[0]),
        .O(\packet_size_counter[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hEA00EA00EA000000)) 
    \packet_size_counter[15]_i_1 
       (.I0(focus_tlast_reg_n_0),
        .I1(\packet_size_counter[15]_i_3_n_0 ),
        .I2(m_axis_tlast),
        .I3(s_axis_tready),
        .I4(m_axis_tvalid),
        .I5(s_axis_tvalid_packetnouf_reg_n_0),
        .O(s_axis_tvalid_packetnouf));
  LUT3 #(
    .INIT(8'hA8)) 
    \packet_size_counter[15]_i_2 
       (.I0(s_axis_tready),
        .I1(m_axis_tvalid),
        .I2(s_axis_tvalid_packetnouf_reg_n_0),
        .O(p_3_in));
  LUT6 #(
    .INIT(64'hFFFEFEFEFEFEFEFE)) 
    \packet_size_counter[15]_i_3 
       (.I0(s_axis_tlast_INST_0_i_3_n_0),
        .I1(s_axis_tlast_INST_0_i_2_n_0),
        .I2(\packet_size_counter[15]_i_4_n_0 ),
        .I3(packet_size_counter_reg[0]),
        .I4(packet_size_counter_reg[1]),
        .I5(packet_size_counter_reg[2]),
        .O(\packet_size_counter[15]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \packet_size_counter[15]_i_4 
       (.I0(packet_size_counter_reg[5]),
        .I1(packet_size_counter_reg[3]),
        .I2(packet_size_counter_reg[10]),
        .I3(packet_size_counter_reg[7]),
        .O(\packet_size_counter[15]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \packet_size_counter_reg[0] 
       (.C(clk),
        .CE(p_3_in),
        .D(\packet_size_counter[0]_i_1_n_0 ),
        .Q(packet_size_counter_reg[0]),
        .R(s_axis_tvalid_packetnouf));
  FDRE #(
    .INIT(1'b0)) 
    \packet_size_counter_reg[10] 
       (.C(clk),
        .CE(p_3_in),
        .D(p_0_in[10]),
        .Q(packet_size_counter_reg[10]),
        .R(s_axis_tvalid_packetnouf));
  FDRE #(
    .INIT(1'b0)) 
    \packet_size_counter_reg[11] 
       (.C(clk),
        .CE(p_3_in),
        .D(p_0_in[11]),
        .Q(packet_size_counter_reg[11]),
        .R(s_axis_tvalid_packetnouf));
  FDRE #(
    .INIT(1'b0)) 
    \packet_size_counter_reg[12] 
       (.C(clk),
        .CE(p_3_in),
        .D(p_0_in[12]),
        .Q(packet_size_counter_reg[12]),
        .R(s_axis_tvalid_packetnouf));
  FDRE #(
    .INIT(1'b0)) 
    \packet_size_counter_reg[13] 
       (.C(clk),
        .CE(p_3_in),
        .D(p_0_in[13]),
        .Q(packet_size_counter_reg[13]),
        .R(s_axis_tvalid_packetnouf));
  FDRE #(
    .INIT(1'b0)) 
    \packet_size_counter_reg[14] 
       (.C(clk),
        .CE(p_3_in),
        .D(p_0_in[14]),
        .Q(packet_size_counter_reg[14]),
        .R(s_axis_tvalid_packetnouf));
  FDRE #(
    .INIT(1'b0)) 
    \packet_size_counter_reg[15] 
       (.C(clk),
        .CE(p_3_in),
        .D(p_0_in[15]),
        .Q(packet_size_counter_reg[15]),
        .R(s_axis_tvalid_packetnouf));
  FDRE #(
    .INIT(1'b0)) 
    \packet_size_counter_reg[1] 
       (.C(clk),
        .CE(p_3_in),
        .D(p_0_in[1]),
        .Q(packet_size_counter_reg[1]),
        .R(s_axis_tvalid_packetnouf));
  FDRE #(
    .INIT(1'b0)) 
    \packet_size_counter_reg[2] 
       (.C(clk),
        .CE(p_3_in),
        .D(p_0_in[2]),
        .Q(packet_size_counter_reg[2]),
        .R(s_axis_tvalid_packetnouf));
  FDRE #(
    .INIT(1'b0)) 
    \packet_size_counter_reg[3] 
       (.C(clk),
        .CE(p_3_in),
        .D(p_0_in[3]),
        .Q(packet_size_counter_reg[3]),
        .R(s_axis_tvalid_packetnouf));
  FDRE #(
    .INIT(1'b0)) 
    \packet_size_counter_reg[4] 
       (.C(clk),
        .CE(p_3_in),
        .D(p_0_in[4]),
        .Q(packet_size_counter_reg[4]),
        .R(s_axis_tvalid_packetnouf));
  FDRE #(
    .INIT(1'b0)) 
    \packet_size_counter_reg[5] 
       (.C(clk),
        .CE(p_3_in),
        .D(p_0_in[5]),
        .Q(packet_size_counter_reg[5]),
        .R(s_axis_tvalid_packetnouf));
  FDRE #(
    .INIT(1'b0)) 
    \packet_size_counter_reg[6] 
       (.C(clk),
        .CE(p_3_in),
        .D(p_0_in[6]),
        .Q(packet_size_counter_reg[6]),
        .R(s_axis_tvalid_packetnouf));
  FDRE #(
    .INIT(1'b0)) 
    \packet_size_counter_reg[7] 
       (.C(clk),
        .CE(p_3_in),
        .D(p_0_in[7]),
        .Q(packet_size_counter_reg[7]),
        .R(s_axis_tvalid_packetnouf));
  FDRE #(
    .INIT(1'b0)) 
    \packet_size_counter_reg[8] 
       (.C(clk),
        .CE(p_3_in),
        .D(p_0_in[8]),
        .Q(packet_size_counter_reg[8]),
        .R(s_axis_tvalid_packetnouf));
  FDRE #(
    .INIT(1'b0)) 
    \packet_size_counter_reg[9] 
       (.C(clk),
        .CE(p_3_in),
        .D(p_0_in[9]),
        .Q(packet_size_counter_reg[9]),
        .R(s_axis_tvalid_packetnouf));
  (* SOFT_HLUTNM = "soft_lutpair128" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[0]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[0]),
        .O(s_axis_tdata[0]));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[100]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[100]),
        .O(s_axis_tdata[100]));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[101]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[101]),
        .O(s_axis_tdata[101]));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[102]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[102]),
        .O(s_axis_tdata[102]));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[103]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[103]),
        .O(s_axis_tdata[103]));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[104]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[104]),
        .O(s_axis_tdata[104]));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[105]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[105]),
        .O(s_axis_tdata[105]));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[106]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[106]),
        .O(s_axis_tdata[106]));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[107]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[107]),
        .O(s_axis_tdata[107]));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[108]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[108]),
        .O(s_axis_tdata[108]));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[109]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[109]),
        .O(s_axis_tdata[109]));
  (* SOFT_HLUTNM = "soft_lutpair123" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[10]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[10]),
        .O(s_axis_tdata[10]));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[110]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[110]),
        .O(s_axis_tdata[110]));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[111]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[111]),
        .O(s_axis_tdata[111]));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[112]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[112]),
        .O(s_axis_tdata[112]));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[113]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[113]),
        .O(s_axis_tdata[113]));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[114]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[114]),
        .O(s_axis_tdata[114]));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[115]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[115]),
        .O(s_axis_tdata[115]));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[116]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[116]),
        .O(s_axis_tdata[116]));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[117]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[117]),
        .O(s_axis_tdata[117]));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[118]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[118]),
        .O(s_axis_tdata[118]));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[119]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[119]),
        .O(s_axis_tdata[119]));
  (* SOFT_HLUTNM = "soft_lutpair122" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[11]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[11]),
        .O(s_axis_tdata[11]));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[120]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[120]),
        .O(s_axis_tdata[120]));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[121]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[121]),
        .O(s_axis_tdata[121]));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[122]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[122]),
        .O(s_axis_tdata[122]));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[123]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[123]),
        .O(s_axis_tdata[123]));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[124]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[124]),
        .O(s_axis_tdata[124]));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[125]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[125]),
        .O(s_axis_tdata[125]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[126]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[126]),
        .O(s_axis_tdata[126]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[127]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[127]),
        .O(s_axis_tdata[127]));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[128]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[128]),
        .O(s_axis_tdata[128]));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[129]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[129]),
        .O(s_axis_tdata[129]));
  (* SOFT_HLUTNM = "soft_lutpair122" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[12]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[12]),
        .O(s_axis_tdata[12]));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[130]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[130]),
        .O(s_axis_tdata[130]));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[131]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[131]),
        .O(s_axis_tdata[131]));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[132]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[132]),
        .O(s_axis_tdata[132]));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[133]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[133]),
        .O(s_axis_tdata[133]));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[134]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[134]),
        .O(s_axis_tdata[134]));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[135]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[135]),
        .O(s_axis_tdata[135]));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[136]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[136]),
        .O(s_axis_tdata[136]));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[137]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[137]),
        .O(s_axis_tdata[137]));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[138]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[138]),
        .O(s_axis_tdata[138]));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[139]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[139]),
        .O(s_axis_tdata[139]));
  (* SOFT_HLUTNM = "soft_lutpair121" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[13]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[13]),
        .O(s_axis_tdata[13]));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[140]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[140]),
        .O(s_axis_tdata[140]));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[141]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[141]),
        .O(s_axis_tdata[141]));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[142]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[142]),
        .O(s_axis_tdata[142]));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[143]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[143]),
        .O(s_axis_tdata[143]));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[144]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[144]),
        .O(s_axis_tdata[144]));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[145]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[145]),
        .O(s_axis_tdata[145]));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[146]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[146]),
        .O(s_axis_tdata[146]));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[147]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[147]),
        .O(s_axis_tdata[147]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[148]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[148]),
        .O(s_axis_tdata[148]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[149]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[149]),
        .O(s_axis_tdata[149]));
  (* SOFT_HLUTNM = "soft_lutpair121" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[14]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[14]),
        .O(s_axis_tdata[14]));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[150]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[150]),
        .O(s_axis_tdata[150]));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[151]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[151]),
        .O(s_axis_tdata[151]));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[152]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[152]),
        .O(s_axis_tdata[152]));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[153]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[153]),
        .O(s_axis_tdata[153]));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[154]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[154]),
        .O(s_axis_tdata[154]));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[155]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[155]),
        .O(s_axis_tdata[155]));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[156]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[156]),
        .O(s_axis_tdata[156]));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[157]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[157]),
        .O(s_axis_tdata[157]));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[158]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[158]),
        .O(s_axis_tdata[158]));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[159]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[159]),
        .O(s_axis_tdata[159]));
  (* SOFT_HLUTNM = "soft_lutpair120" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[15]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[15]),
        .O(s_axis_tdata[15]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[160]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[160]),
        .O(s_axis_tdata[160]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[161]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[161]),
        .O(s_axis_tdata[161]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[162]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[162]),
        .O(s_axis_tdata[162]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[163]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[163]),
        .O(s_axis_tdata[163]));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[164]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[164]),
        .O(s_axis_tdata[164]));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[165]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[165]),
        .O(s_axis_tdata[165]));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[166]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[166]),
        .O(s_axis_tdata[166]));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[167]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[167]),
        .O(s_axis_tdata[167]));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[168]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[168]),
        .O(s_axis_tdata[168]));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[169]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[169]),
        .O(s_axis_tdata[169]));
  (* SOFT_HLUTNM = "soft_lutpair120" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[16]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[16]),
        .O(s_axis_tdata[16]));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[170]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[170]),
        .O(s_axis_tdata[170]));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[171]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[171]),
        .O(s_axis_tdata[171]));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[172]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[172]),
        .O(s_axis_tdata[172]));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[173]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[173]),
        .O(s_axis_tdata[173]));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[174]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[174]),
        .O(s_axis_tdata[174]));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[175]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[175]),
        .O(s_axis_tdata[175]));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[176]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[176]),
        .O(s_axis_tdata[176]));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[177]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[177]),
        .O(s_axis_tdata[177]));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[178]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[178]),
        .O(s_axis_tdata[178]));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[179]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[179]),
        .O(s_axis_tdata[179]));
  (* SOFT_HLUTNM = "soft_lutpair119" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[17]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[17]),
        .O(s_axis_tdata[17]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[180]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[180]),
        .O(s_axis_tdata[180]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[181]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[181]),
        .O(s_axis_tdata[181]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[182]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[182]),
        .O(s_axis_tdata[182]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[183]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[183]),
        .O(s_axis_tdata[183]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[184]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[184]),
        .O(s_axis_tdata[184]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[185]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[185]),
        .O(s_axis_tdata[185]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[186]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[186]),
        .O(s_axis_tdata[186]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[187]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[187]),
        .O(s_axis_tdata[187]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[188]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[188]),
        .O(s_axis_tdata[188]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[189]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[189]),
        .O(s_axis_tdata[189]));
  (* SOFT_HLUTNM = "soft_lutpair119" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[18]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[18]),
        .O(s_axis_tdata[18]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[190]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[190]),
        .O(s_axis_tdata[190]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[191]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[191]),
        .O(s_axis_tdata[191]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[192]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[192]),
        .O(s_axis_tdata[192]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[193]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[193]),
        .O(s_axis_tdata[193]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[194]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[194]),
        .O(s_axis_tdata[194]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[195]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[195]),
        .O(s_axis_tdata[195]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[196]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[196]),
        .O(s_axis_tdata[196]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[197]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[197]),
        .O(s_axis_tdata[197]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[198]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[198]),
        .O(s_axis_tdata[198]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[199]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[199]),
        .O(s_axis_tdata[199]));
  (* SOFT_HLUTNM = "soft_lutpair118" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[19]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[19]),
        .O(s_axis_tdata[19]));
  (* SOFT_HLUTNM = "soft_lutpair127" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[1]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[1]),
        .O(s_axis_tdata[1]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[200]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[200]),
        .O(s_axis_tdata[200]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[201]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[201]),
        .O(s_axis_tdata[201]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[202]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[202]),
        .O(s_axis_tdata[202]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[203]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[203]),
        .O(s_axis_tdata[203]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[204]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[204]),
        .O(s_axis_tdata[204]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[205]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[205]),
        .O(s_axis_tdata[205]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[206]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[206]),
        .O(s_axis_tdata[206]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[207]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[207]),
        .O(s_axis_tdata[207]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[208]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[208]),
        .O(s_axis_tdata[208]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[209]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[209]),
        .O(s_axis_tdata[209]));
  (* SOFT_HLUTNM = "soft_lutpair118" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[20]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[20]),
        .O(s_axis_tdata[20]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[210]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[210]),
        .O(s_axis_tdata[210]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[211]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[211]),
        .O(s_axis_tdata[211]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[212]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[212]),
        .O(s_axis_tdata[212]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[213]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[213]),
        .O(s_axis_tdata[213]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[214]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[214]),
        .O(s_axis_tdata[214]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[215]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[215]),
        .O(s_axis_tdata[215]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[216]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[216]),
        .O(s_axis_tdata[216]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[217]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[217]),
        .O(s_axis_tdata[217]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[218]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[218]),
        .O(s_axis_tdata[218]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[219]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[219]),
        .O(s_axis_tdata[219]));
  (* SOFT_HLUTNM = "soft_lutpair117" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[21]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[21]),
        .O(s_axis_tdata[21]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[220]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[220]),
        .O(s_axis_tdata[220]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[221]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[221]),
        .O(s_axis_tdata[221]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[222]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[222]),
        .O(s_axis_tdata[222]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[223]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[223]),
        .O(s_axis_tdata[223]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[224]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[224]),
        .O(s_axis_tdata[224]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[225]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[225]),
        .O(s_axis_tdata[225]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[226]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[226]),
        .O(s_axis_tdata[226]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[227]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[227]),
        .O(s_axis_tdata[227]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[228]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[228]),
        .O(s_axis_tdata[228]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[229]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[229]),
        .O(s_axis_tdata[229]));
  (* SOFT_HLUTNM = "soft_lutpair117" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[22]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[22]),
        .O(s_axis_tdata[22]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[230]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[230]),
        .O(s_axis_tdata[230]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[231]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[231]),
        .O(s_axis_tdata[231]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[232]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[232]),
        .O(s_axis_tdata[232]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[233]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[233]),
        .O(s_axis_tdata[233]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[234]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[234]),
        .O(s_axis_tdata[234]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[235]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[235]),
        .O(s_axis_tdata[235]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[236]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[236]),
        .O(s_axis_tdata[236]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[237]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[237]),
        .O(s_axis_tdata[237]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[238]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[238]),
        .O(s_axis_tdata[238]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[239]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[239]),
        .O(s_axis_tdata[239]));
  (* SOFT_HLUTNM = "soft_lutpair116" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[23]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[23]),
        .O(s_axis_tdata[23]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[240]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[240]),
        .O(s_axis_tdata[240]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[241]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[241]),
        .O(s_axis_tdata[241]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[242]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[242]),
        .O(s_axis_tdata[242]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[243]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[243]),
        .O(s_axis_tdata[243]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[244]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[244]),
        .O(s_axis_tdata[244]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[245]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[245]),
        .O(s_axis_tdata[245]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[246]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[246]),
        .O(s_axis_tdata[246]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[247]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[247]),
        .O(s_axis_tdata[247]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[248]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[248]),
        .O(s_axis_tdata[248]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[249]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[249]),
        .O(s_axis_tdata[249]));
  (* SOFT_HLUTNM = "soft_lutpair116" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[24]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[24]),
        .O(s_axis_tdata[24]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[250]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[250]),
        .O(s_axis_tdata[250]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[251]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[251]),
        .O(s_axis_tdata[251]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[252]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[252]),
        .O(s_axis_tdata[252]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[253]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[253]),
        .O(s_axis_tdata[253]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[254]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[254]),
        .O(s_axis_tdata[254]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[255]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[255]),
        .O(s_axis_tdata[255]));
  (* SOFT_HLUTNM = "soft_lutpair115" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[25]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[25]),
        .O(s_axis_tdata[25]));
  (* SOFT_HLUTNM = "soft_lutpair115" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[26]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[26]),
        .O(s_axis_tdata[26]));
  (* SOFT_HLUTNM = "soft_lutpair114" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[27]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[27]),
        .O(s_axis_tdata[27]));
  (* SOFT_HLUTNM = "soft_lutpair114" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[28]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[28]),
        .O(s_axis_tdata[28]));
  (* SOFT_HLUTNM = "soft_lutpair113" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[29]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[29]),
        .O(s_axis_tdata[29]));
  (* SOFT_HLUTNM = "soft_lutpair127" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[2]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[2]),
        .O(s_axis_tdata[2]));
  (* SOFT_HLUTNM = "soft_lutpair113" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[30]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[30]),
        .O(s_axis_tdata[30]));
  (* SOFT_HLUTNM = "soft_lutpair112" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[31]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[31]),
        .O(s_axis_tdata[31]));
  (* SOFT_HLUTNM = "soft_lutpair112" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[32]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[32]),
        .O(s_axis_tdata[32]));
  (* SOFT_HLUTNM = "soft_lutpair111" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[33]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[33]),
        .O(s_axis_tdata[33]));
  (* SOFT_HLUTNM = "soft_lutpair111" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[34]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[34]),
        .O(s_axis_tdata[34]));
  (* SOFT_HLUTNM = "soft_lutpair110" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[35]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[35]),
        .O(s_axis_tdata[35]));
  (* SOFT_HLUTNM = "soft_lutpair110" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[36]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[36]),
        .O(s_axis_tdata[36]));
  (* SOFT_HLUTNM = "soft_lutpair109" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[37]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[37]),
        .O(s_axis_tdata[37]));
  (* SOFT_HLUTNM = "soft_lutpair109" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[38]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[38]),
        .O(s_axis_tdata[38]));
  (* SOFT_HLUTNM = "soft_lutpair108" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[39]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[39]),
        .O(s_axis_tdata[39]));
  (* SOFT_HLUTNM = "soft_lutpair126" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[3]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[3]),
        .O(s_axis_tdata[3]));
  (* SOFT_HLUTNM = "soft_lutpair108" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[40]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[40]),
        .O(s_axis_tdata[40]));
  (* SOFT_HLUTNM = "soft_lutpair107" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[41]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[41]),
        .O(s_axis_tdata[41]));
  (* SOFT_HLUTNM = "soft_lutpair107" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[42]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[42]),
        .O(s_axis_tdata[42]));
  (* SOFT_HLUTNM = "soft_lutpair106" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[43]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[43]),
        .O(s_axis_tdata[43]));
  (* SOFT_HLUTNM = "soft_lutpair106" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[44]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[44]),
        .O(s_axis_tdata[44]));
  (* SOFT_HLUTNM = "soft_lutpair105" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[45]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[45]),
        .O(s_axis_tdata[45]));
  (* SOFT_HLUTNM = "soft_lutpair105" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[46]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[46]),
        .O(s_axis_tdata[46]));
  (* SOFT_HLUTNM = "soft_lutpair104" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[47]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[47]),
        .O(s_axis_tdata[47]));
  (* SOFT_HLUTNM = "soft_lutpair104" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[48]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[48]),
        .O(s_axis_tdata[48]));
  (* SOFT_HLUTNM = "soft_lutpair103" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[49]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[49]),
        .O(s_axis_tdata[49]));
  (* SOFT_HLUTNM = "soft_lutpair126" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[4]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[4]),
        .O(s_axis_tdata[4]));
  (* SOFT_HLUTNM = "soft_lutpair103" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[50]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[50]),
        .O(s_axis_tdata[50]));
  (* SOFT_HLUTNM = "soft_lutpair102" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[51]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[51]),
        .O(s_axis_tdata[51]));
  (* SOFT_HLUTNM = "soft_lutpair102" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[52]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[52]),
        .O(s_axis_tdata[52]));
  (* SOFT_HLUTNM = "soft_lutpair101" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[53]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[53]),
        .O(s_axis_tdata[53]));
  (* SOFT_HLUTNM = "soft_lutpair101" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[54]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[54]),
        .O(s_axis_tdata[54]));
  (* SOFT_HLUTNM = "soft_lutpair100" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[55]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[55]),
        .O(s_axis_tdata[55]));
  (* SOFT_HLUTNM = "soft_lutpair100" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[56]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[56]),
        .O(s_axis_tdata[56]));
  (* SOFT_HLUTNM = "soft_lutpair99" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[57]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[57]),
        .O(s_axis_tdata[57]));
  (* SOFT_HLUTNM = "soft_lutpair99" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[58]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[58]),
        .O(s_axis_tdata[58]));
  (* SOFT_HLUTNM = "soft_lutpair98" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[59]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[59]),
        .O(s_axis_tdata[59]));
  (* SOFT_HLUTNM = "soft_lutpair125" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[5]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[5]),
        .O(s_axis_tdata[5]));
  (* SOFT_HLUTNM = "soft_lutpair98" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[60]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[60]),
        .O(s_axis_tdata[60]));
  (* SOFT_HLUTNM = "soft_lutpair97" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[61]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[61]),
        .O(s_axis_tdata[61]));
  (* SOFT_HLUTNM = "soft_lutpair97" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[62]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[62]),
        .O(s_axis_tdata[62]));
  (* SOFT_HLUTNM = "soft_lutpair128" *) 
  LUT3 #(
    .INIT(8'hF8)) 
    \s_axis_tdata[63]_INST_0 
       (.I0(m_axis_tvalid),
        .I1(m_axis_tdata[63]),
        .I2(stat_FSM),
        .O(s_axis_tdata[63]));
  (* SOFT_HLUTNM = "soft_lutpair96" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[64]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[64]),
        .O(s_axis_tdata[64]));
  (* SOFT_HLUTNM = "soft_lutpair96" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[65]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[65]),
        .O(s_axis_tdata[65]));
  (* SOFT_HLUTNM = "soft_lutpair95" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[66]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[66]),
        .O(s_axis_tdata[66]));
  (* SOFT_HLUTNM = "soft_lutpair95" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[67]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[67]),
        .O(s_axis_tdata[67]));
  (* SOFT_HLUTNM = "soft_lutpair94" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[68]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[68]),
        .O(s_axis_tdata[68]));
  (* SOFT_HLUTNM = "soft_lutpair94" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[69]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[69]),
        .O(s_axis_tdata[69]));
  (* SOFT_HLUTNM = "soft_lutpair125" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[6]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[6]),
        .O(s_axis_tdata[6]));
  (* SOFT_HLUTNM = "soft_lutpair93" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[70]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[70]),
        .O(s_axis_tdata[70]));
  (* SOFT_HLUTNM = "soft_lutpair93" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[71]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[71]),
        .O(s_axis_tdata[71]));
  (* SOFT_HLUTNM = "soft_lutpair92" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[72]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[72]),
        .O(s_axis_tdata[72]));
  (* SOFT_HLUTNM = "soft_lutpair92" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[73]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[73]),
        .O(s_axis_tdata[73]));
  (* SOFT_HLUTNM = "soft_lutpair91" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[74]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[74]),
        .O(s_axis_tdata[74]));
  (* SOFT_HLUTNM = "soft_lutpair91" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[75]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[75]),
        .O(s_axis_tdata[75]));
  (* SOFT_HLUTNM = "soft_lutpair90" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[76]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[76]),
        .O(s_axis_tdata[76]));
  (* SOFT_HLUTNM = "soft_lutpair90" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[77]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[77]),
        .O(s_axis_tdata[77]));
  (* SOFT_HLUTNM = "soft_lutpair89" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[78]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[78]),
        .O(s_axis_tdata[78]));
  (* SOFT_HLUTNM = "soft_lutpair89" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[79]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[79]),
        .O(s_axis_tdata[79]));
  (* SOFT_HLUTNM = "soft_lutpair124" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[7]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[7]),
        .O(s_axis_tdata[7]));
  (* SOFT_HLUTNM = "soft_lutpair88" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[80]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[80]),
        .O(s_axis_tdata[80]));
  (* SOFT_HLUTNM = "soft_lutpair88" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[81]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[81]),
        .O(s_axis_tdata[81]));
  (* SOFT_HLUTNM = "soft_lutpair87" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[82]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[82]),
        .O(s_axis_tdata[82]));
  (* SOFT_HLUTNM = "soft_lutpair87" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[83]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[83]),
        .O(s_axis_tdata[83]));
  (* SOFT_HLUTNM = "soft_lutpair86" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[84]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[84]),
        .O(s_axis_tdata[84]));
  (* SOFT_HLUTNM = "soft_lutpair86" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[85]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[85]),
        .O(s_axis_tdata[85]));
  (* SOFT_HLUTNM = "soft_lutpair85" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[86]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[86]),
        .O(s_axis_tdata[86]));
  (* SOFT_HLUTNM = "soft_lutpair85" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[87]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[87]),
        .O(s_axis_tdata[87]));
  (* SOFT_HLUTNM = "soft_lutpair84" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[88]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[88]),
        .O(s_axis_tdata[88]));
  (* SOFT_HLUTNM = "soft_lutpair84" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[89]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[89]),
        .O(s_axis_tdata[89]));
  (* SOFT_HLUTNM = "soft_lutpair124" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[8]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[8]),
        .O(s_axis_tdata[8]));
  (* SOFT_HLUTNM = "soft_lutpair83" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[90]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[90]),
        .O(s_axis_tdata[90]));
  (* SOFT_HLUTNM = "soft_lutpair83" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[91]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[91]),
        .O(s_axis_tdata[91]));
  (* SOFT_HLUTNM = "soft_lutpair82" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[92]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[92]),
        .O(s_axis_tdata[92]));
  (* SOFT_HLUTNM = "soft_lutpair82" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[93]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[93]),
        .O(s_axis_tdata[93]));
  (* SOFT_HLUTNM = "soft_lutpair81" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[94]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[94]),
        .O(s_axis_tdata[94]));
  (* SOFT_HLUTNM = "soft_lutpair81" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[95]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[95]),
        .O(s_axis_tdata[95]));
  (* SOFT_HLUTNM = "soft_lutpair80" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[96]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[96]),
        .O(s_axis_tdata[96]));
  (* SOFT_HLUTNM = "soft_lutpair80" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[97]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[97]),
        .O(s_axis_tdata[97]));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[98]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[98]),
        .O(s_axis_tdata[98]));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \s_axis_tdata[99]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[99]),
        .O(s_axis_tdata[99]));
  (* SOFT_HLUTNM = "soft_lutpair123" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \s_axis_tdata[9]_INST_0 
       (.I0(stat_FSM),
        .I1(m_axis_tvalid),
        .I2(m_axis_tdata[9]),
        .O(s_axis_tdata[9]));
  LUT1 #(
    .INIT(2'h1)) 
    \s_axis_tkeep[31]_INST_0 
       (.I0(focus_tkeep),
        .O(s_axis_tkeep));
  LUT6 #(
    .INIT(64'hEEEAEAEAEAEAEAEA)) 
    s_axis_tlast_INST_0
       (.I0(focus_tlast_reg_n_0),
        .I1(m_axis_tlast),
        .I2(s_axis_tlast_INST_0_i_1_n_0),
        .I3(packet_size_counter_reg[0]),
        .I4(packet_size_counter_reg[1]),
        .I5(packet_size_counter_reg[2]),
        .O(s_axis_tlast));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    s_axis_tlast_INST_0_i_1
       (.I0(packet_size_counter_reg[7]),
        .I1(packet_size_counter_reg[10]),
        .I2(packet_size_counter_reg[3]),
        .I3(packet_size_counter_reg[5]),
        .I4(s_axis_tlast_INST_0_i_2_n_0),
        .I5(s_axis_tlast_INST_0_i_3_n_0),
        .O(s_axis_tlast_INST_0_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    s_axis_tlast_INST_0_i_2
       (.I0(packet_size_counter_reg[11]),
        .I1(packet_size_counter_reg[9]),
        .I2(packet_size_counter_reg[13]),
        .I3(packet_size_counter_reg[12]),
        .O(s_axis_tlast_INST_0_i_2_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    s_axis_tlast_INST_0_i_3
       (.I0(packet_size_counter_reg[4]),
        .I1(packet_size_counter_reg[14]),
        .I2(packet_size_counter_reg[15]),
        .I3(packet_size_counter_reg[8]),
        .I4(packet_size_counter_reg[6]),
        .O(s_axis_tlast_INST_0_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair130" *) 
  LUT2 #(
    .INIT(4'hE)) 
    s_axis_tvalid_INST_0
       (.I0(m_axis_tvalid),
        .I1(s_axis_tvalid_packetnouf_reg_n_0),
        .O(s_axis_tvalid));
  LUT6 #(
    .INIT(64'h07FF07FF07000000)) 
    s_axis_tvalid_packetnouf_i_1
       (.I0(\packet_size_counter[15]_i_3_n_0 ),
        .I1(m_axis_tlast),
        .I2(focus_tlast_reg_n_0),
        .I3(s_axis_tready),
        .I4(m_axis_tvalid),
        .I5(s_axis_tvalid_packetnouf_reg_n_0),
        .O(s_axis_tvalid_packetnouf_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    s_axis_tvalid_packetnouf_reg
       (.C(clk),
        .CE(1'b1),
        .D(s_axis_tvalid_packetnouf_i_1_n_0),
        .Q(s_axis_tvalid_packetnouf_reg_n_0),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hAAA8AFA8)) 
    stat_FSM_i_1
       (.I0(stat_FSM),
        .I1(focus_tlast_i_5_n_0),
        .I2(s_axis_tlast_INST_0_i_1_n_0),
        .I3(focus_tlast_i_4_n_0),
        .I4(stat_FSM_i_2_n_0),
        .O(stat_FSM_next));
  (* SOFT_HLUTNM = "soft_lutpair131" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    stat_FSM_i_2
       (.I0(m_axis_tvalid),
        .I1(s_axis_tready),
        .I2(m_axis_tlast),
        .O(stat_FSM_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    stat_FSM_reg
       (.C(clk),
        .CE(1'b1),
        .D(stat_FSM_next),
        .Q(stat_FSM),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
