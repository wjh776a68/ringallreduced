//Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
//Date        : Tue Dec  6 16:10:17 2022
//Host        : dell-Vostro-3910-China-HDD-Protection running 64-bit Ubuntu 22.04.1 LTS
//Command     : generate_target design_qsfp_40G_wrapper.bd
//Design      : design_qsfp_40G_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_qsfp_40G_wrapper
   (clk,
    dclk,
    gt_ref_clk_0_clk_n,
    gt_ref_clk_0_clk_p,
    gt_ref_clk_1_clk_n,
    gt_ref_clk_1_clk_p,
    gt_serial_port_0_grx_n,
    gt_serial_port_0_grx_p,
    gt_serial_port_0_gtx_n,
    gt_serial_port_0_gtx_p,
    gt_serial_port_1_grx_n,
    gt_serial_port_1_grx_p,
    gt_serial_port_1_gtx_n,
    gt_serial_port_1_gtx_p,
    reset,
    sfp_m_axis_tdata,
    sfp_m_axis_tkeep,
    sfp_m_axis_tlast,
    sfp_m_axis_tready,
    sfp_m_axis_tuser,
    sfp_m_axis_tvalid,
    sfp_s_axis_tdata,
    sfp_s_axis_tkeep,
    sfp_s_axis_tlast,
    sfp_s_axis_tready,
    sfp_s_axis_tuser,
    sfp_s_axis_tvalid,
    stat_rx_status_sfp0,
    stat_rx_status_sfp1);
  input clk;
  input dclk;
  input gt_ref_clk_0_clk_n;
  input gt_ref_clk_0_clk_p;
  input gt_ref_clk_1_clk_n;
  input gt_ref_clk_1_clk_p;
  input [3:0]gt_serial_port_0_grx_n;
  input [3:0]gt_serial_port_0_grx_p;
  output [3:0]gt_serial_port_0_gtx_n;
  output [3:0]gt_serial_port_0_gtx_p;
  input [3:0]gt_serial_port_1_grx_n;
  input [3:0]gt_serial_port_1_grx_p;
  output [3:0]gt_serial_port_1_gtx_n;
  output [3:0]gt_serial_port_1_gtx_p;
  input reset;
  output [255:0]sfp_m_axis_tdata;
  output [31:0]sfp_m_axis_tkeep;
  output sfp_m_axis_tlast;
  input sfp_m_axis_tready;
  output [0:0]sfp_m_axis_tuser;
  output sfp_m_axis_tvalid;
  input [255:0]sfp_s_axis_tdata;
  input [31:0]sfp_s_axis_tkeep;
  input sfp_s_axis_tlast;
  output sfp_s_axis_tready;
  input [0:0]sfp_s_axis_tuser;
  input sfp_s_axis_tvalid;
  output stat_rx_status_sfp0;
  output stat_rx_status_sfp1;

  wire clk;
  wire dclk;
  wire gt_ref_clk_0_clk_n;
  wire gt_ref_clk_0_clk_p;
  wire gt_ref_clk_1_clk_n;
  wire gt_ref_clk_1_clk_p;
  wire [3:0]gt_serial_port_0_grx_n;
  wire [3:0]gt_serial_port_0_grx_p;
  wire [3:0]gt_serial_port_0_gtx_n;
  wire [3:0]gt_serial_port_0_gtx_p;
  wire [3:0]gt_serial_port_1_grx_n;
  wire [3:0]gt_serial_port_1_grx_p;
  wire [3:0]gt_serial_port_1_gtx_n;
  wire [3:0]gt_serial_port_1_gtx_p;
  wire reset;
  wire [255:0]sfp_m_axis_tdata;
  wire [31:0]sfp_m_axis_tkeep;
  wire sfp_m_axis_tlast;
  wire sfp_m_axis_tready;
  wire [0:0]sfp_m_axis_tuser;
  wire sfp_m_axis_tvalid;
  wire [255:0]sfp_s_axis_tdata;
  wire [31:0]sfp_s_axis_tkeep;
  wire sfp_s_axis_tlast;
  wire sfp_s_axis_tready;
  wire [0:0]sfp_s_axis_tuser;
  wire sfp_s_axis_tvalid;
  wire stat_rx_status_sfp0;
  wire stat_rx_status_sfp1;

  design_qsfp_40G design_qsfp_40G_i
       (.clk(clk),
        .dclk(dclk),
        .gt_ref_clk_0_clk_n(gt_ref_clk_0_clk_n),
        .gt_ref_clk_0_clk_p(gt_ref_clk_0_clk_p),
        .gt_ref_clk_1_clk_n(gt_ref_clk_1_clk_n),
        .gt_ref_clk_1_clk_p(gt_ref_clk_1_clk_p),
        .gt_serial_port_0_grx_n(gt_serial_port_0_grx_n),
        .gt_serial_port_0_grx_p(gt_serial_port_0_grx_p),
        .gt_serial_port_0_gtx_n(gt_serial_port_0_gtx_n),
        .gt_serial_port_0_gtx_p(gt_serial_port_0_gtx_p),
        .gt_serial_port_1_grx_n(gt_serial_port_1_grx_n),
        .gt_serial_port_1_grx_p(gt_serial_port_1_grx_p),
        .gt_serial_port_1_gtx_n(gt_serial_port_1_gtx_n),
        .gt_serial_port_1_gtx_p(gt_serial_port_1_gtx_p),
        .reset(reset),
        .sfp_m_axis_tdata(sfp_m_axis_tdata),
        .sfp_m_axis_tkeep(sfp_m_axis_tkeep),
        .sfp_m_axis_tlast(sfp_m_axis_tlast),
        .sfp_m_axis_tready(sfp_m_axis_tready),
        .sfp_m_axis_tuser(sfp_m_axis_tuser),
        .sfp_m_axis_tvalid(sfp_m_axis_tvalid),
        .sfp_s_axis_tdata(sfp_s_axis_tdata),
        .sfp_s_axis_tkeep(sfp_s_axis_tkeep),
        .sfp_s_axis_tlast(sfp_s_axis_tlast),
        .sfp_s_axis_tready(sfp_s_axis_tready),
        .sfp_s_axis_tuser(sfp_s_axis_tuser),
        .sfp_s_axis_tvalid(sfp_s_axis_tvalid),
        .stat_rx_status_sfp0(stat_rx_status_sfp0),
        .stat_rx_status_sfp1(stat_rx_status_sfp1));
endmodule
