//Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
//Date        : Tue Dec  6 16:10:17 2022
//Host        : dell-Vostro-3910-China-HDD-Protection running 64-bit Ubuntu 22.04.1 LTS
//Command     : generate_target design_qsfp_40G.bd
//Design      : design_qsfp_40G
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CORE_GENERATION_INFO = "design_qsfp_40G,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=design_qsfp_40G,x_ipVersion=1.00.a,x_ipLanguage=VERILOG,numBlks=30,numReposBlks=30,numNonXlnxBlks=0,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=2,numPkgbdBlks=0,bdsource=USER,synth_mode=OOC_per_IP}" *) (* HW_HANDOFF = "design_qsfp_40G.hwdef" *) 
module design_qsfp_40G
   (clk,
    dclk,
    gt_ref_clk_0_clk_n,
    gt_ref_clk_0_clk_p,
    gt_ref_clk_1_clk_n,
    gt_ref_clk_1_clk_p,
    gt_serial_port_0_grx_n,
    gt_serial_port_0_grx_p,
    gt_serial_port_0_gtx_n,
    gt_serial_port_0_gtx_p,
    gt_serial_port_1_grx_n,
    gt_serial_port_1_grx_p,
    gt_serial_port_1_gtx_n,
    gt_serial_port_1_gtx_p,
    reset,
    sfp_m_axis_tdata,
    sfp_m_axis_tkeep,
    sfp_m_axis_tlast,
    sfp_m_axis_tready,
    sfp_m_axis_tuser,
    sfp_m_axis_tvalid,
    sfp_s_axis_tdata,
    sfp_s_axis_tkeep,
    sfp_s_axis_tlast,
    sfp_s_axis_tready,
    sfp_s_axis_tuser,
    sfp_s_axis_tvalid,
    stat_rx_status_sfp0,
    stat_rx_status_sfp1);
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 CLK.CLK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME CLK.CLK, ASSOCIATED_BUSIF sfp_s_axis:sfp_m_axis, CLK_DOMAIN design_qsfp_40G_clk, FREQ_HZ 320000000, FREQ_TOLERANCE_HZ 0, INSERT_VIP 0, PHASE 0.0" *) input clk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 CLK.DCLK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME CLK.DCLK, CLK_DOMAIN design_qsfp_40G_dclk, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, INSERT_VIP 0, PHASE 0.0" *) input dclk;
  (* X_INTERFACE_INFO = "xilinx.com:interface:diff_clock:1.0 gt_ref_clk_0 CLK_N" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME gt_ref_clk_0, CAN_DEBUG false, FREQ_HZ 156250000" *) input gt_ref_clk_0_clk_n;
  (* X_INTERFACE_INFO = "xilinx.com:interface:diff_clock:1.0 gt_ref_clk_0 CLK_P" *) input gt_ref_clk_0_clk_p;
  (* X_INTERFACE_INFO = "xilinx.com:interface:diff_clock:1.0 gt_ref_clk_1 CLK_N" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME gt_ref_clk_1, CAN_DEBUG false, FREQ_HZ 156250000" *) input gt_ref_clk_1_clk_n;
  (* X_INTERFACE_INFO = "xilinx.com:interface:diff_clock:1.0 gt_ref_clk_1 CLK_P" *) input gt_ref_clk_1_clk_p;
  (* X_INTERFACE_INFO = "xilinx.com:interface:gt:1.0 gt_serial_port_0 GRX_N" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME gt_serial_port_0, CAN_DEBUG false" *) input [3:0]gt_serial_port_0_grx_n;
  (* X_INTERFACE_INFO = "xilinx.com:interface:gt:1.0 gt_serial_port_0 GRX_P" *) input [3:0]gt_serial_port_0_grx_p;
  (* X_INTERFACE_INFO = "xilinx.com:interface:gt:1.0 gt_serial_port_0 GTX_N" *) output [3:0]gt_serial_port_0_gtx_n;
  (* X_INTERFACE_INFO = "xilinx.com:interface:gt:1.0 gt_serial_port_0 GTX_P" *) output [3:0]gt_serial_port_0_gtx_p;
  (* X_INTERFACE_INFO = "xilinx.com:interface:gt:1.0 gt_serial_port_1 GRX_N" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME gt_serial_port_1, CAN_DEBUG false" *) input [3:0]gt_serial_port_1_grx_n;
  (* X_INTERFACE_INFO = "xilinx.com:interface:gt:1.0 gt_serial_port_1 GRX_P" *) input [3:0]gt_serial_port_1_grx_p;
  (* X_INTERFACE_INFO = "xilinx.com:interface:gt:1.0 gt_serial_port_1 GTX_N" *) output [3:0]gt_serial_port_1_gtx_n;
  (* X_INTERFACE_INFO = "xilinx.com:interface:gt:1.0 gt_serial_port_1 GTX_P" *) output [3:0]gt_serial_port_1_gtx_p;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 RST.RESET RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME RST.RESET, INSERT_VIP 0, POLARITY ACTIVE_HIGH" *) input reset;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 sfp_m_axis TDATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME sfp_m_axis, CLK_DOMAIN design_qsfp_40G_clk, FREQ_HZ 320000000, HAS_TKEEP 1, HAS_TLAST 1, HAS_TREADY 1, HAS_TSTRB 0, INSERT_VIP 0, LAYERED_METADATA undef, PHASE 0.0, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 1" *) output [255:0]sfp_m_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 sfp_m_axis TKEEP" *) output [31:0]sfp_m_axis_tkeep;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 sfp_m_axis TLAST" *) output sfp_m_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 sfp_m_axis TREADY" *) input sfp_m_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 sfp_m_axis TUSER" *) output [0:0]sfp_m_axis_tuser;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 sfp_m_axis TVALID" *) output sfp_m_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 sfp_s_axis TDATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME sfp_s_axis, CLK_DOMAIN design_qsfp_40G_clk, FREQ_HZ 320000000, HAS_TKEEP 1, HAS_TLAST 1, HAS_TREADY 1, HAS_TSTRB 0, INSERT_VIP 0, LAYERED_METADATA undef, PHASE 0.0, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 1" *) input [255:0]sfp_s_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 sfp_s_axis TKEEP" *) input [31:0]sfp_s_axis_tkeep;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 sfp_s_axis TLAST" *) input sfp_s_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 sfp_s_axis TREADY" *) output sfp_s_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 sfp_s_axis TUSER" *) input [0:0]sfp_s_axis_tuser;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 sfp_s_axis TVALID" *) input sfp_s_axis_tvalid;
  output stat_rx_status_sfp0;
  output stat_rx_status_sfp1;

  wire [255:0]S_AXIS_0_1_TDATA;
  wire [31:0]S_AXIS_0_1_TKEEP;
  wire S_AXIS_0_1_TLAST;
  wire S_AXIS_0_1_TREADY;
  wire [0:0]S_AXIS_0_1_TUSER;
  wire S_AXIS_0_1_TVALID;
  wire [255:0]axis_data_fifo_0_M_AXIS_TDATA;
  wire [31:0]axis_data_fifo_0_M_AXIS_TKEEP;
  wire axis_data_fifo_0_M_AXIS_TLAST;
  wire axis_data_fifo_0_M_AXIS_TREADY;
  wire [0:0]axis_data_fifo_0_M_AXIS_TUSER;
  wire axis_data_fifo_0_M_AXIS_TVALID;
  wire [255:0]axis_data_fifo_4_M_AXIS_TDATA;
  wire [31:0]axis_data_fifo_4_M_AXIS_TKEEP;
  wire axis_data_fifo_4_M_AXIS_TLAST;
  wire axis_data_fifo_4_M_AXIS_TREADY;
  wire [0:0]axis_data_fifo_4_M_AXIS_TUSER;
  wire axis_data_fifo_4_M_AXIS_TVALID;
  wire clk_wiz_clk_out1;
  wire [255:0]forward_0_s_axis_TDATA;
  wire [31:0]forward_0_s_axis_TKEEP;
  wire forward_0_s_axis_TLAST;
  wire forward_0_s_axis_TREADY;
  wire [0:0]forward_0_s_axis_TUSER;
  wire forward_0_s_axis_TVALID;
  wire gt_ref_clk_0_1_CLK_N;
  wire gt_ref_clk_0_1_CLK_P;
  wire gt_ref_clk_1_1_CLK_N;
  wire gt_ref_clk_1_1_CLK_P;
  wire [3:0]l_ethernet_0_gt_serial_port_GRX_N;
  wire [3:0]l_ethernet_0_gt_serial_port_GRX_P;
  wire [3:0]l_ethernet_0_gt_serial_port_GTX_N;
  wire [3:0]l_ethernet_0_gt_serial_port_GTX_P;
  wire l_ethernet_0_rx_clk_out_0;
  wire l_ethernet_0_stat_rx_status_0;
  wire l_ethernet_0_tx_clk_out_0;
  wire [255:0]l_ethernet_1_axis_rx_0_TDATA;
  wire [31:0]l_ethernet_1_axis_rx_0_TKEEP;
  wire l_ethernet_1_axis_rx_0_TLAST;
  wire [0:0]l_ethernet_1_axis_rx_0_TUSER;
  wire l_ethernet_1_axis_rx_0_TVALID;
  wire [3:0]l_ethernet_1_gt_serial_port_GRX_N;
  wire [3:0]l_ethernet_1_gt_serial_port_GRX_P;
  wire [3:0]l_ethernet_1_gt_serial_port_GTX_N;
  wire [3:0]l_ethernet_1_gt_serial_port_GTX_P;
  wire l_ethernet_1_rx_clk_out_0;
  wire l_ethernet_1_stat_rx_status_0;
  wire l_ethernet_1_user_rx_reset_0;
  wire [0:0]proc_sys_reset_0_peripheral_aresetn;
  wire [0:0]proc_sys_reset_1_peripheral_aresetn;
  wire [255:0]recv_adjustpacket_0_s_axis_TDATA;
  wire [31:0]recv_adjustpacket_0_s_axis_TKEEP;
  wire recv_adjustpacket_0_s_axis_TLAST;
  wire recv_adjustpacket_0_s_axis_TREADY;
  wire [0:0]recv_adjustpacket_0_s_axis_TUSER;
  wire recv_adjustpacket_0_s_axis_TVALID;
  wire reset_1;
  wire [0:0]rst_clk_wiz_100M_peripheral_reset;
  wire [0:0]xlconstant_0_dout;
  wire [7:0]xlconstant_10_dout;
  wire [11:0]xlconstant_11_dout;
  wire [11:0]xlconstant_2_dout;
  wire [55:0]xlconstant_4_dout;
  wire [0:0]xlconstant_5_dout;
  wire [0:0]xlconstant_6_dout;
  wire [3:0]xlconstant_7_dout;
  wire [14:0]xlconstant_9_dout;
  wire xxv_ethernet_0_tx_clk_out_0;
  wire NLW_l_ethernet_0_gt_refclk_out_UNCONNECTED;
  wire NLW_l_ethernet_0_rx_axis_tlast_0_UNCONNECTED;
  wire NLW_l_ethernet_0_rx_axis_tvalid_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_aligned_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_aligned_err_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_bad_preamble_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_bad_sfd_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_bip_err_0_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_bip_err_1_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_bip_err_2_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_bip_err_3_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_broadcast_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_framing_err_0_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_framing_err_1_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_framing_err_2_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_framing_err_3_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_framing_err_valid_0_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_framing_err_valid_1_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_framing_err_valid_2_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_framing_err_valid_3_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_got_signal_os_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_hi_ber_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_inrangeerr_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_internal_local_fault_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_jabber_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_local_fault_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_misaligned_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_multicast_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_oversize_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_packet_1024_1518_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_packet_128_255_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_packet_1519_1522_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_packet_1523_1548_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_packet_1549_2047_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_packet_2048_4095_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_packet_256_511_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_packet_4096_8191_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_packet_512_1023_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_packet_64_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_packet_65_127_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_packet_8192_9215_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_packet_bad_fcs_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_packet_large_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_received_local_fault_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_remote_fault_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_toolong_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_total_good_packets_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_truncated_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_unicast_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_rx_vlan_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_tx_bad_fcs_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_tx_broadcast_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_tx_frame_error_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_tx_local_fault_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_tx_multicast_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_tx_overflow_err_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_tx_packet_1024_1518_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_tx_packet_128_255_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_tx_packet_1519_1522_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_tx_packet_1523_1548_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_tx_packet_1549_2047_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_tx_packet_2048_4095_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_tx_packet_256_511_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_tx_packet_4096_8191_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_tx_packet_512_1023_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_tx_packet_64_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_tx_packet_65_127_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_tx_packet_8192_9215_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_tx_packet_large_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_tx_packet_small_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_tx_total_good_packets_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_tx_total_packets_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_tx_underflow_err_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_tx_unicast_0_UNCONNECTED;
  wire NLW_l_ethernet_0_stat_tx_vlan_0_UNCONNECTED;
  wire NLW_l_ethernet_0_tx_unfout_0_UNCONNECTED;
  wire NLW_l_ethernet_0_user_rx_reset_0_UNCONNECTED;
  wire NLW_l_ethernet_0_user_tx_reset_0_UNCONNECTED;
  wire [3:0]NLW_l_ethernet_0_gtpowergood_out_0_UNCONNECTED;
  wire [255:0]NLW_l_ethernet_0_rx_axis_tdata_0_UNCONNECTED;
  wire [31:0]NLW_l_ethernet_0_rx_axis_tkeep_0_UNCONNECTED;
  wire [0:0]NLW_l_ethernet_0_rx_axis_tuser_0_UNCONNECTED;
  wire [55:0]NLW_l_ethernet_0_rx_preambleout_0_UNCONNECTED;
  wire [3:0]NLW_l_ethernet_0_rxrecclkout_0_UNCONNECTED;
  wire [1:0]NLW_l_ethernet_0_stat_rx_bad_code_0_UNCONNECTED;
  wire [1:0]NLW_l_ethernet_0_stat_rx_bad_fcs_0_UNCONNECTED;
  wire [3:0]NLW_l_ethernet_0_stat_rx_block_lock_0_UNCONNECTED;
  wire [1:0]NLW_l_ethernet_0_stat_rx_fragment_0_UNCONNECTED;
  wire [3:0]NLW_l_ethernet_0_stat_rx_mf_err_0_UNCONNECTED;
  wire [3:0]NLW_l_ethernet_0_stat_rx_mf_len_err_0_UNCONNECTED;
  wire [3:0]NLW_l_ethernet_0_stat_rx_mf_repeat_err_0_UNCONNECTED;
  wire [1:0]NLW_l_ethernet_0_stat_rx_packet_small_0_UNCONNECTED;
  wire [1:0]NLW_l_ethernet_0_stat_rx_stomped_fcs_0_UNCONNECTED;
  wire [3:0]NLW_l_ethernet_0_stat_rx_synced_0_UNCONNECTED;
  wire [3:0]NLW_l_ethernet_0_stat_rx_synced_err_0_UNCONNECTED;
  wire [1:0]NLW_l_ethernet_0_stat_rx_test_pattern_mismatch_0_UNCONNECTED;
  wire [5:0]NLW_l_ethernet_0_stat_rx_total_bytes_0_UNCONNECTED;
  wire [13:0]NLW_l_ethernet_0_stat_rx_total_good_bytes_0_UNCONNECTED;
  wire [1:0]NLW_l_ethernet_0_stat_rx_total_packets_0_UNCONNECTED;
  wire [1:0]NLW_l_ethernet_0_stat_rx_undersize_0_UNCONNECTED;
  wire [3:0]NLW_l_ethernet_0_stat_rx_vl_demuxed_0_UNCONNECTED;
  wire [1:0]NLW_l_ethernet_0_stat_rx_vl_number_0_0_UNCONNECTED;
  wire [1:0]NLW_l_ethernet_0_stat_rx_vl_number_1_0_UNCONNECTED;
  wire [1:0]NLW_l_ethernet_0_stat_rx_vl_number_2_0_UNCONNECTED;
  wire [1:0]NLW_l_ethernet_0_stat_rx_vl_number_3_0_UNCONNECTED;
  wire [4:0]NLW_l_ethernet_0_stat_tx_total_bytes_0_UNCONNECTED;
  wire [13:0]NLW_l_ethernet_0_stat_tx_total_good_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_1_gt_refclk_out_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_aligned_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_aligned_err_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_bad_preamble_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_bad_sfd_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_bip_err_0_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_bip_err_1_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_bip_err_2_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_bip_err_3_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_broadcast_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_framing_err_0_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_framing_err_1_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_framing_err_2_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_framing_err_3_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_framing_err_valid_0_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_framing_err_valid_1_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_framing_err_valid_2_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_framing_err_valid_3_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_got_signal_os_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_hi_ber_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_inrangeerr_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_internal_local_fault_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_jabber_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_local_fault_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_misaligned_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_multicast_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_oversize_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_packet_1024_1518_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_packet_128_255_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_packet_1519_1522_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_packet_1523_1548_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_packet_1549_2047_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_packet_2048_4095_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_packet_256_511_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_packet_4096_8191_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_packet_512_1023_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_packet_64_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_packet_65_127_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_packet_8192_9215_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_packet_bad_fcs_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_packet_large_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_received_local_fault_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_remote_fault_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_toolong_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_total_good_packets_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_truncated_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_unicast_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_rx_vlan_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_tx_bad_fcs_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_tx_broadcast_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_tx_frame_error_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_tx_local_fault_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_tx_multicast_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_tx_overflow_err_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_tx_packet_1024_1518_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_tx_packet_128_255_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_tx_packet_1519_1522_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_tx_packet_1523_1548_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_tx_packet_1549_2047_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_tx_packet_2048_4095_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_tx_packet_256_511_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_tx_packet_4096_8191_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_tx_packet_512_1023_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_tx_packet_64_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_tx_packet_65_127_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_tx_packet_8192_9215_bytes_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_tx_packet_large_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_tx_packet_small_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_tx_total_good_packets_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_tx_total_packets_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_tx_underflow_err_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_tx_unicast_0_UNCONNECTED;
  wire NLW_l_ethernet_1_stat_tx_vlan_0_UNCONNECTED;
  wire NLW_l_ethernet_1_tx_axis_tready_0_UNCONNECTED;
  wire NLW_l_ethernet_1_tx_clk_out_0_UNCONNECTED;
  wire NLW_l_ethernet_1_tx_unfout_0_UNCONNECTED;
  wire NLW_l_ethernet_1_user_tx_reset_0_UNCONNECTED;
  wire [3:0]NLW_l_ethernet_1_gtpowergood_out_0_UNCONNECTED;
  wire [55:0]NLW_l_ethernet_1_rx_preambleout_0_UNCONNECTED;
  wire [3:0]NLW_l_ethernet_1_rxrecclkout_0_UNCONNECTED;
  wire [1:0]NLW_l_ethernet_1_stat_rx_bad_code_0_UNCONNECTED;
  wire [1:0]NLW_l_ethernet_1_stat_rx_bad_fcs_0_UNCONNECTED;
  wire [3:0]NLW_l_ethernet_1_stat_rx_block_lock_0_UNCONNECTED;
  wire [1:0]NLW_l_ethernet_1_stat_rx_fragment_0_UNCONNECTED;
  wire [3:0]NLW_l_ethernet_1_stat_rx_mf_err_0_UNCONNECTED;
  wire [3:0]NLW_l_ethernet_1_stat_rx_mf_len_err_0_UNCONNECTED;
  wire [3:0]NLW_l_ethernet_1_stat_rx_mf_repeat_err_0_UNCONNECTED;
  wire [1:0]NLW_l_ethernet_1_stat_rx_packet_small_0_UNCONNECTED;
  wire [1:0]NLW_l_ethernet_1_stat_rx_stomped_fcs_0_UNCONNECTED;
  wire [3:0]NLW_l_ethernet_1_stat_rx_synced_0_UNCONNECTED;
  wire [3:0]NLW_l_ethernet_1_stat_rx_synced_err_0_UNCONNECTED;
  wire [1:0]NLW_l_ethernet_1_stat_rx_test_pattern_mismatch_0_UNCONNECTED;
  wire [5:0]NLW_l_ethernet_1_stat_rx_total_bytes_0_UNCONNECTED;
  wire [13:0]NLW_l_ethernet_1_stat_rx_total_good_bytes_0_UNCONNECTED;
  wire [1:0]NLW_l_ethernet_1_stat_rx_total_packets_0_UNCONNECTED;
  wire [1:0]NLW_l_ethernet_1_stat_rx_undersize_0_UNCONNECTED;
  wire [3:0]NLW_l_ethernet_1_stat_rx_vl_demuxed_0_UNCONNECTED;
  wire [1:0]NLW_l_ethernet_1_stat_rx_vl_number_0_0_UNCONNECTED;
  wire [1:0]NLW_l_ethernet_1_stat_rx_vl_number_1_0_UNCONNECTED;
  wire [1:0]NLW_l_ethernet_1_stat_rx_vl_number_2_0_UNCONNECTED;
  wire [1:0]NLW_l_ethernet_1_stat_rx_vl_number_3_0_UNCONNECTED;
  wire [4:0]NLW_l_ethernet_1_stat_tx_total_bytes_0_UNCONNECTED;
  wire [13:0]NLW_l_ethernet_1_stat_tx_total_good_bytes_0_UNCONNECTED;
  wire NLW_proc_sys_reset_0_mb_reset_UNCONNECTED;
  wire [0:0]NLW_proc_sys_reset_0_bus_struct_reset_UNCONNECTED;
  wire [0:0]NLW_proc_sys_reset_0_interconnect_aresetn_UNCONNECTED;
  wire [0:0]NLW_proc_sys_reset_0_peripheral_reset_UNCONNECTED;
  wire NLW_proc_sys_reset_1_mb_reset_UNCONNECTED;
  wire [0:0]NLW_proc_sys_reset_1_bus_struct_reset_UNCONNECTED;
  wire [0:0]NLW_proc_sys_reset_1_interconnect_aresetn_UNCONNECTED;
  wire [0:0]NLW_proc_sys_reset_1_peripheral_reset_UNCONNECTED;
  wire NLW_recv_adjustpacket_0_m_axis_tready_UNCONNECTED;
  wire NLW_rst_clk_wiz_100M_mb_reset_UNCONNECTED;
  wire [0:0]NLW_rst_clk_wiz_100M_bus_struct_reset_UNCONNECTED;
  wire [0:0]NLW_rst_clk_wiz_100M_interconnect_aresetn_UNCONNECTED;
  wire [0:0]NLW_rst_clk_wiz_100M_peripheral_aresetn_UNCONNECTED;
  wire [15:0]NLW_xlconstant_1_dout_UNCONNECTED;
  wire [0:0]NLW_xlconstant_12_dout_UNCONNECTED;
  wire [47:0]NLW_xlconstant_13_dout_UNCONNECTED;
  wire [47:0]NLW_xlconstant_14_dout_UNCONNECTED;
  wire [15:0]NLW_xlconstant_15_dout_UNCONNECTED;
  wire [15:0]NLW_xlconstant_16_dout_UNCONNECTED;
  wire [0:0]NLW_xlconstant_17_dout_UNCONNECTED;
  wire [0:0]NLW_xlconstant_18_dout_UNCONNECTED;
  wire [0:0]NLW_xlconstant_19_dout_UNCONNECTED;
  wire [8:0]NLW_xlconstant_20_dout_UNCONNECTED;
  wire [0:0]NLW_xlconstant_3_dout_UNCONNECTED;
  wire [57:0]NLW_xlconstant_8_dout_UNCONNECTED;

  assign S_AXIS_0_1_TDATA = sfp_s_axis_tdata[255:0];
  assign S_AXIS_0_1_TKEEP = sfp_s_axis_tkeep[31:0];
  assign S_AXIS_0_1_TLAST = sfp_s_axis_tlast;
  assign S_AXIS_0_1_TUSER = sfp_s_axis_tuser[0];
  assign S_AXIS_0_1_TVALID = sfp_s_axis_tvalid;
  assign axis_data_fifo_4_M_AXIS_TREADY = sfp_m_axis_tready;
  assign clk_wiz_clk_out1 = dclk;
  assign gt_ref_clk_0_1_CLK_N = gt_ref_clk_1_clk_n;
  assign gt_ref_clk_0_1_CLK_P = gt_ref_clk_1_clk_p;
  assign gt_ref_clk_1_1_CLK_N = gt_ref_clk_0_clk_n;
  assign gt_ref_clk_1_1_CLK_P = gt_ref_clk_0_clk_p;
  assign gt_serial_port_0_gtx_n[3:0] = l_ethernet_0_gt_serial_port_GTX_N;
  assign gt_serial_port_0_gtx_p[3:0] = l_ethernet_0_gt_serial_port_GTX_P;
  assign gt_serial_port_1_gtx_n[3:0] = l_ethernet_1_gt_serial_port_GTX_N;
  assign gt_serial_port_1_gtx_p[3:0] = l_ethernet_1_gt_serial_port_GTX_P;
  assign l_ethernet_0_gt_serial_port_GRX_N = gt_serial_port_0_grx_n[3:0];
  assign l_ethernet_0_gt_serial_port_GRX_P = gt_serial_port_0_grx_p[3:0];
  assign l_ethernet_0_tx_clk_out_0 = clk;
  assign l_ethernet_1_gt_serial_port_GRX_N = gt_serial_port_1_grx_n[3:0];
  assign l_ethernet_1_gt_serial_port_GRX_P = gt_serial_port_1_grx_p[3:0];
  assign reset_1 = reset;
  assign sfp_m_axis_tdata[255:0] = axis_data_fifo_4_M_AXIS_TDATA;
  assign sfp_m_axis_tkeep[31:0] = axis_data_fifo_4_M_AXIS_TKEEP;
  assign sfp_m_axis_tlast = axis_data_fifo_4_M_AXIS_TLAST;
  assign sfp_m_axis_tuser[0] = axis_data_fifo_4_M_AXIS_TUSER;
  assign sfp_m_axis_tvalid = axis_data_fifo_4_M_AXIS_TVALID;
  assign sfp_s_axis_tready = S_AXIS_0_1_TREADY;
  assign stat_rx_status_sfp0 = l_ethernet_0_stat_rx_status_0;
  assign stat_rx_status_sfp1 = l_ethernet_1_stat_rx_status_0;
  design_qsfp_40G_axis_data_fifo_0_0 axis_data_fifo_0
       (.m_axis_aclk(xxv_ethernet_0_tx_clk_out_0),
        .m_axis_tdata(axis_data_fifo_0_M_AXIS_TDATA),
        .m_axis_tkeep(axis_data_fifo_0_M_AXIS_TKEEP),
        .m_axis_tlast(axis_data_fifo_0_M_AXIS_TLAST),
        .m_axis_tready(axis_data_fifo_0_M_AXIS_TREADY),
        .m_axis_tuser(axis_data_fifo_0_M_AXIS_TUSER),
        .m_axis_tvalid(axis_data_fifo_0_M_AXIS_TVALID),
        .s_axis_aclk(l_ethernet_0_tx_clk_out_0),
        .s_axis_aresetn(proc_sys_reset_1_peripheral_aresetn),
        .s_axis_tdata(S_AXIS_0_1_TDATA),
        .s_axis_tkeep(S_AXIS_0_1_TKEEP),
        .s_axis_tlast(S_AXIS_0_1_TLAST),
        .s_axis_tready(S_AXIS_0_1_TREADY),
        .s_axis_tuser(S_AXIS_0_1_TUSER),
        .s_axis_tvalid(S_AXIS_0_1_TVALID));
  design_qsfp_40G_axis_data_fifo_4_0 axis_data_fifo_4
       (.m_axis_aclk(l_ethernet_0_tx_clk_out_0),
        .m_axis_tdata(axis_data_fifo_4_M_AXIS_TDATA),
        .m_axis_tkeep(axis_data_fifo_4_M_AXIS_TKEEP),
        .m_axis_tlast(axis_data_fifo_4_M_AXIS_TLAST),
        .m_axis_tready(axis_data_fifo_4_M_AXIS_TREADY),
        .m_axis_tuser(axis_data_fifo_4_M_AXIS_TUSER),
        .m_axis_tvalid(axis_data_fifo_4_M_AXIS_TVALID),
        .s_axis_aclk(l_ethernet_1_rx_clk_out_0),
        .s_axis_aresetn(proc_sys_reset_0_peripheral_aresetn),
        .s_axis_tdata(recv_adjustpacket_0_s_axis_TDATA),
        .s_axis_tkeep(recv_adjustpacket_0_s_axis_TKEEP),
        .s_axis_tlast(recv_adjustpacket_0_s_axis_TLAST),
        .s_axis_tready(recv_adjustpacket_0_s_axis_TREADY),
        .s_axis_tuser(recv_adjustpacket_0_s_axis_TUSER),
        .s_axis_tvalid(recv_adjustpacket_0_s_axis_TVALID));
  design_qsfp_40G_forward_0_0 forward_0
       (.clk(xxv_ethernet_0_tx_clk_out_0),
        .m_axis_tdata(axis_data_fifo_0_M_AXIS_TDATA),
        .m_axis_tkeep(axis_data_fifo_0_M_AXIS_TKEEP),
        .m_axis_tlast(axis_data_fifo_0_M_AXIS_TLAST),
        .m_axis_tready(axis_data_fifo_0_M_AXIS_TREADY),
        .m_axis_tuser(axis_data_fifo_0_M_AXIS_TUSER),
        .m_axis_tvalid(axis_data_fifo_0_M_AXIS_TVALID),
        .s_axis_tdata(forward_0_s_axis_TDATA),
        .s_axis_tkeep(forward_0_s_axis_TKEEP),
        .s_axis_tlast(forward_0_s_axis_TLAST),
        .s_axis_tready(forward_0_s_axis_TREADY),
        .s_axis_tuser(forward_0_s_axis_TUSER),
        .s_axis_tvalid(forward_0_s_axis_TVALID));
  design_qsfp_40G_l_ethernet_0_0 l_ethernet_0
       (.ctl_rx_check_preamble_0(xlconstant_6_dout),
        .ctl_rx_check_sfd_0(xlconstant_6_dout),
        .ctl_rx_custom_preamble_enable_0(xlconstant_5_dout),
        .ctl_rx_delete_fcs_0(xlconstant_6_dout),
        .ctl_rx_enable_0(xlconstant_6_dout),
        .ctl_rx_force_resync_0(xlconstant_5_dout),
        .ctl_rx_ignore_fcs_0(xlconstant_5_dout),
        .ctl_rx_max_packet_len_0(xlconstant_9_dout),
        .ctl_rx_min_packet_len_0(xlconstant_10_dout),
        .ctl_rx_process_lfi_0(xlconstant_5_dout),
        .ctl_rx_test_pattern_0(xlconstant_5_dout),
        .ctl_tx_custom_preamble_enable_0(xlconstant_5_dout),
        .ctl_tx_enable_0(xlconstant_6_dout),
        .ctl_tx_fcs_ins_enable_0(xlconstant_6_dout),
        .ctl_tx_ignore_fcs_0(xlconstant_5_dout),
        .ctl_tx_ipg_value_0(xlconstant_7_dout),
        .ctl_tx_send_idle_0(xlconstant_5_dout),
        .ctl_tx_send_lfi_0(xlconstant_5_dout),
        .ctl_tx_send_rfi_0(xlconstant_5_dout),
        .ctl_tx_test_pattern_0(xlconstant_5_dout),
        .dclk(clk_wiz_clk_out1),
        .gt_loopback_in_0(xlconstant_2_dout),
        .gt_refclk_n(gt_ref_clk_1_1_CLK_N),
        .gt_refclk_p(gt_ref_clk_1_1_CLK_P),
        .gt_rxn_in(l_ethernet_0_gt_serial_port_GRX_N),
        .gt_rxp_in(l_ethernet_0_gt_serial_port_GRX_P),
        .gt_txn_out(l_ethernet_0_gt_serial_port_GTX_N),
        .gt_txp_out(l_ethernet_0_gt_serial_port_GTX_P),
        .gtwiz_reset_rx_datapath_0(xlconstant_0_dout),
        .gtwiz_reset_tx_datapath_0(xlconstant_0_dout),
        .rx_clk_out_0(l_ethernet_0_rx_clk_out_0),
        .rx_core_clk_0(l_ethernet_0_rx_clk_out_0),
        .rx_reset_0(xlconstant_0_dout),
        .rxoutclksel_in_0(xlconstant_11_dout),
        .stat_rx_status_0(l_ethernet_0_stat_rx_status_0),
        .sys_reset(rst_clk_wiz_100M_peripheral_reset),
        .tx_axis_tdata_0(forward_0_s_axis_TDATA),
        .tx_axis_tkeep_0(forward_0_s_axis_TKEEP),
        .tx_axis_tlast_0(forward_0_s_axis_TLAST),
        .tx_axis_tready_0(forward_0_s_axis_TREADY),
        .tx_axis_tuser_0(forward_0_s_axis_TUSER),
        .tx_axis_tvalid_0(forward_0_s_axis_TVALID),
        .tx_clk_out_0(xxv_ethernet_0_tx_clk_out_0),
        .tx_preamblein_0(xlconstant_4_dout),
        .tx_reset_0(xlconstant_0_dout),
        .txoutclksel_in_0(xlconstant_11_dout));
  design_qsfp_40G_l_ethernet_0_1 l_ethernet_1
       (.ctl_rx_check_preamble_0(xlconstant_6_dout),
        .ctl_rx_check_sfd_0(xlconstant_6_dout),
        .ctl_rx_custom_preamble_enable_0(xlconstant_5_dout),
        .ctl_rx_delete_fcs_0(xlconstant_6_dout),
        .ctl_rx_enable_0(xlconstant_6_dout),
        .ctl_rx_force_resync_0(xlconstant_5_dout),
        .ctl_rx_ignore_fcs_0(xlconstant_5_dout),
        .ctl_rx_max_packet_len_0(xlconstant_9_dout),
        .ctl_rx_min_packet_len_0(xlconstant_10_dout),
        .ctl_rx_process_lfi_0(xlconstant_5_dout),
        .ctl_rx_test_pattern_0(xlconstant_5_dout),
        .ctl_tx_custom_preamble_enable_0(xlconstant_5_dout),
        .ctl_tx_enable_0(xlconstant_6_dout),
        .ctl_tx_fcs_ins_enable_0(xlconstant_6_dout),
        .ctl_tx_ignore_fcs_0(xlconstant_5_dout),
        .ctl_tx_ipg_value_0(xlconstant_7_dout),
        .ctl_tx_send_idle_0(xlconstant_5_dout),
        .ctl_tx_send_lfi_0(xlconstant_5_dout),
        .ctl_tx_send_rfi_0(xlconstant_5_dout),
        .ctl_tx_test_pattern_0(xlconstant_5_dout),
        .dclk(clk_wiz_clk_out1),
        .gt_loopback_in_0(xlconstant_2_dout),
        .gt_refclk_n(gt_ref_clk_0_1_CLK_N),
        .gt_refclk_p(gt_ref_clk_0_1_CLK_P),
        .gt_rxn_in(l_ethernet_1_gt_serial_port_GRX_N),
        .gt_rxp_in(l_ethernet_1_gt_serial_port_GRX_P),
        .gt_txn_out(l_ethernet_1_gt_serial_port_GTX_N),
        .gt_txp_out(l_ethernet_1_gt_serial_port_GTX_P),
        .gtwiz_reset_rx_datapath_0(xlconstant_0_dout),
        .gtwiz_reset_tx_datapath_0(xlconstant_0_dout),
        .rx_axis_tdata_0(l_ethernet_1_axis_rx_0_TDATA),
        .rx_axis_tkeep_0(l_ethernet_1_axis_rx_0_TKEEP),
        .rx_axis_tlast_0(l_ethernet_1_axis_rx_0_TLAST),
        .rx_axis_tuser_0(l_ethernet_1_axis_rx_0_TUSER),
        .rx_axis_tvalid_0(l_ethernet_1_axis_rx_0_TVALID),
        .rx_clk_out_0(l_ethernet_1_rx_clk_out_0),
        .rx_core_clk_0(l_ethernet_1_rx_clk_out_0),
        .rx_reset_0(xlconstant_0_dout),
        .rxoutclksel_in_0(xlconstant_11_dout),
        .stat_rx_status_0(l_ethernet_1_stat_rx_status_0),
        .sys_reset(rst_clk_wiz_100M_peripheral_reset),
        .tx_axis_tdata_0({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .tx_axis_tkeep_0({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .tx_axis_tlast_0(1'b0),
        .tx_axis_tuser_0(1'b0),
        .tx_axis_tvalid_0(1'b0),
        .tx_preamblein_0(xlconstant_4_dout),
        .tx_reset_0(xlconstant_0_dout),
        .txoutclksel_in_0(xlconstant_11_dout),
        .user_rx_reset_0(l_ethernet_1_user_rx_reset_0));
  design_qsfp_40G_proc_sys_reset_0_0 proc_sys_reset_0
       (.aux_reset_in(1'b1),
        .dcm_locked(1'b1),
        .ext_reset_in(l_ethernet_1_user_rx_reset_0),
        .mb_debug_sys_rst(1'b0),
        .peripheral_aresetn(proc_sys_reset_0_peripheral_aresetn),
        .slowest_sync_clk(l_ethernet_1_rx_clk_out_0));
  design_qsfp_40G_proc_sys_reset_1_0 proc_sys_reset_1
       (.aux_reset_in(1'b1),
        .dcm_locked(1'b1),
        .ext_reset_in(reset_1),
        .mb_debug_sys_rst(1'b0),
        .peripheral_aresetn(proc_sys_reset_1_peripheral_aresetn),
        .slowest_sync_clk(l_ethernet_0_tx_clk_out_0));
  design_qsfp_40G_recv_adjustpacket_0_0 recv_adjustpacket_0
       (.clk(l_ethernet_1_rx_clk_out_0),
        .m_axis_tdata(l_ethernet_1_axis_rx_0_TDATA),
        .m_axis_tkeep(l_ethernet_1_axis_rx_0_TKEEP),
        .m_axis_tlast(l_ethernet_1_axis_rx_0_TLAST),
        .m_axis_tuser(l_ethernet_1_axis_rx_0_TUSER),
        .m_axis_tvalid(l_ethernet_1_axis_rx_0_TVALID),
        .s_axis_tdata(recv_adjustpacket_0_s_axis_TDATA),
        .s_axis_tkeep(recv_adjustpacket_0_s_axis_TKEEP),
        .s_axis_tlast(recv_adjustpacket_0_s_axis_TLAST),
        .s_axis_tready(recv_adjustpacket_0_s_axis_TREADY),
        .s_axis_tuser(recv_adjustpacket_0_s_axis_TUSER),
        .s_axis_tvalid(recv_adjustpacket_0_s_axis_TVALID));
  design_qsfp_40G_rst_clk_wiz_100M_0 rst_clk_wiz_100M
       (.aux_reset_in(1'b1),
        .dcm_locked(1'b1),
        .ext_reset_in(reset_1),
        .mb_debug_sys_rst(1'b0),
        .peripheral_reset(rst_clk_wiz_100M_peripheral_reset),
        .slowest_sync_clk(clk_wiz_clk_out1));
  design_qsfp_40G_xlconstant_0_0 xlconstant_0
       (.dout(xlconstant_0_dout));
  design_qsfp_40G_xlconstant_1_0 xlconstant_1
       ();
  design_qsfp_40G_xlconstant_10_0 xlconstant_10
       (.dout(xlconstant_10_dout));
  design_qsfp_40G_xlconstant_11_0 xlconstant_11
       (.dout(xlconstant_11_dout));
  design_qsfp_40G_xlconstant_12_0 xlconstant_12
       ();
  design_qsfp_40G_xlconstant_13_0 xlconstant_13
       ();
  design_qsfp_40G_xlconstant_14_0 xlconstant_14
       ();
  design_qsfp_40G_xlconstant_15_0 xlconstant_15
       ();
  design_qsfp_40G_xlconstant_16_0 xlconstant_16
       ();
  design_qsfp_40G_xlconstant_17_0 xlconstant_17
       ();
  design_qsfp_40G_xlconstant_18_0 xlconstant_18
       ();
  design_qsfp_40G_xlconstant_19_0 xlconstant_19
       ();
  design_qsfp_40G_xlconstant_2_0 xlconstant_2
       (.dout(xlconstant_2_dout));
  design_qsfp_40G_xlconstant_20_0 xlconstant_20
       ();
  design_qsfp_40G_xlconstant_3_0 xlconstant_3
       ();
  design_qsfp_40G_xlconstant_4_0 xlconstant_4
       (.dout(xlconstant_4_dout));
  design_qsfp_40G_xlconstant_5_0 xlconstant_5
       (.dout(xlconstant_5_dout));
  design_qsfp_40G_xlconstant_6_0 xlconstant_6
       (.dout(xlconstant_6_dout));
  design_qsfp_40G_xlconstant_7_0 xlconstant_7
       (.dout(xlconstant_7_dout));
  design_qsfp_40G_xlconstant_8_0 xlconstant_8
       ();
  design_qsfp_40G_xlconstant_9_0 xlconstant_9
       (.dout(xlconstant_9_dout));
endmodule
