-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
-- Date        : Tue Dec  6 16:16:38 2022
-- Host        : dell-Vostro-3910-China-HDD-Protection running 64-bit Ubuntu 22.04.1 LTS
-- Command     : write_vhdl -force -mode synth_stub
--               /media/dell/workspace/Xilinx_projects/ringallreduced/ringallreduced.gen/sources_1/bd/design_qsfp_40G/ip/design_qsfp_40G_forward_0_0/design_qsfp_40G_forward_0_0_stub.vhdl
-- Design      : design_qsfp_40G_forward_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xcvu37p-fsvh2892-2L-e
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity design_qsfp_40G_forward_0_0 is
  Port ( 
    m_axis_tready : out STD_LOGIC;
    m_axis_tvalid : in STD_LOGIC;
    m_axis_tdata : in STD_LOGIC_VECTOR ( 255 downto 0 );
    m_axis_tkeep : in STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axis_tlast : in STD_LOGIC;
    m_axis_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_tready : in STD_LOGIC;
    s_axis_tvalid : out STD_LOGIC;
    s_axis_tdata : out STD_LOGIC_VECTOR ( 255 downto 0 );
    s_axis_tkeep : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axis_tlast : out STD_LOGIC;
    s_axis_tuser : out STD_LOGIC_VECTOR ( 0 to 0 );
    clk : in STD_LOGIC
  );

end design_qsfp_40G_forward_0_0;

architecture stub of design_qsfp_40G_forward_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "m_axis_tready,m_axis_tvalid,m_axis_tdata[255:0],m_axis_tkeep[31:0],m_axis_tlast,m_axis_tuser[0:0],s_axis_tready,s_axis_tvalid,s_axis_tdata[255:0],s_axis_tkeep[31:0],s_axis_tlast,s_axis_tuser[0:0],clk";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "forward,Vivado 2022.2";
begin
end;
