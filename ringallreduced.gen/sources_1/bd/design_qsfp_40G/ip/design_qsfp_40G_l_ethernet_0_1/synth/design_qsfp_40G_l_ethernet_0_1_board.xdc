#--------------------Physical Constraints-----------------

set_property BOARD_PIN {qsfp2_156mhz_p} [get_ports gt_refclk_p]
set_property BOARD_PIN {qsfp2_156mhz_n} [get_ports gt_refclk_n]
