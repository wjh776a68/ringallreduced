//Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
//Date        : Tue Dec  6 08:23:05 2022
//Host        : dell-Vostro-3910-China-HDD-Protection running 64-bit Ubuntu 22.04.1 LTS
//Command     : generate_target design_ringallreduce_wrapper.bd
//Design      : design_ringallreduce_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_ringallreduce_wrapper
   (CLK_IN1_D_0_clk_n,
    CLK_IN1_D_0_clk_p,
    qsfp1_156mhz_clk_n,
    qsfp1_156mhz_clk_p,
    qsfp1_4x_grx_n,
    qsfp1_4x_grx_p,
    qsfp1_4x_gtx_n,
    qsfp1_4x_gtx_p,
    reset);
  input CLK_IN1_D_0_clk_n;
  input CLK_IN1_D_0_clk_p;
  input qsfp1_156mhz_clk_n;
  input qsfp1_156mhz_clk_p;
  input [3:0]qsfp1_4x_grx_n;
  input [3:0]qsfp1_4x_grx_p;
  output [3:0]qsfp1_4x_gtx_n;
  output [3:0]qsfp1_4x_gtx_p;
  input reset;

  wire CLK_IN1_D_0_clk_n;
  wire CLK_IN1_D_0_clk_p;
  wire qsfp1_156mhz_clk_n;
  wire qsfp1_156mhz_clk_p;
  wire [3:0]qsfp1_4x_grx_n;
  wire [3:0]qsfp1_4x_grx_p;
  wire [3:0]qsfp1_4x_gtx_n;
  wire [3:0]qsfp1_4x_gtx_p;
  wire reset;

  design_ringallreduce design_ringallreduce_i
       (.CLK_IN1_D_0_clk_n(CLK_IN1_D_0_clk_n),
        .CLK_IN1_D_0_clk_p(CLK_IN1_D_0_clk_p),
        .qsfp1_156mhz_clk_n(qsfp1_156mhz_clk_n),
        .qsfp1_156mhz_clk_p(qsfp1_156mhz_clk_p),
        .qsfp1_4x_grx_n(qsfp1_4x_grx_n),
        .qsfp1_4x_grx_p(qsfp1_4x_grx_p),
        .qsfp1_4x_gtx_n(qsfp1_4x_gtx_n),
        .qsfp1_4x_gtx_p(qsfp1_4x_gtx_p),
        .reset(reset));
endmodule
