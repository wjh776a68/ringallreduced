//Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
//Date        : Tue Dec  6 08:22:37 2022
//Host        : dell-Vostro-3910-China-HDD-Protection running 64-bit Ubuntu 22.04.1 LTS
//Command     : generate_target design_qsfp_25G_wrapper.bd
//Design      : design_qsfp_25G_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_qsfp_25G_wrapper
   (CLK_IN1_D_clk_n,
    CLK_IN1_D_clk_p,
    clk,
    gt_ref_clk_clk_n,
    gt_ref_clk_clk_p,
    gt_serial_port_grx_n,
    gt_serial_port_grx_p,
    gt_serial_port_gtx_n,
    gt_serial_port_gtx_p,
    reset,
    sfp_m_axis_tdata,
    sfp_m_axis_tkeep,
    sfp_m_axis_tlast,
    sfp_m_axis_tready,
    sfp_m_axis_tuser,
    sfp_m_axis_tvalid,
    sfp_s_axis_tdata,
    sfp_s_axis_tkeep,
    sfp_s_axis_tlast,
    sfp_s_axis_tready,
    sfp_s_axis_tuser,
    sfp_s_axis_tvalid,
    stat_rx_status_sfp0,
    stat_rx_status_sfp1);
  input CLK_IN1_D_clk_n;
  input CLK_IN1_D_clk_p;
  input clk;
  input gt_ref_clk_clk_n;
  input gt_ref_clk_clk_p;
  input [1:0]gt_serial_port_grx_n;
  input [1:0]gt_serial_port_grx_p;
  output [1:0]gt_serial_port_gtx_n;
  output [1:0]gt_serial_port_gtx_p;
  input reset;
  output [63:0]sfp_m_axis_tdata;
  output [7:0]sfp_m_axis_tkeep;
  output sfp_m_axis_tlast;
  input sfp_m_axis_tready;
  output [0:0]sfp_m_axis_tuser;
  output sfp_m_axis_tvalid;
  input [63:0]sfp_s_axis_tdata;
  input [7:0]sfp_s_axis_tkeep;
  input sfp_s_axis_tlast;
  output sfp_s_axis_tready;
  input [0:0]sfp_s_axis_tuser;
  input sfp_s_axis_tvalid;
  output stat_rx_status_sfp0;
  output stat_rx_status_sfp1;

  wire CLK_IN1_D_clk_n;
  wire CLK_IN1_D_clk_p;
  wire clk;
  wire gt_ref_clk_clk_n;
  wire gt_ref_clk_clk_p;
  wire [1:0]gt_serial_port_grx_n;
  wire [1:0]gt_serial_port_grx_p;
  wire [1:0]gt_serial_port_gtx_n;
  wire [1:0]gt_serial_port_gtx_p;
  wire reset;
  wire [63:0]sfp_m_axis_tdata;
  wire [7:0]sfp_m_axis_tkeep;
  wire sfp_m_axis_tlast;
  wire sfp_m_axis_tready;
  wire [0:0]sfp_m_axis_tuser;
  wire sfp_m_axis_tvalid;
  wire [63:0]sfp_s_axis_tdata;
  wire [7:0]sfp_s_axis_tkeep;
  wire sfp_s_axis_tlast;
  wire sfp_s_axis_tready;
  wire [0:0]sfp_s_axis_tuser;
  wire sfp_s_axis_tvalid;
  wire stat_rx_status_sfp0;
  wire stat_rx_status_sfp1;

  design_qsfp_25G design_qsfp_25G_i
       (.CLK_IN1_D_clk_n(CLK_IN1_D_clk_n),
        .CLK_IN1_D_clk_p(CLK_IN1_D_clk_p),
        .clk(clk),
        .gt_ref_clk_clk_n(gt_ref_clk_clk_n),
        .gt_ref_clk_clk_p(gt_ref_clk_clk_p),
        .gt_serial_port_grx_n(gt_serial_port_grx_n),
        .gt_serial_port_grx_p(gt_serial_port_grx_p),
        .gt_serial_port_gtx_n(gt_serial_port_gtx_n),
        .gt_serial_port_gtx_p(gt_serial_port_gtx_p),
        .reset(reset),
        .sfp_m_axis_tdata(sfp_m_axis_tdata),
        .sfp_m_axis_tkeep(sfp_m_axis_tkeep),
        .sfp_m_axis_tlast(sfp_m_axis_tlast),
        .sfp_m_axis_tready(sfp_m_axis_tready),
        .sfp_m_axis_tuser(sfp_m_axis_tuser),
        .sfp_m_axis_tvalid(sfp_m_axis_tvalid),
        .sfp_s_axis_tdata(sfp_s_axis_tdata),
        .sfp_s_axis_tkeep(sfp_s_axis_tkeep),
        .sfp_s_axis_tlast(sfp_s_axis_tlast),
        .sfp_s_axis_tready(sfp_s_axis_tready),
        .sfp_s_axis_tuser(sfp_s_axis_tuser),
        .sfp_s_axis_tvalid(sfp_s_axis_tvalid),
        .stat_rx_status_sfp0(stat_rx_status_sfp0),
        .stat_rx_status_sfp1(stat_rx_status_sfp1));
endmodule
