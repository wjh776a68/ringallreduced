# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "STATUS_0" -parent ${Page_0}
  ipgui::add_param $IPINST -name "STATUS_1" -parent ${Page_0}
  ipgui::add_param $IPINST -name "STATUS_10" -parent ${Page_0}
  ipgui::add_param $IPINST -name "STATUS_11" -parent ${Page_0}
  ipgui::add_param $IPINST -name "STATUS_12" -parent ${Page_0}
  ipgui::add_param $IPINST -name "STATUS_13" -parent ${Page_0}
  ipgui::add_param $IPINST -name "STATUS_2" -parent ${Page_0}
  ipgui::add_param $IPINST -name "STATUS_3" -parent ${Page_0}
  ipgui::add_param $IPINST -name "STATUS_4" -parent ${Page_0}
  ipgui::add_param $IPINST -name "STATUS_5" -parent ${Page_0}
  ipgui::add_param $IPINST -name "STATUS_6" -parent ${Page_0}
  ipgui::add_param $IPINST -name "STATUS_7" -parent ${Page_0}
  ipgui::add_param $IPINST -name "STATUS_8" -parent ${Page_0}
  ipgui::add_param $IPINST -name "STATUS_9" -parent ${Page_0}


}

proc update_PARAM_VALUE.STATUS_0 { PARAM_VALUE.STATUS_0 } {
	# Procedure called to update STATUS_0 when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.STATUS_0 { PARAM_VALUE.STATUS_0 } {
	# Procedure called to validate STATUS_0
	return true
}

proc update_PARAM_VALUE.STATUS_1 { PARAM_VALUE.STATUS_1 } {
	# Procedure called to update STATUS_1 when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.STATUS_1 { PARAM_VALUE.STATUS_1 } {
	# Procedure called to validate STATUS_1
	return true
}

proc update_PARAM_VALUE.STATUS_10 { PARAM_VALUE.STATUS_10 } {
	# Procedure called to update STATUS_10 when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.STATUS_10 { PARAM_VALUE.STATUS_10 } {
	# Procedure called to validate STATUS_10
	return true
}

proc update_PARAM_VALUE.STATUS_11 { PARAM_VALUE.STATUS_11 } {
	# Procedure called to update STATUS_11 when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.STATUS_11 { PARAM_VALUE.STATUS_11 } {
	# Procedure called to validate STATUS_11
	return true
}

proc update_PARAM_VALUE.STATUS_12 { PARAM_VALUE.STATUS_12 } {
	# Procedure called to update STATUS_12 when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.STATUS_12 { PARAM_VALUE.STATUS_12 } {
	# Procedure called to validate STATUS_12
	return true
}

proc update_PARAM_VALUE.STATUS_13 { PARAM_VALUE.STATUS_13 } {
	# Procedure called to update STATUS_13 when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.STATUS_13 { PARAM_VALUE.STATUS_13 } {
	# Procedure called to validate STATUS_13
	return true
}

proc update_PARAM_VALUE.STATUS_2 { PARAM_VALUE.STATUS_2 } {
	# Procedure called to update STATUS_2 when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.STATUS_2 { PARAM_VALUE.STATUS_2 } {
	# Procedure called to validate STATUS_2
	return true
}

proc update_PARAM_VALUE.STATUS_3 { PARAM_VALUE.STATUS_3 } {
	# Procedure called to update STATUS_3 when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.STATUS_3 { PARAM_VALUE.STATUS_3 } {
	# Procedure called to validate STATUS_3
	return true
}

proc update_PARAM_VALUE.STATUS_4 { PARAM_VALUE.STATUS_4 } {
	# Procedure called to update STATUS_4 when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.STATUS_4 { PARAM_VALUE.STATUS_4 } {
	# Procedure called to validate STATUS_4
	return true
}

proc update_PARAM_VALUE.STATUS_5 { PARAM_VALUE.STATUS_5 } {
	# Procedure called to update STATUS_5 when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.STATUS_5 { PARAM_VALUE.STATUS_5 } {
	# Procedure called to validate STATUS_5
	return true
}

proc update_PARAM_VALUE.STATUS_6 { PARAM_VALUE.STATUS_6 } {
	# Procedure called to update STATUS_6 when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.STATUS_6 { PARAM_VALUE.STATUS_6 } {
	# Procedure called to validate STATUS_6
	return true
}

proc update_PARAM_VALUE.STATUS_7 { PARAM_VALUE.STATUS_7 } {
	# Procedure called to update STATUS_7 when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.STATUS_7 { PARAM_VALUE.STATUS_7 } {
	# Procedure called to validate STATUS_7
	return true
}

proc update_PARAM_VALUE.STATUS_8 { PARAM_VALUE.STATUS_8 } {
	# Procedure called to update STATUS_8 when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.STATUS_8 { PARAM_VALUE.STATUS_8 } {
	# Procedure called to validate STATUS_8
	return true
}

proc update_PARAM_VALUE.STATUS_9 { PARAM_VALUE.STATUS_9 } {
	# Procedure called to update STATUS_9 when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.STATUS_9 { PARAM_VALUE.STATUS_9 } {
	# Procedure called to validate STATUS_9
	return true
}


proc update_MODELPARAM_VALUE.STATUS_0 { MODELPARAM_VALUE.STATUS_0 PARAM_VALUE.STATUS_0 } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.STATUS_0}] ${MODELPARAM_VALUE.STATUS_0}
}

proc update_MODELPARAM_VALUE.STATUS_1 { MODELPARAM_VALUE.STATUS_1 PARAM_VALUE.STATUS_1 } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.STATUS_1}] ${MODELPARAM_VALUE.STATUS_1}
}

proc update_MODELPARAM_VALUE.STATUS_2 { MODELPARAM_VALUE.STATUS_2 PARAM_VALUE.STATUS_2 } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.STATUS_2}] ${MODELPARAM_VALUE.STATUS_2}
}

proc update_MODELPARAM_VALUE.STATUS_3 { MODELPARAM_VALUE.STATUS_3 PARAM_VALUE.STATUS_3 } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.STATUS_3}] ${MODELPARAM_VALUE.STATUS_3}
}

proc update_MODELPARAM_VALUE.STATUS_4 { MODELPARAM_VALUE.STATUS_4 PARAM_VALUE.STATUS_4 } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.STATUS_4}] ${MODELPARAM_VALUE.STATUS_4}
}

proc update_MODELPARAM_VALUE.STATUS_5 { MODELPARAM_VALUE.STATUS_5 PARAM_VALUE.STATUS_5 } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.STATUS_5}] ${MODELPARAM_VALUE.STATUS_5}
}

proc update_MODELPARAM_VALUE.STATUS_6 { MODELPARAM_VALUE.STATUS_6 PARAM_VALUE.STATUS_6 } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.STATUS_6}] ${MODELPARAM_VALUE.STATUS_6}
}

proc update_MODELPARAM_VALUE.STATUS_7 { MODELPARAM_VALUE.STATUS_7 PARAM_VALUE.STATUS_7 } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.STATUS_7}] ${MODELPARAM_VALUE.STATUS_7}
}

proc update_MODELPARAM_VALUE.STATUS_8 { MODELPARAM_VALUE.STATUS_8 PARAM_VALUE.STATUS_8 } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.STATUS_8}] ${MODELPARAM_VALUE.STATUS_8}
}

proc update_MODELPARAM_VALUE.STATUS_9 { MODELPARAM_VALUE.STATUS_9 PARAM_VALUE.STATUS_9 } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.STATUS_9}] ${MODELPARAM_VALUE.STATUS_9}
}

proc update_MODELPARAM_VALUE.STATUS_10 { MODELPARAM_VALUE.STATUS_10 PARAM_VALUE.STATUS_10 } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.STATUS_10}] ${MODELPARAM_VALUE.STATUS_10}
}

proc update_MODELPARAM_VALUE.STATUS_11 { MODELPARAM_VALUE.STATUS_11 PARAM_VALUE.STATUS_11 } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.STATUS_11}] ${MODELPARAM_VALUE.STATUS_11}
}

proc update_MODELPARAM_VALUE.STATUS_12 { MODELPARAM_VALUE.STATUS_12 PARAM_VALUE.STATUS_12 } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.STATUS_12}] ${MODELPARAM_VALUE.STATUS_12}
}

proc update_MODELPARAM_VALUE.STATUS_13 { MODELPARAM_VALUE.STATUS_13 PARAM_VALUE.STATUS_13 } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.STATUS_13}] ${MODELPARAM_VALUE.STATUS_13}
}

