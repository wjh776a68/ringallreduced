# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "KEEP_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "PACKET_SIZE_MAX" -parent ${Page_0}
  ipgui::add_param $IPINST -name "PACKET_SIZE_MIN" -parent ${Page_0}


}

proc update_PARAM_VALUE.DATA_WIDTH { PARAM_VALUE.DATA_WIDTH } {
	# Procedure called to update DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DATA_WIDTH { PARAM_VALUE.DATA_WIDTH } {
	# Procedure called to validate DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.KEEP_WIDTH { PARAM_VALUE.KEEP_WIDTH } {
	# Procedure called to update KEEP_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.KEEP_WIDTH { PARAM_VALUE.KEEP_WIDTH } {
	# Procedure called to validate KEEP_WIDTH
	return true
}

proc update_PARAM_VALUE.PACKET_SIZE_MAX { PARAM_VALUE.PACKET_SIZE_MAX } {
	# Procedure called to update PACKET_SIZE_MAX when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.PACKET_SIZE_MAX { PARAM_VALUE.PACKET_SIZE_MAX } {
	# Procedure called to validate PACKET_SIZE_MAX
	return true
}

proc update_PARAM_VALUE.PACKET_SIZE_MIN { PARAM_VALUE.PACKET_SIZE_MIN } {
	# Procedure called to update PACKET_SIZE_MIN when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.PACKET_SIZE_MIN { PARAM_VALUE.PACKET_SIZE_MIN } {
	# Procedure called to validate PACKET_SIZE_MIN
	return true
}


proc update_MODELPARAM_VALUE.DATA_WIDTH { MODELPARAM_VALUE.DATA_WIDTH PARAM_VALUE.DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DATA_WIDTH}] ${MODELPARAM_VALUE.DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.KEEP_WIDTH { MODELPARAM_VALUE.KEEP_WIDTH PARAM_VALUE.KEEP_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.KEEP_WIDTH}] ${MODELPARAM_VALUE.KEEP_WIDTH}
}

proc update_MODELPARAM_VALUE.PACKET_SIZE_MIN { MODELPARAM_VALUE.PACKET_SIZE_MIN PARAM_VALUE.PACKET_SIZE_MIN } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.PACKET_SIZE_MIN}] ${MODELPARAM_VALUE.PACKET_SIZE_MIN}
}

proc update_MODELPARAM_VALUE.PACKET_SIZE_MAX { MODELPARAM_VALUE.PACKET_SIZE_MAX PARAM_VALUE.PACKET_SIZE_MAX } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.PACKET_SIZE_MAX}] ${MODELPARAM_VALUE.PACKET_SIZE_MAX}
}

