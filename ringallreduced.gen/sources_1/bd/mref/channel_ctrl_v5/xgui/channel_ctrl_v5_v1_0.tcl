# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "BASE_ADDRESS" -parent ${Page_0}
  ipgui::add_param $IPINST -name "HIGH_ADDRESS" -parent ${Page_0}


}

proc update_PARAM_VALUE.BASE_ADDRESS { PARAM_VALUE.BASE_ADDRESS } {
	# Procedure called to update BASE_ADDRESS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.BASE_ADDRESS { PARAM_VALUE.BASE_ADDRESS } {
	# Procedure called to validate BASE_ADDRESS
	return true
}

proc update_PARAM_VALUE.HIGH_ADDRESS { PARAM_VALUE.HIGH_ADDRESS } {
	# Procedure called to update HIGH_ADDRESS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.HIGH_ADDRESS { PARAM_VALUE.HIGH_ADDRESS } {
	# Procedure called to validate HIGH_ADDRESS
	return true
}


proc update_MODELPARAM_VALUE.BASE_ADDRESS { MODELPARAM_VALUE.BASE_ADDRESS PARAM_VALUE.BASE_ADDRESS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.BASE_ADDRESS}] ${MODELPARAM_VALUE.BASE_ADDRESS}
}

proc update_MODELPARAM_VALUE.HIGH_ADDRESS { MODELPARAM_VALUE.HIGH_ADDRESS PARAM_VALUE.HIGH_ADDRESS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.HIGH_ADDRESS}] ${MODELPARAM_VALUE.HIGH_ADDRESS}
}

