// (c) Copyright 1995-2022 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.


// IP VLNV: xilinx.com:module_ref:design_hbm_channel_wrapper:1.0
// IP Revision: 1

(* X_CORE_INFO = "design_hbm_channel_wrapper,Vivado 2022.2" *)
(* CHECK_LICENSE_TYPE = "design_hbm_design_hbm_channel_w_0_1,design_hbm_channel_wrapper,{}" *)
(* CORE_GENERATION_INFO = "design_hbm_design_hbm_channel_w_0_1,design_hbm_channel_wrapper,{x_ipProduct=Vivado 2022.2,x_ipVendor=xilinx.com,x_ipLibrary=module_ref,x_ipName=design_hbm_channel_wrapper,x_ipVersion=1.0,x_ipCoreRevision=1,x_ipLanguage=VERILOG,x_ipSimLanguage=MIXED}" *)
(* IP_DEFINITION_SOURCE = "module_ref" *)
(* DowngradeIPIdentifiedWarnings = "yes" *)
module design_hbm_design_hbm_channel_w_0_1 (
  HBM_AXI_araddr,
  HBM_AXI_arburst,
  HBM_AXI_arid,
  HBM_AXI_arlen,
  HBM_AXI_arready,
  HBM_AXI_arsize,
  HBM_AXI_arvalid,
  HBM_AXI_awaddr,
  HBM_AXI_awburst,
  HBM_AXI_awid,
  HBM_AXI_awlen,
  HBM_AXI_awready,
  HBM_AXI_awsize,
  HBM_AXI_awvalid,
  HBM_AXI_bid,
  HBM_AXI_bready,
  HBM_AXI_bresp,
  HBM_AXI_bvalid,
  HBM_AXI_rdata,
  HBM_AXI_rid,
  HBM_AXI_rlast,
  HBM_AXI_rready,
  HBM_AXI_rresp,
  HBM_AXI_rvalid,
  HBM_AXI_wdata,
  HBM_AXI_wid,
  HBM_AXI_wlast,
  HBM_AXI_wready,
  HBM_AXI_wstrb,
  HBM_AXI_wvalid,
  HBM_READ_tdata,
  HBM_READ_tkeep,
  HBM_READ_tlast,
  HBM_READ_tready,
  HBM_READ_tvalid,
  HBM_WRITE_tdata,
  HBM_WRITE_tready,
  HBM_WRITE_tvalid,
  clk_312,
  clk_450,
  rd_running,
  user_hbm_channel_ctrl_ports_busy,
  user_hbm_channel_ctrl_ports_rd_blockamount,
  user_hbm_channel_ctrl_ports_rd_startaddress,
  user_hbm_channel_ctrl_ports_wr_blockamount,
  user_hbm_channel_ctrl_ports_wr_startaddress,
  wr_running
);

(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI ARADDR" *)
output wire [32 : 0] HBM_AXI_araddr;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI ARBURST" *)
output wire [1 : 0] HBM_AXI_arburst;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI ARID" *)
output wire [5 : 0] HBM_AXI_arid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI ARLEN" *)
output wire [3 : 0] HBM_AXI_arlen;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI ARREADY" *)
input wire HBM_AXI_arready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI ARSIZE" *)
output wire [2 : 0] HBM_AXI_arsize;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI ARVALID" *)
output wire HBM_AXI_arvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI AWADDR" *)
output wire [32 : 0] HBM_AXI_awaddr;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI AWBURST" *)
output wire [1 : 0] HBM_AXI_awburst;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI AWID" *)
output wire [5 : 0] HBM_AXI_awid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI AWLEN" *)
output wire [3 : 0] HBM_AXI_awlen;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI AWREADY" *)
input wire HBM_AXI_awready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI AWSIZE" *)
output wire [2 : 0] HBM_AXI_awsize;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI AWVALID" *)
output wire HBM_AXI_awvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI BID" *)
input wire [5 : 0] HBM_AXI_bid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI BREADY" *)
output wire HBM_AXI_bready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI BRESP" *)
input wire [1 : 0] HBM_AXI_bresp;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI BVALID" *)
input wire HBM_AXI_bvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI RDATA" *)
input wire [255 : 0] HBM_AXI_rdata;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI RID" *)
input wire [5 : 0] HBM_AXI_rid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI RLAST" *)
input wire HBM_AXI_rlast;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI RREADY" *)
output wire HBM_AXI_rready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI RRESP" *)
input wire [1 : 0] HBM_AXI_rresp;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI RVALID" *)
input wire HBM_AXI_rvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI WDATA" *)
output wire [255 : 0] HBM_AXI_wdata;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI WID" *)
output wire [5 : 0] HBM_AXI_wid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI WLAST" *)
output wire HBM_AXI_wlast;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI WREADY" *)
input wire HBM_AXI_wready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI WSTRB" *)
output wire [31 : 0] HBM_AXI_wstrb;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME HBM_AXI, DATA_WIDTH 256, PROTOCOL AXI3, FREQ_HZ 450000000, ID_WIDTH 6, ADDR_WIDTH 33, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 1, HAS_LOCK 0, HAS_PROT 0, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 16, PHASE 0.0, CLK_DOMAIN design_hbm_clk_wiz_1_0_clk_out1, NUM_READ_THREADS 1, NUM_WRI\
TE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI WVALID" *)
output wire HBM_AXI_wvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_READ TDATA" *)
output wire [255 : 0] HBM_READ_tdata;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_READ TKEEP" *)
output wire [31 : 0] HBM_READ_tkeep;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_READ TLAST" *)
output wire HBM_READ_tlast;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_READ TREADY" *)
input wire HBM_READ_tready;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME HBM_READ, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN design_hbm_clk_312MHz, LAYERED_METADATA undef, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_READ TVALID" *)
output wire HBM_READ_tvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_WRITE TDATA" *)
input wire [255 : 0] HBM_WRITE_tdata;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_WRITE TREADY" *)
output wire HBM_WRITE_tready;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME HBM_WRITE, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.0, LAYERED_METADATA undef, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_WRITE TVALID" *)
input wire HBM_WRITE_tvalid;
input wire clk_312;
input wire clk_450;
(* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 ctrl_ports rd_run" *)
input wire rd_running;
(* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 ctrl_ports busy" *)
output wire user_hbm_channel_ctrl_ports_busy;
(* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 ctrl_ports rd_blockamount" *)
input wire [31 : 0] user_hbm_channel_ctrl_ports_rd_blockamount;
(* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 ctrl_ports rd_startaddress" *)
input wire [31 : 0] user_hbm_channel_ctrl_ports_rd_startaddress;
(* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 ctrl_ports wr_blockamount" *)
input wire [31 : 0] user_hbm_channel_ctrl_ports_wr_blockamount;
(* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 ctrl_ports wr_startaddress" *)
input wire [31 : 0] user_hbm_channel_ctrl_ports_wr_startaddress;
(* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 ctrl_ports wr_run" *)
input wire wr_running;

  design_hbm_channel_wrapper inst (
    .HBM_AXI_araddr(HBM_AXI_araddr),
    .HBM_AXI_arburst(HBM_AXI_arburst),
    .HBM_AXI_arid(HBM_AXI_arid),
    .HBM_AXI_arlen(HBM_AXI_arlen),
    .HBM_AXI_arready(HBM_AXI_arready),
    .HBM_AXI_arsize(HBM_AXI_arsize),
    .HBM_AXI_arvalid(HBM_AXI_arvalid),
    .HBM_AXI_awaddr(HBM_AXI_awaddr),
    .HBM_AXI_awburst(HBM_AXI_awburst),
    .HBM_AXI_awid(HBM_AXI_awid),
    .HBM_AXI_awlen(HBM_AXI_awlen),
    .HBM_AXI_awready(HBM_AXI_awready),
    .HBM_AXI_awsize(HBM_AXI_awsize),
    .HBM_AXI_awvalid(HBM_AXI_awvalid),
    .HBM_AXI_bid(HBM_AXI_bid),
    .HBM_AXI_bready(HBM_AXI_bready),
    .HBM_AXI_bresp(HBM_AXI_bresp),
    .HBM_AXI_bvalid(HBM_AXI_bvalid),
    .HBM_AXI_rdata(HBM_AXI_rdata),
    .HBM_AXI_rid(HBM_AXI_rid),
    .HBM_AXI_rlast(HBM_AXI_rlast),
    .HBM_AXI_rready(HBM_AXI_rready),
    .HBM_AXI_rresp(HBM_AXI_rresp),
    .HBM_AXI_rvalid(HBM_AXI_rvalid),
    .HBM_AXI_wdata(HBM_AXI_wdata),
    .HBM_AXI_wid(HBM_AXI_wid),
    .HBM_AXI_wlast(HBM_AXI_wlast),
    .HBM_AXI_wready(HBM_AXI_wready),
    .HBM_AXI_wstrb(HBM_AXI_wstrb),
    .HBM_AXI_wvalid(HBM_AXI_wvalid),
    .HBM_READ_tdata(HBM_READ_tdata),
    .HBM_READ_tkeep(HBM_READ_tkeep),
    .HBM_READ_tlast(HBM_READ_tlast),
    .HBM_READ_tready(HBM_READ_tready),
    .HBM_READ_tvalid(HBM_READ_tvalid),
    .HBM_WRITE_tdata(HBM_WRITE_tdata),
    .HBM_WRITE_tready(HBM_WRITE_tready),
    .HBM_WRITE_tvalid(HBM_WRITE_tvalid),
    .clk_312(clk_312),
    .clk_450(clk_450),
    .rd_running(rd_running),
    .user_hbm_channel_ctrl_ports_busy(user_hbm_channel_ctrl_ports_busy),
    .user_hbm_channel_ctrl_ports_rd_blockamount(user_hbm_channel_ctrl_ports_rd_blockamount),
    .user_hbm_channel_ctrl_ports_rd_startaddress(user_hbm_channel_ctrl_ports_rd_startaddress),
    .user_hbm_channel_ctrl_ports_wr_blockamount(user_hbm_channel_ctrl_ports_wr_blockamount),
    .user_hbm_channel_ctrl_ports_wr_startaddress(user_hbm_channel_ctrl_ports_wr_startaddress),
    .wr_running(wr_running)
  );
endmodule
