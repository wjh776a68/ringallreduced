// (c) Copyright 1995-2022 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.


// IP VLNV: xilinx.com:module_ref:hbmlane_mux31:1.0
// IP Revision: 1

(* X_CORE_INFO = "hbmlane_mux31,Vivado 2022.2" *)
(* CHECK_LICENSE_TYPE = "design_hbm_hbmlane_mux31_0_1,hbmlane_mux31,{}" *)
(* CORE_GENERATION_INFO = "design_hbm_hbmlane_mux31_0_1,hbmlane_mux31,{x_ipProduct=Vivado 2022.2,x_ipVendor=xilinx.com,x_ipLibrary=module_ref,x_ipName=hbmlane_mux31,x_ipVersion=1.0,x_ipCoreRevision=1,x_ipLanguage=VERILOG,x_ipSimLanguage=MIXED}" *)
(* IP_DEFINITION_SOURCE = "module_ref" *)
(* DowngradeIPIdentifiedWarnings = "yes" *)
module design_hbm_hbmlane_mux31_0_1 (
  clk,
  mux_select,
  m_HBM_READ_tvalid,
  m_HBM_READ_tdata,
  m_HBM_READ_tready,
  m_HBM_READ_tlast,
  m_HBM_READ_tkeep,
  m_HBM_WRITE_tready,
  m_HBM_WRITE_tdata,
  m_HBM_WRITE_tvalid,
  m_rd_blockamount,
  m_rd_run,
  m_rd_startaddress,
  m_wr_blockamount,
  m_wr_run,
  m_wr_startaddress,
  m_busy,
  s1_HBM_READ_tvalid,
  s1_HBM_READ_tdata,
  s1_HBM_READ_tready,
  s1_HBM_READ_tlast,
  s1_HBM_READ_tkeep,
  s1_HBM_WRITE_tready,
  s1_HBM_WRITE_tdata,
  s1_HBM_WRITE_tvalid,
  s1_rd_blockamount,
  s1_rd_run,
  s1_rd_startaddress,
  s1_wr_blockamount,
  s1_wr_run,
  s1_wr_startaddress,
  s1_busy,
  s2_HBM_READ_tvalid,
  s2_HBM_READ_tdata,
  s2_HBM_READ_tready,
  s2_HBM_READ_tlast,
  s2_HBM_READ_tkeep,
  s2_HBM_WRITE_tready,
  s2_HBM_WRITE_tdata,
  s2_HBM_WRITE_tvalid,
  s2_rd_blockamount,
  s2_rd_run,
  s2_rd_startaddress,
  s2_wr_blockamount,
  s2_wr_run,
  s2_wr_startaddress,
  s2_busy,
  s3_HBM_READ_tvalid,
  s3_HBM_READ_tdata,
  s3_HBM_READ_tready,
  s3_HBM_READ_tlast,
  s3_HBM_READ_tkeep,
  s3_HBM_WRITE_tready,
  s3_HBM_WRITE_tdata,
  s3_HBM_WRITE_tvalid,
  s3_rd_blockamount,
  s3_rd_run,
  s3_rd_startaddress,
  s3_wr_blockamount,
  s3_wr_run,
  s3_wr_startaddress,
  s3_busy
);

(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME clk, ASSOCIATED_BUSIF m_HBM_READ:m_HBM_WRITE:s1_HBM_READ:s1_HBM_WRITE:s2_HBM_READ:s2_HBM_WRITE:s3_HBM_READ:s3_HBM_WRITE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN design_hbm_clk_312MHz, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 clk CLK" *)
input wire clk;
input wire [1 : 0] mux_select;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_HBM_READ TVALID" *)
output wire m_HBM_READ_tvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_HBM_READ TDATA" *)
output wire [255 : 0] m_HBM_READ_tdata;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_HBM_READ TREADY" *)
input wire m_HBM_READ_tready;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_HBM_READ TLAST" *)
output wire m_HBM_READ_tlast;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m_HBM_READ, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN design_hbm_clk_312MHz, LAYERED_METADATA undef, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_HBM_READ TKEEP" *)
output wire [31 : 0] m_HBM_READ_tkeep;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_HBM_WRITE TREADY" *)
output wire m_HBM_WRITE_tready;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_HBM_WRITE TDATA" *)
input wire [255 : 0] m_HBM_WRITE_tdata;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m_HBM_WRITE, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN design_hbm_clk_312MHz, LAYERED_METADATA undef, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_HBM_WRITE TVALID" *)
input wire m_HBM_WRITE_tvalid;
(* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 m_ctrl_ports rd_blockamount" *)
input wire [31 : 0] m_rd_blockamount;
(* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 m_ctrl_ports rd_run" *)
input wire m_rd_run;
(* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 m_ctrl_ports rd_startaddress" *)
input wire [31 : 0] m_rd_startaddress;
(* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 m_ctrl_ports wr_blockamount" *)
input wire [31 : 0] m_wr_blockamount;
(* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 m_ctrl_ports wr_run" *)
input wire m_wr_run;
(* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 m_ctrl_ports wr_startaddress" *)
input wire [31 : 0] m_wr_startaddress;
(* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 m_ctrl_ports busy" *)
output wire m_busy;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s1_HBM_READ TVALID" *)
input wire s1_HBM_READ_tvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s1_HBM_READ TDATA" *)
input wire [255 : 0] s1_HBM_READ_tdata;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s1_HBM_READ TREADY" *)
output wire s1_HBM_READ_tready;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s1_HBM_READ TLAST" *)
input wire s1_HBM_READ_tlast;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s1_HBM_READ, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN design_hbm_clk_312MHz, LAYERED_METADATA undef, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s1_HBM_READ TKEEP" *)
input wire [31 : 0] s1_HBM_READ_tkeep;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s1_HBM_WRITE TREADY" *)
input wire s1_HBM_WRITE_tready;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s1_HBM_WRITE TDATA" *)
output wire [255 : 0] s1_HBM_WRITE_tdata;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s1_HBM_WRITE, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN design_hbm_clk_312MHz, LAYERED_METADATA undef, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s1_HBM_WRITE TVALID" *)
output wire s1_HBM_WRITE_tvalid;
(* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 s1_ctrl_ports rd_blockamount" *)
output wire [31 : 0] s1_rd_blockamount;
(* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 s1_ctrl_ports rd_run" *)
output wire s1_rd_run;
(* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 s1_ctrl_ports rd_startaddress" *)
output wire [31 : 0] s1_rd_startaddress;
(* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 s1_ctrl_ports wr_blockamount" *)
output wire [31 : 0] s1_wr_blockamount;
(* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 s1_ctrl_ports wr_run" *)
output wire s1_wr_run;
(* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 s1_ctrl_ports wr_startaddress" *)
output wire [31 : 0] s1_wr_startaddress;
(* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 s1_ctrl_ports busy" *)
input wire s1_busy;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s2_HBM_READ TVALID" *)
input wire s2_HBM_READ_tvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s2_HBM_READ TDATA" *)
input wire [255 : 0] s2_HBM_READ_tdata;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s2_HBM_READ TREADY" *)
output wire s2_HBM_READ_tready;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s2_HBM_READ TLAST" *)
input wire s2_HBM_READ_tlast;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s2_HBM_READ, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN design_hbm_clk_312MHz, LAYERED_METADATA undef, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s2_HBM_READ TKEEP" *)
input wire [31 : 0] s2_HBM_READ_tkeep;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s2_HBM_WRITE TREADY" *)
input wire s2_HBM_WRITE_tready;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s2_HBM_WRITE TDATA" *)
output wire [255 : 0] s2_HBM_WRITE_tdata;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s2_HBM_WRITE, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN design_hbm_clk_312MHz, LAYERED_METADATA undef, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s2_HBM_WRITE TVALID" *)
output wire s2_HBM_WRITE_tvalid;
(* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 s2_ctrl_ports rd_blockamount" *)
output wire [31 : 0] s2_rd_blockamount;
(* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 s2_ctrl_ports rd_run" *)
output wire s2_rd_run;
(* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 s2_ctrl_ports rd_startaddress" *)
output wire [31 : 0] s2_rd_startaddress;
(* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 s2_ctrl_ports wr_blockamount" *)
output wire [31 : 0] s2_wr_blockamount;
(* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 s2_ctrl_ports wr_run" *)
output wire s2_wr_run;
(* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 s2_ctrl_ports wr_startaddress" *)
output wire [31 : 0] s2_wr_startaddress;
(* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 s2_ctrl_ports busy" *)
input wire s2_busy;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s3_HBM_READ TVALID" *)
input wire s3_HBM_READ_tvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s3_HBM_READ TDATA" *)
input wire [255 : 0] s3_HBM_READ_tdata;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s3_HBM_READ TREADY" *)
output wire s3_HBM_READ_tready;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s3_HBM_READ TLAST" *)
input wire s3_HBM_READ_tlast;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s3_HBM_READ, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN design_hbm_clk_312MHz, LAYERED_METADATA undef, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s3_HBM_READ TKEEP" *)
input wire [31 : 0] s3_HBM_READ_tkeep;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s3_HBM_WRITE TREADY" *)
input wire s3_HBM_WRITE_tready;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s3_HBM_WRITE TDATA" *)
output wire [255 : 0] s3_HBM_WRITE_tdata;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s3_HBM_WRITE, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN design_hbm_clk_312MHz, LAYERED_METADATA undef, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s3_HBM_WRITE TVALID" *)
output wire s3_HBM_WRITE_tvalid;
(* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 s3_ctrl_ports rd_blockamount" *)
output wire [31 : 0] s3_rd_blockamount;
(* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 s3_ctrl_ports rd_run" *)
output wire s3_rd_run;
(* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 s3_ctrl_ports rd_startaddress" *)
output wire [31 : 0] s3_rd_startaddress;
(* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 s3_ctrl_ports wr_blockamount" *)
output wire [31 : 0] s3_wr_blockamount;
(* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 s3_ctrl_ports wr_run" *)
output wire s3_wr_run;
(* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 s3_ctrl_ports wr_startaddress" *)
output wire [31 : 0] s3_wr_startaddress;
(* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 s3_ctrl_ports busy" *)
input wire s3_busy;

  hbmlane_mux31 inst (
    .clk(clk),
    .mux_select(mux_select),
    .m_HBM_READ_tvalid(m_HBM_READ_tvalid),
    .m_HBM_READ_tdata(m_HBM_READ_tdata),
    .m_HBM_READ_tready(m_HBM_READ_tready),
    .m_HBM_READ_tlast(m_HBM_READ_tlast),
    .m_HBM_READ_tkeep(m_HBM_READ_tkeep),
    .m_HBM_WRITE_tready(m_HBM_WRITE_tready),
    .m_HBM_WRITE_tdata(m_HBM_WRITE_tdata),
    .m_HBM_WRITE_tvalid(m_HBM_WRITE_tvalid),
    .m_rd_blockamount(m_rd_blockamount),
    .m_rd_run(m_rd_run),
    .m_rd_startaddress(m_rd_startaddress),
    .m_wr_blockamount(m_wr_blockamount),
    .m_wr_run(m_wr_run),
    .m_wr_startaddress(m_wr_startaddress),
    .m_busy(m_busy),
    .s1_HBM_READ_tvalid(s1_HBM_READ_tvalid),
    .s1_HBM_READ_tdata(s1_HBM_READ_tdata),
    .s1_HBM_READ_tready(s1_HBM_READ_tready),
    .s1_HBM_READ_tlast(s1_HBM_READ_tlast),
    .s1_HBM_READ_tkeep(s1_HBM_READ_tkeep),
    .s1_HBM_WRITE_tready(s1_HBM_WRITE_tready),
    .s1_HBM_WRITE_tdata(s1_HBM_WRITE_tdata),
    .s1_HBM_WRITE_tvalid(s1_HBM_WRITE_tvalid),
    .s1_rd_blockamount(s1_rd_blockamount),
    .s1_rd_run(s1_rd_run),
    .s1_rd_startaddress(s1_rd_startaddress),
    .s1_wr_blockamount(s1_wr_blockamount),
    .s1_wr_run(s1_wr_run),
    .s1_wr_startaddress(s1_wr_startaddress),
    .s1_busy(s1_busy),
    .s2_HBM_READ_tvalid(s2_HBM_READ_tvalid),
    .s2_HBM_READ_tdata(s2_HBM_READ_tdata),
    .s2_HBM_READ_tready(s2_HBM_READ_tready),
    .s2_HBM_READ_tlast(s2_HBM_READ_tlast),
    .s2_HBM_READ_tkeep(s2_HBM_READ_tkeep),
    .s2_HBM_WRITE_tready(s2_HBM_WRITE_tready),
    .s2_HBM_WRITE_tdata(s2_HBM_WRITE_tdata),
    .s2_HBM_WRITE_tvalid(s2_HBM_WRITE_tvalid),
    .s2_rd_blockamount(s2_rd_blockamount),
    .s2_rd_run(s2_rd_run),
    .s2_rd_startaddress(s2_rd_startaddress),
    .s2_wr_blockamount(s2_wr_blockamount),
    .s2_wr_run(s2_wr_run),
    .s2_wr_startaddress(s2_wr_startaddress),
    .s2_busy(s2_busy),
    .s3_HBM_READ_tvalid(s3_HBM_READ_tvalid),
    .s3_HBM_READ_tdata(s3_HBM_READ_tdata),
    .s3_HBM_READ_tready(s3_HBM_READ_tready),
    .s3_HBM_READ_tlast(s3_HBM_READ_tlast),
    .s3_HBM_READ_tkeep(s3_HBM_READ_tkeep),
    .s3_HBM_WRITE_tready(s3_HBM_WRITE_tready),
    .s3_HBM_WRITE_tdata(s3_HBM_WRITE_tdata),
    .s3_HBM_WRITE_tvalid(s3_HBM_WRITE_tvalid),
    .s3_rd_blockamount(s3_rd_blockamount),
    .s3_rd_run(s3_rd_run),
    .s3_rd_startaddress(s3_rd_startaddress),
    .s3_wr_blockamount(s3_wr_blockamount),
    .s3_wr_run(s3_wr_run),
    .s3_wr_startaddress(s3_wr_startaddress),
    .s3_busy(s3_busy)
  );
endmodule
