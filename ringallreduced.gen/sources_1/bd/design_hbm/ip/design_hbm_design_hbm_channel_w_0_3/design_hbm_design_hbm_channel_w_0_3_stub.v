// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
// Date        : Tue Dec  6 16:31:49 2022
// Host        : dell-Vostro-3910-China-HDD-Protection running 64-bit Ubuntu 22.04.1 LTS
// Command     : write_verilog -force -mode synth_stub
//               /media/dell/workspace/Xilinx_projects/ringallreduced/ringallreduced.gen/sources_1/bd/design_hbm/ip/design_hbm_design_hbm_channel_w_0_3/design_hbm_design_hbm_channel_w_0_3_stub.v
// Design      : design_hbm_design_hbm_channel_w_0_3
// Purpose     : Stub declaration of top-level module interface
// Device      : xcvu37p-fsvh2892-2L-e
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "design_hbm_channel_wrapper,Vivado 2022.2" *)
module design_hbm_design_hbm_channel_w_0_3(HBM_AXI_araddr, HBM_AXI_arburst, 
  HBM_AXI_arid, HBM_AXI_arlen, HBM_AXI_arready, HBM_AXI_arsize, HBM_AXI_arvalid, 
  HBM_AXI_awaddr, HBM_AXI_awburst, HBM_AXI_awid, HBM_AXI_awlen, HBM_AXI_awready, 
  HBM_AXI_awsize, HBM_AXI_awvalid, HBM_AXI_bid, HBM_AXI_bready, HBM_AXI_bresp, 
  HBM_AXI_bvalid, HBM_AXI_rdata, HBM_AXI_rid, HBM_AXI_rlast, HBM_AXI_rready, HBM_AXI_rresp, 
  HBM_AXI_rvalid, HBM_AXI_wdata, HBM_AXI_wid, HBM_AXI_wlast, HBM_AXI_wready, HBM_AXI_wstrb, 
  HBM_AXI_wvalid, HBM_READ_tdata, HBM_READ_tkeep, HBM_READ_tlast, HBM_READ_tready, 
  HBM_READ_tvalid, HBM_WRITE_tdata, HBM_WRITE_tready, HBM_WRITE_tvalid, clk_312, clk_450, 
  rd_running, user_hbm_channel_ctrl_ports_busy, 
  user_hbm_channel_ctrl_ports_rd_blockamount, 
  user_hbm_channel_ctrl_ports_rd_startaddress, 
  user_hbm_channel_ctrl_ports_wr_blockamount, 
  user_hbm_channel_ctrl_ports_wr_startaddress, wr_running)
/* synthesis syn_black_box black_box_pad_pin="HBM_AXI_araddr[32:0],HBM_AXI_arburst[1:0],HBM_AXI_arid[5:0],HBM_AXI_arlen[3:0],HBM_AXI_arready,HBM_AXI_arsize[2:0],HBM_AXI_arvalid,HBM_AXI_awaddr[32:0],HBM_AXI_awburst[1:0],HBM_AXI_awid[5:0],HBM_AXI_awlen[3:0],HBM_AXI_awready,HBM_AXI_awsize[2:0],HBM_AXI_awvalid,HBM_AXI_bid[5:0],HBM_AXI_bready,HBM_AXI_bresp[1:0],HBM_AXI_bvalid,HBM_AXI_rdata[255:0],HBM_AXI_rid[5:0],HBM_AXI_rlast,HBM_AXI_rready,HBM_AXI_rresp[1:0],HBM_AXI_rvalid,HBM_AXI_wdata[255:0],HBM_AXI_wid[5:0],HBM_AXI_wlast,HBM_AXI_wready,HBM_AXI_wstrb[31:0],HBM_AXI_wvalid,HBM_READ_tdata[255:0],HBM_READ_tkeep[31:0],HBM_READ_tlast,HBM_READ_tready,HBM_READ_tvalid,HBM_WRITE_tdata[255:0],HBM_WRITE_tready,HBM_WRITE_tvalid,clk_312,clk_450,rd_running,user_hbm_channel_ctrl_ports_busy,user_hbm_channel_ctrl_ports_rd_blockamount[31:0],user_hbm_channel_ctrl_ports_rd_startaddress[31:0],user_hbm_channel_ctrl_ports_wr_blockamount[31:0],user_hbm_channel_ctrl_ports_wr_startaddress[31:0],wr_running" */;
  output [32:0]HBM_AXI_araddr;
  output [1:0]HBM_AXI_arburst;
  output [5:0]HBM_AXI_arid;
  output [3:0]HBM_AXI_arlen;
  input HBM_AXI_arready;
  output [2:0]HBM_AXI_arsize;
  output HBM_AXI_arvalid;
  output [32:0]HBM_AXI_awaddr;
  output [1:0]HBM_AXI_awburst;
  output [5:0]HBM_AXI_awid;
  output [3:0]HBM_AXI_awlen;
  input HBM_AXI_awready;
  output [2:0]HBM_AXI_awsize;
  output HBM_AXI_awvalid;
  input [5:0]HBM_AXI_bid;
  output HBM_AXI_bready;
  input [1:0]HBM_AXI_bresp;
  input HBM_AXI_bvalid;
  input [255:0]HBM_AXI_rdata;
  input [5:0]HBM_AXI_rid;
  input HBM_AXI_rlast;
  output HBM_AXI_rready;
  input [1:0]HBM_AXI_rresp;
  input HBM_AXI_rvalid;
  output [255:0]HBM_AXI_wdata;
  output [5:0]HBM_AXI_wid;
  output HBM_AXI_wlast;
  input HBM_AXI_wready;
  output [31:0]HBM_AXI_wstrb;
  output HBM_AXI_wvalid;
  output [255:0]HBM_READ_tdata;
  output [31:0]HBM_READ_tkeep;
  output HBM_READ_tlast;
  input HBM_READ_tready;
  output HBM_READ_tvalid;
  input [255:0]HBM_WRITE_tdata;
  output HBM_WRITE_tready;
  input HBM_WRITE_tvalid;
  input clk_312;
  input clk_450;
  input rd_running;
  output user_hbm_channel_ctrl_ports_busy;
  input [31:0]user_hbm_channel_ctrl_ports_rd_blockamount;
  input [31:0]user_hbm_channel_ctrl_ports_rd_startaddress;
  input [31:0]user_hbm_channel_ctrl_ports_wr_blockamount;
  input [31:0]user_hbm_channel_ctrl_ports_wr_startaddress;
  input wr_running;
endmodule
