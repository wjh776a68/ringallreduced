-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
-- Date        : Tue Dec  6 16:31:48 2022
-- Host        : dell-Vostro-3910-China-HDD-Protection running 64-bit Ubuntu 22.04.1 LTS
-- Command     : write_vhdl -force -mode synth_stub -rename_top design_hbm_design_hbm_channel_w_1_0 -prefix
--               design_hbm_design_hbm_channel_w_1_0_ design_hbm_design_hbm_channel_w_0_2_stub.vhdl
-- Design      : design_hbm_design_hbm_channel_w_0_2
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xcvu37p-fsvh2892-2L-e
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity design_hbm_design_hbm_channel_w_1_0 is
  Port ( 
    HBM_AXI_araddr : out STD_LOGIC_VECTOR ( 32 downto 0 );
    HBM_AXI_arburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    HBM_AXI_arid : out STD_LOGIC_VECTOR ( 5 downto 0 );
    HBM_AXI_arlen : out STD_LOGIC_VECTOR ( 3 downto 0 );
    HBM_AXI_arready : in STD_LOGIC;
    HBM_AXI_arsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    HBM_AXI_arvalid : out STD_LOGIC;
    HBM_AXI_awaddr : out STD_LOGIC_VECTOR ( 32 downto 0 );
    HBM_AXI_awburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    HBM_AXI_awid : out STD_LOGIC_VECTOR ( 5 downto 0 );
    HBM_AXI_awlen : out STD_LOGIC_VECTOR ( 3 downto 0 );
    HBM_AXI_awready : in STD_LOGIC;
    HBM_AXI_awsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    HBM_AXI_awvalid : out STD_LOGIC;
    HBM_AXI_bid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    HBM_AXI_bready : out STD_LOGIC;
    HBM_AXI_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    HBM_AXI_bvalid : in STD_LOGIC;
    HBM_AXI_rdata : in STD_LOGIC_VECTOR ( 255 downto 0 );
    HBM_AXI_rid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    HBM_AXI_rlast : in STD_LOGIC;
    HBM_AXI_rready : out STD_LOGIC;
    HBM_AXI_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    HBM_AXI_rvalid : in STD_LOGIC;
    HBM_AXI_wdata : out STD_LOGIC_VECTOR ( 255 downto 0 );
    HBM_AXI_wid : out STD_LOGIC_VECTOR ( 5 downto 0 );
    HBM_AXI_wlast : out STD_LOGIC;
    HBM_AXI_wready : in STD_LOGIC;
    HBM_AXI_wstrb : out STD_LOGIC_VECTOR ( 31 downto 0 );
    HBM_AXI_wvalid : out STD_LOGIC;
    HBM_READ_tdata : out STD_LOGIC_VECTOR ( 255 downto 0 );
    HBM_READ_tkeep : out STD_LOGIC_VECTOR ( 31 downto 0 );
    HBM_READ_tlast : out STD_LOGIC;
    HBM_READ_tready : in STD_LOGIC;
    HBM_READ_tvalid : out STD_LOGIC;
    HBM_WRITE_tdata : in STD_LOGIC_VECTOR ( 255 downto 0 );
    HBM_WRITE_tready : out STD_LOGIC;
    HBM_WRITE_tvalid : in STD_LOGIC;
    clk_312 : in STD_LOGIC;
    clk_450 : in STD_LOGIC;
    rd_running : in STD_LOGIC;
    user_hbm_channel_ctrl_ports_busy : out STD_LOGIC;
    user_hbm_channel_ctrl_ports_rd_blockamount : in STD_LOGIC_VECTOR ( 31 downto 0 );
    user_hbm_channel_ctrl_ports_rd_startaddress : in STD_LOGIC_VECTOR ( 31 downto 0 );
    user_hbm_channel_ctrl_ports_wr_blockamount : in STD_LOGIC_VECTOR ( 31 downto 0 );
    user_hbm_channel_ctrl_ports_wr_startaddress : in STD_LOGIC_VECTOR ( 31 downto 0 );
    wr_running : in STD_LOGIC
  );

end design_hbm_design_hbm_channel_w_1_0;

architecture stub of design_hbm_design_hbm_channel_w_1_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "HBM_AXI_araddr[32:0],HBM_AXI_arburst[1:0],HBM_AXI_arid[5:0],HBM_AXI_arlen[3:0],HBM_AXI_arready,HBM_AXI_arsize[2:0],HBM_AXI_arvalid,HBM_AXI_awaddr[32:0],HBM_AXI_awburst[1:0],HBM_AXI_awid[5:0],HBM_AXI_awlen[3:0],HBM_AXI_awready,HBM_AXI_awsize[2:0],HBM_AXI_awvalid,HBM_AXI_bid[5:0],HBM_AXI_bready,HBM_AXI_bresp[1:0],HBM_AXI_bvalid,HBM_AXI_rdata[255:0],HBM_AXI_rid[5:0],HBM_AXI_rlast,HBM_AXI_rready,HBM_AXI_rresp[1:0],HBM_AXI_rvalid,HBM_AXI_wdata[255:0],HBM_AXI_wid[5:0],HBM_AXI_wlast,HBM_AXI_wready,HBM_AXI_wstrb[31:0],HBM_AXI_wvalid,HBM_READ_tdata[255:0],HBM_READ_tkeep[31:0],HBM_READ_tlast,HBM_READ_tready,HBM_READ_tvalid,HBM_WRITE_tdata[255:0],HBM_WRITE_tready,HBM_WRITE_tvalid,clk_312,clk_450,rd_running,user_hbm_channel_ctrl_ports_busy,user_hbm_channel_ctrl_ports_rd_blockamount[31:0],user_hbm_channel_ctrl_ports_rd_startaddress[31:0],user_hbm_channel_ctrl_ports_wr_blockamount[31:0],user_hbm_channel_ctrl_ports_wr_startaddress[31:0],wr_running";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "design_hbm_channel_wrapper,Vivado 2022.2";
begin
end;
