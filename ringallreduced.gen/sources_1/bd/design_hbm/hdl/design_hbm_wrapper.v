//Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
//Date        : Tue Dec  6 16:10:09 2022
//Host        : dell-Vostro-3910-China-HDD-Protection running 64-bit Ubuntu 22.04.1 LTS
//Command     : generate_target design_hbm_wrapper.bd
//Design      : design_hbm_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_hbm_wrapper
   (HBM_READ_m_0_tdata,
    HBM_READ_m_0_tkeep,
    HBM_READ_m_0_tlast,
    HBM_READ_m_0_tready,
    HBM_READ_m_0_tvalid,
    HBM_READ_m_1_tdata,
    HBM_READ_m_1_tkeep,
    HBM_READ_m_1_tlast,
    HBM_READ_m_1_tready,
    HBM_READ_m_1_tvalid,
    HBM_READ_m_2_tdata,
    HBM_READ_m_2_tkeep,
    HBM_READ_m_2_tlast,
    HBM_READ_m_2_tready,
    HBM_READ_m_2_tvalid,
    HBM_STATUS,
    HBM_WRITE_m_0_tdata,
    HBM_WRITE_m_0_tready,
    HBM_WRITE_m_0_tvalid,
    HBM_WRITE_m_1_tdata,
    HBM_WRITE_m_1_tready,
    HBM_WRITE_m_1_tvalid,
    HBM_WRITE_m_2_tdata,
    HBM_WRITE_m_2_tready,
    HBM_WRITE_m_2_tvalid,
    clk_100MHz,
    clk_312MHz,
    m_busy_0,
    m_busy_1,
    m_busy_2,
    m_ctrl_ports_0_rd_blockamount,
    m_ctrl_ports_0_rd_run,
    m_ctrl_ports_0_rd_startaddress,
    m_ctrl_ports_0_wr_blockamount,
    m_ctrl_ports_0_wr_run,
    m_ctrl_ports_0_wr_startaddress,
    m_ctrl_ports_1_rd_blockamount,
    m_ctrl_ports_1_rd_run,
    m_ctrl_ports_1_rd_startaddress,
    m_ctrl_ports_1_wr_blockamount,
    m_ctrl_ports_1_wr_run,
    m_ctrl_ports_1_wr_startaddress,
    m_ctrl_ports_2_rd_blockamount,
    m_ctrl_ports_2_rd_run,
    m_ctrl_ports_2_rd_startaddress,
    m_ctrl_ports_2_wr_blockamount,
    m_ctrl_ports_2_wr_run,
    m_ctrl_ports_2_wr_startaddress,
    mux_select_0,
    mux_select_1,
    mux_select_2,
    reset);
  output [255:0]HBM_READ_m_0_tdata;
  output [31:0]HBM_READ_m_0_tkeep;
  output HBM_READ_m_0_tlast;
  input HBM_READ_m_0_tready;
  output HBM_READ_m_0_tvalid;
  output [255:0]HBM_READ_m_1_tdata;
  output [31:0]HBM_READ_m_1_tkeep;
  output HBM_READ_m_1_tlast;
  input HBM_READ_m_1_tready;
  output HBM_READ_m_1_tvalid;
  output [255:0]HBM_READ_m_2_tdata;
  output [31:0]HBM_READ_m_2_tkeep;
  output HBM_READ_m_2_tlast;
  input HBM_READ_m_2_tready;
  output HBM_READ_m_2_tvalid;
  output [0:0]HBM_STATUS;
  input [63:0]HBM_WRITE_m_0_tdata;
  output HBM_WRITE_m_0_tready;
  input HBM_WRITE_m_0_tvalid;
  input [63:0]HBM_WRITE_m_1_tdata;
  output HBM_WRITE_m_1_tready;
  input HBM_WRITE_m_1_tvalid;
  input [63:0]HBM_WRITE_m_2_tdata;
  output HBM_WRITE_m_2_tready;
  input HBM_WRITE_m_2_tvalid;
  input clk_100MHz;
  input clk_312MHz;
  output m_busy_0;
  output m_busy_1;
  output m_busy_2;
  input [31:0]m_ctrl_ports_0_rd_blockamount;
  input m_ctrl_ports_0_rd_run;
  input [31:0]m_ctrl_ports_0_rd_startaddress;
  input [31:0]m_ctrl_ports_0_wr_blockamount;
  input m_ctrl_ports_0_wr_run;
  input [31:0]m_ctrl_ports_0_wr_startaddress;
  input [31:0]m_ctrl_ports_1_rd_blockamount;
  input m_ctrl_ports_1_rd_run;
  input [31:0]m_ctrl_ports_1_rd_startaddress;
  input [31:0]m_ctrl_ports_1_wr_blockamount;
  input m_ctrl_ports_1_wr_run;
  input [31:0]m_ctrl_ports_1_wr_startaddress;
  input [31:0]m_ctrl_ports_2_rd_blockamount;
  input m_ctrl_ports_2_rd_run;
  input [31:0]m_ctrl_ports_2_rd_startaddress;
  input [31:0]m_ctrl_ports_2_wr_blockamount;
  input m_ctrl_ports_2_wr_run;
  input [31:0]m_ctrl_ports_2_wr_startaddress;
  input [1:0]mux_select_0;
  input [1:0]mux_select_1;
  input [1:0]mux_select_2;
  input reset;

  wire [255:0]HBM_READ_m_0_tdata;
  wire [31:0]HBM_READ_m_0_tkeep;
  wire HBM_READ_m_0_tlast;
  wire HBM_READ_m_0_tready;
  wire HBM_READ_m_0_tvalid;
  wire [255:0]HBM_READ_m_1_tdata;
  wire [31:0]HBM_READ_m_1_tkeep;
  wire HBM_READ_m_1_tlast;
  wire HBM_READ_m_1_tready;
  wire HBM_READ_m_1_tvalid;
  wire [255:0]HBM_READ_m_2_tdata;
  wire [31:0]HBM_READ_m_2_tkeep;
  wire HBM_READ_m_2_tlast;
  wire HBM_READ_m_2_tready;
  wire HBM_READ_m_2_tvalid;
  wire [0:0]HBM_STATUS;
  wire [63:0]HBM_WRITE_m_0_tdata;
  wire HBM_WRITE_m_0_tready;
  wire HBM_WRITE_m_0_tvalid;
  wire [63:0]HBM_WRITE_m_1_tdata;
  wire HBM_WRITE_m_1_tready;
  wire HBM_WRITE_m_1_tvalid;
  wire [63:0]HBM_WRITE_m_2_tdata;
  wire HBM_WRITE_m_2_tready;
  wire HBM_WRITE_m_2_tvalid;
  wire clk_100MHz;
  wire clk_312MHz;
  wire m_busy_0;
  wire m_busy_1;
  wire m_busy_2;
  wire [31:0]m_ctrl_ports_0_rd_blockamount;
  wire m_ctrl_ports_0_rd_run;
  wire [31:0]m_ctrl_ports_0_rd_startaddress;
  wire [31:0]m_ctrl_ports_0_wr_blockamount;
  wire m_ctrl_ports_0_wr_run;
  wire [31:0]m_ctrl_ports_0_wr_startaddress;
  wire [31:0]m_ctrl_ports_1_rd_blockamount;
  wire m_ctrl_ports_1_rd_run;
  wire [31:0]m_ctrl_ports_1_rd_startaddress;
  wire [31:0]m_ctrl_ports_1_wr_blockamount;
  wire m_ctrl_ports_1_wr_run;
  wire [31:0]m_ctrl_ports_1_wr_startaddress;
  wire [31:0]m_ctrl_ports_2_rd_blockamount;
  wire m_ctrl_ports_2_rd_run;
  wire [31:0]m_ctrl_ports_2_rd_startaddress;
  wire [31:0]m_ctrl_ports_2_wr_blockamount;
  wire m_ctrl_ports_2_wr_run;
  wire [31:0]m_ctrl_ports_2_wr_startaddress;
  wire [1:0]mux_select_0;
  wire [1:0]mux_select_1;
  wire [1:0]mux_select_2;
  wire reset;

  design_hbm design_hbm_i
       (.HBM_READ_m_0_tdata(HBM_READ_m_0_tdata),
        .HBM_READ_m_0_tkeep(HBM_READ_m_0_tkeep),
        .HBM_READ_m_0_tlast(HBM_READ_m_0_tlast),
        .HBM_READ_m_0_tready(HBM_READ_m_0_tready),
        .HBM_READ_m_0_tvalid(HBM_READ_m_0_tvalid),
        .HBM_READ_m_1_tdata(HBM_READ_m_1_tdata),
        .HBM_READ_m_1_tkeep(HBM_READ_m_1_tkeep),
        .HBM_READ_m_1_tlast(HBM_READ_m_1_tlast),
        .HBM_READ_m_1_tready(HBM_READ_m_1_tready),
        .HBM_READ_m_1_tvalid(HBM_READ_m_1_tvalid),
        .HBM_READ_m_2_tdata(HBM_READ_m_2_tdata),
        .HBM_READ_m_2_tkeep(HBM_READ_m_2_tkeep),
        .HBM_READ_m_2_tlast(HBM_READ_m_2_tlast),
        .HBM_READ_m_2_tready(HBM_READ_m_2_tready),
        .HBM_READ_m_2_tvalid(HBM_READ_m_2_tvalid),
        .HBM_STATUS(HBM_STATUS),
        .HBM_WRITE_m_0_tdata(HBM_WRITE_m_0_tdata),
        .HBM_WRITE_m_0_tready(HBM_WRITE_m_0_tready),
        .HBM_WRITE_m_0_tvalid(HBM_WRITE_m_0_tvalid),
        .HBM_WRITE_m_1_tdata(HBM_WRITE_m_1_tdata),
        .HBM_WRITE_m_1_tready(HBM_WRITE_m_1_tready),
        .HBM_WRITE_m_1_tvalid(HBM_WRITE_m_1_tvalid),
        .HBM_WRITE_m_2_tdata(HBM_WRITE_m_2_tdata),
        .HBM_WRITE_m_2_tready(HBM_WRITE_m_2_tready),
        .HBM_WRITE_m_2_tvalid(HBM_WRITE_m_2_tvalid),
        .clk_100MHz(clk_100MHz),
        .clk_312MHz(clk_312MHz),
        .m_busy_0(m_busy_0),
        .m_busy_1(m_busy_1),
        .m_busy_2(m_busy_2),
        .m_ctrl_ports_0_rd_blockamount(m_ctrl_ports_0_rd_blockamount),
        .m_ctrl_ports_0_rd_run(m_ctrl_ports_0_rd_run),
        .m_ctrl_ports_0_rd_startaddress(m_ctrl_ports_0_rd_startaddress),
        .m_ctrl_ports_0_wr_blockamount(m_ctrl_ports_0_wr_blockamount),
        .m_ctrl_ports_0_wr_run(m_ctrl_ports_0_wr_run),
        .m_ctrl_ports_0_wr_startaddress(m_ctrl_ports_0_wr_startaddress),
        .m_ctrl_ports_1_rd_blockamount(m_ctrl_ports_1_rd_blockamount),
        .m_ctrl_ports_1_rd_run(m_ctrl_ports_1_rd_run),
        .m_ctrl_ports_1_rd_startaddress(m_ctrl_ports_1_rd_startaddress),
        .m_ctrl_ports_1_wr_blockamount(m_ctrl_ports_1_wr_blockamount),
        .m_ctrl_ports_1_wr_run(m_ctrl_ports_1_wr_run),
        .m_ctrl_ports_1_wr_startaddress(m_ctrl_ports_1_wr_startaddress),
        .m_ctrl_ports_2_rd_blockamount(m_ctrl_ports_2_rd_blockamount),
        .m_ctrl_ports_2_rd_run(m_ctrl_ports_2_rd_run),
        .m_ctrl_ports_2_rd_startaddress(m_ctrl_ports_2_rd_startaddress),
        .m_ctrl_ports_2_wr_blockamount(m_ctrl_ports_2_wr_blockamount),
        .m_ctrl_ports_2_wr_run(m_ctrl_ports_2_wr_run),
        .m_ctrl_ports_2_wr_startaddress(m_ctrl_ports_2_wr_startaddress),
        .mux_select_0(mux_select_0),
        .mux_select_1(mux_select_1),
        .mux_select_2(mux_select_2),
        .reset(reset));
endmodule
