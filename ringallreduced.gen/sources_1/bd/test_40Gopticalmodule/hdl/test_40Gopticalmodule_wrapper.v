//Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
//Date        : Tue Dec  6 08:21:46 2022
//Host        : dell-Vostro-3910-China-HDD-Protection running 64-bit Ubuntu 22.04.1 LTS
//Command     : generate_target test_40Gopticalmodule_wrapper.bd
//Design      : test_40Gopticalmodule_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module test_40Gopticalmodule_wrapper
   (CLK_IN1_D_0_clk_n,
    CLK_IN1_D_0_clk_p,
    gt_ref_clk_2_clk_n,
    gt_ref_clk_2_clk_p,
    gt_ref_clk_3_clk_n,
    gt_ref_clk_3_clk_p,
    gt_serial_port_2_grx_n,
    gt_serial_port_2_grx_p,
    gt_serial_port_2_gtx_n,
    gt_serial_port_2_gtx_p,
    gt_serial_port_3_grx_n,
    gt_serial_port_3_grx_p,
    gt_serial_port_3_gtx_n,
    gt_serial_port_3_gtx_p,
    reset,
    stat_rx_status_sfp0,
    stat_rx_status_sfp1);
  input CLK_IN1_D_0_clk_n;
  input CLK_IN1_D_0_clk_p;
  input gt_ref_clk_2_clk_n;
  input gt_ref_clk_2_clk_p;
  input gt_ref_clk_3_clk_n;
  input gt_ref_clk_3_clk_p;
  input gt_serial_port_2_grx_n;
  input gt_serial_port_2_grx_p;
  output gt_serial_port_2_gtx_n;
  output gt_serial_port_2_gtx_p;
  input gt_serial_port_3_grx_n;
  input gt_serial_port_3_grx_p;
  output gt_serial_port_3_gtx_n;
  output gt_serial_port_3_gtx_p;
  input reset;
  output stat_rx_status_sfp0;
  output stat_rx_status_sfp1;

  wire CLK_IN1_D_0_clk_n;
  wire CLK_IN1_D_0_clk_p;
  wire gt_ref_clk_2_clk_n;
  wire gt_ref_clk_2_clk_p;
  wire gt_ref_clk_3_clk_n;
  wire gt_ref_clk_3_clk_p;
  wire gt_serial_port_2_grx_n;
  wire gt_serial_port_2_grx_p;
  wire gt_serial_port_2_gtx_n;
  wire gt_serial_port_2_gtx_p;
  wire gt_serial_port_3_grx_n;
  wire gt_serial_port_3_grx_p;
  wire gt_serial_port_3_gtx_n;
  wire gt_serial_port_3_gtx_p;
  wire reset;
  wire stat_rx_status_sfp0;
  wire stat_rx_status_sfp1;

  test_40Gopticalmodule test_40Gopticalmodule_i
       (.CLK_IN1_D_0_clk_n(CLK_IN1_D_0_clk_n),
        .CLK_IN1_D_0_clk_p(CLK_IN1_D_0_clk_p),
        .gt_ref_clk_2_clk_n(gt_ref_clk_2_clk_n),
        .gt_ref_clk_2_clk_p(gt_ref_clk_2_clk_p),
        .gt_ref_clk_3_clk_n(gt_ref_clk_3_clk_n),
        .gt_ref_clk_3_clk_p(gt_ref_clk_3_clk_p),
        .gt_serial_port_2_grx_n(gt_serial_port_2_grx_n),
        .gt_serial_port_2_grx_p(gt_serial_port_2_grx_p),
        .gt_serial_port_2_gtx_n(gt_serial_port_2_gtx_n),
        .gt_serial_port_2_gtx_p(gt_serial_port_2_gtx_p),
        .gt_serial_port_3_grx_n(gt_serial_port_3_grx_n),
        .gt_serial_port_3_grx_p(gt_serial_port_3_grx_p),
        .gt_serial_port_3_gtx_n(gt_serial_port_3_gtx_n),
        .gt_serial_port_3_gtx_p(gt_serial_port_3_gtx_p),
        .reset(reset),
        .stat_rx_status_sfp0(stat_rx_status_sfp0),
        .stat_rx_status_sfp1(stat_rx_status_sfp1));
endmodule
