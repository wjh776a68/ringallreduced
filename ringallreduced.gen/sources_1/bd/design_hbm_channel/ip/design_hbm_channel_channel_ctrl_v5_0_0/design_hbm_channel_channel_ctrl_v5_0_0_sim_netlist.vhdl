-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
-- Date        : Tue Dec  6 16:15:51 2022
-- Host        : dell-Vostro-3910-China-HDD-Protection running 64-bit Ubuntu 22.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim
--               /media/dell/workspace/Xilinx_projects/ringallreduced/ringallreduced.gen/sources_1/bd/design_hbm_channel/ip/design_hbm_channel_channel_ctrl_v5_0_0/design_hbm_channel_channel_ctrl_v5_0_0_sim_netlist.vhdl
-- Design      : design_hbm_channel_channel_ctrl_v5_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xcvu37p-fsvh2892-2L-e
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_hbm_channel_channel_ctrl_v5_0_0_channel_ctrl_v5 is
  port (
    AXI_ARADDR : out STD_LOGIC_VECTOR ( 32 downto 0 );
    AXI_ARLEN : out STD_LOGIC_VECTOR ( 3 downto 0 );
    AXI_AWADDR : out STD_LOGIC_VECTOR ( 32 downto 0 );
    AXI_AWLEN : out STD_LOGIC_VECTOR ( 3 downto 0 );
    AXI_WDATA_PARITY : out STD_LOGIC_VECTOR ( 31 downto 0 );
    FIFO_WRITE_tlast : out STD_LOGIC;
    AXI_AWVALID_reg_0 : out STD_LOGIC;
    AXI_ARVALID_reg_0 : out STD_LOGIC;
    AXI_WLAST : out STD_LOGIC;
    busy : out STD_LOGIC;
    clk : in STD_LOGIC;
    wr_blockamount : in STD_LOGIC_VECTOR ( 31 downto 0 );
    rd_blockamount : in STD_LOGIC_VECTOR ( 31 downto 0 );
    rd_startaddress : in STD_LOGIC_VECTOR ( 31 downto 0 );
    wr_startaddress : in STD_LOGIC_VECTOR ( 31 downto 0 );
    FIFO_READ_tdata : in STD_LOGIC_VECTOR ( 255 downto 0 );
    AXI_RVALID : in STD_LOGIC;
    FIFO_WRITE_tready : in STD_LOGIC;
    AXI_AWREADY : in STD_LOGIC;
    FIFO_READ_tvalid : in STD_LOGIC;
    AXI_WREADY : in STD_LOGIC;
    AXI_ARREADY : in STD_LOGIC;
    rd_running : in STD_LOGIC;
    wr_running : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_hbm_channel_channel_ctrl_v5_0_0_channel_ctrl_v5 : entity is "channel_ctrl_v5";
end design_hbm_channel_channel_ctrl_v5_0_0_channel_ctrl_v5;

architecture STRUCTURE of design_hbm_channel_channel_ctrl_v5_0_0_channel_ctrl_v5 is
  signal AXI_ARADDR0 : STD_LOGIC;
  signal AXI_ARADDR00_in : STD_LOGIC_VECTOR ( 32 downto 4 );
  signal \AXI_ARADDR[11]_i_2_n_0\ : STD_LOGIC;
  signal \AXI_ARADDR[11]_i_3_n_0\ : STD_LOGIC;
  signal \AXI_ARADDR[11]_i_4_n_0\ : STD_LOGIC;
  signal \AXI_ARADDR[11]_i_5_n_0\ : STD_LOGIC;
  signal \AXI_ARADDR[11]_i_6_n_0\ : STD_LOGIC;
  signal \AXI_ARADDR[11]_i_7_n_0\ : STD_LOGIC;
  signal \AXI_ARADDR[11]_i_8_n_0\ : STD_LOGIC;
  signal \AXI_ARADDR[11]_i_9_n_0\ : STD_LOGIC;
  signal \AXI_ARADDR[19]_i_2_n_0\ : STD_LOGIC;
  signal \AXI_ARADDR[19]_i_3_n_0\ : STD_LOGIC;
  signal \AXI_ARADDR[19]_i_4_n_0\ : STD_LOGIC;
  signal \AXI_ARADDR[19]_i_5_n_0\ : STD_LOGIC;
  signal \AXI_ARADDR[19]_i_6_n_0\ : STD_LOGIC;
  signal \AXI_ARADDR[19]_i_7_n_0\ : STD_LOGIC;
  signal \AXI_ARADDR[19]_i_8_n_0\ : STD_LOGIC;
  signal \AXI_ARADDR[19]_i_9_n_0\ : STD_LOGIC;
  signal \AXI_ARADDR[27]_i_2_n_0\ : STD_LOGIC;
  signal \AXI_ARADDR[27]_i_3_n_0\ : STD_LOGIC;
  signal \AXI_ARADDR[27]_i_4_n_0\ : STD_LOGIC;
  signal \AXI_ARADDR[27]_i_5_n_0\ : STD_LOGIC;
  signal \AXI_ARADDR[27]_i_6_n_0\ : STD_LOGIC;
  signal \AXI_ARADDR[27]_i_7_n_0\ : STD_LOGIC;
  signal \AXI_ARADDR[27]_i_8_n_0\ : STD_LOGIC;
  signal \AXI_ARADDR[27]_i_9_n_0\ : STD_LOGIC;
  signal \AXI_ARADDR[32]_i_2_n_0\ : STD_LOGIC;
  signal \AXI_ARADDR[32]_i_4_n_0\ : STD_LOGIC;
  signal \AXI_ARADDR[32]_i_5_n_0\ : STD_LOGIC;
  signal \AXI_ARADDR[32]_i_6_n_0\ : STD_LOGIC;
  signal \AXI_ARADDR[32]_i_7_n_0\ : STD_LOGIC;
  signal \AXI_ARADDR_reg[11]_i_1_n_0\ : STD_LOGIC;
  signal \AXI_ARADDR_reg[11]_i_1_n_1\ : STD_LOGIC;
  signal \AXI_ARADDR_reg[11]_i_1_n_2\ : STD_LOGIC;
  signal \AXI_ARADDR_reg[11]_i_1_n_3\ : STD_LOGIC;
  signal \AXI_ARADDR_reg[11]_i_1_n_4\ : STD_LOGIC;
  signal \AXI_ARADDR_reg[11]_i_1_n_5\ : STD_LOGIC;
  signal \AXI_ARADDR_reg[11]_i_1_n_6\ : STD_LOGIC;
  signal \AXI_ARADDR_reg[11]_i_1_n_7\ : STD_LOGIC;
  signal \AXI_ARADDR_reg[19]_i_1_n_0\ : STD_LOGIC;
  signal \AXI_ARADDR_reg[19]_i_1_n_1\ : STD_LOGIC;
  signal \AXI_ARADDR_reg[19]_i_1_n_2\ : STD_LOGIC;
  signal \AXI_ARADDR_reg[19]_i_1_n_3\ : STD_LOGIC;
  signal \AXI_ARADDR_reg[19]_i_1_n_4\ : STD_LOGIC;
  signal \AXI_ARADDR_reg[19]_i_1_n_5\ : STD_LOGIC;
  signal \AXI_ARADDR_reg[19]_i_1_n_6\ : STD_LOGIC;
  signal \AXI_ARADDR_reg[19]_i_1_n_7\ : STD_LOGIC;
  signal \AXI_ARADDR_reg[27]_i_1_n_0\ : STD_LOGIC;
  signal \AXI_ARADDR_reg[27]_i_1_n_1\ : STD_LOGIC;
  signal \AXI_ARADDR_reg[27]_i_1_n_2\ : STD_LOGIC;
  signal \AXI_ARADDR_reg[27]_i_1_n_3\ : STD_LOGIC;
  signal \AXI_ARADDR_reg[27]_i_1_n_4\ : STD_LOGIC;
  signal \AXI_ARADDR_reg[27]_i_1_n_5\ : STD_LOGIC;
  signal \AXI_ARADDR_reg[27]_i_1_n_6\ : STD_LOGIC;
  signal \AXI_ARADDR_reg[27]_i_1_n_7\ : STD_LOGIC;
  signal \AXI_ARADDR_reg[32]_i_3_n_5\ : STD_LOGIC;
  signal \AXI_ARADDR_reg[32]_i_3_n_6\ : STD_LOGIC;
  signal \AXI_ARADDR_reg[32]_i_3_n_7\ : STD_LOGIC;
  signal \AXI_ARLEN[0]_i_1_n_0\ : STD_LOGIC;
  signal \AXI_ARLEN[1]_i_1_n_0\ : STD_LOGIC;
  signal \AXI_ARLEN[2]_i_1_n_0\ : STD_LOGIC;
  signal \AXI_ARLEN[3]_i_1_n_0\ : STD_LOGIC;
  signal \AXI_ARLEN[3]_i_2_n_0\ : STD_LOGIC;
  signal AXI_ARVALID_i_1_n_0 : STD_LOGIC;
  signal \^axi_arvalid_reg_0\ : STD_LOGIC;
  signal AXI_AWADDR0 : STD_LOGIC_VECTOR ( 32 downto 4 );
  signal \AXI_AWADDR[11]_i_2_n_0\ : STD_LOGIC;
  signal \AXI_AWADDR[11]_i_3_n_0\ : STD_LOGIC;
  signal \AXI_AWADDR[11]_i_4_n_0\ : STD_LOGIC;
  signal \AXI_AWADDR[11]_i_5_n_0\ : STD_LOGIC;
  signal \AXI_AWADDR[11]_i_6_n_0\ : STD_LOGIC;
  signal \AXI_AWADDR[11]_i_7_n_0\ : STD_LOGIC;
  signal \AXI_AWADDR[11]_i_8_n_0\ : STD_LOGIC;
  signal \AXI_AWADDR[11]_i_9_n_0\ : STD_LOGIC;
  signal \AXI_AWADDR[19]_i_2_n_0\ : STD_LOGIC;
  signal \AXI_AWADDR[19]_i_3_n_0\ : STD_LOGIC;
  signal \AXI_AWADDR[19]_i_4_n_0\ : STD_LOGIC;
  signal \AXI_AWADDR[19]_i_5_n_0\ : STD_LOGIC;
  signal \AXI_AWADDR[19]_i_6_n_0\ : STD_LOGIC;
  signal \AXI_AWADDR[19]_i_7_n_0\ : STD_LOGIC;
  signal \AXI_AWADDR[19]_i_8_n_0\ : STD_LOGIC;
  signal \AXI_AWADDR[19]_i_9_n_0\ : STD_LOGIC;
  signal \AXI_AWADDR[27]_i_2_n_0\ : STD_LOGIC;
  signal \AXI_AWADDR[27]_i_3_n_0\ : STD_LOGIC;
  signal \AXI_AWADDR[27]_i_4_n_0\ : STD_LOGIC;
  signal \AXI_AWADDR[27]_i_5_n_0\ : STD_LOGIC;
  signal \AXI_AWADDR[27]_i_6_n_0\ : STD_LOGIC;
  signal \AXI_AWADDR[27]_i_7_n_0\ : STD_LOGIC;
  signal \AXI_AWADDR[27]_i_8_n_0\ : STD_LOGIC;
  signal \AXI_AWADDR[27]_i_9_n_0\ : STD_LOGIC;
  signal \AXI_AWADDR[32]_i_2_n_0\ : STD_LOGIC;
  signal \AXI_AWADDR[32]_i_4_n_0\ : STD_LOGIC;
  signal \AXI_AWADDR[32]_i_5_n_0\ : STD_LOGIC;
  signal \AXI_AWADDR[32]_i_6_n_0\ : STD_LOGIC;
  signal \AXI_AWADDR[32]_i_7_n_0\ : STD_LOGIC;
  signal \AXI_AWADDR_reg[11]_i_1_n_0\ : STD_LOGIC;
  signal \AXI_AWADDR_reg[11]_i_1_n_1\ : STD_LOGIC;
  signal \AXI_AWADDR_reg[11]_i_1_n_2\ : STD_LOGIC;
  signal \AXI_AWADDR_reg[11]_i_1_n_3\ : STD_LOGIC;
  signal \AXI_AWADDR_reg[11]_i_1_n_4\ : STD_LOGIC;
  signal \AXI_AWADDR_reg[11]_i_1_n_5\ : STD_LOGIC;
  signal \AXI_AWADDR_reg[11]_i_1_n_6\ : STD_LOGIC;
  signal \AXI_AWADDR_reg[11]_i_1_n_7\ : STD_LOGIC;
  signal \AXI_AWADDR_reg[19]_i_1_n_0\ : STD_LOGIC;
  signal \AXI_AWADDR_reg[19]_i_1_n_1\ : STD_LOGIC;
  signal \AXI_AWADDR_reg[19]_i_1_n_2\ : STD_LOGIC;
  signal \AXI_AWADDR_reg[19]_i_1_n_3\ : STD_LOGIC;
  signal \AXI_AWADDR_reg[19]_i_1_n_4\ : STD_LOGIC;
  signal \AXI_AWADDR_reg[19]_i_1_n_5\ : STD_LOGIC;
  signal \AXI_AWADDR_reg[19]_i_1_n_6\ : STD_LOGIC;
  signal \AXI_AWADDR_reg[19]_i_1_n_7\ : STD_LOGIC;
  signal \AXI_AWADDR_reg[27]_i_1_n_0\ : STD_LOGIC;
  signal \AXI_AWADDR_reg[27]_i_1_n_1\ : STD_LOGIC;
  signal \AXI_AWADDR_reg[27]_i_1_n_2\ : STD_LOGIC;
  signal \AXI_AWADDR_reg[27]_i_1_n_3\ : STD_LOGIC;
  signal \AXI_AWADDR_reg[27]_i_1_n_4\ : STD_LOGIC;
  signal \AXI_AWADDR_reg[27]_i_1_n_5\ : STD_LOGIC;
  signal \AXI_AWADDR_reg[27]_i_1_n_6\ : STD_LOGIC;
  signal \AXI_AWADDR_reg[27]_i_1_n_7\ : STD_LOGIC;
  signal \AXI_AWADDR_reg[32]_i_3_n_5\ : STD_LOGIC;
  signal \AXI_AWADDR_reg[32]_i_3_n_6\ : STD_LOGIC;
  signal \AXI_AWADDR_reg[32]_i_3_n_7\ : STD_LOGIC;
  signal \AXI_AWLEN[0]_i_1_n_0\ : STD_LOGIC;
  signal \AXI_AWLEN[1]_i_1_n_0\ : STD_LOGIC;
  signal \AXI_AWLEN[2]_i_1_n_0\ : STD_LOGIC;
  signal \AXI_AWLEN[3]_i_1_n_0\ : STD_LOGIC;
  signal \AXI_AWLEN[3]_i_2_n_0\ : STD_LOGIC;
  signal AXI_AWVALID_i_1_n_0 : STD_LOGIC;
  signal \^axi_awvalid_reg_0\ : STD_LOGIC;
  signal \AXI_WDATA_PARITY0_inferred__0/AXI_WDATA_PARITY[1]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \AXI_WDATA_PARITY0_inferred__1/AXI_WDATA_PARITY[2]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \AXI_WDATA_PARITY0_inferred__10/AXI_WDATA_PARITY[11]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \AXI_WDATA_PARITY0_inferred__11/AXI_WDATA_PARITY[12]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \AXI_WDATA_PARITY0_inferred__12/AXI_WDATA_PARITY[13]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \AXI_WDATA_PARITY0_inferred__13/AXI_WDATA_PARITY[14]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \AXI_WDATA_PARITY0_inferred__14/AXI_WDATA_PARITY[15]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \AXI_WDATA_PARITY0_inferred__15/AXI_WDATA_PARITY[16]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \AXI_WDATA_PARITY0_inferred__16/AXI_WDATA_PARITY[17]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \AXI_WDATA_PARITY0_inferred__17/AXI_WDATA_PARITY[18]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \AXI_WDATA_PARITY0_inferred__18/AXI_WDATA_PARITY[19]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \AXI_WDATA_PARITY0_inferred__19/AXI_WDATA_PARITY[20]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \AXI_WDATA_PARITY0_inferred__2/AXI_WDATA_PARITY[3]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \AXI_WDATA_PARITY0_inferred__20/AXI_WDATA_PARITY[21]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \AXI_WDATA_PARITY0_inferred__21/AXI_WDATA_PARITY[22]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \AXI_WDATA_PARITY0_inferred__22/AXI_WDATA_PARITY[23]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \AXI_WDATA_PARITY0_inferred__23/AXI_WDATA_PARITY[24]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \AXI_WDATA_PARITY0_inferred__24/AXI_WDATA_PARITY[25]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \AXI_WDATA_PARITY0_inferred__25/AXI_WDATA_PARITY[26]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \AXI_WDATA_PARITY0_inferred__26/AXI_WDATA_PARITY[27]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \AXI_WDATA_PARITY0_inferred__27/AXI_WDATA_PARITY[28]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \AXI_WDATA_PARITY0_inferred__28/AXI_WDATA_PARITY[29]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \AXI_WDATA_PARITY0_inferred__29/AXI_WDATA_PARITY[30]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \AXI_WDATA_PARITY0_inferred__3/AXI_WDATA_PARITY[4]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \AXI_WDATA_PARITY0_inferred__30/AXI_WDATA_PARITY[31]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \AXI_WDATA_PARITY0_inferred__4/AXI_WDATA_PARITY[5]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \AXI_WDATA_PARITY0_inferred__5/AXI_WDATA_PARITY[6]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \AXI_WDATA_PARITY0_inferred__6/AXI_WDATA_PARITY[7]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \AXI_WDATA_PARITY0_inferred__7/AXI_WDATA_PARITY[8]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \AXI_WDATA_PARITY0_inferred__8/AXI_WDATA_PARITY[9]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \AXI_WDATA_PARITY0_inferred__9/AXI_WDATA_PARITY[10]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \AXI_WDATA_PARITY[0]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal FIFO_WRITE_tlast0 : STD_LOGIC;
  signal \FIFO_WRITE_tlast0_carry__0_i_10_n_0\ : STD_LOGIC;
  signal \FIFO_WRITE_tlast0_carry__0_i_11_n_0\ : STD_LOGIC;
  signal \FIFO_WRITE_tlast0_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \FIFO_WRITE_tlast0_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \FIFO_WRITE_tlast0_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \FIFO_WRITE_tlast0_carry__0_i_4_n_2\ : STD_LOGIC;
  signal \FIFO_WRITE_tlast0_carry__0_i_4_n_3\ : STD_LOGIC;
  signal \FIFO_WRITE_tlast0_carry__0_i_4_n_4\ : STD_LOGIC;
  signal \FIFO_WRITE_tlast0_carry__0_i_4_n_5\ : STD_LOGIC;
  signal \FIFO_WRITE_tlast0_carry__0_i_4_n_6\ : STD_LOGIC;
  signal \FIFO_WRITE_tlast0_carry__0_i_4_n_7\ : STD_LOGIC;
  signal \FIFO_WRITE_tlast0_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \FIFO_WRITE_tlast0_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \FIFO_WRITE_tlast0_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \FIFO_WRITE_tlast0_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \FIFO_WRITE_tlast0_carry__0_i_9_n_0\ : STD_LOGIC;
  signal \FIFO_WRITE_tlast0_carry__0_n_6\ : STD_LOGIC;
  signal \FIFO_WRITE_tlast0_carry__0_n_7\ : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_10_n_0 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_10_n_1 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_10_n_2 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_10_n_3 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_10_n_4 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_10_n_5 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_10_n_6 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_10_n_7 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_11_n_0 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_11_n_1 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_11_n_2 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_11_n_3 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_11_n_4 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_11_n_5 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_11_n_6 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_11_n_7 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_12_n_0 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_13_n_0 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_14_n_0 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_15_n_0 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_16_n_0 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_17_n_0 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_18_n_0 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_19_n_0 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_1_n_0 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_20_n_0 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_21_n_0 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_22_n_0 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_23_n_0 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_24_n_0 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_25_n_0 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_26_n_0 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_27_n_0 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_28_n_0 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_29_n_0 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_2_n_0 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_30_n_0 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_31_n_0 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_32_n_0 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_33_n_0 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_34_n_0 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_35_n_0 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_3_n_0 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_4_n_0 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_5_n_0 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_6_n_0 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_7_n_0 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_8_n_0 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_9_n_0 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_9_n_1 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_9_n_2 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_9_n_3 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_9_n_4 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_9_n_5 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_9_n_6 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_i_9_n_7 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_n_0 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_n_1 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_n_2 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_n_3 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_n_4 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_n_5 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_n_6 : STD_LOGIC;
  signal FIFO_WRITE_tlast0_carry_n_7 : STD_LOGIC;
  signal FIFO_WRITE_tlast1 : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal FSM : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal FSM_next : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \FSM_next__0\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \FSM_sequential_FSM_next_reg[2]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_FSM_next_reg[2]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_readaddr_FSM_next_reg[1]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_readaddr_FSM_next_reg[1]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_readaddr_FSM_next_reg[1]_i_4_n_0\ : STD_LOGIC;
  signal \FSM_sequential_readaddr_FSM_next_reg[1]_i_5_n_0\ : STD_LOGIC;
  signal \FSM_sequential_readaddr_FSM_next_reg[1]_i_6_n_0\ : STD_LOGIC;
  signal \FSM_sequential_readaddr_FSM_next_reg[1]_i_7_n_0\ : STD_LOGIC;
  signal \FSM_sequential_readaddr_FSM_next_reg[1]_i_8_n_0\ : STD_LOGIC;
  signal \FSM_sequential_readaddr_FSM_next_reg[1]_i_9_n_0\ : STD_LOGIC;
  signal \FSM_sequential_readaddr_FSM_next_reg[2]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_readaddr_FSM_next_reg[2]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_readaddr_FSM_next_reg_n_0_[1]\ : STD_LOGIC;
  signal \FSM_sequential_readdata_FSM_next_reg[2]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_readdata_FSM_next_reg[2]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_readdata_FSM_next_reg_n_0_[1]\ : STD_LOGIC;
  signal \FSM_sequential_writeaddr_FSM_next_reg[1]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_writeaddr_FSM_next_reg[1]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_writeaddr_FSM_next_reg[1]_i_4_n_0\ : STD_LOGIC;
  signal \FSM_sequential_writeaddr_FSM_next_reg[1]_i_5_n_0\ : STD_LOGIC;
  signal \FSM_sequential_writeaddr_FSM_next_reg[1]_i_6_n_0\ : STD_LOGIC;
  signal \FSM_sequential_writeaddr_FSM_next_reg[1]_i_7_n_0\ : STD_LOGIC;
  signal \FSM_sequential_writeaddr_FSM_next_reg[1]_i_8_n_0\ : STD_LOGIC;
  signal \FSM_sequential_writeaddr_FSM_next_reg[1]_i_9_n_0\ : STD_LOGIC;
  signal \FSM_sequential_writeaddr_FSM_next_reg[2]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_writeaddr_FSM_next_reg[2]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_writeaddr_FSM_next_reg_n_0_[1]\ : STD_LOGIC;
  signal \FSM_sequential_writedata_FSM_next_reg[2]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_writedata_FSM_next_reg[2]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_writedata_FSM_next_reg_n_0_[1]\ : STD_LOGIC;
  signal \^busy\ : STD_LOGIC;
  signal busy_i_1_n_0 : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 31 downto 4 );
  signal read_blockamount : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal read_start_i_1_n_0 : STD_LOGIC;
  signal read_start_reg_n_0 : STD_LOGIC;
  signal read_startaddress : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal read_startaddress_1 : STD_LOGIC;
  signal readaddr_FSM : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal readaddr_FSM_next : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal readaddr_FSM_next17_in : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_i_10_n_0\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_i_11_n_0\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_i_12_n_0\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_i_13_n_0\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_i_14_n_0\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_i_15_n_11\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_i_15_n_12\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_i_15_n_13\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_i_15_n_14\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_i_15_n_15\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_i_15_n_4\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_i_15_n_5\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_i_15_n_6\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_i_15_n_7\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_i_16_n_0\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_i_16_n_1\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_i_16_n_10\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_i_16_n_11\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_i_16_n_12\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_i_16_n_13\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_i_16_n_14\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_i_16_n_15\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_i_16_n_2\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_i_16_n_3\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_i_16_n_4\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_i_16_n_5\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_i_16_n_6\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_i_16_n_7\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_i_16_n_8\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_i_16_n_9\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_i_9_n_0\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_n_2\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_n_3\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_n_4\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_n_5\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_n_6\ : STD_LOGIC;
  signal \readaddr_FSM_next1_carry__0_n_7\ : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_10_n_0 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_11_n_0 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_12_n_0 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_13_n_0 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_14_n_0 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_15_n_0 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_16_n_0 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_17_n_0 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_17_n_1 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_17_n_10 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_17_n_11 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_17_n_12 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_17_n_13 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_17_n_14 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_17_n_15 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_17_n_2 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_17_n_3 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_17_n_4 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_17_n_5 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_17_n_6 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_17_n_7 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_17_n_8 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_17_n_9 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_18_n_0 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_18_n_1 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_18_n_10 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_18_n_11 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_18_n_12 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_18_n_13 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_18_n_14 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_18_n_2 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_18_n_3 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_18_n_4 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_18_n_5 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_18_n_6 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_18_n_7 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_18_n_8 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_18_n_9 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_19_n_0 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_1_n_0 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_2_n_0 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_3_n_0 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_4_n_0 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_5_n_0 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_6_n_0 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_7_n_0 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_8_n_0 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_i_9_n_0 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_n_0 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_n_1 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_n_2 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_n_3 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_n_4 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_n_5 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_n_6 : STD_LOGIC;
  signal readaddr_FSM_next1_carry_n_7 : STD_LOGIC;
  signal \readaddr_FSM_next__0\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal readaddr_count : STD_LOGIC;
  signal \readaddr_count[4]_i_3_n_0\ : STD_LOGIC;
  signal readaddr_count_reg : STD_LOGIC_VECTOR ( 31 downto 4 );
  signal \readaddr_count_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \readaddr_count_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \readaddr_count_reg[12]_i_1_n_10\ : STD_LOGIC;
  signal \readaddr_count_reg[12]_i_1_n_11\ : STD_LOGIC;
  signal \readaddr_count_reg[12]_i_1_n_12\ : STD_LOGIC;
  signal \readaddr_count_reg[12]_i_1_n_13\ : STD_LOGIC;
  signal \readaddr_count_reg[12]_i_1_n_14\ : STD_LOGIC;
  signal \readaddr_count_reg[12]_i_1_n_15\ : STD_LOGIC;
  signal \readaddr_count_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \readaddr_count_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \readaddr_count_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \readaddr_count_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \readaddr_count_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \readaddr_count_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \readaddr_count_reg[12]_i_1_n_8\ : STD_LOGIC;
  signal \readaddr_count_reg[12]_i_1_n_9\ : STD_LOGIC;
  signal \readaddr_count_reg[20]_i_1_n_0\ : STD_LOGIC;
  signal \readaddr_count_reg[20]_i_1_n_1\ : STD_LOGIC;
  signal \readaddr_count_reg[20]_i_1_n_10\ : STD_LOGIC;
  signal \readaddr_count_reg[20]_i_1_n_11\ : STD_LOGIC;
  signal \readaddr_count_reg[20]_i_1_n_12\ : STD_LOGIC;
  signal \readaddr_count_reg[20]_i_1_n_13\ : STD_LOGIC;
  signal \readaddr_count_reg[20]_i_1_n_14\ : STD_LOGIC;
  signal \readaddr_count_reg[20]_i_1_n_15\ : STD_LOGIC;
  signal \readaddr_count_reg[20]_i_1_n_2\ : STD_LOGIC;
  signal \readaddr_count_reg[20]_i_1_n_3\ : STD_LOGIC;
  signal \readaddr_count_reg[20]_i_1_n_4\ : STD_LOGIC;
  signal \readaddr_count_reg[20]_i_1_n_5\ : STD_LOGIC;
  signal \readaddr_count_reg[20]_i_1_n_6\ : STD_LOGIC;
  signal \readaddr_count_reg[20]_i_1_n_7\ : STD_LOGIC;
  signal \readaddr_count_reg[20]_i_1_n_8\ : STD_LOGIC;
  signal \readaddr_count_reg[20]_i_1_n_9\ : STD_LOGIC;
  signal \readaddr_count_reg[28]_i_1_n_12\ : STD_LOGIC;
  signal \readaddr_count_reg[28]_i_1_n_13\ : STD_LOGIC;
  signal \readaddr_count_reg[28]_i_1_n_14\ : STD_LOGIC;
  signal \readaddr_count_reg[28]_i_1_n_15\ : STD_LOGIC;
  signal \readaddr_count_reg[28]_i_1_n_5\ : STD_LOGIC;
  signal \readaddr_count_reg[28]_i_1_n_6\ : STD_LOGIC;
  signal \readaddr_count_reg[28]_i_1_n_7\ : STD_LOGIC;
  signal \readaddr_count_reg[4]_i_2_n_0\ : STD_LOGIC;
  signal \readaddr_count_reg[4]_i_2_n_1\ : STD_LOGIC;
  signal \readaddr_count_reg[4]_i_2_n_10\ : STD_LOGIC;
  signal \readaddr_count_reg[4]_i_2_n_11\ : STD_LOGIC;
  signal \readaddr_count_reg[4]_i_2_n_12\ : STD_LOGIC;
  signal \readaddr_count_reg[4]_i_2_n_13\ : STD_LOGIC;
  signal \readaddr_count_reg[4]_i_2_n_14\ : STD_LOGIC;
  signal \readaddr_count_reg[4]_i_2_n_15\ : STD_LOGIC;
  signal \readaddr_count_reg[4]_i_2_n_2\ : STD_LOGIC;
  signal \readaddr_count_reg[4]_i_2_n_3\ : STD_LOGIC;
  signal \readaddr_count_reg[4]_i_2_n_4\ : STD_LOGIC;
  signal \readaddr_count_reg[4]_i_2_n_5\ : STD_LOGIC;
  signal \readaddr_count_reg[4]_i_2_n_6\ : STD_LOGIC;
  signal \readaddr_count_reg[4]_i_2_n_7\ : STD_LOGIC;
  signal \readaddr_count_reg[4]_i_2_n_8\ : STD_LOGIC;
  signal \readaddr_count_reg[4]_i_2_n_9\ : STD_LOGIC;
  signal readaddr_done_i_1_n_0 : STD_LOGIC;
  signal readaddr_done_reg_n_0 : STD_LOGIC;
  signal readdata_FSM : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal readdata_FSM_next : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \readdata_FSM_next__0\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal readdata_count : STD_LOGIC;
  signal readdata_count0 : STD_LOGIC;
  signal \readdata_count[0]_i_4_n_0\ : STD_LOGIC;
  signal readdata_count_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \readdata_count_reg[0]_i_3_n_0\ : STD_LOGIC;
  signal \readdata_count_reg[0]_i_3_n_1\ : STD_LOGIC;
  signal \readdata_count_reg[0]_i_3_n_10\ : STD_LOGIC;
  signal \readdata_count_reg[0]_i_3_n_11\ : STD_LOGIC;
  signal \readdata_count_reg[0]_i_3_n_12\ : STD_LOGIC;
  signal \readdata_count_reg[0]_i_3_n_13\ : STD_LOGIC;
  signal \readdata_count_reg[0]_i_3_n_14\ : STD_LOGIC;
  signal \readdata_count_reg[0]_i_3_n_15\ : STD_LOGIC;
  signal \readdata_count_reg[0]_i_3_n_2\ : STD_LOGIC;
  signal \readdata_count_reg[0]_i_3_n_3\ : STD_LOGIC;
  signal \readdata_count_reg[0]_i_3_n_4\ : STD_LOGIC;
  signal \readdata_count_reg[0]_i_3_n_5\ : STD_LOGIC;
  signal \readdata_count_reg[0]_i_3_n_6\ : STD_LOGIC;
  signal \readdata_count_reg[0]_i_3_n_7\ : STD_LOGIC;
  signal \readdata_count_reg[0]_i_3_n_8\ : STD_LOGIC;
  signal \readdata_count_reg[0]_i_3_n_9\ : STD_LOGIC;
  signal \readdata_count_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \readdata_count_reg[16]_i_1_n_1\ : STD_LOGIC;
  signal \readdata_count_reg[16]_i_1_n_10\ : STD_LOGIC;
  signal \readdata_count_reg[16]_i_1_n_11\ : STD_LOGIC;
  signal \readdata_count_reg[16]_i_1_n_12\ : STD_LOGIC;
  signal \readdata_count_reg[16]_i_1_n_13\ : STD_LOGIC;
  signal \readdata_count_reg[16]_i_1_n_14\ : STD_LOGIC;
  signal \readdata_count_reg[16]_i_1_n_15\ : STD_LOGIC;
  signal \readdata_count_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \readdata_count_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \readdata_count_reg[16]_i_1_n_4\ : STD_LOGIC;
  signal \readdata_count_reg[16]_i_1_n_5\ : STD_LOGIC;
  signal \readdata_count_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \readdata_count_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \readdata_count_reg[16]_i_1_n_8\ : STD_LOGIC;
  signal \readdata_count_reg[16]_i_1_n_9\ : STD_LOGIC;
  signal \readdata_count_reg[24]_i_1_n_1\ : STD_LOGIC;
  signal \readdata_count_reg[24]_i_1_n_10\ : STD_LOGIC;
  signal \readdata_count_reg[24]_i_1_n_11\ : STD_LOGIC;
  signal \readdata_count_reg[24]_i_1_n_12\ : STD_LOGIC;
  signal \readdata_count_reg[24]_i_1_n_13\ : STD_LOGIC;
  signal \readdata_count_reg[24]_i_1_n_14\ : STD_LOGIC;
  signal \readdata_count_reg[24]_i_1_n_15\ : STD_LOGIC;
  signal \readdata_count_reg[24]_i_1_n_2\ : STD_LOGIC;
  signal \readdata_count_reg[24]_i_1_n_3\ : STD_LOGIC;
  signal \readdata_count_reg[24]_i_1_n_4\ : STD_LOGIC;
  signal \readdata_count_reg[24]_i_1_n_5\ : STD_LOGIC;
  signal \readdata_count_reg[24]_i_1_n_6\ : STD_LOGIC;
  signal \readdata_count_reg[24]_i_1_n_7\ : STD_LOGIC;
  signal \readdata_count_reg[24]_i_1_n_8\ : STD_LOGIC;
  signal \readdata_count_reg[24]_i_1_n_9\ : STD_LOGIC;
  signal \readdata_count_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \readdata_count_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \readdata_count_reg[8]_i_1_n_10\ : STD_LOGIC;
  signal \readdata_count_reg[8]_i_1_n_11\ : STD_LOGIC;
  signal \readdata_count_reg[8]_i_1_n_12\ : STD_LOGIC;
  signal \readdata_count_reg[8]_i_1_n_13\ : STD_LOGIC;
  signal \readdata_count_reg[8]_i_1_n_14\ : STD_LOGIC;
  signal \readdata_count_reg[8]_i_1_n_15\ : STD_LOGIC;
  signal \readdata_count_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \readdata_count_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \readdata_count_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \readdata_count_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \readdata_count_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \readdata_count_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal \readdata_count_reg[8]_i_1_n_8\ : STD_LOGIC;
  signal \readdata_count_reg[8]_i_1_n_9\ : STD_LOGIC;
  signal readdata_done_i_1_n_0 : STD_LOGIC;
  signal readdata_done_reg_n_0 : STD_LOGIC;
  signal write_blockamount : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal write_start_i_1_n_0 : STD_LOGIC;
  signal write_start_reg_n_0 : STD_LOGIC;
  signal write_startaddress : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal write_startaddress_0 : STD_LOGIC;
  signal writeaddr_FSM : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal writeaddr_FSM_next : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal writeaddr_FSM_next15_in : STD_LOGIC;
  signal \writeaddr_FSM_next1_carry__0_i_10_n_0\ : STD_LOGIC;
  signal \writeaddr_FSM_next1_carry__0_i_11_n_0\ : STD_LOGIC;
  signal \writeaddr_FSM_next1_carry__0_i_12_n_0\ : STD_LOGIC;
  signal \writeaddr_FSM_next1_carry__0_i_13_n_0\ : STD_LOGIC;
  signal \writeaddr_FSM_next1_carry__0_i_14_n_0\ : STD_LOGIC;
  signal \writeaddr_FSM_next1_carry__0_i_15_n_4\ : STD_LOGIC;
  signal \writeaddr_FSM_next1_carry__0_i_15_n_5\ : STD_LOGIC;
  signal \writeaddr_FSM_next1_carry__0_i_15_n_6\ : STD_LOGIC;
  signal \writeaddr_FSM_next1_carry__0_i_15_n_7\ : STD_LOGIC;
  signal \writeaddr_FSM_next1_carry__0_i_16_n_0\ : STD_LOGIC;
  signal \writeaddr_FSM_next1_carry__0_i_16_n_1\ : STD_LOGIC;
  signal \writeaddr_FSM_next1_carry__0_i_16_n_2\ : STD_LOGIC;
  signal \writeaddr_FSM_next1_carry__0_i_16_n_3\ : STD_LOGIC;
  signal \writeaddr_FSM_next1_carry__0_i_16_n_4\ : STD_LOGIC;
  signal \writeaddr_FSM_next1_carry__0_i_16_n_5\ : STD_LOGIC;
  signal \writeaddr_FSM_next1_carry__0_i_16_n_6\ : STD_LOGIC;
  signal \writeaddr_FSM_next1_carry__0_i_16_n_7\ : STD_LOGIC;
  signal \writeaddr_FSM_next1_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \writeaddr_FSM_next1_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \writeaddr_FSM_next1_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \writeaddr_FSM_next1_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \writeaddr_FSM_next1_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \writeaddr_FSM_next1_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \writeaddr_FSM_next1_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \writeaddr_FSM_next1_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \writeaddr_FSM_next1_carry__0_i_9_n_0\ : STD_LOGIC;
  signal \writeaddr_FSM_next1_carry__0_n_2\ : STD_LOGIC;
  signal \writeaddr_FSM_next1_carry__0_n_3\ : STD_LOGIC;
  signal \writeaddr_FSM_next1_carry__0_n_4\ : STD_LOGIC;
  signal \writeaddr_FSM_next1_carry__0_n_5\ : STD_LOGIC;
  signal \writeaddr_FSM_next1_carry__0_n_6\ : STD_LOGIC;
  signal \writeaddr_FSM_next1_carry__0_n_7\ : STD_LOGIC;
  signal writeaddr_FSM_next1_carry_i_10_n_0 : STD_LOGIC;
  signal writeaddr_FSM_next1_carry_i_11_n_0 : STD_LOGIC;
  signal writeaddr_FSM_next1_carry_i_12_n_0 : STD_LOGIC;
  signal writeaddr_FSM_next1_carry_i_13_n_0 : STD_LOGIC;
  signal writeaddr_FSM_next1_carry_i_14_n_0 : STD_LOGIC;
  signal writeaddr_FSM_next1_carry_i_15_n_0 : STD_LOGIC;
  signal writeaddr_FSM_next1_carry_i_16_n_0 : STD_LOGIC;
  signal writeaddr_FSM_next1_carry_i_17_n_0 : STD_LOGIC;
  signal writeaddr_FSM_next1_carry_i_17_n_1 : STD_LOGIC;
  signal writeaddr_FSM_next1_carry_i_17_n_2 : STD_LOGIC;
  signal writeaddr_FSM_next1_carry_i_17_n_3 : STD_LOGIC;
  signal writeaddr_FSM_next1_carry_i_17_n_4 : STD_LOGIC;
  signal writeaddr_FSM_next1_carry_i_17_n_5 : STD_LOGIC;
  signal writeaddr_FSM_next1_carry_i_17_n_6 : STD_LOGIC;
  signal writeaddr_FSM_next1_carry_i_17_n_7 : STD_LOGIC;
  signal writeaddr_FSM_next1_carry_i_18_n_0 : STD_LOGIC;
  signal writeaddr_FSM_next1_carry_i_18_n_1 : STD_LOGIC;
  signal writeaddr_FSM_next1_carry_i_18_n_2 : STD_LOGIC;
  signal writeaddr_FSM_next1_carry_i_18_n_3 : STD_LOGIC;
  signal writeaddr_FSM_next1_carry_i_18_n_4 : STD_LOGIC;
  signal writeaddr_FSM_next1_carry_i_18_n_5 : STD_LOGIC;
  signal writeaddr_FSM_next1_carry_i_18_n_6 : STD_LOGIC;
  signal writeaddr_FSM_next1_carry_i_18_n_7 : STD_LOGIC;
  signal writeaddr_FSM_next1_carry_i_19_n_0 : STD_LOGIC;
  signal writeaddr_FSM_next1_carry_i_1_n_0 : STD_LOGIC;
  signal writeaddr_FSM_next1_carry_i_2_n_0 : STD_LOGIC;
  signal writeaddr_FSM_next1_carry_i_3_n_0 : STD_LOGIC;
  signal writeaddr_FSM_next1_carry_i_4_n_0 : STD_LOGIC;
  signal writeaddr_FSM_next1_carry_i_5_n_0 : STD_LOGIC;
  signal writeaddr_FSM_next1_carry_i_6_n_0 : STD_LOGIC;
  signal writeaddr_FSM_next1_carry_i_7_n_0 : STD_LOGIC;
  signal writeaddr_FSM_next1_carry_i_8_n_0 : STD_LOGIC;
  signal writeaddr_FSM_next1_carry_i_9_n_0 : STD_LOGIC;
  signal writeaddr_FSM_next1_carry_n_0 : STD_LOGIC;
  signal writeaddr_FSM_next1_carry_n_1 : STD_LOGIC;
  signal writeaddr_FSM_next1_carry_n_2 : STD_LOGIC;
  signal writeaddr_FSM_next1_carry_n_3 : STD_LOGIC;
  signal writeaddr_FSM_next1_carry_n_4 : STD_LOGIC;
  signal writeaddr_FSM_next1_carry_n_5 : STD_LOGIC;
  signal writeaddr_FSM_next1_carry_n_6 : STD_LOGIC;
  signal writeaddr_FSM_next1_carry_n_7 : STD_LOGIC;
  signal \writeaddr_FSM_next__0\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal writeaddr_count : STD_LOGIC;
  signal writeaddr_count0 : STD_LOGIC;
  signal \writeaddr_count[4]_i_3_n_0\ : STD_LOGIC;
  signal writeaddr_count_reg : STD_LOGIC_VECTOR ( 31 downto 4 );
  signal \writeaddr_count_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \writeaddr_count_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \writeaddr_count_reg[12]_i_1_n_10\ : STD_LOGIC;
  signal \writeaddr_count_reg[12]_i_1_n_11\ : STD_LOGIC;
  signal \writeaddr_count_reg[12]_i_1_n_12\ : STD_LOGIC;
  signal \writeaddr_count_reg[12]_i_1_n_13\ : STD_LOGIC;
  signal \writeaddr_count_reg[12]_i_1_n_14\ : STD_LOGIC;
  signal \writeaddr_count_reg[12]_i_1_n_15\ : STD_LOGIC;
  signal \writeaddr_count_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \writeaddr_count_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \writeaddr_count_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \writeaddr_count_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \writeaddr_count_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \writeaddr_count_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \writeaddr_count_reg[12]_i_1_n_8\ : STD_LOGIC;
  signal \writeaddr_count_reg[12]_i_1_n_9\ : STD_LOGIC;
  signal \writeaddr_count_reg[20]_i_1_n_0\ : STD_LOGIC;
  signal \writeaddr_count_reg[20]_i_1_n_1\ : STD_LOGIC;
  signal \writeaddr_count_reg[20]_i_1_n_10\ : STD_LOGIC;
  signal \writeaddr_count_reg[20]_i_1_n_11\ : STD_LOGIC;
  signal \writeaddr_count_reg[20]_i_1_n_12\ : STD_LOGIC;
  signal \writeaddr_count_reg[20]_i_1_n_13\ : STD_LOGIC;
  signal \writeaddr_count_reg[20]_i_1_n_14\ : STD_LOGIC;
  signal \writeaddr_count_reg[20]_i_1_n_15\ : STD_LOGIC;
  signal \writeaddr_count_reg[20]_i_1_n_2\ : STD_LOGIC;
  signal \writeaddr_count_reg[20]_i_1_n_3\ : STD_LOGIC;
  signal \writeaddr_count_reg[20]_i_1_n_4\ : STD_LOGIC;
  signal \writeaddr_count_reg[20]_i_1_n_5\ : STD_LOGIC;
  signal \writeaddr_count_reg[20]_i_1_n_6\ : STD_LOGIC;
  signal \writeaddr_count_reg[20]_i_1_n_7\ : STD_LOGIC;
  signal \writeaddr_count_reg[20]_i_1_n_8\ : STD_LOGIC;
  signal \writeaddr_count_reg[20]_i_1_n_9\ : STD_LOGIC;
  signal \writeaddr_count_reg[28]_i_1_n_12\ : STD_LOGIC;
  signal \writeaddr_count_reg[28]_i_1_n_13\ : STD_LOGIC;
  signal \writeaddr_count_reg[28]_i_1_n_14\ : STD_LOGIC;
  signal \writeaddr_count_reg[28]_i_1_n_15\ : STD_LOGIC;
  signal \writeaddr_count_reg[28]_i_1_n_5\ : STD_LOGIC;
  signal \writeaddr_count_reg[28]_i_1_n_6\ : STD_LOGIC;
  signal \writeaddr_count_reg[28]_i_1_n_7\ : STD_LOGIC;
  signal \writeaddr_count_reg[4]_i_2_n_0\ : STD_LOGIC;
  signal \writeaddr_count_reg[4]_i_2_n_1\ : STD_LOGIC;
  signal \writeaddr_count_reg[4]_i_2_n_10\ : STD_LOGIC;
  signal \writeaddr_count_reg[4]_i_2_n_11\ : STD_LOGIC;
  signal \writeaddr_count_reg[4]_i_2_n_12\ : STD_LOGIC;
  signal \writeaddr_count_reg[4]_i_2_n_13\ : STD_LOGIC;
  signal \writeaddr_count_reg[4]_i_2_n_14\ : STD_LOGIC;
  signal \writeaddr_count_reg[4]_i_2_n_15\ : STD_LOGIC;
  signal \writeaddr_count_reg[4]_i_2_n_2\ : STD_LOGIC;
  signal \writeaddr_count_reg[4]_i_2_n_3\ : STD_LOGIC;
  signal \writeaddr_count_reg[4]_i_2_n_4\ : STD_LOGIC;
  signal \writeaddr_count_reg[4]_i_2_n_5\ : STD_LOGIC;
  signal \writeaddr_count_reg[4]_i_2_n_6\ : STD_LOGIC;
  signal \writeaddr_count_reg[4]_i_2_n_7\ : STD_LOGIC;
  signal \writeaddr_count_reg[4]_i_2_n_8\ : STD_LOGIC;
  signal \writeaddr_count_reg[4]_i_2_n_9\ : STD_LOGIC;
  signal writeaddr_done_i_1_n_0 : STD_LOGIC;
  signal writeaddr_done_reg_n_0 : STD_LOGIC;
  signal writedata_FSM : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal writedata_FSM_next : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal writedata_FSM_next1 : STD_LOGIC;
  signal \writedata_FSM_next1_carry__0_i_10_n_0\ : STD_LOGIC;
  signal \writedata_FSM_next1_carry__0_i_11_n_0\ : STD_LOGIC;
  signal \writedata_FSM_next1_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \writedata_FSM_next1_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \writedata_FSM_next1_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \writedata_FSM_next1_carry__0_i_4_n_2\ : STD_LOGIC;
  signal \writedata_FSM_next1_carry__0_i_4_n_3\ : STD_LOGIC;
  signal \writedata_FSM_next1_carry__0_i_4_n_4\ : STD_LOGIC;
  signal \writedata_FSM_next1_carry__0_i_4_n_5\ : STD_LOGIC;
  signal \writedata_FSM_next1_carry__0_i_4_n_6\ : STD_LOGIC;
  signal \writedata_FSM_next1_carry__0_i_4_n_7\ : STD_LOGIC;
  signal \writedata_FSM_next1_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \writedata_FSM_next1_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \writedata_FSM_next1_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \writedata_FSM_next1_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \writedata_FSM_next1_carry__0_i_9_n_0\ : STD_LOGIC;
  signal \writedata_FSM_next1_carry__0_n_6\ : STD_LOGIC;
  signal \writedata_FSM_next1_carry__0_n_7\ : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_10_n_0 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_10_n_1 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_10_n_2 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_10_n_3 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_10_n_4 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_10_n_5 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_10_n_6 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_10_n_7 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_11_n_0 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_11_n_1 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_11_n_2 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_11_n_3 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_11_n_4 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_11_n_5 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_11_n_6 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_11_n_7 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_12_n_0 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_13_n_0 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_14_n_0 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_15_n_0 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_16_n_0 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_17_n_0 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_18_n_0 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_19_n_0 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_1_n_0 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_20_n_0 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_21_n_0 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_22_n_0 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_23_n_0 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_24_n_0 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_25_n_0 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_26_n_0 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_27_n_0 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_28_n_0 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_29_n_0 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_2_n_0 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_30_n_0 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_31_n_0 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_32_n_0 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_33_n_0 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_34_n_0 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_35_n_0 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_3_n_0 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_4_n_0 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_5_n_0 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_6_n_0 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_7_n_0 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_8_n_0 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_9_n_0 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_9_n_1 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_9_n_2 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_9_n_3 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_9_n_4 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_9_n_5 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_9_n_6 : STD_LOGIC;
  signal writedata_FSM_next1_carry_i_9_n_7 : STD_LOGIC;
  signal writedata_FSM_next1_carry_n_0 : STD_LOGIC;
  signal writedata_FSM_next1_carry_n_1 : STD_LOGIC;
  signal writedata_FSM_next1_carry_n_2 : STD_LOGIC;
  signal writedata_FSM_next1_carry_n_3 : STD_LOGIC;
  signal writedata_FSM_next1_carry_n_4 : STD_LOGIC;
  signal writedata_FSM_next1_carry_n_5 : STD_LOGIC;
  signal writedata_FSM_next1_carry_n_6 : STD_LOGIC;
  signal writedata_FSM_next1_carry_n_7 : STD_LOGIC;
  signal writedata_FSM_next2 : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal \writedata_FSM_next__0\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal writedata_count : STD_LOGIC;
  signal writedata_count0 : STD_LOGIC;
  signal \writedata_count[0]_i_4_n_0\ : STD_LOGIC;
  signal writedata_count_reg : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \writedata_count_reg[0]_i_3_n_0\ : STD_LOGIC;
  signal \writedata_count_reg[0]_i_3_n_1\ : STD_LOGIC;
  signal \writedata_count_reg[0]_i_3_n_10\ : STD_LOGIC;
  signal \writedata_count_reg[0]_i_3_n_11\ : STD_LOGIC;
  signal \writedata_count_reg[0]_i_3_n_12\ : STD_LOGIC;
  signal \writedata_count_reg[0]_i_3_n_13\ : STD_LOGIC;
  signal \writedata_count_reg[0]_i_3_n_14\ : STD_LOGIC;
  signal \writedata_count_reg[0]_i_3_n_15\ : STD_LOGIC;
  signal \writedata_count_reg[0]_i_3_n_2\ : STD_LOGIC;
  signal \writedata_count_reg[0]_i_3_n_3\ : STD_LOGIC;
  signal \writedata_count_reg[0]_i_3_n_4\ : STD_LOGIC;
  signal \writedata_count_reg[0]_i_3_n_5\ : STD_LOGIC;
  signal \writedata_count_reg[0]_i_3_n_6\ : STD_LOGIC;
  signal \writedata_count_reg[0]_i_3_n_7\ : STD_LOGIC;
  signal \writedata_count_reg[0]_i_3_n_8\ : STD_LOGIC;
  signal \writedata_count_reg[0]_i_3_n_9\ : STD_LOGIC;
  signal \writedata_count_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \writedata_count_reg[16]_i_1_n_1\ : STD_LOGIC;
  signal \writedata_count_reg[16]_i_1_n_10\ : STD_LOGIC;
  signal \writedata_count_reg[16]_i_1_n_11\ : STD_LOGIC;
  signal \writedata_count_reg[16]_i_1_n_12\ : STD_LOGIC;
  signal \writedata_count_reg[16]_i_1_n_13\ : STD_LOGIC;
  signal \writedata_count_reg[16]_i_1_n_14\ : STD_LOGIC;
  signal \writedata_count_reg[16]_i_1_n_15\ : STD_LOGIC;
  signal \writedata_count_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \writedata_count_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \writedata_count_reg[16]_i_1_n_4\ : STD_LOGIC;
  signal \writedata_count_reg[16]_i_1_n_5\ : STD_LOGIC;
  signal \writedata_count_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \writedata_count_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \writedata_count_reg[16]_i_1_n_8\ : STD_LOGIC;
  signal \writedata_count_reg[16]_i_1_n_9\ : STD_LOGIC;
  signal \writedata_count_reg[24]_i_1_n_1\ : STD_LOGIC;
  signal \writedata_count_reg[24]_i_1_n_10\ : STD_LOGIC;
  signal \writedata_count_reg[24]_i_1_n_11\ : STD_LOGIC;
  signal \writedata_count_reg[24]_i_1_n_12\ : STD_LOGIC;
  signal \writedata_count_reg[24]_i_1_n_13\ : STD_LOGIC;
  signal \writedata_count_reg[24]_i_1_n_14\ : STD_LOGIC;
  signal \writedata_count_reg[24]_i_1_n_15\ : STD_LOGIC;
  signal \writedata_count_reg[24]_i_1_n_2\ : STD_LOGIC;
  signal \writedata_count_reg[24]_i_1_n_3\ : STD_LOGIC;
  signal \writedata_count_reg[24]_i_1_n_4\ : STD_LOGIC;
  signal \writedata_count_reg[24]_i_1_n_5\ : STD_LOGIC;
  signal \writedata_count_reg[24]_i_1_n_6\ : STD_LOGIC;
  signal \writedata_count_reg[24]_i_1_n_7\ : STD_LOGIC;
  signal \writedata_count_reg[24]_i_1_n_8\ : STD_LOGIC;
  signal \writedata_count_reg[24]_i_1_n_9\ : STD_LOGIC;
  signal \writedata_count_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \writedata_count_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \writedata_count_reg[8]_i_1_n_10\ : STD_LOGIC;
  signal \writedata_count_reg[8]_i_1_n_11\ : STD_LOGIC;
  signal \writedata_count_reg[8]_i_1_n_12\ : STD_LOGIC;
  signal \writedata_count_reg[8]_i_1_n_13\ : STD_LOGIC;
  signal \writedata_count_reg[8]_i_1_n_14\ : STD_LOGIC;
  signal \writedata_count_reg[8]_i_1_n_15\ : STD_LOGIC;
  signal \writedata_count_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \writedata_count_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \writedata_count_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \writedata_count_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \writedata_count_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \writedata_count_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal \writedata_count_reg[8]_i_1_n_8\ : STD_LOGIC;
  signal \writedata_count_reg[8]_i_1_n_9\ : STD_LOGIC;
  signal \writedata_count_reg__0\ : STD_LOGIC_VECTOR ( 31 downto 4 );
  signal writedata_done_i_1_n_0 : STD_LOGIC;
  signal writedata_done_reg_n_0 : STD_LOGIC;
  signal \NLW_AXI_ARADDR_reg[32]_i_3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 3 );
  signal \NLW_AXI_ARADDR_reg[32]_i_3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 4 );
  signal \NLW_AXI_AWADDR_reg[32]_i_3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 3 );
  signal \NLW_AXI_AWADDR_reg[32]_i_3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 4 );
  signal NLW_FIFO_WRITE_tlast0_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_FIFO_WRITE_tlast0_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 3 );
  signal \NLW_FIFO_WRITE_tlast0_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_FIFO_WRITE_tlast0_carry__0_i_4_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 6 );
  signal \NLW_FIFO_WRITE_tlast0_carry__0_i_4_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 to 7 );
  signal NLW_readaddr_FSM_next1_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_readaddr_FSM_next1_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 to 7 );
  signal \NLW_readaddr_FSM_next1_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_readaddr_FSM_next1_carry__0_i_15_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 4 );
  signal \NLW_readaddr_FSM_next1_carry__0_i_15_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 5 );
  signal NLW_readaddr_FSM_next1_carry_i_18_O_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_readaddr_count_reg[28]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 3 );
  signal \NLW_readaddr_count_reg[28]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 4 );
  signal \NLW_readdata_count_reg[24]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 to 7 );
  signal NLW_writeaddr_FSM_next1_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_writeaddr_FSM_next1_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 to 7 );
  signal \NLW_writeaddr_FSM_next1_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_writeaddr_FSM_next1_carry__0_i_15_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 4 );
  signal \NLW_writeaddr_FSM_next1_carry__0_i_15_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 5 );
  signal NLW_writeaddr_FSM_next1_carry_i_18_O_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_writeaddr_count_reg[28]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 3 );
  signal \NLW_writeaddr_count_reg[28]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 4 );
  signal NLW_writedata_FSM_next1_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_writedata_FSM_next1_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 3 );
  signal \NLW_writedata_FSM_next1_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_writedata_FSM_next1_carry__0_i_4_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 6 );
  signal \NLW_writedata_FSM_next1_carry__0_i_4_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 to 7 );
  signal \NLW_writedata_count_reg[24]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 to 7 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \AXI_ARLEN[1]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \AXI_ARLEN[2]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of AXI_ARVALID_i_1 : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \AXI_AWLEN[1]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \AXI_AWLEN[2]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of AXI_AWVALID_i_1 : label is "soft_lutpair7";
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of \FIFO_WRITE_tlast0_carry__0_i_4\ : label is 35;
  attribute ADDER_THRESHOLD of FIFO_WRITE_tlast0_carry_i_10 : label is 35;
  attribute ADDER_THRESHOLD of FIFO_WRITE_tlast0_carry_i_11 : label is 35;
  attribute ADDER_THRESHOLD of FIFO_WRITE_tlast0_carry_i_9 : label is 35;
  attribute SOFT_HLUTNM of FIFO_WRITE_tlast_INST_0 : label is "soft_lutpair9";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of \FSM_sequential_FSM_next_reg[0]\ : label is "LD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of \FSM_sequential_FSM_next_reg[0]\ : label is "VCC:GE GND:CLR";
  attribute SOFT_HLUTNM of \FSM_sequential_FSM_next_reg[0]_i_1\ : label is "soft_lutpair2";
  attribute XILINX_LEGACY_PRIM of \FSM_sequential_FSM_next_reg[1]\ : label is "LD";
  attribute XILINX_TRANSFORM_PINMAP of \FSM_sequential_FSM_next_reg[1]\ : label is "VCC:GE GND:CLR";
  attribute SOFT_HLUTNM of \FSM_sequential_FSM_next_reg[1]_i_1\ : label is "soft_lutpair2";
  attribute XILINX_LEGACY_PRIM of \FSM_sequential_FSM_next_reg[2]\ : label is "LD";
  attribute XILINX_TRANSFORM_PINMAP of \FSM_sequential_FSM_next_reg[2]\ : label is "VCC:GE GND:CLR";
  attribute SOFT_HLUTNM of \FSM_sequential_FSM_next_reg[2]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \FSM_sequential_FSM_next_reg[2]_i_2\ : label is "soft_lutpair5";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_sequential_FSM_reg[0]\ : label is "iSTATE:010,iSTATE0:100,iSTATE1:101,iSTATE2:011,iSTATE3:001,iSTATE4:000";
  attribute FSM_ENCODED_STATES of \FSM_sequential_FSM_reg[1]\ : label is "iSTATE:010,iSTATE0:100,iSTATE1:101,iSTATE2:011,iSTATE3:001,iSTATE4:000";
  attribute FSM_ENCODED_STATES of \FSM_sequential_FSM_reg[2]\ : label is "iSTATE:010,iSTATE0:100,iSTATE1:101,iSTATE2:011,iSTATE3:001,iSTATE4:000";
  attribute XILINX_LEGACY_PRIM of \FSM_sequential_readaddr_FSM_next_reg[0]\ : label is "LD";
  attribute XILINX_TRANSFORM_PINMAP of \FSM_sequential_readaddr_FSM_next_reg[0]\ : label is "VCC:GE GND:CLR";
  attribute XILINX_LEGACY_PRIM of \FSM_sequential_readaddr_FSM_next_reg[1]\ : label is "LD";
  attribute XILINX_TRANSFORM_PINMAP of \FSM_sequential_readaddr_FSM_next_reg[1]\ : label is "VCC:GE GND:CLR";
  attribute XILINX_LEGACY_PRIM of \FSM_sequential_readaddr_FSM_next_reg[2]\ : label is "LD";
  attribute XILINX_TRANSFORM_PINMAP of \FSM_sequential_readaddr_FSM_next_reg[2]\ : label is "VCC:GE GND:CLR";
  attribute FSM_ENCODED_STATES of \FSM_sequential_readaddr_FSM_reg[0]\ : label is "iSTATE:011,iSTATE0:001,iSTATE1:010,iSTATE2:000,iSTATE3:100";
  attribute FSM_ENCODED_STATES of \FSM_sequential_readaddr_FSM_reg[1]\ : label is "iSTATE:011,iSTATE0:001,iSTATE1:010,iSTATE2:000,iSTATE3:100";
  attribute FSM_ENCODED_STATES of \FSM_sequential_readaddr_FSM_reg[2]\ : label is "iSTATE:011,iSTATE0:001,iSTATE1:010,iSTATE2:000,iSTATE3:100";
  attribute XILINX_LEGACY_PRIM of \FSM_sequential_readdata_FSM_next_reg[0]\ : label is "LD";
  attribute XILINX_TRANSFORM_PINMAP of \FSM_sequential_readdata_FSM_next_reg[0]\ : label is "VCC:GE GND:CLR";
  attribute XILINX_LEGACY_PRIM of \FSM_sequential_readdata_FSM_next_reg[1]\ : label is "LD";
  attribute XILINX_TRANSFORM_PINMAP of \FSM_sequential_readdata_FSM_next_reg[1]\ : label is "VCC:GE GND:CLR";
  attribute SOFT_HLUTNM of \FSM_sequential_readdata_FSM_next_reg[1]_i_1\ : label is "soft_lutpair4";
  attribute XILINX_LEGACY_PRIM of \FSM_sequential_readdata_FSM_next_reg[2]\ : label is "LD";
  attribute XILINX_TRANSFORM_PINMAP of \FSM_sequential_readdata_FSM_next_reg[2]\ : label is "VCC:GE GND:CLR";
  attribute SOFT_HLUTNM of \FSM_sequential_readdata_FSM_next_reg[2]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \FSM_sequential_readdata_FSM_next_reg[2]_i_3\ : label is "soft_lutpair9";
  attribute FSM_ENCODED_STATES of \FSM_sequential_readdata_FSM_reg[0]\ : label is "iSTATE:001,iSTATE0:010,iSTATE1:011,iSTATE2:000,iSTATE3:100";
  attribute FSM_ENCODED_STATES of \FSM_sequential_readdata_FSM_reg[1]\ : label is "iSTATE:001,iSTATE0:010,iSTATE1:011,iSTATE2:000,iSTATE3:100";
  attribute FSM_ENCODED_STATES of \FSM_sequential_readdata_FSM_reg[2]\ : label is "iSTATE:001,iSTATE0:010,iSTATE1:011,iSTATE2:000,iSTATE3:100";
  attribute XILINX_LEGACY_PRIM of \FSM_sequential_writeaddr_FSM_next_reg[0]\ : label is "LD";
  attribute XILINX_TRANSFORM_PINMAP of \FSM_sequential_writeaddr_FSM_next_reg[0]\ : label is "VCC:GE GND:CLR";
  attribute XILINX_LEGACY_PRIM of \FSM_sequential_writeaddr_FSM_next_reg[1]\ : label is "LD";
  attribute XILINX_TRANSFORM_PINMAP of \FSM_sequential_writeaddr_FSM_next_reg[1]\ : label is "VCC:GE GND:CLR";
  attribute XILINX_LEGACY_PRIM of \FSM_sequential_writeaddr_FSM_next_reg[2]\ : label is "LD";
  attribute XILINX_TRANSFORM_PINMAP of \FSM_sequential_writeaddr_FSM_next_reg[2]\ : label is "VCC:GE GND:CLR";
  attribute FSM_ENCODED_STATES of \FSM_sequential_writeaddr_FSM_reg[0]\ : label is "iSTATE:011,iSTATE0:001,iSTATE1:010,iSTATE2:000,iSTATE3:100";
  attribute FSM_ENCODED_STATES of \FSM_sequential_writeaddr_FSM_reg[1]\ : label is "iSTATE:011,iSTATE0:001,iSTATE1:010,iSTATE2:000,iSTATE3:100";
  attribute FSM_ENCODED_STATES of \FSM_sequential_writeaddr_FSM_reg[2]\ : label is "iSTATE:011,iSTATE0:001,iSTATE1:010,iSTATE2:000,iSTATE3:100";
  attribute XILINX_LEGACY_PRIM of \FSM_sequential_writedata_FSM_next_reg[0]\ : label is "LD";
  attribute XILINX_TRANSFORM_PINMAP of \FSM_sequential_writedata_FSM_next_reg[0]\ : label is "VCC:GE GND:CLR";
  attribute XILINX_LEGACY_PRIM of \FSM_sequential_writedata_FSM_next_reg[1]\ : label is "LD";
  attribute XILINX_TRANSFORM_PINMAP of \FSM_sequential_writedata_FSM_next_reg[1]\ : label is "VCC:GE GND:CLR";
  attribute SOFT_HLUTNM of \FSM_sequential_writedata_FSM_next_reg[1]_i_1\ : label is "soft_lutpair3";
  attribute XILINX_LEGACY_PRIM of \FSM_sequential_writedata_FSM_next_reg[2]\ : label is "LD";
  attribute XILINX_TRANSFORM_PINMAP of \FSM_sequential_writedata_FSM_next_reg[2]\ : label is "VCC:GE GND:CLR";
  attribute SOFT_HLUTNM of \FSM_sequential_writedata_FSM_next_reg[2]_i_1\ : label is "soft_lutpair3";
  attribute FSM_ENCODED_STATES of \FSM_sequential_writedata_FSM_reg[0]\ : label is "iSTATE:001,iSTATE0:010,iSTATE1:011,iSTATE2:000,iSTATE3:100";
  attribute FSM_ENCODED_STATES of \FSM_sequential_writedata_FSM_reg[1]\ : label is "iSTATE:001,iSTATE0:010,iSTATE1:011,iSTATE2:000,iSTATE3:100";
  attribute FSM_ENCODED_STATES of \FSM_sequential_writedata_FSM_reg[2]\ : label is "iSTATE:001,iSTATE0:010,iSTATE1:011,iSTATE2:000,iSTATE3:100";
  attribute SOFT_HLUTNM of read_start_i_1 : label is "soft_lutpair6";
  attribute COMPARATOR_THRESHOLD : integer;
  attribute COMPARATOR_THRESHOLD of readaddr_FSM_next1_carry : label is 11;
  attribute COMPARATOR_THRESHOLD of \readaddr_FSM_next1_carry__0\ : label is 11;
  attribute ADDER_THRESHOLD of \readaddr_FSM_next1_carry__0_i_15\ : label is 35;
  attribute ADDER_THRESHOLD of \readaddr_FSM_next1_carry__0_i_16\ : label is 35;
  attribute ADDER_THRESHOLD of readaddr_FSM_next1_carry_i_17 : label is 35;
  attribute ADDER_THRESHOLD of readaddr_FSM_next1_carry_i_18 : label is 35;
  attribute ADDER_THRESHOLD of \readaddr_count_reg[12]_i_1\ : label is 16;
  attribute ADDER_THRESHOLD of \readaddr_count_reg[20]_i_1\ : label is 16;
  attribute ADDER_THRESHOLD of \readaddr_count_reg[28]_i_1\ : label is 16;
  attribute ADDER_THRESHOLD of \readaddr_count_reg[4]_i_2\ : label is 16;
  attribute SOFT_HLUTNM of readaddr_done_i_1 : label is "soft_lutpair8";
  attribute ADDER_THRESHOLD of \readdata_count_reg[0]_i_3\ : label is 16;
  attribute ADDER_THRESHOLD of \readdata_count_reg[16]_i_1\ : label is 16;
  attribute ADDER_THRESHOLD of \readdata_count_reg[24]_i_1\ : label is 16;
  attribute ADDER_THRESHOLD of \readdata_count_reg[8]_i_1\ : label is 16;
  attribute SOFT_HLUTNM of write_start_i_1 : label is "soft_lutpair6";
  attribute COMPARATOR_THRESHOLD of writeaddr_FSM_next1_carry : label is 11;
  attribute COMPARATOR_THRESHOLD of \writeaddr_FSM_next1_carry__0\ : label is 11;
  attribute ADDER_THRESHOLD of \writeaddr_FSM_next1_carry__0_i_15\ : label is 35;
  attribute ADDER_THRESHOLD of \writeaddr_FSM_next1_carry__0_i_16\ : label is 35;
  attribute ADDER_THRESHOLD of writeaddr_FSM_next1_carry_i_17 : label is 35;
  attribute ADDER_THRESHOLD of writeaddr_FSM_next1_carry_i_18 : label is 35;
  attribute ADDER_THRESHOLD of \writeaddr_count_reg[12]_i_1\ : label is 16;
  attribute ADDER_THRESHOLD of \writeaddr_count_reg[20]_i_1\ : label is 16;
  attribute ADDER_THRESHOLD of \writeaddr_count_reg[28]_i_1\ : label is 16;
  attribute ADDER_THRESHOLD of \writeaddr_count_reg[4]_i_2\ : label is 16;
  attribute SOFT_HLUTNM of writeaddr_done_i_1 : label is "soft_lutpair7";
  attribute ADDER_THRESHOLD of \writedata_FSM_next1_carry__0_i_4\ : label is 35;
  attribute ADDER_THRESHOLD of writedata_FSM_next1_carry_i_10 : label is 35;
  attribute ADDER_THRESHOLD of writedata_FSM_next1_carry_i_11 : label is 35;
  attribute ADDER_THRESHOLD of writedata_FSM_next1_carry_i_9 : label is 35;
  attribute ADDER_THRESHOLD of \writedata_count_reg[0]_i_3\ : label is 16;
  attribute ADDER_THRESHOLD of \writedata_count_reg[16]_i_1\ : label is 16;
  attribute ADDER_THRESHOLD of \writedata_count_reg[24]_i_1\ : label is 16;
  attribute ADDER_THRESHOLD of \writedata_count_reg[8]_i_1\ : label is 16;
begin
  AXI_ARVALID_reg_0 <= \^axi_arvalid_reg_0\;
  AXI_AWVALID_reg_0 <= \^axi_awvalid_reg_0\;
  busy <= \^busy\;
\/i_\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => FSM_next(0),
      I1 => FSM_next(2),
      I2 => FSM_next(1),
      O => write_startaddress_0
    );
\/i___0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => FSM_next(2),
      I1 => FSM_next(0),
      I2 => FSM_next(1),
      O => read_startaddress_1
    );
\AXI_ARADDR[11]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => read_startaddress(11),
      I1 => readaddr_count_reg(11),
      O => \AXI_ARADDR[11]_i_2_n_0\
    );
\AXI_ARADDR[11]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => read_startaddress(10),
      I1 => readaddr_count_reg(10),
      O => \AXI_ARADDR[11]_i_3_n_0\
    );
\AXI_ARADDR[11]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => read_startaddress(9),
      I1 => readaddr_count_reg(9),
      O => \AXI_ARADDR[11]_i_4_n_0\
    );
\AXI_ARADDR[11]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => read_startaddress(8),
      I1 => readaddr_count_reg(8),
      O => \AXI_ARADDR[11]_i_5_n_0\
    );
\AXI_ARADDR[11]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => read_startaddress(7),
      I1 => readaddr_count_reg(7),
      O => \AXI_ARADDR[11]_i_6_n_0\
    );
\AXI_ARADDR[11]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => read_startaddress(6),
      I1 => readaddr_count_reg(6),
      O => \AXI_ARADDR[11]_i_7_n_0\
    );
\AXI_ARADDR[11]_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => read_startaddress(5),
      I1 => readaddr_count_reg(5),
      O => \AXI_ARADDR[11]_i_8_n_0\
    );
\AXI_ARADDR[11]_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => read_startaddress(4),
      I1 => readaddr_count_reg(4),
      O => \AXI_ARADDR[11]_i_9_n_0\
    );
\AXI_ARADDR[19]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => read_startaddress(19),
      I1 => readaddr_count_reg(19),
      O => \AXI_ARADDR[19]_i_2_n_0\
    );
\AXI_ARADDR[19]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => read_startaddress(18),
      I1 => readaddr_count_reg(18),
      O => \AXI_ARADDR[19]_i_3_n_0\
    );
\AXI_ARADDR[19]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => read_startaddress(17),
      I1 => readaddr_count_reg(17),
      O => \AXI_ARADDR[19]_i_4_n_0\
    );
\AXI_ARADDR[19]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => read_startaddress(16),
      I1 => readaddr_count_reg(16),
      O => \AXI_ARADDR[19]_i_5_n_0\
    );
\AXI_ARADDR[19]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => read_startaddress(15),
      I1 => readaddr_count_reg(15),
      O => \AXI_ARADDR[19]_i_6_n_0\
    );
\AXI_ARADDR[19]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => read_startaddress(14),
      I1 => readaddr_count_reg(14),
      O => \AXI_ARADDR[19]_i_7_n_0\
    );
\AXI_ARADDR[19]_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => read_startaddress(13),
      I1 => readaddr_count_reg(13),
      O => \AXI_ARADDR[19]_i_8_n_0\
    );
\AXI_ARADDR[19]_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => read_startaddress(12),
      I1 => readaddr_count_reg(12),
      O => \AXI_ARADDR[19]_i_9_n_0\
    );
\AXI_ARADDR[27]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => read_startaddress(27),
      I1 => readaddr_count_reg(27),
      O => \AXI_ARADDR[27]_i_2_n_0\
    );
\AXI_ARADDR[27]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => read_startaddress(26),
      I1 => readaddr_count_reg(26),
      O => \AXI_ARADDR[27]_i_3_n_0\
    );
\AXI_ARADDR[27]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => read_startaddress(25),
      I1 => readaddr_count_reg(25),
      O => \AXI_ARADDR[27]_i_4_n_0\
    );
\AXI_ARADDR[27]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => read_startaddress(24),
      I1 => readaddr_count_reg(24),
      O => \AXI_ARADDR[27]_i_5_n_0\
    );
\AXI_ARADDR[27]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => read_startaddress(23),
      I1 => readaddr_count_reg(23),
      O => \AXI_ARADDR[27]_i_6_n_0\
    );
\AXI_ARADDR[27]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => read_startaddress(22),
      I1 => readaddr_count_reg(22),
      O => \AXI_ARADDR[27]_i_7_n_0\
    );
\AXI_ARADDR[27]_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => read_startaddress(21),
      I1 => readaddr_count_reg(21),
      O => \AXI_ARADDR[27]_i_8_n_0\
    );
\AXI_ARADDR[27]_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => read_startaddress(20),
      I1 => readaddr_count_reg(20),
      O => \AXI_ARADDR[27]_i_9_n_0\
    );
\AXI_ARADDR[32]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => readaddr_FSM_next(2),
      I1 => \FSM_sequential_readaddr_FSM_next_reg_n_0_[1]\,
      I2 => readaddr_FSM_next(0),
      O => AXI_ARADDR0
    );
\AXI_ARADDR[32]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"06"
    )
        port map (
      I0 => \FSM_sequential_readaddr_FSM_next_reg_n_0_[1]\,
      I1 => readaddr_FSM_next(0),
      I2 => readaddr_FSM_next(2),
      O => \AXI_ARADDR[32]_i_2_n_0\
    );
\AXI_ARADDR[32]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => read_startaddress(31),
      I1 => readaddr_count_reg(31),
      O => \AXI_ARADDR[32]_i_4_n_0\
    );
\AXI_ARADDR[32]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => read_startaddress(30),
      I1 => readaddr_count_reg(30),
      O => \AXI_ARADDR[32]_i_5_n_0\
    );
\AXI_ARADDR[32]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => read_startaddress(29),
      I1 => readaddr_count_reg(29),
      O => \AXI_ARADDR[32]_i_6_n_0\
    );
\AXI_ARADDR[32]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => read_startaddress(28),
      I1 => readaddr_count_reg(28),
      O => \AXI_ARADDR[32]_i_7_n_0\
    );
\AXI_ARADDR_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_ARADDR[32]_i_2_n_0\,
      D => read_startaddress(0),
      Q => AXI_ARADDR(0),
      R => AXI_ARADDR0
    );
\AXI_ARADDR_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_ARADDR[32]_i_2_n_0\,
      D => AXI_ARADDR00_in(10),
      Q => AXI_ARADDR(10),
      R => AXI_ARADDR0
    );
\AXI_ARADDR_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_ARADDR[32]_i_2_n_0\,
      D => AXI_ARADDR00_in(11),
      Q => AXI_ARADDR(11),
      R => AXI_ARADDR0
    );
\AXI_ARADDR_reg[11]_i_1\: unisim.vcomponents.CARRY8
     port map (
      CI => '0',
      CI_TOP => '0',
      CO(7) => \AXI_ARADDR_reg[11]_i_1_n_0\,
      CO(6) => \AXI_ARADDR_reg[11]_i_1_n_1\,
      CO(5) => \AXI_ARADDR_reg[11]_i_1_n_2\,
      CO(4) => \AXI_ARADDR_reg[11]_i_1_n_3\,
      CO(3) => \AXI_ARADDR_reg[11]_i_1_n_4\,
      CO(2) => \AXI_ARADDR_reg[11]_i_1_n_5\,
      CO(1) => \AXI_ARADDR_reg[11]_i_1_n_6\,
      CO(0) => \AXI_ARADDR_reg[11]_i_1_n_7\,
      DI(7 downto 0) => read_startaddress(11 downto 4),
      O(7 downto 0) => AXI_ARADDR00_in(11 downto 4),
      S(7) => \AXI_ARADDR[11]_i_2_n_0\,
      S(6) => \AXI_ARADDR[11]_i_3_n_0\,
      S(5) => \AXI_ARADDR[11]_i_4_n_0\,
      S(4) => \AXI_ARADDR[11]_i_5_n_0\,
      S(3) => \AXI_ARADDR[11]_i_6_n_0\,
      S(2) => \AXI_ARADDR[11]_i_7_n_0\,
      S(1) => \AXI_ARADDR[11]_i_8_n_0\,
      S(0) => \AXI_ARADDR[11]_i_9_n_0\
    );
\AXI_ARADDR_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_ARADDR[32]_i_2_n_0\,
      D => AXI_ARADDR00_in(12),
      Q => AXI_ARADDR(12),
      R => AXI_ARADDR0
    );
\AXI_ARADDR_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_ARADDR[32]_i_2_n_0\,
      D => AXI_ARADDR00_in(13),
      Q => AXI_ARADDR(13),
      R => AXI_ARADDR0
    );
\AXI_ARADDR_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_ARADDR[32]_i_2_n_0\,
      D => AXI_ARADDR00_in(14),
      Q => AXI_ARADDR(14),
      R => AXI_ARADDR0
    );
\AXI_ARADDR_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_ARADDR[32]_i_2_n_0\,
      D => AXI_ARADDR00_in(15),
      Q => AXI_ARADDR(15),
      R => AXI_ARADDR0
    );
\AXI_ARADDR_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_ARADDR[32]_i_2_n_0\,
      D => AXI_ARADDR00_in(16),
      Q => AXI_ARADDR(16),
      R => AXI_ARADDR0
    );
\AXI_ARADDR_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_ARADDR[32]_i_2_n_0\,
      D => AXI_ARADDR00_in(17),
      Q => AXI_ARADDR(17),
      R => AXI_ARADDR0
    );
\AXI_ARADDR_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_ARADDR[32]_i_2_n_0\,
      D => AXI_ARADDR00_in(18),
      Q => AXI_ARADDR(18),
      R => AXI_ARADDR0
    );
\AXI_ARADDR_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_ARADDR[32]_i_2_n_0\,
      D => AXI_ARADDR00_in(19),
      Q => AXI_ARADDR(19),
      R => AXI_ARADDR0
    );
\AXI_ARADDR_reg[19]_i_1\: unisim.vcomponents.CARRY8
     port map (
      CI => \AXI_ARADDR_reg[11]_i_1_n_0\,
      CI_TOP => '0',
      CO(7) => \AXI_ARADDR_reg[19]_i_1_n_0\,
      CO(6) => \AXI_ARADDR_reg[19]_i_1_n_1\,
      CO(5) => \AXI_ARADDR_reg[19]_i_1_n_2\,
      CO(4) => \AXI_ARADDR_reg[19]_i_1_n_3\,
      CO(3) => \AXI_ARADDR_reg[19]_i_1_n_4\,
      CO(2) => \AXI_ARADDR_reg[19]_i_1_n_5\,
      CO(1) => \AXI_ARADDR_reg[19]_i_1_n_6\,
      CO(0) => \AXI_ARADDR_reg[19]_i_1_n_7\,
      DI(7 downto 0) => read_startaddress(19 downto 12),
      O(7 downto 0) => AXI_ARADDR00_in(19 downto 12),
      S(7) => \AXI_ARADDR[19]_i_2_n_0\,
      S(6) => \AXI_ARADDR[19]_i_3_n_0\,
      S(5) => \AXI_ARADDR[19]_i_4_n_0\,
      S(4) => \AXI_ARADDR[19]_i_5_n_0\,
      S(3) => \AXI_ARADDR[19]_i_6_n_0\,
      S(2) => \AXI_ARADDR[19]_i_7_n_0\,
      S(1) => \AXI_ARADDR[19]_i_8_n_0\,
      S(0) => \AXI_ARADDR[19]_i_9_n_0\
    );
\AXI_ARADDR_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_ARADDR[32]_i_2_n_0\,
      D => read_startaddress(1),
      Q => AXI_ARADDR(1),
      R => AXI_ARADDR0
    );
\AXI_ARADDR_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_ARADDR[32]_i_2_n_0\,
      D => AXI_ARADDR00_in(20),
      Q => AXI_ARADDR(20),
      R => AXI_ARADDR0
    );
\AXI_ARADDR_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_ARADDR[32]_i_2_n_0\,
      D => AXI_ARADDR00_in(21),
      Q => AXI_ARADDR(21),
      R => AXI_ARADDR0
    );
\AXI_ARADDR_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_ARADDR[32]_i_2_n_0\,
      D => AXI_ARADDR00_in(22),
      Q => AXI_ARADDR(22),
      R => AXI_ARADDR0
    );
\AXI_ARADDR_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_ARADDR[32]_i_2_n_0\,
      D => AXI_ARADDR00_in(23),
      Q => AXI_ARADDR(23),
      R => AXI_ARADDR0
    );
\AXI_ARADDR_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_ARADDR[32]_i_2_n_0\,
      D => AXI_ARADDR00_in(24),
      Q => AXI_ARADDR(24),
      R => AXI_ARADDR0
    );
\AXI_ARADDR_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_ARADDR[32]_i_2_n_0\,
      D => AXI_ARADDR00_in(25),
      Q => AXI_ARADDR(25),
      R => AXI_ARADDR0
    );
\AXI_ARADDR_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_ARADDR[32]_i_2_n_0\,
      D => AXI_ARADDR00_in(26),
      Q => AXI_ARADDR(26),
      R => AXI_ARADDR0
    );
\AXI_ARADDR_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_ARADDR[32]_i_2_n_0\,
      D => AXI_ARADDR00_in(27),
      Q => AXI_ARADDR(27),
      R => AXI_ARADDR0
    );
\AXI_ARADDR_reg[27]_i_1\: unisim.vcomponents.CARRY8
     port map (
      CI => \AXI_ARADDR_reg[19]_i_1_n_0\,
      CI_TOP => '0',
      CO(7) => \AXI_ARADDR_reg[27]_i_1_n_0\,
      CO(6) => \AXI_ARADDR_reg[27]_i_1_n_1\,
      CO(5) => \AXI_ARADDR_reg[27]_i_1_n_2\,
      CO(4) => \AXI_ARADDR_reg[27]_i_1_n_3\,
      CO(3) => \AXI_ARADDR_reg[27]_i_1_n_4\,
      CO(2) => \AXI_ARADDR_reg[27]_i_1_n_5\,
      CO(1) => \AXI_ARADDR_reg[27]_i_1_n_6\,
      CO(0) => \AXI_ARADDR_reg[27]_i_1_n_7\,
      DI(7 downto 0) => read_startaddress(27 downto 20),
      O(7 downto 0) => AXI_ARADDR00_in(27 downto 20),
      S(7) => \AXI_ARADDR[27]_i_2_n_0\,
      S(6) => \AXI_ARADDR[27]_i_3_n_0\,
      S(5) => \AXI_ARADDR[27]_i_4_n_0\,
      S(4) => \AXI_ARADDR[27]_i_5_n_0\,
      S(3) => \AXI_ARADDR[27]_i_6_n_0\,
      S(2) => \AXI_ARADDR[27]_i_7_n_0\,
      S(1) => \AXI_ARADDR[27]_i_8_n_0\,
      S(0) => \AXI_ARADDR[27]_i_9_n_0\
    );
\AXI_ARADDR_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_ARADDR[32]_i_2_n_0\,
      D => AXI_ARADDR00_in(28),
      Q => AXI_ARADDR(28),
      R => AXI_ARADDR0
    );
\AXI_ARADDR_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_ARADDR[32]_i_2_n_0\,
      D => AXI_ARADDR00_in(29),
      Q => AXI_ARADDR(29),
      R => AXI_ARADDR0
    );
\AXI_ARADDR_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_ARADDR[32]_i_2_n_0\,
      D => read_startaddress(2),
      Q => AXI_ARADDR(2),
      R => AXI_ARADDR0
    );
\AXI_ARADDR_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_ARADDR[32]_i_2_n_0\,
      D => AXI_ARADDR00_in(30),
      Q => AXI_ARADDR(30),
      R => AXI_ARADDR0
    );
\AXI_ARADDR_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_ARADDR[32]_i_2_n_0\,
      D => AXI_ARADDR00_in(31),
      Q => AXI_ARADDR(31),
      R => AXI_ARADDR0
    );
\AXI_ARADDR_reg[32]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_ARADDR[32]_i_2_n_0\,
      D => AXI_ARADDR00_in(32),
      Q => AXI_ARADDR(32),
      R => AXI_ARADDR0
    );
\AXI_ARADDR_reg[32]_i_3\: unisim.vcomponents.CARRY8
     port map (
      CI => \AXI_ARADDR_reg[27]_i_1_n_0\,
      CI_TOP => '0',
      CO(7 downto 5) => \NLW_AXI_ARADDR_reg[32]_i_3_CO_UNCONNECTED\(7 downto 5),
      CO(4) => AXI_ARADDR00_in(32),
      CO(3) => \NLW_AXI_ARADDR_reg[32]_i_3_CO_UNCONNECTED\(3),
      CO(2) => \AXI_ARADDR_reg[32]_i_3_n_5\,
      CO(1) => \AXI_ARADDR_reg[32]_i_3_n_6\,
      CO(0) => \AXI_ARADDR_reg[32]_i_3_n_7\,
      DI(7 downto 4) => B"0000",
      DI(3 downto 0) => read_startaddress(31 downto 28),
      O(7 downto 4) => \NLW_AXI_ARADDR_reg[32]_i_3_O_UNCONNECTED\(7 downto 4),
      O(3 downto 0) => AXI_ARADDR00_in(31 downto 28),
      S(7 downto 4) => B"0001",
      S(3) => \AXI_ARADDR[32]_i_4_n_0\,
      S(2) => \AXI_ARADDR[32]_i_5_n_0\,
      S(1) => \AXI_ARADDR[32]_i_6_n_0\,
      S(0) => \AXI_ARADDR[32]_i_7_n_0\
    );
\AXI_ARADDR_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_ARADDR[32]_i_2_n_0\,
      D => read_startaddress(3),
      Q => AXI_ARADDR(3),
      R => AXI_ARADDR0
    );
\AXI_ARADDR_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_ARADDR[32]_i_2_n_0\,
      D => AXI_ARADDR00_in(4),
      Q => AXI_ARADDR(4),
      R => AXI_ARADDR0
    );
\AXI_ARADDR_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_ARADDR[32]_i_2_n_0\,
      D => AXI_ARADDR00_in(5),
      Q => AXI_ARADDR(5),
      R => AXI_ARADDR0
    );
\AXI_ARADDR_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_ARADDR[32]_i_2_n_0\,
      D => AXI_ARADDR00_in(6),
      Q => AXI_ARADDR(6),
      R => AXI_ARADDR0
    );
\AXI_ARADDR_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_ARADDR[32]_i_2_n_0\,
      D => AXI_ARADDR00_in(7),
      Q => AXI_ARADDR(7),
      R => AXI_ARADDR0
    );
\AXI_ARADDR_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_ARADDR[32]_i_2_n_0\,
      D => AXI_ARADDR00_in(8),
      Q => AXI_ARADDR(8),
      R => AXI_ARADDR0
    );
\AXI_ARADDR_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_ARADDR[32]_i_2_n_0\,
      D => AXI_ARADDR00_in(9),
      Q => AXI_ARADDR(9),
      R => AXI_ARADDR0
    );
\AXI_ARLEN[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F4"
    )
        port map (
      I0 => read_blockamount(0),
      I1 => \FSM_sequential_readaddr_FSM_next_reg_n_0_[1]\,
      I2 => readaddr_FSM_next(0),
      O => \AXI_ARLEN[0]_i_1_n_0\
    );
\AXI_ARLEN[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EAAE"
    )
        port map (
      I0 => readaddr_FSM_next(0),
      I1 => \FSM_sequential_readaddr_FSM_next_reg_n_0_[1]\,
      I2 => read_blockamount(0),
      I3 => read_blockamount(1),
      O => \AXI_ARLEN[1]_i_1_n_0\
    );
\AXI_ARLEN[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEABAAAA"
    )
        port map (
      I0 => readaddr_FSM_next(0),
      I1 => read_blockamount(0),
      I2 => read_blockamount(1),
      I3 => read_blockamount(2),
      I4 => \FSM_sequential_readaddr_FSM_next_reg_n_0_[1]\,
      O => \AXI_ARLEN[2]_i_1_n_0\
    );
\AXI_ARLEN[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"07"
    )
        port map (
      I0 => readaddr_FSM_next(0),
      I1 => \FSM_sequential_readaddr_FSM_next_reg_n_0_[1]\,
      I2 => readaddr_FSM_next(2),
      O => \AXI_ARLEN[3]_i_1_n_0\
    );
\AXI_ARLEN[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEAAAAAAABAAAA"
    )
        port map (
      I0 => readaddr_FSM_next(0),
      I1 => read_blockamount(0),
      I2 => read_blockamount(1),
      I3 => read_blockamount(2),
      I4 => \FSM_sequential_readaddr_FSM_next_reg_n_0_[1]\,
      I5 => read_blockamount(3),
      O => \AXI_ARLEN[3]_i_2_n_0\
    );
\AXI_ARLEN_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_ARLEN[3]_i_1_n_0\,
      D => \AXI_ARLEN[0]_i_1_n_0\,
      Q => AXI_ARLEN(0),
      R => '0'
    );
\AXI_ARLEN_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_ARLEN[3]_i_1_n_0\,
      D => \AXI_ARLEN[1]_i_1_n_0\,
      Q => AXI_ARLEN(1),
      R => '0'
    );
\AXI_ARLEN_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_ARLEN[3]_i_1_n_0\,
      D => \AXI_ARLEN[2]_i_1_n_0\,
      Q => AXI_ARLEN(2),
      R => '0'
    );
\AXI_ARLEN_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_ARLEN[3]_i_1_n_0\,
      D => \AXI_ARLEN[3]_i_2_n_0\,
      Q => AXI_ARLEN(3),
      R => '0'
    );
AXI_ARVALID_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8BB8"
    )
        port map (
      I0 => \^axi_arvalid_reg_0\,
      I1 => readaddr_FSM_next(2),
      I2 => readaddr_FSM_next(0),
      I3 => \FSM_sequential_readaddr_FSM_next_reg_n_0_[1]\,
      O => AXI_ARVALID_i_1_n_0
    );
AXI_ARVALID_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => AXI_ARVALID_i_1_n_0,
      Q => \^axi_arvalid_reg_0\,
      R => '0'
    );
\AXI_AWADDR[11]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => write_startaddress(11),
      I1 => writeaddr_count_reg(11),
      O => \AXI_AWADDR[11]_i_2_n_0\
    );
\AXI_AWADDR[11]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => write_startaddress(10),
      I1 => writeaddr_count_reg(10),
      O => \AXI_AWADDR[11]_i_3_n_0\
    );
\AXI_AWADDR[11]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => write_startaddress(9),
      I1 => writeaddr_count_reg(9),
      O => \AXI_AWADDR[11]_i_4_n_0\
    );
\AXI_AWADDR[11]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => write_startaddress(8),
      I1 => writeaddr_count_reg(8),
      O => \AXI_AWADDR[11]_i_5_n_0\
    );
\AXI_AWADDR[11]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => write_startaddress(7),
      I1 => writeaddr_count_reg(7),
      O => \AXI_AWADDR[11]_i_6_n_0\
    );
\AXI_AWADDR[11]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => write_startaddress(6),
      I1 => writeaddr_count_reg(6),
      O => \AXI_AWADDR[11]_i_7_n_0\
    );
\AXI_AWADDR[11]_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => write_startaddress(5),
      I1 => writeaddr_count_reg(5),
      O => \AXI_AWADDR[11]_i_8_n_0\
    );
\AXI_AWADDR[11]_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => write_startaddress(4),
      I1 => writeaddr_count_reg(4),
      O => \AXI_AWADDR[11]_i_9_n_0\
    );
\AXI_AWADDR[19]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => write_startaddress(19),
      I1 => writeaddr_count_reg(19),
      O => \AXI_AWADDR[19]_i_2_n_0\
    );
\AXI_AWADDR[19]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => write_startaddress(18),
      I1 => writeaddr_count_reg(18),
      O => \AXI_AWADDR[19]_i_3_n_0\
    );
\AXI_AWADDR[19]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => write_startaddress(17),
      I1 => writeaddr_count_reg(17),
      O => \AXI_AWADDR[19]_i_4_n_0\
    );
\AXI_AWADDR[19]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => write_startaddress(16),
      I1 => writeaddr_count_reg(16),
      O => \AXI_AWADDR[19]_i_5_n_0\
    );
\AXI_AWADDR[19]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => write_startaddress(15),
      I1 => writeaddr_count_reg(15),
      O => \AXI_AWADDR[19]_i_6_n_0\
    );
\AXI_AWADDR[19]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => write_startaddress(14),
      I1 => writeaddr_count_reg(14),
      O => \AXI_AWADDR[19]_i_7_n_0\
    );
\AXI_AWADDR[19]_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => write_startaddress(13),
      I1 => writeaddr_count_reg(13),
      O => \AXI_AWADDR[19]_i_8_n_0\
    );
\AXI_AWADDR[19]_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => write_startaddress(12),
      I1 => writeaddr_count_reg(12),
      O => \AXI_AWADDR[19]_i_9_n_0\
    );
\AXI_AWADDR[27]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => write_startaddress(27),
      I1 => writeaddr_count_reg(27),
      O => \AXI_AWADDR[27]_i_2_n_0\
    );
\AXI_AWADDR[27]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => write_startaddress(26),
      I1 => writeaddr_count_reg(26),
      O => \AXI_AWADDR[27]_i_3_n_0\
    );
\AXI_AWADDR[27]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => write_startaddress(25),
      I1 => writeaddr_count_reg(25),
      O => \AXI_AWADDR[27]_i_4_n_0\
    );
\AXI_AWADDR[27]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => write_startaddress(24),
      I1 => writeaddr_count_reg(24),
      O => \AXI_AWADDR[27]_i_5_n_0\
    );
\AXI_AWADDR[27]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => write_startaddress(23),
      I1 => writeaddr_count_reg(23),
      O => \AXI_AWADDR[27]_i_6_n_0\
    );
\AXI_AWADDR[27]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => write_startaddress(22),
      I1 => writeaddr_count_reg(22),
      O => \AXI_AWADDR[27]_i_7_n_0\
    );
\AXI_AWADDR[27]_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => write_startaddress(21),
      I1 => writeaddr_count_reg(21),
      O => \AXI_AWADDR[27]_i_8_n_0\
    );
\AXI_AWADDR[27]_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => write_startaddress(20),
      I1 => writeaddr_count_reg(20),
      O => \AXI_AWADDR[27]_i_9_n_0\
    );
\AXI_AWADDR[32]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => writeaddr_FSM_next(2),
      I1 => \FSM_sequential_writeaddr_FSM_next_reg_n_0_[1]\,
      I2 => writeaddr_FSM_next(0),
      O => writeaddr_count0
    );
\AXI_AWADDR[32]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"06"
    )
        port map (
      I0 => \FSM_sequential_writeaddr_FSM_next_reg_n_0_[1]\,
      I1 => writeaddr_FSM_next(0),
      I2 => writeaddr_FSM_next(2),
      O => \AXI_AWADDR[32]_i_2_n_0\
    );
\AXI_AWADDR[32]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => write_startaddress(31),
      I1 => writeaddr_count_reg(31),
      O => \AXI_AWADDR[32]_i_4_n_0\
    );
\AXI_AWADDR[32]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => write_startaddress(30),
      I1 => writeaddr_count_reg(30),
      O => \AXI_AWADDR[32]_i_5_n_0\
    );
\AXI_AWADDR[32]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => write_startaddress(29),
      I1 => writeaddr_count_reg(29),
      O => \AXI_AWADDR[32]_i_6_n_0\
    );
\AXI_AWADDR[32]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => write_startaddress(28),
      I1 => writeaddr_count_reg(28),
      O => \AXI_AWADDR[32]_i_7_n_0\
    );
\AXI_AWADDR_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_AWADDR[32]_i_2_n_0\,
      D => write_startaddress(0),
      Q => AXI_AWADDR(0),
      R => writeaddr_count0
    );
\AXI_AWADDR_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_AWADDR[32]_i_2_n_0\,
      D => AXI_AWADDR0(10),
      Q => AXI_AWADDR(10),
      R => writeaddr_count0
    );
\AXI_AWADDR_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_AWADDR[32]_i_2_n_0\,
      D => AXI_AWADDR0(11),
      Q => AXI_AWADDR(11),
      R => writeaddr_count0
    );
\AXI_AWADDR_reg[11]_i_1\: unisim.vcomponents.CARRY8
     port map (
      CI => '0',
      CI_TOP => '0',
      CO(7) => \AXI_AWADDR_reg[11]_i_1_n_0\,
      CO(6) => \AXI_AWADDR_reg[11]_i_1_n_1\,
      CO(5) => \AXI_AWADDR_reg[11]_i_1_n_2\,
      CO(4) => \AXI_AWADDR_reg[11]_i_1_n_3\,
      CO(3) => \AXI_AWADDR_reg[11]_i_1_n_4\,
      CO(2) => \AXI_AWADDR_reg[11]_i_1_n_5\,
      CO(1) => \AXI_AWADDR_reg[11]_i_1_n_6\,
      CO(0) => \AXI_AWADDR_reg[11]_i_1_n_7\,
      DI(7 downto 0) => write_startaddress(11 downto 4),
      O(7 downto 0) => AXI_AWADDR0(11 downto 4),
      S(7) => \AXI_AWADDR[11]_i_2_n_0\,
      S(6) => \AXI_AWADDR[11]_i_3_n_0\,
      S(5) => \AXI_AWADDR[11]_i_4_n_0\,
      S(4) => \AXI_AWADDR[11]_i_5_n_0\,
      S(3) => \AXI_AWADDR[11]_i_6_n_0\,
      S(2) => \AXI_AWADDR[11]_i_7_n_0\,
      S(1) => \AXI_AWADDR[11]_i_8_n_0\,
      S(0) => \AXI_AWADDR[11]_i_9_n_0\
    );
\AXI_AWADDR_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_AWADDR[32]_i_2_n_0\,
      D => AXI_AWADDR0(12),
      Q => AXI_AWADDR(12),
      R => writeaddr_count0
    );
\AXI_AWADDR_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_AWADDR[32]_i_2_n_0\,
      D => AXI_AWADDR0(13),
      Q => AXI_AWADDR(13),
      R => writeaddr_count0
    );
\AXI_AWADDR_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_AWADDR[32]_i_2_n_0\,
      D => AXI_AWADDR0(14),
      Q => AXI_AWADDR(14),
      R => writeaddr_count0
    );
\AXI_AWADDR_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_AWADDR[32]_i_2_n_0\,
      D => AXI_AWADDR0(15),
      Q => AXI_AWADDR(15),
      R => writeaddr_count0
    );
\AXI_AWADDR_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_AWADDR[32]_i_2_n_0\,
      D => AXI_AWADDR0(16),
      Q => AXI_AWADDR(16),
      R => writeaddr_count0
    );
\AXI_AWADDR_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_AWADDR[32]_i_2_n_0\,
      D => AXI_AWADDR0(17),
      Q => AXI_AWADDR(17),
      R => writeaddr_count0
    );
\AXI_AWADDR_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_AWADDR[32]_i_2_n_0\,
      D => AXI_AWADDR0(18),
      Q => AXI_AWADDR(18),
      R => writeaddr_count0
    );
\AXI_AWADDR_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_AWADDR[32]_i_2_n_0\,
      D => AXI_AWADDR0(19),
      Q => AXI_AWADDR(19),
      R => writeaddr_count0
    );
\AXI_AWADDR_reg[19]_i_1\: unisim.vcomponents.CARRY8
     port map (
      CI => \AXI_AWADDR_reg[11]_i_1_n_0\,
      CI_TOP => '0',
      CO(7) => \AXI_AWADDR_reg[19]_i_1_n_0\,
      CO(6) => \AXI_AWADDR_reg[19]_i_1_n_1\,
      CO(5) => \AXI_AWADDR_reg[19]_i_1_n_2\,
      CO(4) => \AXI_AWADDR_reg[19]_i_1_n_3\,
      CO(3) => \AXI_AWADDR_reg[19]_i_1_n_4\,
      CO(2) => \AXI_AWADDR_reg[19]_i_1_n_5\,
      CO(1) => \AXI_AWADDR_reg[19]_i_1_n_6\,
      CO(0) => \AXI_AWADDR_reg[19]_i_1_n_7\,
      DI(7 downto 0) => write_startaddress(19 downto 12),
      O(7 downto 0) => AXI_AWADDR0(19 downto 12),
      S(7) => \AXI_AWADDR[19]_i_2_n_0\,
      S(6) => \AXI_AWADDR[19]_i_3_n_0\,
      S(5) => \AXI_AWADDR[19]_i_4_n_0\,
      S(4) => \AXI_AWADDR[19]_i_5_n_0\,
      S(3) => \AXI_AWADDR[19]_i_6_n_0\,
      S(2) => \AXI_AWADDR[19]_i_7_n_0\,
      S(1) => \AXI_AWADDR[19]_i_8_n_0\,
      S(0) => \AXI_AWADDR[19]_i_9_n_0\
    );
\AXI_AWADDR_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_AWADDR[32]_i_2_n_0\,
      D => write_startaddress(1),
      Q => AXI_AWADDR(1),
      R => writeaddr_count0
    );
\AXI_AWADDR_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_AWADDR[32]_i_2_n_0\,
      D => AXI_AWADDR0(20),
      Q => AXI_AWADDR(20),
      R => writeaddr_count0
    );
\AXI_AWADDR_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_AWADDR[32]_i_2_n_0\,
      D => AXI_AWADDR0(21),
      Q => AXI_AWADDR(21),
      R => writeaddr_count0
    );
\AXI_AWADDR_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_AWADDR[32]_i_2_n_0\,
      D => AXI_AWADDR0(22),
      Q => AXI_AWADDR(22),
      R => writeaddr_count0
    );
\AXI_AWADDR_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_AWADDR[32]_i_2_n_0\,
      D => AXI_AWADDR0(23),
      Q => AXI_AWADDR(23),
      R => writeaddr_count0
    );
\AXI_AWADDR_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_AWADDR[32]_i_2_n_0\,
      D => AXI_AWADDR0(24),
      Q => AXI_AWADDR(24),
      R => writeaddr_count0
    );
\AXI_AWADDR_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_AWADDR[32]_i_2_n_0\,
      D => AXI_AWADDR0(25),
      Q => AXI_AWADDR(25),
      R => writeaddr_count0
    );
\AXI_AWADDR_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_AWADDR[32]_i_2_n_0\,
      D => AXI_AWADDR0(26),
      Q => AXI_AWADDR(26),
      R => writeaddr_count0
    );
\AXI_AWADDR_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_AWADDR[32]_i_2_n_0\,
      D => AXI_AWADDR0(27),
      Q => AXI_AWADDR(27),
      R => writeaddr_count0
    );
\AXI_AWADDR_reg[27]_i_1\: unisim.vcomponents.CARRY8
     port map (
      CI => \AXI_AWADDR_reg[19]_i_1_n_0\,
      CI_TOP => '0',
      CO(7) => \AXI_AWADDR_reg[27]_i_1_n_0\,
      CO(6) => \AXI_AWADDR_reg[27]_i_1_n_1\,
      CO(5) => \AXI_AWADDR_reg[27]_i_1_n_2\,
      CO(4) => \AXI_AWADDR_reg[27]_i_1_n_3\,
      CO(3) => \AXI_AWADDR_reg[27]_i_1_n_4\,
      CO(2) => \AXI_AWADDR_reg[27]_i_1_n_5\,
      CO(1) => \AXI_AWADDR_reg[27]_i_1_n_6\,
      CO(0) => \AXI_AWADDR_reg[27]_i_1_n_7\,
      DI(7 downto 0) => write_startaddress(27 downto 20),
      O(7 downto 0) => AXI_AWADDR0(27 downto 20),
      S(7) => \AXI_AWADDR[27]_i_2_n_0\,
      S(6) => \AXI_AWADDR[27]_i_3_n_0\,
      S(5) => \AXI_AWADDR[27]_i_4_n_0\,
      S(4) => \AXI_AWADDR[27]_i_5_n_0\,
      S(3) => \AXI_AWADDR[27]_i_6_n_0\,
      S(2) => \AXI_AWADDR[27]_i_7_n_0\,
      S(1) => \AXI_AWADDR[27]_i_8_n_0\,
      S(0) => \AXI_AWADDR[27]_i_9_n_0\
    );
\AXI_AWADDR_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_AWADDR[32]_i_2_n_0\,
      D => AXI_AWADDR0(28),
      Q => AXI_AWADDR(28),
      R => writeaddr_count0
    );
\AXI_AWADDR_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_AWADDR[32]_i_2_n_0\,
      D => AXI_AWADDR0(29),
      Q => AXI_AWADDR(29),
      R => writeaddr_count0
    );
\AXI_AWADDR_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_AWADDR[32]_i_2_n_0\,
      D => write_startaddress(2),
      Q => AXI_AWADDR(2),
      R => writeaddr_count0
    );
\AXI_AWADDR_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_AWADDR[32]_i_2_n_0\,
      D => AXI_AWADDR0(30),
      Q => AXI_AWADDR(30),
      R => writeaddr_count0
    );
\AXI_AWADDR_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_AWADDR[32]_i_2_n_0\,
      D => AXI_AWADDR0(31),
      Q => AXI_AWADDR(31),
      R => writeaddr_count0
    );
\AXI_AWADDR_reg[32]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_AWADDR[32]_i_2_n_0\,
      D => AXI_AWADDR0(32),
      Q => AXI_AWADDR(32),
      R => writeaddr_count0
    );
\AXI_AWADDR_reg[32]_i_3\: unisim.vcomponents.CARRY8
     port map (
      CI => \AXI_AWADDR_reg[27]_i_1_n_0\,
      CI_TOP => '0',
      CO(7 downto 5) => \NLW_AXI_AWADDR_reg[32]_i_3_CO_UNCONNECTED\(7 downto 5),
      CO(4) => AXI_AWADDR0(32),
      CO(3) => \NLW_AXI_AWADDR_reg[32]_i_3_CO_UNCONNECTED\(3),
      CO(2) => \AXI_AWADDR_reg[32]_i_3_n_5\,
      CO(1) => \AXI_AWADDR_reg[32]_i_3_n_6\,
      CO(0) => \AXI_AWADDR_reg[32]_i_3_n_7\,
      DI(7 downto 4) => B"0000",
      DI(3 downto 0) => write_startaddress(31 downto 28),
      O(7 downto 4) => \NLW_AXI_AWADDR_reg[32]_i_3_O_UNCONNECTED\(7 downto 4),
      O(3 downto 0) => AXI_AWADDR0(31 downto 28),
      S(7 downto 4) => B"0001",
      S(3) => \AXI_AWADDR[32]_i_4_n_0\,
      S(2) => \AXI_AWADDR[32]_i_5_n_0\,
      S(1) => \AXI_AWADDR[32]_i_6_n_0\,
      S(0) => \AXI_AWADDR[32]_i_7_n_0\
    );
\AXI_AWADDR_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_AWADDR[32]_i_2_n_0\,
      D => write_startaddress(3),
      Q => AXI_AWADDR(3),
      R => writeaddr_count0
    );
\AXI_AWADDR_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_AWADDR[32]_i_2_n_0\,
      D => AXI_AWADDR0(4),
      Q => AXI_AWADDR(4),
      R => writeaddr_count0
    );
\AXI_AWADDR_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_AWADDR[32]_i_2_n_0\,
      D => AXI_AWADDR0(5),
      Q => AXI_AWADDR(5),
      R => writeaddr_count0
    );
\AXI_AWADDR_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_AWADDR[32]_i_2_n_0\,
      D => AXI_AWADDR0(6),
      Q => AXI_AWADDR(6),
      R => writeaddr_count0
    );
\AXI_AWADDR_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_AWADDR[32]_i_2_n_0\,
      D => AXI_AWADDR0(7),
      Q => AXI_AWADDR(7),
      R => writeaddr_count0
    );
\AXI_AWADDR_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_AWADDR[32]_i_2_n_0\,
      D => AXI_AWADDR0(8),
      Q => AXI_AWADDR(8),
      R => writeaddr_count0
    );
\AXI_AWADDR_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_AWADDR[32]_i_2_n_0\,
      D => AXI_AWADDR0(9),
      Q => AXI_AWADDR(9),
      R => writeaddr_count0
    );
\AXI_AWLEN[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F4"
    )
        port map (
      I0 => write_blockamount(0),
      I1 => \FSM_sequential_writeaddr_FSM_next_reg_n_0_[1]\,
      I2 => writeaddr_FSM_next(0),
      O => \AXI_AWLEN[0]_i_1_n_0\
    );
\AXI_AWLEN[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EAAE"
    )
        port map (
      I0 => writeaddr_FSM_next(0),
      I1 => \FSM_sequential_writeaddr_FSM_next_reg_n_0_[1]\,
      I2 => write_blockamount(0),
      I3 => write_blockamount(1),
      O => \AXI_AWLEN[1]_i_1_n_0\
    );
\AXI_AWLEN[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEABAAAA"
    )
        port map (
      I0 => writeaddr_FSM_next(0),
      I1 => write_blockamount(0),
      I2 => write_blockamount(1),
      I3 => write_blockamount(2),
      I4 => \FSM_sequential_writeaddr_FSM_next_reg_n_0_[1]\,
      O => \AXI_AWLEN[2]_i_1_n_0\
    );
\AXI_AWLEN[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"07"
    )
        port map (
      I0 => writeaddr_FSM_next(0),
      I1 => \FSM_sequential_writeaddr_FSM_next_reg_n_0_[1]\,
      I2 => writeaddr_FSM_next(2),
      O => \AXI_AWLEN[3]_i_1_n_0\
    );
\AXI_AWLEN[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEAAAAAAABAAAA"
    )
        port map (
      I0 => writeaddr_FSM_next(0),
      I1 => write_blockamount(0),
      I2 => write_blockamount(1),
      I3 => write_blockamount(2),
      I4 => \FSM_sequential_writeaddr_FSM_next_reg_n_0_[1]\,
      I5 => write_blockamount(3),
      O => \AXI_AWLEN[3]_i_2_n_0\
    );
\AXI_AWLEN_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_AWLEN[3]_i_1_n_0\,
      D => \AXI_AWLEN[0]_i_1_n_0\,
      Q => AXI_AWLEN(0),
      R => '0'
    );
\AXI_AWLEN_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_AWLEN[3]_i_1_n_0\,
      D => \AXI_AWLEN[1]_i_1_n_0\,
      Q => AXI_AWLEN(1),
      R => '0'
    );
\AXI_AWLEN_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_AWLEN[3]_i_1_n_0\,
      D => \AXI_AWLEN[2]_i_1_n_0\,
      Q => AXI_AWLEN(2),
      R => '0'
    );
\AXI_AWLEN_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \AXI_AWLEN[3]_i_1_n_0\,
      D => \AXI_AWLEN[3]_i_2_n_0\,
      Q => AXI_AWLEN(3),
      R => '0'
    );
AXI_AWVALID_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8BB8"
    )
        port map (
      I0 => \^axi_awvalid_reg_0\,
      I1 => writeaddr_FSM_next(2),
      I2 => writeaddr_FSM_next(0),
      I3 => \FSM_sequential_writeaddr_FSM_next_reg_n_0_[1]\,
      O => AXI_AWVALID_i_1_n_0
    );
AXI_AWVALID_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => AXI_AWVALID_i_1_n_0,
      Q => \^axi_awvalid_reg_0\,
      R => '0'
    );
\AXI_WDATA_PARITY0_inferred__0/AXI_WDATA_PARITY[1]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => FIFO_READ_tdata(13),
      I1 => FIFO_READ_tdata(12),
      I2 => FIFO_READ_tdata(15),
      I3 => FIFO_READ_tdata(14),
      I4 => \AXI_WDATA_PARITY0_inferred__0/AXI_WDATA_PARITY[1]_INST_0_i_1_n_0\,
      O => AXI_WDATA_PARITY(1)
    );
\AXI_WDATA_PARITY0_inferred__0/AXI_WDATA_PARITY[1]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => FIFO_READ_tdata(10),
      I1 => FIFO_READ_tdata(11),
      I2 => FIFO_READ_tdata(8),
      I3 => FIFO_READ_tdata(9),
      O => \AXI_WDATA_PARITY0_inferred__0/AXI_WDATA_PARITY[1]_INST_0_i_1_n_0\
    );
\AXI_WDATA_PARITY0_inferred__1/AXI_WDATA_PARITY[2]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => FIFO_READ_tdata(21),
      I1 => FIFO_READ_tdata(20),
      I2 => FIFO_READ_tdata(23),
      I3 => FIFO_READ_tdata(22),
      I4 => \AXI_WDATA_PARITY0_inferred__1/AXI_WDATA_PARITY[2]_INST_0_i_1_n_0\,
      O => AXI_WDATA_PARITY(2)
    );
\AXI_WDATA_PARITY0_inferred__1/AXI_WDATA_PARITY[2]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => FIFO_READ_tdata(18),
      I1 => FIFO_READ_tdata(19),
      I2 => FIFO_READ_tdata(16),
      I3 => FIFO_READ_tdata(17),
      O => \AXI_WDATA_PARITY0_inferred__1/AXI_WDATA_PARITY[2]_INST_0_i_1_n_0\
    );
\AXI_WDATA_PARITY0_inferred__10/AXI_WDATA_PARITY[11]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => FIFO_READ_tdata(93),
      I1 => FIFO_READ_tdata(92),
      I2 => FIFO_READ_tdata(95),
      I3 => FIFO_READ_tdata(94),
      I4 => \AXI_WDATA_PARITY0_inferred__10/AXI_WDATA_PARITY[11]_INST_0_i_1_n_0\,
      O => AXI_WDATA_PARITY(11)
    );
\AXI_WDATA_PARITY0_inferred__10/AXI_WDATA_PARITY[11]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => FIFO_READ_tdata(90),
      I1 => FIFO_READ_tdata(91),
      I2 => FIFO_READ_tdata(88),
      I3 => FIFO_READ_tdata(89),
      O => \AXI_WDATA_PARITY0_inferred__10/AXI_WDATA_PARITY[11]_INST_0_i_1_n_0\
    );
\AXI_WDATA_PARITY0_inferred__11/AXI_WDATA_PARITY[12]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => FIFO_READ_tdata(101),
      I1 => FIFO_READ_tdata(100),
      I2 => FIFO_READ_tdata(103),
      I3 => FIFO_READ_tdata(102),
      I4 => \AXI_WDATA_PARITY0_inferred__11/AXI_WDATA_PARITY[12]_INST_0_i_1_n_0\,
      O => AXI_WDATA_PARITY(12)
    );
\AXI_WDATA_PARITY0_inferred__11/AXI_WDATA_PARITY[12]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => FIFO_READ_tdata(98),
      I1 => FIFO_READ_tdata(99),
      I2 => FIFO_READ_tdata(96),
      I3 => FIFO_READ_tdata(97),
      O => \AXI_WDATA_PARITY0_inferred__11/AXI_WDATA_PARITY[12]_INST_0_i_1_n_0\
    );
\AXI_WDATA_PARITY0_inferred__12/AXI_WDATA_PARITY[13]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => FIFO_READ_tdata(109),
      I1 => FIFO_READ_tdata(108),
      I2 => FIFO_READ_tdata(111),
      I3 => FIFO_READ_tdata(110),
      I4 => \AXI_WDATA_PARITY0_inferred__12/AXI_WDATA_PARITY[13]_INST_0_i_1_n_0\,
      O => AXI_WDATA_PARITY(13)
    );
\AXI_WDATA_PARITY0_inferred__12/AXI_WDATA_PARITY[13]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => FIFO_READ_tdata(106),
      I1 => FIFO_READ_tdata(107),
      I2 => FIFO_READ_tdata(104),
      I3 => FIFO_READ_tdata(105),
      O => \AXI_WDATA_PARITY0_inferred__12/AXI_WDATA_PARITY[13]_INST_0_i_1_n_0\
    );
\AXI_WDATA_PARITY0_inferred__13/AXI_WDATA_PARITY[14]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => FIFO_READ_tdata(117),
      I1 => FIFO_READ_tdata(116),
      I2 => FIFO_READ_tdata(119),
      I3 => FIFO_READ_tdata(118),
      I4 => \AXI_WDATA_PARITY0_inferred__13/AXI_WDATA_PARITY[14]_INST_0_i_1_n_0\,
      O => AXI_WDATA_PARITY(14)
    );
\AXI_WDATA_PARITY0_inferred__13/AXI_WDATA_PARITY[14]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => FIFO_READ_tdata(114),
      I1 => FIFO_READ_tdata(115),
      I2 => FIFO_READ_tdata(112),
      I3 => FIFO_READ_tdata(113),
      O => \AXI_WDATA_PARITY0_inferred__13/AXI_WDATA_PARITY[14]_INST_0_i_1_n_0\
    );
\AXI_WDATA_PARITY0_inferred__14/AXI_WDATA_PARITY[15]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => FIFO_READ_tdata(125),
      I1 => FIFO_READ_tdata(124),
      I2 => FIFO_READ_tdata(127),
      I3 => FIFO_READ_tdata(126),
      I4 => \AXI_WDATA_PARITY0_inferred__14/AXI_WDATA_PARITY[15]_INST_0_i_1_n_0\,
      O => AXI_WDATA_PARITY(15)
    );
\AXI_WDATA_PARITY0_inferred__14/AXI_WDATA_PARITY[15]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => FIFO_READ_tdata(122),
      I1 => FIFO_READ_tdata(123),
      I2 => FIFO_READ_tdata(120),
      I3 => FIFO_READ_tdata(121),
      O => \AXI_WDATA_PARITY0_inferred__14/AXI_WDATA_PARITY[15]_INST_0_i_1_n_0\
    );
\AXI_WDATA_PARITY0_inferred__15/AXI_WDATA_PARITY[16]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => FIFO_READ_tdata(133),
      I1 => FIFO_READ_tdata(132),
      I2 => FIFO_READ_tdata(135),
      I3 => FIFO_READ_tdata(134),
      I4 => \AXI_WDATA_PARITY0_inferred__15/AXI_WDATA_PARITY[16]_INST_0_i_1_n_0\,
      O => AXI_WDATA_PARITY(16)
    );
\AXI_WDATA_PARITY0_inferred__15/AXI_WDATA_PARITY[16]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => FIFO_READ_tdata(130),
      I1 => FIFO_READ_tdata(131),
      I2 => FIFO_READ_tdata(128),
      I3 => FIFO_READ_tdata(129),
      O => \AXI_WDATA_PARITY0_inferred__15/AXI_WDATA_PARITY[16]_INST_0_i_1_n_0\
    );
\AXI_WDATA_PARITY0_inferred__16/AXI_WDATA_PARITY[17]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => FIFO_READ_tdata(141),
      I1 => FIFO_READ_tdata(140),
      I2 => FIFO_READ_tdata(143),
      I3 => FIFO_READ_tdata(142),
      I4 => \AXI_WDATA_PARITY0_inferred__16/AXI_WDATA_PARITY[17]_INST_0_i_1_n_0\,
      O => AXI_WDATA_PARITY(17)
    );
\AXI_WDATA_PARITY0_inferred__16/AXI_WDATA_PARITY[17]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => FIFO_READ_tdata(138),
      I1 => FIFO_READ_tdata(139),
      I2 => FIFO_READ_tdata(136),
      I3 => FIFO_READ_tdata(137),
      O => \AXI_WDATA_PARITY0_inferred__16/AXI_WDATA_PARITY[17]_INST_0_i_1_n_0\
    );
\AXI_WDATA_PARITY0_inferred__17/AXI_WDATA_PARITY[18]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => FIFO_READ_tdata(149),
      I1 => FIFO_READ_tdata(148),
      I2 => FIFO_READ_tdata(151),
      I3 => FIFO_READ_tdata(150),
      I4 => \AXI_WDATA_PARITY0_inferred__17/AXI_WDATA_PARITY[18]_INST_0_i_1_n_0\,
      O => AXI_WDATA_PARITY(18)
    );
\AXI_WDATA_PARITY0_inferred__17/AXI_WDATA_PARITY[18]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => FIFO_READ_tdata(146),
      I1 => FIFO_READ_tdata(147),
      I2 => FIFO_READ_tdata(144),
      I3 => FIFO_READ_tdata(145),
      O => \AXI_WDATA_PARITY0_inferred__17/AXI_WDATA_PARITY[18]_INST_0_i_1_n_0\
    );
\AXI_WDATA_PARITY0_inferred__18/AXI_WDATA_PARITY[19]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => FIFO_READ_tdata(157),
      I1 => FIFO_READ_tdata(156),
      I2 => FIFO_READ_tdata(159),
      I3 => FIFO_READ_tdata(158),
      I4 => \AXI_WDATA_PARITY0_inferred__18/AXI_WDATA_PARITY[19]_INST_0_i_1_n_0\,
      O => AXI_WDATA_PARITY(19)
    );
\AXI_WDATA_PARITY0_inferred__18/AXI_WDATA_PARITY[19]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => FIFO_READ_tdata(154),
      I1 => FIFO_READ_tdata(155),
      I2 => FIFO_READ_tdata(152),
      I3 => FIFO_READ_tdata(153),
      O => \AXI_WDATA_PARITY0_inferred__18/AXI_WDATA_PARITY[19]_INST_0_i_1_n_0\
    );
\AXI_WDATA_PARITY0_inferred__19/AXI_WDATA_PARITY[20]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => FIFO_READ_tdata(165),
      I1 => FIFO_READ_tdata(164),
      I2 => FIFO_READ_tdata(167),
      I3 => FIFO_READ_tdata(166),
      I4 => \AXI_WDATA_PARITY0_inferred__19/AXI_WDATA_PARITY[20]_INST_0_i_1_n_0\,
      O => AXI_WDATA_PARITY(20)
    );
\AXI_WDATA_PARITY0_inferred__19/AXI_WDATA_PARITY[20]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => FIFO_READ_tdata(162),
      I1 => FIFO_READ_tdata(163),
      I2 => FIFO_READ_tdata(160),
      I3 => FIFO_READ_tdata(161),
      O => \AXI_WDATA_PARITY0_inferred__19/AXI_WDATA_PARITY[20]_INST_0_i_1_n_0\
    );
\AXI_WDATA_PARITY0_inferred__2/AXI_WDATA_PARITY[3]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => FIFO_READ_tdata(29),
      I1 => FIFO_READ_tdata(28),
      I2 => FIFO_READ_tdata(31),
      I3 => FIFO_READ_tdata(30),
      I4 => \AXI_WDATA_PARITY0_inferred__2/AXI_WDATA_PARITY[3]_INST_0_i_1_n_0\,
      O => AXI_WDATA_PARITY(3)
    );
\AXI_WDATA_PARITY0_inferred__2/AXI_WDATA_PARITY[3]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => FIFO_READ_tdata(26),
      I1 => FIFO_READ_tdata(27),
      I2 => FIFO_READ_tdata(24),
      I3 => FIFO_READ_tdata(25),
      O => \AXI_WDATA_PARITY0_inferred__2/AXI_WDATA_PARITY[3]_INST_0_i_1_n_0\
    );
\AXI_WDATA_PARITY0_inferred__20/AXI_WDATA_PARITY[21]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => FIFO_READ_tdata(173),
      I1 => FIFO_READ_tdata(172),
      I2 => FIFO_READ_tdata(175),
      I3 => FIFO_READ_tdata(174),
      I4 => \AXI_WDATA_PARITY0_inferred__20/AXI_WDATA_PARITY[21]_INST_0_i_1_n_0\,
      O => AXI_WDATA_PARITY(21)
    );
\AXI_WDATA_PARITY0_inferred__20/AXI_WDATA_PARITY[21]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => FIFO_READ_tdata(170),
      I1 => FIFO_READ_tdata(171),
      I2 => FIFO_READ_tdata(168),
      I3 => FIFO_READ_tdata(169),
      O => \AXI_WDATA_PARITY0_inferred__20/AXI_WDATA_PARITY[21]_INST_0_i_1_n_0\
    );
\AXI_WDATA_PARITY0_inferred__21/AXI_WDATA_PARITY[22]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => FIFO_READ_tdata(181),
      I1 => FIFO_READ_tdata(180),
      I2 => FIFO_READ_tdata(183),
      I3 => FIFO_READ_tdata(182),
      I4 => \AXI_WDATA_PARITY0_inferred__21/AXI_WDATA_PARITY[22]_INST_0_i_1_n_0\,
      O => AXI_WDATA_PARITY(22)
    );
\AXI_WDATA_PARITY0_inferred__21/AXI_WDATA_PARITY[22]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => FIFO_READ_tdata(178),
      I1 => FIFO_READ_tdata(179),
      I2 => FIFO_READ_tdata(176),
      I3 => FIFO_READ_tdata(177),
      O => \AXI_WDATA_PARITY0_inferred__21/AXI_WDATA_PARITY[22]_INST_0_i_1_n_0\
    );
\AXI_WDATA_PARITY0_inferred__22/AXI_WDATA_PARITY[23]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => FIFO_READ_tdata(189),
      I1 => FIFO_READ_tdata(188),
      I2 => FIFO_READ_tdata(191),
      I3 => FIFO_READ_tdata(190),
      I4 => \AXI_WDATA_PARITY0_inferred__22/AXI_WDATA_PARITY[23]_INST_0_i_1_n_0\,
      O => AXI_WDATA_PARITY(23)
    );
\AXI_WDATA_PARITY0_inferred__22/AXI_WDATA_PARITY[23]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => FIFO_READ_tdata(186),
      I1 => FIFO_READ_tdata(187),
      I2 => FIFO_READ_tdata(184),
      I3 => FIFO_READ_tdata(185),
      O => \AXI_WDATA_PARITY0_inferred__22/AXI_WDATA_PARITY[23]_INST_0_i_1_n_0\
    );
\AXI_WDATA_PARITY0_inferred__23/AXI_WDATA_PARITY[24]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => FIFO_READ_tdata(197),
      I1 => FIFO_READ_tdata(196),
      I2 => FIFO_READ_tdata(199),
      I3 => FIFO_READ_tdata(198),
      I4 => \AXI_WDATA_PARITY0_inferred__23/AXI_WDATA_PARITY[24]_INST_0_i_1_n_0\,
      O => AXI_WDATA_PARITY(24)
    );
\AXI_WDATA_PARITY0_inferred__23/AXI_WDATA_PARITY[24]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => FIFO_READ_tdata(194),
      I1 => FIFO_READ_tdata(195),
      I2 => FIFO_READ_tdata(192),
      I3 => FIFO_READ_tdata(193),
      O => \AXI_WDATA_PARITY0_inferred__23/AXI_WDATA_PARITY[24]_INST_0_i_1_n_0\
    );
\AXI_WDATA_PARITY0_inferred__24/AXI_WDATA_PARITY[25]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => FIFO_READ_tdata(205),
      I1 => FIFO_READ_tdata(204),
      I2 => FIFO_READ_tdata(207),
      I3 => FIFO_READ_tdata(206),
      I4 => \AXI_WDATA_PARITY0_inferred__24/AXI_WDATA_PARITY[25]_INST_0_i_1_n_0\,
      O => AXI_WDATA_PARITY(25)
    );
\AXI_WDATA_PARITY0_inferred__24/AXI_WDATA_PARITY[25]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => FIFO_READ_tdata(202),
      I1 => FIFO_READ_tdata(203),
      I2 => FIFO_READ_tdata(200),
      I3 => FIFO_READ_tdata(201),
      O => \AXI_WDATA_PARITY0_inferred__24/AXI_WDATA_PARITY[25]_INST_0_i_1_n_0\
    );
\AXI_WDATA_PARITY0_inferred__25/AXI_WDATA_PARITY[26]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => FIFO_READ_tdata(213),
      I1 => FIFO_READ_tdata(212),
      I2 => FIFO_READ_tdata(215),
      I3 => FIFO_READ_tdata(214),
      I4 => \AXI_WDATA_PARITY0_inferred__25/AXI_WDATA_PARITY[26]_INST_0_i_1_n_0\,
      O => AXI_WDATA_PARITY(26)
    );
\AXI_WDATA_PARITY0_inferred__25/AXI_WDATA_PARITY[26]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => FIFO_READ_tdata(210),
      I1 => FIFO_READ_tdata(211),
      I2 => FIFO_READ_tdata(208),
      I3 => FIFO_READ_tdata(209),
      O => \AXI_WDATA_PARITY0_inferred__25/AXI_WDATA_PARITY[26]_INST_0_i_1_n_0\
    );
\AXI_WDATA_PARITY0_inferred__26/AXI_WDATA_PARITY[27]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => FIFO_READ_tdata(221),
      I1 => FIFO_READ_tdata(220),
      I2 => FIFO_READ_tdata(223),
      I3 => FIFO_READ_tdata(222),
      I4 => \AXI_WDATA_PARITY0_inferred__26/AXI_WDATA_PARITY[27]_INST_0_i_1_n_0\,
      O => AXI_WDATA_PARITY(27)
    );
\AXI_WDATA_PARITY0_inferred__26/AXI_WDATA_PARITY[27]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => FIFO_READ_tdata(218),
      I1 => FIFO_READ_tdata(219),
      I2 => FIFO_READ_tdata(216),
      I3 => FIFO_READ_tdata(217),
      O => \AXI_WDATA_PARITY0_inferred__26/AXI_WDATA_PARITY[27]_INST_0_i_1_n_0\
    );
\AXI_WDATA_PARITY0_inferred__27/AXI_WDATA_PARITY[28]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => FIFO_READ_tdata(229),
      I1 => FIFO_READ_tdata(228),
      I2 => FIFO_READ_tdata(231),
      I3 => FIFO_READ_tdata(230),
      I4 => \AXI_WDATA_PARITY0_inferred__27/AXI_WDATA_PARITY[28]_INST_0_i_1_n_0\,
      O => AXI_WDATA_PARITY(28)
    );
\AXI_WDATA_PARITY0_inferred__27/AXI_WDATA_PARITY[28]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => FIFO_READ_tdata(226),
      I1 => FIFO_READ_tdata(227),
      I2 => FIFO_READ_tdata(224),
      I3 => FIFO_READ_tdata(225),
      O => \AXI_WDATA_PARITY0_inferred__27/AXI_WDATA_PARITY[28]_INST_0_i_1_n_0\
    );
\AXI_WDATA_PARITY0_inferred__28/AXI_WDATA_PARITY[29]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => FIFO_READ_tdata(237),
      I1 => FIFO_READ_tdata(236),
      I2 => FIFO_READ_tdata(239),
      I3 => FIFO_READ_tdata(238),
      I4 => \AXI_WDATA_PARITY0_inferred__28/AXI_WDATA_PARITY[29]_INST_0_i_1_n_0\,
      O => AXI_WDATA_PARITY(29)
    );
\AXI_WDATA_PARITY0_inferred__28/AXI_WDATA_PARITY[29]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => FIFO_READ_tdata(234),
      I1 => FIFO_READ_tdata(235),
      I2 => FIFO_READ_tdata(232),
      I3 => FIFO_READ_tdata(233),
      O => \AXI_WDATA_PARITY0_inferred__28/AXI_WDATA_PARITY[29]_INST_0_i_1_n_0\
    );
\AXI_WDATA_PARITY0_inferred__29/AXI_WDATA_PARITY[30]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => FIFO_READ_tdata(245),
      I1 => FIFO_READ_tdata(244),
      I2 => FIFO_READ_tdata(247),
      I3 => FIFO_READ_tdata(246),
      I4 => \AXI_WDATA_PARITY0_inferred__29/AXI_WDATA_PARITY[30]_INST_0_i_1_n_0\,
      O => AXI_WDATA_PARITY(30)
    );
\AXI_WDATA_PARITY0_inferred__29/AXI_WDATA_PARITY[30]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => FIFO_READ_tdata(242),
      I1 => FIFO_READ_tdata(243),
      I2 => FIFO_READ_tdata(240),
      I3 => FIFO_READ_tdata(241),
      O => \AXI_WDATA_PARITY0_inferred__29/AXI_WDATA_PARITY[30]_INST_0_i_1_n_0\
    );
\AXI_WDATA_PARITY0_inferred__3/AXI_WDATA_PARITY[4]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => FIFO_READ_tdata(37),
      I1 => FIFO_READ_tdata(36),
      I2 => FIFO_READ_tdata(39),
      I3 => FIFO_READ_tdata(38),
      I4 => \AXI_WDATA_PARITY0_inferred__3/AXI_WDATA_PARITY[4]_INST_0_i_1_n_0\,
      O => AXI_WDATA_PARITY(4)
    );
\AXI_WDATA_PARITY0_inferred__3/AXI_WDATA_PARITY[4]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => FIFO_READ_tdata(34),
      I1 => FIFO_READ_tdata(35),
      I2 => FIFO_READ_tdata(32),
      I3 => FIFO_READ_tdata(33),
      O => \AXI_WDATA_PARITY0_inferred__3/AXI_WDATA_PARITY[4]_INST_0_i_1_n_0\
    );
\AXI_WDATA_PARITY0_inferred__30/AXI_WDATA_PARITY[31]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => FIFO_READ_tdata(253),
      I1 => FIFO_READ_tdata(252),
      I2 => FIFO_READ_tdata(255),
      I3 => FIFO_READ_tdata(254),
      I4 => \AXI_WDATA_PARITY0_inferred__30/AXI_WDATA_PARITY[31]_INST_0_i_1_n_0\,
      O => AXI_WDATA_PARITY(31)
    );
\AXI_WDATA_PARITY0_inferred__30/AXI_WDATA_PARITY[31]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => FIFO_READ_tdata(250),
      I1 => FIFO_READ_tdata(251),
      I2 => FIFO_READ_tdata(248),
      I3 => FIFO_READ_tdata(249),
      O => \AXI_WDATA_PARITY0_inferred__30/AXI_WDATA_PARITY[31]_INST_0_i_1_n_0\
    );
\AXI_WDATA_PARITY0_inferred__4/AXI_WDATA_PARITY[5]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => FIFO_READ_tdata(45),
      I1 => FIFO_READ_tdata(44),
      I2 => FIFO_READ_tdata(47),
      I3 => FIFO_READ_tdata(46),
      I4 => \AXI_WDATA_PARITY0_inferred__4/AXI_WDATA_PARITY[5]_INST_0_i_1_n_0\,
      O => AXI_WDATA_PARITY(5)
    );
\AXI_WDATA_PARITY0_inferred__4/AXI_WDATA_PARITY[5]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => FIFO_READ_tdata(42),
      I1 => FIFO_READ_tdata(43),
      I2 => FIFO_READ_tdata(40),
      I3 => FIFO_READ_tdata(41),
      O => \AXI_WDATA_PARITY0_inferred__4/AXI_WDATA_PARITY[5]_INST_0_i_1_n_0\
    );
\AXI_WDATA_PARITY0_inferred__5/AXI_WDATA_PARITY[6]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => FIFO_READ_tdata(53),
      I1 => FIFO_READ_tdata(52),
      I2 => FIFO_READ_tdata(55),
      I3 => FIFO_READ_tdata(54),
      I4 => \AXI_WDATA_PARITY0_inferred__5/AXI_WDATA_PARITY[6]_INST_0_i_1_n_0\,
      O => AXI_WDATA_PARITY(6)
    );
\AXI_WDATA_PARITY0_inferred__5/AXI_WDATA_PARITY[6]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => FIFO_READ_tdata(50),
      I1 => FIFO_READ_tdata(51),
      I2 => FIFO_READ_tdata(48),
      I3 => FIFO_READ_tdata(49),
      O => \AXI_WDATA_PARITY0_inferred__5/AXI_WDATA_PARITY[6]_INST_0_i_1_n_0\
    );
\AXI_WDATA_PARITY0_inferred__6/AXI_WDATA_PARITY[7]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => FIFO_READ_tdata(61),
      I1 => FIFO_READ_tdata(60),
      I2 => FIFO_READ_tdata(63),
      I3 => FIFO_READ_tdata(62),
      I4 => \AXI_WDATA_PARITY0_inferred__6/AXI_WDATA_PARITY[7]_INST_0_i_1_n_0\,
      O => AXI_WDATA_PARITY(7)
    );
\AXI_WDATA_PARITY0_inferred__6/AXI_WDATA_PARITY[7]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => FIFO_READ_tdata(58),
      I1 => FIFO_READ_tdata(59),
      I2 => FIFO_READ_tdata(56),
      I3 => FIFO_READ_tdata(57),
      O => \AXI_WDATA_PARITY0_inferred__6/AXI_WDATA_PARITY[7]_INST_0_i_1_n_0\
    );
\AXI_WDATA_PARITY0_inferred__7/AXI_WDATA_PARITY[8]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => FIFO_READ_tdata(69),
      I1 => FIFO_READ_tdata(68),
      I2 => FIFO_READ_tdata(71),
      I3 => FIFO_READ_tdata(70),
      I4 => \AXI_WDATA_PARITY0_inferred__7/AXI_WDATA_PARITY[8]_INST_0_i_1_n_0\,
      O => AXI_WDATA_PARITY(8)
    );
\AXI_WDATA_PARITY0_inferred__7/AXI_WDATA_PARITY[8]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => FIFO_READ_tdata(66),
      I1 => FIFO_READ_tdata(67),
      I2 => FIFO_READ_tdata(64),
      I3 => FIFO_READ_tdata(65),
      O => \AXI_WDATA_PARITY0_inferred__7/AXI_WDATA_PARITY[8]_INST_0_i_1_n_0\
    );
\AXI_WDATA_PARITY0_inferred__8/AXI_WDATA_PARITY[9]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => FIFO_READ_tdata(77),
      I1 => FIFO_READ_tdata(76),
      I2 => FIFO_READ_tdata(79),
      I3 => FIFO_READ_tdata(78),
      I4 => \AXI_WDATA_PARITY0_inferred__8/AXI_WDATA_PARITY[9]_INST_0_i_1_n_0\,
      O => AXI_WDATA_PARITY(9)
    );
\AXI_WDATA_PARITY0_inferred__8/AXI_WDATA_PARITY[9]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => FIFO_READ_tdata(74),
      I1 => FIFO_READ_tdata(75),
      I2 => FIFO_READ_tdata(72),
      I3 => FIFO_READ_tdata(73),
      O => \AXI_WDATA_PARITY0_inferred__8/AXI_WDATA_PARITY[9]_INST_0_i_1_n_0\
    );
\AXI_WDATA_PARITY0_inferred__9/AXI_WDATA_PARITY[10]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => FIFO_READ_tdata(85),
      I1 => FIFO_READ_tdata(84),
      I2 => FIFO_READ_tdata(87),
      I3 => FIFO_READ_tdata(86),
      I4 => \AXI_WDATA_PARITY0_inferred__9/AXI_WDATA_PARITY[10]_INST_0_i_1_n_0\,
      O => AXI_WDATA_PARITY(10)
    );
\AXI_WDATA_PARITY0_inferred__9/AXI_WDATA_PARITY[10]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => FIFO_READ_tdata(82),
      I1 => FIFO_READ_tdata(83),
      I2 => FIFO_READ_tdata(80),
      I3 => FIFO_READ_tdata(81),
      O => \AXI_WDATA_PARITY0_inferred__9/AXI_WDATA_PARITY[10]_INST_0_i_1_n_0\
    );
\AXI_WDATA_PARITY[0]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => FIFO_READ_tdata(5),
      I1 => FIFO_READ_tdata(4),
      I2 => FIFO_READ_tdata(7),
      I3 => FIFO_READ_tdata(6),
      I4 => \AXI_WDATA_PARITY[0]_INST_0_i_1_n_0\,
      O => AXI_WDATA_PARITY(0)
    );
\AXI_WDATA_PARITY[0]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => FIFO_READ_tdata(2),
      I1 => FIFO_READ_tdata(3),
      I2 => FIFO_READ_tdata(0),
      I3 => FIFO_READ_tdata(1),
      O => \AXI_WDATA_PARITY[0]_INST_0_i_1_n_0\
    );
AXI_WLAST_INST_0: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF8000"
    )
        port map (
      I0 => writedata_count_reg(2),
      I1 => writedata_count_reg(3),
      I2 => writedata_count_reg(0),
      I3 => writedata_count_reg(1),
      I4 => writedata_FSM_next1,
      O => AXI_WLAST
    );
FIFO_WRITE_tlast0_carry: unisim.vcomponents.CARRY8
     port map (
      CI => '1',
      CI_TOP => '0',
      CO(7) => FIFO_WRITE_tlast0_carry_n_0,
      CO(6) => FIFO_WRITE_tlast0_carry_n_1,
      CO(5) => FIFO_WRITE_tlast0_carry_n_2,
      CO(4) => FIFO_WRITE_tlast0_carry_n_3,
      CO(3) => FIFO_WRITE_tlast0_carry_n_4,
      CO(2) => FIFO_WRITE_tlast0_carry_n_5,
      CO(1) => FIFO_WRITE_tlast0_carry_n_6,
      CO(0) => FIFO_WRITE_tlast0_carry_n_7,
      DI(7 downto 0) => B"00000000",
      O(7 downto 0) => NLW_FIFO_WRITE_tlast0_carry_O_UNCONNECTED(7 downto 0),
      S(7) => FIFO_WRITE_tlast0_carry_i_1_n_0,
      S(6) => FIFO_WRITE_tlast0_carry_i_2_n_0,
      S(5) => FIFO_WRITE_tlast0_carry_i_3_n_0,
      S(4) => FIFO_WRITE_tlast0_carry_i_4_n_0,
      S(3) => FIFO_WRITE_tlast0_carry_i_5_n_0,
      S(2) => FIFO_WRITE_tlast0_carry_i_6_n_0,
      S(1) => FIFO_WRITE_tlast0_carry_i_7_n_0,
      S(0) => FIFO_WRITE_tlast0_carry_i_8_n_0
    );
\FIFO_WRITE_tlast0_carry__0\: unisim.vcomponents.CARRY8
     port map (
      CI => FIFO_WRITE_tlast0_carry_n_0,
      CI_TOP => '0',
      CO(7 downto 3) => \NLW_FIFO_WRITE_tlast0_carry__0_CO_UNCONNECTED\(7 downto 3),
      CO(2) => FIFO_WRITE_tlast0,
      CO(1) => \FIFO_WRITE_tlast0_carry__0_n_6\,
      CO(0) => \FIFO_WRITE_tlast0_carry__0_n_7\,
      DI(7 downto 0) => B"00000000",
      O(7 downto 0) => \NLW_FIFO_WRITE_tlast0_carry__0_O_UNCONNECTED\(7 downto 0),
      S(7 downto 3) => B"00000",
      S(2) => \FIFO_WRITE_tlast0_carry__0_i_1_n_0\,
      S(1) => \FIFO_WRITE_tlast0_carry__0_i_2_n_0\,
      S(0) => \FIFO_WRITE_tlast0_carry__0_i_3_n_0\
    );
\FIFO_WRITE_tlast0_carry__0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => readdata_count_reg(30),
      I1 => FIFO_WRITE_tlast1(30),
      I2 => FIFO_WRITE_tlast1(31),
      I3 => readdata_count_reg(31),
      O => \FIFO_WRITE_tlast0_carry__0_i_1_n_0\
    );
\FIFO_WRITE_tlast0_carry__0_i_10\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => read_blockamount(26),
      O => \FIFO_WRITE_tlast0_carry__0_i_10_n_0\
    );
\FIFO_WRITE_tlast0_carry__0_i_11\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => read_blockamount(25),
      O => \FIFO_WRITE_tlast0_carry__0_i_11_n_0\
    );
\FIFO_WRITE_tlast0_carry__0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => readdata_count_reg(28),
      I1 => FIFO_WRITE_tlast1(28),
      I2 => readdata_count_reg(27),
      I3 => FIFO_WRITE_tlast1(27),
      I4 => FIFO_WRITE_tlast1(29),
      I5 => readdata_count_reg(29),
      O => \FIFO_WRITE_tlast0_carry__0_i_2_n_0\
    );
\FIFO_WRITE_tlast0_carry__0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => readdata_count_reg(25),
      I1 => FIFO_WRITE_tlast1(25),
      I2 => readdata_count_reg(24),
      I3 => FIFO_WRITE_tlast1(24),
      I4 => FIFO_WRITE_tlast1(26),
      I5 => readdata_count_reg(26),
      O => \FIFO_WRITE_tlast0_carry__0_i_3_n_0\
    );
\FIFO_WRITE_tlast0_carry__0_i_4\: unisim.vcomponents.CARRY8
     port map (
      CI => FIFO_WRITE_tlast0_carry_i_9_n_0,
      CI_TOP => '0',
      CO(7 downto 6) => \NLW_FIFO_WRITE_tlast0_carry__0_i_4_CO_UNCONNECTED\(7 downto 6),
      CO(5) => \FIFO_WRITE_tlast0_carry__0_i_4_n_2\,
      CO(4) => \FIFO_WRITE_tlast0_carry__0_i_4_n_3\,
      CO(3) => \FIFO_WRITE_tlast0_carry__0_i_4_n_4\,
      CO(2) => \FIFO_WRITE_tlast0_carry__0_i_4_n_5\,
      CO(1) => \FIFO_WRITE_tlast0_carry__0_i_4_n_6\,
      CO(0) => \FIFO_WRITE_tlast0_carry__0_i_4_n_7\,
      DI(7 downto 6) => B"00",
      DI(5 downto 0) => read_blockamount(30 downto 25),
      O(7) => \NLW_FIFO_WRITE_tlast0_carry__0_i_4_O_UNCONNECTED\(7),
      O(6 downto 0) => FIFO_WRITE_tlast1(31 downto 25),
      S(7) => '0',
      S(6) => \FIFO_WRITE_tlast0_carry__0_i_5_n_0\,
      S(5) => \FIFO_WRITE_tlast0_carry__0_i_6_n_0\,
      S(4) => \FIFO_WRITE_tlast0_carry__0_i_7_n_0\,
      S(3) => \FIFO_WRITE_tlast0_carry__0_i_8_n_0\,
      S(2) => \FIFO_WRITE_tlast0_carry__0_i_9_n_0\,
      S(1) => \FIFO_WRITE_tlast0_carry__0_i_10_n_0\,
      S(0) => \FIFO_WRITE_tlast0_carry__0_i_11_n_0\
    );
\FIFO_WRITE_tlast0_carry__0_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => read_blockamount(31),
      O => \FIFO_WRITE_tlast0_carry__0_i_5_n_0\
    );
\FIFO_WRITE_tlast0_carry__0_i_6\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => read_blockamount(30),
      O => \FIFO_WRITE_tlast0_carry__0_i_6_n_0\
    );
\FIFO_WRITE_tlast0_carry__0_i_7\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => read_blockamount(29),
      O => \FIFO_WRITE_tlast0_carry__0_i_7_n_0\
    );
\FIFO_WRITE_tlast0_carry__0_i_8\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => read_blockamount(28),
      O => \FIFO_WRITE_tlast0_carry__0_i_8_n_0\
    );
\FIFO_WRITE_tlast0_carry__0_i_9\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => read_blockamount(27),
      O => \FIFO_WRITE_tlast0_carry__0_i_9_n_0\
    );
FIFO_WRITE_tlast0_carry_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => readdata_count_reg(22),
      I1 => FIFO_WRITE_tlast1(22),
      I2 => readdata_count_reg(21),
      I3 => FIFO_WRITE_tlast1(21),
      I4 => FIFO_WRITE_tlast1(23),
      I5 => readdata_count_reg(23),
      O => FIFO_WRITE_tlast0_carry_i_1_n_0
    );
FIFO_WRITE_tlast0_carry_i_10: unisim.vcomponents.CARRY8
     port map (
      CI => FIFO_WRITE_tlast0_carry_i_11_n_0,
      CI_TOP => '0',
      CO(7) => FIFO_WRITE_tlast0_carry_i_10_n_0,
      CO(6) => FIFO_WRITE_tlast0_carry_i_10_n_1,
      CO(5) => FIFO_WRITE_tlast0_carry_i_10_n_2,
      CO(4) => FIFO_WRITE_tlast0_carry_i_10_n_3,
      CO(3) => FIFO_WRITE_tlast0_carry_i_10_n_4,
      CO(2) => FIFO_WRITE_tlast0_carry_i_10_n_5,
      CO(1) => FIFO_WRITE_tlast0_carry_i_10_n_6,
      CO(0) => FIFO_WRITE_tlast0_carry_i_10_n_7,
      DI(7 downto 0) => read_blockamount(16 downto 9),
      O(7 downto 0) => FIFO_WRITE_tlast1(16 downto 9),
      S(7) => FIFO_WRITE_tlast0_carry_i_20_n_0,
      S(6) => FIFO_WRITE_tlast0_carry_i_21_n_0,
      S(5) => FIFO_WRITE_tlast0_carry_i_22_n_0,
      S(4) => FIFO_WRITE_tlast0_carry_i_23_n_0,
      S(3) => FIFO_WRITE_tlast0_carry_i_24_n_0,
      S(2) => FIFO_WRITE_tlast0_carry_i_25_n_0,
      S(1) => FIFO_WRITE_tlast0_carry_i_26_n_0,
      S(0) => FIFO_WRITE_tlast0_carry_i_27_n_0
    );
FIFO_WRITE_tlast0_carry_i_11: unisim.vcomponents.CARRY8
     port map (
      CI => read_blockamount(0),
      CI_TOP => '0',
      CO(7) => FIFO_WRITE_tlast0_carry_i_11_n_0,
      CO(6) => FIFO_WRITE_tlast0_carry_i_11_n_1,
      CO(5) => FIFO_WRITE_tlast0_carry_i_11_n_2,
      CO(4) => FIFO_WRITE_tlast0_carry_i_11_n_3,
      CO(3) => FIFO_WRITE_tlast0_carry_i_11_n_4,
      CO(2) => FIFO_WRITE_tlast0_carry_i_11_n_5,
      CO(1) => FIFO_WRITE_tlast0_carry_i_11_n_6,
      CO(0) => FIFO_WRITE_tlast0_carry_i_11_n_7,
      DI(7 downto 0) => read_blockamount(8 downto 1),
      O(7 downto 0) => FIFO_WRITE_tlast1(8 downto 1),
      S(7) => FIFO_WRITE_tlast0_carry_i_28_n_0,
      S(6) => FIFO_WRITE_tlast0_carry_i_29_n_0,
      S(5) => FIFO_WRITE_tlast0_carry_i_30_n_0,
      S(4) => FIFO_WRITE_tlast0_carry_i_31_n_0,
      S(3) => FIFO_WRITE_tlast0_carry_i_32_n_0,
      S(2) => FIFO_WRITE_tlast0_carry_i_33_n_0,
      S(1) => FIFO_WRITE_tlast0_carry_i_34_n_0,
      S(0) => FIFO_WRITE_tlast0_carry_i_35_n_0
    );
FIFO_WRITE_tlast0_carry_i_12: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => read_blockamount(24),
      O => FIFO_WRITE_tlast0_carry_i_12_n_0
    );
FIFO_WRITE_tlast0_carry_i_13: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => read_blockamount(23),
      O => FIFO_WRITE_tlast0_carry_i_13_n_0
    );
FIFO_WRITE_tlast0_carry_i_14: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => read_blockamount(22),
      O => FIFO_WRITE_tlast0_carry_i_14_n_0
    );
FIFO_WRITE_tlast0_carry_i_15: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => read_blockamount(21),
      O => FIFO_WRITE_tlast0_carry_i_15_n_0
    );
FIFO_WRITE_tlast0_carry_i_16: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => read_blockamount(20),
      O => FIFO_WRITE_tlast0_carry_i_16_n_0
    );
FIFO_WRITE_tlast0_carry_i_17: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => read_blockamount(19),
      O => FIFO_WRITE_tlast0_carry_i_17_n_0
    );
FIFO_WRITE_tlast0_carry_i_18: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => read_blockamount(18),
      O => FIFO_WRITE_tlast0_carry_i_18_n_0
    );
FIFO_WRITE_tlast0_carry_i_19: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => read_blockamount(17),
      O => FIFO_WRITE_tlast0_carry_i_19_n_0
    );
FIFO_WRITE_tlast0_carry_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => readdata_count_reg(19),
      I1 => FIFO_WRITE_tlast1(19),
      I2 => readdata_count_reg(18),
      I3 => FIFO_WRITE_tlast1(18),
      I4 => FIFO_WRITE_tlast1(20),
      I5 => readdata_count_reg(20),
      O => FIFO_WRITE_tlast0_carry_i_2_n_0
    );
FIFO_WRITE_tlast0_carry_i_20: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => read_blockamount(16),
      O => FIFO_WRITE_tlast0_carry_i_20_n_0
    );
FIFO_WRITE_tlast0_carry_i_21: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => read_blockamount(15),
      O => FIFO_WRITE_tlast0_carry_i_21_n_0
    );
FIFO_WRITE_tlast0_carry_i_22: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => read_blockamount(14),
      O => FIFO_WRITE_tlast0_carry_i_22_n_0
    );
FIFO_WRITE_tlast0_carry_i_23: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => read_blockamount(13),
      O => FIFO_WRITE_tlast0_carry_i_23_n_0
    );
FIFO_WRITE_tlast0_carry_i_24: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => read_blockamount(12),
      O => FIFO_WRITE_tlast0_carry_i_24_n_0
    );
FIFO_WRITE_tlast0_carry_i_25: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => read_blockamount(11),
      O => FIFO_WRITE_tlast0_carry_i_25_n_0
    );
FIFO_WRITE_tlast0_carry_i_26: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => read_blockamount(10),
      O => FIFO_WRITE_tlast0_carry_i_26_n_0
    );
FIFO_WRITE_tlast0_carry_i_27: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => read_blockamount(9),
      O => FIFO_WRITE_tlast0_carry_i_27_n_0
    );
FIFO_WRITE_tlast0_carry_i_28: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => read_blockamount(8),
      O => FIFO_WRITE_tlast0_carry_i_28_n_0
    );
FIFO_WRITE_tlast0_carry_i_29: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => read_blockamount(7),
      O => FIFO_WRITE_tlast0_carry_i_29_n_0
    );
FIFO_WRITE_tlast0_carry_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => readdata_count_reg(16),
      I1 => FIFO_WRITE_tlast1(16),
      I2 => readdata_count_reg(15),
      I3 => FIFO_WRITE_tlast1(15),
      I4 => FIFO_WRITE_tlast1(17),
      I5 => readdata_count_reg(17),
      O => FIFO_WRITE_tlast0_carry_i_3_n_0
    );
FIFO_WRITE_tlast0_carry_i_30: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => read_blockamount(6),
      O => FIFO_WRITE_tlast0_carry_i_30_n_0
    );
FIFO_WRITE_tlast0_carry_i_31: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => read_blockamount(5),
      O => FIFO_WRITE_tlast0_carry_i_31_n_0
    );
FIFO_WRITE_tlast0_carry_i_32: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => read_blockamount(4),
      O => FIFO_WRITE_tlast0_carry_i_32_n_0
    );
FIFO_WRITE_tlast0_carry_i_33: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => read_blockamount(3),
      O => FIFO_WRITE_tlast0_carry_i_33_n_0
    );
FIFO_WRITE_tlast0_carry_i_34: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => read_blockamount(2),
      O => FIFO_WRITE_tlast0_carry_i_34_n_0
    );
FIFO_WRITE_tlast0_carry_i_35: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => read_blockamount(1),
      O => FIFO_WRITE_tlast0_carry_i_35_n_0
    );
FIFO_WRITE_tlast0_carry_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => readdata_count_reg(13),
      I1 => FIFO_WRITE_tlast1(13),
      I2 => readdata_count_reg(12),
      I3 => FIFO_WRITE_tlast1(12),
      I4 => FIFO_WRITE_tlast1(14),
      I5 => readdata_count_reg(14),
      O => FIFO_WRITE_tlast0_carry_i_4_n_0
    );
FIFO_WRITE_tlast0_carry_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => readdata_count_reg(10),
      I1 => FIFO_WRITE_tlast1(10),
      I2 => readdata_count_reg(9),
      I3 => FIFO_WRITE_tlast1(9),
      I4 => FIFO_WRITE_tlast1(11),
      I5 => readdata_count_reg(11),
      O => FIFO_WRITE_tlast0_carry_i_5_n_0
    );
FIFO_WRITE_tlast0_carry_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => readdata_count_reg(7),
      I1 => FIFO_WRITE_tlast1(7),
      I2 => readdata_count_reg(6),
      I3 => FIFO_WRITE_tlast1(6),
      I4 => FIFO_WRITE_tlast1(8),
      I5 => readdata_count_reg(8),
      O => FIFO_WRITE_tlast0_carry_i_6_n_0
    );
FIFO_WRITE_tlast0_carry_i_7: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => readdata_count_reg(4),
      I1 => FIFO_WRITE_tlast1(4),
      I2 => readdata_count_reg(3),
      I3 => FIFO_WRITE_tlast1(3),
      I4 => FIFO_WRITE_tlast1(5),
      I5 => readdata_count_reg(5),
      O => FIFO_WRITE_tlast0_carry_i_7_n_0
    );
FIFO_WRITE_tlast0_carry_i_8: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0990000000000990"
    )
        port map (
      I0 => readdata_count_reg(1),
      I1 => FIFO_WRITE_tlast1(1),
      I2 => read_blockamount(0),
      I3 => readdata_count_reg(0),
      I4 => FIFO_WRITE_tlast1(2),
      I5 => readdata_count_reg(2),
      O => FIFO_WRITE_tlast0_carry_i_8_n_0
    );
FIFO_WRITE_tlast0_carry_i_9: unisim.vcomponents.CARRY8
     port map (
      CI => FIFO_WRITE_tlast0_carry_i_10_n_0,
      CI_TOP => '0',
      CO(7) => FIFO_WRITE_tlast0_carry_i_9_n_0,
      CO(6) => FIFO_WRITE_tlast0_carry_i_9_n_1,
      CO(5) => FIFO_WRITE_tlast0_carry_i_9_n_2,
      CO(4) => FIFO_WRITE_tlast0_carry_i_9_n_3,
      CO(3) => FIFO_WRITE_tlast0_carry_i_9_n_4,
      CO(2) => FIFO_WRITE_tlast0_carry_i_9_n_5,
      CO(1) => FIFO_WRITE_tlast0_carry_i_9_n_6,
      CO(0) => FIFO_WRITE_tlast0_carry_i_9_n_7,
      DI(7 downto 0) => read_blockamount(24 downto 17),
      O(7 downto 0) => FIFO_WRITE_tlast1(24 downto 17),
      S(7) => FIFO_WRITE_tlast0_carry_i_12_n_0,
      S(6) => FIFO_WRITE_tlast0_carry_i_13_n_0,
      S(5) => FIFO_WRITE_tlast0_carry_i_14_n_0,
      S(4) => FIFO_WRITE_tlast0_carry_i_15_n_0,
      S(3) => FIFO_WRITE_tlast0_carry_i_16_n_0,
      S(2) => FIFO_WRITE_tlast0_carry_i_17_n_0,
      S(1) => FIFO_WRITE_tlast0_carry_i_18_n_0,
      S(0) => FIFO_WRITE_tlast0_carry_i_19_n_0
    );
FIFO_WRITE_tlast_INST_0: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => FIFO_WRITE_tready,
      I1 => AXI_RVALID,
      I2 => FIFO_WRITE_tlast0,
      O => FIFO_WRITE_tlast
    );
\FSM_sequential_FSM_next_reg[0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \FSM_next__0\(0),
      G => \FSM_sequential_FSM_next_reg[2]_i_2_n_0\,
      GE => '1',
      Q => FSM_next(0)
    );
\FSM_sequential_FSM_next_reg[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"14151514"
    )
        port map (
      I0 => FSM(0),
      I1 => FSM(1),
      I2 => FSM(2),
      I3 => rd_running,
      I4 => wr_running,
      O => \FSM_next__0\(0)
    );
\FSM_sequential_FSM_next_reg[1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \FSM_next__0\(1),
      G => \FSM_sequential_FSM_next_reg[2]_i_2_n_0\,
      GE => '1',
      Q => FSM_next(1)
    );
\FSM_sequential_FSM_next_reg[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"000000F2"
    )
        port map (
      I0 => rd_running,
      I1 => wr_running,
      I2 => FSM(0),
      I3 => FSM(2),
      I4 => FSM(1),
      O => \FSM_next__0\(1)
    );
\FSM_sequential_FSM_next_reg[2]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \FSM_next__0\(2),
      G => \FSM_sequential_FSM_next_reg[2]_i_2_n_0\,
      GE => '1',
      Q => FSM_next(2)
    );
\FSM_sequential_FSM_next_reg[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"26"
    )
        port map (
      I0 => FSM(1),
      I1 => FSM(2),
      I2 => FSM(0),
      O => \FSM_next__0\(2)
    );
\FSM_sequential_FSM_next_reg[2]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7770"
    )
        port map (
      I0 => FSM(2),
      I1 => FSM(1),
      I2 => \FSM_sequential_FSM_next_reg[2]_i_3_n_0\,
      I3 => FSM(0),
      O => \FSM_sequential_FSM_next_reg[2]_i_2_n_0\
    );
\FSM_sequential_FSM_next_reg[2]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"D5D500D500D500D5"
    )
        port map (
      I0 => FSM(1),
      I1 => writedata_done_reg_n_0,
      I2 => writeaddr_done_reg_n_0,
      I3 => FSM(2),
      I4 => readaddr_done_reg_n_0,
      I5 => readdata_done_reg_n_0,
      O => \FSM_sequential_FSM_next_reg[2]_i_3_n_0\
    );
\FSM_sequential_FSM_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => FSM_next(0),
      Q => FSM(0),
      R => '0'
    );
\FSM_sequential_FSM_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => FSM_next(1),
      Q => FSM(1),
      R => '0'
    );
\FSM_sequential_FSM_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => FSM_next(2),
      Q => FSM(2),
      R => '0'
    );
\FSM_sequential_readaddr_FSM_next_reg[0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \readaddr_FSM_next__0\(0),
      G => \FSM_sequential_readaddr_FSM_next_reg[2]_i_2_n_0\,
      GE => '1',
      Q => readaddr_FSM_next(0)
    );
\FSM_sequential_readaddr_FSM_next_reg[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1111111111111110"
    )
        port map (
      I0 => readaddr_FSM(2),
      I1 => readaddr_FSM(0),
      I2 => readaddr_FSM(1),
      I3 => \FSM_sequential_readaddr_FSM_next_reg[1]_i_2_n_0\,
      I4 => \FSM_sequential_readaddr_FSM_next_reg[1]_i_3_n_0\,
      I5 => \FSM_sequential_readaddr_FSM_next_reg[1]_i_4_n_0\,
      O => \readaddr_FSM_next__0\(0)
    );
\FSM_sequential_readaddr_FSM_next_reg[1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \readaddr_FSM_next__0\(1),
      G => \FSM_sequential_readaddr_FSM_next_reg[2]_i_2_n_0\,
      GE => '1',
      Q => \FSM_sequential_readaddr_FSM_next_reg_n_0_[1]\
    );
\FSM_sequential_readaddr_FSM_next_reg[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000FF00FF0001"
    )
        port map (
      I0 => \FSM_sequential_readaddr_FSM_next_reg[1]_i_2_n_0\,
      I1 => \FSM_sequential_readaddr_FSM_next_reg[1]_i_3_n_0\,
      I2 => \FSM_sequential_readaddr_FSM_next_reg[1]_i_4_n_0\,
      I3 => readaddr_FSM(2),
      I4 => readaddr_FSM(1),
      I5 => readaddr_FSM(0),
      O => \readaddr_FSM_next__0\(1)
    );
\FSM_sequential_readaddr_FSM_next_reg[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \FSM_sequential_readaddr_FSM_next_reg[1]_i_5_n_0\,
      I1 => \FSM_sequential_readaddr_FSM_next_reg[1]_i_6_n_0\,
      I2 => \FSM_sequential_readaddr_FSM_next_reg[1]_i_7_n_0\,
      I3 => \FSM_sequential_readaddr_FSM_next_reg[1]_i_8_n_0\,
      I4 => read_blockamount(16),
      I5 => read_blockamount(17),
      O => \FSM_sequential_readaddr_FSM_next_reg[1]_i_2_n_0\
    );
\FSM_sequential_readaddr_FSM_next_reg[1]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \FSM_sequential_readaddr_FSM_next_reg[1]_i_9_n_0\,
      I1 => read_blockamount(26),
      I2 => read_blockamount(27),
      I3 => read_blockamount(28),
      I4 => read_blockamount(29),
      O => \FSM_sequential_readaddr_FSM_next_reg[1]_i_3_n_0\
    );
\FSM_sequential_readaddr_FSM_next_reg[1]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0F0F0E0"
    )
        port map (
      I0 => read_blockamount(1),
      I1 => read_blockamount(0),
      I2 => read_blockamount(4),
      I3 => read_blockamount(3),
      I4 => read_blockamount(2),
      O => \FSM_sequential_readaddr_FSM_next_reg[1]_i_4_n_0\
    );
\FSM_sequential_readaddr_FSM_next_reg[1]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => read_blockamount(25),
      I1 => read_blockamount(24),
      I2 => read_blockamount(23),
      I3 => read_blockamount(22),
      O => \FSM_sequential_readaddr_FSM_next_reg[1]_i_5_n_0\
    );
\FSM_sequential_readaddr_FSM_next_reg[1]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => read_blockamount(7),
      I1 => read_blockamount(6),
      I2 => read_blockamount(14),
      O => \FSM_sequential_readaddr_FSM_next_reg[1]_i_6_n_0\
    );
\FSM_sequential_readaddr_FSM_next_reg[1]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => read_blockamount(8),
      I1 => read_blockamount(11),
      I2 => read_blockamount(5),
      I3 => read_blockamount(9),
      O => \FSM_sequential_readaddr_FSM_next_reg[1]_i_7_n_0\
    );
\FSM_sequential_readaddr_FSM_next_reg[1]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => read_blockamount(12),
      I1 => read_blockamount(15),
      I2 => read_blockamount(10),
      I3 => read_blockamount(13),
      O => \FSM_sequential_readaddr_FSM_next_reg[1]_i_8_n_0\
    );
\FSM_sequential_readaddr_FSM_next_reg[1]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => read_blockamount(18),
      I1 => read_blockamount(19),
      I2 => read_blockamount(20),
      I3 => read_blockamount(21),
      I4 => read_blockamount(31),
      I5 => read_blockamount(30),
      O => \FSM_sequential_readaddr_FSM_next_reg[1]_i_9_n_0\
    );
\FSM_sequential_readaddr_FSM_next_reg[2]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \readaddr_FSM_next__0\(2),
      G => \FSM_sequential_readaddr_FSM_next_reg[2]_i_2_n_0\,
      GE => '1',
      Q => readaddr_FSM_next(2)
    );
\FSM_sequential_readaddr_FSM_next_reg[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => readaddr_FSM(1),
      I1 => readaddr_FSM(0),
      I2 => readaddr_FSM(2),
      I3 => read_start_reg_n_0,
      O => \readaddr_FSM_next__0\(2)
    );
\FSM_sequential_readaddr_FSM_next_reg[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000005FCA500A"
    )
        port map (
      I0 => read_start_reg_n_0,
      I1 => readaddr_FSM_next17_in,
      I2 => readaddr_FSM(0),
      I3 => readaddr_FSM(1),
      I4 => \FSM_sequential_readaddr_FSM_next_reg[2]_i_3_n_0\,
      I5 => readaddr_FSM(2),
      O => \FSM_sequential_readaddr_FSM_next_reg[2]_i_2_n_0\
    );
\FSM_sequential_readaddr_FSM_next_reg[2]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^axi_arvalid_reg_0\,
      I1 => AXI_ARREADY,
      O => \FSM_sequential_readaddr_FSM_next_reg[2]_i_3_n_0\
    );
\FSM_sequential_readaddr_FSM_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => readaddr_FSM_next(0),
      Q => readaddr_FSM(0),
      R => '0'
    );
\FSM_sequential_readaddr_FSM_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_sequential_readaddr_FSM_next_reg_n_0_[1]\,
      Q => readaddr_FSM(1),
      R => '0'
    );
\FSM_sequential_readaddr_FSM_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => readaddr_FSM_next(2),
      Q => readaddr_FSM(2),
      R => '0'
    );
\FSM_sequential_readdata_FSM_next_reg[0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \readdata_FSM_next__0\(0),
      G => \FSM_sequential_readdata_FSM_next_reg[2]_i_2_n_0\,
      GE => '1',
      Q => readdata_FSM_next(0)
    );
\FSM_sequential_readdata_FSM_next_reg[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => readdata_FSM(0),
      I1 => readdata_FSM(2),
      O => \readdata_FSM_next__0\(0)
    );
\FSM_sequential_readdata_FSM_next_reg[1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \readdata_FSM_next__0\(1),
      G => \FSM_sequential_readdata_FSM_next_reg[2]_i_2_n_0\,
      GE => '1',
      Q => \FSM_sequential_readdata_FSM_next_reg_n_0_[1]\
    );
\FSM_sequential_readdata_FSM_next_reg[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"06"
    )
        port map (
      I0 => readdata_FSM(0),
      I1 => readdata_FSM(1),
      I2 => readdata_FSM(2),
      O => \readdata_FSM_next__0\(1)
    );
\FSM_sequential_readdata_FSM_next_reg[2]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \readdata_FSM_next__0\(2),
      G => \FSM_sequential_readdata_FSM_next_reg[2]_i_2_n_0\,
      GE => '1',
      Q => readdata_FSM_next(2)
    );
\FSM_sequential_readdata_FSM_next_reg[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4000"
    )
        port map (
      I0 => readdata_FSM(2),
      I1 => readdata_FSM(1),
      I2 => read_start_reg_n_0,
      I3 => readdata_FSM(0),
      O => \readdata_FSM_next__0\(2)
    );
\FSM_sequential_readdata_FSM_next_reg[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"050F0C0A050F000A"
    )
        port map (
      I0 => read_start_reg_n_0,
      I1 => \FSM_sequential_readdata_FSM_next_reg[2]_i_3_n_0\,
      I2 => readdata_FSM(2),
      I3 => readdata_FSM(0),
      I4 => readdata_FSM(1),
      I5 => FIFO_WRITE_tlast0,
      O => \FSM_sequential_readdata_FSM_next_reg[2]_i_2_n_0\
    );
\FSM_sequential_readdata_FSM_next_reg[2]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => AXI_RVALID,
      I1 => FIFO_WRITE_tready,
      O => \FSM_sequential_readdata_FSM_next_reg[2]_i_3_n_0\
    );
\FSM_sequential_readdata_FSM_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => readdata_FSM_next(0),
      Q => readdata_FSM(0),
      R => '0'
    );
\FSM_sequential_readdata_FSM_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_sequential_readdata_FSM_next_reg_n_0_[1]\,
      Q => readdata_FSM(1),
      R => '0'
    );
\FSM_sequential_readdata_FSM_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => readdata_FSM_next(2),
      Q => readdata_FSM(2),
      R => '0'
    );
\FSM_sequential_writeaddr_FSM_next_reg[0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \writeaddr_FSM_next__0\(0),
      G => \FSM_sequential_writeaddr_FSM_next_reg[2]_i_2_n_0\,
      GE => '1',
      Q => writeaddr_FSM_next(0)
    );
\FSM_sequential_writeaddr_FSM_next_reg[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1111111111111110"
    )
        port map (
      I0 => writeaddr_FSM(2),
      I1 => writeaddr_FSM(0),
      I2 => writeaddr_FSM(1),
      I3 => \FSM_sequential_writeaddr_FSM_next_reg[1]_i_2_n_0\,
      I4 => \FSM_sequential_writeaddr_FSM_next_reg[1]_i_3_n_0\,
      I5 => \FSM_sequential_writeaddr_FSM_next_reg[1]_i_4_n_0\,
      O => \writeaddr_FSM_next__0\(0)
    );
\FSM_sequential_writeaddr_FSM_next_reg[1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \writeaddr_FSM_next__0\(1),
      G => \FSM_sequential_writeaddr_FSM_next_reg[2]_i_2_n_0\,
      GE => '1',
      Q => \FSM_sequential_writeaddr_FSM_next_reg_n_0_[1]\
    );
\FSM_sequential_writeaddr_FSM_next_reg[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000FF00FF0001"
    )
        port map (
      I0 => \FSM_sequential_writeaddr_FSM_next_reg[1]_i_2_n_0\,
      I1 => \FSM_sequential_writeaddr_FSM_next_reg[1]_i_3_n_0\,
      I2 => \FSM_sequential_writeaddr_FSM_next_reg[1]_i_4_n_0\,
      I3 => writeaddr_FSM(2),
      I4 => writeaddr_FSM(1),
      I5 => writeaddr_FSM(0),
      O => \writeaddr_FSM_next__0\(1)
    );
\FSM_sequential_writeaddr_FSM_next_reg[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \FSM_sequential_writeaddr_FSM_next_reg[1]_i_5_n_0\,
      I1 => \FSM_sequential_writeaddr_FSM_next_reg[1]_i_6_n_0\,
      I2 => \FSM_sequential_writeaddr_FSM_next_reg[1]_i_7_n_0\,
      I3 => \FSM_sequential_writeaddr_FSM_next_reg[1]_i_8_n_0\,
      I4 => write_blockamount(16),
      I5 => write_blockamount(17),
      O => \FSM_sequential_writeaddr_FSM_next_reg[1]_i_2_n_0\
    );
\FSM_sequential_writeaddr_FSM_next_reg[1]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \FSM_sequential_writeaddr_FSM_next_reg[1]_i_9_n_0\,
      I1 => write_blockamount(26),
      I2 => write_blockamount(27),
      I3 => write_blockamount(28),
      I4 => write_blockamount(29),
      O => \FSM_sequential_writeaddr_FSM_next_reg[1]_i_3_n_0\
    );
\FSM_sequential_writeaddr_FSM_next_reg[1]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0F0F0E0"
    )
        port map (
      I0 => write_blockamount(1),
      I1 => write_blockamount(0),
      I2 => write_blockamount(4),
      I3 => write_blockamount(3),
      I4 => write_blockamount(2),
      O => \FSM_sequential_writeaddr_FSM_next_reg[1]_i_4_n_0\
    );
\FSM_sequential_writeaddr_FSM_next_reg[1]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => write_blockamount(25),
      I1 => write_blockamount(24),
      I2 => write_blockamount(23),
      I3 => write_blockamount(22),
      O => \FSM_sequential_writeaddr_FSM_next_reg[1]_i_5_n_0\
    );
\FSM_sequential_writeaddr_FSM_next_reg[1]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => write_blockamount(7),
      I1 => write_blockamount(6),
      I2 => write_blockamount(14),
      O => \FSM_sequential_writeaddr_FSM_next_reg[1]_i_6_n_0\
    );
\FSM_sequential_writeaddr_FSM_next_reg[1]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => write_blockamount(8),
      I1 => write_blockamount(11),
      I2 => write_blockamount(5),
      I3 => write_blockamount(9),
      O => \FSM_sequential_writeaddr_FSM_next_reg[1]_i_7_n_0\
    );
\FSM_sequential_writeaddr_FSM_next_reg[1]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => write_blockamount(12),
      I1 => write_blockamount(15),
      I2 => write_blockamount(10),
      I3 => write_blockamount(13),
      O => \FSM_sequential_writeaddr_FSM_next_reg[1]_i_8_n_0\
    );
\FSM_sequential_writeaddr_FSM_next_reg[1]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => write_blockamount(18),
      I1 => write_blockamount(19),
      I2 => write_blockamount(20),
      I3 => write_blockamount(21),
      I4 => write_blockamount(31),
      I5 => write_blockamount(30),
      O => \FSM_sequential_writeaddr_FSM_next_reg[1]_i_9_n_0\
    );
\FSM_sequential_writeaddr_FSM_next_reg[2]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \writeaddr_FSM_next__0\(2),
      G => \FSM_sequential_writeaddr_FSM_next_reg[2]_i_2_n_0\,
      GE => '1',
      Q => writeaddr_FSM_next(2)
    );
\FSM_sequential_writeaddr_FSM_next_reg[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => writeaddr_FSM(1),
      I1 => writeaddr_FSM(0),
      I2 => writeaddr_FSM(2),
      I3 => write_start_reg_n_0,
      O => \writeaddr_FSM_next__0\(2)
    );
\FSM_sequential_writeaddr_FSM_next_reg[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000005FCA500A"
    )
        port map (
      I0 => write_start_reg_n_0,
      I1 => writeaddr_FSM_next15_in,
      I2 => writeaddr_FSM(0),
      I3 => writeaddr_FSM(1),
      I4 => \FSM_sequential_writeaddr_FSM_next_reg[2]_i_3_n_0\,
      I5 => writeaddr_FSM(2),
      O => \FSM_sequential_writeaddr_FSM_next_reg[2]_i_2_n_0\
    );
\FSM_sequential_writeaddr_FSM_next_reg[2]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => AXI_AWREADY,
      I1 => \^axi_awvalid_reg_0\,
      O => \FSM_sequential_writeaddr_FSM_next_reg[2]_i_3_n_0\
    );
\FSM_sequential_writeaddr_FSM_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => writeaddr_FSM_next(0),
      Q => writeaddr_FSM(0),
      R => '0'
    );
\FSM_sequential_writeaddr_FSM_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_sequential_writeaddr_FSM_next_reg_n_0_[1]\,
      Q => writeaddr_FSM(1),
      R => '0'
    );
\FSM_sequential_writeaddr_FSM_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => writeaddr_FSM_next(2),
      Q => writeaddr_FSM(2),
      R => '0'
    );
\FSM_sequential_writedata_FSM_next_reg[0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \writedata_FSM_next__0\(0),
      G => \FSM_sequential_writedata_FSM_next_reg[2]_i_2_n_0\,
      GE => '1',
      Q => writedata_FSM_next(0)
    );
\FSM_sequential_writedata_FSM_next_reg[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => writedata_FSM(0),
      I1 => writedata_FSM(2),
      O => \writedata_FSM_next__0\(0)
    );
\FSM_sequential_writedata_FSM_next_reg[1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \writedata_FSM_next__0\(1),
      G => \FSM_sequential_writedata_FSM_next_reg[2]_i_2_n_0\,
      GE => '1',
      Q => \FSM_sequential_writedata_FSM_next_reg_n_0_[1]\
    );
\FSM_sequential_writedata_FSM_next_reg[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"06"
    )
        port map (
      I0 => writedata_FSM(0),
      I1 => writedata_FSM(1),
      I2 => writedata_FSM(2),
      O => \writedata_FSM_next__0\(1)
    );
\FSM_sequential_writedata_FSM_next_reg[2]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \writedata_FSM_next__0\(2),
      G => \FSM_sequential_writedata_FSM_next_reg[2]_i_2_n_0\,
      GE => '1',
      Q => writedata_FSM_next(2)
    );
\FSM_sequential_writedata_FSM_next_reg[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4000"
    )
        port map (
      I0 => writedata_FSM(2),
      I1 => writedata_FSM(1),
      I2 => write_start_reg_n_0,
      I3 => writedata_FSM(0),
      O => \writedata_FSM_next__0\(2)
    );
\FSM_sequential_writedata_FSM_next_reg[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"050F0C0A050F000A"
    )
        port map (
      I0 => write_start_reg_n_0,
      I1 => \FSM_sequential_writedata_FSM_next_reg[2]_i_3_n_0\,
      I2 => writedata_FSM(2),
      I3 => writedata_FSM(0),
      I4 => writedata_FSM(1),
      I5 => writedata_FSM_next1,
      O => \FSM_sequential_writedata_FSM_next_reg[2]_i_2_n_0\
    );
\FSM_sequential_writedata_FSM_next_reg[2]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => FIFO_READ_tvalid,
      I1 => AXI_WREADY,
      O => \FSM_sequential_writedata_FSM_next_reg[2]_i_3_n_0\
    );
\FSM_sequential_writedata_FSM_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => writedata_FSM_next(0),
      Q => writedata_FSM(0),
      R => '0'
    );
\FSM_sequential_writedata_FSM_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_sequential_writedata_FSM_next_reg_n_0_[1]\,
      Q => writedata_FSM(1),
      R => '0'
    );
\FSM_sequential_writedata_FSM_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => writedata_FSM_next(2),
      Q => writedata_FSM(2),
      R => '0'
    );
busy_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE44"
    )
        port map (
      I0 => FSM_next(2),
      I1 => FSM_next(0),
      I2 => FSM_next(1),
      I3 => \^busy\,
      O => busy_i_1_n_0
    );
busy_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => busy_i_1_n_0,
      Q => \^busy\,
      R => '0'
    );
\read_blockamount_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_blockamount(0),
      Q => read_blockamount(0),
      R => '0'
    );
\read_blockamount_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_blockamount(10),
      Q => read_blockamount(10),
      R => '0'
    );
\read_blockamount_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_blockamount(11),
      Q => read_blockamount(11),
      R => '0'
    );
\read_blockamount_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_blockamount(12),
      Q => read_blockamount(12),
      R => '0'
    );
\read_blockamount_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_blockamount(13),
      Q => read_blockamount(13),
      R => '0'
    );
\read_blockamount_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_blockamount(14),
      Q => read_blockamount(14),
      R => '0'
    );
\read_blockamount_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_blockamount(15),
      Q => read_blockamount(15),
      R => '0'
    );
\read_blockamount_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_blockamount(16),
      Q => read_blockamount(16),
      R => '0'
    );
\read_blockamount_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_blockamount(17),
      Q => read_blockamount(17),
      R => '0'
    );
\read_blockamount_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_blockamount(18),
      Q => read_blockamount(18),
      R => '0'
    );
\read_blockamount_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_blockamount(19),
      Q => read_blockamount(19),
      R => '0'
    );
\read_blockamount_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_blockamount(1),
      Q => read_blockamount(1),
      R => '0'
    );
\read_blockamount_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_blockamount(20),
      Q => read_blockamount(20),
      R => '0'
    );
\read_blockamount_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_blockamount(21),
      Q => read_blockamount(21),
      R => '0'
    );
\read_blockamount_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_blockamount(22),
      Q => read_blockamount(22),
      R => '0'
    );
\read_blockamount_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_blockamount(23),
      Q => read_blockamount(23),
      R => '0'
    );
\read_blockamount_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_blockamount(24),
      Q => read_blockamount(24),
      R => '0'
    );
\read_blockamount_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_blockamount(25),
      Q => read_blockamount(25),
      R => '0'
    );
\read_blockamount_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_blockamount(26),
      Q => read_blockamount(26),
      R => '0'
    );
\read_blockamount_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_blockamount(27),
      Q => read_blockamount(27),
      R => '0'
    );
\read_blockamount_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_blockamount(28),
      Q => read_blockamount(28),
      R => '0'
    );
\read_blockamount_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_blockamount(29),
      Q => read_blockamount(29),
      R => '0'
    );
\read_blockamount_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_blockamount(2),
      Q => read_blockamount(2),
      R => '0'
    );
\read_blockamount_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_blockamount(30),
      Q => read_blockamount(30),
      R => '0'
    );
\read_blockamount_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_blockamount(31),
      Q => read_blockamount(31),
      R => '0'
    );
\read_blockamount_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_blockamount(3),
      Q => read_blockamount(3),
      R => '0'
    );
\read_blockamount_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_blockamount(4),
      Q => read_blockamount(4),
      R => '0'
    );
\read_blockamount_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_blockamount(5),
      Q => read_blockamount(5),
      R => '0'
    );
\read_blockamount_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_blockamount(6),
      Q => read_blockamount(6),
      R => '0'
    );
\read_blockamount_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_blockamount(7),
      Q => read_blockamount(7),
      R => '0'
    );
\read_blockamount_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_blockamount(8),
      Q => read_blockamount(8),
      R => '0'
    );
\read_blockamount_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_blockamount(9),
      Q => read_blockamount(9),
      R => '0'
    );
read_start_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F720"
    )
        port map (
      I0 => FSM_next(0),
      I1 => FSM_next(2),
      I2 => FSM_next(1),
      I3 => read_start_reg_n_0,
      O => read_start_i_1_n_0
    );
read_start_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => read_start_i_1_n_0,
      Q => read_start_reg_n_0,
      R => '0'
    );
\read_startaddress_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_startaddress(0),
      Q => read_startaddress(0),
      R => '0'
    );
\read_startaddress_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_startaddress(10),
      Q => read_startaddress(10),
      R => '0'
    );
\read_startaddress_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_startaddress(11),
      Q => read_startaddress(11),
      R => '0'
    );
\read_startaddress_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_startaddress(12),
      Q => read_startaddress(12),
      R => '0'
    );
\read_startaddress_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_startaddress(13),
      Q => read_startaddress(13),
      R => '0'
    );
\read_startaddress_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_startaddress(14),
      Q => read_startaddress(14),
      R => '0'
    );
\read_startaddress_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_startaddress(15),
      Q => read_startaddress(15),
      R => '0'
    );
\read_startaddress_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_startaddress(16),
      Q => read_startaddress(16),
      R => '0'
    );
\read_startaddress_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_startaddress(17),
      Q => read_startaddress(17),
      R => '0'
    );
\read_startaddress_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_startaddress(18),
      Q => read_startaddress(18),
      R => '0'
    );
\read_startaddress_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_startaddress(19),
      Q => read_startaddress(19),
      R => '0'
    );
\read_startaddress_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_startaddress(1),
      Q => read_startaddress(1),
      R => '0'
    );
\read_startaddress_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_startaddress(20),
      Q => read_startaddress(20),
      R => '0'
    );
\read_startaddress_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_startaddress(21),
      Q => read_startaddress(21),
      R => '0'
    );
\read_startaddress_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_startaddress(22),
      Q => read_startaddress(22),
      R => '0'
    );
\read_startaddress_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_startaddress(23),
      Q => read_startaddress(23),
      R => '0'
    );
\read_startaddress_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_startaddress(24),
      Q => read_startaddress(24),
      R => '0'
    );
\read_startaddress_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_startaddress(25),
      Q => read_startaddress(25),
      R => '0'
    );
\read_startaddress_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_startaddress(26),
      Q => read_startaddress(26),
      R => '0'
    );
\read_startaddress_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_startaddress(27),
      Q => read_startaddress(27),
      R => '0'
    );
\read_startaddress_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_startaddress(28),
      Q => read_startaddress(28),
      R => '0'
    );
\read_startaddress_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_startaddress(29),
      Q => read_startaddress(29),
      R => '0'
    );
\read_startaddress_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_startaddress(2),
      Q => read_startaddress(2),
      R => '0'
    );
\read_startaddress_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_startaddress(30),
      Q => read_startaddress(30),
      R => '0'
    );
\read_startaddress_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_startaddress(31),
      Q => read_startaddress(31),
      R => '0'
    );
\read_startaddress_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_startaddress(3),
      Q => read_startaddress(3),
      R => '0'
    );
\read_startaddress_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_startaddress(4),
      Q => read_startaddress(4),
      R => '0'
    );
\read_startaddress_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_startaddress(5),
      Q => read_startaddress(5),
      R => '0'
    );
\read_startaddress_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_startaddress(6),
      Q => read_startaddress(6),
      R => '0'
    );
\read_startaddress_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_startaddress(7),
      Q => read_startaddress(7),
      R => '0'
    );
\read_startaddress_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_startaddress(8),
      Q => read_startaddress(8),
      R => '0'
    );
\read_startaddress_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => read_startaddress_1,
      D => rd_startaddress(9),
      Q => read_startaddress(9),
      R => '0'
    );
readaddr_FSM_next1_carry: unisim.vcomponents.CARRY8
     port map (
      CI => readaddr_FSM_next1_carry_i_1_n_0,
      CI_TOP => '0',
      CO(7) => readaddr_FSM_next1_carry_n_0,
      CO(6) => readaddr_FSM_next1_carry_n_1,
      CO(5) => readaddr_FSM_next1_carry_n_2,
      CO(4) => readaddr_FSM_next1_carry_n_3,
      CO(3) => readaddr_FSM_next1_carry_n_4,
      CO(2) => readaddr_FSM_next1_carry_n_5,
      CO(1) => readaddr_FSM_next1_carry_n_6,
      CO(0) => readaddr_FSM_next1_carry_n_7,
      DI(7) => readaddr_FSM_next1_carry_i_2_n_0,
      DI(6) => readaddr_FSM_next1_carry_i_3_n_0,
      DI(5) => readaddr_FSM_next1_carry_i_4_n_0,
      DI(4) => readaddr_FSM_next1_carry_i_5_n_0,
      DI(3) => readaddr_FSM_next1_carry_i_6_n_0,
      DI(2) => readaddr_FSM_next1_carry_i_7_n_0,
      DI(1) => readaddr_FSM_next1_carry_i_8_n_0,
      DI(0) => '0',
      O(7 downto 0) => NLW_readaddr_FSM_next1_carry_O_UNCONNECTED(7 downto 0),
      S(7) => readaddr_FSM_next1_carry_i_9_n_0,
      S(6) => readaddr_FSM_next1_carry_i_10_n_0,
      S(5) => readaddr_FSM_next1_carry_i_11_n_0,
      S(4) => readaddr_FSM_next1_carry_i_12_n_0,
      S(3) => readaddr_FSM_next1_carry_i_13_n_0,
      S(2) => readaddr_FSM_next1_carry_i_14_n_0,
      S(1) => readaddr_FSM_next1_carry_i_15_n_0,
      S(0) => readaddr_FSM_next1_carry_i_16_n_0
    );
\readaddr_FSM_next1_carry__0\: unisim.vcomponents.CARRY8
     port map (
      CI => readaddr_FSM_next1_carry_n_0,
      CI_TOP => '0',
      CO(7) => \NLW_readaddr_FSM_next1_carry__0_CO_UNCONNECTED\(7),
      CO(6) => readaddr_FSM_next17_in,
      CO(5) => \readaddr_FSM_next1_carry__0_n_2\,
      CO(4) => \readaddr_FSM_next1_carry__0_n_3\,
      CO(3) => \readaddr_FSM_next1_carry__0_n_4\,
      CO(2) => \readaddr_FSM_next1_carry__0_n_5\,
      CO(1) => \readaddr_FSM_next1_carry__0_n_6\,
      CO(0) => \readaddr_FSM_next1_carry__0_n_7\,
      DI(7) => '0',
      DI(6) => \readaddr_FSM_next1_carry__0_i_1_n_0\,
      DI(5) => \readaddr_FSM_next1_carry__0_i_2_n_0\,
      DI(4) => \readaddr_FSM_next1_carry__0_i_3_n_0\,
      DI(3) => \readaddr_FSM_next1_carry__0_i_4_n_0\,
      DI(2) => \readaddr_FSM_next1_carry__0_i_5_n_0\,
      DI(1) => \readaddr_FSM_next1_carry__0_i_6_n_0\,
      DI(0) => \readaddr_FSM_next1_carry__0_i_7_n_0\,
      O(7 downto 0) => \NLW_readaddr_FSM_next1_carry__0_O_UNCONNECTED\(7 downto 0),
      S(7) => '0',
      S(6) => \readaddr_FSM_next1_carry__0_i_8_n_0\,
      S(5) => \readaddr_FSM_next1_carry__0_i_9_n_0\,
      S(4) => \readaddr_FSM_next1_carry__0_i_10_n_0\,
      S(3) => \readaddr_FSM_next1_carry__0_i_11_n_0\,
      S(2) => \readaddr_FSM_next1_carry__0_i_12_n_0\,
      S(1) => \readaddr_FSM_next1_carry__0_i_13_n_0\,
      S(0) => \readaddr_FSM_next1_carry__0_i_14_n_0\
    );
\readaddr_FSM_next1_carry__0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \readaddr_FSM_next1_carry__0_i_15_n_12\,
      I1 => read_blockamount(30),
      I2 => read_blockamount(31),
      I3 => \readaddr_FSM_next1_carry__0_i_15_n_11\,
      O => \readaddr_FSM_next1_carry__0_i_1_n_0\
    );
\readaddr_FSM_next1_carry__0_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => read_blockamount(27),
      I1 => \readaddr_FSM_next1_carry__0_i_15_n_15\,
      I2 => \readaddr_FSM_next1_carry__0_i_16_n_8\,
      I3 => read_blockamount(26),
      O => \readaddr_FSM_next1_carry__0_i_10_n_0\
    );
\readaddr_FSM_next1_carry__0_i_11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => read_blockamount(25),
      I1 => \readaddr_FSM_next1_carry__0_i_16_n_9\,
      I2 => \readaddr_FSM_next1_carry__0_i_16_n_10\,
      I3 => read_blockamount(24),
      O => \readaddr_FSM_next1_carry__0_i_11_n_0\
    );
\readaddr_FSM_next1_carry__0_i_12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => read_blockamount(23),
      I1 => \readaddr_FSM_next1_carry__0_i_16_n_11\,
      I2 => \readaddr_FSM_next1_carry__0_i_16_n_12\,
      I3 => read_blockamount(22),
      O => \readaddr_FSM_next1_carry__0_i_12_n_0\
    );
\readaddr_FSM_next1_carry__0_i_13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => read_blockamount(21),
      I1 => \readaddr_FSM_next1_carry__0_i_16_n_13\,
      I2 => \readaddr_FSM_next1_carry__0_i_16_n_14\,
      I3 => read_blockamount(20),
      O => \readaddr_FSM_next1_carry__0_i_13_n_0\
    );
\readaddr_FSM_next1_carry__0_i_14\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => read_blockamount(19),
      I1 => \readaddr_FSM_next1_carry__0_i_16_n_15\,
      I2 => readaddr_FSM_next1_carry_i_17_n_8,
      I3 => read_blockamount(18),
      O => \readaddr_FSM_next1_carry__0_i_14_n_0\
    );
\readaddr_FSM_next1_carry__0_i_15\: unisim.vcomponents.CARRY8
     port map (
      CI => \readaddr_FSM_next1_carry__0_i_16_n_0\,
      CI_TOP => '0',
      CO(7 downto 4) => \NLW_readaddr_FSM_next1_carry__0_i_15_CO_UNCONNECTED\(7 downto 4),
      CO(3) => \readaddr_FSM_next1_carry__0_i_15_n_4\,
      CO(2) => \readaddr_FSM_next1_carry__0_i_15_n_5\,
      CO(1) => \readaddr_FSM_next1_carry__0_i_15_n_6\,
      CO(0) => \readaddr_FSM_next1_carry__0_i_15_n_7\,
      DI(7 downto 0) => B"00000000",
      O(7 downto 5) => \NLW_readaddr_FSM_next1_carry__0_i_15_O_UNCONNECTED\(7 downto 5),
      O(4) => \readaddr_FSM_next1_carry__0_i_15_n_11\,
      O(3) => \readaddr_FSM_next1_carry__0_i_15_n_12\,
      O(2) => \readaddr_FSM_next1_carry__0_i_15_n_13\,
      O(1) => \readaddr_FSM_next1_carry__0_i_15_n_14\,
      O(0) => \readaddr_FSM_next1_carry__0_i_15_n_15\,
      S(7 downto 5) => B"000",
      S(4 downto 0) => readaddr_count_reg(31 downto 27)
    );
\readaddr_FSM_next1_carry__0_i_16\: unisim.vcomponents.CARRY8
     port map (
      CI => readaddr_FSM_next1_carry_i_17_n_0,
      CI_TOP => '0',
      CO(7) => \readaddr_FSM_next1_carry__0_i_16_n_0\,
      CO(6) => \readaddr_FSM_next1_carry__0_i_16_n_1\,
      CO(5) => \readaddr_FSM_next1_carry__0_i_16_n_2\,
      CO(4) => \readaddr_FSM_next1_carry__0_i_16_n_3\,
      CO(3) => \readaddr_FSM_next1_carry__0_i_16_n_4\,
      CO(2) => \readaddr_FSM_next1_carry__0_i_16_n_5\,
      CO(1) => \readaddr_FSM_next1_carry__0_i_16_n_6\,
      CO(0) => \readaddr_FSM_next1_carry__0_i_16_n_7\,
      DI(7 downto 0) => B"00000000",
      O(7) => \readaddr_FSM_next1_carry__0_i_16_n_8\,
      O(6) => \readaddr_FSM_next1_carry__0_i_16_n_9\,
      O(5) => \readaddr_FSM_next1_carry__0_i_16_n_10\,
      O(4) => \readaddr_FSM_next1_carry__0_i_16_n_11\,
      O(3) => \readaddr_FSM_next1_carry__0_i_16_n_12\,
      O(2) => \readaddr_FSM_next1_carry__0_i_16_n_13\,
      O(1) => \readaddr_FSM_next1_carry__0_i_16_n_14\,
      O(0) => \readaddr_FSM_next1_carry__0_i_16_n_15\,
      S(7 downto 0) => readaddr_count_reg(26 downto 19)
    );
\readaddr_FSM_next1_carry__0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \readaddr_FSM_next1_carry__0_i_15_n_14\,
      I1 => read_blockamount(28),
      I2 => read_blockamount(29),
      I3 => \readaddr_FSM_next1_carry__0_i_15_n_13\,
      O => \readaddr_FSM_next1_carry__0_i_2_n_0\
    );
\readaddr_FSM_next1_carry__0_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \readaddr_FSM_next1_carry__0_i_16_n_8\,
      I1 => read_blockamount(26),
      I2 => read_blockamount(27),
      I3 => \readaddr_FSM_next1_carry__0_i_15_n_15\,
      O => \readaddr_FSM_next1_carry__0_i_3_n_0\
    );
\readaddr_FSM_next1_carry__0_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \readaddr_FSM_next1_carry__0_i_16_n_10\,
      I1 => read_blockamount(24),
      I2 => read_blockamount(25),
      I3 => \readaddr_FSM_next1_carry__0_i_16_n_9\,
      O => \readaddr_FSM_next1_carry__0_i_4_n_0\
    );
\readaddr_FSM_next1_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \readaddr_FSM_next1_carry__0_i_16_n_12\,
      I1 => read_blockamount(22),
      I2 => read_blockamount(23),
      I3 => \readaddr_FSM_next1_carry__0_i_16_n_11\,
      O => \readaddr_FSM_next1_carry__0_i_5_n_0\
    );
\readaddr_FSM_next1_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \readaddr_FSM_next1_carry__0_i_16_n_14\,
      I1 => read_blockamount(20),
      I2 => read_blockamount(21),
      I3 => \readaddr_FSM_next1_carry__0_i_16_n_13\,
      O => \readaddr_FSM_next1_carry__0_i_6_n_0\
    );
\readaddr_FSM_next1_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => readaddr_FSM_next1_carry_i_17_n_8,
      I1 => read_blockamount(18),
      I2 => read_blockamount(19),
      I3 => \readaddr_FSM_next1_carry__0_i_16_n_15\,
      O => \readaddr_FSM_next1_carry__0_i_7_n_0\
    );
\readaddr_FSM_next1_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => read_blockamount(31),
      I1 => \readaddr_FSM_next1_carry__0_i_15_n_11\,
      I2 => \readaddr_FSM_next1_carry__0_i_15_n_12\,
      I3 => read_blockamount(30),
      O => \readaddr_FSM_next1_carry__0_i_8_n_0\
    );
\readaddr_FSM_next1_carry__0_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => read_blockamount(29),
      I1 => \readaddr_FSM_next1_carry__0_i_15_n_13\,
      I2 => \readaddr_FSM_next1_carry__0_i_15_n_14\,
      I3 => read_blockamount(28),
      O => \readaddr_FSM_next1_carry__0_i_9_n_0\
    );
readaddr_FSM_next1_carry_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => read_blockamount(0),
      I1 => read_blockamount(1),
      O => readaddr_FSM_next1_carry_i_1_n_0
    );
readaddr_FSM_next1_carry_i_10: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => read_blockamount(15),
      I1 => readaddr_FSM_next1_carry_i_17_n_11,
      I2 => readaddr_FSM_next1_carry_i_17_n_12,
      I3 => read_blockamount(14),
      O => readaddr_FSM_next1_carry_i_10_n_0
    );
readaddr_FSM_next1_carry_i_11: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => read_blockamount(13),
      I1 => readaddr_FSM_next1_carry_i_17_n_13,
      I2 => readaddr_FSM_next1_carry_i_17_n_14,
      I3 => read_blockamount(12),
      O => readaddr_FSM_next1_carry_i_11_n_0
    );
readaddr_FSM_next1_carry_i_12: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => read_blockamount(11),
      I1 => readaddr_FSM_next1_carry_i_17_n_15,
      I2 => readaddr_FSM_next1_carry_i_18_n_8,
      I3 => read_blockamount(10),
      O => readaddr_FSM_next1_carry_i_12_n_0
    );
readaddr_FSM_next1_carry_i_13: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => read_blockamount(9),
      I1 => readaddr_FSM_next1_carry_i_18_n_9,
      I2 => readaddr_FSM_next1_carry_i_18_n_10,
      I3 => read_blockamount(8),
      O => readaddr_FSM_next1_carry_i_13_n_0
    );
readaddr_FSM_next1_carry_i_14: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => read_blockamount(7),
      I1 => readaddr_FSM_next1_carry_i_18_n_11,
      I2 => readaddr_FSM_next1_carry_i_18_n_12,
      I3 => read_blockamount(6),
      O => readaddr_FSM_next1_carry_i_14_n_0
    );
readaddr_FSM_next1_carry_i_15: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => read_blockamount(5),
      I1 => readaddr_FSM_next1_carry_i_18_n_13,
      I2 => readaddr_FSM_next1_carry_i_18_n_14,
      I3 => read_blockamount(4),
      O => readaddr_FSM_next1_carry_i_15_n_0
    );
readaddr_FSM_next1_carry_i_16: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => read_blockamount(2),
      I1 => read_blockamount(3),
      O => readaddr_FSM_next1_carry_i_16_n_0
    );
readaddr_FSM_next1_carry_i_17: unisim.vcomponents.CARRY8
     port map (
      CI => readaddr_FSM_next1_carry_i_18_n_0,
      CI_TOP => '0',
      CO(7) => readaddr_FSM_next1_carry_i_17_n_0,
      CO(6) => readaddr_FSM_next1_carry_i_17_n_1,
      CO(5) => readaddr_FSM_next1_carry_i_17_n_2,
      CO(4) => readaddr_FSM_next1_carry_i_17_n_3,
      CO(3) => readaddr_FSM_next1_carry_i_17_n_4,
      CO(2) => readaddr_FSM_next1_carry_i_17_n_5,
      CO(1) => readaddr_FSM_next1_carry_i_17_n_6,
      CO(0) => readaddr_FSM_next1_carry_i_17_n_7,
      DI(7 downto 0) => B"00000000",
      O(7) => readaddr_FSM_next1_carry_i_17_n_8,
      O(6) => readaddr_FSM_next1_carry_i_17_n_9,
      O(5) => readaddr_FSM_next1_carry_i_17_n_10,
      O(4) => readaddr_FSM_next1_carry_i_17_n_11,
      O(3) => readaddr_FSM_next1_carry_i_17_n_12,
      O(2) => readaddr_FSM_next1_carry_i_17_n_13,
      O(1) => readaddr_FSM_next1_carry_i_17_n_14,
      O(0) => readaddr_FSM_next1_carry_i_17_n_15,
      S(7 downto 0) => readaddr_count_reg(18 downto 11)
    );
readaddr_FSM_next1_carry_i_18: unisim.vcomponents.CARRY8
     port map (
      CI => '0',
      CI_TOP => '0',
      CO(7) => readaddr_FSM_next1_carry_i_18_n_0,
      CO(6) => readaddr_FSM_next1_carry_i_18_n_1,
      CO(5) => readaddr_FSM_next1_carry_i_18_n_2,
      CO(4) => readaddr_FSM_next1_carry_i_18_n_3,
      CO(3) => readaddr_FSM_next1_carry_i_18_n_4,
      CO(2) => readaddr_FSM_next1_carry_i_18_n_5,
      CO(1) => readaddr_FSM_next1_carry_i_18_n_6,
      CO(0) => readaddr_FSM_next1_carry_i_18_n_7,
      DI(7 downto 2) => B"000000",
      DI(1) => readaddr_count_reg(4),
      DI(0) => '0',
      O(7) => readaddr_FSM_next1_carry_i_18_n_8,
      O(6) => readaddr_FSM_next1_carry_i_18_n_9,
      O(5) => readaddr_FSM_next1_carry_i_18_n_10,
      O(4) => readaddr_FSM_next1_carry_i_18_n_11,
      O(3) => readaddr_FSM_next1_carry_i_18_n_12,
      O(2) => readaddr_FSM_next1_carry_i_18_n_13,
      O(1) => readaddr_FSM_next1_carry_i_18_n_14,
      O(0) => NLW_readaddr_FSM_next1_carry_i_18_O_UNCONNECTED(0),
      S(7 downto 2) => readaddr_count_reg(10 downto 5),
      S(1) => readaddr_FSM_next1_carry_i_19_n_0,
      S(0) => '0'
    );
readaddr_FSM_next1_carry_i_19: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => readaddr_count_reg(4),
      O => readaddr_FSM_next1_carry_i_19_n_0
    );
readaddr_FSM_next1_carry_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => readaddr_FSM_next1_carry_i_17_n_10,
      I1 => read_blockamount(16),
      I2 => read_blockamount(17),
      I3 => readaddr_FSM_next1_carry_i_17_n_9,
      O => readaddr_FSM_next1_carry_i_2_n_0
    );
readaddr_FSM_next1_carry_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => readaddr_FSM_next1_carry_i_17_n_12,
      I1 => read_blockamount(14),
      I2 => read_blockamount(15),
      I3 => readaddr_FSM_next1_carry_i_17_n_11,
      O => readaddr_FSM_next1_carry_i_3_n_0
    );
readaddr_FSM_next1_carry_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => readaddr_FSM_next1_carry_i_17_n_14,
      I1 => read_blockamount(12),
      I2 => read_blockamount(13),
      I3 => readaddr_FSM_next1_carry_i_17_n_13,
      O => readaddr_FSM_next1_carry_i_4_n_0
    );
readaddr_FSM_next1_carry_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => readaddr_FSM_next1_carry_i_18_n_8,
      I1 => read_blockamount(10),
      I2 => read_blockamount(11),
      I3 => readaddr_FSM_next1_carry_i_17_n_15,
      O => readaddr_FSM_next1_carry_i_5_n_0
    );
readaddr_FSM_next1_carry_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => readaddr_FSM_next1_carry_i_18_n_10,
      I1 => read_blockamount(8),
      I2 => read_blockamount(9),
      I3 => readaddr_FSM_next1_carry_i_18_n_9,
      O => readaddr_FSM_next1_carry_i_6_n_0
    );
readaddr_FSM_next1_carry_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => readaddr_FSM_next1_carry_i_18_n_12,
      I1 => read_blockamount(6),
      I2 => read_blockamount(7),
      I3 => readaddr_FSM_next1_carry_i_18_n_11,
      O => readaddr_FSM_next1_carry_i_7_n_0
    );
readaddr_FSM_next1_carry_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => readaddr_FSM_next1_carry_i_18_n_14,
      I1 => read_blockamount(4),
      I2 => read_blockamount(5),
      I3 => readaddr_FSM_next1_carry_i_18_n_13,
      O => readaddr_FSM_next1_carry_i_8_n_0
    );
readaddr_FSM_next1_carry_i_9: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => read_blockamount(17),
      I1 => readaddr_FSM_next1_carry_i_17_n_9,
      I2 => readaddr_FSM_next1_carry_i_17_n_10,
      I3 => read_blockamount(16),
      O => readaddr_FSM_next1_carry_i_9_n_0
    );
\readaddr_count[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => readaddr_FSM_next(0),
      I1 => \FSM_sequential_readaddr_FSM_next_reg_n_0_[1]\,
      I2 => readaddr_FSM_next(2),
      I3 => AXI_ARREADY,
      I4 => \^axi_arvalid_reg_0\,
      O => readaddr_count
    );
\readaddr_count[4]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => readaddr_count_reg(4),
      O => \readaddr_count[4]_i_3_n_0\
    );
\readaddr_count_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readaddr_count,
      D => \readaddr_count_reg[4]_i_2_n_9\,
      Q => readaddr_count_reg(10),
      R => AXI_ARADDR0
    );
\readaddr_count_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readaddr_count,
      D => \readaddr_count_reg[4]_i_2_n_8\,
      Q => readaddr_count_reg(11),
      R => AXI_ARADDR0
    );
\readaddr_count_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readaddr_count,
      D => \readaddr_count_reg[12]_i_1_n_15\,
      Q => readaddr_count_reg(12),
      R => AXI_ARADDR0
    );
\readaddr_count_reg[12]_i_1\: unisim.vcomponents.CARRY8
     port map (
      CI => \readaddr_count_reg[4]_i_2_n_0\,
      CI_TOP => '0',
      CO(7) => \readaddr_count_reg[12]_i_1_n_0\,
      CO(6) => \readaddr_count_reg[12]_i_1_n_1\,
      CO(5) => \readaddr_count_reg[12]_i_1_n_2\,
      CO(4) => \readaddr_count_reg[12]_i_1_n_3\,
      CO(3) => \readaddr_count_reg[12]_i_1_n_4\,
      CO(2) => \readaddr_count_reg[12]_i_1_n_5\,
      CO(1) => \readaddr_count_reg[12]_i_1_n_6\,
      CO(0) => \readaddr_count_reg[12]_i_1_n_7\,
      DI(7 downto 0) => B"00000000",
      O(7) => \readaddr_count_reg[12]_i_1_n_8\,
      O(6) => \readaddr_count_reg[12]_i_1_n_9\,
      O(5) => \readaddr_count_reg[12]_i_1_n_10\,
      O(4) => \readaddr_count_reg[12]_i_1_n_11\,
      O(3) => \readaddr_count_reg[12]_i_1_n_12\,
      O(2) => \readaddr_count_reg[12]_i_1_n_13\,
      O(1) => \readaddr_count_reg[12]_i_1_n_14\,
      O(0) => \readaddr_count_reg[12]_i_1_n_15\,
      S(7 downto 0) => readaddr_count_reg(19 downto 12)
    );
\readaddr_count_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readaddr_count,
      D => \readaddr_count_reg[12]_i_1_n_14\,
      Q => readaddr_count_reg(13),
      R => AXI_ARADDR0
    );
\readaddr_count_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readaddr_count,
      D => \readaddr_count_reg[12]_i_1_n_13\,
      Q => readaddr_count_reg(14),
      R => AXI_ARADDR0
    );
\readaddr_count_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readaddr_count,
      D => \readaddr_count_reg[12]_i_1_n_12\,
      Q => readaddr_count_reg(15),
      R => AXI_ARADDR0
    );
\readaddr_count_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readaddr_count,
      D => \readaddr_count_reg[12]_i_1_n_11\,
      Q => readaddr_count_reg(16),
      R => AXI_ARADDR0
    );
\readaddr_count_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readaddr_count,
      D => \readaddr_count_reg[12]_i_1_n_10\,
      Q => readaddr_count_reg(17),
      R => AXI_ARADDR0
    );
\readaddr_count_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readaddr_count,
      D => \readaddr_count_reg[12]_i_1_n_9\,
      Q => readaddr_count_reg(18),
      R => AXI_ARADDR0
    );
\readaddr_count_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readaddr_count,
      D => \readaddr_count_reg[12]_i_1_n_8\,
      Q => readaddr_count_reg(19),
      R => AXI_ARADDR0
    );
\readaddr_count_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readaddr_count,
      D => \readaddr_count_reg[20]_i_1_n_15\,
      Q => readaddr_count_reg(20),
      R => AXI_ARADDR0
    );
\readaddr_count_reg[20]_i_1\: unisim.vcomponents.CARRY8
     port map (
      CI => \readaddr_count_reg[12]_i_1_n_0\,
      CI_TOP => '0',
      CO(7) => \readaddr_count_reg[20]_i_1_n_0\,
      CO(6) => \readaddr_count_reg[20]_i_1_n_1\,
      CO(5) => \readaddr_count_reg[20]_i_1_n_2\,
      CO(4) => \readaddr_count_reg[20]_i_1_n_3\,
      CO(3) => \readaddr_count_reg[20]_i_1_n_4\,
      CO(2) => \readaddr_count_reg[20]_i_1_n_5\,
      CO(1) => \readaddr_count_reg[20]_i_1_n_6\,
      CO(0) => \readaddr_count_reg[20]_i_1_n_7\,
      DI(7 downto 0) => B"00000000",
      O(7) => \readaddr_count_reg[20]_i_1_n_8\,
      O(6) => \readaddr_count_reg[20]_i_1_n_9\,
      O(5) => \readaddr_count_reg[20]_i_1_n_10\,
      O(4) => \readaddr_count_reg[20]_i_1_n_11\,
      O(3) => \readaddr_count_reg[20]_i_1_n_12\,
      O(2) => \readaddr_count_reg[20]_i_1_n_13\,
      O(1) => \readaddr_count_reg[20]_i_1_n_14\,
      O(0) => \readaddr_count_reg[20]_i_1_n_15\,
      S(7 downto 0) => readaddr_count_reg(27 downto 20)
    );
\readaddr_count_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readaddr_count,
      D => \readaddr_count_reg[20]_i_1_n_14\,
      Q => readaddr_count_reg(21),
      R => AXI_ARADDR0
    );
\readaddr_count_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readaddr_count,
      D => \readaddr_count_reg[20]_i_1_n_13\,
      Q => readaddr_count_reg(22),
      R => AXI_ARADDR0
    );
\readaddr_count_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readaddr_count,
      D => \readaddr_count_reg[20]_i_1_n_12\,
      Q => readaddr_count_reg(23),
      R => AXI_ARADDR0
    );
\readaddr_count_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readaddr_count,
      D => \readaddr_count_reg[20]_i_1_n_11\,
      Q => readaddr_count_reg(24),
      R => AXI_ARADDR0
    );
\readaddr_count_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readaddr_count,
      D => \readaddr_count_reg[20]_i_1_n_10\,
      Q => readaddr_count_reg(25),
      R => AXI_ARADDR0
    );
\readaddr_count_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readaddr_count,
      D => \readaddr_count_reg[20]_i_1_n_9\,
      Q => readaddr_count_reg(26),
      R => AXI_ARADDR0
    );
\readaddr_count_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readaddr_count,
      D => \readaddr_count_reg[20]_i_1_n_8\,
      Q => readaddr_count_reg(27),
      R => AXI_ARADDR0
    );
\readaddr_count_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readaddr_count,
      D => \readaddr_count_reg[28]_i_1_n_15\,
      Q => readaddr_count_reg(28),
      R => AXI_ARADDR0
    );
\readaddr_count_reg[28]_i_1\: unisim.vcomponents.CARRY8
     port map (
      CI => \readaddr_count_reg[20]_i_1_n_0\,
      CI_TOP => '0',
      CO(7 downto 3) => \NLW_readaddr_count_reg[28]_i_1_CO_UNCONNECTED\(7 downto 3),
      CO(2) => \readaddr_count_reg[28]_i_1_n_5\,
      CO(1) => \readaddr_count_reg[28]_i_1_n_6\,
      CO(0) => \readaddr_count_reg[28]_i_1_n_7\,
      DI(7 downto 0) => B"00000000",
      O(7 downto 4) => \NLW_readaddr_count_reg[28]_i_1_O_UNCONNECTED\(7 downto 4),
      O(3) => \readaddr_count_reg[28]_i_1_n_12\,
      O(2) => \readaddr_count_reg[28]_i_1_n_13\,
      O(1) => \readaddr_count_reg[28]_i_1_n_14\,
      O(0) => \readaddr_count_reg[28]_i_1_n_15\,
      S(7 downto 4) => B"0000",
      S(3 downto 0) => readaddr_count_reg(31 downto 28)
    );
\readaddr_count_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readaddr_count,
      D => \readaddr_count_reg[28]_i_1_n_14\,
      Q => readaddr_count_reg(29),
      R => AXI_ARADDR0
    );
\readaddr_count_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readaddr_count,
      D => \readaddr_count_reg[28]_i_1_n_13\,
      Q => readaddr_count_reg(30),
      R => AXI_ARADDR0
    );
\readaddr_count_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readaddr_count,
      D => \readaddr_count_reg[28]_i_1_n_12\,
      Q => readaddr_count_reg(31),
      R => AXI_ARADDR0
    );
\readaddr_count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readaddr_count,
      D => \readaddr_count_reg[4]_i_2_n_15\,
      Q => readaddr_count_reg(4),
      R => AXI_ARADDR0
    );
\readaddr_count_reg[4]_i_2\: unisim.vcomponents.CARRY8
     port map (
      CI => '0',
      CI_TOP => '0',
      CO(7) => \readaddr_count_reg[4]_i_2_n_0\,
      CO(6) => \readaddr_count_reg[4]_i_2_n_1\,
      CO(5) => \readaddr_count_reg[4]_i_2_n_2\,
      CO(4) => \readaddr_count_reg[4]_i_2_n_3\,
      CO(3) => \readaddr_count_reg[4]_i_2_n_4\,
      CO(2) => \readaddr_count_reg[4]_i_2_n_5\,
      CO(1) => \readaddr_count_reg[4]_i_2_n_6\,
      CO(0) => \readaddr_count_reg[4]_i_2_n_7\,
      DI(7 downto 0) => B"00000001",
      O(7) => \readaddr_count_reg[4]_i_2_n_8\,
      O(6) => \readaddr_count_reg[4]_i_2_n_9\,
      O(5) => \readaddr_count_reg[4]_i_2_n_10\,
      O(4) => \readaddr_count_reg[4]_i_2_n_11\,
      O(3) => \readaddr_count_reg[4]_i_2_n_12\,
      O(2) => \readaddr_count_reg[4]_i_2_n_13\,
      O(1) => \readaddr_count_reg[4]_i_2_n_14\,
      O(0) => \readaddr_count_reg[4]_i_2_n_15\,
      S(7 downto 1) => readaddr_count_reg(11 downto 5),
      S(0) => \readaddr_count[4]_i_3_n_0\
    );
\readaddr_count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readaddr_count,
      D => \readaddr_count_reg[4]_i_2_n_14\,
      Q => readaddr_count_reg(5),
      R => AXI_ARADDR0
    );
\readaddr_count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readaddr_count,
      D => \readaddr_count_reg[4]_i_2_n_13\,
      Q => readaddr_count_reg(6),
      R => AXI_ARADDR0
    );
\readaddr_count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readaddr_count,
      D => \readaddr_count_reg[4]_i_2_n_12\,
      Q => readaddr_count_reg(7),
      R => AXI_ARADDR0
    );
\readaddr_count_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readaddr_count,
      D => \readaddr_count_reg[4]_i_2_n_11\,
      Q => readaddr_count_reg(8),
      R => AXI_ARADDR0
    );
\readaddr_count_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readaddr_count,
      D => \readaddr_count_reg[4]_i_2_n_10\,
      Q => readaddr_count_reg(9),
      R => AXI_ARADDR0
    );
readaddr_done_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE40"
    )
        port map (
      I0 => readaddr_FSM_next(2),
      I1 => \FSM_sequential_readaddr_FSM_next_reg_n_0_[1]\,
      I2 => readaddr_FSM_next(0),
      I3 => readaddr_done_reg_n_0,
      O => readaddr_done_i_1_n_0
    );
readaddr_done_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => readaddr_done_i_1_n_0,
      Q => readaddr_done_reg_n_0,
      R => '0'
    );
\readdata_count[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \FSM_sequential_readdata_FSM_next_reg_n_0_[1]\,
      I1 => readdata_FSM_next(0),
      I2 => readdata_FSM_next(2),
      O => readdata_count0
    );
\readdata_count[0]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"40400040"
    )
        port map (
      I0 => readdata_FSM_next(2),
      I1 => AXI_RVALID,
      I2 => FIFO_WRITE_tready,
      I3 => \FSM_sequential_readdata_FSM_next_reg_n_0_[1]\,
      I4 => readdata_FSM_next(0),
      O => readdata_count
    );
\readdata_count[0]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => readdata_count_reg(0),
      O => \readdata_count[0]_i_4_n_0\
    );
\readdata_count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readdata_count,
      D => \readdata_count_reg[0]_i_3_n_15\,
      Q => readdata_count_reg(0),
      R => readdata_count0
    );
\readdata_count_reg[0]_i_3\: unisim.vcomponents.CARRY8
     port map (
      CI => '0',
      CI_TOP => '0',
      CO(7) => \readdata_count_reg[0]_i_3_n_0\,
      CO(6) => \readdata_count_reg[0]_i_3_n_1\,
      CO(5) => \readdata_count_reg[0]_i_3_n_2\,
      CO(4) => \readdata_count_reg[0]_i_3_n_3\,
      CO(3) => \readdata_count_reg[0]_i_3_n_4\,
      CO(2) => \readdata_count_reg[0]_i_3_n_5\,
      CO(1) => \readdata_count_reg[0]_i_3_n_6\,
      CO(0) => \readdata_count_reg[0]_i_3_n_7\,
      DI(7 downto 0) => B"00000001",
      O(7) => \readdata_count_reg[0]_i_3_n_8\,
      O(6) => \readdata_count_reg[0]_i_3_n_9\,
      O(5) => \readdata_count_reg[0]_i_3_n_10\,
      O(4) => \readdata_count_reg[0]_i_3_n_11\,
      O(3) => \readdata_count_reg[0]_i_3_n_12\,
      O(2) => \readdata_count_reg[0]_i_3_n_13\,
      O(1) => \readdata_count_reg[0]_i_3_n_14\,
      O(0) => \readdata_count_reg[0]_i_3_n_15\,
      S(7 downto 1) => readdata_count_reg(7 downto 1),
      S(0) => \readdata_count[0]_i_4_n_0\
    );
\readdata_count_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readdata_count,
      D => \readdata_count_reg[8]_i_1_n_13\,
      Q => readdata_count_reg(10),
      R => readdata_count0
    );
\readdata_count_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readdata_count,
      D => \readdata_count_reg[8]_i_1_n_12\,
      Q => readdata_count_reg(11),
      R => readdata_count0
    );
\readdata_count_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readdata_count,
      D => \readdata_count_reg[8]_i_1_n_11\,
      Q => readdata_count_reg(12),
      R => readdata_count0
    );
\readdata_count_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readdata_count,
      D => \readdata_count_reg[8]_i_1_n_10\,
      Q => readdata_count_reg(13),
      R => readdata_count0
    );
\readdata_count_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readdata_count,
      D => \readdata_count_reg[8]_i_1_n_9\,
      Q => readdata_count_reg(14),
      R => readdata_count0
    );
\readdata_count_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readdata_count,
      D => \readdata_count_reg[8]_i_1_n_8\,
      Q => readdata_count_reg(15),
      R => readdata_count0
    );
\readdata_count_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readdata_count,
      D => \readdata_count_reg[16]_i_1_n_15\,
      Q => readdata_count_reg(16),
      R => readdata_count0
    );
\readdata_count_reg[16]_i_1\: unisim.vcomponents.CARRY8
     port map (
      CI => \readdata_count_reg[8]_i_1_n_0\,
      CI_TOP => '0',
      CO(7) => \readdata_count_reg[16]_i_1_n_0\,
      CO(6) => \readdata_count_reg[16]_i_1_n_1\,
      CO(5) => \readdata_count_reg[16]_i_1_n_2\,
      CO(4) => \readdata_count_reg[16]_i_1_n_3\,
      CO(3) => \readdata_count_reg[16]_i_1_n_4\,
      CO(2) => \readdata_count_reg[16]_i_1_n_5\,
      CO(1) => \readdata_count_reg[16]_i_1_n_6\,
      CO(0) => \readdata_count_reg[16]_i_1_n_7\,
      DI(7 downto 0) => B"00000000",
      O(7) => \readdata_count_reg[16]_i_1_n_8\,
      O(6) => \readdata_count_reg[16]_i_1_n_9\,
      O(5) => \readdata_count_reg[16]_i_1_n_10\,
      O(4) => \readdata_count_reg[16]_i_1_n_11\,
      O(3) => \readdata_count_reg[16]_i_1_n_12\,
      O(2) => \readdata_count_reg[16]_i_1_n_13\,
      O(1) => \readdata_count_reg[16]_i_1_n_14\,
      O(0) => \readdata_count_reg[16]_i_1_n_15\,
      S(7 downto 0) => readdata_count_reg(23 downto 16)
    );
\readdata_count_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readdata_count,
      D => \readdata_count_reg[16]_i_1_n_14\,
      Q => readdata_count_reg(17),
      R => readdata_count0
    );
\readdata_count_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readdata_count,
      D => \readdata_count_reg[16]_i_1_n_13\,
      Q => readdata_count_reg(18),
      R => readdata_count0
    );
\readdata_count_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readdata_count,
      D => \readdata_count_reg[16]_i_1_n_12\,
      Q => readdata_count_reg(19),
      R => readdata_count0
    );
\readdata_count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readdata_count,
      D => \readdata_count_reg[0]_i_3_n_14\,
      Q => readdata_count_reg(1),
      R => readdata_count0
    );
\readdata_count_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readdata_count,
      D => \readdata_count_reg[16]_i_1_n_11\,
      Q => readdata_count_reg(20),
      R => readdata_count0
    );
\readdata_count_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readdata_count,
      D => \readdata_count_reg[16]_i_1_n_10\,
      Q => readdata_count_reg(21),
      R => readdata_count0
    );
\readdata_count_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readdata_count,
      D => \readdata_count_reg[16]_i_1_n_9\,
      Q => readdata_count_reg(22),
      R => readdata_count0
    );
\readdata_count_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readdata_count,
      D => \readdata_count_reg[16]_i_1_n_8\,
      Q => readdata_count_reg(23),
      R => readdata_count0
    );
\readdata_count_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readdata_count,
      D => \readdata_count_reg[24]_i_1_n_15\,
      Q => readdata_count_reg(24),
      R => readdata_count0
    );
\readdata_count_reg[24]_i_1\: unisim.vcomponents.CARRY8
     port map (
      CI => \readdata_count_reg[16]_i_1_n_0\,
      CI_TOP => '0',
      CO(7) => \NLW_readdata_count_reg[24]_i_1_CO_UNCONNECTED\(7),
      CO(6) => \readdata_count_reg[24]_i_1_n_1\,
      CO(5) => \readdata_count_reg[24]_i_1_n_2\,
      CO(4) => \readdata_count_reg[24]_i_1_n_3\,
      CO(3) => \readdata_count_reg[24]_i_1_n_4\,
      CO(2) => \readdata_count_reg[24]_i_1_n_5\,
      CO(1) => \readdata_count_reg[24]_i_1_n_6\,
      CO(0) => \readdata_count_reg[24]_i_1_n_7\,
      DI(7 downto 0) => B"00000000",
      O(7) => \readdata_count_reg[24]_i_1_n_8\,
      O(6) => \readdata_count_reg[24]_i_1_n_9\,
      O(5) => \readdata_count_reg[24]_i_1_n_10\,
      O(4) => \readdata_count_reg[24]_i_1_n_11\,
      O(3) => \readdata_count_reg[24]_i_1_n_12\,
      O(2) => \readdata_count_reg[24]_i_1_n_13\,
      O(1) => \readdata_count_reg[24]_i_1_n_14\,
      O(0) => \readdata_count_reg[24]_i_1_n_15\,
      S(7 downto 0) => readdata_count_reg(31 downto 24)
    );
\readdata_count_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readdata_count,
      D => \readdata_count_reg[24]_i_1_n_14\,
      Q => readdata_count_reg(25),
      R => readdata_count0
    );
\readdata_count_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readdata_count,
      D => \readdata_count_reg[24]_i_1_n_13\,
      Q => readdata_count_reg(26),
      R => readdata_count0
    );
\readdata_count_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readdata_count,
      D => \readdata_count_reg[24]_i_1_n_12\,
      Q => readdata_count_reg(27),
      R => readdata_count0
    );
\readdata_count_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readdata_count,
      D => \readdata_count_reg[24]_i_1_n_11\,
      Q => readdata_count_reg(28),
      R => readdata_count0
    );
\readdata_count_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readdata_count,
      D => \readdata_count_reg[24]_i_1_n_10\,
      Q => readdata_count_reg(29),
      R => readdata_count0
    );
\readdata_count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readdata_count,
      D => \readdata_count_reg[0]_i_3_n_13\,
      Q => readdata_count_reg(2),
      R => readdata_count0
    );
\readdata_count_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readdata_count,
      D => \readdata_count_reg[24]_i_1_n_9\,
      Q => readdata_count_reg(30),
      R => readdata_count0
    );
\readdata_count_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readdata_count,
      D => \readdata_count_reg[24]_i_1_n_8\,
      Q => readdata_count_reg(31),
      R => readdata_count0
    );
\readdata_count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readdata_count,
      D => \readdata_count_reg[0]_i_3_n_12\,
      Q => readdata_count_reg(3),
      R => readdata_count0
    );
\readdata_count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readdata_count,
      D => \readdata_count_reg[0]_i_3_n_11\,
      Q => readdata_count_reg(4),
      R => readdata_count0
    );
\readdata_count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readdata_count,
      D => \readdata_count_reg[0]_i_3_n_10\,
      Q => readdata_count_reg(5),
      R => readdata_count0
    );
\readdata_count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readdata_count,
      D => \readdata_count_reg[0]_i_3_n_9\,
      Q => readdata_count_reg(6),
      R => readdata_count0
    );
\readdata_count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readdata_count,
      D => \readdata_count_reg[0]_i_3_n_8\,
      Q => readdata_count_reg(7),
      R => readdata_count0
    );
\readdata_count_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readdata_count,
      D => \readdata_count_reg[8]_i_1_n_15\,
      Q => readdata_count_reg(8),
      R => readdata_count0
    );
\readdata_count_reg[8]_i_1\: unisim.vcomponents.CARRY8
     port map (
      CI => \readdata_count_reg[0]_i_3_n_0\,
      CI_TOP => '0',
      CO(7) => \readdata_count_reg[8]_i_1_n_0\,
      CO(6) => \readdata_count_reg[8]_i_1_n_1\,
      CO(5) => \readdata_count_reg[8]_i_1_n_2\,
      CO(4) => \readdata_count_reg[8]_i_1_n_3\,
      CO(3) => \readdata_count_reg[8]_i_1_n_4\,
      CO(2) => \readdata_count_reg[8]_i_1_n_5\,
      CO(1) => \readdata_count_reg[8]_i_1_n_6\,
      CO(0) => \readdata_count_reg[8]_i_1_n_7\,
      DI(7 downto 0) => B"00000000",
      O(7) => \readdata_count_reg[8]_i_1_n_8\,
      O(6) => \readdata_count_reg[8]_i_1_n_9\,
      O(5) => \readdata_count_reg[8]_i_1_n_10\,
      O(4) => \readdata_count_reg[8]_i_1_n_11\,
      O(3) => \readdata_count_reg[8]_i_1_n_12\,
      O(2) => \readdata_count_reg[8]_i_1_n_13\,
      O(1) => \readdata_count_reg[8]_i_1_n_14\,
      O(0) => \readdata_count_reg[8]_i_1_n_15\,
      S(7 downto 0) => readdata_count_reg(15 downto 8)
    );
\readdata_count_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => readdata_count,
      D => \readdata_count_reg[8]_i_1_n_14\,
      Q => readdata_count_reg(9),
      R => readdata_count0
    );
readdata_done_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE02"
    )
        port map (
      I0 => \FSM_sequential_readdata_FSM_next_reg_n_0_[1]\,
      I1 => readdata_FSM_next(0),
      I2 => readdata_FSM_next(2),
      I3 => readdata_done_reg_n_0,
      O => readdata_done_i_1_n_0
    );
readdata_done_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => readdata_done_i_1_n_0,
      Q => readdata_done_reg_n_0,
      R => '0'
    );
\write_blockamount_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_blockamount(0),
      Q => write_blockamount(0),
      R => '0'
    );
\write_blockamount_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_blockamount(10),
      Q => write_blockamount(10),
      R => '0'
    );
\write_blockamount_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_blockamount(11),
      Q => write_blockamount(11),
      R => '0'
    );
\write_blockamount_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_blockamount(12),
      Q => write_blockamount(12),
      R => '0'
    );
\write_blockamount_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_blockamount(13),
      Q => write_blockamount(13),
      R => '0'
    );
\write_blockamount_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_blockamount(14),
      Q => write_blockamount(14),
      R => '0'
    );
\write_blockamount_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_blockamount(15),
      Q => write_blockamount(15),
      R => '0'
    );
\write_blockamount_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_blockamount(16),
      Q => write_blockamount(16),
      R => '0'
    );
\write_blockamount_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_blockamount(17),
      Q => write_blockamount(17),
      R => '0'
    );
\write_blockamount_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_blockamount(18),
      Q => write_blockamount(18),
      R => '0'
    );
\write_blockamount_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_blockamount(19),
      Q => write_blockamount(19),
      R => '0'
    );
\write_blockamount_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_blockamount(1),
      Q => write_blockamount(1),
      R => '0'
    );
\write_blockamount_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_blockamount(20),
      Q => write_blockamount(20),
      R => '0'
    );
\write_blockamount_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_blockamount(21),
      Q => write_blockamount(21),
      R => '0'
    );
\write_blockamount_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_blockamount(22),
      Q => write_blockamount(22),
      R => '0'
    );
\write_blockamount_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_blockamount(23),
      Q => write_blockamount(23),
      R => '0'
    );
\write_blockamount_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_blockamount(24),
      Q => write_blockamount(24),
      R => '0'
    );
\write_blockamount_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_blockamount(25),
      Q => write_blockamount(25),
      R => '0'
    );
\write_blockamount_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_blockamount(26),
      Q => write_blockamount(26),
      R => '0'
    );
\write_blockamount_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_blockamount(27),
      Q => write_blockamount(27),
      R => '0'
    );
\write_blockamount_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_blockamount(28),
      Q => write_blockamount(28),
      R => '0'
    );
\write_blockamount_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_blockamount(29),
      Q => write_blockamount(29),
      R => '0'
    );
\write_blockamount_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_blockamount(2),
      Q => write_blockamount(2),
      R => '0'
    );
\write_blockamount_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_blockamount(30),
      Q => write_blockamount(30),
      R => '0'
    );
\write_blockamount_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_blockamount(31),
      Q => write_blockamount(31),
      R => '0'
    );
\write_blockamount_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_blockamount(3),
      Q => write_blockamount(3),
      R => '0'
    );
\write_blockamount_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_blockamount(4),
      Q => write_blockamount(4),
      R => '0'
    );
\write_blockamount_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_blockamount(5),
      Q => write_blockamount(5),
      R => '0'
    );
\write_blockamount_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_blockamount(6),
      Q => write_blockamount(6),
      R => '0'
    );
\write_blockamount_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_blockamount(7),
      Q => write_blockamount(7),
      R => '0'
    );
\write_blockamount_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_blockamount(8),
      Q => write_blockamount(8),
      R => '0'
    );
\write_blockamount_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_blockamount(9),
      Q => write_blockamount(9),
      R => '0'
    );
write_start_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F704"
    )
        port map (
      I0 => FSM_next(2),
      I1 => FSM_next(0),
      I2 => FSM_next(1),
      I3 => write_start_reg_n_0,
      O => write_start_i_1_n_0
    );
write_start_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => write_start_i_1_n_0,
      Q => write_start_reg_n_0,
      R => '0'
    );
\write_startaddress_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_startaddress(0),
      Q => write_startaddress(0),
      R => '0'
    );
\write_startaddress_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_startaddress(10),
      Q => write_startaddress(10),
      R => '0'
    );
\write_startaddress_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_startaddress(11),
      Q => write_startaddress(11),
      R => '0'
    );
\write_startaddress_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_startaddress(12),
      Q => write_startaddress(12),
      R => '0'
    );
\write_startaddress_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_startaddress(13),
      Q => write_startaddress(13),
      R => '0'
    );
\write_startaddress_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_startaddress(14),
      Q => write_startaddress(14),
      R => '0'
    );
\write_startaddress_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_startaddress(15),
      Q => write_startaddress(15),
      R => '0'
    );
\write_startaddress_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_startaddress(16),
      Q => write_startaddress(16),
      R => '0'
    );
\write_startaddress_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_startaddress(17),
      Q => write_startaddress(17),
      R => '0'
    );
\write_startaddress_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_startaddress(18),
      Q => write_startaddress(18),
      R => '0'
    );
\write_startaddress_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_startaddress(19),
      Q => write_startaddress(19),
      R => '0'
    );
\write_startaddress_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_startaddress(1),
      Q => write_startaddress(1),
      R => '0'
    );
\write_startaddress_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_startaddress(20),
      Q => write_startaddress(20),
      R => '0'
    );
\write_startaddress_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_startaddress(21),
      Q => write_startaddress(21),
      R => '0'
    );
\write_startaddress_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_startaddress(22),
      Q => write_startaddress(22),
      R => '0'
    );
\write_startaddress_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_startaddress(23),
      Q => write_startaddress(23),
      R => '0'
    );
\write_startaddress_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_startaddress(24),
      Q => write_startaddress(24),
      R => '0'
    );
\write_startaddress_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_startaddress(25),
      Q => write_startaddress(25),
      R => '0'
    );
\write_startaddress_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_startaddress(26),
      Q => write_startaddress(26),
      R => '0'
    );
\write_startaddress_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_startaddress(27),
      Q => write_startaddress(27),
      R => '0'
    );
\write_startaddress_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_startaddress(28),
      Q => write_startaddress(28),
      R => '0'
    );
\write_startaddress_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_startaddress(29),
      Q => write_startaddress(29),
      R => '0'
    );
\write_startaddress_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_startaddress(2),
      Q => write_startaddress(2),
      R => '0'
    );
\write_startaddress_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_startaddress(30),
      Q => write_startaddress(30),
      R => '0'
    );
\write_startaddress_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_startaddress(31),
      Q => write_startaddress(31),
      R => '0'
    );
\write_startaddress_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_startaddress(3),
      Q => write_startaddress(3),
      R => '0'
    );
\write_startaddress_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_startaddress(4),
      Q => write_startaddress(4),
      R => '0'
    );
\write_startaddress_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_startaddress(5),
      Q => write_startaddress(5),
      R => '0'
    );
\write_startaddress_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_startaddress(6),
      Q => write_startaddress(6),
      R => '0'
    );
\write_startaddress_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_startaddress(7),
      Q => write_startaddress(7),
      R => '0'
    );
\write_startaddress_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_startaddress(8),
      Q => write_startaddress(8),
      R => '0'
    );
\write_startaddress_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => write_startaddress_0,
      D => wr_startaddress(9),
      Q => write_startaddress(9),
      R => '0'
    );
writeaddr_FSM_next1_carry: unisim.vcomponents.CARRY8
     port map (
      CI => writeaddr_FSM_next1_carry_i_1_n_0,
      CI_TOP => '0',
      CO(7) => writeaddr_FSM_next1_carry_n_0,
      CO(6) => writeaddr_FSM_next1_carry_n_1,
      CO(5) => writeaddr_FSM_next1_carry_n_2,
      CO(4) => writeaddr_FSM_next1_carry_n_3,
      CO(3) => writeaddr_FSM_next1_carry_n_4,
      CO(2) => writeaddr_FSM_next1_carry_n_5,
      CO(1) => writeaddr_FSM_next1_carry_n_6,
      CO(0) => writeaddr_FSM_next1_carry_n_7,
      DI(7) => writeaddr_FSM_next1_carry_i_2_n_0,
      DI(6) => writeaddr_FSM_next1_carry_i_3_n_0,
      DI(5) => writeaddr_FSM_next1_carry_i_4_n_0,
      DI(4) => writeaddr_FSM_next1_carry_i_5_n_0,
      DI(3) => writeaddr_FSM_next1_carry_i_6_n_0,
      DI(2) => writeaddr_FSM_next1_carry_i_7_n_0,
      DI(1) => writeaddr_FSM_next1_carry_i_8_n_0,
      DI(0) => '0',
      O(7 downto 0) => NLW_writeaddr_FSM_next1_carry_O_UNCONNECTED(7 downto 0),
      S(7) => writeaddr_FSM_next1_carry_i_9_n_0,
      S(6) => writeaddr_FSM_next1_carry_i_10_n_0,
      S(5) => writeaddr_FSM_next1_carry_i_11_n_0,
      S(4) => writeaddr_FSM_next1_carry_i_12_n_0,
      S(3) => writeaddr_FSM_next1_carry_i_13_n_0,
      S(2) => writeaddr_FSM_next1_carry_i_14_n_0,
      S(1) => writeaddr_FSM_next1_carry_i_15_n_0,
      S(0) => writeaddr_FSM_next1_carry_i_16_n_0
    );
\writeaddr_FSM_next1_carry__0\: unisim.vcomponents.CARRY8
     port map (
      CI => writeaddr_FSM_next1_carry_n_0,
      CI_TOP => '0',
      CO(7) => \NLW_writeaddr_FSM_next1_carry__0_CO_UNCONNECTED\(7),
      CO(6) => writeaddr_FSM_next15_in,
      CO(5) => \writeaddr_FSM_next1_carry__0_n_2\,
      CO(4) => \writeaddr_FSM_next1_carry__0_n_3\,
      CO(3) => \writeaddr_FSM_next1_carry__0_n_4\,
      CO(2) => \writeaddr_FSM_next1_carry__0_n_5\,
      CO(1) => \writeaddr_FSM_next1_carry__0_n_6\,
      CO(0) => \writeaddr_FSM_next1_carry__0_n_7\,
      DI(7) => '0',
      DI(6) => \writeaddr_FSM_next1_carry__0_i_1_n_0\,
      DI(5) => \writeaddr_FSM_next1_carry__0_i_2_n_0\,
      DI(4) => \writeaddr_FSM_next1_carry__0_i_3_n_0\,
      DI(3) => \writeaddr_FSM_next1_carry__0_i_4_n_0\,
      DI(2) => \writeaddr_FSM_next1_carry__0_i_5_n_0\,
      DI(1) => \writeaddr_FSM_next1_carry__0_i_6_n_0\,
      DI(0) => \writeaddr_FSM_next1_carry__0_i_7_n_0\,
      O(7 downto 0) => \NLW_writeaddr_FSM_next1_carry__0_O_UNCONNECTED\(7 downto 0),
      S(7) => '0',
      S(6) => \writeaddr_FSM_next1_carry__0_i_8_n_0\,
      S(5) => \writeaddr_FSM_next1_carry__0_i_9_n_0\,
      S(4) => \writeaddr_FSM_next1_carry__0_i_10_n_0\,
      S(3) => \writeaddr_FSM_next1_carry__0_i_11_n_0\,
      S(2) => \writeaddr_FSM_next1_carry__0_i_12_n_0\,
      S(1) => \writeaddr_FSM_next1_carry__0_i_13_n_0\,
      S(0) => \writeaddr_FSM_next1_carry__0_i_14_n_0\
    );
\writeaddr_FSM_next1_carry__0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => p_0_in(30),
      I1 => write_blockamount(30),
      I2 => write_blockamount(31),
      I3 => p_0_in(31),
      O => \writeaddr_FSM_next1_carry__0_i_1_n_0\
    );
\writeaddr_FSM_next1_carry__0_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => write_blockamount(27),
      I1 => p_0_in(27),
      I2 => write_blockamount(26),
      I3 => p_0_in(26),
      O => \writeaddr_FSM_next1_carry__0_i_10_n_0\
    );
\writeaddr_FSM_next1_carry__0_i_11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => write_blockamount(25),
      I1 => p_0_in(25),
      I2 => write_blockamount(24),
      I3 => p_0_in(24),
      O => \writeaddr_FSM_next1_carry__0_i_11_n_0\
    );
\writeaddr_FSM_next1_carry__0_i_12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => write_blockamount(23),
      I1 => p_0_in(23),
      I2 => write_blockamount(22),
      I3 => p_0_in(22),
      O => \writeaddr_FSM_next1_carry__0_i_12_n_0\
    );
\writeaddr_FSM_next1_carry__0_i_13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => write_blockamount(21),
      I1 => p_0_in(21),
      I2 => write_blockamount(20),
      I3 => p_0_in(20),
      O => \writeaddr_FSM_next1_carry__0_i_13_n_0\
    );
\writeaddr_FSM_next1_carry__0_i_14\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => write_blockamount(19),
      I1 => p_0_in(19),
      I2 => write_blockamount(18),
      I3 => p_0_in(18),
      O => \writeaddr_FSM_next1_carry__0_i_14_n_0\
    );
\writeaddr_FSM_next1_carry__0_i_15\: unisim.vcomponents.CARRY8
     port map (
      CI => \writeaddr_FSM_next1_carry__0_i_16_n_0\,
      CI_TOP => '0',
      CO(7 downto 4) => \NLW_writeaddr_FSM_next1_carry__0_i_15_CO_UNCONNECTED\(7 downto 4),
      CO(3) => \writeaddr_FSM_next1_carry__0_i_15_n_4\,
      CO(2) => \writeaddr_FSM_next1_carry__0_i_15_n_5\,
      CO(1) => \writeaddr_FSM_next1_carry__0_i_15_n_6\,
      CO(0) => \writeaddr_FSM_next1_carry__0_i_15_n_7\,
      DI(7 downto 0) => B"00000000",
      O(7 downto 5) => \NLW_writeaddr_FSM_next1_carry__0_i_15_O_UNCONNECTED\(7 downto 5),
      O(4 downto 0) => p_0_in(31 downto 27),
      S(7 downto 5) => B"000",
      S(4 downto 0) => writeaddr_count_reg(31 downto 27)
    );
\writeaddr_FSM_next1_carry__0_i_16\: unisim.vcomponents.CARRY8
     port map (
      CI => writeaddr_FSM_next1_carry_i_17_n_0,
      CI_TOP => '0',
      CO(7) => \writeaddr_FSM_next1_carry__0_i_16_n_0\,
      CO(6) => \writeaddr_FSM_next1_carry__0_i_16_n_1\,
      CO(5) => \writeaddr_FSM_next1_carry__0_i_16_n_2\,
      CO(4) => \writeaddr_FSM_next1_carry__0_i_16_n_3\,
      CO(3) => \writeaddr_FSM_next1_carry__0_i_16_n_4\,
      CO(2) => \writeaddr_FSM_next1_carry__0_i_16_n_5\,
      CO(1) => \writeaddr_FSM_next1_carry__0_i_16_n_6\,
      CO(0) => \writeaddr_FSM_next1_carry__0_i_16_n_7\,
      DI(7 downto 0) => B"00000000",
      O(7 downto 0) => p_0_in(26 downto 19),
      S(7 downto 0) => writeaddr_count_reg(26 downto 19)
    );
\writeaddr_FSM_next1_carry__0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => p_0_in(28),
      I1 => write_blockamount(28),
      I2 => write_blockamount(29),
      I3 => p_0_in(29),
      O => \writeaddr_FSM_next1_carry__0_i_2_n_0\
    );
\writeaddr_FSM_next1_carry__0_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => p_0_in(26),
      I1 => write_blockamount(26),
      I2 => write_blockamount(27),
      I3 => p_0_in(27),
      O => \writeaddr_FSM_next1_carry__0_i_3_n_0\
    );
\writeaddr_FSM_next1_carry__0_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => p_0_in(24),
      I1 => write_blockamount(24),
      I2 => write_blockamount(25),
      I3 => p_0_in(25),
      O => \writeaddr_FSM_next1_carry__0_i_4_n_0\
    );
\writeaddr_FSM_next1_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => p_0_in(22),
      I1 => write_blockamount(22),
      I2 => write_blockamount(23),
      I3 => p_0_in(23),
      O => \writeaddr_FSM_next1_carry__0_i_5_n_0\
    );
\writeaddr_FSM_next1_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => p_0_in(20),
      I1 => write_blockamount(20),
      I2 => write_blockamount(21),
      I3 => p_0_in(21),
      O => \writeaddr_FSM_next1_carry__0_i_6_n_0\
    );
\writeaddr_FSM_next1_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => p_0_in(18),
      I1 => write_blockamount(18),
      I2 => write_blockamount(19),
      I3 => p_0_in(19),
      O => \writeaddr_FSM_next1_carry__0_i_7_n_0\
    );
\writeaddr_FSM_next1_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => write_blockamount(31),
      I1 => p_0_in(31),
      I2 => write_blockamount(30),
      I3 => p_0_in(30),
      O => \writeaddr_FSM_next1_carry__0_i_8_n_0\
    );
\writeaddr_FSM_next1_carry__0_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => write_blockamount(29),
      I1 => p_0_in(29),
      I2 => write_blockamount(28),
      I3 => p_0_in(28),
      O => \writeaddr_FSM_next1_carry__0_i_9_n_0\
    );
writeaddr_FSM_next1_carry_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => write_blockamount(0),
      I1 => write_blockamount(1),
      O => writeaddr_FSM_next1_carry_i_1_n_0
    );
writeaddr_FSM_next1_carry_i_10: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => write_blockamount(15),
      I1 => p_0_in(15),
      I2 => write_blockamount(14),
      I3 => p_0_in(14),
      O => writeaddr_FSM_next1_carry_i_10_n_0
    );
writeaddr_FSM_next1_carry_i_11: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => write_blockamount(13),
      I1 => p_0_in(13),
      I2 => write_blockamount(12),
      I3 => p_0_in(12),
      O => writeaddr_FSM_next1_carry_i_11_n_0
    );
writeaddr_FSM_next1_carry_i_12: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => write_blockamount(11),
      I1 => p_0_in(11),
      I2 => write_blockamount(10),
      I3 => p_0_in(10),
      O => writeaddr_FSM_next1_carry_i_12_n_0
    );
writeaddr_FSM_next1_carry_i_13: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => write_blockamount(9),
      I1 => p_0_in(9),
      I2 => write_blockamount(8),
      I3 => p_0_in(8),
      O => writeaddr_FSM_next1_carry_i_13_n_0
    );
writeaddr_FSM_next1_carry_i_14: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => write_blockamount(7),
      I1 => p_0_in(7),
      I2 => write_blockamount(6),
      I3 => p_0_in(6),
      O => writeaddr_FSM_next1_carry_i_14_n_0
    );
writeaddr_FSM_next1_carry_i_15: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => write_blockamount(5),
      I1 => p_0_in(5),
      I2 => write_blockamount(4),
      I3 => p_0_in(4),
      O => writeaddr_FSM_next1_carry_i_15_n_0
    );
writeaddr_FSM_next1_carry_i_16: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => write_blockamount(2),
      I1 => write_blockamount(3),
      O => writeaddr_FSM_next1_carry_i_16_n_0
    );
writeaddr_FSM_next1_carry_i_17: unisim.vcomponents.CARRY8
     port map (
      CI => writeaddr_FSM_next1_carry_i_18_n_0,
      CI_TOP => '0',
      CO(7) => writeaddr_FSM_next1_carry_i_17_n_0,
      CO(6) => writeaddr_FSM_next1_carry_i_17_n_1,
      CO(5) => writeaddr_FSM_next1_carry_i_17_n_2,
      CO(4) => writeaddr_FSM_next1_carry_i_17_n_3,
      CO(3) => writeaddr_FSM_next1_carry_i_17_n_4,
      CO(2) => writeaddr_FSM_next1_carry_i_17_n_5,
      CO(1) => writeaddr_FSM_next1_carry_i_17_n_6,
      CO(0) => writeaddr_FSM_next1_carry_i_17_n_7,
      DI(7 downto 0) => B"00000000",
      O(7 downto 0) => p_0_in(18 downto 11),
      S(7 downto 0) => writeaddr_count_reg(18 downto 11)
    );
writeaddr_FSM_next1_carry_i_18: unisim.vcomponents.CARRY8
     port map (
      CI => '0',
      CI_TOP => '0',
      CO(7) => writeaddr_FSM_next1_carry_i_18_n_0,
      CO(6) => writeaddr_FSM_next1_carry_i_18_n_1,
      CO(5) => writeaddr_FSM_next1_carry_i_18_n_2,
      CO(4) => writeaddr_FSM_next1_carry_i_18_n_3,
      CO(3) => writeaddr_FSM_next1_carry_i_18_n_4,
      CO(2) => writeaddr_FSM_next1_carry_i_18_n_5,
      CO(1) => writeaddr_FSM_next1_carry_i_18_n_6,
      CO(0) => writeaddr_FSM_next1_carry_i_18_n_7,
      DI(7 downto 2) => B"000000",
      DI(1) => writeaddr_count_reg(4),
      DI(0) => '0',
      O(7 downto 1) => p_0_in(10 downto 4),
      O(0) => NLW_writeaddr_FSM_next1_carry_i_18_O_UNCONNECTED(0),
      S(7 downto 2) => writeaddr_count_reg(10 downto 5),
      S(1) => writeaddr_FSM_next1_carry_i_19_n_0,
      S(0) => '0'
    );
writeaddr_FSM_next1_carry_i_19: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => writeaddr_count_reg(4),
      O => writeaddr_FSM_next1_carry_i_19_n_0
    );
writeaddr_FSM_next1_carry_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => p_0_in(16),
      I1 => write_blockamount(16),
      I2 => write_blockamount(17),
      I3 => p_0_in(17),
      O => writeaddr_FSM_next1_carry_i_2_n_0
    );
writeaddr_FSM_next1_carry_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => p_0_in(14),
      I1 => write_blockamount(14),
      I2 => write_blockamount(15),
      I3 => p_0_in(15),
      O => writeaddr_FSM_next1_carry_i_3_n_0
    );
writeaddr_FSM_next1_carry_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => p_0_in(12),
      I1 => write_blockamount(12),
      I2 => write_blockamount(13),
      I3 => p_0_in(13),
      O => writeaddr_FSM_next1_carry_i_4_n_0
    );
writeaddr_FSM_next1_carry_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => p_0_in(10),
      I1 => write_blockamount(10),
      I2 => write_blockamount(11),
      I3 => p_0_in(11),
      O => writeaddr_FSM_next1_carry_i_5_n_0
    );
writeaddr_FSM_next1_carry_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => p_0_in(8),
      I1 => write_blockamount(8),
      I2 => write_blockamount(9),
      I3 => p_0_in(9),
      O => writeaddr_FSM_next1_carry_i_6_n_0
    );
writeaddr_FSM_next1_carry_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => p_0_in(6),
      I1 => write_blockamount(6),
      I2 => write_blockamount(7),
      I3 => p_0_in(7),
      O => writeaddr_FSM_next1_carry_i_7_n_0
    );
writeaddr_FSM_next1_carry_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => p_0_in(4),
      I1 => write_blockamount(4),
      I2 => write_blockamount(5),
      I3 => p_0_in(5),
      O => writeaddr_FSM_next1_carry_i_8_n_0
    );
writeaddr_FSM_next1_carry_i_9: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => write_blockamount(17),
      I1 => p_0_in(17),
      I2 => write_blockamount(16),
      I3 => p_0_in(16),
      O => writeaddr_FSM_next1_carry_i_9_n_0
    );
\writeaddr_count[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => writeaddr_FSM_next(0),
      I1 => \FSM_sequential_writeaddr_FSM_next_reg_n_0_[1]\,
      I2 => writeaddr_FSM_next(2),
      I3 => \^axi_awvalid_reg_0\,
      I4 => AXI_AWREADY,
      O => writeaddr_count
    );
\writeaddr_count[4]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => writeaddr_count_reg(4),
      O => \writeaddr_count[4]_i_3_n_0\
    );
\writeaddr_count_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writeaddr_count,
      D => \writeaddr_count_reg[4]_i_2_n_9\,
      Q => writeaddr_count_reg(10),
      R => writeaddr_count0
    );
\writeaddr_count_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writeaddr_count,
      D => \writeaddr_count_reg[4]_i_2_n_8\,
      Q => writeaddr_count_reg(11),
      R => writeaddr_count0
    );
\writeaddr_count_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writeaddr_count,
      D => \writeaddr_count_reg[12]_i_1_n_15\,
      Q => writeaddr_count_reg(12),
      R => writeaddr_count0
    );
\writeaddr_count_reg[12]_i_1\: unisim.vcomponents.CARRY8
     port map (
      CI => \writeaddr_count_reg[4]_i_2_n_0\,
      CI_TOP => '0',
      CO(7) => \writeaddr_count_reg[12]_i_1_n_0\,
      CO(6) => \writeaddr_count_reg[12]_i_1_n_1\,
      CO(5) => \writeaddr_count_reg[12]_i_1_n_2\,
      CO(4) => \writeaddr_count_reg[12]_i_1_n_3\,
      CO(3) => \writeaddr_count_reg[12]_i_1_n_4\,
      CO(2) => \writeaddr_count_reg[12]_i_1_n_5\,
      CO(1) => \writeaddr_count_reg[12]_i_1_n_6\,
      CO(0) => \writeaddr_count_reg[12]_i_1_n_7\,
      DI(7 downto 0) => B"00000000",
      O(7) => \writeaddr_count_reg[12]_i_1_n_8\,
      O(6) => \writeaddr_count_reg[12]_i_1_n_9\,
      O(5) => \writeaddr_count_reg[12]_i_1_n_10\,
      O(4) => \writeaddr_count_reg[12]_i_1_n_11\,
      O(3) => \writeaddr_count_reg[12]_i_1_n_12\,
      O(2) => \writeaddr_count_reg[12]_i_1_n_13\,
      O(1) => \writeaddr_count_reg[12]_i_1_n_14\,
      O(0) => \writeaddr_count_reg[12]_i_1_n_15\,
      S(7 downto 0) => writeaddr_count_reg(19 downto 12)
    );
\writeaddr_count_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writeaddr_count,
      D => \writeaddr_count_reg[12]_i_1_n_14\,
      Q => writeaddr_count_reg(13),
      R => writeaddr_count0
    );
\writeaddr_count_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writeaddr_count,
      D => \writeaddr_count_reg[12]_i_1_n_13\,
      Q => writeaddr_count_reg(14),
      R => writeaddr_count0
    );
\writeaddr_count_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writeaddr_count,
      D => \writeaddr_count_reg[12]_i_1_n_12\,
      Q => writeaddr_count_reg(15),
      R => writeaddr_count0
    );
\writeaddr_count_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writeaddr_count,
      D => \writeaddr_count_reg[12]_i_1_n_11\,
      Q => writeaddr_count_reg(16),
      R => writeaddr_count0
    );
\writeaddr_count_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writeaddr_count,
      D => \writeaddr_count_reg[12]_i_1_n_10\,
      Q => writeaddr_count_reg(17),
      R => writeaddr_count0
    );
\writeaddr_count_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writeaddr_count,
      D => \writeaddr_count_reg[12]_i_1_n_9\,
      Q => writeaddr_count_reg(18),
      R => writeaddr_count0
    );
\writeaddr_count_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writeaddr_count,
      D => \writeaddr_count_reg[12]_i_1_n_8\,
      Q => writeaddr_count_reg(19),
      R => writeaddr_count0
    );
\writeaddr_count_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writeaddr_count,
      D => \writeaddr_count_reg[20]_i_1_n_15\,
      Q => writeaddr_count_reg(20),
      R => writeaddr_count0
    );
\writeaddr_count_reg[20]_i_1\: unisim.vcomponents.CARRY8
     port map (
      CI => \writeaddr_count_reg[12]_i_1_n_0\,
      CI_TOP => '0',
      CO(7) => \writeaddr_count_reg[20]_i_1_n_0\,
      CO(6) => \writeaddr_count_reg[20]_i_1_n_1\,
      CO(5) => \writeaddr_count_reg[20]_i_1_n_2\,
      CO(4) => \writeaddr_count_reg[20]_i_1_n_3\,
      CO(3) => \writeaddr_count_reg[20]_i_1_n_4\,
      CO(2) => \writeaddr_count_reg[20]_i_1_n_5\,
      CO(1) => \writeaddr_count_reg[20]_i_1_n_6\,
      CO(0) => \writeaddr_count_reg[20]_i_1_n_7\,
      DI(7 downto 0) => B"00000000",
      O(7) => \writeaddr_count_reg[20]_i_1_n_8\,
      O(6) => \writeaddr_count_reg[20]_i_1_n_9\,
      O(5) => \writeaddr_count_reg[20]_i_1_n_10\,
      O(4) => \writeaddr_count_reg[20]_i_1_n_11\,
      O(3) => \writeaddr_count_reg[20]_i_1_n_12\,
      O(2) => \writeaddr_count_reg[20]_i_1_n_13\,
      O(1) => \writeaddr_count_reg[20]_i_1_n_14\,
      O(0) => \writeaddr_count_reg[20]_i_1_n_15\,
      S(7 downto 0) => writeaddr_count_reg(27 downto 20)
    );
\writeaddr_count_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writeaddr_count,
      D => \writeaddr_count_reg[20]_i_1_n_14\,
      Q => writeaddr_count_reg(21),
      R => writeaddr_count0
    );
\writeaddr_count_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writeaddr_count,
      D => \writeaddr_count_reg[20]_i_1_n_13\,
      Q => writeaddr_count_reg(22),
      R => writeaddr_count0
    );
\writeaddr_count_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writeaddr_count,
      D => \writeaddr_count_reg[20]_i_1_n_12\,
      Q => writeaddr_count_reg(23),
      R => writeaddr_count0
    );
\writeaddr_count_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writeaddr_count,
      D => \writeaddr_count_reg[20]_i_1_n_11\,
      Q => writeaddr_count_reg(24),
      R => writeaddr_count0
    );
\writeaddr_count_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writeaddr_count,
      D => \writeaddr_count_reg[20]_i_1_n_10\,
      Q => writeaddr_count_reg(25),
      R => writeaddr_count0
    );
\writeaddr_count_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writeaddr_count,
      D => \writeaddr_count_reg[20]_i_1_n_9\,
      Q => writeaddr_count_reg(26),
      R => writeaddr_count0
    );
\writeaddr_count_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writeaddr_count,
      D => \writeaddr_count_reg[20]_i_1_n_8\,
      Q => writeaddr_count_reg(27),
      R => writeaddr_count0
    );
\writeaddr_count_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writeaddr_count,
      D => \writeaddr_count_reg[28]_i_1_n_15\,
      Q => writeaddr_count_reg(28),
      R => writeaddr_count0
    );
\writeaddr_count_reg[28]_i_1\: unisim.vcomponents.CARRY8
     port map (
      CI => \writeaddr_count_reg[20]_i_1_n_0\,
      CI_TOP => '0',
      CO(7 downto 3) => \NLW_writeaddr_count_reg[28]_i_1_CO_UNCONNECTED\(7 downto 3),
      CO(2) => \writeaddr_count_reg[28]_i_1_n_5\,
      CO(1) => \writeaddr_count_reg[28]_i_1_n_6\,
      CO(0) => \writeaddr_count_reg[28]_i_1_n_7\,
      DI(7 downto 0) => B"00000000",
      O(7 downto 4) => \NLW_writeaddr_count_reg[28]_i_1_O_UNCONNECTED\(7 downto 4),
      O(3) => \writeaddr_count_reg[28]_i_1_n_12\,
      O(2) => \writeaddr_count_reg[28]_i_1_n_13\,
      O(1) => \writeaddr_count_reg[28]_i_1_n_14\,
      O(0) => \writeaddr_count_reg[28]_i_1_n_15\,
      S(7 downto 4) => B"0000",
      S(3 downto 0) => writeaddr_count_reg(31 downto 28)
    );
\writeaddr_count_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writeaddr_count,
      D => \writeaddr_count_reg[28]_i_1_n_14\,
      Q => writeaddr_count_reg(29),
      R => writeaddr_count0
    );
\writeaddr_count_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writeaddr_count,
      D => \writeaddr_count_reg[28]_i_1_n_13\,
      Q => writeaddr_count_reg(30),
      R => writeaddr_count0
    );
\writeaddr_count_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writeaddr_count,
      D => \writeaddr_count_reg[28]_i_1_n_12\,
      Q => writeaddr_count_reg(31),
      R => writeaddr_count0
    );
\writeaddr_count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writeaddr_count,
      D => \writeaddr_count_reg[4]_i_2_n_15\,
      Q => writeaddr_count_reg(4),
      R => writeaddr_count0
    );
\writeaddr_count_reg[4]_i_2\: unisim.vcomponents.CARRY8
     port map (
      CI => '0',
      CI_TOP => '0',
      CO(7) => \writeaddr_count_reg[4]_i_2_n_0\,
      CO(6) => \writeaddr_count_reg[4]_i_2_n_1\,
      CO(5) => \writeaddr_count_reg[4]_i_2_n_2\,
      CO(4) => \writeaddr_count_reg[4]_i_2_n_3\,
      CO(3) => \writeaddr_count_reg[4]_i_2_n_4\,
      CO(2) => \writeaddr_count_reg[4]_i_2_n_5\,
      CO(1) => \writeaddr_count_reg[4]_i_2_n_6\,
      CO(0) => \writeaddr_count_reg[4]_i_2_n_7\,
      DI(7 downto 0) => B"00000001",
      O(7) => \writeaddr_count_reg[4]_i_2_n_8\,
      O(6) => \writeaddr_count_reg[4]_i_2_n_9\,
      O(5) => \writeaddr_count_reg[4]_i_2_n_10\,
      O(4) => \writeaddr_count_reg[4]_i_2_n_11\,
      O(3) => \writeaddr_count_reg[4]_i_2_n_12\,
      O(2) => \writeaddr_count_reg[4]_i_2_n_13\,
      O(1) => \writeaddr_count_reg[4]_i_2_n_14\,
      O(0) => \writeaddr_count_reg[4]_i_2_n_15\,
      S(7 downto 1) => writeaddr_count_reg(11 downto 5),
      S(0) => \writeaddr_count[4]_i_3_n_0\
    );
\writeaddr_count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writeaddr_count,
      D => \writeaddr_count_reg[4]_i_2_n_14\,
      Q => writeaddr_count_reg(5),
      R => writeaddr_count0
    );
\writeaddr_count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writeaddr_count,
      D => \writeaddr_count_reg[4]_i_2_n_13\,
      Q => writeaddr_count_reg(6),
      R => writeaddr_count0
    );
\writeaddr_count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writeaddr_count,
      D => \writeaddr_count_reg[4]_i_2_n_12\,
      Q => writeaddr_count_reg(7),
      R => writeaddr_count0
    );
\writeaddr_count_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writeaddr_count,
      D => \writeaddr_count_reg[4]_i_2_n_11\,
      Q => writeaddr_count_reg(8),
      R => writeaddr_count0
    );
\writeaddr_count_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writeaddr_count,
      D => \writeaddr_count_reg[4]_i_2_n_10\,
      Q => writeaddr_count_reg(9),
      R => writeaddr_count0
    );
writeaddr_done_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE40"
    )
        port map (
      I0 => writeaddr_FSM_next(2),
      I1 => \FSM_sequential_writeaddr_FSM_next_reg_n_0_[1]\,
      I2 => writeaddr_FSM_next(0),
      I3 => writeaddr_done_reg_n_0,
      O => writeaddr_done_i_1_n_0
    );
writeaddr_done_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => writeaddr_done_i_1_n_0,
      Q => writeaddr_done_reg_n_0,
      R => '0'
    );
writedata_FSM_next1_carry: unisim.vcomponents.CARRY8
     port map (
      CI => '1',
      CI_TOP => '0',
      CO(7) => writedata_FSM_next1_carry_n_0,
      CO(6) => writedata_FSM_next1_carry_n_1,
      CO(5) => writedata_FSM_next1_carry_n_2,
      CO(4) => writedata_FSM_next1_carry_n_3,
      CO(3) => writedata_FSM_next1_carry_n_4,
      CO(2) => writedata_FSM_next1_carry_n_5,
      CO(1) => writedata_FSM_next1_carry_n_6,
      CO(0) => writedata_FSM_next1_carry_n_7,
      DI(7 downto 0) => B"00000000",
      O(7 downto 0) => NLW_writedata_FSM_next1_carry_O_UNCONNECTED(7 downto 0),
      S(7) => writedata_FSM_next1_carry_i_1_n_0,
      S(6) => writedata_FSM_next1_carry_i_2_n_0,
      S(5) => writedata_FSM_next1_carry_i_3_n_0,
      S(4) => writedata_FSM_next1_carry_i_4_n_0,
      S(3) => writedata_FSM_next1_carry_i_5_n_0,
      S(2) => writedata_FSM_next1_carry_i_6_n_0,
      S(1) => writedata_FSM_next1_carry_i_7_n_0,
      S(0) => writedata_FSM_next1_carry_i_8_n_0
    );
\writedata_FSM_next1_carry__0\: unisim.vcomponents.CARRY8
     port map (
      CI => writedata_FSM_next1_carry_n_0,
      CI_TOP => '0',
      CO(7 downto 3) => \NLW_writedata_FSM_next1_carry__0_CO_UNCONNECTED\(7 downto 3),
      CO(2) => writedata_FSM_next1,
      CO(1) => \writedata_FSM_next1_carry__0_n_6\,
      CO(0) => \writedata_FSM_next1_carry__0_n_7\,
      DI(7 downto 0) => B"00000000",
      O(7 downto 0) => \NLW_writedata_FSM_next1_carry__0_O_UNCONNECTED\(7 downto 0),
      S(7 downto 3) => B"00000",
      S(2) => \writedata_FSM_next1_carry__0_i_1_n_0\,
      S(1) => \writedata_FSM_next1_carry__0_i_2_n_0\,
      S(0) => \writedata_FSM_next1_carry__0_i_3_n_0\
    );
\writedata_FSM_next1_carry__0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \writedata_count_reg__0\(30),
      I1 => writedata_FSM_next2(30),
      I2 => \writedata_count_reg__0\(31),
      I3 => writedata_FSM_next2(31),
      O => \writedata_FSM_next1_carry__0_i_1_n_0\
    );
\writedata_FSM_next1_carry__0_i_10\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => write_blockamount(26),
      O => \writedata_FSM_next1_carry__0_i_10_n_0\
    );
\writedata_FSM_next1_carry__0_i_11\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => write_blockamount(25),
      O => \writedata_FSM_next1_carry__0_i_11_n_0\
    );
\writedata_FSM_next1_carry__0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \writedata_count_reg__0\(28),
      I1 => writedata_FSM_next2(28),
      I2 => \writedata_count_reg__0\(27),
      I3 => writedata_FSM_next2(27),
      I4 => writedata_FSM_next2(29),
      I5 => \writedata_count_reg__0\(29),
      O => \writedata_FSM_next1_carry__0_i_2_n_0\
    );
\writedata_FSM_next1_carry__0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \writedata_count_reg__0\(25),
      I1 => writedata_FSM_next2(25),
      I2 => \writedata_count_reg__0\(24),
      I3 => writedata_FSM_next2(24),
      I4 => writedata_FSM_next2(26),
      I5 => \writedata_count_reg__0\(26),
      O => \writedata_FSM_next1_carry__0_i_3_n_0\
    );
\writedata_FSM_next1_carry__0_i_4\: unisim.vcomponents.CARRY8
     port map (
      CI => writedata_FSM_next1_carry_i_9_n_0,
      CI_TOP => '0',
      CO(7 downto 6) => \NLW_writedata_FSM_next1_carry__0_i_4_CO_UNCONNECTED\(7 downto 6),
      CO(5) => \writedata_FSM_next1_carry__0_i_4_n_2\,
      CO(4) => \writedata_FSM_next1_carry__0_i_4_n_3\,
      CO(3) => \writedata_FSM_next1_carry__0_i_4_n_4\,
      CO(2) => \writedata_FSM_next1_carry__0_i_4_n_5\,
      CO(1) => \writedata_FSM_next1_carry__0_i_4_n_6\,
      CO(0) => \writedata_FSM_next1_carry__0_i_4_n_7\,
      DI(7 downto 6) => B"00",
      DI(5 downto 0) => write_blockamount(30 downto 25),
      O(7) => \NLW_writedata_FSM_next1_carry__0_i_4_O_UNCONNECTED\(7),
      O(6 downto 0) => writedata_FSM_next2(31 downto 25),
      S(7) => '0',
      S(6) => \writedata_FSM_next1_carry__0_i_5_n_0\,
      S(5) => \writedata_FSM_next1_carry__0_i_6_n_0\,
      S(4) => \writedata_FSM_next1_carry__0_i_7_n_0\,
      S(3) => \writedata_FSM_next1_carry__0_i_8_n_0\,
      S(2) => \writedata_FSM_next1_carry__0_i_9_n_0\,
      S(1) => \writedata_FSM_next1_carry__0_i_10_n_0\,
      S(0) => \writedata_FSM_next1_carry__0_i_11_n_0\
    );
\writedata_FSM_next1_carry__0_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => write_blockamount(31),
      O => \writedata_FSM_next1_carry__0_i_5_n_0\
    );
\writedata_FSM_next1_carry__0_i_6\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => write_blockamount(30),
      O => \writedata_FSM_next1_carry__0_i_6_n_0\
    );
\writedata_FSM_next1_carry__0_i_7\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => write_blockamount(29),
      O => \writedata_FSM_next1_carry__0_i_7_n_0\
    );
\writedata_FSM_next1_carry__0_i_8\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => write_blockamount(28),
      O => \writedata_FSM_next1_carry__0_i_8_n_0\
    );
\writedata_FSM_next1_carry__0_i_9\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => write_blockamount(27),
      O => \writedata_FSM_next1_carry__0_i_9_n_0\
    );
writedata_FSM_next1_carry_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \writedata_count_reg__0\(22),
      I1 => writedata_FSM_next2(22),
      I2 => \writedata_count_reg__0\(21),
      I3 => writedata_FSM_next2(21),
      I4 => writedata_FSM_next2(23),
      I5 => \writedata_count_reg__0\(23),
      O => writedata_FSM_next1_carry_i_1_n_0
    );
writedata_FSM_next1_carry_i_10: unisim.vcomponents.CARRY8
     port map (
      CI => writedata_FSM_next1_carry_i_11_n_0,
      CI_TOP => '0',
      CO(7) => writedata_FSM_next1_carry_i_10_n_0,
      CO(6) => writedata_FSM_next1_carry_i_10_n_1,
      CO(5) => writedata_FSM_next1_carry_i_10_n_2,
      CO(4) => writedata_FSM_next1_carry_i_10_n_3,
      CO(3) => writedata_FSM_next1_carry_i_10_n_4,
      CO(2) => writedata_FSM_next1_carry_i_10_n_5,
      CO(1) => writedata_FSM_next1_carry_i_10_n_6,
      CO(0) => writedata_FSM_next1_carry_i_10_n_7,
      DI(7 downto 0) => write_blockamount(16 downto 9),
      O(7 downto 0) => writedata_FSM_next2(16 downto 9),
      S(7) => writedata_FSM_next1_carry_i_20_n_0,
      S(6) => writedata_FSM_next1_carry_i_21_n_0,
      S(5) => writedata_FSM_next1_carry_i_22_n_0,
      S(4) => writedata_FSM_next1_carry_i_23_n_0,
      S(3) => writedata_FSM_next1_carry_i_24_n_0,
      S(2) => writedata_FSM_next1_carry_i_25_n_0,
      S(1) => writedata_FSM_next1_carry_i_26_n_0,
      S(0) => writedata_FSM_next1_carry_i_27_n_0
    );
writedata_FSM_next1_carry_i_11: unisim.vcomponents.CARRY8
     port map (
      CI => write_blockamount(0),
      CI_TOP => '0',
      CO(7) => writedata_FSM_next1_carry_i_11_n_0,
      CO(6) => writedata_FSM_next1_carry_i_11_n_1,
      CO(5) => writedata_FSM_next1_carry_i_11_n_2,
      CO(4) => writedata_FSM_next1_carry_i_11_n_3,
      CO(3) => writedata_FSM_next1_carry_i_11_n_4,
      CO(2) => writedata_FSM_next1_carry_i_11_n_5,
      CO(1) => writedata_FSM_next1_carry_i_11_n_6,
      CO(0) => writedata_FSM_next1_carry_i_11_n_7,
      DI(7 downto 0) => write_blockamount(8 downto 1),
      O(7 downto 0) => writedata_FSM_next2(8 downto 1),
      S(7) => writedata_FSM_next1_carry_i_28_n_0,
      S(6) => writedata_FSM_next1_carry_i_29_n_0,
      S(5) => writedata_FSM_next1_carry_i_30_n_0,
      S(4) => writedata_FSM_next1_carry_i_31_n_0,
      S(3) => writedata_FSM_next1_carry_i_32_n_0,
      S(2) => writedata_FSM_next1_carry_i_33_n_0,
      S(1) => writedata_FSM_next1_carry_i_34_n_0,
      S(0) => writedata_FSM_next1_carry_i_35_n_0
    );
writedata_FSM_next1_carry_i_12: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => write_blockamount(24),
      O => writedata_FSM_next1_carry_i_12_n_0
    );
writedata_FSM_next1_carry_i_13: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => write_blockamount(23),
      O => writedata_FSM_next1_carry_i_13_n_0
    );
writedata_FSM_next1_carry_i_14: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => write_blockamount(22),
      O => writedata_FSM_next1_carry_i_14_n_0
    );
writedata_FSM_next1_carry_i_15: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => write_blockamount(21),
      O => writedata_FSM_next1_carry_i_15_n_0
    );
writedata_FSM_next1_carry_i_16: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => write_blockamount(20),
      O => writedata_FSM_next1_carry_i_16_n_0
    );
writedata_FSM_next1_carry_i_17: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => write_blockamount(19),
      O => writedata_FSM_next1_carry_i_17_n_0
    );
writedata_FSM_next1_carry_i_18: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => write_blockamount(18),
      O => writedata_FSM_next1_carry_i_18_n_0
    );
writedata_FSM_next1_carry_i_19: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => write_blockamount(17),
      O => writedata_FSM_next1_carry_i_19_n_0
    );
writedata_FSM_next1_carry_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \writedata_count_reg__0\(19),
      I1 => writedata_FSM_next2(19),
      I2 => \writedata_count_reg__0\(18),
      I3 => writedata_FSM_next2(18),
      I4 => writedata_FSM_next2(20),
      I5 => \writedata_count_reg__0\(20),
      O => writedata_FSM_next1_carry_i_2_n_0
    );
writedata_FSM_next1_carry_i_20: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => write_blockamount(16),
      O => writedata_FSM_next1_carry_i_20_n_0
    );
writedata_FSM_next1_carry_i_21: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => write_blockamount(15),
      O => writedata_FSM_next1_carry_i_21_n_0
    );
writedata_FSM_next1_carry_i_22: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => write_blockamount(14),
      O => writedata_FSM_next1_carry_i_22_n_0
    );
writedata_FSM_next1_carry_i_23: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => write_blockamount(13),
      O => writedata_FSM_next1_carry_i_23_n_0
    );
writedata_FSM_next1_carry_i_24: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => write_blockamount(12),
      O => writedata_FSM_next1_carry_i_24_n_0
    );
writedata_FSM_next1_carry_i_25: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => write_blockamount(11),
      O => writedata_FSM_next1_carry_i_25_n_0
    );
writedata_FSM_next1_carry_i_26: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => write_blockamount(10),
      O => writedata_FSM_next1_carry_i_26_n_0
    );
writedata_FSM_next1_carry_i_27: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => write_blockamount(9),
      O => writedata_FSM_next1_carry_i_27_n_0
    );
writedata_FSM_next1_carry_i_28: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => write_blockamount(8),
      O => writedata_FSM_next1_carry_i_28_n_0
    );
writedata_FSM_next1_carry_i_29: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => write_blockamount(7),
      O => writedata_FSM_next1_carry_i_29_n_0
    );
writedata_FSM_next1_carry_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \writedata_count_reg__0\(16),
      I1 => writedata_FSM_next2(16),
      I2 => \writedata_count_reg__0\(15),
      I3 => writedata_FSM_next2(15),
      I4 => writedata_FSM_next2(17),
      I5 => \writedata_count_reg__0\(17),
      O => writedata_FSM_next1_carry_i_3_n_0
    );
writedata_FSM_next1_carry_i_30: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => write_blockamount(6),
      O => writedata_FSM_next1_carry_i_30_n_0
    );
writedata_FSM_next1_carry_i_31: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => write_blockamount(5),
      O => writedata_FSM_next1_carry_i_31_n_0
    );
writedata_FSM_next1_carry_i_32: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => write_blockamount(4),
      O => writedata_FSM_next1_carry_i_32_n_0
    );
writedata_FSM_next1_carry_i_33: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => write_blockamount(3),
      O => writedata_FSM_next1_carry_i_33_n_0
    );
writedata_FSM_next1_carry_i_34: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => write_blockamount(2),
      O => writedata_FSM_next1_carry_i_34_n_0
    );
writedata_FSM_next1_carry_i_35: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => write_blockamount(1),
      O => writedata_FSM_next1_carry_i_35_n_0
    );
writedata_FSM_next1_carry_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \writedata_count_reg__0\(13),
      I1 => writedata_FSM_next2(13),
      I2 => \writedata_count_reg__0\(12),
      I3 => writedata_FSM_next2(12),
      I4 => writedata_FSM_next2(14),
      I5 => \writedata_count_reg__0\(14),
      O => writedata_FSM_next1_carry_i_4_n_0
    );
writedata_FSM_next1_carry_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \writedata_count_reg__0\(10),
      I1 => writedata_FSM_next2(10),
      I2 => \writedata_count_reg__0\(9),
      I3 => writedata_FSM_next2(9),
      I4 => writedata_FSM_next2(11),
      I5 => \writedata_count_reg__0\(11),
      O => writedata_FSM_next1_carry_i_5_n_0
    );
writedata_FSM_next1_carry_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \writedata_count_reg__0\(7),
      I1 => writedata_FSM_next2(7),
      I2 => \writedata_count_reg__0\(6),
      I3 => writedata_FSM_next2(6),
      I4 => writedata_FSM_next2(8),
      I5 => \writedata_count_reg__0\(8),
      O => writedata_FSM_next1_carry_i_6_n_0
    );
writedata_FSM_next1_carry_i_7: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \writedata_count_reg__0\(4),
      I1 => writedata_FSM_next2(4),
      I2 => writedata_count_reg(3),
      I3 => writedata_FSM_next2(3),
      I4 => writedata_FSM_next2(5),
      I5 => \writedata_count_reg__0\(5),
      O => writedata_FSM_next1_carry_i_7_n_0
    );
writedata_FSM_next1_carry_i_8: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0990000000000990"
    )
        port map (
      I0 => writedata_count_reg(1),
      I1 => writedata_FSM_next2(1),
      I2 => write_blockamount(0),
      I3 => writedata_count_reg(0),
      I4 => writedata_FSM_next2(2),
      I5 => writedata_count_reg(2),
      O => writedata_FSM_next1_carry_i_8_n_0
    );
writedata_FSM_next1_carry_i_9: unisim.vcomponents.CARRY8
     port map (
      CI => writedata_FSM_next1_carry_i_10_n_0,
      CI_TOP => '0',
      CO(7) => writedata_FSM_next1_carry_i_9_n_0,
      CO(6) => writedata_FSM_next1_carry_i_9_n_1,
      CO(5) => writedata_FSM_next1_carry_i_9_n_2,
      CO(4) => writedata_FSM_next1_carry_i_9_n_3,
      CO(3) => writedata_FSM_next1_carry_i_9_n_4,
      CO(2) => writedata_FSM_next1_carry_i_9_n_5,
      CO(1) => writedata_FSM_next1_carry_i_9_n_6,
      CO(0) => writedata_FSM_next1_carry_i_9_n_7,
      DI(7 downto 0) => write_blockamount(24 downto 17),
      O(7 downto 0) => writedata_FSM_next2(24 downto 17),
      S(7) => writedata_FSM_next1_carry_i_12_n_0,
      S(6) => writedata_FSM_next1_carry_i_13_n_0,
      S(5) => writedata_FSM_next1_carry_i_14_n_0,
      S(4) => writedata_FSM_next1_carry_i_15_n_0,
      S(3) => writedata_FSM_next1_carry_i_16_n_0,
      S(2) => writedata_FSM_next1_carry_i_17_n_0,
      S(1) => writedata_FSM_next1_carry_i_18_n_0,
      S(0) => writedata_FSM_next1_carry_i_19_n_0
    );
\writedata_count[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \FSM_sequential_writedata_FSM_next_reg_n_0_[1]\,
      I1 => writedata_FSM_next(0),
      I2 => writedata_FSM_next(2),
      O => writedata_count0
    );
\writedata_count[0]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"40400040"
    )
        port map (
      I0 => writedata_FSM_next(2),
      I1 => FIFO_READ_tvalid,
      I2 => AXI_WREADY,
      I3 => \FSM_sequential_writedata_FSM_next_reg_n_0_[1]\,
      I4 => writedata_FSM_next(0),
      O => writedata_count
    );
\writedata_count[0]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => writedata_count_reg(0),
      O => \writedata_count[0]_i_4_n_0\
    );
\writedata_count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writedata_count,
      D => \writedata_count_reg[0]_i_3_n_15\,
      Q => writedata_count_reg(0),
      R => writedata_count0
    );
\writedata_count_reg[0]_i_3\: unisim.vcomponents.CARRY8
     port map (
      CI => '0',
      CI_TOP => '0',
      CO(7) => \writedata_count_reg[0]_i_3_n_0\,
      CO(6) => \writedata_count_reg[0]_i_3_n_1\,
      CO(5) => \writedata_count_reg[0]_i_3_n_2\,
      CO(4) => \writedata_count_reg[0]_i_3_n_3\,
      CO(3) => \writedata_count_reg[0]_i_3_n_4\,
      CO(2) => \writedata_count_reg[0]_i_3_n_5\,
      CO(1) => \writedata_count_reg[0]_i_3_n_6\,
      CO(0) => \writedata_count_reg[0]_i_3_n_7\,
      DI(7 downto 0) => B"00000001",
      O(7) => \writedata_count_reg[0]_i_3_n_8\,
      O(6) => \writedata_count_reg[0]_i_3_n_9\,
      O(5) => \writedata_count_reg[0]_i_3_n_10\,
      O(4) => \writedata_count_reg[0]_i_3_n_11\,
      O(3) => \writedata_count_reg[0]_i_3_n_12\,
      O(2) => \writedata_count_reg[0]_i_3_n_13\,
      O(1) => \writedata_count_reg[0]_i_3_n_14\,
      O(0) => \writedata_count_reg[0]_i_3_n_15\,
      S(7 downto 4) => \writedata_count_reg__0\(7 downto 4),
      S(3 downto 1) => writedata_count_reg(3 downto 1),
      S(0) => \writedata_count[0]_i_4_n_0\
    );
\writedata_count_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writedata_count,
      D => \writedata_count_reg[8]_i_1_n_13\,
      Q => \writedata_count_reg__0\(10),
      R => writedata_count0
    );
\writedata_count_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writedata_count,
      D => \writedata_count_reg[8]_i_1_n_12\,
      Q => \writedata_count_reg__0\(11),
      R => writedata_count0
    );
\writedata_count_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writedata_count,
      D => \writedata_count_reg[8]_i_1_n_11\,
      Q => \writedata_count_reg__0\(12),
      R => writedata_count0
    );
\writedata_count_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writedata_count,
      D => \writedata_count_reg[8]_i_1_n_10\,
      Q => \writedata_count_reg__0\(13),
      R => writedata_count0
    );
\writedata_count_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writedata_count,
      D => \writedata_count_reg[8]_i_1_n_9\,
      Q => \writedata_count_reg__0\(14),
      R => writedata_count0
    );
\writedata_count_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writedata_count,
      D => \writedata_count_reg[8]_i_1_n_8\,
      Q => \writedata_count_reg__0\(15),
      R => writedata_count0
    );
\writedata_count_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writedata_count,
      D => \writedata_count_reg[16]_i_1_n_15\,
      Q => \writedata_count_reg__0\(16),
      R => writedata_count0
    );
\writedata_count_reg[16]_i_1\: unisim.vcomponents.CARRY8
     port map (
      CI => \writedata_count_reg[8]_i_1_n_0\,
      CI_TOP => '0',
      CO(7) => \writedata_count_reg[16]_i_1_n_0\,
      CO(6) => \writedata_count_reg[16]_i_1_n_1\,
      CO(5) => \writedata_count_reg[16]_i_1_n_2\,
      CO(4) => \writedata_count_reg[16]_i_1_n_3\,
      CO(3) => \writedata_count_reg[16]_i_1_n_4\,
      CO(2) => \writedata_count_reg[16]_i_1_n_5\,
      CO(1) => \writedata_count_reg[16]_i_1_n_6\,
      CO(0) => \writedata_count_reg[16]_i_1_n_7\,
      DI(7 downto 0) => B"00000000",
      O(7) => \writedata_count_reg[16]_i_1_n_8\,
      O(6) => \writedata_count_reg[16]_i_1_n_9\,
      O(5) => \writedata_count_reg[16]_i_1_n_10\,
      O(4) => \writedata_count_reg[16]_i_1_n_11\,
      O(3) => \writedata_count_reg[16]_i_1_n_12\,
      O(2) => \writedata_count_reg[16]_i_1_n_13\,
      O(1) => \writedata_count_reg[16]_i_1_n_14\,
      O(0) => \writedata_count_reg[16]_i_1_n_15\,
      S(7 downto 0) => \writedata_count_reg__0\(23 downto 16)
    );
\writedata_count_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writedata_count,
      D => \writedata_count_reg[16]_i_1_n_14\,
      Q => \writedata_count_reg__0\(17),
      R => writedata_count0
    );
\writedata_count_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writedata_count,
      D => \writedata_count_reg[16]_i_1_n_13\,
      Q => \writedata_count_reg__0\(18),
      R => writedata_count0
    );
\writedata_count_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writedata_count,
      D => \writedata_count_reg[16]_i_1_n_12\,
      Q => \writedata_count_reg__0\(19),
      R => writedata_count0
    );
\writedata_count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writedata_count,
      D => \writedata_count_reg[0]_i_3_n_14\,
      Q => writedata_count_reg(1),
      R => writedata_count0
    );
\writedata_count_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writedata_count,
      D => \writedata_count_reg[16]_i_1_n_11\,
      Q => \writedata_count_reg__0\(20),
      R => writedata_count0
    );
\writedata_count_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writedata_count,
      D => \writedata_count_reg[16]_i_1_n_10\,
      Q => \writedata_count_reg__0\(21),
      R => writedata_count0
    );
\writedata_count_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writedata_count,
      D => \writedata_count_reg[16]_i_1_n_9\,
      Q => \writedata_count_reg__0\(22),
      R => writedata_count0
    );
\writedata_count_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writedata_count,
      D => \writedata_count_reg[16]_i_1_n_8\,
      Q => \writedata_count_reg__0\(23),
      R => writedata_count0
    );
\writedata_count_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writedata_count,
      D => \writedata_count_reg[24]_i_1_n_15\,
      Q => \writedata_count_reg__0\(24),
      R => writedata_count0
    );
\writedata_count_reg[24]_i_1\: unisim.vcomponents.CARRY8
     port map (
      CI => \writedata_count_reg[16]_i_1_n_0\,
      CI_TOP => '0',
      CO(7) => \NLW_writedata_count_reg[24]_i_1_CO_UNCONNECTED\(7),
      CO(6) => \writedata_count_reg[24]_i_1_n_1\,
      CO(5) => \writedata_count_reg[24]_i_1_n_2\,
      CO(4) => \writedata_count_reg[24]_i_1_n_3\,
      CO(3) => \writedata_count_reg[24]_i_1_n_4\,
      CO(2) => \writedata_count_reg[24]_i_1_n_5\,
      CO(1) => \writedata_count_reg[24]_i_1_n_6\,
      CO(0) => \writedata_count_reg[24]_i_1_n_7\,
      DI(7 downto 0) => B"00000000",
      O(7) => \writedata_count_reg[24]_i_1_n_8\,
      O(6) => \writedata_count_reg[24]_i_1_n_9\,
      O(5) => \writedata_count_reg[24]_i_1_n_10\,
      O(4) => \writedata_count_reg[24]_i_1_n_11\,
      O(3) => \writedata_count_reg[24]_i_1_n_12\,
      O(2) => \writedata_count_reg[24]_i_1_n_13\,
      O(1) => \writedata_count_reg[24]_i_1_n_14\,
      O(0) => \writedata_count_reg[24]_i_1_n_15\,
      S(7 downto 0) => \writedata_count_reg__0\(31 downto 24)
    );
\writedata_count_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writedata_count,
      D => \writedata_count_reg[24]_i_1_n_14\,
      Q => \writedata_count_reg__0\(25),
      R => writedata_count0
    );
\writedata_count_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writedata_count,
      D => \writedata_count_reg[24]_i_1_n_13\,
      Q => \writedata_count_reg__0\(26),
      R => writedata_count0
    );
\writedata_count_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writedata_count,
      D => \writedata_count_reg[24]_i_1_n_12\,
      Q => \writedata_count_reg__0\(27),
      R => writedata_count0
    );
\writedata_count_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writedata_count,
      D => \writedata_count_reg[24]_i_1_n_11\,
      Q => \writedata_count_reg__0\(28),
      R => writedata_count0
    );
\writedata_count_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writedata_count,
      D => \writedata_count_reg[24]_i_1_n_10\,
      Q => \writedata_count_reg__0\(29),
      R => writedata_count0
    );
\writedata_count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writedata_count,
      D => \writedata_count_reg[0]_i_3_n_13\,
      Q => writedata_count_reg(2),
      R => writedata_count0
    );
\writedata_count_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writedata_count,
      D => \writedata_count_reg[24]_i_1_n_9\,
      Q => \writedata_count_reg__0\(30),
      R => writedata_count0
    );
\writedata_count_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writedata_count,
      D => \writedata_count_reg[24]_i_1_n_8\,
      Q => \writedata_count_reg__0\(31),
      R => writedata_count0
    );
\writedata_count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writedata_count,
      D => \writedata_count_reg[0]_i_3_n_12\,
      Q => writedata_count_reg(3),
      R => writedata_count0
    );
\writedata_count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writedata_count,
      D => \writedata_count_reg[0]_i_3_n_11\,
      Q => \writedata_count_reg__0\(4),
      R => writedata_count0
    );
\writedata_count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writedata_count,
      D => \writedata_count_reg[0]_i_3_n_10\,
      Q => \writedata_count_reg__0\(5),
      R => writedata_count0
    );
\writedata_count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writedata_count,
      D => \writedata_count_reg[0]_i_3_n_9\,
      Q => \writedata_count_reg__0\(6),
      R => writedata_count0
    );
\writedata_count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writedata_count,
      D => \writedata_count_reg[0]_i_3_n_8\,
      Q => \writedata_count_reg__0\(7),
      R => writedata_count0
    );
\writedata_count_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writedata_count,
      D => \writedata_count_reg[8]_i_1_n_15\,
      Q => \writedata_count_reg__0\(8),
      R => writedata_count0
    );
\writedata_count_reg[8]_i_1\: unisim.vcomponents.CARRY8
     port map (
      CI => \writedata_count_reg[0]_i_3_n_0\,
      CI_TOP => '0',
      CO(7) => \writedata_count_reg[8]_i_1_n_0\,
      CO(6) => \writedata_count_reg[8]_i_1_n_1\,
      CO(5) => \writedata_count_reg[8]_i_1_n_2\,
      CO(4) => \writedata_count_reg[8]_i_1_n_3\,
      CO(3) => \writedata_count_reg[8]_i_1_n_4\,
      CO(2) => \writedata_count_reg[8]_i_1_n_5\,
      CO(1) => \writedata_count_reg[8]_i_1_n_6\,
      CO(0) => \writedata_count_reg[8]_i_1_n_7\,
      DI(7 downto 0) => B"00000000",
      O(7) => \writedata_count_reg[8]_i_1_n_8\,
      O(6) => \writedata_count_reg[8]_i_1_n_9\,
      O(5) => \writedata_count_reg[8]_i_1_n_10\,
      O(4) => \writedata_count_reg[8]_i_1_n_11\,
      O(3) => \writedata_count_reg[8]_i_1_n_12\,
      O(2) => \writedata_count_reg[8]_i_1_n_13\,
      O(1) => \writedata_count_reg[8]_i_1_n_14\,
      O(0) => \writedata_count_reg[8]_i_1_n_15\,
      S(7 downto 0) => \writedata_count_reg__0\(15 downto 8)
    );
\writedata_count_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => writedata_count,
      D => \writedata_count_reg[8]_i_1_n_14\,
      Q => \writedata_count_reg__0\(9),
      R => writedata_count0
    );
writedata_done_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE02"
    )
        port map (
      I0 => \FSM_sequential_writedata_FSM_next_reg_n_0_[1]\,
      I1 => writedata_FSM_next(0),
      I2 => writedata_FSM_next(2),
      I3 => writedata_done_reg_n_0,
      O => writedata_done_i_1_n_0
    );
writedata_done_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => writedata_done_i_1_n_0,
      Q => writedata_done_reg_n_0,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_hbm_channel_channel_ctrl_v5_0_0 is
  port (
    AXI_ARADDR : out STD_LOGIC_VECTOR ( 32 downto 0 );
    AXI_ARBURST : out STD_LOGIC_VECTOR ( 1 downto 0 );
    AXI_ARID : out STD_LOGIC_VECTOR ( 5 downto 0 );
    AXI_ARLEN : out STD_LOGIC_VECTOR ( 3 downto 0 );
    AXI_ARREADY : in STD_LOGIC;
    AXI_ARSIZE : out STD_LOGIC_VECTOR ( 2 downto 0 );
    AXI_ARVALID : out STD_LOGIC;
    AXI_AWADDR : out STD_LOGIC_VECTOR ( 32 downto 0 );
    AXI_AWBURST : out STD_LOGIC_VECTOR ( 1 downto 0 );
    AXI_AWID : out STD_LOGIC_VECTOR ( 5 downto 0 );
    AXI_AWLEN : out STD_LOGIC_VECTOR ( 3 downto 0 );
    AXI_AWREADY : in STD_LOGIC;
    AXI_AWSIZE : out STD_LOGIC_VECTOR ( 2 downto 0 );
    AXI_AWVALID : out STD_LOGIC;
    AXI_BID : in STD_LOGIC_VECTOR ( 5 downto 0 );
    AXI_BREADY : out STD_LOGIC;
    AXI_BRESP : in STD_LOGIC_VECTOR ( 1 downto 0 );
    AXI_BVALID : in STD_LOGIC;
    AXI_RDATA : in STD_LOGIC_VECTOR ( 255 downto 0 );
    AXI_RID : in STD_LOGIC_VECTOR ( 5 downto 0 );
    AXI_RLAST : in STD_LOGIC;
    AXI_RREADY : out STD_LOGIC;
    AXI_RRESP : in STD_LOGIC_VECTOR ( 1 downto 0 );
    AXI_RVALID : in STD_LOGIC;
    AXI_WID : out STD_LOGIC_VECTOR ( 5 downto 0 );
    AXI_WDATA : out STD_LOGIC_VECTOR ( 255 downto 0 );
    AXI_WLAST : out STD_LOGIC;
    AXI_WREADY : in STD_LOGIC;
    AXI_WSTRB : out STD_LOGIC_VECTOR ( 31 downto 0 );
    AXI_WVALID : out STD_LOGIC;
    AXI_WDATA_PARITY : out STD_LOGIC_VECTOR ( 31 downto 0 );
    FIFO_WRITE_tready : in STD_LOGIC;
    FIFO_WRITE_tdata : out STD_LOGIC_VECTOR ( 255 downto 0 );
    FIFO_WRITE_tvalid : out STD_LOGIC;
    FIFO_WRITE_tkeep : out STD_LOGIC_VECTOR ( 31 downto 0 );
    FIFO_WRITE_tlast : out STD_LOGIC;
    FIFO_READ_tvalid : in STD_LOGIC;
    FIFO_READ_tdata : in STD_LOGIC_VECTOR ( 255 downto 0 );
    FIFO_READ_tready : out STD_LOGIC;
    rd_running : in STD_LOGIC;
    rd_startaddress : in STD_LOGIC_VECTOR ( 31 downto 0 );
    rd_blockamount : in STD_LOGIC_VECTOR ( 31 downto 0 );
    wr_running : in STD_LOGIC;
    wr_startaddress : in STD_LOGIC_VECTOR ( 31 downto 0 );
    wr_blockamount : in STD_LOGIC_VECTOR ( 31 downto 0 );
    busy : out STD_LOGIC;
    clk : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_hbm_channel_channel_ctrl_v5_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_hbm_channel_channel_ctrl_v5_0_0 : entity is "design_hbm_channel_channel_ctrl_v5_0_0,channel_ctrl_v5,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of design_hbm_channel_channel_ctrl_v5_0_0 : entity is "yes";
  attribute IP_DEFINITION_SOURCE : string;
  attribute IP_DEFINITION_SOURCE of design_hbm_channel_channel_ctrl_v5_0_0 : entity is "module_ref";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of design_hbm_channel_channel_ctrl_v5_0_0 : entity is "channel_ctrl_v5,Vivado 2022.2";
end design_hbm_channel_channel_ctrl_v5_0_0;

architecture STRUCTURE of design_hbm_channel_channel_ctrl_v5_0_0 is
  signal \<const0>\ : STD_LOGIC;
  signal \<const1>\ : STD_LOGIC;
  signal \^axi_rdata\ : STD_LOGIC_VECTOR ( 255 downto 0 );
  signal \^axi_rvalid\ : STD_LOGIC;
  signal \^axi_wready\ : STD_LOGIC;
  signal \^fifo_read_tdata\ : STD_LOGIC_VECTOR ( 255 downto 0 );
  signal \^fifo_read_tvalid\ : STD_LOGIC;
  signal \^fifo_write_tready\ : STD_LOGIC;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of AXI_ARREADY : signal is "xilinx.com:interface:aximm:1.0 AXI ARREADY";
  attribute X_INTERFACE_INFO of AXI_ARVALID : signal is "xilinx.com:interface:aximm:1.0 AXI ARVALID";
  attribute X_INTERFACE_INFO of AXI_AWREADY : signal is "xilinx.com:interface:aximm:1.0 AXI AWREADY";
  attribute X_INTERFACE_INFO of AXI_AWVALID : signal is "xilinx.com:interface:aximm:1.0 AXI AWVALID";
  attribute X_INTERFACE_INFO of AXI_BREADY : signal is "xilinx.com:interface:aximm:1.0 AXI BREADY";
  attribute X_INTERFACE_INFO of AXI_BVALID : signal is "xilinx.com:interface:aximm:1.0 AXI BVALID";
  attribute X_INTERFACE_INFO of AXI_RLAST : signal is "xilinx.com:interface:aximm:1.0 AXI RLAST";
  attribute X_INTERFACE_INFO of AXI_RREADY : signal is "xilinx.com:interface:aximm:1.0 AXI RREADY";
  attribute X_INTERFACE_INFO of AXI_RVALID : signal is "xilinx.com:interface:aximm:1.0 AXI RVALID";
  attribute X_INTERFACE_INFO of AXI_WLAST : signal is "xilinx.com:interface:aximm:1.0 AXI WLAST";
  attribute X_INTERFACE_INFO of AXI_WREADY : signal is "xilinx.com:interface:aximm:1.0 AXI WREADY";
  attribute X_INTERFACE_INFO of AXI_WVALID : signal is "xilinx.com:interface:aximm:1.0 AXI WVALID";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of AXI_WVALID : signal is "XIL_INTERFACENAME AXI, DATA_WIDTH 256, PROTOCOL AXI3, FREQ_HZ 450000000, ID_WIDTH 6, ADDR_WIDTH 33, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 1, HAS_LOCK 0, HAS_PROT 0, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 16, PHASE 0.0, CLK_DOMAIN design_hbm_channel_clk_450, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of FIFO_READ_tready : signal is "xilinx.com:interface:axis:1.0 FIFO_READ TREADY";
  attribute X_INTERFACE_PARAMETER of FIFO_READ_tready : signal is "XIL_INTERFACENAME FIFO_READ, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 450000000, PHASE 0.0, CLK_DOMAIN design_hbm_channel_clk_450, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of FIFO_READ_tvalid : signal is "xilinx.com:interface:axis:1.0 FIFO_READ TVALID";
  attribute X_INTERFACE_INFO of FIFO_WRITE_tlast : signal is "xilinx.com:interface:axis:1.0 FIFO_WRITE TLAST";
  attribute X_INTERFACE_PARAMETER of FIFO_WRITE_tlast : signal is "XIL_INTERFACENAME FIFO_WRITE, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 450000000, PHASE 0.0, CLK_DOMAIN design_hbm_channel_clk_450, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of FIFO_WRITE_tready : signal is "xilinx.com:interface:axis:1.0 FIFO_WRITE TREADY";
  attribute X_INTERFACE_INFO of FIFO_WRITE_tvalid : signal is "xilinx.com:interface:axis:1.0 FIFO_WRITE TVALID";
  attribute X_INTERFACE_INFO of busy : signal is "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 user_hbm_channel_ctrl_ports busy";
  attribute X_INTERFACE_INFO of clk : signal is "xilinx.com:signal:clock:1.0 clk CLK";
  attribute X_INTERFACE_PARAMETER of clk : signal is "XIL_INTERFACENAME clk, ASSOCIATED_BUSIF FIFO_READ:FIFO_WRITE:AXI, FREQ_HZ 450000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN design_hbm_channel_clk_450, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of AXI_ARADDR : signal is "xilinx.com:interface:aximm:1.0 AXI ARADDR";
  attribute X_INTERFACE_INFO of AXI_ARBURST : signal is "xilinx.com:interface:aximm:1.0 AXI ARBURST";
  attribute X_INTERFACE_INFO of AXI_ARID : signal is "xilinx.com:interface:aximm:1.0 AXI ARID";
  attribute X_INTERFACE_INFO of AXI_ARLEN : signal is "xilinx.com:interface:aximm:1.0 AXI ARLEN";
  attribute X_INTERFACE_INFO of AXI_ARSIZE : signal is "xilinx.com:interface:aximm:1.0 AXI ARSIZE";
  attribute X_INTERFACE_INFO of AXI_AWADDR : signal is "xilinx.com:interface:aximm:1.0 AXI AWADDR";
  attribute X_INTERFACE_INFO of AXI_AWBURST : signal is "xilinx.com:interface:aximm:1.0 AXI AWBURST";
  attribute X_INTERFACE_INFO of AXI_AWID : signal is "xilinx.com:interface:aximm:1.0 AXI AWID";
  attribute X_INTERFACE_INFO of AXI_AWLEN : signal is "xilinx.com:interface:aximm:1.0 AXI AWLEN";
  attribute X_INTERFACE_INFO of AXI_AWSIZE : signal is "xilinx.com:interface:aximm:1.0 AXI AWSIZE";
  attribute X_INTERFACE_INFO of AXI_BID : signal is "xilinx.com:interface:aximm:1.0 AXI BID";
  attribute X_INTERFACE_INFO of AXI_BRESP : signal is "xilinx.com:interface:aximm:1.0 AXI BRESP";
  attribute X_INTERFACE_INFO of AXI_RDATA : signal is "xilinx.com:interface:aximm:1.0 AXI RDATA";
  attribute X_INTERFACE_INFO of AXI_RID : signal is "xilinx.com:interface:aximm:1.0 AXI RID";
  attribute X_INTERFACE_INFO of AXI_RRESP : signal is "xilinx.com:interface:aximm:1.0 AXI RRESP";
  attribute X_INTERFACE_INFO of AXI_WDATA : signal is "xilinx.com:interface:aximm:1.0 AXI WDATA";
  attribute X_INTERFACE_INFO of AXI_WID : signal is "xilinx.com:interface:aximm:1.0 AXI WID";
  attribute X_INTERFACE_INFO of AXI_WSTRB : signal is "xilinx.com:interface:aximm:1.0 AXI WSTRB";
  attribute X_INTERFACE_INFO of FIFO_READ_tdata : signal is "xilinx.com:interface:axis:1.0 FIFO_READ TDATA";
  attribute X_INTERFACE_INFO of FIFO_WRITE_tdata : signal is "xilinx.com:interface:axis:1.0 FIFO_WRITE TDATA";
  attribute X_INTERFACE_INFO of FIFO_WRITE_tkeep : signal is "xilinx.com:interface:axis:1.0 FIFO_WRITE TKEEP";
  attribute X_INTERFACE_INFO of rd_blockamount : signal is "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 user_hbm_channel_ctrl_ports rd_blockamount";
  attribute X_INTERFACE_INFO of rd_startaddress : signal is "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 user_hbm_channel_ctrl_ports rd_startaddress";
  attribute X_INTERFACE_INFO of wr_blockamount : signal is "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 user_hbm_channel_ctrl_ports wr_blockamount";
  attribute X_INTERFACE_INFO of wr_startaddress : signal is "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 user_hbm_channel_ctrl_ports wr_startaddress";
begin
  AXI_ARBURST(1) <= \<const0>\;
  AXI_ARBURST(0) <= \<const1>\;
  AXI_ARID(5) <= \<const0>\;
  AXI_ARID(4) <= \<const0>\;
  AXI_ARID(3) <= \<const0>\;
  AXI_ARID(2) <= \<const0>\;
  AXI_ARID(1) <= \<const0>\;
  AXI_ARID(0) <= \<const0>\;
  AXI_ARSIZE(2) <= \<const1>\;
  AXI_ARSIZE(1) <= \<const0>\;
  AXI_ARSIZE(0) <= \<const1>\;
  AXI_AWBURST(1) <= \<const0>\;
  AXI_AWBURST(0) <= \<const1>\;
  AXI_AWID(5) <= \<const0>\;
  AXI_AWID(4) <= \<const0>\;
  AXI_AWID(3) <= \<const0>\;
  AXI_AWID(2) <= \<const0>\;
  AXI_AWID(1) <= \<const0>\;
  AXI_AWID(0) <= \<const0>\;
  AXI_AWSIZE(2) <= \<const1>\;
  AXI_AWSIZE(1) <= \<const0>\;
  AXI_AWSIZE(0) <= \<const1>\;
  AXI_BREADY <= \<const1>\;
  AXI_RREADY <= \^fifo_write_tready\;
  AXI_WDATA(255 downto 0) <= \^fifo_read_tdata\(255 downto 0);
  AXI_WID(5) <= \<const0>\;
  AXI_WID(4) <= \<const0>\;
  AXI_WID(3) <= \<const0>\;
  AXI_WID(2) <= \<const0>\;
  AXI_WID(1) <= \<const0>\;
  AXI_WID(0) <= \<const0>\;
  AXI_WSTRB(31) <= \<const1>\;
  AXI_WSTRB(30) <= \<const1>\;
  AXI_WSTRB(29) <= \<const1>\;
  AXI_WSTRB(28) <= \<const1>\;
  AXI_WSTRB(27) <= \<const1>\;
  AXI_WSTRB(26) <= \<const1>\;
  AXI_WSTRB(25) <= \<const1>\;
  AXI_WSTRB(24) <= \<const1>\;
  AXI_WSTRB(23) <= \<const1>\;
  AXI_WSTRB(22) <= \<const1>\;
  AXI_WSTRB(21) <= \<const1>\;
  AXI_WSTRB(20) <= \<const1>\;
  AXI_WSTRB(19) <= \<const1>\;
  AXI_WSTRB(18) <= \<const1>\;
  AXI_WSTRB(17) <= \<const1>\;
  AXI_WSTRB(16) <= \<const1>\;
  AXI_WSTRB(15) <= \<const1>\;
  AXI_WSTRB(14) <= \<const1>\;
  AXI_WSTRB(13) <= \<const1>\;
  AXI_WSTRB(12) <= \<const1>\;
  AXI_WSTRB(11) <= \<const1>\;
  AXI_WSTRB(10) <= \<const1>\;
  AXI_WSTRB(9) <= \<const1>\;
  AXI_WSTRB(8) <= \<const1>\;
  AXI_WSTRB(7) <= \<const1>\;
  AXI_WSTRB(6) <= \<const1>\;
  AXI_WSTRB(5) <= \<const1>\;
  AXI_WSTRB(4) <= \<const1>\;
  AXI_WSTRB(3) <= \<const1>\;
  AXI_WSTRB(2) <= \<const1>\;
  AXI_WSTRB(1) <= \<const1>\;
  AXI_WSTRB(0) <= \<const1>\;
  AXI_WVALID <= \^fifo_read_tvalid\;
  FIFO_READ_tready <= \^axi_wready\;
  FIFO_WRITE_tdata(255 downto 0) <= \^axi_rdata\(255 downto 0);
  FIFO_WRITE_tkeep(31) <= \<const1>\;
  FIFO_WRITE_tkeep(30) <= \<const1>\;
  FIFO_WRITE_tkeep(29) <= \<const1>\;
  FIFO_WRITE_tkeep(28) <= \<const1>\;
  FIFO_WRITE_tkeep(27) <= \<const1>\;
  FIFO_WRITE_tkeep(26) <= \<const1>\;
  FIFO_WRITE_tkeep(25) <= \<const1>\;
  FIFO_WRITE_tkeep(24) <= \<const1>\;
  FIFO_WRITE_tkeep(23) <= \<const1>\;
  FIFO_WRITE_tkeep(22) <= \<const1>\;
  FIFO_WRITE_tkeep(21) <= \<const1>\;
  FIFO_WRITE_tkeep(20) <= \<const1>\;
  FIFO_WRITE_tkeep(19) <= \<const1>\;
  FIFO_WRITE_tkeep(18) <= \<const1>\;
  FIFO_WRITE_tkeep(17) <= \<const1>\;
  FIFO_WRITE_tkeep(16) <= \<const1>\;
  FIFO_WRITE_tkeep(15) <= \<const1>\;
  FIFO_WRITE_tkeep(14) <= \<const1>\;
  FIFO_WRITE_tkeep(13) <= \<const1>\;
  FIFO_WRITE_tkeep(12) <= \<const1>\;
  FIFO_WRITE_tkeep(11) <= \<const1>\;
  FIFO_WRITE_tkeep(10) <= \<const1>\;
  FIFO_WRITE_tkeep(9) <= \<const1>\;
  FIFO_WRITE_tkeep(8) <= \<const1>\;
  FIFO_WRITE_tkeep(7) <= \<const1>\;
  FIFO_WRITE_tkeep(6) <= \<const1>\;
  FIFO_WRITE_tkeep(5) <= \<const1>\;
  FIFO_WRITE_tkeep(4) <= \<const1>\;
  FIFO_WRITE_tkeep(3) <= \<const1>\;
  FIFO_WRITE_tkeep(2) <= \<const1>\;
  FIFO_WRITE_tkeep(1) <= \<const1>\;
  FIFO_WRITE_tkeep(0) <= \<const1>\;
  FIFO_WRITE_tvalid <= \^axi_rvalid\;
  \^axi_rdata\(255 downto 0) <= AXI_RDATA(255 downto 0);
  \^axi_rvalid\ <= AXI_RVALID;
  \^axi_wready\ <= AXI_WREADY;
  \^fifo_read_tdata\(255 downto 0) <= FIFO_READ_tdata(255 downto 0);
  \^fifo_read_tvalid\ <= FIFO_READ_tvalid;
  \^fifo_write_tready\ <= FIFO_WRITE_tready;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
VCC: unisim.vcomponents.VCC
     port map (
      P => \<const1>\
    );
inst: entity work.design_hbm_channel_channel_ctrl_v5_0_0_channel_ctrl_v5
     port map (
      AXI_ARADDR(32 downto 0) => AXI_ARADDR(32 downto 0),
      AXI_ARLEN(3 downto 0) => AXI_ARLEN(3 downto 0),
      AXI_ARREADY => AXI_ARREADY,
      AXI_ARVALID_reg_0 => AXI_ARVALID,
      AXI_AWADDR(32 downto 0) => AXI_AWADDR(32 downto 0),
      AXI_AWLEN(3 downto 0) => AXI_AWLEN(3 downto 0),
      AXI_AWREADY => AXI_AWREADY,
      AXI_AWVALID_reg_0 => AXI_AWVALID,
      AXI_RVALID => \^axi_rvalid\,
      AXI_WDATA_PARITY(31 downto 0) => AXI_WDATA_PARITY(31 downto 0),
      AXI_WLAST => AXI_WLAST,
      AXI_WREADY => \^axi_wready\,
      FIFO_READ_tdata(255 downto 0) => \^fifo_read_tdata\(255 downto 0),
      FIFO_READ_tvalid => \^fifo_read_tvalid\,
      FIFO_WRITE_tlast => FIFO_WRITE_tlast,
      FIFO_WRITE_tready => \^fifo_write_tready\,
      busy => busy,
      clk => clk,
      rd_blockamount(31 downto 0) => rd_blockamount(31 downto 0),
      rd_running => rd_running,
      rd_startaddress(31 downto 0) => rd_startaddress(31 downto 0),
      wr_blockamount(31 downto 0) => wr_blockamount(31 downto 0),
      wr_running => wr_running,
      wr_startaddress(31 downto 0) => wr_startaddress(31 downto 0)
    );
end STRUCTURE;
