// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
// Date        : Tue Dec  6 16:15:51 2022
// Host        : dell-Vostro-3910-China-HDD-Protection running 64-bit Ubuntu 22.04.1 LTS
// Command     : write_verilog -force -mode funcsim
//               /media/dell/workspace/Xilinx_projects/ringallreduced/ringallreduced.gen/sources_1/bd/design_hbm_channel/ip/design_hbm_channel_channel_ctrl_v5_0_0/design_hbm_channel_channel_ctrl_v5_0_0_sim_netlist.v
// Design      : design_hbm_channel_channel_ctrl_v5_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcvu37p-fsvh2892-2L-e
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_hbm_channel_channel_ctrl_v5_0_0,channel_ctrl_v5,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* IP_DEFINITION_SOURCE = "module_ref" *) 
(* X_CORE_INFO = "channel_ctrl_v5,Vivado 2022.2" *) 
(* NotValidForBitStream *)
module design_hbm_channel_channel_ctrl_v5_0_0
   (AXI_ARADDR,
    AXI_ARBURST,
    AXI_ARID,
    AXI_ARLEN,
    AXI_ARREADY,
    AXI_ARSIZE,
    AXI_ARVALID,
    AXI_AWADDR,
    AXI_AWBURST,
    AXI_AWID,
    AXI_AWLEN,
    AXI_AWREADY,
    AXI_AWSIZE,
    AXI_AWVALID,
    AXI_BID,
    AXI_BREADY,
    AXI_BRESP,
    AXI_BVALID,
    AXI_RDATA,
    AXI_RID,
    AXI_RLAST,
    AXI_RREADY,
    AXI_RRESP,
    AXI_RVALID,
    AXI_WID,
    AXI_WDATA,
    AXI_WLAST,
    AXI_WREADY,
    AXI_WSTRB,
    AXI_WVALID,
    AXI_WDATA_PARITY,
    FIFO_WRITE_tready,
    FIFO_WRITE_tdata,
    FIFO_WRITE_tvalid,
    FIFO_WRITE_tkeep,
    FIFO_WRITE_tlast,
    FIFO_READ_tvalid,
    FIFO_READ_tdata,
    FIFO_READ_tready,
    rd_running,
    rd_startaddress,
    rd_blockamount,
    wr_running,
    wr_startaddress,
    wr_blockamount,
    busy,
    clk);
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI ARADDR" *) output [32:0]AXI_ARADDR;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI ARBURST" *) output [1:0]AXI_ARBURST;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI ARID" *) output [5:0]AXI_ARID;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI ARLEN" *) output [3:0]AXI_ARLEN;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI ARREADY" *) input AXI_ARREADY;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI ARSIZE" *) output [2:0]AXI_ARSIZE;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI ARVALID" *) output AXI_ARVALID;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI AWADDR" *) output [32:0]AXI_AWADDR;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI AWBURST" *) output [1:0]AXI_AWBURST;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI AWID" *) output [5:0]AXI_AWID;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI AWLEN" *) output [3:0]AXI_AWLEN;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI AWREADY" *) input AXI_AWREADY;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI AWSIZE" *) output [2:0]AXI_AWSIZE;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI AWVALID" *) output AXI_AWVALID;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI BID" *) input [5:0]AXI_BID;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI BREADY" *) output AXI_BREADY;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI BRESP" *) input [1:0]AXI_BRESP;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI BVALID" *) input AXI_BVALID;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI RDATA" *) input [255:0]AXI_RDATA;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI RID" *) input [5:0]AXI_RID;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI RLAST" *) input AXI_RLAST;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI RREADY" *) output AXI_RREADY;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI RRESP" *) input [1:0]AXI_RRESP;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI RVALID" *) input AXI_RVALID;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI WID" *) output [5:0]AXI_WID;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI WDATA" *) output [255:0]AXI_WDATA;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI WLAST" *) output AXI_WLAST;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI WREADY" *) input AXI_WREADY;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI WSTRB" *) output [31:0]AXI_WSTRB;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI WVALID" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME AXI, DATA_WIDTH 256, PROTOCOL AXI3, FREQ_HZ 450000000, ID_WIDTH 6, ADDR_WIDTH 33, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 1, HAS_LOCK 0, HAS_PROT 0, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 16, PHASE 0.0, CLK_DOMAIN design_hbm_channel_clk_450, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) output AXI_WVALID;
  output [31:0]AXI_WDATA_PARITY;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 FIFO_WRITE TREADY" *) input FIFO_WRITE_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 FIFO_WRITE TDATA" *) output [255:0]FIFO_WRITE_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 FIFO_WRITE TVALID" *) output FIFO_WRITE_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 FIFO_WRITE TKEEP" *) output [31:0]FIFO_WRITE_tkeep;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 FIFO_WRITE TLAST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME FIFO_WRITE, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 450000000, PHASE 0.0, CLK_DOMAIN design_hbm_channel_clk_450, LAYERED_METADATA undef, INSERT_VIP 0" *) output FIFO_WRITE_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 FIFO_READ TVALID" *) input FIFO_READ_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 FIFO_READ TDATA" *) input [255:0]FIFO_READ_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 FIFO_READ TREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME FIFO_READ, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 450000000, PHASE 0.0, CLK_DOMAIN design_hbm_channel_clk_450, LAYERED_METADATA undef, INSERT_VIP 0" *) output FIFO_READ_tready;
  input rd_running;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 user_hbm_channel_ctrl_ports rd_startaddress" *) input [31:0]rd_startaddress;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 user_hbm_channel_ctrl_ports rd_blockamount" *) input [31:0]rd_blockamount;
  input wr_running;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 user_hbm_channel_ctrl_ports wr_startaddress" *) input [31:0]wr_startaddress;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 user_hbm_channel_ctrl_ports wr_blockamount" *) input [31:0]wr_blockamount;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 user_hbm_channel_ctrl_ports busy" *) output busy;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 clk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME clk, ASSOCIATED_BUSIF FIFO_READ:FIFO_WRITE:AXI, FREQ_HZ 450000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN design_hbm_channel_clk_450, INSERT_VIP 0" *) input clk;

  wire \<const0> ;
  wire \<const1> ;
  wire [32:0]AXI_ARADDR;
  wire [3:0]AXI_ARLEN;
  wire AXI_ARREADY;
  wire AXI_ARVALID;
  wire [32:0]AXI_AWADDR;
  wire [3:0]AXI_AWLEN;
  wire AXI_AWREADY;
  wire AXI_AWVALID;
  wire [255:0]AXI_RDATA;
  wire AXI_RVALID;
  wire [31:0]AXI_WDATA_PARITY;
  wire AXI_WLAST;
  wire AXI_WREADY;
  wire [255:0]FIFO_READ_tdata;
  wire FIFO_READ_tvalid;
  wire FIFO_WRITE_tlast;
  wire FIFO_WRITE_tready;
  wire busy;
  wire clk;
  wire [31:0]rd_blockamount;
  wire rd_running;
  wire [31:0]rd_startaddress;
  wire [31:0]wr_blockamount;
  wire wr_running;
  wire [31:0]wr_startaddress;

  assign AXI_ARBURST[1] = \<const0> ;
  assign AXI_ARBURST[0] = \<const1> ;
  assign AXI_ARID[5] = \<const0> ;
  assign AXI_ARID[4] = \<const0> ;
  assign AXI_ARID[3] = \<const0> ;
  assign AXI_ARID[2] = \<const0> ;
  assign AXI_ARID[1] = \<const0> ;
  assign AXI_ARID[0] = \<const0> ;
  assign AXI_ARSIZE[2] = \<const1> ;
  assign AXI_ARSIZE[1] = \<const0> ;
  assign AXI_ARSIZE[0] = \<const1> ;
  assign AXI_AWBURST[1] = \<const0> ;
  assign AXI_AWBURST[0] = \<const1> ;
  assign AXI_AWID[5] = \<const0> ;
  assign AXI_AWID[4] = \<const0> ;
  assign AXI_AWID[3] = \<const0> ;
  assign AXI_AWID[2] = \<const0> ;
  assign AXI_AWID[1] = \<const0> ;
  assign AXI_AWID[0] = \<const0> ;
  assign AXI_AWSIZE[2] = \<const1> ;
  assign AXI_AWSIZE[1] = \<const0> ;
  assign AXI_AWSIZE[0] = \<const1> ;
  assign AXI_BREADY = \<const1> ;
  assign AXI_RREADY = FIFO_WRITE_tready;
  assign AXI_WDATA[255:0] = FIFO_READ_tdata;
  assign AXI_WID[5] = \<const0> ;
  assign AXI_WID[4] = \<const0> ;
  assign AXI_WID[3] = \<const0> ;
  assign AXI_WID[2] = \<const0> ;
  assign AXI_WID[1] = \<const0> ;
  assign AXI_WID[0] = \<const0> ;
  assign AXI_WSTRB[31] = \<const1> ;
  assign AXI_WSTRB[30] = \<const1> ;
  assign AXI_WSTRB[29] = \<const1> ;
  assign AXI_WSTRB[28] = \<const1> ;
  assign AXI_WSTRB[27] = \<const1> ;
  assign AXI_WSTRB[26] = \<const1> ;
  assign AXI_WSTRB[25] = \<const1> ;
  assign AXI_WSTRB[24] = \<const1> ;
  assign AXI_WSTRB[23] = \<const1> ;
  assign AXI_WSTRB[22] = \<const1> ;
  assign AXI_WSTRB[21] = \<const1> ;
  assign AXI_WSTRB[20] = \<const1> ;
  assign AXI_WSTRB[19] = \<const1> ;
  assign AXI_WSTRB[18] = \<const1> ;
  assign AXI_WSTRB[17] = \<const1> ;
  assign AXI_WSTRB[16] = \<const1> ;
  assign AXI_WSTRB[15] = \<const1> ;
  assign AXI_WSTRB[14] = \<const1> ;
  assign AXI_WSTRB[13] = \<const1> ;
  assign AXI_WSTRB[12] = \<const1> ;
  assign AXI_WSTRB[11] = \<const1> ;
  assign AXI_WSTRB[10] = \<const1> ;
  assign AXI_WSTRB[9] = \<const1> ;
  assign AXI_WSTRB[8] = \<const1> ;
  assign AXI_WSTRB[7] = \<const1> ;
  assign AXI_WSTRB[6] = \<const1> ;
  assign AXI_WSTRB[5] = \<const1> ;
  assign AXI_WSTRB[4] = \<const1> ;
  assign AXI_WSTRB[3] = \<const1> ;
  assign AXI_WSTRB[2] = \<const1> ;
  assign AXI_WSTRB[1] = \<const1> ;
  assign AXI_WSTRB[0] = \<const1> ;
  assign AXI_WVALID = FIFO_READ_tvalid;
  assign FIFO_READ_tready = AXI_WREADY;
  assign FIFO_WRITE_tdata[255:0] = AXI_RDATA;
  assign FIFO_WRITE_tkeep[31] = \<const1> ;
  assign FIFO_WRITE_tkeep[30] = \<const1> ;
  assign FIFO_WRITE_tkeep[29] = \<const1> ;
  assign FIFO_WRITE_tkeep[28] = \<const1> ;
  assign FIFO_WRITE_tkeep[27] = \<const1> ;
  assign FIFO_WRITE_tkeep[26] = \<const1> ;
  assign FIFO_WRITE_tkeep[25] = \<const1> ;
  assign FIFO_WRITE_tkeep[24] = \<const1> ;
  assign FIFO_WRITE_tkeep[23] = \<const1> ;
  assign FIFO_WRITE_tkeep[22] = \<const1> ;
  assign FIFO_WRITE_tkeep[21] = \<const1> ;
  assign FIFO_WRITE_tkeep[20] = \<const1> ;
  assign FIFO_WRITE_tkeep[19] = \<const1> ;
  assign FIFO_WRITE_tkeep[18] = \<const1> ;
  assign FIFO_WRITE_tkeep[17] = \<const1> ;
  assign FIFO_WRITE_tkeep[16] = \<const1> ;
  assign FIFO_WRITE_tkeep[15] = \<const1> ;
  assign FIFO_WRITE_tkeep[14] = \<const1> ;
  assign FIFO_WRITE_tkeep[13] = \<const1> ;
  assign FIFO_WRITE_tkeep[12] = \<const1> ;
  assign FIFO_WRITE_tkeep[11] = \<const1> ;
  assign FIFO_WRITE_tkeep[10] = \<const1> ;
  assign FIFO_WRITE_tkeep[9] = \<const1> ;
  assign FIFO_WRITE_tkeep[8] = \<const1> ;
  assign FIFO_WRITE_tkeep[7] = \<const1> ;
  assign FIFO_WRITE_tkeep[6] = \<const1> ;
  assign FIFO_WRITE_tkeep[5] = \<const1> ;
  assign FIFO_WRITE_tkeep[4] = \<const1> ;
  assign FIFO_WRITE_tkeep[3] = \<const1> ;
  assign FIFO_WRITE_tkeep[2] = \<const1> ;
  assign FIFO_WRITE_tkeep[1] = \<const1> ;
  assign FIFO_WRITE_tkeep[0] = \<const1> ;
  assign FIFO_WRITE_tvalid = AXI_RVALID;
  GND GND
       (.G(\<const0> ));
  VCC VCC
       (.P(\<const1> ));
  design_hbm_channel_channel_ctrl_v5_0_0_channel_ctrl_v5 inst
       (.AXI_ARADDR(AXI_ARADDR),
        .AXI_ARLEN(AXI_ARLEN),
        .AXI_ARREADY(AXI_ARREADY),
        .AXI_ARVALID_reg_0(AXI_ARVALID),
        .AXI_AWADDR(AXI_AWADDR),
        .AXI_AWLEN(AXI_AWLEN),
        .AXI_AWREADY(AXI_AWREADY),
        .AXI_AWVALID_reg_0(AXI_AWVALID),
        .AXI_RVALID(AXI_RVALID),
        .AXI_WDATA_PARITY(AXI_WDATA_PARITY),
        .AXI_WLAST(AXI_WLAST),
        .AXI_WREADY(AXI_WREADY),
        .FIFO_READ_tdata(FIFO_READ_tdata),
        .FIFO_READ_tvalid(FIFO_READ_tvalid),
        .FIFO_WRITE_tlast(FIFO_WRITE_tlast),
        .FIFO_WRITE_tready(FIFO_WRITE_tready),
        .busy(busy),
        .clk(clk),
        .rd_blockamount(rd_blockamount),
        .rd_running(rd_running),
        .rd_startaddress(rd_startaddress),
        .wr_blockamount(wr_blockamount),
        .wr_running(wr_running),
        .wr_startaddress(wr_startaddress));
endmodule

(* ORIG_REF_NAME = "channel_ctrl_v5" *) 
module design_hbm_channel_channel_ctrl_v5_0_0_channel_ctrl_v5
   (AXI_ARADDR,
    AXI_ARLEN,
    AXI_AWADDR,
    AXI_AWLEN,
    AXI_WDATA_PARITY,
    FIFO_WRITE_tlast,
    AXI_AWVALID_reg_0,
    AXI_ARVALID_reg_0,
    AXI_WLAST,
    busy,
    clk,
    wr_blockamount,
    rd_blockamount,
    rd_startaddress,
    wr_startaddress,
    FIFO_READ_tdata,
    AXI_RVALID,
    FIFO_WRITE_tready,
    AXI_AWREADY,
    FIFO_READ_tvalid,
    AXI_WREADY,
    AXI_ARREADY,
    rd_running,
    wr_running);
  output [32:0]AXI_ARADDR;
  output [3:0]AXI_ARLEN;
  output [32:0]AXI_AWADDR;
  output [3:0]AXI_AWLEN;
  output [31:0]AXI_WDATA_PARITY;
  output FIFO_WRITE_tlast;
  output AXI_AWVALID_reg_0;
  output AXI_ARVALID_reg_0;
  output AXI_WLAST;
  output busy;
  input clk;
  input [31:0]wr_blockamount;
  input [31:0]rd_blockamount;
  input [31:0]rd_startaddress;
  input [31:0]wr_startaddress;
  input [255:0]FIFO_READ_tdata;
  input AXI_RVALID;
  input FIFO_WRITE_tready;
  input AXI_AWREADY;
  input FIFO_READ_tvalid;
  input AXI_WREADY;
  input AXI_ARREADY;
  input rd_running;
  input wr_running;

  wire [32:0]AXI_ARADDR;
  wire AXI_ARADDR0;
  wire [32:4]AXI_ARADDR00_in;
  wire \AXI_ARADDR[11]_i_2_n_0 ;
  wire \AXI_ARADDR[11]_i_3_n_0 ;
  wire \AXI_ARADDR[11]_i_4_n_0 ;
  wire \AXI_ARADDR[11]_i_5_n_0 ;
  wire \AXI_ARADDR[11]_i_6_n_0 ;
  wire \AXI_ARADDR[11]_i_7_n_0 ;
  wire \AXI_ARADDR[11]_i_8_n_0 ;
  wire \AXI_ARADDR[11]_i_9_n_0 ;
  wire \AXI_ARADDR[19]_i_2_n_0 ;
  wire \AXI_ARADDR[19]_i_3_n_0 ;
  wire \AXI_ARADDR[19]_i_4_n_0 ;
  wire \AXI_ARADDR[19]_i_5_n_0 ;
  wire \AXI_ARADDR[19]_i_6_n_0 ;
  wire \AXI_ARADDR[19]_i_7_n_0 ;
  wire \AXI_ARADDR[19]_i_8_n_0 ;
  wire \AXI_ARADDR[19]_i_9_n_0 ;
  wire \AXI_ARADDR[27]_i_2_n_0 ;
  wire \AXI_ARADDR[27]_i_3_n_0 ;
  wire \AXI_ARADDR[27]_i_4_n_0 ;
  wire \AXI_ARADDR[27]_i_5_n_0 ;
  wire \AXI_ARADDR[27]_i_6_n_0 ;
  wire \AXI_ARADDR[27]_i_7_n_0 ;
  wire \AXI_ARADDR[27]_i_8_n_0 ;
  wire \AXI_ARADDR[27]_i_9_n_0 ;
  wire \AXI_ARADDR[32]_i_2_n_0 ;
  wire \AXI_ARADDR[32]_i_4_n_0 ;
  wire \AXI_ARADDR[32]_i_5_n_0 ;
  wire \AXI_ARADDR[32]_i_6_n_0 ;
  wire \AXI_ARADDR[32]_i_7_n_0 ;
  wire \AXI_ARADDR_reg[11]_i_1_n_0 ;
  wire \AXI_ARADDR_reg[11]_i_1_n_1 ;
  wire \AXI_ARADDR_reg[11]_i_1_n_2 ;
  wire \AXI_ARADDR_reg[11]_i_1_n_3 ;
  wire \AXI_ARADDR_reg[11]_i_1_n_4 ;
  wire \AXI_ARADDR_reg[11]_i_1_n_5 ;
  wire \AXI_ARADDR_reg[11]_i_1_n_6 ;
  wire \AXI_ARADDR_reg[11]_i_1_n_7 ;
  wire \AXI_ARADDR_reg[19]_i_1_n_0 ;
  wire \AXI_ARADDR_reg[19]_i_1_n_1 ;
  wire \AXI_ARADDR_reg[19]_i_1_n_2 ;
  wire \AXI_ARADDR_reg[19]_i_1_n_3 ;
  wire \AXI_ARADDR_reg[19]_i_1_n_4 ;
  wire \AXI_ARADDR_reg[19]_i_1_n_5 ;
  wire \AXI_ARADDR_reg[19]_i_1_n_6 ;
  wire \AXI_ARADDR_reg[19]_i_1_n_7 ;
  wire \AXI_ARADDR_reg[27]_i_1_n_0 ;
  wire \AXI_ARADDR_reg[27]_i_1_n_1 ;
  wire \AXI_ARADDR_reg[27]_i_1_n_2 ;
  wire \AXI_ARADDR_reg[27]_i_1_n_3 ;
  wire \AXI_ARADDR_reg[27]_i_1_n_4 ;
  wire \AXI_ARADDR_reg[27]_i_1_n_5 ;
  wire \AXI_ARADDR_reg[27]_i_1_n_6 ;
  wire \AXI_ARADDR_reg[27]_i_1_n_7 ;
  wire \AXI_ARADDR_reg[32]_i_3_n_5 ;
  wire \AXI_ARADDR_reg[32]_i_3_n_6 ;
  wire \AXI_ARADDR_reg[32]_i_3_n_7 ;
  wire [3:0]AXI_ARLEN;
  wire \AXI_ARLEN[0]_i_1_n_0 ;
  wire \AXI_ARLEN[1]_i_1_n_0 ;
  wire \AXI_ARLEN[2]_i_1_n_0 ;
  wire \AXI_ARLEN[3]_i_1_n_0 ;
  wire \AXI_ARLEN[3]_i_2_n_0 ;
  wire AXI_ARREADY;
  wire AXI_ARVALID_i_1_n_0;
  wire AXI_ARVALID_reg_0;
  wire [32:0]AXI_AWADDR;
  wire [32:4]AXI_AWADDR0;
  wire \AXI_AWADDR[11]_i_2_n_0 ;
  wire \AXI_AWADDR[11]_i_3_n_0 ;
  wire \AXI_AWADDR[11]_i_4_n_0 ;
  wire \AXI_AWADDR[11]_i_5_n_0 ;
  wire \AXI_AWADDR[11]_i_6_n_0 ;
  wire \AXI_AWADDR[11]_i_7_n_0 ;
  wire \AXI_AWADDR[11]_i_8_n_0 ;
  wire \AXI_AWADDR[11]_i_9_n_0 ;
  wire \AXI_AWADDR[19]_i_2_n_0 ;
  wire \AXI_AWADDR[19]_i_3_n_0 ;
  wire \AXI_AWADDR[19]_i_4_n_0 ;
  wire \AXI_AWADDR[19]_i_5_n_0 ;
  wire \AXI_AWADDR[19]_i_6_n_0 ;
  wire \AXI_AWADDR[19]_i_7_n_0 ;
  wire \AXI_AWADDR[19]_i_8_n_0 ;
  wire \AXI_AWADDR[19]_i_9_n_0 ;
  wire \AXI_AWADDR[27]_i_2_n_0 ;
  wire \AXI_AWADDR[27]_i_3_n_0 ;
  wire \AXI_AWADDR[27]_i_4_n_0 ;
  wire \AXI_AWADDR[27]_i_5_n_0 ;
  wire \AXI_AWADDR[27]_i_6_n_0 ;
  wire \AXI_AWADDR[27]_i_7_n_0 ;
  wire \AXI_AWADDR[27]_i_8_n_0 ;
  wire \AXI_AWADDR[27]_i_9_n_0 ;
  wire \AXI_AWADDR[32]_i_2_n_0 ;
  wire \AXI_AWADDR[32]_i_4_n_0 ;
  wire \AXI_AWADDR[32]_i_5_n_0 ;
  wire \AXI_AWADDR[32]_i_6_n_0 ;
  wire \AXI_AWADDR[32]_i_7_n_0 ;
  wire \AXI_AWADDR_reg[11]_i_1_n_0 ;
  wire \AXI_AWADDR_reg[11]_i_1_n_1 ;
  wire \AXI_AWADDR_reg[11]_i_1_n_2 ;
  wire \AXI_AWADDR_reg[11]_i_1_n_3 ;
  wire \AXI_AWADDR_reg[11]_i_1_n_4 ;
  wire \AXI_AWADDR_reg[11]_i_1_n_5 ;
  wire \AXI_AWADDR_reg[11]_i_1_n_6 ;
  wire \AXI_AWADDR_reg[11]_i_1_n_7 ;
  wire \AXI_AWADDR_reg[19]_i_1_n_0 ;
  wire \AXI_AWADDR_reg[19]_i_1_n_1 ;
  wire \AXI_AWADDR_reg[19]_i_1_n_2 ;
  wire \AXI_AWADDR_reg[19]_i_1_n_3 ;
  wire \AXI_AWADDR_reg[19]_i_1_n_4 ;
  wire \AXI_AWADDR_reg[19]_i_1_n_5 ;
  wire \AXI_AWADDR_reg[19]_i_1_n_6 ;
  wire \AXI_AWADDR_reg[19]_i_1_n_7 ;
  wire \AXI_AWADDR_reg[27]_i_1_n_0 ;
  wire \AXI_AWADDR_reg[27]_i_1_n_1 ;
  wire \AXI_AWADDR_reg[27]_i_1_n_2 ;
  wire \AXI_AWADDR_reg[27]_i_1_n_3 ;
  wire \AXI_AWADDR_reg[27]_i_1_n_4 ;
  wire \AXI_AWADDR_reg[27]_i_1_n_5 ;
  wire \AXI_AWADDR_reg[27]_i_1_n_6 ;
  wire \AXI_AWADDR_reg[27]_i_1_n_7 ;
  wire \AXI_AWADDR_reg[32]_i_3_n_5 ;
  wire \AXI_AWADDR_reg[32]_i_3_n_6 ;
  wire \AXI_AWADDR_reg[32]_i_3_n_7 ;
  wire [3:0]AXI_AWLEN;
  wire \AXI_AWLEN[0]_i_1_n_0 ;
  wire \AXI_AWLEN[1]_i_1_n_0 ;
  wire \AXI_AWLEN[2]_i_1_n_0 ;
  wire \AXI_AWLEN[3]_i_1_n_0 ;
  wire \AXI_AWLEN[3]_i_2_n_0 ;
  wire AXI_AWREADY;
  wire AXI_AWVALID_i_1_n_0;
  wire AXI_AWVALID_reg_0;
  wire AXI_RVALID;
  wire [31:0]AXI_WDATA_PARITY;
  wire \AXI_WDATA_PARITY0_inferred__0/AXI_WDATA_PARITY[1]_INST_0_i_1_n_0 ;
  wire \AXI_WDATA_PARITY0_inferred__1/AXI_WDATA_PARITY[2]_INST_0_i_1_n_0 ;
  wire \AXI_WDATA_PARITY0_inferred__10/AXI_WDATA_PARITY[11]_INST_0_i_1_n_0 ;
  wire \AXI_WDATA_PARITY0_inferred__11/AXI_WDATA_PARITY[12]_INST_0_i_1_n_0 ;
  wire \AXI_WDATA_PARITY0_inferred__12/AXI_WDATA_PARITY[13]_INST_0_i_1_n_0 ;
  wire \AXI_WDATA_PARITY0_inferred__13/AXI_WDATA_PARITY[14]_INST_0_i_1_n_0 ;
  wire \AXI_WDATA_PARITY0_inferred__14/AXI_WDATA_PARITY[15]_INST_0_i_1_n_0 ;
  wire \AXI_WDATA_PARITY0_inferred__15/AXI_WDATA_PARITY[16]_INST_0_i_1_n_0 ;
  wire \AXI_WDATA_PARITY0_inferred__16/AXI_WDATA_PARITY[17]_INST_0_i_1_n_0 ;
  wire \AXI_WDATA_PARITY0_inferred__17/AXI_WDATA_PARITY[18]_INST_0_i_1_n_0 ;
  wire \AXI_WDATA_PARITY0_inferred__18/AXI_WDATA_PARITY[19]_INST_0_i_1_n_0 ;
  wire \AXI_WDATA_PARITY0_inferred__19/AXI_WDATA_PARITY[20]_INST_0_i_1_n_0 ;
  wire \AXI_WDATA_PARITY0_inferred__2/AXI_WDATA_PARITY[3]_INST_0_i_1_n_0 ;
  wire \AXI_WDATA_PARITY0_inferred__20/AXI_WDATA_PARITY[21]_INST_0_i_1_n_0 ;
  wire \AXI_WDATA_PARITY0_inferred__21/AXI_WDATA_PARITY[22]_INST_0_i_1_n_0 ;
  wire \AXI_WDATA_PARITY0_inferred__22/AXI_WDATA_PARITY[23]_INST_0_i_1_n_0 ;
  wire \AXI_WDATA_PARITY0_inferred__23/AXI_WDATA_PARITY[24]_INST_0_i_1_n_0 ;
  wire \AXI_WDATA_PARITY0_inferred__24/AXI_WDATA_PARITY[25]_INST_0_i_1_n_0 ;
  wire \AXI_WDATA_PARITY0_inferred__25/AXI_WDATA_PARITY[26]_INST_0_i_1_n_0 ;
  wire \AXI_WDATA_PARITY0_inferred__26/AXI_WDATA_PARITY[27]_INST_0_i_1_n_0 ;
  wire \AXI_WDATA_PARITY0_inferred__27/AXI_WDATA_PARITY[28]_INST_0_i_1_n_0 ;
  wire \AXI_WDATA_PARITY0_inferred__28/AXI_WDATA_PARITY[29]_INST_0_i_1_n_0 ;
  wire \AXI_WDATA_PARITY0_inferred__29/AXI_WDATA_PARITY[30]_INST_0_i_1_n_0 ;
  wire \AXI_WDATA_PARITY0_inferred__3/AXI_WDATA_PARITY[4]_INST_0_i_1_n_0 ;
  wire \AXI_WDATA_PARITY0_inferred__30/AXI_WDATA_PARITY[31]_INST_0_i_1_n_0 ;
  wire \AXI_WDATA_PARITY0_inferred__4/AXI_WDATA_PARITY[5]_INST_0_i_1_n_0 ;
  wire \AXI_WDATA_PARITY0_inferred__5/AXI_WDATA_PARITY[6]_INST_0_i_1_n_0 ;
  wire \AXI_WDATA_PARITY0_inferred__6/AXI_WDATA_PARITY[7]_INST_0_i_1_n_0 ;
  wire \AXI_WDATA_PARITY0_inferred__7/AXI_WDATA_PARITY[8]_INST_0_i_1_n_0 ;
  wire \AXI_WDATA_PARITY0_inferred__8/AXI_WDATA_PARITY[9]_INST_0_i_1_n_0 ;
  wire \AXI_WDATA_PARITY0_inferred__9/AXI_WDATA_PARITY[10]_INST_0_i_1_n_0 ;
  wire \AXI_WDATA_PARITY[0]_INST_0_i_1_n_0 ;
  wire AXI_WLAST;
  wire AXI_WREADY;
  wire [255:0]FIFO_READ_tdata;
  wire FIFO_READ_tvalid;
  wire FIFO_WRITE_tlast;
  wire FIFO_WRITE_tlast0;
  wire FIFO_WRITE_tlast0_carry__0_i_10_n_0;
  wire FIFO_WRITE_tlast0_carry__0_i_11_n_0;
  wire FIFO_WRITE_tlast0_carry__0_i_1_n_0;
  wire FIFO_WRITE_tlast0_carry__0_i_2_n_0;
  wire FIFO_WRITE_tlast0_carry__0_i_3_n_0;
  wire FIFO_WRITE_tlast0_carry__0_i_4_n_2;
  wire FIFO_WRITE_tlast0_carry__0_i_4_n_3;
  wire FIFO_WRITE_tlast0_carry__0_i_4_n_4;
  wire FIFO_WRITE_tlast0_carry__0_i_4_n_5;
  wire FIFO_WRITE_tlast0_carry__0_i_4_n_6;
  wire FIFO_WRITE_tlast0_carry__0_i_4_n_7;
  wire FIFO_WRITE_tlast0_carry__0_i_5_n_0;
  wire FIFO_WRITE_tlast0_carry__0_i_6_n_0;
  wire FIFO_WRITE_tlast0_carry__0_i_7_n_0;
  wire FIFO_WRITE_tlast0_carry__0_i_8_n_0;
  wire FIFO_WRITE_tlast0_carry__0_i_9_n_0;
  wire FIFO_WRITE_tlast0_carry__0_n_6;
  wire FIFO_WRITE_tlast0_carry__0_n_7;
  wire FIFO_WRITE_tlast0_carry_i_10_n_0;
  wire FIFO_WRITE_tlast0_carry_i_10_n_1;
  wire FIFO_WRITE_tlast0_carry_i_10_n_2;
  wire FIFO_WRITE_tlast0_carry_i_10_n_3;
  wire FIFO_WRITE_tlast0_carry_i_10_n_4;
  wire FIFO_WRITE_tlast0_carry_i_10_n_5;
  wire FIFO_WRITE_tlast0_carry_i_10_n_6;
  wire FIFO_WRITE_tlast0_carry_i_10_n_7;
  wire FIFO_WRITE_tlast0_carry_i_11_n_0;
  wire FIFO_WRITE_tlast0_carry_i_11_n_1;
  wire FIFO_WRITE_tlast0_carry_i_11_n_2;
  wire FIFO_WRITE_tlast0_carry_i_11_n_3;
  wire FIFO_WRITE_tlast0_carry_i_11_n_4;
  wire FIFO_WRITE_tlast0_carry_i_11_n_5;
  wire FIFO_WRITE_tlast0_carry_i_11_n_6;
  wire FIFO_WRITE_tlast0_carry_i_11_n_7;
  wire FIFO_WRITE_tlast0_carry_i_12_n_0;
  wire FIFO_WRITE_tlast0_carry_i_13_n_0;
  wire FIFO_WRITE_tlast0_carry_i_14_n_0;
  wire FIFO_WRITE_tlast0_carry_i_15_n_0;
  wire FIFO_WRITE_tlast0_carry_i_16_n_0;
  wire FIFO_WRITE_tlast0_carry_i_17_n_0;
  wire FIFO_WRITE_tlast0_carry_i_18_n_0;
  wire FIFO_WRITE_tlast0_carry_i_19_n_0;
  wire FIFO_WRITE_tlast0_carry_i_1_n_0;
  wire FIFO_WRITE_tlast0_carry_i_20_n_0;
  wire FIFO_WRITE_tlast0_carry_i_21_n_0;
  wire FIFO_WRITE_tlast0_carry_i_22_n_0;
  wire FIFO_WRITE_tlast0_carry_i_23_n_0;
  wire FIFO_WRITE_tlast0_carry_i_24_n_0;
  wire FIFO_WRITE_tlast0_carry_i_25_n_0;
  wire FIFO_WRITE_tlast0_carry_i_26_n_0;
  wire FIFO_WRITE_tlast0_carry_i_27_n_0;
  wire FIFO_WRITE_tlast0_carry_i_28_n_0;
  wire FIFO_WRITE_tlast0_carry_i_29_n_0;
  wire FIFO_WRITE_tlast0_carry_i_2_n_0;
  wire FIFO_WRITE_tlast0_carry_i_30_n_0;
  wire FIFO_WRITE_tlast0_carry_i_31_n_0;
  wire FIFO_WRITE_tlast0_carry_i_32_n_0;
  wire FIFO_WRITE_tlast0_carry_i_33_n_0;
  wire FIFO_WRITE_tlast0_carry_i_34_n_0;
  wire FIFO_WRITE_tlast0_carry_i_35_n_0;
  wire FIFO_WRITE_tlast0_carry_i_3_n_0;
  wire FIFO_WRITE_tlast0_carry_i_4_n_0;
  wire FIFO_WRITE_tlast0_carry_i_5_n_0;
  wire FIFO_WRITE_tlast0_carry_i_6_n_0;
  wire FIFO_WRITE_tlast0_carry_i_7_n_0;
  wire FIFO_WRITE_tlast0_carry_i_8_n_0;
  wire FIFO_WRITE_tlast0_carry_i_9_n_0;
  wire FIFO_WRITE_tlast0_carry_i_9_n_1;
  wire FIFO_WRITE_tlast0_carry_i_9_n_2;
  wire FIFO_WRITE_tlast0_carry_i_9_n_3;
  wire FIFO_WRITE_tlast0_carry_i_9_n_4;
  wire FIFO_WRITE_tlast0_carry_i_9_n_5;
  wire FIFO_WRITE_tlast0_carry_i_9_n_6;
  wire FIFO_WRITE_tlast0_carry_i_9_n_7;
  wire FIFO_WRITE_tlast0_carry_n_0;
  wire FIFO_WRITE_tlast0_carry_n_1;
  wire FIFO_WRITE_tlast0_carry_n_2;
  wire FIFO_WRITE_tlast0_carry_n_3;
  wire FIFO_WRITE_tlast0_carry_n_4;
  wire FIFO_WRITE_tlast0_carry_n_5;
  wire FIFO_WRITE_tlast0_carry_n_6;
  wire FIFO_WRITE_tlast0_carry_n_7;
  wire [31:1]FIFO_WRITE_tlast1;
  wire FIFO_WRITE_tready;
  wire [2:0]FSM;
  wire [2:0]FSM_next;
  wire [2:0]FSM_next__0;
  wire \FSM_sequential_FSM_next_reg[2]_i_2_n_0 ;
  wire \FSM_sequential_FSM_next_reg[2]_i_3_n_0 ;
  wire \FSM_sequential_readaddr_FSM_next_reg[1]_i_2_n_0 ;
  wire \FSM_sequential_readaddr_FSM_next_reg[1]_i_3_n_0 ;
  wire \FSM_sequential_readaddr_FSM_next_reg[1]_i_4_n_0 ;
  wire \FSM_sequential_readaddr_FSM_next_reg[1]_i_5_n_0 ;
  wire \FSM_sequential_readaddr_FSM_next_reg[1]_i_6_n_0 ;
  wire \FSM_sequential_readaddr_FSM_next_reg[1]_i_7_n_0 ;
  wire \FSM_sequential_readaddr_FSM_next_reg[1]_i_8_n_0 ;
  wire \FSM_sequential_readaddr_FSM_next_reg[1]_i_9_n_0 ;
  wire \FSM_sequential_readaddr_FSM_next_reg[2]_i_2_n_0 ;
  wire \FSM_sequential_readaddr_FSM_next_reg[2]_i_3_n_0 ;
  wire \FSM_sequential_readaddr_FSM_next_reg_n_0_[1] ;
  wire \FSM_sequential_readdata_FSM_next_reg[2]_i_2_n_0 ;
  wire \FSM_sequential_readdata_FSM_next_reg[2]_i_3_n_0 ;
  wire \FSM_sequential_readdata_FSM_next_reg_n_0_[1] ;
  wire \FSM_sequential_writeaddr_FSM_next_reg[1]_i_2_n_0 ;
  wire \FSM_sequential_writeaddr_FSM_next_reg[1]_i_3_n_0 ;
  wire \FSM_sequential_writeaddr_FSM_next_reg[1]_i_4_n_0 ;
  wire \FSM_sequential_writeaddr_FSM_next_reg[1]_i_5_n_0 ;
  wire \FSM_sequential_writeaddr_FSM_next_reg[1]_i_6_n_0 ;
  wire \FSM_sequential_writeaddr_FSM_next_reg[1]_i_7_n_0 ;
  wire \FSM_sequential_writeaddr_FSM_next_reg[1]_i_8_n_0 ;
  wire \FSM_sequential_writeaddr_FSM_next_reg[1]_i_9_n_0 ;
  wire \FSM_sequential_writeaddr_FSM_next_reg[2]_i_2_n_0 ;
  wire \FSM_sequential_writeaddr_FSM_next_reg[2]_i_3_n_0 ;
  wire \FSM_sequential_writeaddr_FSM_next_reg_n_0_[1] ;
  wire \FSM_sequential_writedata_FSM_next_reg[2]_i_2_n_0 ;
  wire \FSM_sequential_writedata_FSM_next_reg[2]_i_3_n_0 ;
  wire \FSM_sequential_writedata_FSM_next_reg_n_0_[1] ;
  wire busy;
  wire busy_i_1_n_0;
  wire clk;
  wire [31:4]p_0_in;
  wire [31:0]rd_blockamount;
  wire rd_running;
  wire [31:0]rd_startaddress;
  wire [31:0]read_blockamount;
  wire read_start_i_1_n_0;
  wire read_start_reg_n_0;
  wire [31:0]read_startaddress;
  wire read_startaddress_1;
  wire [2:0]readaddr_FSM;
  wire [2:0]readaddr_FSM_next;
  wire readaddr_FSM_next17_in;
  wire readaddr_FSM_next1_carry__0_i_10_n_0;
  wire readaddr_FSM_next1_carry__0_i_11_n_0;
  wire readaddr_FSM_next1_carry__0_i_12_n_0;
  wire readaddr_FSM_next1_carry__0_i_13_n_0;
  wire readaddr_FSM_next1_carry__0_i_14_n_0;
  wire readaddr_FSM_next1_carry__0_i_15_n_11;
  wire readaddr_FSM_next1_carry__0_i_15_n_12;
  wire readaddr_FSM_next1_carry__0_i_15_n_13;
  wire readaddr_FSM_next1_carry__0_i_15_n_14;
  wire readaddr_FSM_next1_carry__0_i_15_n_15;
  wire readaddr_FSM_next1_carry__0_i_15_n_4;
  wire readaddr_FSM_next1_carry__0_i_15_n_5;
  wire readaddr_FSM_next1_carry__0_i_15_n_6;
  wire readaddr_FSM_next1_carry__0_i_15_n_7;
  wire readaddr_FSM_next1_carry__0_i_16_n_0;
  wire readaddr_FSM_next1_carry__0_i_16_n_1;
  wire readaddr_FSM_next1_carry__0_i_16_n_10;
  wire readaddr_FSM_next1_carry__0_i_16_n_11;
  wire readaddr_FSM_next1_carry__0_i_16_n_12;
  wire readaddr_FSM_next1_carry__0_i_16_n_13;
  wire readaddr_FSM_next1_carry__0_i_16_n_14;
  wire readaddr_FSM_next1_carry__0_i_16_n_15;
  wire readaddr_FSM_next1_carry__0_i_16_n_2;
  wire readaddr_FSM_next1_carry__0_i_16_n_3;
  wire readaddr_FSM_next1_carry__0_i_16_n_4;
  wire readaddr_FSM_next1_carry__0_i_16_n_5;
  wire readaddr_FSM_next1_carry__0_i_16_n_6;
  wire readaddr_FSM_next1_carry__0_i_16_n_7;
  wire readaddr_FSM_next1_carry__0_i_16_n_8;
  wire readaddr_FSM_next1_carry__0_i_16_n_9;
  wire readaddr_FSM_next1_carry__0_i_1_n_0;
  wire readaddr_FSM_next1_carry__0_i_2_n_0;
  wire readaddr_FSM_next1_carry__0_i_3_n_0;
  wire readaddr_FSM_next1_carry__0_i_4_n_0;
  wire readaddr_FSM_next1_carry__0_i_5_n_0;
  wire readaddr_FSM_next1_carry__0_i_6_n_0;
  wire readaddr_FSM_next1_carry__0_i_7_n_0;
  wire readaddr_FSM_next1_carry__0_i_8_n_0;
  wire readaddr_FSM_next1_carry__0_i_9_n_0;
  wire readaddr_FSM_next1_carry__0_n_2;
  wire readaddr_FSM_next1_carry__0_n_3;
  wire readaddr_FSM_next1_carry__0_n_4;
  wire readaddr_FSM_next1_carry__0_n_5;
  wire readaddr_FSM_next1_carry__0_n_6;
  wire readaddr_FSM_next1_carry__0_n_7;
  wire readaddr_FSM_next1_carry_i_10_n_0;
  wire readaddr_FSM_next1_carry_i_11_n_0;
  wire readaddr_FSM_next1_carry_i_12_n_0;
  wire readaddr_FSM_next1_carry_i_13_n_0;
  wire readaddr_FSM_next1_carry_i_14_n_0;
  wire readaddr_FSM_next1_carry_i_15_n_0;
  wire readaddr_FSM_next1_carry_i_16_n_0;
  wire readaddr_FSM_next1_carry_i_17_n_0;
  wire readaddr_FSM_next1_carry_i_17_n_1;
  wire readaddr_FSM_next1_carry_i_17_n_10;
  wire readaddr_FSM_next1_carry_i_17_n_11;
  wire readaddr_FSM_next1_carry_i_17_n_12;
  wire readaddr_FSM_next1_carry_i_17_n_13;
  wire readaddr_FSM_next1_carry_i_17_n_14;
  wire readaddr_FSM_next1_carry_i_17_n_15;
  wire readaddr_FSM_next1_carry_i_17_n_2;
  wire readaddr_FSM_next1_carry_i_17_n_3;
  wire readaddr_FSM_next1_carry_i_17_n_4;
  wire readaddr_FSM_next1_carry_i_17_n_5;
  wire readaddr_FSM_next1_carry_i_17_n_6;
  wire readaddr_FSM_next1_carry_i_17_n_7;
  wire readaddr_FSM_next1_carry_i_17_n_8;
  wire readaddr_FSM_next1_carry_i_17_n_9;
  wire readaddr_FSM_next1_carry_i_18_n_0;
  wire readaddr_FSM_next1_carry_i_18_n_1;
  wire readaddr_FSM_next1_carry_i_18_n_10;
  wire readaddr_FSM_next1_carry_i_18_n_11;
  wire readaddr_FSM_next1_carry_i_18_n_12;
  wire readaddr_FSM_next1_carry_i_18_n_13;
  wire readaddr_FSM_next1_carry_i_18_n_14;
  wire readaddr_FSM_next1_carry_i_18_n_2;
  wire readaddr_FSM_next1_carry_i_18_n_3;
  wire readaddr_FSM_next1_carry_i_18_n_4;
  wire readaddr_FSM_next1_carry_i_18_n_5;
  wire readaddr_FSM_next1_carry_i_18_n_6;
  wire readaddr_FSM_next1_carry_i_18_n_7;
  wire readaddr_FSM_next1_carry_i_18_n_8;
  wire readaddr_FSM_next1_carry_i_18_n_9;
  wire readaddr_FSM_next1_carry_i_19_n_0;
  wire readaddr_FSM_next1_carry_i_1_n_0;
  wire readaddr_FSM_next1_carry_i_2_n_0;
  wire readaddr_FSM_next1_carry_i_3_n_0;
  wire readaddr_FSM_next1_carry_i_4_n_0;
  wire readaddr_FSM_next1_carry_i_5_n_0;
  wire readaddr_FSM_next1_carry_i_6_n_0;
  wire readaddr_FSM_next1_carry_i_7_n_0;
  wire readaddr_FSM_next1_carry_i_8_n_0;
  wire readaddr_FSM_next1_carry_i_9_n_0;
  wire readaddr_FSM_next1_carry_n_0;
  wire readaddr_FSM_next1_carry_n_1;
  wire readaddr_FSM_next1_carry_n_2;
  wire readaddr_FSM_next1_carry_n_3;
  wire readaddr_FSM_next1_carry_n_4;
  wire readaddr_FSM_next1_carry_n_5;
  wire readaddr_FSM_next1_carry_n_6;
  wire readaddr_FSM_next1_carry_n_7;
  wire [2:0]readaddr_FSM_next__0;
  wire readaddr_count;
  wire \readaddr_count[4]_i_3_n_0 ;
  wire [31:4]readaddr_count_reg;
  wire \readaddr_count_reg[12]_i_1_n_0 ;
  wire \readaddr_count_reg[12]_i_1_n_1 ;
  wire \readaddr_count_reg[12]_i_1_n_10 ;
  wire \readaddr_count_reg[12]_i_1_n_11 ;
  wire \readaddr_count_reg[12]_i_1_n_12 ;
  wire \readaddr_count_reg[12]_i_1_n_13 ;
  wire \readaddr_count_reg[12]_i_1_n_14 ;
  wire \readaddr_count_reg[12]_i_1_n_15 ;
  wire \readaddr_count_reg[12]_i_1_n_2 ;
  wire \readaddr_count_reg[12]_i_1_n_3 ;
  wire \readaddr_count_reg[12]_i_1_n_4 ;
  wire \readaddr_count_reg[12]_i_1_n_5 ;
  wire \readaddr_count_reg[12]_i_1_n_6 ;
  wire \readaddr_count_reg[12]_i_1_n_7 ;
  wire \readaddr_count_reg[12]_i_1_n_8 ;
  wire \readaddr_count_reg[12]_i_1_n_9 ;
  wire \readaddr_count_reg[20]_i_1_n_0 ;
  wire \readaddr_count_reg[20]_i_1_n_1 ;
  wire \readaddr_count_reg[20]_i_1_n_10 ;
  wire \readaddr_count_reg[20]_i_1_n_11 ;
  wire \readaddr_count_reg[20]_i_1_n_12 ;
  wire \readaddr_count_reg[20]_i_1_n_13 ;
  wire \readaddr_count_reg[20]_i_1_n_14 ;
  wire \readaddr_count_reg[20]_i_1_n_15 ;
  wire \readaddr_count_reg[20]_i_1_n_2 ;
  wire \readaddr_count_reg[20]_i_1_n_3 ;
  wire \readaddr_count_reg[20]_i_1_n_4 ;
  wire \readaddr_count_reg[20]_i_1_n_5 ;
  wire \readaddr_count_reg[20]_i_1_n_6 ;
  wire \readaddr_count_reg[20]_i_1_n_7 ;
  wire \readaddr_count_reg[20]_i_1_n_8 ;
  wire \readaddr_count_reg[20]_i_1_n_9 ;
  wire \readaddr_count_reg[28]_i_1_n_12 ;
  wire \readaddr_count_reg[28]_i_1_n_13 ;
  wire \readaddr_count_reg[28]_i_1_n_14 ;
  wire \readaddr_count_reg[28]_i_1_n_15 ;
  wire \readaddr_count_reg[28]_i_1_n_5 ;
  wire \readaddr_count_reg[28]_i_1_n_6 ;
  wire \readaddr_count_reg[28]_i_1_n_7 ;
  wire \readaddr_count_reg[4]_i_2_n_0 ;
  wire \readaddr_count_reg[4]_i_2_n_1 ;
  wire \readaddr_count_reg[4]_i_2_n_10 ;
  wire \readaddr_count_reg[4]_i_2_n_11 ;
  wire \readaddr_count_reg[4]_i_2_n_12 ;
  wire \readaddr_count_reg[4]_i_2_n_13 ;
  wire \readaddr_count_reg[4]_i_2_n_14 ;
  wire \readaddr_count_reg[4]_i_2_n_15 ;
  wire \readaddr_count_reg[4]_i_2_n_2 ;
  wire \readaddr_count_reg[4]_i_2_n_3 ;
  wire \readaddr_count_reg[4]_i_2_n_4 ;
  wire \readaddr_count_reg[4]_i_2_n_5 ;
  wire \readaddr_count_reg[4]_i_2_n_6 ;
  wire \readaddr_count_reg[4]_i_2_n_7 ;
  wire \readaddr_count_reg[4]_i_2_n_8 ;
  wire \readaddr_count_reg[4]_i_2_n_9 ;
  wire readaddr_done_i_1_n_0;
  wire readaddr_done_reg_n_0;
  wire [2:0]readdata_FSM;
  wire [2:0]readdata_FSM_next;
  wire [2:0]readdata_FSM_next__0;
  wire readdata_count;
  wire readdata_count0;
  wire \readdata_count[0]_i_4_n_0 ;
  wire [31:0]readdata_count_reg;
  wire \readdata_count_reg[0]_i_3_n_0 ;
  wire \readdata_count_reg[0]_i_3_n_1 ;
  wire \readdata_count_reg[0]_i_3_n_10 ;
  wire \readdata_count_reg[0]_i_3_n_11 ;
  wire \readdata_count_reg[0]_i_3_n_12 ;
  wire \readdata_count_reg[0]_i_3_n_13 ;
  wire \readdata_count_reg[0]_i_3_n_14 ;
  wire \readdata_count_reg[0]_i_3_n_15 ;
  wire \readdata_count_reg[0]_i_3_n_2 ;
  wire \readdata_count_reg[0]_i_3_n_3 ;
  wire \readdata_count_reg[0]_i_3_n_4 ;
  wire \readdata_count_reg[0]_i_3_n_5 ;
  wire \readdata_count_reg[0]_i_3_n_6 ;
  wire \readdata_count_reg[0]_i_3_n_7 ;
  wire \readdata_count_reg[0]_i_3_n_8 ;
  wire \readdata_count_reg[0]_i_3_n_9 ;
  wire \readdata_count_reg[16]_i_1_n_0 ;
  wire \readdata_count_reg[16]_i_1_n_1 ;
  wire \readdata_count_reg[16]_i_1_n_10 ;
  wire \readdata_count_reg[16]_i_1_n_11 ;
  wire \readdata_count_reg[16]_i_1_n_12 ;
  wire \readdata_count_reg[16]_i_1_n_13 ;
  wire \readdata_count_reg[16]_i_1_n_14 ;
  wire \readdata_count_reg[16]_i_1_n_15 ;
  wire \readdata_count_reg[16]_i_1_n_2 ;
  wire \readdata_count_reg[16]_i_1_n_3 ;
  wire \readdata_count_reg[16]_i_1_n_4 ;
  wire \readdata_count_reg[16]_i_1_n_5 ;
  wire \readdata_count_reg[16]_i_1_n_6 ;
  wire \readdata_count_reg[16]_i_1_n_7 ;
  wire \readdata_count_reg[16]_i_1_n_8 ;
  wire \readdata_count_reg[16]_i_1_n_9 ;
  wire \readdata_count_reg[24]_i_1_n_1 ;
  wire \readdata_count_reg[24]_i_1_n_10 ;
  wire \readdata_count_reg[24]_i_1_n_11 ;
  wire \readdata_count_reg[24]_i_1_n_12 ;
  wire \readdata_count_reg[24]_i_1_n_13 ;
  wire \readdata_count_reg[24]_i_1_n_14 ;
  wire \readdata_count_reg[24]_i_1_n_15 ;
  wire \readdata_count_reg[24]_i_1_n_2 ;
  wire \readdata_count_reg[24]_i_1_n_3 ;
  wire \readdata_count_reg[24]_i_1_n_4 ;
  wire \readdata_count_reg[24]_i_1_n_5 ;
  wire \readdata_count_reg[24]_i_1_n_6 ;
  wire \readdata_count_reg[24]_i_1_n_7 ;
  wire \readdata_count_reg[24]_i_1_n_8 ;
  wire \readdata_count_reg[24]_i_1_n_9 ;
  wire \readdata_count_reg[8]_i_1_n_0 ;
  wire \readdata_count_reg[8]_i_1_n_1 ;
  wire \readdata_count_reg[8]_i_1_n_10 ;
  wire \readdata_count_reg[8]_i_1_n_11 ;
  wire \readdata_count_reg[8]_i_1_n_12 ;
  wire \readdata_count_reg[8]_i_1_n_13 ;
  wire \readdata_count_reg[8]_i_1_n_14 ;
  wire \readdata_count_reg[8]_i_1_n_15 ;
  wire \readdata_count_reg[8]_i_1_n_2 ;
  wire \readdata_count_reg[8]_i_1_n_3 ;
  wire \readdata_count_reg[8]_i_1_n_4 ;
  wire \readdata_count_reg[8]_i_1_n_5 ;
  wire \readdata_count_reg[8]_i_1_n_6 ;
  wire \readdata_count_reg[8]_i_1_n_7 ;
  wire \readdata_count_reg[8]_i_1_n_8 ;
  wire \readdata_count_reg[8]_i_1_n_9 ;
  wire readdata_done_i_1_n_0;
  wire readdata_done_reg_n_0;
  wire [31:0]wr_blockamount;
  wire wr_running;
  wire [31:0]wr_startaddress;
  wire [31:0]write_blockamount;
  wire write_start_i_1_n_0;
  wire write_start_reg_n_0;
  wire [31:0]write_startaddress;
  wire write_startaddress_0;
  wire [2:0]writeaddr_FSM;
  wire [2:0]writeaddr_FSM_next;
  wire writeaddr_FSM_next15_in;
  wire writeaddr_FSM_next1_carry__0_i_10_n_0;
  wire writeaddr_FSM_next1_carry__0_i_11_n_0;
  wire writeaddr_FSM_next1_carry__0_i_12_n_0;
  wire writeaddr_FSM_next1_carry__0_i_13_n_0;
  wire writeaddr_FSM_next1_carry__0_i_14_n_0;
  wire writeaddr_FSM_next1_carry__0_i_15_n_4;
  wire writeaddr_FSM_next1_carry__0_i_15_n_5;
  wire writeaddr_FSM_next1_carry__0_i_15_n_6;
  wire writeaddr_FSM_next1_carry__0_i_15_n_7;
  wire writeaddr_FSM_next1_carry__0_i_16_n_0;
  wire writeaddr_FSM_next1_carry__0_i_16_n_1;
  wire writeaddr_FSM_next1_carry__0_i_16_n_2;
  wire writeaddr_FSM_next1_carry__0_i_16_n_3;
  wire writeaddr_FSM_next1_carry__0_i_16_n_4;
  wire writeaddr_FSM_next1_carry__0_i_16_n_5;
  wire writeaddr_FSM_next1_carry__0_i_16_n_6;
  wire writeaddr_FSM_next1_carry__0_i_16_n_7;
  wire writeaddr_FSM_next1_carry__0_i_1_n_0;
  wire writeaddr_FSM_next1_carry__0_i_2_n_0;
  wire writeaddr_FSM_next1_carry__0_i_3_n_0;
  wire writeaddr_FSM_next1_carry__0_i_4_n_0;
  wire writeaddr_FSM_next1_carry__0_i_5_n_0;
  wire writeaddr_FSM_next1_carry__0_i_6_n_0;
  wire writeaddr_FSM_next1_carry__0_i_7_n_0;
  wire writeaddr_FSM_next1_carry__0_i_8_n_0;
  wire writeaddr_FSM_next1_carry__0_i_9_n_0;
  wire writeaddr_FSM_next1_carry__0_n_2;
  wire writeaddr_FSM_next1_carry__0_n_3;
  wire writeaddr_FSM_next1_carry__0_n_4;
  wire writeaddr_FSM_next1_carry__0_n_5;
  wire writeaddr_FSM_next1_carry__0_n_6;
  wire writeaddr_FSM_next1_carry__0_n_7;
  wire writeaddr_FSM_next1_carry_i_10_n_0;
  wire writeaddr_FSM_next1_carry_i_11_n_0;
  wire writeaddr_FSM_next1_carry_i_12_n_0;
  wire writeaddr_FSM_next1_carry_i_13_n_0;
  wire writeaddr_FSM_next1_carry_i_14_n_0;
  wire writeaddr_FSM_next1_carry_i_15_n_0;
  wire writeaddr_FSM_next1_carry_i_16_n_0;
  wire writeaddr_FSM_next1_carry_i_17_n_0;
  wire writeaddr_FSM_next1_carry_i_17_n_1;
  wire writeaddr_FSM_next1_carry_i_17_n_2;
  wire writeaddr_FSM_next1_carry_i_17_n_3;
  wire writeaddr_FSM_next1_carry_i_17_n_4;
  wire writeaddr_FSM_next1_carry_i_17_n_5;
  wire writeaddr_FSM_next1_carry_i_17_n_6;
  wire writeaddr_FSM_next1_carry_i_17_n_7;
  wire writeaddr_FSM_next1_carry_i_18_n_0;
  wire writeaddr_FSM_next1_carry_i_18_n_1;
  wire writeaddr_FSM_next1_carry_i_18_n_2;
  wire writeaddr_FSM_next1_carry_i_18_n_3;
  wire writeaddr_FSM_next1_carry_i_18_n_4;
  wire writeaddr_FSM_next1_carry_i_18_n_5;
  wire writeaddr_FSM_next1_carry_i_18_n_6;
  wire writeaddr_FSM_next1_carry_i_18_n_7;
  wire writeaddr_FSM_next1_carry_i_19_n_0;
  wire writeaddr_FSM_next1_carry_i_1_n_0;
  wire writeaddr_FSM_next1_carry_i_2_n_0;
  wire writeaddr_FSM_next1_carry_i_3_n_0;
  wire writeaddr_FSM_next1_carry_i_4_n_0;
  wire writeaddr_FSM_next1_carry_i_5_n_0;
  wire writeaddr_FSM_next1_carry_i_6_n_0;
  wire writeaddr_FSM_next1_carry_i_7_n_0;
  wire writeaddr_FSM_next1_carry_i_8_n_0;
  wire writeaddr_FSM_next1_carry_i_9_n_0;
  wire writeaddr_FSM_next1_carry_n_0;
  wire writeaddr_FSM_next1_carry_n_1;
  wire writeaddr_FSM_next1_carry_n_2;
  wire writeaddr_FSM_next1_carry_n_3;
  wire writeaddr_FSM_next1_carry_n_4;
  wire writeaddr_FSM_next1_carry_n_5;
  wire writeaddr_FSM_next1_carry_n_6;
  wire writeaddr_FSM_next1_carry_n_7;
  wire [2:0]writeaddr_FSM_next__0;
  wire writeaddr_count;
  wire writeaddr_count0;
  wire \writeaddr_count[4]_i_3_n_0 ;
  wire [31:4]writeaddr_count_reg;
  wire \writeaddr_count_reg[12]_i_1_n_0 ;
  wire \writeaddr_count_reg[12]_i_1_n_1 ;
  wire \writeaddr_count_reg[12]_i_1_n_10 ;
  wire \writeaddr_count_reg[12]_i_1_n_11 ;
  wire \writeaddr_count_reg[12]_i_1_n_12 ;
  wire \writeaddr_count_reg[12]_i_1_n_13 ;
  wire \writeaddr_count_reg[12]_i_1_n_14 ;
  wire \writeaddr_count_reg[12]_i_1_n_15 ;
  wire \writeaddr_count_reg[12]_i_1_n_2 ;
  wire \writeaddr_count_reg[12]_i_1_n_3 ;
  wire \writeaddr_count_reg[12]_i_1_n_4 ;
  wire \writeaddr_count_reg[12]_i_1_n_5 ;
  wire \writeaddr_count_reg[12]_i_1_n_6 ;
  wire \writeaddr_count_reg[12]_i_1_n_7 ;
  wire \writeaddr_count_reg[12]_i_1_n_8 ;
  wire \writeaddr_count_reg[12]_i_1_n_9 ;
  wire \writeaddr_count_reg[20]_i_1_n_0 ;
  wire \writeaddr_count_reg[20]_i_1_n_1 ;
  wire \writeaddr_count_reg[20]_i_1_n_10 ;
  wire \writeaddr_count_reg[20]_i_1_n_11 ;
  wire \writeaddr_count_reg[20]_i_1_n_12 ;
  wire \writeaddr_count_reg[20]_i_1_n_13 ;
  wire \writeaddr_count_reg[20]_i_1_n_14 ;
  wire \writeaddr_count_reg[20]_i_1_n_15 ;
  wire \writeaddr_count_reg[20]_i_1_n_2 ;
  wire \writeaddr_count_reg[20]_i_1_n_3 ;
  wire \writeaddr_count_reg[20]_i_1_n_4 ;
  wire \writeaddr_count_reg[20]_i_1_n_5 ;
  wire \writeaddr_count_reg[20]_i_1_n_6 ;
  wire \writeaddr_count_reg[20]_i_1_n_7 ;
  wire \writeaddr_count_reg[20]_i_1_n_8 ;
  wire \writeaddr_count_reg[20]_i_1_n_9 ;
  wire \writeaddr_count_reg[28]_i_1_n_12 ;
  wire \writeaddr_count_reg[28]_i_1_n_13 ;
  wire \writeaddr_count_reg[28]_i_1_n_14 ;
  wire \writeaddr_count_reg[28]_i_1_n_15 ;
  wire \writeaddr_count_reg[28]_i_1_n_5 ;
  wire \writeaddr_count_reg[28]_i_1_n_6 ;
  wire \writeaddr_count_reg[28]_i_1_n_7 ;
  wire \writeaddr_count_reg[4]_i_2_n_0 ;
  wire \writeaddr_count_reg[4]_i_2_n_1 ;
  wire \writeaddr_count_reg[4]_i_2_n_10 ;
  wire \writeaddr_count_reg[4]_i_2_n_11 ;
  wire \writeaddr_count_reg[4]_i_2_n_12 ;
  wire \writeaddr_count_reg[4]_i_2_n_13 ;
  wire \writeaddr_count_reg[4]_i_2_n_14 ;
  wire \writeaddr_count_reg[4]_i_2_n_15 ;
  wire \writeaddr_count_reg[4]_i_2_n_2 ;
  wire \writeaddr_count_reg[4]_i_2_n_3 ;
  wire \writeaddr_count_reg[4]_i_2_n_4 ;
  wire \writeaddr_count_reg[4]_i_2_n_5 ;
  wire \writeaddr_count_reg[4]_i_2_n_6 ;
  wire \writeaddr_count_reg[4]_i_2_n_7 ;
  wire \writeaddr_count_reg[4]_i_2_n_8 ;
  wire \writeaddr_count_reg[4]_i_2_n_9 ;
  wire writeaddr_done_i_1_n_0;
  wire writeaddr_done_reg_n_0;
  wire [2:0]writedata_FSM;
  wire [2:0]writedata_FSM_next;
  wire writedata_FSM_next1;
  wire writedata_FSM_next1_carry__0_i_10_n_0;
  wire writedata_FSM_next1_carry__0_i_11_n_0;
  wire writedata_FSM_next1_carry__0_i_1_n_0;
  wire writedata_FSM_next1_carry__0_i_2_n_0;
  wire writedata_FSM_next1_carry__0_i_3_n_0;
  wire writedata_FSM_next1_carry__0_i_4_n_2;
  wire writedata_FSM_next1_carry__0_i_4_n_3;
  wire writedata_FSM_next1_carry__0_i_4_n_4;
  wire writedata_FSM_next1_carry__0_i_4_n_5;
  wire writedata_FSM_next1_carry__0_i_4_n_6;
  wire writedata_FSM_next1_carry__0_i_4_n_7;
  wire writedata_FSM_next1_carry__0_i_5_n_0;
  wire writedata_FSM_next1_carry__0_i_6_n_0;
  wire writedata_FSM_next1_carry__0_i_7_n_0;
  wire writedata_FSM_next1_carry__0_i_8_n_0;
  wire writedata_FSM_next1_carry__0_i_9_n_0;
  wire writedata_FSM_next1_carry__0_n_6;
  wire writedata_FSM_next1_carry__0_n_7;
  wire writedata_FSM_next1_carry_i_10_n_0;
  wire writedata_FSM_next1_carry_i_10_n_1;
  wire writedata_FSM_next1_carry_i_10_n_2;
  wire writedata_FSM_next1_carry_i_10_n_3;
  wire writedata_FSM_next1_carry_i_10_n_4;
  wire writedata_FSM_next1_carry_i_10_n_5;
  wire writedata_FSM_next1_carry_i_10_n_6;
  wire writedata_FSM_next1_carry_i_10_n_7;
  wire writedata_FSM_next1_carry_i_11_n_0;
  wire writedata_FSM_next1_carry_i_11_n_1;
  wire writedata_FSM_next1_carry_i_11_n_2;
  wire writedata_FSM_next1_carry_i_11_n_3;
  wire writedata_FSM_next1_carry_i_11_n_4;
  wire writedata_FSM_next1_carry_i_11_n_5;
  wire writedata_FSM_next1_carry_i_11_n_6;
  wire writedata_FSM_next1_carry_i_11_n_7;
  wire writedata_FSM_next1_carry_i_12_n_0;
  wire writedata_FSM_next1_carry_i_13_n_0;
  wire writedata_FSM_next1_carry_i_14_n_0;
  wire writedata_FSM_next1_carry_i_15_n_0;
  wire writedata_FSM_next1_carry_i_16_n_0;
  wire writedata_FSM_next1_carry_i_17_n_0;
  wire writedata_FSM_next1_carry_i_18_n_0;
  wire writedata_FSM_next1_carry_i_19_n_0;
  wire writedata_FSM_next1_carry_i_1_n_0;
  wire writedata_FSM_next1_carry_i_20_n_0;
  wire writedata_FSM_next1_carry_i_21_n_0;
  wire writedata_FSM_next1_carry_i_22_n_0;
  wire writedata_FSM_next1_carry_i_23_n_0;
  wire writedata_FSM_next1_carry_i_24_n_0;
  wire writedata_FSM_next1_carry_i_25_n_0;
  wire writedata_FSM_next1_carry_i_26_n_0;
  wire writedata_FSM_next1_carry_i_27_n_0;
  wire writedata_FSM_next1_carry_i_28_n_0;
  wire writedata_FSM_next1_carry_i_29_n_0;
  wire writedata_FSM_next1_carry_i_2_n_0;
  wire writedata_FSM_next1_carry_i_30_n_0;
  wire writedata_FSM_next1_carry_i_31_n_0;
  wire writedata_FSM_next1_carry_i_32_n_0;
  wire writedata_FSM_next1_carry_i_33_n_0;
  wire writedata_FSM_next1_carry_i_34_n_0;
  wire writedata_FSM_next1_carry_i_35_n_0;
  wire writedata_FSM_next1_carry_i_3_n_0;
  wire writedata_FSM_next1_carry_i_4_n_0;
  wire writedata_FSM_next1_carry_i_5_n_0;
  wire writedata_FSM_next1_carry_i_6_n_0;
  wire writedata_FSM_next1_carry_i_7_n_0;
  wire writedata_FSM_next1_carry_i_8_n_0;
  wire writedata_FSM_next1_carry_i_9_n_0;
  wire writedata_FSM_next1_carry_i_9_n_1;
  wire writedata_FSM_next1_carry_i_9_n_2;
  wire writedata_FSM_next1_carry_i_9_n_3;
  wire writedata_FSM_next1_carry_i_9_n_4;
  wire writedata_FSM_next1_carry_i_9_n_5;
  wire writedata_FSM_next1_carry_i_9_n_6;
  wire writedata_FSM_next1_carry_i_9_n_7;
  wire writedata_FSM_next1_carry_n_0;
  wire writedata_FSM_next1_carry_n_1;
  wire writedata_FSM_next1_carry_n_2;
  wire writedata_FSM_next1_carry_n_3;
  wire writedata_FSM_next1_carry_n_4;
  wire writedata_FSM_next1_carry_n_5;
  wire writedata_FSM_next1_carry_n_6;
  wire writedata_FSM_next1_carry_n_7;
  wire [31:1]writedata_FSM_next2;
  wire [2:0]writedata_FSM_next__0;
  wire writedata_count;
  wire writedata_count0;
  wire \writedata_count[0]_i_4_n_0 ;
  wire [3:0]writedata_count_reg;
  wire \writedata_count_reg[0]_i_3_n_0 ;
  wire \writedata_count_reg[0]_i_3_n_1 ;
  wire \writedata_count_reg[0]_i_3_n_10 ;
  wire \writedata_count_reg[0]_i_3_n_11 ;
  wire \writedata_count_reg[0]_i_3_n_12 ;
  wire \writedata_count_reg[0]_i_3_n_13 ;
  wire \writedata_count_reg[0]_i_3_n_14 ;
  wire \writedata_count_reg[0]_i_3_n_15 ;
  wire \writedata_count_reg[0]_i_3_n_2 ;
  wire \writedata_count_reg[0]_i_3_n_3 ;
  wire \writedata_count_reg[0]_i_3_n_4 ;
  wire \writedata_count_reg[0]_i_3_n_5 ;
  wire \writedata_count_reg[0]_i_3_n_6 ;
  wire \writedata_count_reg[0]_i_3_n_7 ;
  wire \writedata_count_reg[0]_i_3_n_8 ;
  wire \writedata_count_reg[0]_i_3_n_9 ;
  wire \writedata_count_reg[16]_i_1_n_0 ;
  wire \writedata_count_reg[16]_i_1_n_1 ;
  wire \writedata_count_reg[16]_i_1_n_10 ;
  wire \writedata_count_reg[16]_i_1_n_11 ;
  wire \writedata_count_reg[16]_i_1_n_12 ;
  wire \writedata_count_reg[16]_i_1_n_13 ;
  wire \writedata_count_reg[16]_i_1_n_14 ;
  wire \writedata_count_reg[16]_i_1_n_15 ;
  wire \writedata_count_reg[16]_i_1_n_2 ;
  wire \writedata_count_reg[16]_i_1_n_3 ;
  wire \writedata_count_reg[16]_i_1_n_4 ;
  wire \writedata_count_reg[16]_i_1_n_5 ;
  wire \writedata_count_reg[16]_i_1_n_6 ;
  wire \writedata_count_reg[16]_i_1_n_7 ;
  wire \writedata_count_reg[16]_i_1_n_8 ;
  wire \writedata_count_reg[16]_i_1_n_9 ;
  wire \writedata_count_reg[24]_i_1_n_1 ;
  wire \writedata_count_reg[24]_i_1_n_10 ;
  wire \writedata_count_reg[24]_i_1_n_11 ;
  wire \writedata_count_reg[24]_i_1_n_12 ;
  wire \writedata_count_reg[24]_i_1_n_13 ;
  wire \writedata_count_reg[24]_i_1_n_14 ;
  wire \writedata_count_reg[24]_i_1_n_15 ;
  wire \writedata_count_reg[24]_i_1_n_2 ;
  wire \writedata_count_reg[24]_i_1_n_3 ;
  wire \writedata_count_reg[24]_i_1_n_4 ;
  wire \writedata_count_reg[24]_i_1_n_5 ;
  wire \writedata_count_reg[24]_i_1_n_6 ;
  wire \writedata_count_reg[24]_i_1_n_7 ;
  wire \writedata_count_reg[24]_i_1_n_8 ;
  wire \writedata_count_reg[24]_i_1_n_9 ;
  wire \writedata_count_reg[8]_i_1_n_0 ;
  wire \writedata_count_reg[8]_i_1_n_1 ;
  wire \writedata_count_reg[8]_i_1_n_10 ;
  wire \writedata_count_reg[8]_i_1_n_11 ;
  wire \writedata_count_reg[8]_i_1_n_12 ;
  wire \writedata_count_reg[8]_i_1_n_13 ;
  wire \writedata_count_reg[8]_i_1_n_14 ;
  wire \writedata_count_reg[8]_i_1_n_15 ;
  wire \writedata_count_reg[8]_i_1_n_2 ;
  wire \writedata_count_reg[8]_i_1_n_3 ;
  wire \writedata_count_reg[8]_i_1_n_4 ;
  wire \writedata_count_reg[8]_i_1_n_5 ;
  wire \writedata_count_reg[8]_i_1_n_6 ;
  wire \writedata_count_reg[8]_i_1_n_7 ;
  wire \writedata_count_reg[8]_i_1_n_8 ;
  wire \writedata_count_reg[8]_i_1_n_9 ;
  wire [31:4]writedata_count_reg__0;
  wire writedata_done_i_1_n_0;
  wire writedata_done_reg_n_0;
  wire [7:3]\NLW_AXI_ARADDR_reg[32]_i_3_CO_UNCONNECTED ;
  wire [7:4]\NLW_AXI_ARADDR_reg[32]_i_3_O_UNCONNECTED ;
  wire [7:3]\NLW_AXI_AWADDR_reg[32]_i_3_CO_UNCONNECTED ;
  wire [7:4]\NLW_AXI_AWADDR_reg[32]_i_3_O_UNCONNECTED ;
  wire [7:0]NLW_FIFO_WRITE_tlast0_carry_O_UNCONNECTED;
  wire [7:3]NLW_FIFO_WRITE_tlast0_carry__0_CO_UNCONNECTED;
  wire [7:0]NLW_FIFO_WRITE_tlast0_carry__0_O_UNCONNECTED;
  wire [7:6]NLW_FIFO_WRITE_tlast0_carry__0_i_4_CO_UNCONNECTED;
  wire [7:7]NLW_FIFO_WRITE_tlast0_carry__0_i_4_O_UNCONNECTED;
  wire [7:0]NLW_readaddr_FSM_next1_carry_O_UNCONNECTED;
  wire [7:7]NLW_readaddr_FSM_next1_carry__0_CO_UNCONNECTED;
  wire [7:0]NLW_readaddr_FSM_next1_carry__0_O_UNCONNECTED;
  wire [7:4]NLW_readaddr_FSM_next1_carry__0_i_15_CO_UNCONNECTED;
  wire [7:5]NLW_readaddr_FSM_next1_carry__0_i_15_O_UNCONNECTED;
  wire [0:0]NLW_readaddr_FSM_next1_carry_i_18_O_UNCONNECTED;
  wire [7:3]\NLW_readaddr_count_reg[28]_i_1_CO_UNCONNECTED ;
  wire [7:4]\NLW_readaddr_count_reg[28]_i_1_O_UNCONNECTED ;
  wire [7:7]\NLW_readdata_count_reg[24]_i_1_CO_UNCONNECTED ;
  wire [7:0]NLW_writeaddr_FSM_next1_carry_O_UNCONNECTED;
  wire [7:7]NLW_writeaddr_FSM_next1_carry__0_CO_UNCONNECTED;
  wire [7:0]NLW_writeaddr_FSM_next1_carry__0_O_UNCONNECTED;
  wire [7:4]NLW_writeaddr_FSM_next1_carry__0_i_15_CO_UNCONNECTED;
  wire [7:5]NLW_writeaddr_FSM_next1_carry__0_i_15_O_UNCONNECTED;
  wire [0:0]NLW_writeaddr_FSM_next1_carry_i_18_O_UNCONNECTED;
  wire [7:3]\NLW_writeaddr_count_reg[28]_i_1_CO_UNCONNECTED ;
  wire [7:4]\NLW_writeaddr_count_reg[28]_i_1_O_UNCONNECTED ;
  wire [7:0]NLW_writedata_FSM_next1_carry_O_UNCONNECTED;
  wire [7:3]NLW_writedata_FSM_next1_carry__0_CO_UNCONNECTED;
  wire [7:0]NLW_writedata_FSM_next1_carry__0_O_UNCONNECTED;
  wire [7:6]NLW_writedata_FSM_next1_carry__0_i_4_CO_UNCONNECTED;
  wire [7:7]NLW_writedata_FSM_next1_carry__0_i_4_O_UNCONNECTED;
  wire [7:7]\NLW_writedata_count_reg[24]_i_1_CO_UNCONNECTED ;

  LUT3 #(
    .INIT(8'h02)) 
    \/i_ 
       (.I0(FSM_next[0]),
        .I1(FSM_next[2]),
        .I2(FSM_next[1]),
        .O(write_startaddress_0));
  LUT3 #(
    .INIT(8'h40)) 
    \/i___0 
       (.I0(FSM_next[2]),
        .I1(FSM_next[0]),
        .I2(FSM_next[1]),
        .O(read_startaddress_1));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_ARADDR[11]_i_2 
       (.I0(read_startaddress[11]),
        .I1(readaddr_count_reg[11]),
        .O(\AXI_ARADDR[11]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_ARADDR[11]_i_3 
       (.I0(read_startaddress[10]),
        .I1(readaddr_count_reg[10]),
        .O(\AXI_ARADDR[11]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_ARADDR[11]_i_4 
       (.I0(read_startaddress[9]),
        .I1(readaddr_count_reg[9]),
        .O(\AXI_ARADDR[11]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_ARADDR[11]_i_5 
       (.I0(read_startaddress[8]),
        .I1(readaddr_count_reg[8]),
        .O(\AXI_ARADDR[11]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_ARADDR[11]_i_6 
       (.I0(read_startaddress[7]),
        .I1(readaddr_count_reg[7]),
        .O(\AXI_ARADDR[11]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_ARADDR[11]_i_7 
       (.I0(read_startaddress[6]),
        .I1(readaddr_count_reg[6]),
        .O(\AXI_ARADDR[11]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_ARADDR[11]_i_8 
       (.I0(read_startaddress[5]),
        .I1(readaddr_count_reg[5]),
        .O(\AXI_ARADDR[11]_i_8_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_ARADDR[11]_i_9 
       (.I0(read_startaddress[4]),
        .I1(readaddr_count_reg[4]),
        .O(\AXI_ARADDR[11]_i_9_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_ARADDR[19]_i_2 
       (.I0(read_startaddress[19]),
        .I1(readaddr_count_reg[19]),
        .O(\AXI_ARADDR[19]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_ARADDR[19]_i_3 
       (.I0(read_startaddress[18]),
        .I1(readaddr_count_reg[18]),
        .O(\AXI_ARADDR[19]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_ARADDR[19]_i_4 
       (.I0(read_startaddress[17]),
        .I1(readaddr_count_reg[17]),
        .O(\AXI_ARADDR[19]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_ARADDR[19]_i_5 
       (.I0(read_startaddress[16]),
        .I1(readaddr_count_reg[16]),
        .O(\AXI_ARADDR[19]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_ARADDR[19]_i_6 
       (.I0(read_startaddress[15]),
        .I1(readaddr_count_reg[15]),
        .O(\AXI_ARADDR[19]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_ARADDR[19]_i_7 
       (.I0(read_startaddress[14]),
        .I1(readaddr_count_reg[14]),
        .O(\AXI_ARADDR[19]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_ARADDR[19]_i_8 
       (.I0(read_startaddress[13]),
        .I1(readaddr_count_reg[13]),
        .O(\AXI_ARADDR[19]_i_8_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_ARADDR[19]_i_9 
       (.I0(read_startaddress[12]),
        .I1(readaddr_count_reg[12]),
        .O(\AXI_ARADDR[19]_i_9_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_ARADDR[27]_i_2 
       (.I0(read_startaddress[27]),
        .I1(readaddr_count_reg[27]),
        .O(\AXI_ARADDR[27]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_ARADDR[27]_i_3 
       (.I0(read_startaddress[26]),
        .I1(readaddr_count_reg[26]),
        .O(\AXI_ARADDR[27]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_ARADDR[27]_i_4 
       (.I0(read_startaddress[25]),
        .I1(readaddr_count_reg[25]),
        .O(\AXI_ARADDR[27]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_ARADDR[27]_i_5 
       (.I0(read_startaddress[24]),
        .I1(readaddr_count_reg[24]),
        .O(\AXI_ARADDR[27]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_ARADDR[27]_i_6 
       (.I0(read_startaddress[23]),
        .I1(readaddr_count_reg[23]),
        .O(\AXI_ARADDR[27]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_ARADDR[27]_i_7 
       (.I0(read_startaddress[22]),
        .I1(readaddr_count_reg[22]),
        .O(\AXI_ARADDR[27]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_ARADDR[27]_i_8 
       (.I0(read_startaddress[21]),
        .I1(readaddr_count_reg[21]),
        .O(\AXI_ARADDR[27]_i_8_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_ARADDR[27]_i_9 
       (.I0(read_startaddress[20]),
        .I1(readaddr_count_reg[20]),
        .O(\AXI_ARADDR[27]_i_9_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \AXI_ARADDR[32]_i_1 
       (.I0(readaddr_FSM_next[2]),
        .I1(\FSM_sequential_readaddr_FSM_next_reg_n_0_[1] ),
        .I2(readaddr_FSM_next[0]),
        .O(AXI_ARADDR0));
  LUT3 #(
    .INIT(8'h06)) 
    \AXI_ARADDR[32]_i_2 
       (.I0(\FSM_sequential_readaddr_FSM_next_reg_n_0_[1] ),
        .I1(readaddr_FSM_next[0]),
        .I2(readaddr_FSM_next[2]),
        .O(\AXI_ARADDR[32]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_ARADDR[32]_i_4 
       (.I0(read_startaddress[31]),
        .I1(readaddr_count_reg[31]),
        .O(\AXI_ARADDR[32]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_ARADDR[32]_i_5 
       (.I0(read_startaddress[30]),
        .I1(readaddr_count_reg[30]),
        .O(\AXI_ARADDR[32]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_ARADDR[32]_i_6 
       (.I0(read_startaddress[29]),
        .I1(readaddr_count_reg[29]),
        .O(\AXI_ARADDR[32]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_ARADDR[32]_i_7 
       (.I0(read_startaddress[28]),
        .I1(readaddr_count_reg[28]),
        .O(\AXI_ARADDR[32]_i_7_n_0 ));
  FDRE \AXI_ARADDR_reg[0] 
       (.C(clk),
        .CE(\AXI_ARADDR[32]_i_2_n_0 ),
        .D(read_startaddress[0]),
        .Q(AXI_ARADDR[0]),
        .R(AXI_ARADDR0));
  FDRE \AXI_ARADDR_reg[10] 
       (.C(clk),
        .CE(\AXI_ARADDR[32]_i_2_n_0 ),
        .D(AXI_ARADDR00_in[10]),
        .Q(AXI_ARADDR[10]),
        .R(AXI_ARADDR0));
  FDRE \AXI_ARADDR_reg[11] 
       (.C(clk),
        .CE(\AXI_ARADDR[32]_i_2_n_0 ),
        .D(AXI_ARADDR00_in[11]),
        .Q(AXI_ARADDR[11]),
        .R(AXI_ARADDR0));
  CARRY8 \AXI_ARADDR_reg[11]_i_1 
       (.CI(1'b0),
        .CI_TOP(1'b0),
        .CO({\AXI_ARADDR_reg[11]_i_1_n_0 ,\AXI_ARADDR_reg[11]_i_1_n_1 ,\AXI_ARADDR_reg[11]_i_1_n_2 ,\AXI_ARADDR_reg[11]_i_1_n_3 ,\AXI_ARADDR_reg[11]_i_1_n_4 ,\AXI_ARADDR_reg[11]_i_1_n_5 ,\AXI_ARADDR_reg[11]_i_1_n_6 ,\AXI_ARADDR_reg[11]_i_1_n_7 }),
        .DI(read_startaddress[11:4]),
        .O(AXI_ARADDR00_in[11:4]),
        .S({\AXI_ARADDR[11]_i_2_n_0 ,\AXI_ARADDR[11]_i_3_n_0 ,\AXI_ARADDR[11]_i_4_n_0 ,\AXI_ARADDR[11]_i_5_n_0 ,\AXI_ARADDR[11]_i_6_n_0 ,\AXI_ARADDR[11]_i_7_n_0 ,\AXI_ARADDR[11]_i_8_n_0 ,\AXI_ARADDR[11]_i_9_n_0 }));
  FDRE \AXI_ARADDR_reg[12] 
       (.C(clk),
        .CE(\AXI_ARADDR[32]_i_2_n_0 ),
        .D(AXI_ARADDR00_in[12]),
        .Q(AXI_ARADDR[12]),
        .R(AXI_ARADDR0));
  FDRE \AXI_ARADDR_reg[13] 
       (.C(clk),
        .CE(\AXI_ARADDR[32]_i_2_n_0 ),
        .D(AXI_ARADDR00_in[13]),
        .Q(AXI_ARADDR[13]),
        .R(AXI_ARADDR0));
  FDRE \AXI_ARADDR_reg[14] 
       (.C(clk),
        .CE(\AXI_ARADDR[32]_i_2_n_0 ),
        .D(AXI_ARADDR00_in[14]),
        .Q(AXI_ARADDR[14]),
        .R(AXI_ARADDR0));
  FDRE \AXI_ARADDR_reg[15] 
       (.C(clk),
        .CE(\AXI_ARADDR[32]_i_2_n_0 ),
        .D(AXI_ARADDR00_in[15]),
        .Q(AXI_ARADDR[15]),
        .R(AXI_ARADDR0));
  FDRE \AXI_ARADDR_reg[16] 
       (.C(clk),
        .CE(\AXI_ARADDR[32]_i_2_n_0 ),
        .D(AXI_ARADDR00_in[16]),
        .Q(AXI_ARADDR[16]),
        .R(AXI_ARADDR0));
  FDRE \AXI_ARADDR_reg[17] 
       (.C(clk),
        .CE(\AXI_ARADDR[32]_i_2_n_0 ),
        .D(AXI_ARADDR00_in[17]),
        .Q(AXI_ARADDR[17]),
        .R(AXI_ARADDR0));
  FDRE \AXI_ARADDR_reg[18] 
       (.C(clk),
        .CE(\AXI_ARADDR[32]_i_2_n_0 ),
        .D(AXI_ARADDR00_in[18]),
        .Q(AXI_ARADDR[18]),
        .R(AXI_ARADDR0));
  FDRE \AXI_ARADDR_reg[19] 
       (.C(clk),
        .CE(\AXI_ARADDR[32]_i_2_n_0 ),
        .D(AXI_ARADDR00_in[19]),
        .Q(AXI_ARADDR[19]),
        .R(AXI_ARADDR0));
  CARRY8 \AXI_ARADDR_reg[19]_i_1 
       (.CI(\AXI_ARADDR_reg[11]_i_1_n_0 ),
        .CI_TOP(1'b0),
        .CO({\AXI_ARADDR_reg[19]_i_1_n_0 ,\AXI_ARADDR_reg[19]_i_1_n_1 ,\AXI_ARADDR_reg[19]_i_1_n_2 ,\AXI_ARADDR_reg[19]_i_1_n_3 ,\AXI_ARADDR_reg[19]_i_1_n_4 ,\AXI_ARADDR_reg[19]_i_1_n_5 ,\AXI_ARADDR_reg[19]_i_1_n_6 ,\AXI_ARADDR_reg[19]_i_1_n_7 }),
        .DI(read_startaddress[19:12]),
        .O(AXI_ARADDR00_in[19:12]),
        .S({\AXI_ARADDR[19]_i_2_n_0 ,\AXI_ARADDR[19]_i_3_n_0 ,\AXI_ARADDR[19]_i_4_n_0 ,\AXI_ARADDR[19]_i_5_n_0 ,\AXI_ARADDR[19]_i_6_n_0 ,\AXI_ARADDR[19]_i_7_n_0 ,\AXI_ARADDR[19]_i_8_n_0 ,\AXI_ARADDR[19]_i_9_n_0 }));
  FDRE \AXI_ARADDR_reg[1] 
       (.C(clk),
        .CE(\AXI_ARADDR[32]_i_2_n_0 ),
        .D(read_startaddress[1]),
        .Q(AXI_ARADDR[1]),
        .R(AXI_ARADDR0));
  FDRE \AXI_ARADDR_reg[20] 
       (.C(clk),
        .CE(\AXI_ARADDR[32]_i_2_n_0 ),
        .D(AXI_ARADDR00_in[20]),
        .Q(AXI_ARADDR[20]),
        .R(AXI_ARADDR0));
  FDRE \AXI_ARADDR_reg[21] 
       (.C(clk),
        .CE(\AXI_ARADDR[32]_i_2_n_0 ),
        .D(AXI_ARADDR00_in[21]),
        .Q(AXI_ARADDR[21]),
        .R(AXI_ARADDR0));
  FDRE \AXI_ARADDR_reg[22] 
       (.C(clk),
        .CE(\AXI_ARADDR[32]_i_2_n_0 ),
        .D(AXI_ARADDR00_in[22]),
        .Q(AXI_ARADDR[22]),
        .R(AXI_ARADDR0));
  FDRE \AXI_ARADDR_reg[23] 
       (.C(clk),
        .CE(\AXI_ARADDR[32]_i_2_n_0 ),
        .D(AXI_ARADDR00_in[23]),
        .Q(AXI_ARADDR[23]),
        .R(AXI_ARADDR0));
  FDRE \AXI_ARADDR_reg[24] 
       (.C(clk),
        .CE(\AXI_ARADDR[32]_i_2_n_0 ),
        .D(AXI_ARADDR00_in[24]),
        .Q(AXI_ARADDR[24]),
        .R(AXI_ARADDR0));
  FDRE \AXI_ARADDR_reg[25] 
       (.C(clk),
        .CE(\AXI_ARADDR[32]_i_2_n_0 ),
        .D(AXI_ARADDR00_in[25]),
        .Q(AXI_ARADDR[25]),
        .R(AXI_ARADDR0));
  FDRE \AXI_ARADDR_reg[26] 
       (.C(clk),
        .CE(\AXI_ARADDR[32]_i_2_n_0 ),
        .D(AXI_ARADDR00_in[26]),
        .Q(AXI_ARADDR[26]),
        .R(AXI_ARADDR0));
  FDRE \AXI_ARADDR_reg[27] 
       (.C(clk),
        .CE(\AXI_ARADDR[32]_i_2_n_0 ),
        .D(AXI_ARADDR00_in[27]),
        .Q(AXI_ARADDR[27]),
        .R(AXI_ARADDR0));
  CARRY8 \AXI_ARADDR_reg[27]_i_1 
       (.CI(\AXI_ARADDR_reg[19]_i_1_n_0 ),
        .CI_TOP(1'b0),
        .CO({\AXI_ARADDR_reg[27]_i_1_n_0 ,\AXI_ARADDR_reg[27]_i_1_n_1 ,\AXI_ARADDR_reg[27]_i_1_n_2 ,\AXI_ARADDR_reg[27]_i_1_n_3 ,\AXI_ARADDR_reg[27]_i_1_n_4 ,\AXI_ARADDR_reg[27]_i_1_n_5 ,\AXI_ARADDR_reg[27]_i_1_n_6 ,\AXI_ARADDR_reg[27]_i_1_n_7 }),
        .DI(read_startaddress[27:20]),
        .O(AXI_ARADDR00_in[27:20]),
        .S({\AXI_ARADDR[27]_i_2_n_0 ,\AXI_ARADDR[27]_i_3_n_0 ,\AXI_ARADDR[27]_i_4_n_0 ,\AXI_ARADDR[27]_i_5_n_0 ,\AXI_ARADDR[27]_i_6_n_0 ,\AXI_ARADDR[27]_i_7_n_0 ,\AXI_ARADDR[27]_i_8_n_0 ,\AXI_ARADDR[27]_i_9_n_0 }));
  FDRE \AXI_ARADDR_reg[28] 
       (.C(clk),
        .CE(\AXI_ARADDR[32]_i_2_n_0 ),
        .D(AXI_ARADDR00_in[28]),
        .Q(AXI_ARADDR[28]),
        .R(AXI_ARADDR0));
  FDRE \AXI_ARADDR_reg[29] 
       (.C(clk),
        .CE(\AXI_ARADDR[32]_i_2_n_0 ),
        .D(AXI_ARADDR00_in[29]),
        .Q(AXI_ARADDR[29]),
        .R(AXI_ARADDR0));
  FDRE \AXI_ARADDR_reg[2] 
       (.C(clk),
        .CE(\AXI_ARADDR[32]_i_2_n_0 ),
        .D(read_startaddress[2]),
        .Q(AXI_ARADDR[2]),
        .R(AXI_ARADDR0));
  FDRE \AXI_ARADDR_reg[30] 
       (.C(clk),
        .CE(\AXI_ARADDR[32]_i_2_n_0 ),
        .D(AXI_ARADDR00_in[30]),
        .Q(AXI_ARADDR[30]),
        .R(AXI_ARADDR0));
  FDRE \AXI_ARADDR_reg[31] 
       (.C(clk),
        .CE(\AXI_ARADDR[32]_i_2_n_0 ),
        .D(AXI_ARADDR00_in[31]),
        .Q(AXI_ARADDR[31]),
        .R(AXI_ARADDR0));
  FDRE \AXI_ARADDR_reg[32] 
       (.C(clk),
        .CE(\AXI_ARADDR[32]_i_2_n_0 ),
        .D(AXI_ARADDR00_in[32]),
        .Q(AXI_ARADDR[32]),
        .R(AXI_ARADDR0));
  CARRY8 \AXI_ARADDR_reg[32]_i_3 
       (.CI(\AXI_ARADDR_reg[27]_i_1_n_0 ),
        .CI_TOP(1'b0),
        .CO({\NLW_AXI_ARADDR_reg[32]_i_3_CO_UNCONNECTED [7:5],AXI_ARADDR00_in[32],\NLW_AXI_ARADDR_reg[32]_i_3_CO_UNCONNECTED [3],\AXI_ARADDR_reg[32]_i_3_n_5 ,\AXI_ARADDR_reg[32]_i_3_n_6 ,\AXI_ARADDR_reg[32]_i_3_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,read_startaddress[31:28]}),
        .O({\NLW_AXI_ARADDR_reg[32]_i_3_O_UNCONNECTED [7:4],AXI_ARADDR00_in[31:28]}),
        .S({1'b0,1'b0,1'b0,1'b1,\AXI_ARADDR[32]_i_4_n_0 ,\AXI_ARADDR[32]_i_5_n_0 ,\AXI_ARADDR[32]_i_6_n_0 ,\AXI_ARADDR[32]_i_7_n_0 }));
  FDRE \AXI_ARADDR_reg[3] 
       (.C(clk),
        .CE(\AXI_ARADDR[32]_i_2_n_0 ),
        .D(read_startaddress[3]),
        .Q(AXI_ARADDR[3]),
        .R(AXI_ARADDR0));
  FDRE \AXI_ARADDR_reg[4] 
       (.C(clk),
        .CE(\AXI_ARADDR[32]_i_2_n_0 ),
        .D(AXI_ARADDR00_in[4]),
        .Q(AXI_ARADDR[4]),
        .R(AXI_ARADDR0));
  FDRE \AXI_ARADDR_reg[5] 
       (.C(clk),
        .CE(\AXI_ARADDR[32]_i_2_n_0 ),
        .D(AXI_ARADDR00_in[5]),
        .Q(AXI_ARADDR[5]),
        .R(AXI_ARADDR0));
  FDRE \AXI_ARADDR_reg[6] 
       (.C(clk),
        .CE(\AXI_ARADDR[32]_i_2_n_0 ),
        .D(AXI_ARADDR00_in[6]),
        .Q(AXI_ARADDR[6]),
        .R(AXI_ARADDR0));
  FDRE \AXI_ARADDR_reg[7] 
       (.C(clk),
        .CE(\AXI_ARADDR[32]_i_2_n_0 ),
        .D(AXI_ARADDR00_in[7]),
        .Q(AXI_ARADDR[7]),
        .R(AXI_ARADDR0));
  FDRE \AXI_ARADDR_reg[8] 
       (.C(clk),
        .CE(\AXI_ARADDR[32]_i_2_n_0 ),
        .D(AXI_ARADDR00_in[8]),
        .Q(AXI_ARADDR[8]),
        .R(AXI_ARADDR0));
  FDRE \AXI_ARADDR_reg[9] 
       (.C(clk),
        .CE(\AXI_ARADDR[32]_i_2_n_0 ),
        .D(AXI_ARADDR00_in[9]),
        .Q(AXI_ARADDR[9]),
        .R(AXI_ARADDR0));
  LUT3 #(
    .INIT(8'hF4)) 
    \AXI_ARLEN[0]_i_1 
       (.I0(read_blockamount[0]),
        .I1(\FSM_sequential_readaddr_FSM_next_reg_n_0_[1] ),
        .I2(readaddr_FSM_next[0]),
        .O(\AXI_ARLEN[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'hEAAE)) 
    \AXI_ARLEN[1]_i_1 
       (.I0(readaddr_FSM_next[0]),
        .I1(\FSM_sequential_readaddr_FSM_next_reg_n_0_[1] ),
        .I2(read_blockamount[0]),
        .I3(read_blockamount[1]),
        .O(\AXI_ARLEN[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'hFEABAAAA)) 
    \AXI_ARLEN[2]_i_1 
       (.I0(readaddr_FSM_next[0]),
        .I1(read_blockamount[0]),
        .I2(read_blockamount[1]),
        .I3(read_blockamount[2]),
        .I4(\FSM_sequential_readaddr_FSM_next_reg_n_0_[1] ),
        .O(\AXI_ARLEN[2]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h07)) 
    \AXI_ARLEN[3]_i_1 
       (.I0(readaddr_FSM_next[0]),
        .I1(\FSM_sequential_readaddr_FSM_next_reg_n_0_[1] ),
        .I2(readaddr_FSM_next[2]),
        .O(\AXI_ARLEN[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFEAAAAAAABAAAA)) 
    \AXI_ARLEN[3]_i_2 
       (.I0(readaddr_FSM_next[0]),
        .I1(read_blockamount[0]),
        .I2(read_blockamount[1]),
        .I3(read_blockamount[2]),
        .I4(\FSM_sequential_readaddr_FSM_next_reg_n_0_[1] ),
        .I5(read_blockamount[3]),
        .O(\AXI_ARLEN[3]_i_2_n_0 ));
  FDRE \AXI_ARLEN_reg[0] 
       (.C(clk),
        .CE(\AXI_ARLEN[3]_i_1_n_0 ),
        .D(\AXI_ARLEN[0]_i_1_n_0 ),
        .Q(AXI_ARLEN[0]),
        .R(1'b0));
  FDRE \AXI_ARLEN_reg[1] 
       (.C(clk),
        .CE(\AXI_ARLEN[3]_i_1_n_0 ),
        .D(\AXI_ARLEN[1]_i_1_n_0 ),
        .Q(AXI_ARLEN[1]),
        .R(1'b0));
  FDRE \AXI_ARLEN_reg[2] 
       (.C(clk),
        .CE(\AXI_ARLEN[3]_i_1_n_0 ),
        .D(\AXI_ARLEN[2]_i_1_n_0 ),
        .Q(AXI_ARLEN[2]),
        .R(1'b0));
  FDRE \AXI_ARLEN_reg[3] 
       (.C(clk),
        .CE(\AXI_ARLEN[3]_i_1_n_0 ),
        .D(\AXI_ARLEN[3]_i_2_n_0 ),
        .Q(AXI_ARLEN[3]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'h8BB8)) 
    AXI_ARVALID_i_1
       (.I0(AXI_ARVALID_reg_0),
        .I1(readaddr_FSM_next[2]),
        .I2(readaddr_FSM_next[0]),
        .I3(\FSM_sequential_readaddr_FSM_next_reg_n_0_[1] ),
        .O(AXI_ARVALID_i_1_n_0));
  FDRE AXI_ARVALID_reg
       (.C(clk),
        .CE(1'b1),
        .D(AXI_ARVALID_i_1_n_0),
        .Q(AXI_ARVALID_reg_0),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_AWADDR[11]_i_2 
       (.I0(write_startaddress[11]),
        .I1(writeaddr_count_reg[11]),
        .O(\AXI_AWADDR[11]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_AWADDR[11]_i_3 
       (.I0(write_startaddress[10]),
        .I1(writeaddr_count_reg[10]),
        .O(\AXI_AWADDR[11]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_AWADDR[11]_i_4 
       (.I0(write_startaddress[9]),
        .I1(writeaddr_count_reg[9]),
        .O(\AXI_AWADDR[11]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_AWADDR[11]_i_5 
       (.I0(write_startaddress[8]),
        .I1(writeaddr_count_reg[8]),
        .O(\AXI_AWADDR[11]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_AWADDR[11]_i_6 
       (.I0(write_startaddress[7]),
        .I1(writeaddr_count_reg[7]),
        .O(\AXI_AWADDR[11]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_AWADDR[11]_i_7 
       (.I0(write_startaddress[6]),
        .I1(writeaddr_count_reg[6]),
        .O(\AXI_AWADDR[11]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_AWADDR[11]_i_8 
       (.I0(write_startaddress[5]),
        .I1(writeaddr_count_reg[5]),
        .O(\AXI_AWADDR[11]_i_8_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_AWADDR[11]_i_9 
       (.I0(write_startaddress[4]),
        .I1(writeaddr_count_reg[4]),
        .O(\AXI_AWADDR[11]_i_9_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_AWADDR[19]_i_2 
       (.I0(write_startaddress[19]),
        .I1(writeaddr_count_reg[19]),
        .O(\AXI_AWADDR[19]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_AWADDR[19]_i_3 
       (.I0(write_startaddress[18]),
        .I1(writeaddr_count_reg[18]),
        .O(\AXI_AWADDR[19]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_AWADDR[19]_i_4 
       (.I0(write_startaddress[17]),
        .I1(writeaddr_count_reg[17]),
        .O(\AXI_AWADDR[19]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_AWADDR[19]_i_5 
       (.I0(write_startaddress[16]),
        .I1(writeaddr_count_reg[16]),
        .O(\AXI_AWADDR[19]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_AWADDR[19]_i_6 
       (.I0(write_startaddress[15]),
        .I1(writeaddr_count_reg[15]),
        .O(\AXI_AWADDR[19]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_AWADDR[19]_i_7 
       (.I0(write_startaddress[14]),
        .I1(writeaddr_count_reg[14]),
        .O(\AXI_AWADDR[19]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_AWADDR[19]_i_8 
       (.I0(write_startaddress[13]),
        .I1(writeaddr_count_reg[13]),
        .O(\AXI_AWADDR[19]_i_8_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_AWADDR[19]_i_9 
       (.I0(write_startaddress[12]),
        .I1(writeaddr_count_reg[12]),
        .O(\AXI_AWADDR[19]_i_9_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_AWADDR[27]_i_2 
       (.I0(write_startaddress[27]),
        .I1(writeaddr_count_reg[27]),
        .O(\AXI_AWADDR[27]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_AWADDR[27]_i_3 
       (.I0(write_startaddress[26]),
        .I1(writeaddr_count_reg[26]),
        .O(\AXI_AWADDR[27]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_AWADDR[27]_i_4 
       (.I0(write_startaddress[25]),
        .I1(writeaddr_count_reg[25]),
        .O(\AXI_AWADDR[27]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_AWADDR[27]_i_5 
       (.I0(write_startaddress[24]),
        .I1(writeaddr_count_reg[24]),
        .O(\AXI_AWADDR[27]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_AWADDR[27]_i_6 
       (.I0(write_startaddress[23]),
        .I1(writeaddr_count_reg[23]),
        .O(\AXI_AWADDR[27]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_AWADDR[27]_i_7 
       (.I0(write_startaddress[22]),
        .I1(writeaddr_count_reg[22]),
        .O(\AXI_AWADDR[27]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_AWADDR[27]_i_8 
       (.I0(write_startaddress[21]),
        .I1(writeaddr_count_reg[21]),
        .O(\AXI_AWADDR[27]_i_8_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_AWADDR[27]_i_9 
       (.I0(write_startaddress[20]),
        .I1(writeaddr_count_reg[20]),
        .O(\AXI_AWADDR[27]_i_9_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \AXI_AWADDR[32]_i_1 
       (.I0(writeaddr_FSM_next[2]),
        .I1(\FSM_sequential_writeaddr_FSM_next_reg_n_0_[1] ),
        .I2(writeaddr_FSM_next[0]),
        .O(writeaddr_count0));
  LUT3 #(
    .INIT(8'h06)) 
    \AXI_AWADDR[32]_i_2 
       (.I0(\FSM_sequential_writeaddr_FSM_next_reg_n_0_[1] ),
        .I1(writeaddr_FSM_next[0]),
        .I2(writeaddr_FSM_next[2]),
        .O(\AXI_AWADDR[32]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_AWADDR[32]_i_4 
       (.I0(write_startaddress[31]),
        .I1(writeaddr_count_reg[31]),
        .O(\AXI_AWADDR[32]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_AWADDR[32]_i_5 
       (.I0(write_startaddress[30]),
        .I1(writeaddr_count_reg[30]),
        .O(\AXI_AWADDR[32]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_AWADDR[32]_i_6 
       (.I0(write_startaddress[29]),
        .I1(writeaddr_count_reg[29]),
        .O(\AXI_AWADDR[32]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \AXI_AWADDR[32]_i_7 
       (.I0(write_startaddress[28]),
        .I1(writeaddr_count_reg[28]),
        .O(\AXI_AWADDR[32]_i_7_n_0 ));
  FDRE \AXI_AWADDR_reg[0] 
       (.C(clk),
        .CE(\AXI_AWADDR[32]_i_2_n_0 ),
        .D(write_startaddress[0]),
        .Q(AXI_AWADDR[0]),
        .R(writeaddr_count0));
  FDRE \AXI_AWADDR_reg[10] 
       (.C(clk),
        .CE(\AXI_AWADDR[32]_i_2_n_0 ),
        .D(AXI_AWADDR0[10]),
        .Q(AXI_AWADDR[10]),
        .R(writeaddr_count0));
  FDRE \AXI_AWADDR_reg[11] 
       (.C(clk),
        .CE(\AXI_AWADDR[32]_i_2_n_0 ),
        .D(AXI_AWADDR0[11]),
        .Q(AXI_AWADDR[11]),
        .R(writeaddr_count0));
  CARRY8 \AXI_AWADDR_reg[11]_i_1 
       (.CI(1'b0),
        .CI_TOP(1'b0),
        .CO({\AXI_AWADDR_reg[11]_i_1_n_0 ,\AXI_AWADDR_reg[11]_i_1_n_1 ,\AXI_AWADDR_reg[11]_i_1_n_2 ,\AXI_AWADDR_reg[11]_i_1_n_3 ,\AXI_AWADDR_reg[11]_i_1_n_4 ,\AXI_AWADDR_reg[11]_i_1_n_5 ,\AXI_AWADDR_reg[11]_i_1_n_6 ,\AXI_AWADDR_reg[11]_i_1_n_7 }),
        .DI(write_startaddress[11:4]),
        .O(AXI_AWADDR0[11:4]),
        .S({\AXI_AWADDR[11]_i_2_n_0 ,\AXI_AWADDR[11]_i_3_n_0 ,\AXI_AWADDR[11]_i_4_n_0 ,\AXI_AWADDR[11]_i_5_n_0 ,\AXI_AWADDR[11]_i_6_n_0 ,\AXI_AWADDR[11]_i_7_n_0 ,\AXI_AWADDR[11]_i_8_n_0 ,\AXI_AWADDR[11]_i_9_n_0 }));
  FDRE \AXI_AWADDR_reg[12] 
       (.C(clk),
        .CE(\AXI_AWADDR[32]_i_2_n_0 ),
        .D(AXI_AWADDR0[12]),
        .Q(AXI_AWADDR[12]),
        .R(writeaddr_count0));
  FDRE \AXI_AWADDR_reg[13] 
       (.C(clk),
        .CE(\AXI_AWADDR[32]_i_2_n_0 ),
        .D(AXI_AWADDR0[13]),
        .Q(AXI_AWADDR[13]),
        .R(writeaddr_count0));
  FDRE \AXI_AWADDR_reg[14] 
       (.C(clk),
        .CE(\AXI_AWADDR[32]_i_2_n_0 ),
        .D(AXI_AWADDR0[14]),
        .Q(AXI_AWADDR[14]),
        .R(writeaddr_count0));
  FDRE \AXI_AWADDR_reg[15] 
       (.C(clk),
        .CE(\AXI_AWADDR[32]_i_2_n_0 ),
        .D(AXI_AWADDR0[15]),
        .Q(AXI_AWADDR[15]),
        .R(writeaddr_count0));
  FDRE \AXI_AWADDR_reg[16] 
       (.C(clk),
        .CE(\AXI_AWADDR[32]_i_2_n_0 ),
        .D(AXI_AWADDR0[16]),
        .Q(AXI_AWADDR[16]),
        .R(writeaddr_count0));
  FDRE \AXI_AWADDR_reg[17] 
       (.C(clk),
        .CE(\AXI_AWADDR[32]_i_2_n_0 ),
        .D(AXI_AWADDR0[17]),
        .Q(AXI_AWADDR[17]),
        .R(writeaddr_count0));
  FDRE \AXI_AWADDR_reg[18] 
       (.C(clk),
        .CE(\AXI_AWADDR[32]_i_2_n_0 ),
        .D(AXI_AWADDR0[18]),
        .Q(AXI_AWADDR[18]),
        .R(writeaddr_count0));
  FDRE \AXI_AWADDR_reg[19] 
       (.C(clk),
        .CE(\AXI_AWADDR[32]_i_2_n_0 ),
        .D(AXI_AWADDR0[19]),
        .Q(AXI_AWADDR[19]),
        .R(writeaddr_count0));
  CARRY8 \AXI_AWADDR_reg[19]_i_1 
       (.CI(\AXI_AWADDR_reg[11]_i_1_n_0 ),
        .CI_TOP(1'b0),
        .CO({\AXI_AWADDR_reg[19]_i_1_n_0 ,\AXI_AWADDR_reg[19]_i_1_n_1 ,\AXI_AWADDR_reg[19]_i_1_n_2 ,\AXI_AWADDR_reg[19]_i_1_n_3 ,\AXI_AWADDR_reg[19]_i_1_n_4 ,\AXI_AWADDR_reg[19]_i_1_n_5 ,\AXI_AWADDR_reg[19]_i_1_n_6 ,\AXI_AWADDR_reg[19]_i_1_n_7 }),
        .DI(write_startaddress[19:12]),
        .O(AXI_AWADDR0[19:12]),
        .S({\AXI_AWADDR[19]_i_2_n_0 ,\AXI_AWADDR[19]_i_3_n_0 ,\AXI_AWADDR[19]_i_4_n_0 ,\AXI_AWADDR[19]_i_5_n_0 ,\AXI_AWADDR[19]_i_6_n_0 ,\AXI_AWADDR[19]_i_7_n_0 ,\AXI_AWADDR[19]_i_8_n_0 ,\AXI_AWADDR[19]_i_9_n_0 }));
  FDRE \AXI_AWADDR_reg[1] 
       (.C(clk),
        .CE(\AXI_AWADDR[32]_i_2_n_0 ),
        .D(write_startaddress[1]),
        .Q(AXI_AWADDR[1]),
        .R(writeaddr_count0));
  FDRE \AXI_AWADDR_reg[20] 
       (.C(clk),
        .CE(\AXI_AWADDR[32]_i_2_n_0 ),
        .D(AXI_AWADDR0[20]),
        .Q(AXI_AWADDR[20]),
        .R(writeaddr_count0));
  FDRE \AXI_AWADDR_reg[21] 
       (.C(clk),
        .CE(\AXI_AWADDR[32]_i_2_n_0 ),
        .D(AXI_AWADDR0[21]),
        .Q(AXI_AWADDR[21]),
        .R(writeaddr_count0));
  FDRE \AXI_AWADDR_reg[22] 
       (.C(clk),
        .CE(\AXI_AWADDR[32]_i_2_n_0 ),
        .D(AXI_AWADDR0[22]),
        .Q(AXI_AWADDR[22]),
        .R(writeaddr_count0));
  FDRE \AXI_AWADDR_reg[23] 
       (.C(clk),
        .CE(\AXI_AWADDR[32]_i_2_n_0 ),
        .D(AXI_AWADDR0[23]),
        .Q(AXI_AWADDR[23]),
        .R(writeaddr_count0));
  FDRE \AXI_AWADDR_reg[24] 
       (.C(clk),
        .CE(\AXI_AWADDR[32]_i_2_n_0 ),
        .D(AXI_AWADDR0[24]),
        .Q(AXI_AWADDR[24]),
        .R(writeaddr_count0));
  FDRE \AXI_AWADDR_reg[25] 
       (.C(clk),
        .CE(\AXI_AWADDR[32]_i_2_n_0 ),
        .D(AXI_AWADDR0[25]),
        .Q(AXI_AWADDR[25]),
        .R(writeaddr_count0));
  FDRE \AXI_AWADDR_reg[26] 
       (.C(clk),
        .CE(\AXI_AWADDR[32]_i_2_n_0 ),
        .D(AXI_AWADDR0[26]),
        .Q(AXI_AWADDR[26]),
        .R(writeaddr_count0));
  FDRE \AXI_AWADDR_reg[27] 
       (.C(clk),
        .CE(\AXI_AWADDR[32]_i_2_n_0 ),
        .D(AXI_AWADDR0[27]),
        .Q(AXI_AWADDR[27]),
        .R(writeaddr_count0));
  CARRY8 \AXI_AWADDR_reg[27]_i_1 
       (.CI(\AXI_AWADDR_reg[19]_i_1_n_0 ),
        .CI_TOP(1'b0),
        .CO({\AXI_AWADDR_reg[27]_i_1_n_0 ,\AXI_AWADDR_reg[27]_i_1_n_1 ,\AXI_AWADDR_reg[27]_i_1_n_2 ,\AXI_AWADDR_reg[27]_i_1_n_3 ,\AXI_AWADDR_reg[27]_i_1_n_4 ,\AXI_AWADDR_reg[27]_i_1_n_5 ,\AXI_AWADDR_reg[27]_i_1_n_6 ,\AXI_AWADDR_reg[27]_i_1_n_7 }),
        .DI(write_startaddress[27:20]),
        .O(AXI_AWADDR0[27:20]),
        .S({\AXI_AWADDR[27]_i_2_n_0 ,\AXI_AWADDR[27]_i_3_n_0 ,\AXI_AWADDR[27]_i_4_n_0 ,\AXI_AWADDR[27]_i_5_n_0 ,\AXI_AWADDR[27]_i_6_n_0 ,\AXI_AWADDR[27]_i_7_n_0 ,\AXI_AWADDR[27]_i_8_n_0 ,\AXI_AWADDR[27]_i_9_n_0 }));
  FDRE \AXI_AWADDR_reg[28] 
       (.C(clk),
        .CE(\AXI_AWADDR[32]_i_2_n_0 ),
        .D(AXI_AWADDR0[28]),
        .Q(AXI_AWADDR[28]),
        .R(writeaddr_count0));
  FDRE \AXI_AWADDR_reg[29] 
       (.C(clk),
        .CE(\AXI_AWADDR[32]_i_2_n_0 ),
        .D(AXI_AWADDR0[29]),
        .Q(AXI_AWADDR[29]),
        .R(writeaddr_count0));
  FDRE \AXI_AWADDR_reg[2] 
       (.C(clk),
        .CE(\AXI_AWADDR[32]_i_2_n_0 ),
        .D(write_startaddress[2]),
        .Q(AXI_AWADDR[2]),
        .R(writeaddr_count0));
  FDRE \AXI_AWADDR_reg[30] 
       (.C(clk),
        .CE(\AXI_AWADDR[32]_i_2_n_0 ),
        .D(AXI_AWADDR0[30]),
        .Q(AXI_AWADDR[30]),
        .R(writeaddr_count0));
  FDRE \AXI_AWADDR_reg[31] 
       (.C(clk),
        .CE(\AXI_AWADDR[32]_i_2_n_0 ),
        .D(AXI_AWADDR0[31]),
        .Q(AXI_AWADDR[31]),
        .R(writeaddr_count0));
  FDRE \AXI_AWADDR_reg[32] 
       (.C(clk),
        .CE(\AXI_AWADDR[32]_i_2_n_0 ),
        .D(AXI_AWADDR0[32]),
        .Q(AXI_AWADDR[32]),
        .R(writeaddr_count0));
  CARRY8 \AXI_AWADDR_reg[32]_i_3 
       (.CI(\AXI_AWADDR_reg[27]_i_1_n_0 ),
        .CI_TOP(1'b0),
        .CO({\NLW_AXI_AWADDR_reg[32]_i_3_CO_UNCONNECTED [7:5],AXI_AWADDR0[32],\NLW_AXI_AWADDR_reg[32]_i_3_CO_UNCONNECTED [3],\AXI_AWADDR_reg[32]_i_3_n_5 ,\AXI_AWADDR_reg[32]_i_3_n_6 ,\AXI_AWADDR_reg[32]_i_3_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,write_startaddress[31:28]}),
        .O({\NLW_AXI_AWADDR_reg[32]_i_3_O_UNCONNECTED [7:4],AXI_AWADDR0[31:28]}),
        .S({1'b0,1'b0,1'b0,1'b1,\AXI_AWADDR[32]_i_4_n_0 ,\AXI_AWADDR[32]_i_5_n_0 ,\AXI_AWADDR[32]_i_6_n_0 ,\AXI_AWADDR[32]_i_7_n_0 }));
  FDRE \AXI_AWADDR_reg[3] 
       (.C(clk),
        .CE(\AXI_AWADDR[32]_i_2_n_0 ),
        .D(write_startaddress[3]),
        .Q(AXI_AWADDR[3]),
        .R(writeaddr_count0));
  FDRE \AXI_AWADDR_reg[4] 
       (.C(clk),
        .CE(\AXI_AWADDR[32]_i_2_n_0 ),
        .D(AXI_AWADDR0[4]),
        .Q(AXI_AWADDR[4]),
        .R(writeaddr_count0));
  FDRE \AXI_AWADDR_reg[5] 
       (.C(clk),
        .CE(\AXI_AWADDR[32]_i_2_n_0 ),
        .D(AXI_AWADDR0[5]),
        .Q(AXI_AWADDR[5]),
        .R(writeaddr_count0));
  FDRE \AXI_AWADDR_reg[6] 
       (.C(clk),
        .CE(\AXI_AWADDR[32]_i_2_n_0 ),
        .D(AXI_AWADDR0[6]),
        .Q(AXI_AWADDR[6]),
        .R(writeaddr_count0));
  FDRE \AXI_AWADDR_reg[7] 
       (.C(clk),
        .CE(\AXI_AWADDR[32]_i_2_n_0 ),
        .D(AXI_AWADDR0[7]),
        .Q(AXI_AWADDR[7]),
        .R(writeaddr_count0));
  FDRE \AXI_AWADDR_reg[8] 
       (.C(clk),
        .CE(\AXI_AWADDR[32]_i_2_n_0 ),
        .D(AXI_AWADDR0[8]),
        .Q(AXI_AWADDR[8]),
        .R(writeaddr_count0));
  FDRE \AXI_AWADDR_reg[9] 
       (.C(clk),
        .CE(\AXI_AWADDR[32]_i_2_n_0 ),
        .D(AXI_AWADDR0[9]),
        .Q(AXI_AWADDR[9]),
        .R(writeaddr_count0));
  LUT3 #(
    .INIT(8'hF4)) 
    \AXI_AWLEN[0]_i_1 
       (.I0(write_blockamount[0]),
        .I1(\FSM_sequential_writeaddr_FSM_next_reg_n_0_[1] ),
        .I2(writeaddr_FSM_next[0]),
        .O(\AXI_AWLEN[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'hEAAE)) 
    \AXI_AWLEN[1]_i_1 
       (.I0(writeaddr_FSM_next[0]),
        .I1(\FSM_sequential_writeaddr_FSM_next_reg_n_0_[1] ),
        .I2(write_blockamount[0]),
        .I3(write_blockamount[1]),
        .O(\AXI_AWLEN[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hFEABAAAA)) 
    \AXI_AWLEN[2]_i_1 
       (.I0(writeaddr_FSM_next[0]),
        .I1(write_blockamount[0]),
        .I2(write_blockamount[1]),
        .I3(write_blockamount[2]),
        .I4(\FSM_sequential_writeaddr_FSM_next_reg_n_0_[1] ),
        .O(\AXI_AWLEN[2]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h07)) 
    \AXI_AWLEN[3]_i_1 
       (.I0(writeaddr_FSM_next[0]),
        .I1(\FSM_sequential_writeaddr_FSM_next_reg_n_0_[1] ),
        .I2(writeaddr_FSM_next[2]),
        .O(\AXI_AWLEN[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFEAAAAAAABAAAA)) 
    \AXI_AWLEN[3]_i_2 
       (.I0(writeaddr_FSM_next[0]),
        .I1(write_blockamount[0]),
        .I2(write_blockamount[1]),
        .I3(write_blockamount[2]),
        .I4(\FSM_sequential_writeaddr_FSM_next_reg_n_0_[1] ),
        .I5(write_blockamount[3]),
        .O(\AXI_AWLEN[3]_i_2_n_0 ));
  FDRE \AXI_AWLEN_reg[0] 
       (.C(clk),
        .CE(\AXI_AWLEN[3]_i_1_n_0 ),
        .D(\AXI_AWLEN[0]_i_1_n_0 ),
        .Q(AXI_AWLEN[0]),
        .R(1'b0));
  FDRE \AXI_AWLEN_reg[1] 
       (.C(clk),
        .CE(\AXI_AWLEN[3]_i_1_n_0 ),
        .D(\AXI_AWLEN[1]_i_1_n_0 ),
        .Q(AXI_AWLEN[1]),
        .R(1'b0));
  FDRE \AXI_AWLEN_reg[2] 
       (.C(clk),
        .CE(\AXI_AWLEN[3]_i_1_n_0 ),
        .D(\AXI_AWLEN[2]_i_1_n_0 ),
        .Q(AXI_AWLEN[2]),
        .R(1'b0));
  FDRE \AXI_AWLEN_reg[3] 
       (.C(clk),
        .CE(\AXI_AWLEN[3]_i_1_n_0 ),
        .D(\AXI_AWLEN[3]_i_2_n_0 ),
        .Q(AXI_AWLEN[3]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'h8BB8)) 
    AXI_AWVALID_i_1
       (.I0(AXI_AWVALID_reg_0),
        .I1(writeaddr_FSM_next[2]),
        .I2(writeaddr_FSM_next[0]),
        .I3(\FSM_sequential_writeaddr_FSM_next_reg_n_0_[1] ),
        .O(AXI_AWVALID_i_1_n_0));
  FDRE AXI_AWVALID_reg
       (.C(clk),
        .CE(1'b1),
        .D(AXI_AWVALID_i_1_n_0),
        .Q(AXI_AWVALID_reg_0),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h96696996)) 
    \AXI_WDATA_PARITY0_inferred__0/AXI_WDATA_PARITY[1]_INST_0 
       (.I0(FIFO_READ_tdata[13]),
        .I1(FIFO_READ_tdata[12]),
        .I2(FIFO_READ_tdata[15]),
        .I3(FIFO_READ_tdata[14]),
        .I4(\AXI_WDATA_PARITY0_inferred__0/AXI_WDATA_PARITY[1]_INST_0_i_1_n_0 ),
        .O(AXI_WDATA_PARITY[1]));
  LUT4 #(
    .INIT(16'h6996)) 
    \AXI_WDATA_PARITY0_inferred__0/AXI_WDATA_PARITY[1]_INST_0_i_1 
       (.I0(FIFO_READ_tdata[10]),
        .I1(FIFO_READ_tdata[11]),
        .I2(FIFO_READ_tdata[8]),
        .I3(FIFO_READ_tdata[9]),
        .O(\AXI_WDATA_PARITY0_inferred__0/AXI_WDATA_PARITY[1]_INST_0_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h96696996)) 
    \AXI_WDATA_PARITY0_inferred__1/AXI_WDATA_PARITY[2]_INST_0 
       (.I0(FIFO_READ_tdata[21]),
        .I1(FIFO_READ_tdata[20]),
        .I2(FIFO_READ_tdata[23]),
        .I3(FIFO_READ_tdata[22]),
        .I4(\AXI_WDATA_PARITY0_inferred__1/AXI_WDATA_PARITY[2]_INST_0_i_1_n_0 ),
        .O(AXI_WDATA_PARITY[2]));
  LUT4 #(
    .INIT(16'h6996)) 
    \AXI_WDATA_PARITY0_inferred__1/AXI_WDATA_PARITY[2]_INST_0_i_1 
       (.I0(FIFO_READ_tdata[18]),
        .I1(FIFO_READ_tdata[19]),
        .I2(FIFO_READ_tdata[16]),
        .I3(FIFO_READ_tdata[17]),
        .O(\AXI_WDATA_PARITY0_inferred__1/AXI_WDATA_PARITY[2]_INST_0_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h96696996)) 
    \AXI_WDATA_PARITY0_inferred__10/AXI_WDATA_PARITY[11]_INST_0 
       (.I0(FIFO_READ_tdata[93]),
        .I1(FIFO_READ_tdata[92]),
        .I2(FIFO_READ_tdata[95]),
        .I3(FIFO_READ_tdata[94]),
        .I4(\AXI_WDATA_PARITY0_inferred__10/AXI_WDATA_PARITY[11]_INST_0_i_1_n_0 ),
        .O(AXI_WDATA_PARITY[11]));
  LUT4 #(
    .INIT(16'h6996)) 
    \AXI_WDATA_PARITY0_inferred__10/AXI_WDATA_PARITY[11]_INST_0_i_1 
       (.I0(FIFO_READ_tdata[90]),
        .I1(FIFO_READ_tdata[91]),
        .I2(FIFO_READ_tdata[88]),
        .I3(FIFO_READ_tdata[89]),
        .O(\AXI_WDATA_PARITY0_inferred__10/AXI_WDATA_PARITY[11]_INST_0_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h96696996)) 
    \AXI_WDATA_PARITY0_inferred__11/AXI_WDATA_PARITY[12]_INST_0 
       (.I0(FIFO_READ_tdata[101]),
        .I1(FIFO_READ_tdata[100]),
        .I2(FIFO_READ_tdata[103]),
        .I3(FIFO_READ_tdata[102]),
        .I4(\AXI_WDATA_PARITY0_inferred__11/AXI_WDATA_PARITY[12]_INST_0_i_1_n_0 ),
        .O(AXI_WDATA_PARITY[12]));
  LUT4 #(
    .INIT(16'h6996)) 
    \AXI_WDATA_PARITY0_inferred__11/AXI_WDATA_PARITY[12]_INST_0_i_1 
       (.I0(FIFO_READ_tdata[98]),
        .I1(FIFO_READ_tdata[99]),
        .I2(FIFO_READ_tdata[96]),
        .I3(FIFO_READ_tdata[97]),
        .O(\AXI_WDATA_PARITY0_inferred__11/AXI_WDATA_PARITY[12]_INST_0_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h96696996)) 
    \AXI_WDATA_PARITY0_inferred__12/AXI_WDATA_PARITY[13]_INST_0 
       (.I0(FIFO_READ_tdata[109]),
        .I1(FIFO_READ_tdata[108]),
        .I2(FIFO_READ_tdata[111]),
        .I3(FIFO_READ_tdata[110]),
        .I4(\AXI_WDATA_PARITY0_inferred__12/AXI_WDATA_PARITY[13]_INST_0_i_1_n_0 ),
        .O(AXI_WDATA_PARITY[13]));
  LUT4 #(
    .INIT(16'h6996)) 
    \AXI_WDATA_PARITY0_inferred__12/AXI_WDATA_PARITY[13]_INST_0_i_1 
       (.I0(FIFO_READ_tdata[106]),
        .I1(FIFO_READ_tdata[107]),
        .I2(FIFO_READ_tdata[104]),
        .I3(FIFO_READ_tdata[105]),
        .O(\AXI_WDATA_PARITY0_inferred__12/AXI_WDATA_PARITY[13]_INST_0_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h96696996)) 
    \AXI_WDATA_PARITY0_inferred__13/AXI_WDATA_PARITY[14]_INST_0 
       (.I0(FIFO_READ_tdata[117]),
        .I1(FIFO_READ_tdata[116]),
        .I2(FIFO_READ_tdata[119]),
        .I3(FIFO_READ_tdata[118]),
        .I4(\AXI_WDATA_PARITY0_inferred__13/AXI_WDATA_PARITY[14]_INST_0_i_1_n_0 ),
        .O(AXI_WDATA_PARITY[14]));
  LUT4 #(
    .INIT(16'h6996)) 
    \AXI_WDATA_PARITY0_inferred__13/AXI_WDATA_PARITY[14]_INST_0_i_1 
       (.I0(FIFO_READ_tdata[114]),
        .I1(FIFO_READ_tdata[115]),
        .I2(FIFO_READ_tdata[112]),
        .I3(FIFO_READ_tdata[113]),
        .O(\AXI_WDATA_PARITY0_inferred__13/AXI_WDATA_PARITY[14]_INST_0_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h96696996)) 
    \AXI_WDATA_PARITY0_inferred__14/AXI_WDATA_PARITY[15]_INST_0 
       (.I0(FIFO_READ_tdata[125]),
        .I1(FIFO_READ_tdata[124]),
        .I2(FIFO_READ_tdata[127]),
        .I3(FIFO_READ_tdata[126]),
        .I4(\AXI_WDATA_PARITY0_inferred__14/AXI_WDATA_PARITY[15]_INST_0_i_1_n_0 ),
        .O(AXI_WDATA_PARITY[15]));
  LUT4 #(
    .INIT(16'h6996)) 
    \AXI_WDATA_PARITY0_inferred__14/AXI_WDATA_PARITY[15]_INST_0_i_1 
       (.I0(FIFO_READ_tdata[122]),
        .I1(FIFO_READ_tdata[123]),
        .I2(FIFO_READ_tdata[120]),
        .I3(FIFO_READ_tdata[121]),
        .O(\AXI_WDATA_PARITY0_inferred__14/AXI_WDATA_PARITY[15]_INST_0_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h96696996)) 
    \AXI_WDATA_PARITY0_inferred__15/AXI_WDATA_PARITY[16]_INST_0 
       (.I0(FIFO_READ_tdata[133]),
        .I1(FIFO_READ_tdata[132]),
        .I2(FIFO_READ_tdata[135]),
        .I3(FIFO_READ_tdata[134]),
        .I4(\AXI_WDATA_PARITY0_inferred__15/AXI_WDATA_PARITY[16]_INST_0_i_1_n_0 ),
        .O(AXI_WDATA_PARITY[16]));
  LUT4 #(
    .INIT(16'h6996)) 
    \AXI_WDATA_PARITY0_inferred__15/AXI_WDATA_PARITY[16]_INST_0_i_1 
       (.I0(FIFO_READ_tdata[130]),
        .I1(FIFO_READ_tdata[131]),
        .I2(FIFO_READ_tdata[128]),
        .I3(FIFO_READ_tdata[129]),
        .O(\AXI_WDATA_PARITY0_inferred__15/AXI_WDATA_PARITY[16]_INST_0_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h96696996)) 
    \AXI_WDATA_PARITY0_inferred__16/AXI_WDATA_PARITY[17]_INST_0 
       (.I0(FIFO_READ_tdata[141]),
        .I1(FIFO_READ_tdata[140]),
        .I2(FIFO_READ_tdata[143]),
        .I3(FIFO_READ_tdata[142]),
        .I4(\AXI_WDATA_PARITY0_inferred__16/AXI_WDATA_PARITY[17]_INST_0_i_1_n_0 ),
        .O(AXI_WDATA_PARITY[17]));
  LUT4 #(
    .INIT(16'h6996)) 
    \AXI_WDATA_PARITY0_inferred__16/AXI_WDATA_PARITY[17]_INST_0_i_1 
       (.I0(FIFO_READ_tdata[138]),
        .I1(FIFO_READ_tdata[139]),
        .I2(FIFO_READ_tdata[136]),
        .I3(FIFO_READ_tdata[137]),
        .O(\AXI_WDATA_PARITY0_inferred__16/AXI_WDATA_PARITY[17]_INST_0_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h96696996)) 
    \AXI_WDATA_PARITY0_inferred__17/AXI_WDATA_PARITY[18]_INST_0 
       (.I0(FIFO_READ_tdata[149]),
        .I1(FIFO_READ_tdata[148]),
        .I2(FIFO_READ_tdata[151]),
        .I3(FIFO_READ_tdata[150]),
        .I4(\AXI_WDATA_PARITY0_inferred__17/AXI_WDATA_PARITY[18]_INST_0_i_1_n_0 ),
        .O(AXI_WDATA_PARITY[18]));
  LUT4 #(
    .INIT(16'h6996)) 
    \AXI_WDATA_PARITY0_inferred__17/AXI_WDATA_PARITY[18]_INST_0_i_1 
       (.I0(FIFO_READ_tdata[146]),
        .I1(FIFO_READ_tdata[147]),
        .I2(FIFO_READ_tdata[144]),
        .I3(FIFO_READ_tdata[145]),
        .O(\AXI_WDATA_PARITY0_inferred__17/AXI_WDATA_PARITY[18]_INST_0_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h96696996)) 
    \AXI_WDATA_PARITY0_inferred__18/AXI_WDATA_PARITY[19]_INST_0 
       (.I0(FIFO_READ_tdata[157]),
        .I1(FIFO_READ_tdata[156]),
        .I2(FIFO_READ_tdata[159]),
        .I3(FIFO_READ_tdata[158]),
        .I4(\AXI_WDATA_PARITY0_inferred__18/AXI_WDATA_PARITY[19]_INST_0_i_1_n_0 ),
        .O(AXI_WDATA_PARITY[19]));
  LUT4 #(
    .INIT(16'h6996)) 
    \AXI_WDATA_PARITY0_inferred__18/AXI_WDATA_PARITY[19]_INST_0_i_1 
       (.I0(FIFO_READ_tdata[154]),
        .I1(FIFO_READ_tdata[155]),
        .I2(FIFO_READ_tdata[152]),
        .I3(FIFO_READ_tdata[153]),
        .O(\AXI_WDATA_PARITY0_inferred__18/AXI_WDATA_PARITY[19]_INST_0_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h96696996)) 
    \AXI_WDATA_PARITY0_inferred__19/AXI_WDATA_PARITY[20]_INST_0 
       (.I0(FIFO_READ_tdata[165]),
        .I1(FIFO_READ_tdata[164]),
        .I2(FIFO_READ_tdata[167]),
        .I3(FIFO_READ_tdata[166]),
        .I4(\AXI_WDATA_PARITY0_inferred__19/AXI_WDATA_PARITY[20]_INST_0_i_1_n_0 ),
        .O(AXI_WDATA_PARITY[20]));
  LUT4 #(
    .INIT(16'h6996)) 
    \AXI_WDATA_PARITY0_inferred__19/AXI_WDATA_PARITY[20]_INST_0_i_1 
       (.I0(FIFO_READ_tdata[162]),
        .I1(FIFO_READ_tdata[163]),
        .I2(FIFO_READ_tdata[160]),
        .I3(FIFO_READ_tdata[161]),
        .O(\AXI_WDATA_PARITY0_inferred__19/AXI_WDATA_PARITY[20]_INST_0_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h96696996)) 
    \AXI_WDATA_PARITY0_inferred__2/AXI_WDATA_PARITY[3]_INST_0 
       (.I0(FIFO_READ_tdata[29]),
        .I1(FIFO_READ_tdata[28]),
        .I2(FIFO_READ_tdata[31]),
        .I3(FIFO_READ_tdata[30]),
        .I4(\AXI_WDATA_PARITY0_inferred__2/AXI_WDATA_PARITY[3]_INST_0_i_1_n_0 ),
        .O(AXI_WDATA_PARITY[3]));
  LUT4 #(
    .INIT(16'h6996)) 
    \AXI_WDATA_PARITY0_inferred__2/AXI_WDATA_PARITY[3]_INST_0_i_1 
       (.I0(FIFO_READ_tdata[26]),
        .I1(FIFO_READ_tdata[27]),
        .I2(FIFO_READ_tdata[24]),
        .I3(FIFO_READ_tdata[25]),
        .O(\AXI_WDATA_PARITY0_inferred__2/AXI_WDATA_PARITY[3]_INST_0_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h96696996)) 
    \AXI_WDATA_PARITY0_inferred__20/AXI_WDATA_PARITY[21]_INST_0 
       (.I0(FIFO_READ_tdata[173]),
        .I1(FIFO_READ_tdata[172]),
        .I2(FIFO_READ_tdata[175]),
        .I3(FIFO_READ_tdata[174]),
        .I4(\AXI_WDATA_PARITY0_inferred__20/AXI_WDATA_PARITY[21]_INST_0_i_1_n_0 ),
        .O(AXI_WDATA_PARITY[21]));
  LUT4 #(
    .INIT(16'h6996)) 
    \AXI_WDATA_PARITY0_inferred__20/AXI_WDATA_PARITY[21]_INST_0_i_1 
       (.I0(FIFO_READ_tdata[170]),
        .I1(FIFO_READ_tdata[171]),
        .I2(FIFO_READ_tdata[168]),
        .I3(FIFO_READ_tdata[169]),
        .O(\AXI_WDATA_PARITY0_inferred__20/AXI_WDATA_PARITY[21]_INST_0_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h96696996)) 
    \AXI_WDATA_PARITY0_inferred__21/AXI_WDATA_PARITY[22]_INST_0 
       (.I0(FIFO_READ_tdata[181]),
        .I1(FIFO_READ_tdata[180]),
        .I2(FIFO_READ_tdata[183]),
        .I3(FIFO_READ_tdata[182]),
        .I4(\AXI_WDATA_PARITY0_inferred__21/AXI_WDATA_PARITY[22]_INST_0_i_1_n_0 ),
        .O(AXI_WDATA_PARITY[22]));
  LUT4 #(
    .INIT(16'h6996)) 
    \AXI_WDATA_PARITY0_inferred__21/AXI_WDATA_PARITY[22]_INST_0_i_1 
       (.I0(FIFO_READ_tdata[178]),
        .I1(FIFO_READ_tdata[179]),
        .I2(FIFO_READ_tdata[176]),
        .I3(FIFO_READ_tdata[177]),
        .O(\AXI_WDATA_PARITY0_inferred__21/AXI_WDATA_PARITY[22]_INST_0_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h96696996)) 
    \AXI_WDATA_PARITY0_inferred__22/AXI_WDATA_PARITY[23]_INST_0 
       (.I0(FIFO_READ_tdata[189]),
        .I1(FIFO_READ_tdata[188]),
        .I2(FIFO_READ_tdata[191]),
        .I3(FIFO_READ_tdata[190]),
        .I4(\AXI_WDATA_PARITY0_inferred__22/AXI_WDATA_PARITY[23]_INST_0_i_1_n_0 ),
        .O(AXI_WDATA_PARITY[23]));
  LUT4 #(
    .INIT(16'h6996)) 
    \AXI_WDATA_PARITY0_inferred__22/AXI_WDATA_PARITY[23]_INST_0_i_1 
       (.I0(FIFO_READ_tdata[186]),
        .I1(FIFO_READ_tdata[187]),
        .I2(FIFO_READ_tdata[184]),
        .I3(FIFO_READ_tdata[185]),
        .O(\AXI_WDATA_PARITY0_inferred__22/AXI_WDATA_PARITY[23]_INST_0_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h96696996)) 
    \AXI_WDATA_PARITY0_inferred__23/AXI_WDATA_PARITY[24]_INST_0 
       (.I0(FIFO_READ_tdata[197]),
        .I1(FIFO_READ_tdata[196]),
        .I2(FIFO_READ_tdata[199]),
        .I3(FIFO_READ_tdata[198]),
        .I4(\AXI_WDATA_PARITY0_inferred__23/AXI_WDATA_PARITY[24]_INST_0_i_1_n_0 ),
        .O(AXI_WDATA_PARITY[24]));
  LUT4 #(
    .INIT(16'h6996)) 
    \AXI_WDATA_PARITY0_inferred__23/AXI_WDATA_PARITY[24]_INST_0_i_1 
       (.I0(FIFO_READ_tdata[194]),
        .I1(FIFO_READ_tdata[195]),
        .I2(FIFO_READ_tdata[192]),
        .I3(FIFO_READ_tdata[193]),
        .O(\AXI_WDATA_PARITY0_inferred__23/AXI_WDATA_PARITY[24]_INST_0_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h96696996)) 
    \AXI_WDATA_PARITY0_inferred__24/AXI_WDATA_PARITY[25]_INST_0 
       (.I0(FIFO_READ_tdata[205]),
        .I1(FIFO_READ_tdata[204]),
        .I2(FIFO_READ_tdata[207]),
        .I3(FIFO_READ_tdata[206]),
        .I4(\AXI_WDATA_PARITY0_inferred__24/AXI_WDATA_PARITY[25]_INST_0_i_1_n_0 ),
        .O(AXI_WDATA_PARITY[25]));
  LUT4 #(
    .INIT(16'h6996)) 
    \AXI_WDATA_PARITY0_inferred__24/AXI_WDATA_PARITY[25]_INST_0_i_1 
       (.I0(FIFO_READ_tdata[202]),
        .I1(FIFO_READ_tdata[203]),
        .I2(FIFO_READ_tdata[200]),
        .I3(FIFO_READ_tdata[201]),
        .O(\AXI_WDATA_PARITY0_inferred__24/AXI_WDATA_PARITY[25]_INST_0_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h96696996)) 
    \AXI_WDATA_PARITY0_inferred__25/AXI_WDATA_PARITY[26]_INST_0 
       (.I0(FIFO_READ_tdata[213]),
        .I1(FIFO_READ_tdata[212]),
        .I2(FIFO_READ_tdata[215]),
        .I3(FIFO_READ_tdata[214]),
        .I4(\AXI_WDATA_PARITY0_inferred__25/AXI_WDATA_PARITY[26]_INST_0_i_1_n_0 ),
        .O(AXI_WDATA_PARITY[26]));
  LUT4 #(
    .INIT(16'h6996)) 
    \AXI_WDATA_PARITY0_inferred__25/AXI_WDATA_PARITY[26]_INST_0_i_1 
       (.I0(FIFO_READ_tdata[210]),
        .I1(FIFO_READ_tdata[211]),
        .I2(FIFO_READ_tdata[208]),
        .I3(FIFO_READ_tdata[209]),
        .O(\AXI_WDATA_PARITY0_inferred__25/AXI_WDATA_PARITY[26]_INST_0_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h96696996)) 
    \AXI_WDATA_PARITY0_inferred__26/AXI_WDATA_PARITY[27]_INST_0 
       (.I0(FIFO_READ_tdata[221]),
        .I1(FIFO_READ_tdata[220]),
        .I2(FIFO_READ_tdata[223]),
        .I3(FIFO_READ_tdata[222]),
        .I4(\AXI_WDATA_PARITY0_inferred__26/AXI_WDATA_PARITY[27]_INST_0_i_1_n_0 ),
        .O(AXI_WDATA_PARITY[27]));
  LUT4 #(
    .INIT(16'h6996)) 
    \AXI_WDATA_PARITY0_inferred__26/AXI_WDATA_PARITY[27]_INST_0_i_1 
       (.I0(FIFO_READ_tdata[218]),
        .I1(FIFO_READ_tdata[219]),
        .I2(FIFO_READ_tdata[216]),
        .I3(FIFO_READ_tdata[217]),
        .O(\AXI_WDATA_PARITY0_inferred__26/AXI_WDATA_PARITY[27]_INST_0_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h96696996)) 
    \AXI_WDATA_PARITY0_inferred__27/AXI_WDATA_PARITY[28]_INST_0 
       (.I0(FIFO_READ_tdata[229]),
        .I1(FIFO_READ_tdata[228]),
        .I2(FIFO_READ_tdata[231]),
        .I3(FIFO_READ_tdata[230]),
        .I4(\AXI_WDATA_PARITY0_inferred__27/AXI_WDATA_PARITY[28]_INST_0_i_1_n_0 ),
        .O(AXI_WDATA_PARITY[28]));
  LUT4 #(
    .INIT(16'h6996)) 
    \AXI_WDATA_PARITY0_inferred__27/AXI_WDATA_PARITY[28]_INST_0_i_1 
       (.I0(FIFO_READ_tdata[226]),
        .I1(FIFO_READ_tdata[227]),
        .I2(FIFO_READ_tdata[224]),
        .I3(FIFO_READ_tdata[225]),
        .O(\AXI_WDATA_PARITY0_inferred__27/AXI_WDATA_PARITY[28]_INST_0_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h96696996)) 
    \AXI_WDATA_PARITY0_inferred__28/AXI_WDATA_PARITY[29]_INST_0 
       (.I0(FIFO_READ_tdata[237]),
        .I1(FIFO_READ_tdata[236]),
        .I2(FIFO_READ_tdata[239]),
        .I3(FIFO_READ_tdata[238]),
        .I4(\AXI_WDATA_PARITY0_inferred__28/AXI_WDATA_PARITY[29]_INST_0_i_1_n_0 ),
        .O(AXI_WDATA_PARITY[29]));
  LUT4 #(
    .INIT(16'h6996)) 
    \AXI_WDATA_PARITY0_inferred__28/AXI_WDATA_PARITY[29]_INST_0_i_1 
       (.I0(FIFO_READ_tdata[234]),
        .I1(FIFO_READ_tdata[235]),
        .I2(FIFO_READ_tdata[232]),
        .I3(FIFO_READ_tdata[233]),
        .O(\AXI_WDATA_PARITY0_inferred__28/AXI_WDATA_PARITY[29]_INST_0_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h96696996)) 
    \AXI_WDATA_PARITY0_inferred__29/AXI_WDATA_PARITY[30]_INST_0 
       (.I0(FIFO_READ_tdata[245]),
        .I1(FIFO_READ_tdata[244]),
        .I2(FIFO_READ_tdata[247]),
        .I3(FIFO_READ_tdata[246]),
        .I4(\AXI_WDATA_PARITY0_inferred__29/AXI_WDATA_PARITY[30]_INST_0_i_1_n_0 ),
        .O(AXI_WDATA_PARITY[30]));
  LUT4 #(
    .INIT(16'h6996)) 
    \AXI_WDATA_PARITY0_inferred__29/AXI_WDATA_PARITY[30]_INST_0_i_1 
       (.I0(FIFO_READ_tdata[242]),
        .I1(FIFO_READ_tdata[243]),
        .I2(FIFO_READ_tdata[240]),
        .I3(FIFO_READ_tdata[241]),
        .O(\AXI_WDATA_PARITY0_inferred__29/AXI_WDATA_PARITY[30]_INST_0_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h96696996)) 
    \AXI_WDATA_PARITY0_inferred__3/AXI_WDATA_PARITY[4]_INST_0 
       (.I0(FIFO_READ_tdata[37]),
        .I1(FIFO_READ_tdata[36]),
        .I2(FIFO_READ_tdata[39]),
        .I3(FIFO_READ_tdata[38]),
        .I4(\AXI_WDATA_PARITY0_inferred__3/AXI_WDATA_PARITY[4]_INST_0_i_1_n_0 ),
        .O(AXI_WDATA_PARITY[4]));
  LUT4 #(
    .INIT(16'h6996)) 
    \AXI_WDATA_PARITY0_inferred__3/AXI_WDATA_PARITY[4]_INST_0_i_1 
       (.I0(FIFO_READ_tdata[34]),
        .I1(FIFO_READ_tdata[35]),
        .I2(FIFO_READ_tdata[32]),
        .I3(FIFO_READ_tdata[33]),
        .O(\AXI_WDATA_PARITY0_inferred__3/AXI_WDATA_PARITY[4]_INST_0_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h96696996)) 
    \AXI_WDATA_PARITY0_inferred__30/AXI_WDATA_PARITY[31]_INST_0 
       (.I0(FIFO_READ_tdata[253]),
        .I1(FIFO_READ_tdata[252]),
        .I2(FIFO_READ_tdata[255]),
        .I3(FIFO_READ_tdata[254]),
        .I4(\AXI_WDATA_PARITY0_inferred__30/AXI_WDATA_PARITY[31]_INST_0_i_1_n_0 ),
        .O(AXI_WDATA_PARITY[31]));
  LUT4 #(
    .INIT(16'h6996)) 
    \AXI_WDATA_PARITY0_inferred__30/AXI_WDATA_PARITY[31]_INST_0_i_1 
       (.I0(FIFO_READ_tdata[250]),
        .I1(FIFO_READ_tdata[251]),
        .I2(FIFO_READ_tdata[248]),
        .I3(FIFO_READ_tdata[249]),
        .O(\AXI_WDATA_PARITY0_inferred__30/AXI_WDATA_PARITY[31]_INST_0_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h96696996)) 
    \AXI_WDATA_PARITY0_inferred__4/AXI_WDATA_PARITY[5]_INST_0 
       (.I0(FIFO_READ_tdata[45]),
        .I1(FIFO_READ_tdata[44]),
        .I2(FIFO_READ_tdata[47]),
        .I3(FIFO_READ_tdata[46]),
        .I4(\AXI_WDATA_PARITY0_inferred__4/AXI_WDATA_PARITY[5]_INST_0_i_1_n_0 ),
        .O(AXI_WDATA_PARITY[5]));
  LUT4 #(
    .INIT(16'h6996)) 
    \AXI_WDATA_PARITY0_inferred__4/AXI_WDATA_PARITY[5]_INST_0_i_1 
       (.I0(FIFO_READ_tdata[42]),
        .I1(FIFO_READ_tdata[43]),
        .I2(FIFO_READ_tdata[40]),
        .I3(FIFO_READ_tdata[41]),
        .O(\AXI_WDATA_PARITY0_inferred__4/AXI_WDATA_PARITY[5]_INST_0_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h96696996)) 
    \AXI_WDATA_PARITY0_inferred__5/AXI_WDATA_PARITY[6]_INST_0 
       (.I0(FIFO_READ_tdata[53]),
        .I1(FIFO_READ_tdata[52]),
        .I2(FIFO_READ_tdata[55]),
        .I3(FIFO_READ_tdata[54]),
        .I4(\AXI_WDATA_PARITY0_inferred__5/AXI_WDATA_PARITY[6]_INST_0_i_1_n_0 ),
        .O(AXI_WDATA_PARITY[6]));
  LUT4 #(
    .INIT(16'h6996)) 
    \AXI_WDATA_PARITY0_inferred__5/AXI_WDATA_PARITY[6]_INST_0_i_1 
       (.I0(FIFO_READ_tdata[50]),
        .I1(FIFO_READ_tdata[51]),
        .I2(FIFO_READ_tdata[48]),
        .I3(FIFO_READ_tdata[49]),
        .O(\AXI_WDATA_PARITY0_inferred__5/AXI_WDATA_PARITY[6]_INST_0_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h96696996)) 
    \AXI_WDATA_PARITY0_inferred__6/AXI_WDATA_PARITY[7]_INST_0 
       (.I0(FIFO_READ_tdata[61]),
        .I1(FIFO_READ_tdata[60]),
        .I2(FIFO_READ_tdata[63]),
        .I3(FIFO_READ_tdata[62]),
        .I4(\AXI_WDATA_PARITY0_inferred__6/AXI_WDATA_PARITY[7]_INST_0_i_1_n_0 ),
        .O(AXI_WDATA_PARITY[7]));
  LUT4 #(
    .INIT(16'h6996)) 
    \AXI_WDATA_PARITY0_inferred__6/AXI_WDATA_PARITY[7]_INST_0_i_1 
       (.I0(FIFO_READ_tdata[58]),
        .I1(FIFO_READ_tdata[59]),
        .I2(FIFO_READ_tdata[56]),
        .I3(FIFO_READ_tdata[57]),
        .O(\AXI_WDATA_PARITY0_inferred__6/AXI_WDATA_PARITY[7]_INST_0_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h96696996)) 
    \AXI_WDATA_PARITY0_inferred__7/AXI_WDATA_PARITY[8]_INST_0 
       (.I0(FIFO_READ_tdata[69]),
        .I1(FIFO_READ_tdata[68]),
        .I2(FIFO_READ_tdata[71]),
        .I3(FIFO_READ_tdata[70]),
        .I4(\AXI_WDATA_PARITY0_inferred__7/AXI_WDATA_PARITY[8]_INST_0_i_1_n_0 ),
        .O(AXI_WDATA_PARITY[8]));
  LUT4 #(
    .INIT(16'h6996)) 
    \AXI_WDATA_PARITY0_inferred__7/AXI_WDATA_PARITY[8]_INST_0_i_1 
       (.I0(FIFO_READ_tdata[66]),
        .I1(FIFO_READ_tdata[67]),
        .I2(FIFO_READ_tdata[64]),
        .I3(FIFO_READ_tdata[65]),
        .O(\AXI_WDATA_PARITY0_inferred__7/AXI_WDATA_PARITY[8]_INST_0_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h96696996)) 
    \AXI_WDATA_PARITY0_inferred__8/AXI_WDATA_PARITY[9]_INST_0 
       (.I0(FIFO_READ_tdata[77]),
        .I1(FIFO_READ_tdata[76]),
        .I2(FIFO_READ_tdata[79]),
        .I3(FIFO_READ_tdata[78]),
        .I4(\AXI_WDATA_PARITY0_inferred__8/AXI_WDATA_PARITY[9]_INST_0_i_1_n_0 ),
        .O(AXI_WDATA_PARITY[9]));
  LUT4 #(
    .INIT(16'h6996)) 
    \AXI_WDATA_PARITY0_inferred__8/AXI_WDATA_PARITY[9]_INST_0_i_1 
       (.I0(FIFO_READ_tdata[74]),
        .I1(FIFO_READ_tdata[75]),
        .I2(FIFO_READ_tdata[72]),
        .I3(FIFO_READ_tdata[73]),
        .O(\AXI_WDATA_PARITY0_inferred__8/AXI_WDATA_PARITY[9]_INST_0_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h96696996)) 
    \AXI_WDATA_PARITY0_inferred__9/AXI_WDATA_PARITY[10]_INST_0 
       (.I0(FIFO_READ_tdata[85]),
        .I1(FIFO_READ_tdata[84]),
        .I2(FIFO_READ_tdata[87]),
        .I3(FIFO_READ_tdata[86]),
        .I4(\AXI_WDATA_PARITY0_inferred__9/AXI_WDATA_PARITY[10]_INST_0_i_1_n_0 ),
        .O(AXI_WDATA_PARITY[10]));
  LUT4 #(
    .INIT(16'h6996)) 
    \AXI_WDATA_PARITY0_inferred__9/AXI_WDATA_PARITY[10]_INST_0_i_1 
       (.I0(FIFO_READ_tdata[82]),
        .I1(FIFO_READ_tdata[83]),
        .I2(FIFO_READ_tdata[80]),
        .I3(FIFO_READ_tdata[81]),
        .O(\AXI_WDATA_PARITY0_inferred__9/AXI_WDATA_PARITY[10]_INST_0_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h96696996)) 
    \AXI_WDATA_PARITY[0]_INST_0 
       (.I0(FIFO_READ_tdata[5]),
        .I1(FIFO_READ_tdata[4]),
        .I2(FIFO_READ_tdata[7]),
        .I3(FIFO_READ_tdata[6]),
        .I4(\AXI_WDATA_PARITY[0]_INST_0_i_1_n_0 ),
        .O(AXI_WDATA_PARITY[0]));
  LUT4 #(
    .INIT(16'h6996)) 
    \AXI_WDATA_PARITY[0]_INST_0_i_1 
       (.I0(FIFO_READ_tdata[2]),
        .I1(FIFO_READ_tdata[3]),
        .I2(FIFO_READ_tdata[0]),
        .I3(FIFO_READ_tdata[1]),
        .O(\AXI_WDATA_PARITY[0]_INST_0_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF8000)) 
    AXI_WLAST_INST_0
       (.I0(writedata_count_reg[2]),
        .I1(writedata_count_reg[3]),
        .I2(writedata_count_reg[0]),
        .I3(writedata_count_reg[1]),
        .I4(writedata_FSM_next1),
        .O(AXI_WLAST));
  CARRY8 FIFO_WRITE_tlast0_carry
       (.CI(1'b1),
        .CI_TOP(1'b0),
        .CO({FIFO_WRITE_tlast0_carry_n_0,FIFO_WRITE_tlast0_carry_n_1,FIFO_WRITE_tlast0_carry_n_2,FIFO_WRITE_tlast0_carry_n_3,FIFO_WRITE_tlast0_carry_n_4,FIFO_WRITE_tlast0_carry_n_5,FIFO_WRITE_tlast0_carry_n_6,FIFO_WRITE_tlast0_carry_n_7}),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_FIFO_WRITE_tlast0_carry_O_UNCONNECTED[7:0]),
        .S({FIFO_WRITE_tlast0_carry_i_1_n_0,FIFO_WRITE_tlast0_carry_i_2_n_0,FIFO_WRITE_tlast0_carry_i_3_n_0,FIFO_WRITE_tlast0_carry_i_4_n_0,FIFO_WRITE_tlast0_carry_i_5_n_0,FIFO_WRITE_tlast0_carry_i_6_n_0,FIFO_WRITE_tlast0_carry_i_7_n_0,FIFO_WRITE_tlast0_carry_i_8_n_0}));
  CARRY8 FIFO_WRITE_tlast0_carry__0
       (.CI(FIFO_WRITE_tlast0_carry_n_0),
        .CI_TOP(1'b0),
        .CO({NLW_FIFO_WRITE_tlast0_carry__0_CO_UNCONNECTED[7:3],FIFO_WRITE_tlast0,FIFO_WRITE_tlast0_carry__0_n_6,FIFO_WRITE_tlast0_carry__0_n_7}),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_FIFO_WRITE_tlast0_carry__0_O_UNCONNECTED[7:0]),
        .S({1'b0,1'b0,1'b0,1'b0,1'b0,FIFO_WRITE_tlast0_carry__0_i_1_n_0,FIFO_WRITE_tlast0_carry__0_i_2_n_0,FIFO_WRITE_tlast0_carry__0_i_3_n_0}));
  LUT4 #(
    .INIT(16'h9009)) 
    FIFO_WRITE_tlast0_carry__0_i_1
       (.I0(readdata_count_reg[30]),
        .I1(FIFO_WRITE_tlast1[30]),
        .I2(FIFO_WRITE_tlast1[31]),
        .I3(readdata_count_reg[31]),
        .O(FIFO_WRITE_tlast0_carry__0_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    FIFO_WRITE_tlast0_carry__0_i_10
       (.I0(read_blockamount[26]),
        .O(FIFO_WRITE_tlast0_carry__0_i_10_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    FIFO_WRITE_tlast0_carry__0_i_11
       (.I0(read_blockamount[25]),
        .O(FIFO_WRITE_tlast0_carry__0_i_11_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    FIFO_WRITE_tlast0_carry__0_i_2
       (.I0(readdata_count_reg[28]),
        .I1(FIFO_WRITE_tlast1[28]),
        .I2(readdata_count_reg[27]),
        .I3(FIFO_WRITE_tlast1[27]),
        .I4(FIFO_WRITE_tlast1[29]),
        .I5(readdata_count_reg[29]),
        .O(FIFO_WRITE_tlast0_carry__0_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    FIFO_WRITE_tlast0_carry__0_i_3
       (.I0(readdata_count_reg[25]),
        .I1(FIFO_WRITE_tlast1[25]),
        .I2(readdata_count_reg[24]),
        .I3(FIFO_WRITE_tlast1[24]),
        .I4(FIFO_WRITE_tlast1[26]),
        .I5(readdata_count_reg[26]),
        .O(FIFO_WRITE_tlast0_carry__0_i_3_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY8 FIFO_WRITE_tlast0_carry__0_i_4
       (.CI(FIFO_WRITE_tlast0_carry_i_9_n_0),
        .CI_TOP(1'b0),
        .CO({NLW_FIFO_WRITE_tlast0_carry__0_i_4_CO_UNCONNECTED[7:6],FIFO_WRITE_tlast0_carry__0_i_4_n_2,FIFO_WRITE_tlast0_carry__0_i_4_n_3,FIFO_WRITE_tlast0_carry__0_i_4_n_4,FIFO_WRITE_tlast0_carry__0_i_4_n_5,FIFO_WRITE_tlast0_carry__0_i_4_n_6,FIFO_WRITE_tlast0_carry__0_i_4_n_7}),
        .DI({1'b0,1'b0,read_blockamount[30:25]}),
        .O({NLW_FIFO_WRITE_tlast0_carry__0_i_4_O_UNCONNECTED[7],FIFO_WRITE_tlast1[31:25]}),
        .S({1'b0,FIFO_WRITE_tlast0_carry__0_i_5_n_0,FIFO_WRITE_tlast0_carry__0_i_6_n_0,FIFO_WRITE_tlast0_carry__0_i_7_n_0,FIFO_WRITE_tlast0_carry__0_i_8_n_0,FIFO_WRITE_tlast0_carry__0_i_9_n_0,FIFO_WRITE_tlast0_carry__0_i_10_n_0,FIFO_WRITE_tlast0_carry__0_i_11_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    FIFO_WRITE_tlast0_carry__0_i_5
       (.I0(read_blockamount[31]),
        .O(FIFO_WRITE_tlast0_carry__0_i_5_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    FIFO_WRITE_tlast0_carry__0_i_6
       (.I0(read_blockamount[30]),
        .O(FIFO_WRITE_tlast0_carry__0_i_6_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    FIFO_WRITE_tlast0_carry__0_i_7
       (.I0(read_blockamount[29]),
        .O(FIFO_WRITE_tlast0_carry__0_i_7_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    FIFO_WRITE_tlast0_carry__0_i_8
       (.I0(read_blockamount[28]),
        .O(FIFO_WRITE_tlast0_carry__0_i_8_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    FIFO_WRITE_tlast0_carry__0_i_9
       (.I0(read_blockamount[27]),
        .O(FIFO_WRITE_tlast0_carry__0_i_9_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    FIFO_WRITE_tlast0_carry_i_1
       (.I0(readdata_count_reg[22]),
        .I1(FIFO_WRITE_tlast1[22]),
        .I2(readdata_count_reg[21]),
        .I3(FIFO_WRITE_tlast1[21]),
        .I4(FIFO_WRITE_tlast1[23]),
        .I5(readdata_count_reg[23]),
        .O(FIFO_WRITE_tlast0_carry_i_1_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY8 FIFO_WRITE_tlast0_carry_i_10
       (.CI(FIFO_WRITE_tlast0_carry_i_11_n_0),
        .CI_TOP(1'b0),
        .CO({FIFO_WRITE_tlast0_carry_i_10_n_0,FIFO_WRITE_tlast0_carry_i_10_n_1,FIFO_WRITE_tlast0_carry_i_10_n_2,FIFO_WRITE_tlast0_carry_i_10_n_3,FIFO_WRITE_tlast0_carry_i_10_n_4,FIFO_WRITE_tlast0_carry_i_10_n_5,FIFO_WRITE_tlast0_carry_i_10_n_6,FIFO_WRITE_tlast0_carry_i_10_n_7}),
        .DI(read_blockamount[16:9]),
        .O(FIFO_WRITE_tlast1[16:9]),
        .S({FIFO_WRITE_tlast0_carry_i_20_n_0,FIFO_WRITE_tlast0_carry_i_21_n_0,FIFO_WRITE_tlast0_carry_i_22_n_0,FIFO_WRITE_tlast0_carry_i_23_n_0,FIFO_WRITE_tlast0_carry_i_24_n_0,FIFO_WRITE_tlast0_carry_i_25_n_0,FIFO_WRITE_tlast0_carry_i_26_n_0,FIFO_WRITE_tlast0_carry_i_27_n_0}));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY8 FIFO_WRITE_tlast0_carry_i_11
       (.CI(read_blockamount[0]),
        .CI_TOP(1'b0),
        .CO({FIFO_WRITE_tlast0_carry_i_11_n_0,FIFO_WRITE_tlast0_carry_i_11_n_1,FIFO_WRITE_tlast0_carry_i_11_n_2,FIFO_WRITE_tlast0_carry_i_11_n_3,FIFO_WRITE_tlast0_carry_i_11_n_4,FIFO_WRITE_tlast0_carry_i_11_n_5,FIFO_WRITE_tlast0_carry_i_11_n_6,FIFO_WRITE_tlast0_carry_i_11_n_7}),
        .DI(read_blockamount[8:1]),
        .O(FIFO_WRITE_tlast1[8:1]),
        .S({FIFO_WRITE_tlast0_carry_i_28_n_0,FIFO_WRITE_tlast0_carry_i_29_n_0,FIFO_WRITE_tlast0_carry_i_30_n_0,FIFO_WRITE_tlast0_carry_i_31_n_0,FIFO_WRITE_tlast0_carry_i_32_n_0,FIFO_WRITE_tlast0_carry_i_33_n_0,FIFO_WRITE_tlast0_carry_i_34_n_0,FIFO_WRITE_tlast0_carry_i_35_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    FIFO_WRITE_tlast0_carry_i_12
       (.I0(read_blockamount[24]),
        .O(FIFO_WRITE_tlast0_carry_i_12_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    FIFO_WRITE_tlast0_carry_i_13
       (.I0(read_blockamount[23]),
        .O(FIFO_WRITE_tlast0_carry_i_13_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    FIFO_WRITE_tlast0_carry_i_14
       (.I0(read_blockamount[22]),
        .O(FIFO_WRITE_tlast0_carry_i_14_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    FIFO_WRITE_tlast0_carry_i_15
       (.I0(read_blockamount[21]),
        .O(FIFO_WRITE_tlast0_carry_i_15_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    FIFO_WRITE_tlast0_carry_i_16
       (.I0(read_blockamount[20]),
        .O(FIFO_WRITE_tlast0_carry_i_16_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    FIFO_WRITE_tlast0_carry_i_17
       (.I0(read_blockamount[19]),
        .O(FIFO_WRITE_tlast0_carry_i_17_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    FIFO_WRITE_tlast0_carry_i_18
       (.I0(read_blockamount[18]),
        .O(FIFO_WRITE_tlast0_carry_i_18_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    FIFO_WRITE_tlast0_carry_i_19
       (.I0(read_blockamount[17]),
        .O(FIFO_WRITE_tlast0_carry_i_19_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    FIFO_WRITE_tlast0_carry_i_2
       (.I0(readdata_count_reg[19]),
        .I1(FIFO_WRITE_tlast1[19]),
        .I2(readdata_count_reg[18]),
        .I3(FIFO_WRITE_tlast1[18]),
        .I4(FIFO_WRITE_tlast1[20]),
        .I5(readdata_count_reg[20]),
        .O(FIFO_WRITE_tlast0_carry_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    FIFO_WRITE_tlast0_carry_i_20
       (.I0(read_blockamount[16]),
        .O(FIFO_WRITE_tlast0_carry_i_20_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    FIFO_WRITE_tlast0_carry_i_21
       (.I0(read_blockamount[15]),
        .O(FIFO_WRITE_tlast0_carry_i_21_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    FIFO_WRITE_tlast0_carry_i_22
       (.I0(read_blockamount[14]),
        .O(FIFO_WRITE_tlast0_carry_i_22_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    FIFO_WRITE_tlast0_carry_i_23
       (.I0(read_blockamount[13]),
        .O(FIFO_WRITE_tlast0_carry_i_23_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    FIFO_WRITE_tlast0_carry_i_24
       (.I0(read_blockamount[12]),
        .O(FIFO_WRITE_tlast0_carry_i_24_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    FIFO_WRITE_tlast0_carry_i_25
       (.I0(read_blockamount[11]),
        .O(FIFO_WRITE_tlast0_carry_i_25_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    FIFO_WRITE_tlast0_carry_i_26
       (.I0(read_blockamount[10]),
        .O(FIFO_WRITE_tlast0_carry_i_26_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    FIFO_WRITE_tlast0_carry_i_27
       (.I0(read_blockamount[9]),
        .O(FIFO_WRITE_tlast0_carry_i_27_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    FIFO_WRITE_tlast0_carry_i_28
       (.I0(read_blockamount[8]),
        .O(FIFO_WRITE_tlast0_carry_i_28_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    FIFO_WRITE_tlast0_carry_i_29
       (.I0(read_blockamount[7]),
        .O(FIFO_WRITE_tlast0_carry_i_29_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    FIFO_WRITE_tlast0_carry_i_3
       (.I0(readdata_count_reg[16]),
        .I1(FIFO_WRITE_tlast1[16]),
        .I2(readdata_count_reg[15]),
        .I3(FIFO_WRITE_tlast1[15]),
        .I4(FIFO_WRITE_tlast1[17]),
        .I5(readdata_count_reg[17]),
        .O(FIFO_WRITE_tlast0_carry_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    FIFO_WRITE_tlast0_carry_i_30
       (.I0(read_blockamount[6]),
        .O(FIFO_WRITE_tlast0_carry_i_30_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    FIFO_WRITE_tlast0_carry_i_31
       (.I0(read_blockamount[5]),
        .O(FIFO_WRITE_tlast0_carry_i_31_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    FIFO_WRITE_tlast0_carry_i_32
       (.I0(read_blockamount[4]),
        .O(FIFO_WRITE_tlast0_carry_i_32_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    FIFO_WRITE_tlast0_carry_i_33
       (.I0(read_blockamount[3]),
        .O(FIFO_WRITE_tlast0_carry_i_33_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    FIFO_WRITE_tlast0_carry_i_34
       (.I0(read_blockamount[2]),
        .O(FIFO_WRITE_tlast0_carry_i_34_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    FIFO_WRITE_tlast0_carry_i_35
       (.I0(read_blockamount[1]),
        .O(FIFO_WRITE_tlast0_carry_i_35_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    FIFO_WRITE_tlast0_carry_i_4
       (.I0(readdata_count_reg[13]),
        .I1(FIFO_WRITE_tlast1[13]),
        .I2(readdata_count_reg[12]),
        .I3(FIFO_WRITE_tlast1[12]),
        .I4(FIFO_WRITE_tlast1[14]),
        .I5(readdata_count_reg[14]),
        .O(FIFO_WRITE_tlast0_carry_i_4_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    FIFO_WRITE_tlast0_carry_i_5
       (.I0(readdata_count_reg[10]),
        .I1(FIFO_WRITE_tlast1[10]),
        .I2(readdata_count_reg[9]),
        .I3(FIFO_WRITE_tlast1[9]),
        .I4(FIFO_WRITE_tlast1[11]),
        .I5(readdata_count_reg[11]),
        .O(FIFO_WRITE_tlast0_carry_i_5_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    FIFO_WRITE_tlast0_carry_i_6
       (.I0(readdata_count_reg[7]),
        .I1(FIFO_WRITE_tlast1[7]),
        .I2(readdata_count_reg[6]),
        .I3(FIFO_WRITE_tlast1[6]),
        .I4(FIFO_WRITE_tlast1[8]),
        .I5(readdata_count_reg[8]),
        .O(FIFO_WRITE_tlast0_carry_i_6_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    FIFO_WRITE_tlast0_carry_i_7
       (.I0(readdata_count_reg[4]),
        .I1(FIFO_WRITE_tlast1[4]),
        .I2(readdata_count_reg[3]),
        .I3(FIFO_WRITE_tlast1[3]),
        .I4(FIFO_WRITE_tlast1[5]),
        .I5(readdata_count_reg[5]),
        .O(FIFO_WRITE_tlast0_carry_i_7_n_0));
  LUT6 #(
    .INIT(64'h0990000000000990)) 
    FIFO_WRITE_tlast0_carry_i_8
       (.I0(readdata_count_reg[1]),
        .I1(FIFO_WRITE_tlast1[1]),
        .I2(read_blockamount[0]),
        .I3(readdata_count_reg[0]),
        .I4(FIFO_WRITE_tlast1[2]),
        .I5(readdata_count_reg[2]),
        .O(FIFO_WRITE_tlast0_carry_i_8_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY8 FIFO_WRITE_tlast0_carry_i_9
       (.CI(FIFO_WRITE_tlast0_carry_i_10_n_0),
        .CI_TOP(1'b0),
        .CO({FIFO_WRITE_tlast0_carry_i_9_n_0,FIFO_WRITE_tlast0_carry_i_9_n_1,FIFO_WRITE_tlast0_carry_i_9_n_2,FIFO_WRITE_tlast0_carry_i_9_n_3,FIFO_WRITE_tlast0_carry_i_9_n_4,FIFO_WRITE_tlast0_carry_i_9_n_5,FIFO_WRITE_tlast0_carry_i_9_n_6,FIFO_WRITE_tlast0_carry_i_9_n_7}),
        .DI(read_blockamount[24:17]),
        .O(FIFO_WRITE_tlast1[24:17]),
        .S({FIFO_WRITE_tlast0_carry_i_12_n_0,FIFO_WRITE_tlast0_carry_i_13_n_0,FIFO_WRITE_tlast0_carry_i_14_n_0,FIFO_WRITE_tlast0_carry_i_15_n_0,FIFO_WRITE_tlast0_carry_i_16_n_0,FIFO_WRITE_tlast0_carry_i_17_n_0,FIFO_WRITE_tlast0_carry_i_18_n_0,FIFO_WRITE_tlast0_carry_i_19_n_0}));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'h80)) 
    FIFO_WRITE_tlast_INST_0
       (.I0(FIFO_WRITE_tready),
        .I1(AXI_RVALID),
        .I2(FIFO_WRITE_tlast0),
        .O(FIFO_WRITE_tlast));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \FSM_sequential_FSM_next_reg[0] 
       (.CLR(1'b0),
        .D(FSM_next__0[0]),
        .G(\FSM_sequential_FSM_next_reg[2]_i_2_n_0 ),
        .GE(1'b1),
        .Q(FSM_next[0]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'h14151514)) 
    \FSM_sequential_FSM_next_reg[0]_i_1 
       (.I0(FSM[0]),
        .I1(FSM[1]),
        .I2(FSM[2]),
        .I3(rd_running),
        .I4(wr_running),
        .O(FSM_next__0[0]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \FSM_sequential_FSM_next_reg[1] 
       (.CLR(1'b0),
        .D(FSM_next__0[1]),
        .G(\FSM_sequential_FSM_next_reg[2]_i_2_n_0 ),
        .GE(1'b1),
        .Q(FSM_next[1]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'h000000F2)) 
    \FSM_sequential_FSM_next_reg[1]_i_1 
       (.I0(rd_running),
        .I1(wr_running),
        .I2(FSM[0]),
        .I3(FSM[2]),
        .I4(FSM[1]),
        .O(FSM_next__0[1]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \FSM_sequential_FSM_next_reg[2] 
       (.CLR(1'b0),
        .D(FSM_next__0[2]),
        .G(\FSM_sequential_FSM_next_reg[2]_i_2_n_0 ),
        .GE(1'b1),
        .Q(FSM_next[2]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'h26)) 
    \FSM_sequential_FSM_next_reg[2]_i_1 
       (.I0(FSM[1]),
        .I1(FSM[2]),
        .I2(FSM[0]),
        .O(FSM_next__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'h7770)) 
    \FSM_sequential_FSM_next_reg[2]_i_2 
       (.I0(FSM[2]),
        .I1(FSM[1]),
        .I2(\FSM_sequential_FSM_next_reg[2]_i_3_n_0 ),
        .I3(FSM[0]),
        .O(\FSM_sequential_FSM_next_reg[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hD5D500D500D500D5)) 
    \FSM_sequential_FSM_next_reg[2]_i_3 
       (.I0(FSM[1]),
        .I1(writedata_done_reg_n_0),
        .I2(writeaddr_done_reg_n_0),
        .I3(FSM[2]),
        .I4(readaddr_done_reg_n_0),
        .I5(readdata_done_reg_n_0),
        .O(\FSM_sequential_FSM_next_reg[2]_i_3_n_0 ));
  (* FSM_ENCODED_STATES = "iSTATE:010,iSTATE0:100,iSTATE1:101,iSTATE2:011,iSTATE3:001,iSTATE4:000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_FSM_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(FSM_next[0]),
        .Q(FSM[0]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "iSTATE:010,iSTATE0:100,iSTATE1:101,iSTATE2:011,iSTATE3:001,iSTATE4:000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_FSM_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(FSM_next[1]),
        .Q(FSM[1]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "iSTATE:010,iSTATE0:100,iSTATE1:101,iSTATE2:011,iSTATE3:001,iSTATE4:000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_FSM_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(FSM_next[2]),
        .Q(FSM[2]),
        .R(1'b0));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \FSM_sequential_readaddr_FSM_next_reg[0] 
       (.CLR(1'b0),
        .D(readaddr_FSM_next__0[0]),
        .G(\FSM_sequential_readaddr_FSM_next_reg[2]_i_2_n_0 ),
        .GE(1'b1),
        .Q(readaddr_FSM_next[0]));
  LUT6 #(
    .INIT(64'h1111111111111110)) 
    \FSM_sequential_readaddr_FSM_next_reg[0]_i_1 
       (.I0(readaddr_FSM[2]),
        .I1(readaddr_FSM[0]),
        .I2(readaddr_FSM[1]),
        .I3(\FSM_sequential_readaddr_FSM_next_reg[1]_i_2_n_0 ),
        .I4(\FSM_sequential_readaddr_FSM_next_reg[1]_i_3_n_0 ),
        .I5(\FSM_sequential_readaddr_FSM_next_reg[1]_i_4_n_0 ),
        .O(readaddr_FSM_next__0[0]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \FSM_sequential_readaddr_FSM_next_reg[1] 
       (.CLR(1'b0),
        .D(readaddr_FSM_next__0[1]),
        .G(\FSM_sequential_readaddr_FSM_next_reg[2]_i_2_n_0 ),
        .GE(1'b1),
        .Q(\FSM_sequential_readaddr_FSM_next_reg_n_0_[1] ));
  LUT6 #(
    .INIT(64'h000000FF00FF0001)) 
    \FSM_sequential_readaddr_FSM_next_reg[1]_i_1 
       (.I0(\FSM_sequential_readaddr_FSM_next_reg[1]_i_2_n_0 ),
        .I1(\FSM_sequential_readaddr_FSM_next_reg[1]_i_3_n_0 ),
        .I2(\FSM_sequential_readaddr_FSM_next_reg[1]_i_4_n_0 ),
        .I3(readaddr_FSM[2]),
        .I4(readaddr_FSM[1]),
        .I5(readaddr_FSM[0]),
        .O(readaddr_FSM_next__0[1]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \FSM_sequential_readaddr_FSM_next_reg[1]_i_2 
       (.I0(\FSM_sequential_readaddr_FSM_next_reg[1]_i_5_n_0 ),
        .I1(\FSM_sequential_readaddr_FSM_next_reg[1]_i_6_n_0 ),
        .I2(\FSM_sequential_readaddr_FSM_next_reg[1]_i_7_n_0 ),
        .I3(\FSM_sequential_readaddr_FSM_next_reg[1]_i_8_n_0 ),
        .I4(read_blockamount[16]),
        .I5(read_blockamount[17]),
        .O(\FSM_sequential_readaddr_FSM_next_reg[1]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \FSM_sequential_readaddr_FSM_next_reg[1]_i_3 
       (.I0(\FSM_sequential_readaddr_FSM_next_reg[1]_i_9_n_0 ),
        .I1(read_blockamount[26]),
        .I2(read_blockamount[27]),
        .I3(read_blockamount[28]),
        .I4(read_blockamount[29]),
        .O(\FSM_sequential_readaddr_FSM_next_reg[1]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hF0F0F0E0)) 
    \FSM_sequential_readaddr_FSM_next_reg[1]_i_4 
       (.I0(read_blockamount[1]),
        .I1(read_blockamount[0]),
        .I2(read_blockamount[4]),
        .I3(read_blockamount[3]),
        .I4(read_blockamount[2]),
        .O(\FSM_sequential_readaddr_FSM_next_reg[1]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \FSM_sequential_readaddr_FSM_next_reg[1]_i_5 
       (.I0(read_blockamount[25]),
        .I1(read_blockamount[24]),
        .I2(read_blockamount[23]),
        .I3(read_blockamount[22]),
        .O(\FSM_sequential_readaddr_FSM_next_reg[1]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'hFE)) 
    \FSM_sequential_readaddr_FSM_next_reg[1]_i_6 
       (.I0(read_blockamount[7]),
        .I1(read_blockamount[6]),
        .I2(read_blockamount[14]),
        .O(\FSM_sequential_readaddr_FSM_next_reg[1]_i_6_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \FSM_sequential_readaddr_FSM_next_reg[1]_i_7 
       (.I0(read_blockamount[8]),
        .I1(read_blockamount[11]),
        .I2(read_blockamount[5]),
        .I3(read_blockamount[9]),
        .O(\FSM_sequential_readaddr_FSM_next_reg[1]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \FSM_sequential_readaddr_FSM_next_reg[1]_i_8 
       (.I0(read_blockamount[12]),
        .I1(read_blockamount[15]),
        .I2(read_blockamount[10]),
        .I3(read_blockamount[13]),
        .O(\FSM_sequential_readaddr_FSM_next_reg[1]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \FSM_sequential_readaddr_FSM_next_reg[1]_i_9 
       (.I0(read_blockamount[18]),
        .I1(read_blockamount[19]),
        .I2(read_blockamount[20]),
        .I3(read_blockamount[21]),
        .I4(read_blockamount[31]),
        .I5(read_blockamount[30]),
        .O(\FSM_sequential_readaddr_FSM_next_reg[1]_i_9_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \FSM_sequential_readaddr_FSM_next_reg[2] 
       (.CLR(1'b0),
        .D(readaddr_FSM_next__0[2]),
        .G(\FSM_sequential_readaddr_FSM_next_reg[2]_i_2_n_0 ),
        .GE(1'b1),
        .Q(readaddr_FSM_next[2]));
  LUT4 #(
    .INIT(16'h0800)) 
    \FSM_sequential_readaddr_FSM_next_reg[2]_i_1 
       (.I0(readaddr_FSM[1]),
        .I1(readaddr_FSM[0]),
        .I2(readaddr_FSM[2]),
        .I3(read_start_reg_n_0),
        .O(readaddr_FSM_next__0[2]));
  LUT6 #(
    .INIT(64'h000000005FCA500A)) 
    \FSM_sequential_readaddr_FSM_next_reg[2]_i_2 
       (.I0(read_start_reg_n_0),
        .I1(readaddr_FSM_next17_in),
        .I2(readaddr_FSM[0]),
        .I3(readaddr_FSM[1]),
        .I4(\FSM_sequential_readaddr_FSM_next_reg[2]_i_3_n_0 ),
        .I5(readaddr_FSM[2]),
        .O(\FSM_sequential_readaddr_FSM_next_reg[2]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \FSM_sequential_readaddr_FSM_next_reg[2]_i_3 
       (.I0(AXI_ARVALID_reg_0),
        .I1(AXI_ARREADY),
        .O(\FSM_sequential_readaddr_FSM_next_reg[2]_i_3_n_0 ));
  (* FSM_ENCODED_STATES = "iSTATE:011,iSTATE0:001,iSTATE1:010,iSTATE2:000,iSTATE3:100" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_readaddr_FSM_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(readaddr_FSM_next[0]),
        .Q(readaddr_FSM[0]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "iSTATE:011,iSTATE0:001,iSTATE1:010,iSTATE2:000,iSTATE3:100" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_readaddr_FSM_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_sequential_readaddr_FSM_next_reg_n_0_[1] ),
        .Q(readaddr_FSM[1]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "iSTATE:011,iSTATE0:001,iSTATE1:010,iSTATE2:000,iSTATE3:100" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_readaddr_FSM_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(readaddr_FSM_next[2]),
        .Q(readaddr_FSM[2]),
        .R(1'b0));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \FSM_sequential_readdata_FSM_next_reg[0] 
       (.CLR(1'b0),
        .D(readdata_FSM_next__0[0]),
        .G(\FSM_sequential_readdata_FSM_next_reg[2]_i_2_n_0 ),
        .GE(1'b1),
        .Q(readdata_FSM_next[0]));
  LUT2 #(
    .INIT(4'h1)) 
    \FSM_sequential_readdata_FSM_next_reg[0]_i_1 
       (.I0(readdata_FSM[0]),
        .I1(readdata_FSM[2]),
        .O(readdata_FSM_next__0[0]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \FSM_sequential_readdata_FSM_next_reg[1] 
       (.CLR(1'b0),
        .D(readdata_FSM_next__0[1]),
        .G(\FSM_sequential_readdata_FSM_next_reg[2]_i_2_n_0 ),
        .GE(1'b1),
        .Q(\FSM_sequential_readdata_FSM_next_reg_n_0_[1] ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'h06)) 
    \FSM_sequential_readdata_FSM_next_reg[1]_i_1 
       (.I0(readdata_FSM[0]),
        .I1(readdata_FSM[1]),
        .I2(readdata_FSM[2]),
        .O(readdata_FSM_next__0[1]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \FSM_sequential_readdata_FSM_next_reg[2] 
       (.CLR(1'b0),
        .D(readdata_FSM_next__0[2]),
        .G(\FSM_sequential_readdata_FSM_next_reg[2]_i_2_n_0 ),
        .GE(1'b1),
        .Q(readdata_FSM_next[2]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'h4000)) 
    \FSM_sequential_readdata_FSM_next_reg[2]_i_1 
       (.I0(readdata_FSM[2]),
        .I1(readdata_FSM[1]),
        .I2(read_start_reg_n_0),
        .I3(readdata_FSM[0]),
        .O(readdata_FSM_next__0[2]));
  LUT6 #(
    .INIT(64'h050F0C0A050F000A)) 
    \FSM_sequential_readdata_FSM_next_reg[2]_i_2 
       (.I0(read_start_reg_n_0),
        .I1(\FSM_sequential_readdata_FSM_next_reg[2]_i_3_n_0 ),
        .I2(readdata_FSM[2]),
        .I3(readdata_FSM[0]),
        .I4(readdata_FSM[1]),
        .I5(FIFO_WRITE_tlast0),
        .O(\FSM_sequential_readdata_FSM_next_reg[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \FSM_sequential_readdata_FSM_next_reg[2]_i_3 
       (.I0(AXI_RVALID),
        .I1(FIFO_WRITE_tready),
        .O(\FSM_sequential_readdata_FSM_next_reg[2]_i_3_n_0 ));
  (* FSM_ENCODED_STATES = "iSTATE:001,iSTATE0:010,iSTATE1:011,iSTATE2:000,iSTATE3:100" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_readdata_FSM_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(readdata_FSM_next[0]),
        .Q(readdata_FSM[0]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "iSTATE:001,iSTATE0:010,iSTATE1:011,iSTATE2:000,iSTATE3:100" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_readdata_FSM_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_sequential_readdata_FSM_next_reg_n_0_[1] ),
        .Q(readdata_FSM[1]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "iSTATE:001,iSTATE0:010,iSTATE1:011,iSTATE2:000,iSTATE3:100" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_readdata_FSM_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(readdata_FSM_next[2]),
        .Q(readdata_FSM[2]),
        .R(1'b0));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \FSM_sequential_writeaddr_FSM_next_reg[0] 
       (.CLR(1'b0),
        .D(writeaddr_FSM_next__0[0]),
        .G(\FSM_sequential_writeaddr_FSM_next_reg[2]_i_2_n_0 ),
        .GE(1'b1),
        .Q(writeaddr_FSM_next[0]));
  LUT6 #(
    .INIT(64'h1111111111111110)) 
    \FSM_sequential_writeaddr_FSM_next_reg[0]_i_1 
       (.I0(writeaddr_FSM[2]),
        .I1(writeaddr_FSM[0]),
        .I2(writeaddr_FSM[1]),
        .I3(\FSM_sequential_writeaddr_FSM_next_reg[1]_i_2_n_0 ),
        .I4(\FSM_sequential_writeaddr_FSM_next_reg[1]_i_3_n_0 ),
        .I5(\FSM_sequential_writeaddr_FSM_next_reg[1]_i_4_n_0 ),
        .O(writeaddr_FSM_next__0[0]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \FSM_sequential_writeaddr_FSM_next_reg[1] 
       (.CLR(1'b0),
        .D(writeaddr_FSM_next__0[1]),
        .G(\FSM_sequential_writeaddr_FSM_next_reg[2]_i_2_n_0 ),
        .GE(1'b1),
        .Q(\FSM_sequential_writeaddr_FSM_next_reg_n_0_[1] ));
  LUT6 #(
    .INIT(64'h000000FF00FF0001)) 
    \FSM_sequential_writeaddr_FSM_next_reg[1]_i_1 
       (.I0(\FSM_sequential_writeaddr_FSM_next_reg[1]_i_2_n_0 ),
        .I1(\FSM_sequential_writeaddr_FSM_next_reg[1]_i_3_n_0 ),
        .I2(\FSM_sequential_writeaddr_FSM_next_reg[1]_i_4_n_0 ),
        .I3(writeaddr_FSM[2]),
        .I4(writeaddr_FSM[1]),
        .I5(writeaddr_FSM[0]),
        .O(writeaddr_FSM_next__0[1]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \FSM_sequential_writeaddr_FSM_next_reg[1]_i_2 
       (.I0(\FSM_sequential_writeaddr_FSM_next_reg[1]_i_5_n_0 ),
        .I1(\FSM_sequential_writeaddr_FSM_next_reg[1]_i_6_n_0 ),
        .I2(\FSM_sequential_writeaddr_FSM_next_reg[1]_i_7_n_0 ),
        .I3(\FSM_sequential_writeaddr_FSM_next_reg[1]_i_8_n_0 ),
        .I4(write_blockamount[16]),
        .I5(write_blockamount[17]),
        .O(\FSM_sequential_writeaddr_FSM_next_reg[1]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \FSM_sequential_writeaddr_FSM_next_reg[1]_i_3 
       (.I0(\FSM_sequential_writeaddr_FSM_next_reg[1]_i_9_n_0 ),
        .I1(write_blockamount[26]),
        .I2(write_blockamount[27]),
        .I3(write_blockamount[28]),
        .I4(write_blockamount[29]),
        .O(\FSM_sequential_writeaddr_FSM_next_reg[1]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hF0F0F0E0)) 
    \FSM_sequential_writeaddr_FSM_next_reg[1]_i_4 
       (.I0(write_blockamount[1]),
        .I1(write_blockamount[0]),
        .I2(write_blockamount[4]),
        .I3(write_blockamount[3]),
        .I4(write_blockamount[2]),
        .O(\FSM_sequential_writeaddr_FSM_next_reg[1]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \FSM_sequential_writeaddr_FSM_next_reg[1]_i_5 
       (.I0(write_blockamount[25]),
        .I1(write_blockamount[24]),
        .I2(write_blockamount[23]),
        .I3(write_blockamount[22]),
        .O(\FSM_sequential_writeaddr_FSM_next_reg[1]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'hFE)) 
    \FSM_sequential_writeaddr_FSM_next_reg[1]_i_6 
       (.I0(write_blockamount[7]),
        .I1(write_blockamount[6]),
        .I2(write_blockamount[14]),
        .O(\FSM_sequential_writeaddr_FSM_next_reg[1]_i_6_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \FSM_sequential_writeaddr_FSM_next_reg[1]_i_7 
       (.I0(write_blockamount[8]),
        .I1(write_blockamount[11]),
        .I2(write_blockamount[5]),
        .I3(write_blockamount[9]),
        .O(\FSM_sequential_writeaddr_FSM_next_reg[1]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \FSM_sequential_writeaddr_FSM_next_reg[1]_i_8 
       (.I0(write_blockamount[12]),
        .I1(write_blockamount[15]),
        .I2(write_blockamount[10]),
        .I3(write_blockamount[13]),
        .O(\FSM_sequential_writeaddr_FSM_next_reg[1]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \FSM_sequential_writeaddr_FSM_next_reg[1]_i_9 
       (.I0(write_blockamount[18]),
        .I1(write_blockamount[19]),
        .I2(write_blockamount[20]),
        .I3(write_blockamount[21]),
        .I4(write_blockamount[31]),
        .I5(write_blockamount[30]),
        .O(\FSM_sequential_writeaddr_FSM_next_reg[1]_i_9_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \FSM_sequential_writeaddr_FSM_next_reg[2] 
       (.CLR(1'b0),
        .D(writeaddr_FSM_next__0[2]),
        .G(\FSM_sequential_writeaddr_FSM_next_reg[2]_i_2_n_0 ),
        .GE(1'b1),
        .Q(writeaddr_FSM_next[2]));
  LUT4 #(
    .INIT(16'h0800)) 
    \FSM_sequential_writeaddr_FSM_next_reg[2]_i_1 
       (.I0(writeaddr_FSM[1]),
        .I1(writeaddr_FSM[0]),
        .I2(writeaddr_FSM[2]),
        .I3(write_start_reg_n_0),
        .O(writeaddr_FSM_next__0[2]));
  LUT6 #(
    .INIT(64'h000000005FCA500A)) 
    \FSM_sequential_writeaddr_FSM_next_reg[2]_i_2 
       (.I0(write_start_reg_n_0),
        .I1(writeaddr_FSM_next15_in),
        .I2(writeaddr_FSM[0]),
        .I3(writeaddr_FSM[1]),
        .I4(\FSM_sequential_writeaddr_FSM_next_reg[2]_i_3_n_0 ),
        .I5(writeaddr_FSM[2]),
        .O(\FSM_sequential_writeaddr_FSM_next_reg[2]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \FSM_sequential_writeaddr_FSM_next_reg[2]_i_3 
       (.I0(AXI_AWREADY),
        .I1(AXI_AWVALID_reg_0),
        .O(\FSM_sequential_writeaddr_FSM_next_reg[2]_i_3_n_0 ));
  (* FSM_ENCODED_STATES = "iSTATE:011,iSTATE0:001,iSTATE1:010,iSTATE2:000,iSTATE3:100" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_writeaddr_FSM_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(writeaddr_FSM_next[0]),
        .Q(writeaddr_FSM[0]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "iSTATE:011,iSTATE0:001,iSTATE1:010,iSTATE2:000,iSTATE3:100" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_writeaddr_FSM_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_sequential_writeaddr_FSM_next_reg_n_0_[1] ),
        .Q(writeaddr_FSM[1]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "iSTATE:011,iSTATE0:001,iSTATE1:010,iSTATE2:000,iSTATE3:100" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_writeaddr_FSM_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(writeaddr_FSM_next[2]),
        .Q(writeaddr_FSM[2]),
        .R(1'b0));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \FSM_sequential_writedata_FSM_next_reg[0] 
       (.CLR(1'b0),
        .D(writedata_FSM_next__0[0]),
        .G(\FSM_sequential_writedata_FSM_next_reg[2]_i_2_n_0 ),
        .GE(1'b1),
        .Q(writedata_FSM_next[0]));
  LUT2 #(
    .INIT(4'h1)) 
    \FSM_sequential_writedata_FSM_next_reg[0]_i_1 
       (.I0(writedata_FSM[0]),
        .I1(writedata_FSM[2]),
        .O(writedata_FSM_next__0[0]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \FSM_sequential_writedata_FSM_next_reg[1] 
       (.CLR(1'b0),
        .D(writedata_FSM_next__0[1]),
        .G(\FSM_sequential_writedata_FSM_next_reg[2]_i_2_n_0 ),
        .GE(1'b1),
        .Q(\FSM_sequential_writedata_FSM_next_reg_n_0_[1] ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'h06)) 
    \FSM_sequential_writedata_FSM_next_reg[1]_i_1 
       (.I0(writedata_FSM[0]),
        .I1(writedata_FSM[1]),
        .I2(writedata_FSM[2]),
        .O(writedata_FSM_next__0[1]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \FSM_sequential_writedata_FSM_next_reg[2] 
       (.CLR(1'b0),
        .D(writedata_FSM_next__0[2]),
        .G(\FSM_sequential_writedata_FSM_next_reg[2]_i_2_n_0 ),
        .GE(1'b1),
        .Q(writedata_FSM_next[2]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h4000)) 
    \FSM_sequential_writedata_FSM_next_reg[2]_i_1 
       (.I0(writedata_FSM[2]),
        .I1(writedata_FSM[1]),
        .I2(write_start_reg_n_0),
        .I3(writedata_FSM[0]),
        .O(writedata_FSM_next__0[2]));
  LUT6 #(
    .INIT(64'h050F0C0A050F000A)) 
    \FSM_sequential_writedata_FSM_next_reg[2]_i_2 
       (.I0(write_start_reg_n_0),
        .I1(\FSM_sequential_writedata_FSM_next_reg[2]_i_3_n_0 ),
        .I2(writedata_FSM[2]),
        .I3(writedata_FSM[0]),
        .I4(writedata_FSM[1]),
        .I5(writedata_FSM_next1),
        .O(\FSM_sequential_writedata_FSM_next_reg[2]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \FSM_sequential_writedata_FSM_next_reg[2]_i_3 
       (.I0(FIFO_READ_tvalid),
        .I1(AXI_WREADY),
        .O(\FSM_sequential_writedata_FSM_next_reg[2]_i_3_n_0 ));
  (* FSM_ENCODED_STATES = "iSTATE:001,iSTATE0:010,iSTATE1:011,iSTATE2:000,iSTATE3:100" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_writedata_FSM_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(writedata_FSM_next[0]),
        .Q(writedata_FSM[0]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "iSTATE:001,iSTATE0:010,iSTATE1:011,iSTATE2:000,iSTATE3:100" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_writedata_FSM_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_sequential_writedata_FSM_next_reg_n_0_[1] ),
        .Q(writedata_FSM[1]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "iSTATE:001,iSTATE0:010,iSTATE1:011,iSTATE2:000,iSTATE3:100" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_writedata_FSM_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(writedata_FSM_next[2]),
        .Q(writedata_FSM[2]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hFE44)) 
    busy_i_1
       (.I0(FSM_next[2]),
        .I1(FSM_next[0]),
        .I2(FSM_next[1]),
        .I3(busy),
        .O(busy_i_1_n_0));
  FDRE busy_reg
       (.C(clk),
        .CE(1'b1),
        .D(busy_i_1_n_0),
        .Q(busy),
        .R(1'b0));
  FDRE \read_blockamount_reg[0] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_blockamount[0]),
        .Q(read_blockamount[0]),
        .R(1'b0));
  FDRE \read_blockamount_reg[10] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_blockamount[10]),
        .Q(read_blockamount[10]),
        .R(1'b0));
  FDRE \read_blockamount_reg[11] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_blockamount[11]),
        .Q(read_blockamount[11]),
        .R(1'b0));
  FDRE \read_blockamount_reg[12] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_blockamount[12]),
        .Q(read_blockamount[12]),
        .R(1'b0));
  FDRE \read_blockamount_reg[13] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_blockamount[13]),
        .Q(read_blockamount[13]),
        .R(1'b0));
  FDRE \read_blockamount_reg[14] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_blockamount[14]),
        .Q(read_blockamount[14]),
        .R(1'b0));
  FDRE \read_blockamount_reg[15] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_blockamount[15]),
        .Q(read_blockamount[15]),
        .R(1'b0));
  FDRE \read_blockamount_reg[16] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_blockamount[16]),
        .Q(read_blockamount[16]),
        .R(1'b0));
  FDRE \read_blockamount_reg[17] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_blockamount[17]),
        .Q(read_blockamount[17]),
        .R(1'b0));
  FDRE \read_blockamount_reg[18] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_blockamount[18]),
        .Q(read_blockamount[18]),
        .R(1'b0));
  FDRE \read_blockamount_reg[19] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_blockamount[19]),
        .Q(read_blockamount[19]),
        .R(1'b0));
  FDRE \read_blockamount_reg[1] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_blockamount[1]),
        .Q(read_blockamount[1]),
        .R(1'b0));
  FDRE \read_blockamount_reg[20] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_blockamount[20]),
        .Q(read_blockamount[20]),
        .R(1'b0));
  FDRE \read_blockamount_reg[21] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_blockamount[21]),
        .Q(read_blockamount[21]),
        .R(1'b0));
  FDRE \read_blockamount_reg[22] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_blockamount[22]),
        .Q(read_blockamount[22]),
        .R(1'b0));
  FDRE \read_blockamount_reg[23] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_blockamount[23]),
        .Q(read_blockamount[23]),
        .R(1'b0));
  FDRE \read_blockamount_reg[24] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_blockamount[24]),
        .Q(read_blockamount[24]),
        .R(1'b0));
  FDRE \read_blockamount_reg[25] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_blockamount[25]),
        .Q(read_blockamount[25]),
        .R(1'b0));
  FDRE \read_blockamount_reg[26] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_blockamount[26]),
        .Q(read_blockamount[26]),
        .R(1'b0));
  FDRE \read_blockamount_reg[27] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_blockamount[27]),
        .Q(read_blockamount[27]),
        .R(1'b0));
  FDRE \read_blockamount_reg[28] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_blockamount[28]),
        .Q(read_blockamount[28]),
        .R(1'b0));
  FDRE \read_blockamount_reg[29] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_blockamount[29]),
        .Q(read_blockamount[29]),
        .R(1'b0));
  FDRE \read_blockamount_reg[2] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_blockamount[2]),
        .Q(read_blockamount[2]),
        .R(1'b0));
  FDRE \read_blockamount_reg[30] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_blockamount[30]),
        .Q(read_blockamount[30]),
        .R(1'b0));
  FDRE \read_blockamount_reg[31] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_blockamount[31]),
        .Q(read_blockamount[31]),
        .R(1'b0));
  FDRE \read_blockamount_reg[3] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_blockamount[3]),
        .Q(read_blockamount[3]),
        .R(1'b0));
  FDRE \read_blockamount_reg[4] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_blockamount[4]),
        .Q(read_blockamount[4]),
        .R(1'b0));
  FDRE \read_blockamount_reg[5] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_blockamount[5]),
        .Q(read_blockamount[5]),
        .R(1'b0));
  FDRE \read_blockamount_reg[6] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_blockamount[6]),
        .Q(read_blockamount[6]),
        .R(1'b0));
  FDRE \read_blockamount_reg[7] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_blockamount[7]),
        .Q(read_blockamount[7]),
        .R(1'b0));
  FDRE \read_blockamount_reg[8] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_blockamount[8]),
        .Q(read_blockamount[8]),
        .R(1'b0));
  FDRE \read_blockamount_reg[9] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_blockamount[9]),
        .Q(read_blockamount[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'hF720)) 
    read_start_i_1
       (.I0(FSM_next[0]),
        .I1(FSM_next[2]),
        .I2(FSM_next[1]),
        .I3(read_start_reg_n_0),
        .O(read_start_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    read_start_reg
       (.C(clk),
        .CE(1'b1),
        .D(read_start_i_1_n_0),
        .Q(read_start_reg_n_0),
        .R(1'b0));
  FDRE \read_startaddress_reg[0] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_startaddress[0]),
        .Q(read_startaddress[0]),
        .R(1'b0));
  FDRE \read_startaddress_reg[10] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_startaddress[10]),
        .Q(read_startaddress[10]),
        .R(1'b0));
  FDRE \read_startaddress_reg[11] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_startaddress[11]),
        .Q(read_startaddress[11]),
        .R(1'b0));
  FDRE \read_startaddress_reg[12] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_startaddress[12]),
        .Q(read_startaddress[12]),
        .R(1'b0));
  FDRE \read_startaddress_reg[13] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_startaddress[13]),
        .Q(read_startaddress[13]),
        .R(1'b0));
  FDRE \read_startaddress_reg[14] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_startaddress[14]),
        .Q(read_startaddress[14]),
        .R(1'b0));
  FDRE \read_startaddress_reg[15] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_startaddress[15]),
        .Q(read_startaddress[15]),
        .R(1'b0));
  FDRE \read_startaddress_reg[16] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_startaddress[16]),
        .Q(read_startaddress[16]),
        .R(1'b0));
  FDRE \read_startaddress_reg[17] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_startaddress[17]),
        .Q(read_startaddress[17]),
        .R(1'b0));
  FDRE \read_startaddress_reg[18] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_startaddress[18]),
        .Q(read_startaddress[18]),
        .R(1'b0));
  FDRE \read_startaddress_reg[19] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_startaddress[19]),
        .Q(read_startaddress[19]),
        .R(1'b0));
  FDRE \read_startaddress_reg[1] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_startaddress[1]),
        .Q(read_startaddress[1]),
        .R(1'b0));
  FDRE \read_startaddress_reg[20] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_startaddress[20]),
        .Q(read_startaddress[20]),
        .R(1'b0));
  FDRE \read_startaddress_reg[21] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_startaddress[21]),
        .Q(read_startaddress[21]),
        .R(1'b0));
  FDRE \read_startaddress_reg[22] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_startaddress[22]),
        .Q(read_startaddress[22]),
        .R(1'b0));
  FDRE \read_startaddress_reg[23] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_startaddress[23]),
        .Q(read_startaddress[23]),
        .R(1'b0));
  FDRE \read_startaddress_reg[24] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_startaddress[24]),
        .Q(read_startaddress[24]),
        .R(1'b0));
  FDRE \read_startaddress_reg[25] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_startaddress[25]),
        .Q(read_startaddress[25]),
        .R(1'b0));
  FDRE \read_startaddress_reg[26] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_startaddress[26]),
        .Q(read_startaddress[26]),
        .R(1'b0));
  FDRE \read_startaddress_reg[27] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_startaddress[27]),
        .Q(read_startaddress[27]),
        .R(1'b0));
  FDRE \read_startaddress_reg[28] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_startaddress[28]),
        .Q(read_startaddress[28]),
        .R(1'b0));
  FDRE \read_startaddress_reg[29] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_startaddress[29]),
        .Q(read_startaddress[29]),
        .R(1'b0));
  FDRE \read_startaddress_reg[2] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_startaddress[2]),
        .Q(read_startaddress[2]),
        .R(1'b0));
  FDRE \read_startaddress_reg[30] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_startaddress[30]),
        .Q(read_startaddress[30]),
        .R(1'b0));
  FDRE \read_startaddress_reg[31] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_startaddress[31]),
        .Q(read_startaddress[31]),
        .R(1'b0));
  FDRE \read_startaddress_reg[3] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_startaddress[3]),
        .Q(read_startaddress[3]),
        .R(1'b0));
  FDRE \read_startaddress_reg[4] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_startaddress[4]),
        .Q(read_startaddress[4]),
        .R(1'b0));
  FDRE \read_startaddress_reg[5] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_startaddress[5]),
        .Q(read_startaddress[5]),
        .R(1'b0));
  FDRE \read_startaddress_reg[6] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_startaddress[6]),
        .Q(read_startaddress[6]),
        .R(1'b0));
  FDRE \read_startaddress_reg[7] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_startaddress[7]),
        .Q(read_startaddress[7]),
        .R(1'b0));
  FDRE \read_startaddress_reg[8] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_startaddress[8]),
        .Q(read_startaddress[8]),
        .R(1'b0));
  FDRE \read_startaddress_reg[9] 
       (.C(clk),
        .CE(read_startaddress_1),
        .D(rd_startaddress[9]),
        .Q(read_startaddress[9]),
        .R(1'b0));
  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY8 readaddr_FSM_next1_carry
       (.CI(readaddr_FSM_next1_carry_i_1_n_0),
        .CI_TOP(1'b0),
        .CO({readaddr_FSM_next1_carry_n_0,readaddr_FSM_next1_carry_n_1,readaddr_FSM_next1_carry_n_2,readaddr_FSM_next1_carry_n_3,readaddr_FSM_next1_carry_n_4,readaddr_FSM_next1_carry_n_5,readaddr_FSM_next1_carry_n_6,readaddr_FSM_next1_carry_n_7}),
        .DI({readaddr_FSM_next1_carry_i_2_n_0,readaddr_FSM_next1_carry_i_3_n_0,readaddr_FSM_next1_carry_i_4_n_0,readaddr_FSM_next1_carry_i_5_n_0,readaddr_FSM_next1_carry_i_6_n_0,readaddr_FSM_next1_carry_i_7_n_0,readaddr_FSM_next1_carry_i_8_n_0,1'b0}),
        .O(NLW_readaddr_FSM_next1_carry_O_UNCONNECTED[7:0]),
        .S({readaddr_FSM_next1_carry_i_9_n_0,readaddr_FSM_next1_carry_i_10_n_0,readaddr_FSM_next1_carry_i_11_n_0,readaddr_FSM_next1_carry_i_12_n_0,readaddr_FSM_next1_carry_i_13_n_0,readaddr_FSM_next1_carry_i_14_n_0,readaddr_FSM_next1_carry_i_15_n_0,readaddr_FSM_next1_carry_i_16_n_0}));
  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY8 readaddr_FSM_next1_carry__0
       (.CI(readaddr_FSM_next1_carry_n_0),
        .CI_TOP(1'b0),
        .CO({NLW_readaddr_FSM_next1_carry__0_CO_UNCONNECTED[7],readaddr_FSM_next17_in,readaddr_FSM_next1_carry__0_n_2,readaddr_FSM_next1_carry__0_n_3,readaddr_FSM_next1_carry__0_n_4,readaddr_FSM_next1_carry__0_n_5,readaddr_FSM_next1_carry__0_n_6,readaddr_FSM_next1_carry__0_n_7}),
        .DI({1'b0,readaddr_FSM_next1_carry__0_i_1_n_0,readaddr_FSM_next1_carry__0_i_2_n_0,readaddr_FSM_next1_carry__0_i_3_n_0,readaddr_FSM_next1_carry__0_i_4_n_0,readaddr_FSM_next1_carry__0_i_5_n_0,readaddr_FSM_next1_carry__0_i_6_n_0,readaddr_FSM_next1_carry__0_i_7_n_0}),
        .O(NLW_readaddr_FSM_next1_carry__0_O_UNCONNECTED[7:0]),
        .S({1'b0,readaddr_FSM_next1_carry__0_i_8_n_0,readaddr_FSM_next1_carry__0_i_9_n_0,readaddr_FSM_next1_carry__0_i_10_n_0,readaddr_FSM_next1_carry__0_i_11_n_0,readaddr_FSM_next1_carry__0_i_12_n_0,readaddr_FSM_next1_carry__0_i_13_n_0,readaddr_FSM_next1_carry__0_i_14_n_0}));
  LUT4 #(
    .INIT(16'h2F02)) 
    readaddr_FSM_next1_carry__0_i_1
       (.I0(readaddr_FSM_next1_carry__0_i_15_n_12),
        .I1(read_blockamount[30]),
        .I2(read_blockamount[31]),
        .I3(readaddr_FSM_next1_carry__0_i_15_n_11),
        .O(readaddr_FSM_next1_carry__0_i_1_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    readaddr_FSM_next1_carry__0_i_10
       (.I0(read_blockamount[27]),
        .I1(readaddr_FSM_next1_carry__0_i_15_n_15),
        .I2(readaddr_FSM_next1_carry__0_i_16_n_8),
        .I3(read_blockamount[26]),
        .O(readaddr_FSM_next1_carry__0_i_10_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    readaddr_FSM_next1_carry__0_i_11
       (.I0(read_blockamount[25]),
        .I1(readaddr_FSM_next1_carry__0_i_16_n_9),
        .I2(readaddr_FSM_next1_carry__0_i_16_n_10),
        .I3(read_blockamount[24]),
        .O(readaddr_FSM_next1_carry__0_i_11_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    readaddr_FSM_next1_carry__0_i_12
       (.I0(read_blockamount[23]),
        .I1(readaddr_FSM_next1_carry__0_i_16_n_11),
        .I2(readaddr_FSM_next1_carry__0_i_16_n_12),
        .I3(read_blockamount[22]),
        .O(readaddr_FSM_next1_carry__0_i_12_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    readaddr_FSM_next1_carry__0_i_13
       (.I0(read_blockamount[21]),
        .I1(readaddr_FSM_next1_carry__0_i_16_n_13),
        .I2(readaddr_FSM_next1_carry__0_i_16_n_14),
        .I3(read_blockamount[20]),
        .O(readaddr_FSM_next1_carry__0_i_13_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    readaddr_FSM_next1_carry__0_i_14
       (.I0(read_blockamount[19]),
        .I1(readaddr_FSM_next1_carry__0_i_16_n_15),
        .I2(readaddr_FSM_next1_carry_i_17_n_8),
        .I3(read_blockamount[18]),
        .O(readaddr_FSM_next1_carry__0_i_14_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY8 readaddr_FSM_next1_carry__0_i_15
       (.CI(readaddr_FSM_next1_carry__0_i_16_n_0),
        .CI_TOP(1'b0),
        .CO({NLW_readaddr_FSM_next1_carry__0_i_15_CO_UNCONNECTED[7:4],readaddr_FSM_next1_carry__0_i_15_n_4,readaddr_FSM_next1_carry__0_i_15_n_5,readaddr_FSM_next1_carry__0_i_15_n_6,readaddr_FSM_next1_carry__0_i_15_n_7}),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_readaddr_FSM_next1_carry__0_i_15_O_UNCONNECTED[7:5],readaddr_FSM_next1_carry__0_i_15_n_11,readaddr_FSM_next1_carry__0_i_15_n_12,readaddr_FSM_next1_carry__0_i_15_n_13,readaddr_FSM_next1_carry__0_i_15_n_14,readaddr_FSM_next1_carry__0_i_15_n_15}),
        .S({1'b0,1'b0,1'b0,readaddr_count_reg[31:27]}));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY8 readaddr_FSM_next1_carry__0_i_16
       (.CI(readaddr_FSM_next1_carry_i_17_n_0),
        .CI_TOP(1'b0),
        .CO({readaddr_FSM_next1_carry__0_i_16_n_0,readaddr_FSM_next1_carry__0_i_16_n_1,readaddr_FSM_next1_carry__0_i_16_n_2,readaddr_FSM_next1_carry__0_i_16_n_3,readaddr_FSM_next1_carry__0_i_16_n_4,readaddr_FSM_next1_carry__0_i_16_n_5,readaddr_FSM_next1_carry__0_i_16_n_6,readaddr_FSM_next1_carry__0_i_16_n_7}),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({readaddr_FSM_next1_carry__0_i_16_n_8,readaddr_FSM_next1_carry__0_i_16_n_9,readaddr_FSM_next1_carry__0_i_16_n_10,readaddr_FSM_next1_carry__0_i_16_n_11,readaddr_FSM_next1_carry__0_i_16_n_12,readaddr_FSM_next1_carry__0_i_16_n_13,readaddr_FSM_next1_carry__0_i_16_n_14,readaddr_FSM_next1_carry__0_i_16_n_15}),
        .S(readaddr_count_reg[26:19]));
  LUT4 #(
    .INIT(16'h2F02)) 
    readaddr_FSM_next1_carry__0_i_2
       (.I0(readaddr_FSM_next1_carry__0_i_15_n_14),
        .I1(read_blockamount[28]),
        .I2(read_blockamount[29]),
        .I3(readaddr_FSM_next1_carry__0_i_15_n_13),
        .O(readaddr_FSM_next1_carry__0_i_2_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    readaddr_FSM_next1_carry__0_i_3
       (.I0(readaddr_FSM_next1_carry__0_i_16_n_8),
        .I1(read_blockamount[26]),
        .I2(read_blockamount[27]),
        .I3(readaddr_FSM_next1_carry__0_i_15_n_15),
        .O(readaddr_FSM_next1_carry__0_i_3_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    readaddr_FSM_next1_carry__0_i_4
       (.I0(readaddr_FSM_next1_carry__0_i_16_n_10),
        .I1(read_blockamount[24]),
        .I2(read_blockamount[25]),
        .I3(readaddr_FSM_next1_carry__0_i_16_n_9),
        .O(readaddr_FSM_next1_carry__0_i_4_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    readaddr_FSM_next1_carry__0_i_5
       (.I0(readaddr_FSM_next1_carry__0_i_16_n_12),
        .I1(read_blockamount[22]),
        .I2(read_blockamount[23]),
        .I3(readaddr_FSM_next1_carry__0_i_16_n_11),
        .O(readaddr_FSM_next1_carry__0_i_5_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    readaddr_FSM_next1_carry__0_i_6
       (.I0(readaddr_FSM_next1_carry__0_i_16_n_14),
        .I1(read_blockamount[20]),
        .I2(read_blockamount[21]),
        .I3(readaddr_FSM_next1_carry__0_i_16_n_13),
        .O(readaddr_FSM_next1_carry__0_i_6_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    readaddr_FSM_next1_carry__0_i_7
       (.I0(readaddr_FSM_next1_carry_i_17_n_8),
        .I1(read_blockamount[18]),
        .I2(read_blockamount[19]),
        .I3(readaddr_FSM_next1_carry__0_i_16_n_15),
        .O(readaddr_FSM_next1_carry__0_i_7_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    readaddr_FSM_next1_carry__0_i_8
       (.I0(read_blockamount[31]),
        .I1(readaddr_FSM_next1_carry__0_i_15_n_11),
        .I2(readaddr_FSM_next1_carry__0_i_15_n_12),
        .I3(read_blockamount[30]),
        .O(readaddr_FSM_next1_carry__0_i_8_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    readaddr_FSM_next1_carry__0_i_9
       (.I0(read_blockamount[29]),
        .I1(readaddr_FSM_next1_carry__0_i_15_n_13),
        .I2(readaddr_FSM_next1_carry__0_i_15_n_14),
        .I3(read_blockamount[28]),
        .O(readaddr_FSM_next1_carry__0_i_9_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    readaddr_FSM_next1_carry_i_1
       (.I0(read_blockamount[0]),
        .I1(read_blockamount[1]),
        .O(readaddr_FSM_next1_carry_i_1_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    readaddr_FSM_next1_carry_i_10
       (.I0(read_blockamount[15]),
        .I1(readaddr_FSM_next1_carry_i_17_n_11),
        .I2(readaddr_FSM_next1_carry_i_17_n_12),
        .I3(read_blockamount[14]),
        .O(readaddr_FSM_next1_carry_i_10_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    readaddr_FSM_next1_carry_i_11
       (.I0(read_blockamount[13]),
        .I1(readaddr_FSM_next1_carry_i_17_n_13),
        .I2(readaddr_FSM_next1_carry_i_17_n_14),
        .I3(read_blockamount[12]),
        .O(readaddr_FSM_next1_carry_i_11_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    readaddr_FSM_next1_carry_i_12
       (.I0(read_blockamount[11]),
        .I1(readaddr_FSM_next1_carry_i_17_n_15),
        .I2(readaddr_FSM_next1_carry_i_18_n_8),
        .I3(read_blockamount[10]),
        .O(readaddr_FSM_next1_carry_i_12_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    readaddr_FSM_next1_carry_i_13
       (.I0(read_blockamount[9]),
        .I1(readaddr_FSM_next1_carry_i_18_n_9),
        .I2(readaddr_FSM_next1_carry_i_18_n_10),
        .I3(read_blockamount[8]),
        .O(readaddr_FSM_next1_carry_i_13_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    readaddr_FSM_next1_carry_i_14
       (.I0(read_blockamount[7]),
        .I1(readaddr_FSM_next1_carry_i_18_n_11),
        .I2(readaddr_FSM_next1_carry_i_18_n_12),
        .I3(read_blockamount[6]),
        .O(readaddr_FSM_next1_carry_i_14_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    readaddr_FSM_next1_carry_i_15
       (.I0(read_blockamount[5]),
        .I1(readaddr_FSM_next1_carry_i_18_n_13),
        .I2(readaddr_FSM_next1_carry_i_18_n_14),
        .I3(read_blockamount[4]),
        .O(readaddr_FSM_next1_carry_i_15_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    readaddr_FSM_next1_carry_i_16
       (.I0(read_blockamount[2]),
        .I1(read_blockamount[3]),
        .O(readaddr_FSM_next1_carry_i_16_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY8 readaddr_FSM_next1_carry_i_17
       (.CI(readaddr_FSM_next1_carry_i_18_n_0),
        .CI_TOP(1'b0),
        .CO({readaddr_FSM_next1_carry_i_17_n_0,readaddr_FSM_next1_carry_i_17_n_1,readaddr_FSM_next1_carry_i_17_n_2,readaddr_FSM_next1_carry_i_17_n_3,readaddr_FSM_next1_carry_i_17_n_4,readaddr_FSM_next1_carry_i_17_n_5,readaddr_FSM_next1_carry_i_17_n_6,readaddr_FSM_next1_carry_i_17_n_7}),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({readaddr_FSM_next1_carry_i_17_n_8,readaddr_FSM_next1_carry_i_17_n_9,readaddr_FSM_next1_carry_i_17_n_10,readaddr_FSM_next1_carry_i_17_n_11,readaddr_FSM_next1_carry_i_17_n_12,readaddr_FSM_next1_carry_i_17_n_13,readaddr_FSM_next1_carry_i_17_n_14,readaddr_FSM_next1_carry_i_17_n_15}),
        .S(readaddr_count_reg[18:11]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY8 readaddr_FSM_next1_carry_i_18
       (.CI(1'b0),
        .CI_TOP(1'b0),
        .CO({readaddr_FSM_next1_carry_i_18_n_0,readaddr_FSM_next1_carry_i_18_n_1,readaddr_FSM_next1_carry_i_18_n_2,readaddr_FSM_next1_carry_i_18_n_3,readaddr_FSM_next1_carry_i_18_n_4,readaddr_FSM_next1_carry_i_18_n_5,readaddr_FSM_next1_carry_i_18_n_6,readaddr_FSM_next1_carry_i_18_n_7}),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,readaddr_count_reg[4],1'b0}),
        .O({readaddr_FSM_next1_carry_i_18_n_8,readaddr_FSM_next1_carry_i_18_n_9,readaddr_FSM_next1_carry_i_18_n_10,readaddr_FSM_next1_carry_i_18_n_11,readaddr_FSM_next1_carry_i_18_n_12,readaddr_FSM_next1_carry_i_18_n_13,readaddr_FSM_next1_carry_i_18_n_14,NLW_readaddr_FSM_next1_carry_i_18_O_UNCONNECTED[0]}),
        .S({readaddr_count_reg[10:5],readaddr_FSM_next1_carry_i_19_n_0,1'b0}));
  LUT1 #(
    .INIT(2'h1)) 
    readaddr_FSM_next1_carry_i_19
       (.I0(readaddr_count_reg[4]),
        .O(readaddr_FSM_next1_carry_i_19_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    readaddr_FSM_next1_carry_i_2
       (.I0(readaddr_FSM_next1_carry_i_17_n_10),
        .I1(read_blockamount[16]),
        .I2(read_blockamount[17]),
        .I3(readaddr_FSM_next1_carry_i_17_n_9),
        .O(readaddr_FSM_next1_carry_i_2_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    readaddr_FSM_next1_carry_i_3
       (.I0(readaddr_FSM_next1_carry_i_17_n_12),
        .I1(read_blockamount[14]),
        .I2(read_blockamount[15]),
        .I3(readaddr_FSM_next1_carry_i_17_n_11),
        .O(readaddr_FSM_next1_carry_i_3_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    readaddr_FSM_next1_carry_i_4
       (.I0(readaddr_FSM_next1_carry_i_17_n_14),
        .I1(read_blockamount[12]),
        .I2(read_blockamount[13]),
        .I3(readaddr_FSM_next1_carry_i_17_n_13),
        .O(readaddr_FSM_next1_carry_i_4_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    readaddr_FSM_next1_carry_i_5
       (.I0(readaddr_FSM_next1_carry_i_18_n_8),
        .I1(read_blockamount[10]),
        .I2(read_blockamount[11]),
        .I3(readaddr_FSM_next1_carry_i_17_n_15),
        .O(readaddr_FSM_next1_carry_i_5_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    readaddr_FSM_next1_carry_i_6
       (.I0(readaddr_FSM_next1_carry_i_18_n_10),
        .I1(read_blockamount[8]),
        .I2(read_blockamount[9]),
        .I3(readaddr_FSM_next1_carry_i_18_n_9),
        .O(readaddr_FSM_next1_carry_i_6_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    readaddr_FSM_next1_carry_i_7
       (.I0(readaddr_FSM_next1_carry_i_18_n_12),
        .I1(read_blockamount[6]),
        .I2(read_blockamount[7]),
        .I3(readaddr_FSM_next1_carry_i_18_n_11),
        .O(readaddr_FSM_next1_carry_i_7_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    readaddr_FSM_next1_carry_i_8
       (.I0(readaddr_FSM_next1_carry_i_18_n_14),
        .I1(read_blockamount[4]),
        .I2(read_blockamount[5]),
        .I3(readaddr_FSM_next1_carry_i_18_n_13),
        .O(readaddr_FSM_next1_carry_i_8_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    readaddr_FSM_next1_carry_i_9
       (.I0(read_blockamount[17]),
        .I1(readaddr_FSM_next1_carry_i_17_n_9),
        .I2(readaddr_FSM_next1_carry_i_17_n_10),
        .I3(read_blockamount[16]),
        .O(readaddr_FSM_next1_carry_i_9_n_0));
  LUT5 #(
    .INIT(32'h02000000)) 
    \readaddr_count[4]_i_1 
       (.I0(readaddr_FSM_next[0]),
        .I1(\FSM_sequential_readaddr_FSM_next_reg_n_0_[1] ),
        .I2(readaddr_FSM_next[2]),
        .I3(AXI_ARREADY),
        .I4(AXI_ARVALID_reg_0),
        .O(readaddr_count));
  LUT1 #(
    .INIT(2'h1)) 
    \readaddr_count[4]_i_3 
       (.I0(readaddr_count_reg[4]),
        .O(\readaddr_count[4]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \readaddr_count_reg[10] 
       (.C(clk),
        .CE(readaddr_count),
        .D(\readaddr_count_reg[4]_i_2_n_9 ),
        .Q(readaddr_count_reg[10]),
        .R(AXI_ARADDR0));
  FDRE #(
    .INIT(1'b0)) 
    \readaddr_count_reg[11] 
       (.C(clk),
        .CE(readaddr_count),
        .D(\readaddr_count_reg[4]_i_2_n_8 ),
        .Q(readaddr_count_reg[11]),
        .R(AXI_ARADDR0));
  FDRE #(
    .INIT(1'b0)) 
    \readaddr_count_reg[12] 
       (.C(clk),
        .CE(readaddr_count),
        .D(\readaddr_count_reg[12]_i_1_n_15 ),
        .Q(readaddr_count_reg[12]),
        .R(AXI_ARADDR0));
  (* ADDER_THRESHOLD = "16" *) 
  CARRY8 \readaddr_count_reg[12]_i_1 
       (.CI(\readaddr_count_reg[4]_i_2_n_0 ),
        .CI_TOP(1'b0),
        .CO({\readaddr_count_reg[12]_i_1_n_0 ,\readaddr_count_reg[12]_i_1_n_1 ,\readaddr_count_reg[12]_i_1_n_2 ,\readaddr_count_reg[12]_i_1_n_3 ,\readaddr_count_reg[12]_i_1_n_4 ,\readaddr_count_reg[12]_i_1_n_5 ,\readaddr_count_reg[12]_i_1_n_6 ,\readaddr_count_reg[12]_i_1_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({\readaddr_count_reg[12]_i_1_n_8 ,\readaddr_count_reg[12]_i_1_n_9 ,\readaddr_count_reg[12]_i_1_n_10 ,\readaddr_count_reg[12]_i_1_n_11 ,\readaddr_count_reg[12]_i_1_n_12 ,\readaddr_count_reg[12]_i_1_n_13 ,\readaddr_count_reg[12]_i_1_n_14 ,\readaddr_count_reg[12]_i_1_n_15 }),
        .S(readaddr_count_reg[19:12]));
  FDRE #(
    .INIT(1'b0)) 
    \readaddr_count_reg[13] 
       (.C(clk),
        .CE(readaddr_count),
        .D(\readaddr_count_reg[12]_i_1_n_14 ),
        .Q(readaddr_count_reg[13]),
        .R(AXI_ARADDR0));
  FDRE #(
    .INIT(1'b0)) 
    \readaddr_count_reg[14] 
       (.C(clk),
        .CE(readaddr_count),
        .D(\readaddr_count_reg[12]_i_1_n_13 ),
        .Q(readaddr_count_reg[14]),
        .R(AXI_ARADDR0));
  FDRE #(
    .INIT(1'b0)) 
    \readaddr_count_reg[15] 
       (.C(clk),
        .CE(readaddr_count),
        .D(\readaddr_count_reg[12]_i_1_n_12 ),
        .Q(readaddr_count_reg[15]),
        .R(AXI_ARADDR0));
  FDRE #(
    .INIT(1'b0)) 
    \readaddr_count_reg[16] 
       (.C(clk),
        .CE(readaddr_count),
        .D(\readaddr_count_reg[12]_i_1_n_11 ),
        .Q(readaddr_count_reg[16]),
        .R(AXI_ARADDR0));
  FDRE #(
    .INIT(1'b0)) 
    \readaddr_count_reg[17] 
       (.C(clk),
        .CE(readaddr_count),
        .D(\readaddr_count_reg[12]_i_1_n_10 ),
        .Q(readaddr_count_reg[17]),
        .R(AXI_ARADDR0));
  FDRE #(
    .INIT(1'b0)) 
    \readaddr_count_reg[18] 
       (.C(clk),
        .CE(readaddr_count),
        .D(\readaddr_count_reg[12]_i_1_n_9 ),
        .Q(readaddr_count_reg[18]),
        .R(AXI_ARADDR0));
  FDRE #(
    .INIT(1'b0)) 
    \readaddr_count_reg[19] 
       (.C(clk),
        .CE(readaddr_count),
        .D(\readaddr_count_reg[12]_i_1_n_8 ),
        .Q(readaddr_count_reg[19]),
        .R(AXI_ARADDR0));
  FDRE #(
    .INIT(1'b0)) 
    \readaddr_count_reg[20] 
       (.C(clk),
        .CE(readaddr_count),
        .D(\readaddr_count_reg[20]_i_1_n_15 ),
        .Q(readaddr_count_reg[20]),
        .R(AXI_ARADDR0));
  (* ADDER_THRESHOLD = "16" *) 
  CARRY8 \readaddr_count_reg[20]_i_1 
       (.CI(\readaddr_count_reg[12]_i_1_n_0 ),
        .CI_TOP(1'b0),
        .CO({\readaddr_count_reg[20]_i_1_n_0 ,\readaddr_count_reg[20]_i_1_n_1 ,\readaddr_count_reg[20]_i_1_n_2 ,\readaddr_count_reg[20]_i_1_n_3 ,\readaddr_count_reg[20]_i_1_n_4 ,\readaddr_count_reg[20]_i_1_n_5 ,\readaddr_count_reg[20]_i_1_n_6 ,\readaddr_count_reg[20]_i_1_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({\readaddr_count_reg[20]_i_1_n_8 ,\readaddr_count_reg[20]_i_1_n_9 ,\readaddr_count_reg[20]_i_1_n_10 ,\readaddr_count_reg[20]_i_1_n_11 ,\readaddr_count_reg[20]_i_1_n_12 ,\readaddr_count_reg[20]_i_1_n_13 ,\readaddr_count_reg[20]_i_1_n_14 ,\readaddr_count_reg[20]_i_1_n_15 }),
        .S(readaddr_count_reg[27:20]));
  FDRE #(
    .INIT(1'b0)) 
    \readaddr_count_reg[21] 
       (.C(clk),
        .CE(readaddr_count),
        .D(\readaddr_count_reg[20]_i_1_n_14 ),
        .Q(readaddr_count_reg[21]),
        .R(AXI_ARADDR0));
  FDRE #(
    .INIT(1'b0)) 
    \readaddr_count_reg[22] 
       (.C(clk),
        .CE(readaddr_count),
        .D(\readaddr_count_reg[20]_i_1_n_13 ),
        .Q(readaddr_count_reg[22]),
        .R(AXI_ARADDR0));
  FDRE #(
    .INIT(1'b0)) 
    \readaddr_count_reg[23] 
       (.C(clk),
        .CE(readaddr_count),
        .D(\readaddr_count_reg[20]_i_1_n_12 ),
        .Q(readaddr_count_reg[23]),
        .R(AXI_ARADDR0));
  FDRE #(
    .INIT(1'b0)) 
    \readaddr_count_reg[24] 
       (.C(clk),
        .CE(readaddr_count),
        .D(\readaddr_count_reg[20]_i_1_n_11 ),
        .Q(readaddr_count_reg[24]),
        .R(AXI_ARADDR0));
  FDRE #(
    .INIT(1'b0)) 
    \readaddr_count_reg[25] 
       (.C(clk),
        .CE(readaddr_count),
        .D(\readaddr_count_reg[20]_i_1_n_10 ),
        .Q(readaddr_count_reg[25]),
        .R(AXI_ARADDR0));
  FDRE #(
    .INIT(1'b0)) 
    \readaddr_count_reg[26] 
       (.C(clk),
        .CE(readaddr_count),
        .D(\readaddr_count_reg[20]_i_1_n_9 ),
        .Q(readaddr_count_reg[26]),
        .R(AXI_ARADDR0));
  FDRE #(
    .INIT(1'b0)) 
    \readaddr_count_reg[27] 
       (.C(clk),
        .CE(readaddr_count),
        .D(\readaddr_count_reg[20]_i_1_n_8 ),
        .Q(readaddr_count_reg[27]),
        .R(AXI_ARADDR0));
  FDRE #(
    .INIT(1'b0)) 
    \readaddr_count_reg[28] 
       (.C(clk),
        .CE(readaddr_count),
        .D(\readaddr_count_reg[28]_i_1_n_15 ),
        .Q(readaddr_count_reg[28]),
        .R(AXI_ARADDR0));
  (* ADDER_THRESHOLD = "16" *) 
  CARRY8 \readaddr_count_reg[28]_i_1 
       (.CI(\readaddr_count_reg[20]_i_1_n_0 ),
        .CI_TOP(1'b0),
        .CO({\NLW_readaddr_count_reg[28]_i_1_CO_UNCONNECTED [7:3],\readaddr_count_reg[28]_i_1_n_5 ,\readaddr_count_reg[28]_i_1_n_6 ,\readaddr_count_reg[28]_i_1_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_readaddr_count_reg[28]_i_1_O_UNCONNECTED [7:4],\readaddr_count_reg[28]_i_1_n_12 ,\readaddr_count_reg[28]_i_1_n_13 ,\readaddr_count_reg[28]_i_1_n_14 ,\readaddr_count_reg[28]_i_1_n_15 }),
        .S({1'b0,1'b0,1'b0,1'b0,readaddr_count_reg[31:28]}));
  FDRE #(
    .INIT(1'b0)) 
    \readaddr_count_reg[29] 
       (.C(clk),
        .CE(readaddr_count),
        .D(\readaddr_count_reg[28]_i_1_n_14 ),
        .Q(readaddr_count_reg[29]),
        .R(AXI_ARADDR0));
  FDRE #(
    .INIT(1'b0)) 
    \readaddr_count_reg[30] 
       (.C(clk),
        .CE(readaddr_count),
        .D(\readaddr_count_reg[28]_i_1_n_13 ),
        .Q(readaddr_count_reg[30]),
        .R(AXI_ARADDR0));
  FDRE #(
    .INIT(1'b0)) 
    \readaddr_count_reg[31] 
       (.C(clk),
        .CE(readaddr_count),
        .D(\readaddr_count_reg[28]_i_1_n_12 ),
        .Q(readaddr_count_reg[31]),
        .R(AXI_ARADDR0));
  FDRE #(
    .INIT(1'b0)) 
    \readaddr_count_reg[4] 
       (.C(clk),
        .CE(readaddr_count),
        .D(\readaddr_count_reg[4]_i_2_n_15 ),
        .Q(readaddr_count_reg[4]),
        .R(AXI_ARADDR0));
  (* ADDER_THRESHOLD = "16" *) 
  CARRY8 \readaddr_count_reg[4]_i_2 
       (.CI(1'b0),
        .CI_TOP(1'b0),
        .CO({\readaddr_count_reg[4]_i_2_n_0 ,\readaddr_count_reg[4]_i_2_n_1 ,\readaddr_count_reg[4]_i_2_n_2 ,\readaddr_count_reg[4]_i_2_n_3 ,\readaddr_count_reg[4]_i_2_n_4 ,\readaddr_count_reg[4]_i_2_n_5 ,\readaddr_count_reg[4]_i_2_n_6 ,\readaddr_count_reg[4]_i_2_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1}),
        .O({\readaddr_count_reg[4]_i_2_n_8 ,\readaddr_count_reg[4]_i_2_n_9 ,\readaddr_count_reg[4]_i_2_n_10 ,\readaddr_count_reg[4]_i_2_n_11 ,\readaddr_count_reg[4]_i_2_n_12 ,\readaddr_count_reg[4]_i_2_n_13 ,\readaddr_count_reg[4]_i_2_n_14 ,\readaddr_count_reg[4]_i_2_n_15 }),
        .S({readaddr_count_reg[11:5],\readaddr_count[4]_i_3_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \readaddr_count_reg[5] 
       (.C(clk),
        .CE(readaddr_count),
        .D(\readaddr_count_reg[4]_i_2_n_14 ),
        .Q(readaddr_count_reg[5]),
        .R(AXI_ARADDR0));
  FDRE #(
    .INIT(1'b0)) 
    \readaddr_count_reg[6] 
       (.C(clk),
        .CE(readaddr_count),
        .D(\readaddr_count_reg[4]_i_2_n_13 ),
        .Q(readaddr_count_reg[6]),
        .R(AXI_ARADDR0));
  FDRE #(
    .INIT(1'b0)) 
    \readaddr_count_reg[7] 
       (.C(clk),
        .CE(readaddr_count),
        .D(\readaddr_count_reg[4]_i_2_n_12 ),
        .Q(readaddr_count_reg[7]),
        .R(AXI_ARADDR0));
  FDRE #(
    .INIT(1'b0)) 
    \readaddr_count_reg[8] 
       (.C(clk),
        .CE(readaddr_count),
        .D(\readaddr_count_reg[4]_i_2_n_11 ),
        .Q(readaddr_count_reg[8]),
        .R(AXI_ARADDR0));
  FDRE #(
    .INIT(1'b0)) 
    \readaddr_count_reg[9] 
       (.C(clk),
        .CE(readaddr_count),
        .D(\readaddr_count_reg[4]_i_2_n_10 ),
        .Q(readaddr_count_reg[9]),
        .R(AXI_ARADDR0));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'hFE40)) 
    readaddr_done_i_1
       (.I0(readaddr_FSM_next[2]),
        .I1(\FSM_sequential_readaddr_FSM_next_reg_n_0_[1] ),
        .I2(readaddr_FSM_next[0]),
        .I3(readaddr_done_reg_n_0),
        .O(readaddr_done_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    readaddr_done_reg
       (.C(clk),
        .CE(1'b1),
        .D(readaddr_done_i_1_n_0),
        .Q(readaddr_done_reg_n_0),
        .R(1'b0));
  LUT3 #(
    .INIT(8'h02)) 
    \readdata_count[0]_i_1 
       (.I0(\FSM_sequential_readdata_FSM_next_reg_n_0_[1] ),
        .I1(readdata_FSM_next[0]),
        .I2(readdata_FSM_next[2]),
        .O(readdata_count0));
  LUT5 #(
    .INIT(32'h40400040)) 
    \readdata_count[0]_i_2 
       (.I0(readdata_FSM_next[2]),
        .I1(AXI_RVALID),
        .I2(FIFO_WRITE_tready),
        .I3(\FSM_sequential_readdata_FSM_next_reg_n_0_[1] ),
        .I4(readdata_FSM_next[0]),
        .O(readdata_count));
  LUT1 #(
    .INIT(2'h1)) 
    \readdata_count[0]_i_4 
       (.I0(readdata_count_reg[0]),
        .O(\readdata_count[0]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \readdata_count_reg[0] 
       (.C(clk),
        .CE(readdata_count),
        .D(\readdata_count_reg[0]_i_3_n_15 ),
        .Q(readdata_count_reg[0]),
        .R(readdata_count0));
  (* ADDER_THRESHOLD = "16" *) 
  CARRY8 \readdata_count_reg[0]_i_3 
       (.CI(1'b0),
        .CI_TOP(1'b0),
        .CO({\readdata_count_reg[0]_i_3_n_0 ,\readdata_count_reg[0]_i_3_n_1 ,\readdata_count_reg[0]_i_3_n_2 ,\readdata_count_reg[0]_i_3_n_3 ,\readdata_count_reg[0]_i_3_n_4 ,\readdata_count_reg[0]_i_3_n_5 ,\readdata_count_reg[0]_i_3_n_6 ,\readdata_count_reg[0]_i_3_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1}),
        .O({\readdata_count_reg[0]_i_3_n_8 ,\readdata_count_reg[0]_i_3_n_9 ,\readdata_count_reg[0]_i_3_n_10 ,\readdata_count_reg[0]_i_3_n_11 ,\readdata_count_reg[0]_i_3_n_12 ,\readdata_count_reg[0]_i_3_n_13 ,\readdata_count_reg[0]_i_3_n_14 ,\readdata_count_reg[0]_i_3_n_15 }),
        .S({readdata_count_reg[7:1],\readdata_count[0]_i_4_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \readdata_count_reg[10] 
       (.C(clk),
        .CE(readdata_count),
        .D(\readdata_count_reg[8]_i_1_n_13 ),
        .Q(readdata_count_reg[10]),
        .R(readdata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \readdata_count_reg[11] 
       (.C(clk),
        .CE(readdata_count),
        .D(\readdata_count_reg[8]_i_1_n_12 ),
        .Q(readdata_count_reg[11]),
        .R(readdata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \readdata_count_reg[12] 
       (.C(clk),
        .CE(readdata_count),
        .D(\readdata_count_reg[8]_i_1_n_11 ),
        .Q(readdata_count_reg[12]),
        .R(readdata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \readdata_count_reg[13] 
       (.C(clk),
        .CE(readdata_count),
        .D(\readdata_count_reg[8]_i_1_n_10 ),
        .Q(readdata_count_reg[13]),
        .R(readdata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \readdata_count_reg[14] 
       (.C(clk),
        .CE(readdata_count),
        .D(\readdata_count_reg[8]_i_1_n_9 ),
        .Q(readdata_count_reg[14]),
        .R(readdata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \readdata_count_reg[15] 
       (.C(clk),
        .CE(readdata_count),
        .D(\readdata_count_reg[8]_i_1_n_8 ),
        .Q(readdata_count_reg[15]),
        .R(readdata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \readdata_count_reg[16] 
       (.C(clk),
        .CE(readdata_count),
        .D(\readdata_count_reg[16]_i_1_n_15 ),
        .Q(readdata_count_reg[16]),
        .R(readdata_count0));
  (* ADDER_THRESHOLD = "16" *) 
  CARRY8 \readdata_count_reg[16]_i_1 
       (.CI(\readdata_count_reg[8]_i_1_n_0 ),
        .CI_TOP(1'b0),
        .CO({\readdata_count_reg[16]_i_1_n_0 ,\readdata_count_reg[16]_i_1_n_1 ,\readdata_count_reg[16]_i_1_n_2 ,\readdata_count_reg[16]_i_1_n_3 ,\readdata_count_reg[16]_i_1_n_4 ,\readdata_count_reg[16]_i_1_n_5 ,\readdata_count_reg[16]_i_1_n_6 ,\readdata_count_reg[16]_i_1_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({\readdata_count_reg[16]_i_1_n_8 ,\readdata_count_reg[16]_i_1_n_9 ,\readdata_count_reg[16]_i_1_n_10 ,\readdata_count_reg[16]_i_1_n_11 ,\readdata_count_reg[16]_i_1_n_12 ,\readdata_count_reg[16]_i_1_n_13 ,\readdata_count_reg[16]_i_1_n_14 ,\readdata_count_reg[16]_i_1_n_15 }),
        .S(readdata_count_reg[23:16]));
  FDRE #(
    .INIT(1'b0)) 
    \readdata_count_reg[17] 
       (.C(clk),
        .CE(readdata_count),
        .D(\readdata_count_reg[16]_i_1_n_14 ),
        .Q(readdata_count_reg[17]),
        .R(readdata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \readdata_count_reg[18] 
       (.C(clk),
        .CE(readdata_count),
        .D(\readdata_count_reg[16]_i_1_n_13 ),
        .Q(readdata_count_reg[18]),
        .R(readdata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \readdata_count_reg[19] 
       (.C(clk),
        .CE(readdata_count),
        .D(\readdata_count_reg[16]_i_1_n_12 ),
        .Q(readdata_count_reg[19]),
        .R(readdata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \readdata_count_reg[1] 
       (.C(clk),
        .CE(readdata_count),
        .D(\readdata_count_reg[0]_i_3_n_14 ),
        .Q(readdata_count_reg[1]),
        .R(readdata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \readdata_count_reg[20] 
       (.C(clk),
        .CE(readdata_count),
        .D(\readdata_count_reg[16]_i_1_n_11 ),
        .Q(readdata_count_reg[20]),
        .R(readdata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \readdata_count_reg[21] 
       (.C(clk),
        .CE(readdata_count),
        .D(\readdata_count_reg[16]_i_1_n_10 ),
        .Q(readdata_count_reg[21]),
        .R(readdata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \readdata_count_reg[22] 
       (.C(clk),
        .CE(readdata_count),
        .D(\readdata_count_reg[16]_i_1_n_9 ),
        .Q(readdata_count_reg[22]),
        .R(readdata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \readdata_count_reg[23] 
       (.C(clk),
        .CE(readdata_count),
        .D(\readdata_count_reg[16]_i_1_n_8 ),
        .Q(readdata_count_reg[23]),
        .R(readdata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \readdata_count_reg[24] 
       (.C(clk),
        .CE(readdata_count),
        .D(\readdata_count_reg[24]_i_1_n_15 ),
        .Q(readdata_count_reg[24]),
        .R(readdata_count0));
  (* ADDER_THRESHOLD = "16" *) 
  CARRY8 \readdata_count_reg[24]_i_1 
       (.CI(\readdata_count_reg[16]_i_1_n_0 ),
        .CI_TOP(1'b0),
        .CO({\NLW_readdata_count_reg[24]_i_1_CO_UNCONNECTED [7],\readdata_count_reg[24]_i_1_n_1 ,\readdata_count_reg[24]_i_1_n_2 ,\readdata_count_reg[24]_i_1_n_3 ,\readdata_count_reg[24]_i_1_n_4 ,\readdata_count_reg[24]_i_1_n_5 ,\readdata_count_reg[24]_i_1_n_6 ,\readdata_count_reg[24]_i_1_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({\readdata_count_reg[24]_i_1_n_8 ,\readdata_count_reg[24]_i_1_n_9 ,\readdata_count_reg[24]_i_1_n_10 ,\readdata_count_reg[24]_i_1_n_11 ,\readdata_count_reg[24]_i_1_n_12 ,\readdata_count_reg[24]_i_1_n_13 ,\readdata_count_reg[24]_i_1_n_14 ,\readdata_count_reg[24]_i_1_n_15 }),
        .S(readdata_count_reg[31:24]));
  FDRE #(
    .INIT(1'b0)) 
    \readdata_count_reg[25] 
       (.C(clk),
        .CE(readdata_count),
        .D(\readdata_count_reg[24]_i_1_n_14 ),
        .Q(readdata_count_reg[25]),
        .R(readdata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \readdata_count_reg[26] 
       (.C(clk),
        .CE(readdata_count),
        .D(\readdata_count_reg[24]_i_1_n_13 ),
        .Q(readdata_count_reg[26]),
        .R(readdata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \readdata_count_reg[27] 
       (.C(clk),
        .CE(readdata_count),
        .D(\readdata_count_reg[24]_i_1_n_12 ),
        .Q(readdata_count_reg[27]),
        .R(readdata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \readdata_count_reg[28] 
       (.C(clk),
        .CE(readdata_count),
        .D(\readdata_count_reg[24]_i_1_n_11 ),
        .Q(readdata_count_reg[28]),
        .R(readdata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \readdata_count_reg[29] 
       (.C(clk),
        .CE(readdata_count),
        .D(\readdata_count_reg[24]_i_1_n_10 ),
        .Q(readdata_count_reg[29]),
        .R(readdata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \readdata_count_reg[2] 
       (.C(clk),
        .CE(readdata_count),
        .D(\readdata_count_reg[0]_i_3_n_13 ),
        .Q(readdata_count_reg[2]),
        .R(readdata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \readdata_count_reg[30] 
       (.C(clk),
        .CE(readdata_count),
        .D(\readdata_count_reg[24]_i_1_n_9 ),
        .Q(readdata_count_reg[30]),
        .R(readdata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \readdata_count_reg[31] 
       (.C(clk),
        .CE(readdata_count),
        .D(\readdata_count_reg[24]_i_1_n_8 ),
        .Q(readdata_count_reg[31]),
        .R(readdata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \readdata_count_reg[3] 
       (.C(clk),
        .CE(readdata_count),
        .D(\readdata_count_reg[0]_i_3_n_12 ),
        .Q(readdata_count_reg[3]),
        .R(readdata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \readdata_count_reg[4] 
       (.C(clk),
        .CE(readdata_count),
        .D(\readdata_count_reg[0]_i_3_n_11 ),
        .Q(readdata_count_reg[4]),
        .R(readdata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \readdata_count_reg[5] 
       (.C(clk),
        .CE(readdata_count),
        .D(\readdata_count_reg[0]_i_3_n_10 ),
        .Q(readdata_count_reg[5]),
        .R(readdata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \readdata_count_reg[6] 
       (.C(clk),
        .CE(readdata_count),
        .D(\readdata_count_reg[0]_i_3_n_9 ),
        .Q(readdata_count_reg[6]),
        .R(readdata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \readdata_count_reg[7] 
       (.C(clk),
        .CE(readdata_count),
        .D(\readdata_count_reg[0]_i_3_n_8 ),
        .Q(readdata_count_reg[7]),
        .R(readdata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \readdata_count_reg[8] 
       (.C(clk),
        .CE(readdata_count),
        .D(\readdata_count_reg[8]_i_1_n_15 ),
        .Q(readdata_count_reg[8]),
        .R(readdata_count0));
  (* ADDER_THRESHOLD = "16" *) 
  CARRY8 \readdata_count_reg[8]_i_1 
       (.CI(\readdata_count_reg[0]_i_3_n_0 ),
        .CI_TOP(1'b0),
        .CO({\readdata_count_reg[8]_i_1_n_0 ,\readdata_count_reg[8]_i_1_n_1 ,\readdata_count_reg[8]_i_1_n_2 ,\readdata_count_reg[8]_i_1_n_3 ,\readdata_count_reg[8]_i_1_n_4 ,\readdata_count_reg[8]_i_1_n_5 ,\readdata_count_reg[8]_i_1_n_6 ,\readdata_count_reg[8]_i_1_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({\readdata_count_reg[8]_i_1_n_8 ,\readdata_count_reg[8]_i_1_n_9 ,\readdata_count_reg[8]_i_1_n_10 ,\readdata_count_reg[8]_i_1_n_11 ,\readdata_count_reg[8]_i_1_n_12 ,\readdata_count_reg[8]_i_1_n_13 ,\readdata_count_reg[8]_i_1_n_14 ,\readdata_count_reg[8]_i_1_n_15 }),
        .S(readdata_count_reg[15:8]));
  FDRE #(
    .INIT(1'b0)) 
    \readdata_count_reg[9] 
       (.C(clk),
        .CE(readdata_count),
        .D(\readdata_count_reg[8]_i_1_n_14 ),
        .Q(readdata_count_reg[9]),
        .R(readdata_count0));
  LUT4 #(
    .INIT(16'hFE02)) 
    readdata_done_i_1
       (.I0(\FSM_sequential_readdata_FSM_next_reg_n_0_[1] ),
        .I1(readdata_FSM_next[0]),
        .I2(readdata_FSM_next[2]),
        .I3(readdata_done_reg_n_0),
        .O(readdata_done_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    readdata_done_reg
       (.C(clk),
        .CE(1'b1),
        .D(readdata_done_i_1_n_0),
        .Q(readdata_done_reg_n_0),
        .R(1'b0));
  FDRE \write_blockamount_reg[0] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_blockamount[0]),
        .Q(write_blockamount[0]),
        .R(1'b0));
  FDRE \write_blockamount_reg[10] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_blockamount[10]),
        .Q(write_blockamount[10]),
        .R(1'b0));
  FDRE \write_blockamount_reg[11] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_blockamount[11]),
        .Q(write_blockamount[11]),
        .R(1'b0));
  FDRE \write_blockamount_reg[12] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_blockamount[12]),
        .Q(write_blockamount[12]),
        .R(1'b0));
  FDRE \write_blockamount_reg[13] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_blockamount[13]),
        .Q(write_blockamount[13]),
        .R(1'b0));
  FDRE \write_blockamount_reg[14] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_blockamount[14]),
        .Q(write_blockamount[14]),
        .R(1'b0));
  FDRE \write_blockamount_reg[15] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_blockamount[15]),
        .Q(write_blockamount[15]),
        .R(1'b0));
  FDRE \write_blockamount_reg[16] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_blockamount[16]),
        .Q(write_blockamount[16]),
        .R(1'b0));
  FDRE \write_blockamount_reg[17] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_blockamount[17]),
        .Q(write_blockamount[17]),
        .R(1'b0));
  FDRE \write_blockamount_reg[18] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_blockamount[18]),
        .Q(write_blockamount[18]),
        .R(1'b0));
  FDRE \write_blockamount_reg[19] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_blockamount[19]),
        .Q(write_blockamount[19]),
        .R(1'b0));
  FDRE \write_blockamount_reg[1] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_blockamount[1]),
        .Q(write_blockamount[1]),
        .R(1'b0));
  FDRE \write_blockamount_reg[20] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_blockamount[20]),
        .Q(write_blockamount[20]),
        .R(1'b0));
  FDRE \write_blockamount_reg[21] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_blockamount[21]),
        .Q(write_blockamount[21]),
        .R(1'b0));
  FDRE \write_blockamount_reg[22] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_blockamount[22]),
        .Q(write_blockamount[22]),
        .R(1'b0));
  FDRE \write_blockamount_reg[23] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_blockamount[23]),
        .Q(write_blockamount[23]),
        .R(1'b0));
  FDRE \write_blockamount_reg[24] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_blockamount[24]),
        .Q(write_blockamount[24]),
        .R(1'b0));
  FDRE \write_blockamount_reg[25] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_blockamount[25]),
        .Q(write_blockamount[25]),
        .R(1'b0));
  FDRE \write_blockamount_reg[26] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_blockamount[26]),
        .Q(write_blockamount[26]),
        .R(1'b0));
  FDRE \write_blockamount_reg[27] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_blockamount[27]),
        .Q(write_blockamount[27]),
        .R(1'b0));
  FDRE \write_blockamount_reg[28] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_blockamount[28]),
        .Q(write_blockamount[28]),
        .R(1'b0));
  FDRE \write_blockamount_reg[29] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_blockamount[29]),
        .Q(write_blockamount[29]),
        .R(1'b0));
  FDRE \write_blockamount_reg[2] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_blockamount[2]),
        .Q(write_blockamount[2]),
        .R(1'b0));
  FDRE \write_blockamount_reg[30] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_blockamount[30]),
        .Q(write_blockamount[30]),
        .R(1'b0));
  FDRE \write_blockamount_reg[31] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_blockamount[31]),
        .Q(write_blockamount[31]),
        .R(1'b0));
  FDRE \write_blockamount_reg[3] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_blockamount[3]),
        .Q(write_blockamount[3]),
        .R(1'b0));
  FDRE \write_blockamount_reg[4] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_blockamount[4]),
        .Q(write_blockamount[4]),
        .R(1'b0));
  FDRE \write_blockamount_reg[5] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_blockamount[5]),
        .Q(write_blockamount[5]),
        .R(1'b0));
  FDRE \write_blockamount_reg[6] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_blockamount[6]),
        .Q(write_blockamount[6]),
        .R(1'b0));
  FDRE \write_blockamount_reg[7] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_blockamount[7]),
        .Q(write_blockamount[7]),
        .R(1'b0));
  FDRE \write_blockamount_reg[8] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_blockamount[8]),
        .Q(write_blockamount[8]),
        .R(1'b0));
  FDRE \write_blockamount_reg[9] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_blockamount[9]),
        .Q(write_blockamount[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'hF704)) 
    write_start_i_1
       (.I0(FSM_next[2]),
        .I1(FSM_next[0]),
        .I2(FSM_next[1]),
        .I3(write_start_reg_n_0),
        .O(write_start_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    write_start_reg
       (.C(clk),
        .CE(1'b1),
        .D(write_start_i_1_n_0),
        .Q(write_start_reg_n_0),
        .R(1'b0));
  FDRE \write_startaddress_reg[0] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_startaddress[0]),
        .Q(write_startaddress[0]),
        .R(1'b0));
  FDRE \write_startaddress_reg[10] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_startaddress[10]),
        .Q(write_startaddress[10]),
        .R(1'b0));
  FDRE \write_startaddress_reg[11] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_startaddress[11]),
        .Q(write_startaddress[11]),
        .R(1'b0));
  FDRE \write_startaddress_reg[12] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_startaddress[12]),
        .Q(write_startaddress[12]),
        .R(1'b0));
  FDRE \write_startaddress_reg[13] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_startaddress[13]),
        .Q(write_startaddress[13]),
        .R(1'b0));
  FDRE \write_startaddress_reg[14] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_startaddress[14]),
        .Q(write_startaddress[14]),
        .R(1'b0));
  FDRE \write_startaddress_reg[15] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_startaddress[15]),
        .Q(write_startaddress[15]),
        .R(1'b0));
  FDRE \write_startaddress_reg[16] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_startaddress[16]),
        .Q(write_startaddress[16]),
        .R(1'b0));
  FDRE \write_startaddress_reg[17] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_startaddress[17]),
        .Q(write_startaddress[17]),
        .R(1'b0));
  FDRE \write_startaddress_reg[18] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_startaddress[18]),
        .Q(write_startaddress[18]),
        .R(1'b0));
  FDRE \write_startaddress_reg[19] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_startaddress[19]),
        .Q(write_startaddress[19]),
        .R(1'b0));
  FDRE \write_startaddress_reg[1] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_startaddress[1]),
        .Q(write_startaddress[1]),
        .R(1'b0));
  FDRE \write_startaddress_reg[20] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_startaddress[20]),
        .Q(write_startaddress[20]),
        .R(1'b0));
  FDRE \write_startaddress_reg[21] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_startaddress[21]),
        .Q(write_startaddress[21]),
        .R(1'b0));
  FDRE \write_startaddress_reg[22] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_startaddress[22]),
        .Q(write_startaddress[22]),
        .R(1'b0));
  FDRE \write_startaddress_reg[23] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_startaddress[23]),
        .Q(write_startaddress[23]),
        .R(1'b0));
  FDRE \write_startaddress_reg[24] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_startaddress[24]),
        .Q(write_startaddress[24]),
        .R(1'b0));
  FDRE \write_startaddress_reg[25] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_startaddress[25]),
        .Q(write_startaddress[25]),
        .R(1'b0));
  FDRE \write_startaddress_reg[26] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_startaddress[26]),
        .Q(write_startaddress[26]),
        .R(1'b0));
  FDRE \write_startaddress_reg[27] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_startaddress[27]),
        .Q(write_startaddress[27]),
        .R(1'b0));
  FDRE \write_startaddress_reg[28] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_startaddress[28]),
        .Q(write_startaddress[28]),
        .R(1'b0));
  FDRE \write_startaddress_reg[29] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_startaddress[29]),
        .Q(write_startaddress[29]),
        .R(1'b0));
  FDRE \write_startaddress_reg[2] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_startaddress[2]),
        .Q(write_startaddress[2]),
        .R(1'b0));
  FDRE \write_startaddress_reg[30] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_startaddress[30]),
        .Q(write_startaddress[30]),
        .R(1'b0));
  FDRE \write_startaddress_reg[31] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_startaddress[31]),
        .Q(write_startaddress[31]),
        .R(1'b0));
  FDRE \write_startaddress_reg[3] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_startaddress[3]),
        .Q(write_startaddress[3]),
        .R(1'b0));
  FDRE \write_startaddress_reg[4] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_startaddress[4]),
        .Q(write_startaddress[4]),
        .R(1'b0));
  FDRE \write_startaddress_reg[5] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_startaddress[5]),
        .Q(write_startaddress[5]),
        .R(1'b0));
  FDRE \write_startaddress_reg[6] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_startaddress[6]),
        .Q(write_startaddress[6]),
        .R(1'b0));
  FDRE \write_startaddress_reg[7] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_startaddress[7]),
        .Q(write_startaddress[7]),
        .R(1'b0));
  FDRE \write_startaddress_reg[8] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_startaddress[8]),
        .Q(write_startaddress[8]),
        .R(1'b0));
  FDRE \write_startaddress_reg[9] 
       (.C(clk),
        .CE(write_startaddress_0),
        .D(wr_startaddress[9]),
        .Q(write_startaddress[9]),
        .R(1'b0));
  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY8 writeaddr_FSM_next1_carry
       (.CI(writeaddr_FSM_next1_carry_i_1_n_0),
        .CI_TOP(1'b0),
        .CO({writeaddr_FSM_next1_carry_n_0,writeaddr_FSM_next1_carry_n_1,writeaddr_FSM_next1_carry_n_2,writeaddr_FSM_next1_carry_n_3,writeaddr_FSM_next1_carry_n_4,writeaddr_FSM_next1_carry_n_5,writeaddr_FSM_next1_carry_n_6,writeaddr_FSM_next1_carry_n_7}),
        .DI({writeaddr_FSM_next1_carry_i_2_n_0,writeaddr_FSM_next1_carry_i_3_n_0,writeaddr_FSM_next1_carry_i_4_n_0,writeaddr_FSM_next1_carry_i_5_n_0,writeaddr_FSM_next1_carry_i_6_n_0,writeaddr_FSM_next1_carry_i_7_n_0,writeaddr_FSM_next1_carry_i_8_n_0,1'b0}),
        .O(NLW_writeaddr_FSM_next1_carry_O_UNCONNECTED[7:0]),
        .S({writeaddr_FSM_next1_carry_i_9_n_0,writeaddr_FSM_next1_carry_i_10_n_0,writeaddr_FSM_next1_carry_i_11_n_0,writeaddr_FSM_next1_carry_i_12_n_0,writeaddr_FSM_next1_carry_i_13_n_0,writeaddr_FSM_next1_carry_i_14_n_0,writeaddr_FSM_next1_carry_i_15_n_0,writeaddr_FSM_next1_carry_i_16_n_0}));
  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY8 writeaddr_FSM_next1_carry__0
       (.CI(writeaddr_FSM_next1_carry_n_0),
        .CI_TOP(1'b0),
        .CO({NLW_writeaddr_FSM_next1_carry__0_CO_UNCONNECTED[7],writeaddr_FSM_next15_in,writeaddr_FSM_next1_carry__0_n_2,writeaddr_FSM_next1_carry__0_n_3,writeaddr_FSM_next1_carry__0_n_4,writeaddr_FSM_next1_carry__0_n_5,writeaddr_FSM_next1_carry__0_n_6,writeaddr_FSM_next1_carry__0_n_7}),
        .DI({1'b0,writeaddr_FSM_next1_carry__0_i_1_n_0,writeaddr_FSM_next1_carry__0_i_2_n_0,writeaddr_FSM_next1_carry__0_i_3_n_0,writeaddr_FSM_next1_carry__0_i_4_n_0,writeaddr_FSM_next1_carry__0_i_5_n_0,writeaddr_FSM_next1_carry__0_i_6_n_0,writeaddr_FSM_next1_carry__0_i_7_n_0}),
        .O(NLW_writeaddr_FSM_next1_carry__0_O_UNCONNECTED[7:0]),
        .S({1'b0,writeaddr_FSM_next1_carry__0_i_8_n_0,writeaddr_FSM_next1_carry__0_i_9_n_0,writeaddr_FSM_next1_carry__0_i_10_n_0,writeaddr_FSM_next1_carry__0_i_11_n_0,writeaddr_FSM_next1_carry__0_i_12_n_0,writeaddr_FSM_next1_carry__0_i_13_n_0,writeaddr_FSM_next1_carry__0_i_14_n_0}));
  LUT4 #(
    .INIT(16'h2F02)) 
    writeaddr_FSM_next1_carry__0_i_1
       (.I0(p_0_in[30]),
        .I1(write_blockamount[30]),
        .I2(write_blockamount[31]),
        .I3(p_0_in[31]),
        .O(writeaddr_FSM_next1_carry__0_i_1_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    writeaddr_FSM_next1_carry__0_i_10
       (.I0(write_blockamount[27]),
        .I1(p_0_in[27]),
        .I2(write_blockamount[26]),
        .I3(p_0_in[26]),
        .O(writeaddr_FSM_next1_carry__0_i_10_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    writeaddr_FSM_next1_carry__0_i_11
       (.I0(write_blockamount[25]),
        .I1(p_0_in[25]),
        .I2(write_blockamount[24]),
        .I3(p_0_in[24]),
        .O(writeaddr_FSM_next1_carry__0_i_11_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    writeaddr_FSM_next1_carry__0_i_12
       (.I0(write_blockamount[23]),
        .I1(p_0_in[23]),
        .I2(write_blockamount[22]),
        .I3(p_0_in[22]),
        .O(writeaddr_FSM_next1_carry__0_i_12_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    writeaddr_FSM_next1_carry__0_i_13
       (.I0(write_blockamount[21]),
        .I1(p_0_in[21]),
        .I2(write_blockamount[20]),
        .I3(p_0_in[20]),
        .O(writeaddr_FSM_next1_carry__0_i_13_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    writeaddr_FSM_next1_carry__0_i_14
       (.I0(write_blockamount[19]),
        .I1(p_0_in[19]),
        .I2(write_blockamount[18]),
        .I3(p_0_in[18]),
        .O(writeaddr_FSM_next1_carry__0_i_14_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY8 writeaddr_FSM_next1_carry__0_i_15
       (.CI(writeaddr_FSM_next1_carry__0_i_16_n_0),
        .CI_TOP(1'b0),
        .CO({NLW_writeaddr_FSM_next1_carry__0_i_15_CO_UNCONNECTED[7:4],writeaddr_FSM_next1_carry__0_i_15_n_4,writeaddr_FSM_next1_carry__0_i_15_n_5,writeaddr_FSM_next1_carry__0_i_15_n_6,writeaddr_FSM_next1_carry__0_i_15_n_7}),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_writeaddr_FSM_next1_carry__0_i_15_O_UNCONNECTED[7:5],p_0_in[31:27]}),
        .S({1'b0,1'b0,1'b0,writeaddr_count_reg[31:27]}));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY8 writeaddr_FSM_next1_carry__0_i_16
       (.CI(writeaddr_FSM_next1_carry_i_17_n_0),
        .CI_TOP(1'b0),
        .CO({writeaddr_FSM_next1_carry__0_i_16_n_0,writeaddr_FSM_next1_carry__0_i_16_n_1,writeaddr_FSM_next1_carry__0_i_16_n_2,writeaddr_FSM_next1_carry__0_i_16_n_3,writeaddr_FSM_next1_carry__0_i_16_n_4,writeaddr_FSM_next1_carry__0_i_16_n_5,writeaddr_FSM_next1_carry__0_i_16_n_6,writeaddr_FSM_next1_carry__0_i_16_n_7}),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O(p_0_in[26:19]),
        .S(writeaddr_count_reg[26:19]));
  LUT4 #(
    .INIT(16'h2F02)) 
    writeaddr_FSM_next1_carry__0_i_2
       (.I0(p_0_in[28]),
        .I1(write_blockamount[28]),
        .I2(write_blockamount[29]),
        .I3(p_0_in[29]),
        .O(writeaddr_FSM_next1_carry__0_i_2_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    writeaddr_FSM_next1_carry__0_i_3
       (.I0(p_0_in[26]),
        .I1(write_blockamount[26]),
        .I2(write_blockamount[27]),
        .I3(p_0_in[27]),
        .O(writeaddr_FSM_next1_carry__0_i_3_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    writeaddr_FSM_next1_carry__0_i_4
       (.I0(p_0_in[24]),
        .I1(write_blockamount[24]),
        .I2(write_blockamount[25]),
        .I3(p_0_in[25]),
        .O(writeaddr_FSM_next1_carry__0_i_4_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    writeaddr_FSM_next1_carry__0_i_5
       (.I0(p_0_in[22]),
        .I1(write_blockamount[22]),
        .I2(write_blockamount[23]),
        .I3(p_0_in[23]),
        .O(writeaddr_FSM_next1_carry__0_i_5_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    writeaddr_FSM_next1_carry__0_i_6
       (.I0(p_0_in[20]),
        .I1(write_blockamount[20]),
        .I2(write_blockamount[21]),
        .I3(p_0_in[21]),
        .O(writeaddr_FSM_next1_carry__0_i_6_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    writeaddr_FSM_next1_carry__0_i_7
       (.I0(p_0_in[18]),
        .I1(write_blockamount[18]),
        .I2(write_blockamount[19]),
        .I3(p_0_in[19]),
        .O(writeaddr_FSM_next1_carry__0_i_7_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    writeaddr_FSM_next1_carry__0_i_8
       (.I0(write_blockamount[31]),
        .I1(p_0_in[31]),
        .I2(write_blockamount[30]),
        .I3(p_0_in[30]),
        .O(writeaddr_FSM_next1_carry__0_i_8_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    writeaddr_FSM_next1_carry__0_i_9
       (.I0(write_blockamount[29]),
        .I1(p_0_in[29]),
        .I2(write_blockamount[28]),
        .I3(p_0_in[28]),
        .O(writeaddr_FSM_next1_carry__0_i_9_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    writeaddr_FSM_next1_carry_i_1
       (.I0(write_blockamount[0]),
        .I1(write_blockamount[1]),
        .O(writeaddr_FSM_next1_carry_i_1_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    writeaddr_FSM_next1_carry_i_10
       (.I0(write_blockamount[15]),
        .I1(p_0_in[15]),
        .I2(write_blockamount[14]),
        .I3(p_0_in[14]),
        .O(writeaddr_FSM_next1_carry_i_10_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    writeaddr_FSM_next1_carry_i_11
       (.I0(write_blockamount[13]),
        .I1(p_0_in[13]),
        .I2(write_blockamount[12]),
        .I3(p_0_in[12]),
        .O(writeaddr_FSM_next1_carry_i_11_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    writeaddr_FSM_next1_carry_i_12
       (.I0(write_blockamount[11]),
        .I1(p_0_in[11]),
        .I2(write_blockamount[10]),
        .I3(p_0_in[10]),
        .O(writeaddr_FSM_next1_carry_i_12_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    writeaddr_FSM_next1_carry_i_13
       (.I0(write_blockamount[9]),
        .I1(p_0_in[9]),
        .I2(write_blockamount[8]),
        .I3(p_0_in[8]),
        .O(writeaddr_FSM_next1_carry_i_13_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    writeaddr_FSM_next1_carry_i_14
       (.I0(write_blockamount[7]),
        .I1(p_0_in[7]),
        .I2(write_blockamount[6]),
        .I3(p_0_in[6]),
        .O(writeaddr_FSM_next1_carry_i_14_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    writeaddr_FSM_next1_carry_i_15
       (.I0(write_blockamount[5]),
        .I1(p_0_in[5]),
        .I2(write_blockamount[4]),
        .I3(p_0_in[4]),
        .O(writeaddr_FSM_next1_carry_i_15_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    writeaddr_FSM_next1_carry_i_16
       (.I0(write_blockamount[2]),
        .I1(write_blockamount[3]),
        .O(writeaddr_FSM_next1_carry_i_16_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY8 writeaddr_FSM_next1_carry_i_17
       (.CI(writeaddr_FSM_next1_carry_i_18_n_0),
        .CI_TOP(1'b0),
        .CO({writeaddr_FSM_next1_carry_i_17_n_0,writeaddr_FSM_next1_carry_i_17_n_1,writeaddr_FSM_next1_carry_i_17_n_2,writeaddr_FSM_next1_carry_i_17_n_3,writeaddr_FSM_next1_carry_i_17_n_4,writeaddr_FSM_next1_carry_i_17_n_5,writeaddr_FSM_next1_carry_i_17_n_6,writeaddr_FSM_next1_carry_i_17_n_7}),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O(p_0_in[18:11]),
        .S(writeaddr_count_reg[18:11]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY8 writeaddr_FSM_next1_carry_i_18
       (.CI(1'b0),
        .CI_TOP(1'b0),
        .CO({writeaddr_FSM_next1_carry_i_18_n_0,writeaddr_FSM_next1_carry_i_18_n_1,writeaddr_FSM_next1_carry_i_18_n_2,writeaddr_FSM_next1_carry_i_18_n_3,writeaddr_FSM_next1_carry_i_18_n_4,writeaddr_FSM_next1_carry_i_18_n_5,writeaddr_FSM_next1_carry_i_18_n_6,writeaddr_FSM_next1_carry_i_18_n_7}),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,writeaddr_count_reg[4],1'b0}),
        .O({p_0_in[10:4],NLW_writeaddr_FSM_next1_carry_i_18_O_UNCONNECTED[0]}),
        .S({writeaddr_count_reg[10:5],writeaddr_FSM_next1_carry_i_19_n_0,1'b0}));
  LUT1 #(
    .INIT(2'h1)) 
    writeaddr_FSM_next1_carry_i_19
       (.I0(writeaddr_count_reg[4]),
        .O(writeaddr_FSM_next1_carry_i_19_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    writeaddr_FSM_next1_carry_i_2
       (.I0(p_0_in[16]),
        .I1(write_blockamount[16]),
        .I2(write_blockamount[17]),
        .I3(p_0_in[17]),
        .O(writeaddr_FSM_next1_carry_i_2_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    writeaddr_FSM_next1_carry_i_3
       (.I0(p_0_in[14]),
        .I1(write_blockamount[14]),
        .I2(write_blockamount[15]),
        .I3(p_0_in[15]),
        .O(writeaddr_FSM_next1_carry_i_3_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    writeaddr_FSM_next1_carry_i_4
       (.I0(p_0_in[12]),
        .I1(write_blockamount[12]),
        .I2(write_blockamount[13]),
        .I3(p_0_in[13]),
        .O(writeaddr_FSM_next1_carry_i_4_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    writeaddr_FSM_next1_carry_i_5
       (.I0(p_0_in[10]),
        .I1(write_blockamount[10]),
        .I2(write_blockamount[11]),
        .I3(p_0_in[11]),
        .O(writeaddr_FSM_next1_carry_i_5_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    writeaddr_FSM_next1_carry_i_6
       (.I0(p_0_in[8]),
        .I1(write_blockamount[8]),
        .I2(write_blockamount[9]),
        .I3(p_0_in[9]),
        .O(writeaddr_FSM_next1_carry_i_6_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    writeaddr_FSM_next1_carry_i_7
       (.I0(p_0_in[6]),
        .I1(write_blockamount[6]),
        .I2(write_blockamount[7]),
        .I3(p_0_in[7]),
        .O(writeaddr_FSM_next1_carry_i_7_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    writeaddr_FSM_next1_carry_i_8
       (.I0(p_0_in[4]),
        .I1(write_blockamount[4]),
        .I2(write_blockamount[5]),
        .I3(p_0_in[5]),
        .O(writeaddr_FSM_next1_carry_i_8_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    writeaddr_FSM_next1_carry_i_9
       (.I0(write_blockamount[17]),
        .I1(p_0_in[17]),
        .I2(write_blockamount[16]),
        .I3(p_0_in[16]),
        .O(writeaddr_FSM_next1_carry_i_9_n_0));
  LUT5 #(
    .INIT(32'h02000000)) 
    \writeaddr_count[4]_i_1 
       (.I0(writeaddr_FSM_next[0]),
        .I1(\FSM_sequential_writeaddr_FSM_next_reg_n_0_[1] ),
        .I2(writeaddr_FSM_next[2]),
        .I3(AXI_AWVALID_reg_0),
        .I4(AXI_AWREADY),
        .O(writeaddr_count));
  LUT1 #(
    .INIT(2'h1)) 
    \writeaddr_count[4]_i_3 
       (.I0(writeaddr_count_reg[4]),
        .O(\writeaddr_count[4]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \writeaddr_count_reg[10] 
       (.C(clk),
        .CE(writeaddr_count),
        .D(\writeaddr_count_reg[4]_i_2_n_9 ),
        .Q(writeaddr_count_reg[10]),
        .R(writeaddr_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writeaddr_count_reg[11] 
       (.C(clk),
        .CE(writeaddr_count),
        .D(\writeaddr_count_reg[4]_i_2_n_8 ),
        .Q(writeaddr_count_reg[11]),
        .R(writeaddr_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writeaddr_count_reg[12] 
       (.C(clk),
        .CE(writeaddr_count),
        .D(\writeaddr_count_reg[12]_i_1_n_15 ),
        .Q(writeaddr_count_reg[12]),
        .R(writeaddr_count0));
  (* ADDER_THRESHOLD = "16" *) 
  CARRY8 \writeaddr_count_reg[12]_i_1 
       (.CI(\writeaddr_count_reg[4]_i_2_n_0 ),
        .CI_TOP(1'b0),
        .CO({\writeaddr_count_reg[12]_i_1_n_0 ,\writeaddr_count_reg[12]_i_1_n_1 ,\writeaddr_count_reg[12]_i_1_n_2 ,\writeaddr_count_reg[12]_i_1_n_3 ,\writeaddr_count_reg[12]_i_1_n_4 ,\writeaddr_count_reg[12]_i_1_n_5 ,\writeaddr_count_reg[12]_i_1_n_6 ,\writeaddr_count_reg[12]_i_1_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({\writeaddr_count_reg[12]_i_1_n_8 ,\writeaddr_count_reg[12]_i_1_n_9 ,\writeaddr_count_reg[12]_i_1_n_10 ,\writeaddr_count_reg[12]_i_1_n_11 ,\writeaddr_count_reg[12]_i_1_n_12 ,\writeaddr_count_reg[12]_i_1_n_13 ,\writeaddr_count_reg[12]_i_1_n_14 ,\writeaddr_count_reg[12]_i_1_n_15 }),
        .S(writeaddr_count_reg[19:12]));
  FDRE #(
    .INIT(1'b0)) 
    \writeaddr_count_reg[13] 
       (.C(clk),
        .CE(writeaddr_count),
        .D(\writeaddr_count_reg[12]_i_1_n_14 ),
        .Q(writeaddr_count_reg[13]),
        .R(writeaddr_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writeaddr_count_reg[14] 
       (.C(clk),
        .CE(writeaddr_count),
        .D(\writeaddr_count_reg[12]_i_1_n_13 ),
        .Q(writeaddr_count_reg[14]),
        .R(writeaddr_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writeaddr_count_reg[15] 
       (.C(clk),
        .CE(writeaddr_count),
        .D(\writeaddr_count_reg[12]_i_1_n_12 ),
        .Q(writeaddr_count_reg[15]),
        .R(writeaddr_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writeaddr_count_reg[16] 
       (.C(clk),
        .CE(writeaddr_count),
        .D(\writeaddr_count_reg[12]_i_1_n_11 ),
        .Q(writeaddr_count_reg[16]),
        .R(writeaddr_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writeaddr_count_reg[17] 
       (.C(clk),
        .CE(writeaddr_count),
        .D(\writeaddr_count_reg[12]_i_1_n_10 ),
        .Q(writeaddr_count_reg[17]),
        .R(writeaddr_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writeaddr_count_reg[18] 
       (.C(clk),
        .CE(writeaddr_count),
        .D(\writeaddr_count_reg[12]_i_1_n_9 ),
        .Q(writeaddr_count_reg[18]),
        .R(writeaddr_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writeaddr_count_reg[19] 
       (.C(clk),
        .CE(writeaddr_count),
        .D(\writeaddr_count_reg[12]_i_1_n_8 ),
        .Q(writeaddr_count_reg[19]),
        .R(writeaddr_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writeaddr_count_reg[20] 
       (.C(clk),
        .CE(writeaddr_count),
        .D(\writeaddr_count_reg[20]_i_1_n_15 ),
        .Q(writeaddr_count_reg[20]),
        .R(writeaddr_count0));
  (* ADDER_THRESHOLD = "16" *) 
  CARRY8 \writeaddr_count_reg[20]_i_1 
       (.CI(\writeaddr_count_reg[12]_i_1_n_0 ),
        .CI_TOP(1'b0),
        .CO({\writeaddr_count_reg[20]_i_1_n_0 ,\writeaddr_count_reg[20]_i_1_n_1 ,\writeaddr_count_reg[20]_i_1_n_2 ,\writeaddr_count_reg[20]_i_1_n_3 ,\writeaddr_count_reg[20]_i_1_n_4 ,\writeaddr_count_reg[20]_i_1_n_5 ,\writeaddr_count_reg[20]_i_1_n_6 ,\writeaddr_count_reg[20]_i_1_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({\writeaddr_count_reg[20]_i_1_n_8 ,\writeaddr_count_reg[20]_i_1_n_9 ,\writeaddr_count_reg[20]_i_1_n_10 ,\writeaddr_count_reg[20]_i_1_n_11 ,\writeaddr_count_reg[20]_i_1_n_12 ,\writeaddr_count_reg[20]_i_1_n_13 ,\writeaddr_count_reg[20]_i_1_n_14 ,\writeaddr_count_reg[20]_i_1_n_15 }),
        .S(writeaddr_count_reg[27:20]));
  FDRE #(
    .INIT(1'b0)) 
    \writeaddr_count_reg[21] 
       (.C(clk),
        .CE(writeaddr_count),
        .D(\writeaddr_count_reg[20]_i_1_n_14 ),
        .Q(writeaddr_count_reg[21]),
        .R(writeaddr_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writeaddr_count_reg[22] 
       (.C(clk),
        .CE(writeaddr_count),
        .D(\writeaddr_count_reg[20]_i_1_n_13 ),
        .Q(writeaddr_count_reg[22]),
        .R(writeaddr_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writeaddr_count_reg[23] 
       (.C(clk),
        .CE(writeaddr_count),
        .D(\writeaddr_count_reg[20]_i_1_n_12 ),
        .Q(writeaddr_count_reg[23]),
        .R(writeaddr_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writeaddr_count_reg[24] 
       (.C(clk),
        .CE(writeaddr_count),
        .D(\writeaddr_count_reg[20]_i_1_n_11 ),
        .Q(writeaddr_count_reg[24]),
        .R(writeaddr_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writeaddr_count_reg[25] 
       (.C(clk),
        .CE(writeaddr_count),
        .D(\writeaddr_count_reg[20]_i_1_n_10 ),
        .Q(writeaddr_count_reg[25]),
        .R(writeaddr_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writeaddr_count_reg[26] 
       (.C(clk),
        .CE(writeaddr_count),
        .D(\writeaddr_count_reg[20]_i_1_n_9 ),
        .Q(writeaddr_count_reg[26]),
        .R(writeaddr_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writeaddr_count_reg[27] 
       (.C(clk),
        .CE(writeaddr_count),
        .D(\writeaddr_count_reg[20]_i_1_n_8 ),
        .Q(writeaddr_count_reg[27]),
        .R(writeaddr_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writeaddr_count_reg[28] 
       (.C(clk),
        .CE(writeaddr_count),
        .D(\writeaddr_count_reg[28]_i_1_n_15 ),
        .Q(writeaddr_count_reg[28]),
        .R(writeaddr_count0));
  (* ADDER_THRESHOLD = "16" *) 
  CARRY8 \writeaddr_count_reg[28]_i_1 
       (.CI(\writeaddr_count_reg[20]_i_1_n_0 ),
        .CI_TOP(1'b0),
        .CO({\NLW_writeaddr_count_reg[28]_i_1_CO_UNCONNECTED [7:3],\writeaddr_count_reg[28]_i_1_n_5 ,\writeaddr_count_reg[28]_i_1_n_6 ,\writeaddr_count_reg[28]_i_1_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_writeaddr_count_reg[28]_i_1_O_UNCONNECTED [7:4],\writeaddr_count_reg[28]_i_1_n_12 ,\writeaddr_count_reg[28]_i_1_n_13 ,\writeaddr_count_reg[28]_i_1_n_14 ,\writeaddr_count_reg[28]_i_1_n_15 }),
        .S({1'b0,1'b0,1'b0,1'b0,writeaddr_count_reg[31:28]}));
  FDRE #(
    .INIT(1'b0)) 
    \writeaddr_count_reg[29] 
       (.C(clk),
        .CE(writeaddr_count),
        .D(\writeaddr_count_reg[28]_i_1_n_14 ),
        .Q(writeaddr_count_reg[29]),
        .R(writeaddr_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writeaddr_count_reg[30] 
       (.C(clk),
        .CE(writeaddr_count),
        .D(\writeaddr_count_reg[28]_i_1_n_13 ),
        .Q(writeaddr_count_reg[30]),
        .R(writeaddr_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writeaddr_count_reg[31] 
       (.C(clk),
        .CE(writeaddr_count),
        .D(\writeaddr_count_reg[28]_i_1_n_12 ),
        .Q(writeaddr_count_reg[31]),
        .R(writeaddr_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writeaddr_count_reg[4] 
       (.C(clk),
        .CE(writeaddr_count),
        .D(\writeaddr_count_reg[4]_i_2_n_15 ),
        .Q(writeaddr_count_reg[4]),
        .R(writeaddr_count0));
  (* ADDER_THRESHOLD = "16" *) 
  CARRY8 \writeaddr_count_reg[4]_i_2 
       (.CI(1'b0),
        .CI_TOP(1'b0),
        .CO({\writeaddr_count_reg[4]_i_2_n_0 ,\writeaddr_count_reg[4]_i_2_n_1 ,\writeaddr_count_reg[4]_i_2_n_2 ,\writeaddr_count_reg[4]_i_2_n_3 ,\writeaddr_count_reg[4]_i_2_n_4 ,\writeaddr_count_reg[4]_i_2_n_5 ,\writeaddr_count_reg[4]_i_2_n_6 ,\writeaddr_count_reg[4]_i_2_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1}),
        .O({\writeaddr_count_reg[4]_i_2_n_8 ,\writeaddr_count_reg[4]_i_2_n_9 ,\writeaddr_count_reg[4]_i_2_n_10 ,\writeaddr_count_reg[4]_i_2_n_11 ,\writeaddr_count_reg[4]_i_2_n_12 ,\writeaddr_count_reg[4]_i_2_n_13 ,\writeaddr_count_reg[4]_i_2_n_14 ,\writeaddr_count_reg[4]_i_2_n_15 }),
        .S({writeaddr_count_reg[11:5],\writeaddr_count[4]_i_3_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \writeaddr_count_reg[5] 
       (.C(clk),
        .CE(writeaddr_count),
        .D(\writeaddr_count_reg[4]_i_2_n_14 ),
        .Q(writeaddr_count_reg[5]),
        .R(writeaddr_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writeaddr_count_reg[6] 
       (.C(clk),
        .CE(writeaddr_count),
        .D(\writeaddr_count_reg[4]_i_2_n_13 ),
        .Q(writeaddr_count_reg[6]),
        .R(writeaddr_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writeaddr_count_reg[7] 
       (.C(clk),
        .CE(writeaddr_count),
        .D(\writeaddr_count_reg[4]_i_2_n_12 ),
        .Q(writeaddr_count_reg[7]),
        .R(writeaddr_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writeaddr_count_reg[8] 
       (.C(clk),
        .CE(writeaddr_count),
        .D(\writeaddr_count_reg[4]_i_2_n_11 ),
        .Q(writeaddr_count_reg[8]),
        .R(writeaddr_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writeaddr_count_reg[9] 
       (.C(clk),
        .CE(writeaddr_count),
        .D(\writeaddr_count_reg[4]_i_2_n_10 ),
        .Q(writeaddr_count_reg[9]),
        .R(writeaddr_count0));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'hFE40)) 
    writeaddr_done_i_1
       (.I0(writeaddr_FSM_next[2]),
        .I1(\FSM_sequential_writeaddr_FSM_next_reg_n_0_[1] ),
        .I2(writeaddr_FSM_next[0]),
        .I3(writeaddr_done_reg_n_0),
        .O(writeaddr_done_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    writeaddr_done_reg
       (.C(clk),
        .CE(1'b1),
        .D(writeaddr_done_i_1_n_0),
        .Q(writeaddr_done_reg_n_0),
        .R(1'b0));
  CARRY8 writedata_FSM_next1_carry
       (.CI(1'b1),
        .CI_TOP(1'b0),
        .CO({writedata_FSM_next1_carry_n_0,writedata_FSM_next1_carry_n_1,writedata_FSM_next1_carry_n_2,writedata_FSM_next1_carry_n_3,writedata_FSM_next1_carry_n_4,writedata_FSM_next1_carry_n_5,writedata_FSM_next1_carry_n_6,writedata_FSM_next1_carry_n_7}),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_writedata_FSM_next1_carry_O_UNCONNECTED[7:0]),
        .S({writedata_FSM_next1_carry_i_1_n_0,writedata_FSM_next1_carry_i_2_n_0,writedata_FSM_next1_carry_i_3_n_0,writedata_FSM_next1_carry_i_4_n_0,writedata_FSM_next1_carry_i_5_n_0,writedata_FSM_next1_carry_i_6_n_0,writedata_FSM_next1_carry_i_7_n_0,writedata_FSM_next1_carry_i_8_n_0}));
  CARRY8 writedata_FSM_next1_carry__0
       (.CI(writedata_FSM_next1_carry_n_0),
        .CI_TOP(1'b0),
        .CO({NLW_writedata_FSM_next1_carry__0_CO_UNCONNECTED[7:3],writedata_FSM_next1,writedata_FSM_next1_carry__0_n_6,writedata_FSM_next1_carry__0_n_7}),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_writedata_FSM_next1_carry__0_O_UNCONNECTED[7:0]),
        .S({1'b0,1'b0,1'b0,1'b0,1'b0,writedata_FSM_next1_carry__0_i_1_n_0,writedata_FSM_next1_carry__0_i_2_n_0,writedata_FSM_next1_carry__0_i_3_n_0}));
  LUT4 #(
    .INIT(16'h9009)) 
    writedata_FSM_next1_carry__0_i_1
       (.I0(writedata_count_reg__0[30]),
        .I1(writedata_FSM_next2[30]),
        .I2(writedata_count_reg__0[31]),
        .I3(writedata_FSM_next2[31]),
        .O(writedata_FSM_next1_carry__0_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    writedata_FSM_next1_carry__0_i_10
       (.I0(write_blockamount[26]),
        .O(writedata_FSM_next1_carry__0_i_10_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    writedata_FSM_next1_carry__0_i_11
       (.I0(write_blockamount[25]),
        .O(writedata_FSM_next1_carry__0_i_11_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    writedata_FSM_next1_carry__0_i_2
       (.I0(writedata_count_reg__0[28]),
        .I1(writedata_FSM_next2[28]),
        .I2(writedata_count_reg__0[27]),
        .I3(writedata_FSM_next2[27]),
        .I4(writedata_FSM_next2[29]),
        .I5(writedata_count_reg__0[29]),
        .O(writedata_FSM_next1_carry__0_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    writedata_FSM_next1_carry__0_i_3
       (.I0(writedata_count_reg__0[25]),
        .I1(writedata_FSM_next2[25]),
        .I2(writedata_count_reg__0[24]),
        .I3(writedata_FSM_next2[24]),
        .I4(writedata_FSM_next2[26]),
        .I5(writedata_count_reg__0[26]),
        .O(writedata_FSM_next1_carry__0_i_3_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY8 writedata_FSM_next1_carry__0_i_4
       (.CI(writedata_FSM_next1_carry_i_9_n_0),
        .CI_TOP(1'b0),
        .CO({NLW_writedata_FSM_next1_carry__0_i_4_CO_UNCONNECTED[7:6],writedata_FSM_next1_carry__0_i_4_n_2,writedata_FSM_next1_carry__0_i_4_n_3,writedata_FSM_next1_carry__0_i_4_n_4,writedata_FSM_next1_carry__0_i_4_n_5,writedata_FSM_next1_carry__0_i_4_n_6,writedata_FSM_next1_carry__0_i_4_n_7}),
        .DI({1'b0,1'b0,write_blockamount[30:25]}),
        .O({NLW_writedata_FSM_next1_carry__0_i_4_O_UNCONNECTED[7],writedata_FSM_next2[31:25]}),
        .S({1'b0,writedata_FSM_next1_carry__0_i_5_n_0,writedata_FSM_next1_carry__0_i_6_n_0,writedata_FSM_next1_carry__0_i_7_n_0,writedata_FSM_next1_carry__0_i_8_n_0,writedata_FSM_next1_carry__0_i_9_n_0,writedata_FSM_next1_carry__0_i_10_n_0,writedata_FSM_next1_carry__0_i_11_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    writedata_FSM_next1_carry__0_i_5
       (.I0(write_blockamount[31]),
        .O(writedata_FSM_next1_carry__0_i_5_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    writedata_FSM_next1_carry__0_i_6
       (.I0(write_blockamount[30]),
        .O(writedata_FSM_next1_carry__0_i_6_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    writedata_FSM_next1_carry__0_i_7
       (.I0(write_blockamount[29]),
        .O(writedata_FSM_next1_carry__0_i_7_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    writedata_FSM_next1_carry__0_i_8
       (.I0(write_blockamount[28]),
        .O(writedata_FSM_next1_carry__0_i_8_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    writedata_FSM_next1_carry__0_i_9
       (.I0(write_blockamount[27]),
        .O(writedata_FSM_next1_carry__0_i_9_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    writedata_FSM_next1_carry_i_1
       (.I0(writedata_count_reg__0[22]),
        .I1(writedata_FSM_next2[22]),
        .I2(writedata_count_reg__0[21]),
        .I3(writedata_FSM_next2[21]),
        .I4(writedata_FSM_next2[23]),
        .I5(writedata_count_reg__0[23]),
        .O(writedata_FSM_next1_carry_i_1_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY8 writedata_FSM_next1_carry_i_10
       (.CI(writedata_FSM_next1_carry_i_11_n_0),
        .CI_TOP(1'b0),
        .CO({writedata_FSM_next1_carry_i_10_n_0,writedata_FSM_next1_carry_i_10_n_1,writedata_FSM_next1_carry_i_10_n_2,writedata_FSM_next1_carry_i_10_n_3,writedata_FSM_next1_carry_i_10_n_4,writedata_FSM_next1_carry_i_10_n_5,writedata_FSM_next1_carry_i_10_n_6,writedata_FSM_next1_carry_i_10_n_7}),
        .DI(write_blockamount[16:9]),
        .O(writedata_FSM_next2[16:9]),
        .S({writedata_FSM_next1_carry_i_20_n_0,writedata_FSM_next1_carry_i_21_n_0,writedata_FSM_next1_carry_i_22_n_0,writedata_FSM_next1_carry_i_23_n_0,writedata_FSM_next1_carry_i_24_n_0,writedata_FSM_next1_carry_i_25_n_0,writedata_FSM_next1_carry_i_26_n_0,writedata_FSM_next1_carry_i_27_n_0}));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY8 writedata_FSM_next1_carry_i_11
       (.CI(write_blockamount[0]),
        .CI_TOP(1'b0),
        .CO({writedata_FSM_next1_carry_i_11_n_0,writedata_FSM_next1_carry_i_11_n_1,writedata_FSM_next1_carry_i_11_n_2,writedata_FSM_next1_carry_i_11_n_3,writedata_FSM_next1_carry_i_11_n_4,writedata_FSM_next1_carry_i_11_n_5,writedata_FSM_next1_carry_i_11_n_6,writedata_FSM_next1_carry_i_11_n_7}),
        .DI(write_blockamount[8:1]),
        .O(writedata_FSM_next2[8:1]),
        .S({writedata_FSM_next1_carry_i_28_n_0,writedata_FSM_next1_carry_i_29_n_0,writedata_FSM_next1_carry_i_30_n_0,writedata_FSM_next1_carry_i_31_n_0,writedata_FSM_next1_carry_i_32_n_0,writedata_FSM_next1_carry_i_33_n_0,writedata_FSM_next1_carry_i_34_n_0,writedata_FSM_next1_carry_i_35_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    writedata_FSM_next1_carry_i_12
       (.I0(write_blockamount[24]),
        .O(writedata_FSM_next1_carry_i_12_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    writedata_FSM_next1_carry_i_13
       (.I0(write_blockamount[23]),
        .O(writedata_FSM_next1_carry_i_13_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    writedata_FSM_next1_carry_i_14
       (.I0(write_blockamount[22]),
        .O(writedata_FSM_next1_carry_i_14_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    writedata_FSM_next1_carry_i_15
       (.I0(write_blockamount[21]),
        .O(writedata_FSM_next1_carry_i_15_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    writedata_FSM_next1_carry_i_16
       (.I0(write_blockamount[20]),
        .O(writedata_FSM_next1_carry_i_16_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    writedata_FSM_next1_carry_i_17
       (.I0(write_blockamount[19]),
        .O(writedata_FSM_next1_carry_i_17_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    writedata_FSM_next1_carry_i_18
       (.I0(write_blockamount[18]),
        .O(writedata_FSM_next1_carry_i_18_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    writedata_FSM_next1_carry_i_19
       (.I0(write_blockamount[17]),
        .O(writedata_FSM_next1_carry_i_19_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    writedata_FSM_next1_carry_i_2
       (.I0(writedata_count_reg__0[19]),
        .I1(writedata_FSM_next2[19]),
        .I2(writedata_count_reg__0[18]),
        .I3(writedata_FSM_next2[18]),
        .I4(writedata_FSM_next2[20]),
        .I5(writedata_count_reg__0[20]),
        .O(writedata_FSM_next1_carry_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    writedata_FSM_next1_carry_i_20
       (.I0(write_blockamount[16]),
        .O(writedata_FSM_next1_carry_i_20_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    writedata_FSM_next1_carry_i_21
       (.I0(write_blockamount[15]),
        .O(writedata_FSM_next1_carry_i_21_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    writedata_FSM_next1_carry_i_22
       (.I0(write_blockamount[14]),
        .O(writedata_FSM_next1_carry_i_22_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    writedata_FSM_next1_carry_i_23
       (.I0(write_blockamount[13]),
        .O(writedata_FSM_next1_carry_i_23_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    writedata_FSM_next1_carry_i_24
       (.I0(write_blockamount[12]),
        .O(writedata_FSM_next1_carry_i_24_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    writedata_FSM_next1_carry_i_25
       (.I0(write_blockamount[11]),
        .O(writedata_FSM_next1_carry_i_25_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    writedata_FSM_next1_carry_i_26
       (.I0(write_blockamount[10]),
        .O(writedata_FSM_next1_carry_i_26_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    writedata_FSM_next1_carry_i_27
       (.I0(write_blockamount[9]),
        .O(writedata_FSM_next1_carry_i_27_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    writedata_FSM_next1_carry_i_28
       (.I0(write_blockamount[8]),
        .O(writedata_FSM_next1_carry_i_28_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    writedata_FSM_next1_carry_i_29
       (.I0(write_blockamount[7]),
        .O(writedata_FSM_next1_carry_i_29_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    writedata_FSM_next1_carry_i_3
       (.I0(writedata_count_reg__0[16]),
        .I1(writedata_FSM_next2[16]),
        .I2(writedata_count_reg__0[15]),
        .I3(writedata_FSM_next2[15]),
        .I4(writedata_FSM_next2[17]),
        .I5(writedata_count_reg__0[17]),
        .O(writedata_FSM_next1_carry_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    writedata_FSM_next1_carry_i_30
       (.I0(write_blockamount[6]),
        .O(writedata_FSM_next1_carry_i_30_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    writedata_FSM_next1_carry_i_31
       (.I0(write_blockamount[5]),
        .O(writedata_FSM_next1_carry_i_31_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    writedata_FSM_next1_carry_i_32
       (.I0(write_blockamount[4]),
        .O(writedata_FSM_next1_carry_i_32_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    writedata_FSM_next1_carry_i_33
       (.I0(write_blockamount[3]),
        .O(writedata_FSM_next1_carry_i_33_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    writedata_FSM_next1_carry_i_34
       (.I0(write_blockamount[2]),
        .O(writedata_FSM_next1_carry_i_34_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    writedata_FSM_next1_carry_i_35
       (.I0(write_blockamount[1]),
        .O(writedata_FSM_next1_carry_i_35_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    writedata_FSM_next1_carry_i_4
       (.I0(writedata_count_reg__0[13]),
        .I1(writedata_FSM_next2[13]),
        .I2(writedata_count_reg__0[12]),
        .I3(writedata_FSM_next2[12]),
        .I4(writedata_FSM_next2[14]),
        .I5(writedata_count_reg__0[14]),
        .O(writedata_FSM_next1_carry_i_4_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    writedata_FSM_next1_carry_i_5
       (.I0(writedata_count_reg__0[10]),
        .I1(writedata_FSM_next2[10]),
        .I2(writedata_count_reg__0[9]),
        .I3(writedata_FSM_next2[9]),
        .I4(writedata_FSM_next2[11]),
        .I5(writedata_count_reg__0[11]),
        .O(writedata_FSM_next1_carry_i_5_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    writedata_FSM_next1_carry_i_6
       (.I0(writedata_count_reg__0[7]),
        .I1(writedata_FSM_next2[7]),
        .I2(writedata_count_reg__0[6]),
        .I3(writedata_FSM_next2[6]),
        .I4(writedata_FSM_next2[8]),
        .I5(writedata_count_reg__0[8]),
        .O(writedata_FSM_next1_carry_i_6_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    writedata_FSM_next1_carry_i_7
       (.I0(writedata_count_reg__0[4]),
        .I1(writedata_FSM_next2[4]),
        .I2(writedata_count_reg[3]),
        .I3(writedata_FSM_next2[3]),
        .I4(writedata_FSM_next2[5]),
        .I5(writedata_count_reg__0[5]),
        .O(writedata_FSM_next1_carry_i_7_n_0));
  LUT6 #(
    .INIT(64'h0990000000000990)) 
    writedata_FSM_next1_carry_i_8
       (.I0(writedata_count_reg[1]),
        .I1(writedata_FSM_next2[1]),
        .I2(write_blockamount[0]),
        .I3(writedata_count_reg[0]),
        .I4(writedata_FSM_next2[2]),
        .I5(writedata_count_reg[2]),
        .O(writedata_FSM_next1_carry_i_8_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY8 writedata_FSM_next1_carry_i_9
       (.CI(writedata_FSM_next1_carry_i_10_n_0),
        .CI_TOP(1'b0),
        .CO({writedata_FSM_next1_carry_i_9_n_0,writedata_FSM_next1_carry_i_9_n_1,writedata_FSM_next1_carry_i_9_n_2,writedata_FSM_next1_carry_i_9_n_3,writedata_FSM_next1_carry_i_9_n_4,writedata_FSM_next1_carry_i_9_n_5,writedata_FSM_next1_carry_i_9_n_6,writedata_FSM_next1_carry_i_9_n_7}),
        .DI(write_blockamount[24:17]),
        .O(writedata_FSM_next2[24:17]),
        .S({writedata_FSM_next1_carry_i_12_n_0,writedata_FSM_next1_carry_i_13_n_0,writedata_FSM_next1_carry_i_14_n_0,writedata_FSM_next1_carry_i_15_n_0,writedata_FSM_next1_carry_i_16_n_0,writedata_FSM_next1_carry_i_17_n_0,writedata_FSM_next1_carry_i_18_n_0,writedata_FSM_next1_carry_i_19_n_0}));
  LUT3 #(
    .INIT(8'h02)) 
    \writedata_count[0]_i_1 
       (.I0(\FSM_sequential_writedata_FSM_next_reg_n_0_[1] ),
        .I1(writedata_FSM_next[0]),
        .I2(writedata_FSM_next[2]),
        .O(writedata_count0));
  LUT5 #(
    .INIT(32'h40400040)) 
    \writedata_count[0]_i_2 
       (.I0(writedata_FSM_next[2]),
        .I1(FIFO_READ_tvalid),
        .I2(AXI_WREADY),
        .I3(\FSM_sequential_writedata_FSM_next_reg_n_0_[1] ),
        .I4(writedata_FSM_next[0]),
        .O(writedata_count));
  LUT1 #(
    .INIT(2'h1)) 
    \writedata_count[0]_i_4 
       (.I0(writedata_count_reg[0]),
        .O(\writedata_count[0]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \writedata_count_reg[0] 
       (.C(clk),
        .CE(writedata_count),
        .D(\writedata_count_reg[0]_i_3_n_15 ),
        .Q(writedata_count_reg[0]),
        .R(writedata_count0));
  (* ADDER_THRESHOLD = "16" *) 
  CARRY8 \writedata_count_reg[0]_i_3 
       (.CI(1'b0),
        .CI_TOP(1'b0),
        .CO({\writedata_count_reg[0]_i_3_n_0 ,\writedata_count_reg[0]_i_3_n_1 ,\writedata_count_reg[0]_i_3_n_2 ,\writedata_count_reg[0]_i_3_n_3 ,\writedata_count_reg[0]_i_3_n_4 ,\writedata_count_reg[0]_i_3_n_5 ,\writedata_count_reg[0]_i_3_n_6 ,\writedata_count_reg[0]_i_3_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1}),
        .O({\writedata_count_reg[0]_i_3_n_8 ,\writedata_count_reg[0]_i_3_n_9 ,\writedata_count_reg[0]_i_3_n_10 ,\writedata_count_reg[0]_i_3_n_11 ,\writedata_count_reg[0]_i_3_n_12 ,\writedata_count_reg[0]_i_3_n_13 ,\writedata_count_reg[0]_i_3_n_14 ,\writedata_count_reg[0]_i_3_n_15 }),
        .S({writedata_count_reg__0[7:4],writedata_count_reg[3:1],\writedata_count[0]_i_4_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \writedata_count_reg[10] 
       (.C(clk),
        .CE(writedata_count),
        .D(\writedata_count_reg[8]_i_1_n_13 ),
        .Q(writedata_count_reg__0[10]),
        .R(writedata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writedata_count_reg[11] 
       (.C(clk),
        .CE(writedata_count),
        .D(\writedata_count_reg[8]_i_1_n_12 ),
        .Q(writedata_count_reg__0[11]),
        .R(writedata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writedata_count_reg[12] 
       (.C(clk),
        .CE(writedata_count),
        .D(\writedata_count_reg[8]_i_1_n_11 ),
        .Q(writedata_count_reg__0[12]),
        .R(writedata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writedata_count_reg[13] 
       (.C(clk),
        .CE(writedata_count),
        .D(\writedata_count_reg[8]_i_1_n_10 ),
        .Q(writedata_count_reg__0[13]),
        .R(writedata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writedata_count_reg[14] 
       (.C(clk),
        .CE(writedata_count),
        .D(\writedata_count_reg[8]_i_1_n_9 ),
        .Q(writedata_count_reg__0[14]),
        .R(writedata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writedata_count_reg[15] 
       (.C(clk),
        .CE(writedata_count),
        .D(\writedata_count_reg[8]_i_1_n_8 ),
        .Q(writedata_count_reg__0[15]),
        .R(writedata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writedata_count_reg[16] 
       (.C(clk),
        .CE(writedata_count),
        .D(\writedata_count_reg[16]_i_1_n_15 ),
        .Q(writedata_count_reg__0[16]),
        .R(writedata_count0));
  (* ADDER_THRESHOLD = "16" *) 
  CARRY8 \writedata_count_reg[16]_i_1 
       (.CI(\writedata_count_reg[8]_i_1_n_0 ),
        .CI_TOP(1'b0),
        .CO({\writedata_count_reg[16]_i_1_n_0 ,\writedata_count_reg[16]_i_1_n_1 ,\writedata_count_reg[16]_i_1_n_2 ,\writedata_count_reg[16]_i_1_n_3 ,\writedata_count_reg[16]_i_1_n_4 ,\writedata_count_reg[16]_i_1_n_5 ,\writedata_count_reg[16]_i_1_n_6 ,\writedata_count_reg[16]_i_1_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({\writedata_count_reg[16]_i_1_n_8 ,\writedata_count_reg[16]_i_1_n_9 ,\writedata_count_reg[16]_i_1_n_10 ,\writedata_count_reg[16]_i_1_n_11 ,\writedata_count_reg[16]_i_1_n_12 ,\writedata_count_reg[16]_i_1_n_13 ,\writedata_count_reg[16]_i_1_n_14 ,\writedata_count_reg[16]_i_1_n_15 }),
        .S(writedata_count_reg__0[23:16]));
  FDRE #(
    .INIT(1'b0)) 
    \writedata_count_reg[17] 
       (.C(clk),
        .CE(writedata_count),
        .D(\writedata_count_reg[16]_i_1_n_14 ),
        .Q(writedata_count_reg__0[17]),
        .R(writedata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writedata_count_reg[18] 
       (.C(clk),
        .CE(writedata_count),
        .D(\writedata_count_reg[16]_i_1_n_13 ),
        .Q(writedata_count_reg__0[18]),
        .R(writedata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writedata_count_reg[19] 
       (.C(clk),
        .CE(writedata_count),
        .D(\writedata_count_reg[16]_i_1_n_12 ),
        .Q(writedata_count_reg__0[19]),
        .R(writedata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writedata_count_reg[1] 
       (.C(clk),
        .CE(writedata_count),
        .D(\writedata_count_reg[0]_i_3_n_14 ),
        .Q(writedata_count_reg[1]),
        .R(writedata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writedata_count_reg[20] 
       (.C(clk),
        .CE(writedata_count),
        .D(\writedata_count_reg[16]_i_1_n_11 ),
        .Q(writedata_count_reg__0[20]),
        .R(writedata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writedata_count_reg[21] 
       (.C(clk),
        .CE(writedata_count),
        .D(\writedata_count_reg[16]_i_1_n_10 ),
        .Q(writedata_count_reg__0[21]),
        .R(writedata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writedata_count_reg[22] 
       (.C(clk),
        .CE(writedata_count),
        .D(\writedata_count_reg[16]_i_1_n_9 ),
        .Q(writedata_count_reg__0[22]),
        .R(writedata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writedata_count_reg[23] 
       (.C(clk),
        .CE(writedata_count),
        .D(\writedata_count_reg[16]_i_1_n_8 ),
        .Q(writedata_count_reg__0[23]),
        .R(writedata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writedata_count_reg[24] 
       (.C(clk),
        .CE(writedata_count),
        .D(\writedata_count_reg[24]_i_1_n_15 ),
        .Q(writedata_count_reg__0[24]),
        .R(writedata_count0));
  (* ADDER_THRESHOLD = "16" *) 
  CARRY8 \writedata_count_reg[24]_i_1 
       (.CI(\writedata_count_reg[16]_i_1_n_0 ),
        .CI_TOP(1'b0),
        .CO({\NLW_writedata_count_reg[24]_i_1_CO_UNCONNECTED [7],\writedata_count_reg[24]_i_1_n_1 ,\writedata_count_reg[24]_i_1_n_2 ,\writedata_count_reg[24]_i_1_n_3 ,\writedata_count_reg[24]_i_1_n_4 ,\writedata_count_reg[24]_i_1_n_5 ,\writedata_count_reg[24]_i_1_n_6 ,\writedata_count_reg[24]_i_1_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({\writedata_count_reg[24]_i_1_n_8 ,\writedata_count_reg[24]_i_1_n_9 ,\writedata_count_reg[24]_i_1_n_10 ,\writedata_count_reg[24]_i_1_n_11 ,\writedata_count_reg[24]_i_1_n_12 ,\writedata_count_reg[24]_i_1_n_13 ,\writedata_count_reg[24]_i_1_n_14 ,\writedata_count_reg[24]_i_1_n_15 }),
        .S(writedata_count_reg__0[31:24]));
  FDRE #(
    .INIT(1'b0)) 
    \writedata_count_reg[25] 
       (.C(clk),
        .CE(writedata_count),
        .D(\writedata_count_reg[24]_i_1_n_14 ),
        .Q(writedata_count_reg__0[25]),
        .R(writedata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writedata_count_reg[26] 
       (.C(clk),
        .CE(writedata_count),
        .D(\writedata_count_reg[24]_i_1_n_13 ),
        .Q(writedata_count_reg__0[26]),
        .R(writedata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writedata_count_reg[27] 
       (.C(clk),
        .CE(writedata_count),
        .D(\writedata_count_reg[24]_i_1_n_12 ),
        .Q(writedata_count_reg__0[27]),
        .R(writedata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writedata_count_reg[28] 
       (.C(clk),
        .CE(writedata_count),
        .D(\writedata_count_reg[24]_i_1_n_11 ),
        .Q(writedata_count_reg__0[28]),
        .R(writedata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writedata_count_reg[29] 
       (.C(clk),
        .CE(writedata_count),
        .D(\writedata_count_reg[24]_i_1_n_10 ),
        .Q(writedata_count_reg__0[29]),
        .R(writedata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writedata_count_reg[2] 
       (.C(clk),
        .CE(writedata_count),
        .D(\writedata_count_reg[0]_i_3_n_13 ),
        .Q(writedata_count_reg[2]),
        .R(writedata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writedata_count_reg[30] 
       (.C(clk),
        .CE(writedata_count),
        .D(\writedata_count_reg[24]_i_1_n_9 ),
        .Q(writedata_count_reg__0[30]),
        .R(writedata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writedata_count_reg[31] 
       (.C(clk),
        .CE(writedata_count),
        .D(\writedata_count_reg[24]_i_1_n_8 ),
        .Q(writedata_count_reg__0[31]),
        .R(writedata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writedata_count_reg[3] 
       (.C(clk),
        .CE(writedata_count),
        .D(\writedata_count_reg[0]_i_3_n_12 ),
        .Q(writedata_count_reg[3]),
        .R(writedata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writedata_count_reg[4] 
       (.C(clk),
        .CE(writedata_count),
        .D(\writedata_count_reg[0]_i_3_n_11 ),
        .Q(writedata_count_reg__0[4]),
        .R(writedata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writedata_count_reg[5] 
       (.C(clk),
        .CE(writedata_count),
        .D(\writedata_count_reg[0]_i_3_n_10 ),
        .Q(writedata_count_reg__0[5]),
        .R(writedata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writedata_count_reg[6] 
       (.C(clk),
        .CE(writedata_count),
        .D(\writedata_count_reg[0]_i_3_n_9 ),
        .Q(writedata_count_reg__0[6]),
        .R(writedata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writedata_count_reg[7] 
       (.C(clk),
        .CE(writedata_count),
        .D(\writedata_count_reg[0]_i_3_n_8 ),
        .Q(writedata_count_reg__0[7]),
        .R(writedata_count0));
  FDRE #(
    .INIT(1'b0)) 
    \writedata_count_reg[8] 
       (.C(clk),
        .CE(writedata_count),
        .D(\writedata_count_reg[8]_i_1_n_15 ),
        .Q(writedata_count_reg__0[8]),
        .R(writedata_count0));
  (* ADDER_THRESHOLD = "16" *) 
  CARRY8 \writedata_count_reg[8]_i_1 
       (.CI(\writedata_count_reg[0]_i_3_n_0 ),
        .CI_TOP(1'b0),
        .CO({\writedata_count_reg[8]_i_1_n_0 ,\writedata_count_reg[8]_i_1_n_1 ,\writedata_count_reg[8]_i_1_n_2 ,\writedata_count_reg[8]_i_1_n_3 ,\writedata_count_reg[8]_i_1_n_4 ,\writedata_count_reg[8]_i_1_n_5 ,\writedata_count_reg[8]_i_1_n_6 ,\writedata_count_reg[8]_i_1_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({\writedata_count_reg[8]_i_1_n_8 ,\writedata_count_reg[8]_i_1_n_9 ,\writedata_count_reg[8]_i_1_n_10 ,\writedata_count_reg[8]_i_1_n_11 ,\writedata_count_reg[8]_i_1_n_12 ,\writedata_count_reg[8]_i_1_n_13 ,\writedata_count_reg[8]_i_1_n_14 ,\writedata_count_reg[8]_i_1_n_15 }),
        .S(writedata_count_reg__0[15:8]));
  FDRE #(
    .INIT(1'b0)) 
    \writedata_count_reg[9] 
       (.C(clk),
        .CE(writedata_count),
        .D(\writedata_count_reg[8]_i_1_n_14 ),
        .Q(writedata_count_reg__0[9]),
        .R(writedata_count0));
  LUT4 #(
    .INIT(16'hFE02)) 
    writedata_done_i_1
       (.I0(\FSM_sequential_writedata_FSM_next_reg_n_0_[1] ),
        .I1(writedata_FSM_next[0]),
        .I2(writedata_FSM_next[2]),
        .I3(writedata_done_reg_n_0),
        .O(writedata_done_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    writedata_done_reg
       (.C(clk),
        .CE(1'b1),
        .D(writedata_done_i_1_n_0),
        .Q(writedata_done_reg_n_0),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
