// (c) Copyright 1995-2022 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.


// IP VLNV: xilinx.com:module_ref:channel_ctrl_v5:1.0
// IP Revision: 1

(* X_CORE_INFO = "channel_ctrl_v5,Vivado 2022.2" *)
(* CHECK_LICENSE_TYPE = "design_hbm_channel_channel_ctrl_v5_0_0,channel_ctrl_v5,{}" *)
(* CORE_GENERATION_INFO = "design_hbm_channel_channel_ctrl_v5_0_0,channel_ctrl_v5,{x_ipProduct=Vivado 2022.2,x_ipVendor=xilinx.com,x_ipLibrary=module_ref,x_ipName=channel_ctrl_v5,x_ipVersion=1.0,x_ipCoreRevision=1,x_ipLanguage=VERILOG,x_ipSimLanguage=MIXED,BASE_ADDRESS=000000000000000000000000000000000,HIGH_ADDRESS=000000000000000000000000100000000}" *)
(* IP_DEFINITION_SOURCE = "module_ref" *)
(* DowngradeIPIdentifiedWarnings = "yes" *)
module design_hbm_channel_channel_ctrl_v5_0_0 (
  AXI_ARADDR,
  AXI_ARBURST,
  AXI_ARID,
  AXI_ARLEN,
  AXI_ARREADY,
  AXI_ARSIZE,
  AXI_ARVALID,
  AXI_AWADDR,
  AXI_AWBURST,
  AXI_AWID,
  AXI_AWLEN,
  AXI_AWREADY,
  AXI_AWSIZE,
  AXI_AWVALID,
  AXI_BID,
  AXI_BREADY,
  AXI_BRESP,
  AXI_BVALID,
  AXI_RDATA,
  AXI_RID,
  AXI_RLAST,
  AXI_RREADY,
  AXI_RRESP,
  AXI_RVALID,
  AXI_WID,
  AXI_WDATA,
  AXI_WLAST,
  AXI_WREADY,
  AXI_WSTRB,
  AXI_WVALID,
  AXI_WDATA_PARITY,
  FIFO_WRITE_tready,
  FIFO_WRITE_tdata,
  FIFO_WRITE_tvalid,
  FIFO_WRITE_tkeep,
  FIFO_WRITE_tlast,
  FIFO_READ_tvalid,
  FIFO_READ_tdata,
  FIFO_READ_tready,
  rd_running,
  rd_startaddress,
  rd_blockamount,
  wr_running,
  wr_startaddress,
  wr_blockamount,
  busy,
  clk
);

(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI ARADDR" *)
output wire [32 : 0] AXI_ARADDR;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI ARBURST" *)
output wire [1 : 0] AXI_ARBURST;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI ARID" *)
output wire [5 : 0] AXI_ARID;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI ARLEN" *)
output wire [3 : 0] AXI_ARLEN;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI ARREADY" *)
input wire AXI_ARREADY;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI ARSIZE" *)
output wire [2 : 0] AXI_ARSIZE;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI ARVALID" *)
output wire AXI_ARVALID;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI AWADDR" *)
output wire [32 : 0] AXI_AWADDR;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI AWBURST" *)
output wire [1 : 0] AXI_AWBURST;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI AWID" *)
output wire [5 : 0] AXI_AWID;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI AWLEN" *)
output wire [3 : 0] AXI_AWLEN;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI AWREADY" *)
input wire AXI_AWREADY;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI AWSIZE" *)
output wire [2 : 0] AXI_AWSIZE;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI AWVALID" *)
output wire AXI_AWVALID;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI BID" *)
input wire [5 : 0] AXI_BID;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI BREADY" *)
output wire AXI_BREADY;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI BRESP" *)
input wire [1 : 0] AXI_BRESP;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI BVALID" *)
input wire AXI_BVALID;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI RDATA" *)
input wire [255 : 0] AXI_RDATA;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI RID" *)
input wire [5 : 0] AXI_RID;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI RLAST" *)
input wire AXI_RLAST;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI RREADY" *)
output wire AXI_RREADY;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI RRESP" *)
input wire [1 : 0] AXI_RRESP;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI RVALID" *)
input wire AXI_RVALID;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI WID" *)
output wire [5 : 0] AXI_WID;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI WDATA" *)
output wire [255 : 0] AXI_WDATA;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI WLAST" *)
output wire AXI_WLAST;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI WREADY" *)
input wire AXI_WREADY;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI WSTRB" *)
output wire [31 : 0] AXI_WSTRB;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME AXI, DATA_WIDTH 256, PROTOCOL AXI3, FREQ_HZ 450000000, ID_WIDTH 6, ADDR_WIDTH 33, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 1, HAS_LOCK 0, HAS_PROT 0, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 16, PHASE 0.0, CLK_DOMAIN design_hbm_channel_clk_450, NUM_READ_THREADS 1, NUM_WRITE_THREAD\
S 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI WVALID" *)
output wire AXI_WVALID;
output wire [31 : 0] AXI_WDATA_PARITY;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 FIFO_WRITE TREADY" *)
input wire FIFO_WRITE_tready;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 FIFO_WRITE TDATA" *)
output wire [255 : 0] FIFO_WRITE_tdata;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 FIFO_WRITE TVALID" *)
output wire FIFO_WRITE_tvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 FIFO_WRITE TKEEP" *)
output wire [31 : 0] FIFO_WRITE_tkeep;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME FIFO_WRITE, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 450000000, PHASE 0.0, CLK_DOMAIN design_hbm_channel_clk_450, LAYERED_METADATA undef, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 FIFO_WRITE TLAST" *)
output wire FIFO_WRITE_tlast;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 FIFO_READ TVALID" *)
input wire FIFO_READ_tvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 FIFO_READ TDATA" *)
input wire [255 : 0] FIFO_READ_tdata;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME FIFO_READ, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 450000000, PHASE 0.0, CLK_DOMAIN design_hbm_channel_clk_450, LAYERED_METADATA undef, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 FIFO_READ TREADY" *)
output wire FIFO_READ_tready;
input wire rd_running;
(* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 user_hbm_channel_ctrl_ports rd_startaddress" *)
input wire [31 : 0] rd_startaddress;
(* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 user_hbm_channel_ctrl_ports rd_blockamount" *)
input wire [31 : 0] rd_blockamount;
input wire wr_running;
(* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 user_hbm_channel_ctrl_ports wr_startaddress" *)
input wire [31 : 0] wr_startaddress;
(* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 user_hbm_channel_ctrl_ports wr_blockamount" *)
input wire [31 : 0] wr_blockamount;
(* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 user_hbm_channel_ctrl_ports busy" *)
output wire busy;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME clk, ASSOCIATED_BUSIF FIFO_READ:FIFO_WRITE:AXI, FREQ_HZ 450000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN design_hbm_channel_clk_450, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 clk CLK" *)
input wire clk;

  channel_ctrl_v5 #(
    .BASE_ADDRESS(33'B000000000000000000000000000000000),
    .HIGH_ADDRESS(33'B000000000000000000000000100000000)
  ) inst (
    .AXI_ARADDR(AXI_ARADDR),
    .AXI_ARBURST(AXI_ARBURST),
    .AXI_ARID(AXI_ARID),
    .AXI_ARLEN(AXI_ARLEN),
    .AXI_ARREADY(AXI_ARREADY),
    .AXI_ARSIZE(AXI_ARSIZE),
    .AXI_ARVALID(AXI_ARVALID),
    .AXI_AWADDR(AXI_AWADDR),
    .AXI_AWBURST(AXI_AWBURST),
    .AXI_AWID(AXI_AWID),
    .AXI_AWLEN(AXI_AWLEN),
    .AXI_AWREADY(AXI_AWREADY),
    .AXI_AWSIZE(AXI_AWSIZE),
    .AXI_AWVALID(AXI_AWVALID),
    .AXI_BID(AXI_BID),
    .AXI_BREADY(AXI_BREADY),
    .AXI_BRESP(AXI_BRESP),
    .AXI_BVALID(AXI_BVALID),
    .AXI_RDATA(AXI_RDATA),
    .AXI_RID(AXI_RID),
    .AXI_RLAST(AXI_RLAST),
    .AXI_RREADY(AXI_RREADY),
    .AXI_RRESP(AXI_RRESP),
    .AXI_RVALID(AXI_RVALID),
    .AXI_WID(AXI_WID),
    .AXI_WDATA(AXI_WDATA),
    .AXI_WLAST(AXI_WLAST),
    .AXI_WREADY(AXI_WREADY),
    .AXI_WSTRB(AXI_WSTRB),
    .AXI_WVALID(AXI_WVALID),
    .AXI_WDATA_PARITY(AXI_WDATA_PARITY),
    .FIFO_WRITE_tready(FIFO_WRITE_tready),
    .FIFO_WRITE_tdata(FIFO_WRITE_tdata),
    .FIFO_WRITE_tvalid(FIFO_WRITE_tvalid),
    .FIFO_WRITE_tkeep(FIFO_WRITE_tkeep),
    .FIFO_WRITE_tlast(FIFO_WRITE_tlast),
    .FIFO_READ_tvalid(FIFO_READ_tvalid),
    .FIFO_READ_tdata(FIFO_READ_tdata),
    .FIFO_READ_tready(FIFO_READ_tready),
    .rd_running(rd_running),
    .rd_startaddress(rd_startaddress),
    .rd_blockamount(rd_blockamount),
    .wr_running(wr_running),
    .wr_startaddress(wr_startaddress),
    .wr_blockamount(wr_blockamount),
    .busy(busy),
    .clk(clk)
  );
endmodule
