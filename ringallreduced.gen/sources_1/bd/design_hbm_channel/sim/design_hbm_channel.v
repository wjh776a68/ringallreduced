//Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
//Date        : Tue Dec  6 16:10:05 2022
//Host        : dell-Vostro-3910-China-HDD-Protection running 64-bit Ubuntu 22.04.1 LTS
//Command     : generate_target design_hbm_channel.bd
//Design      : design_hbm_channel
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CORE_GENERATION_INFO = "design_hbm_channel,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=design_hbm_channel,x_ipVersion=1.00.a,x_ipLanguage=VERILOG,numBlks=9,numReposBlks=9,numNonXlnxBlks=0,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=1,numPkgbdBlks=0,bdsource=USER,synth_mode=OOC_per_IP}" *) (* HW_HANDOFF = "design_hbm_channel.hwdef" *) 
module design_hbm_channel
   (HBM_AXI_araddr,
    HBM_AXI_arburst,
    HBM_AXI_arcache,
    HBM_AXI_arid,
    HBM_AXI_arlen,
    HBM_AXI_arlock,
    HBM_AXI_arprot,
    HBM_AXI_arqos,
    HBM_AXI_arready,
    HBM_AXI_arsize,
    HBM_AXI_arvalid,
    HBM_AXI_awaddr,
    HBM_AXI_awburst,
    HBM_AXI_awcache,
    HBM_AXI_awid,
    HBM_AXI_awlen,
    HBM_AXI_awlock,
    HBM_AXI_awprot,
    HBM_AXI_awqos,
    HBM_AXI_awready,
    HBM_AXI_awsize,
    HBM_AXI_awvalid,
    HBM_AXI_bid,
    HBM_AXI_bready,
    HBM_AXI_bresp,
    HBM_AXI_bvalid,
    HBM_AXI_rdata,
    HBM_AXI_rid,
    HBM_AXI_rlast,
    HBM_AXI_rready,
    HBM_AXI_rresp,
    HBM_AXI_rvalid,
    HBM_AXI_wdata,
    HBM_AXI_wid,
    HBM_AXI_wlast,
    HBM_AXI_wready,
    HBM_AXI_wstrb,
    HBM_AXI_wvalid,
    HBM_READ_tdata,
    HBM_READ_tkeep,
    HBM_READ_tlast,
    HBM_READ_tready,
    HBM_READ_tvalid,
    HBM_WRITE_tdata,
    HBM_WRITE_tkeep,
    HBM_WRITE_tlast,
    HBM_WRITE_tready,
    HBM_WRITE_tvalid,
    clk_312,
    clk_450,
    rd_running,
    user_hbm_channel_ctrl_ports_busy,
    user_hbm_channel_ctrl_ports_rd_blockamount,
    user_hbm_channel_ctrl_ports_rd_startaddress,
    user_hbm_channel_ctrl_ports_wr_blockamount,
    user_hbm_channel_ctrl_ports_wr_startaddress,
    wr_running);
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI ARADDR" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME HBM_AXI, ADDR_WIDTH 33, ARUSER_WIDTH 0, AWUSER_WIDTH 0, BUSER_WIDTH 0, CLK_DOMAIN design_hbm_channel_clk_450, DATA_WIDTH 256, FREQ_HZ 450000000, HAS_BRESP 1, HAS_BURST 1, HAS_CACHE 0, HAS_LOCK 0, HAS_PROT 0, HAS_QOS 0, HAS_REGION 0, HAS_RRESP 1, HAS_WSTRB 1, ID_WIDTH 6, INSERT_VIP 0, MAX_BURST_LENGTH 16, NUM_READ_OUTSTANDING 2, NUM_READ_THREADS 1, NUM_WRITE_OUTSTANDING 2, NUM_WRITE_THREADS 1, PHASE 0.0, PROTOCOL AXI3, READ_WRITE_MODE READ_WRITE, RUSER_BITS_PER_BYTE 0, RUSER_WIDTH 0, SUPPORTS_NARROW_BURST 1, WUSER_BITS_PER_BYTE 0, WUSER_WIDTH 0" *) output [32:0]HBM_AXI_araddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI ARBURST" *) output [1:0]HBM_AXI_arburst;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI ARCACHE" *) output [3:0]HBM_AXI_arcache;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI ARID" *) output [5:0]HBM_AXI_arid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI ARLEN" *) output [3:0]HBM_AXI_arlen;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI ARLOCK" *) output [1:0]HBM_AXI_arlock;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI ARPROT" *) output [2:0]HBM_AXI_arprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI ARQOS" *) output [3:0]HBM_AXI_arqos;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI ARREADY" *) input HBM_AXI_arready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI ARSIZE" *) output [2:0]HBM_AXI_arsize;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI ARVALID" *) output HBM_AXI_arvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI AWADDR" *) output [32:0]HBM_AXI_awaddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI AWBURST" *) output [1:0]HBM_AXI_awburst;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI AWCACHE" *) output [3:0]HBM_AXI_awcache;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI AWID" *) output [5:0]HBM_AXI_awid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI AWLEN" *) output [3:0]HBM_AXI_awlen;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI AWLOCK" *) output [1:0]HBM_AXI_awlock;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI AWPROT" *) output [2:0]HBM_AXI_awprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI AWQOS" *) output [3:0]HBM_AXI_awqos;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI AWREADY" *) input HBM_AXI_awready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI AWSIZE" *) output [2:0]HBM_AXI_awsize;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI AWVALID" *) output HBM_AXI_awvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI BID" *) input [5:0]HBM_AXI_bid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI BREADY" *) output HBM_AXI_bready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI BRESP" *) input [1:0]HBM_AXI_bresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI BVALID" *) input HBM_AXI_bvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI RDATA" *) input [255:0]HBM_AXI_rdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI RID" *) input [5:0]HBM_AXI_rid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI RLAST" *) input HBM_AXI_rlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI RREADY" *) output HBM_AXI_rready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI RRESP" *) input [1:0]HBM_AXI_rresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI RVALID" *) input HBM_AXI_rvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI WDATA" *) output [255:0]HBM_AXI_wdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI WID" *) output [5:0]HBM_AXI_wid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI WLAST" *) output HBM_AXI_wlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI WREADY" *) input HBM_AXI_wready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI WSTRB" *) output [31:0]HBM_AXI_wstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI WVALID" *) output HBM_AXI_wvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_READ TDATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME HBM_READ, CLK_DOMAIN design_hbm_channel_clk_312, FREQ_HZ 312500000, HAS_TKEEP 1, HAS_TLAST 1, HAS_TREADY 1, HAS_TSTRB 0, INSERT_VIP 0, LAYERED_METADATA undef, PHASE 0.0, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0" *) output [255:0]HBM_READ_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_READ TKEEP" *) output [31:0]HBM_READ_tkeep;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_READ TLAST" *) output HBM_READ_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_READ TREADY" *) input HBM_READ_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_READ TVALID" *) output HBM_READ_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_WRITE TDATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME HBM_WRITE, CLK_DOMAIN design_hbm_channel_clk_312, FREQ_HZ 312500000, HAS_TKEEP 1, HAS_TLAST 1, HAS_TREADY 1, HAS_TSTRB 0, INSERT_VIP 0, LAYERED_METADATA undef, PHASE 0.0, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0" *) input [255:0]HBM_WRITE_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_WRITE TKEEP" *) input [31:0]HBM_WRITE_tkeep;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_WRITE TLAST" *) input HBM_WRITE_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_WRITE TREADY" *) output HBM_WRITE_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_WRITE TVALID" *) input HBM_WRITE_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 CLK.CLK_312 CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME CLK.CLK_312, ASSOCIATED_BUSIF HBM_READ:HBM_WRITE, CLK_DOMAIN design_hbm_channel_clk_312, FREQ_HZ 312500000, FREQ_TOLERANCE_HZ 0, INSERT_VIP 0, PHASE 0.0" *) input clk_312;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 CLK.CLK_450 CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME CLK.CLK_450, ASSOCIATED_BUSIF HBM_AXI, CLK_DOMAIN design_hbm_channel_clk_450, FREQ_HZ 450000000, FREQ_TOLERANCE_HZ 0, INSERT_VIP 0, PHASE 0.0" *) input clk_450;
  input rd_running;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 user_hbm_channel_ctrl_ports busy" *) output user_hbm_channel_ctrl_ports_busy;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 user_hbm_channel_ctrl_ports rd_blockamount" *) input [31:0]user_hbm_channel_ctrl_ports_rd_blockamount;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 user_hbm_channel_ctrl_ports rd_startaddress" *) input [31:0]user_hbm_channel_ctrl_ports_rd_startaddress;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 user_hbm_channel_ctrl_ports wr_blockamount" *) input [31:0]user_hbm_channel_ctrl_ports_wr_blockamount;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 user_hbm_channel_ctrl_ports wr_startaddress" *) input [31:0]user_hbm_channel_ctrl_ports_wr_startaddress;
  input wr_running;

  wire [255:0]HBM_WRITE_1_TDATA;
  wire [31:0]HBM_WRITE_1_TKEEP;
  wire HBM_WRITE_1_TLAST;
  wire HBM_WRITE_1_TREADY;
  wire HBM_WRITE_1_TVALID;
  wire Net;
  wire Net1;
  wire [32:0]axi_register_slice_0_M_AXI_ARADDR;
  wire [1:0]axi_register_slice_0_M_AXI_ARBURST;
  wire [3:0]axi_register_slice_0_M_AXI_ARCACHE;
  wire [5:0]axi_register_slice_0_M_AXI_ARID;
  wire [3:0]axi_register_slice_0_M_AXI_ARLEN;
  wire [1:0]axi_register_slice_0_M_AXI_ARLOCK;
  wire [2:0]axi_register_slice_0_M_AXI_ARPROT;
  wire [3:0]axi_register_slice_0_M_AXI_ARQOS;
  wire axi_register_slice_0_M_AXI_ARREADY;
  wire [2:0]axi_register_slice_0_M_AXI_ARSIZE;
  wire axi_register_slice_0_M_AXI_ARVALID;
  wire [32:0]axi_register_slice_0_M_AXI_AWADDR;
  wire [1:0]axi_register_slice_0_M_AXI_AWBURST;
  wire [3:0]axi_register_slice_0_M_AXI_AWCACHE;
  wire [5:0]axi_register_slice_0_M_AXI_AWID;
  wire [3:0]axi_register_slice_0_M_AXI_AWLEN;
  wire [1:0]axi_register_slice_0_M_AXI_AWLOCK;
  wire [2:0]axi_register_slice_0_M_AXI_AWPROT;
  wire [3:0]axi_register_slice_0_M_AXI_AWQOS;
  wire axi_register_slice_0_M_AXI_AWREADY;
  wire [2:0]axi_register_slice_0_M_AXI_AWSIZE;
  wire axi_register_slice_0_M_AXI_AWVALID;
  wire [5:0]axi_register_slice_0_M_AXI_BID;
  wire axi_register_slice_0_M_AXI_BREADY;
  wire [1:0]axi_register_slice_0_M_AXI_BRESP;
  wire axi_register_slice_0_M_AXI_BVALID;
  wire [255:0]axi_register_slice_0_M_AXI_RDATA;
  wire [5:0]axi_register_slice_0_M_AXI_RID;
  wire axi_register_slice_0_M_AXI_RLAST;
  wire axi_register_slice_0_M_AXI_RREADY;
  wire [1:0]axi_register_slice_0_M_AXI_RRESP;
  wire axi_register_slice_0_M_AXI_RVALID;
  wire [255:0]axi_register_slice_0_M_AXI_WDATA;
  wire [5:0]axi_register_slice_0_M_AXI_WID;
  wire axi_register_slice_0_M_AXI_WLAST;
  wire axi_register_slice_0_M_AXI_WREADY;
  wire [31:0]axi_register_slice_0_M_AXI_WSTRB;
  wire axi_register_slice_0_M_AXI_WVALID;
  wire [255:0]axis_data_fifo_0_M_AXIS_TDATA;
  wire [31:0]axis_data_fifo_0_M_AXIS_TKEEP;
  wire axis_data_fifo_0_M_AXIS_TLAST;
  wire axis_data_fifo_0_M_AXIS_TREADY;
  wire axis_data_fifo_0_M_AXIS_TVALID;
  wire [255:0]axis_data_fifo_1_M_AXIS_TDATA;
  wire [31:0]axis_data_fifo_1_M_AXIS_TKEEP;
  wire axis_data_fifo_1_M_AXIS_TLAST;
  wire axis_data_fifo_1_M_AXIS_TREADY;
  wire axis_data_fifo_1_M_AXIS_TVALID;
  wire [255:0]axis_register_slice_0_M_AXIS_TDATA;
  wire [31:0]axis_register_slice_0_M_AXIS_TKEEP;
  wire axis_register_slice_0_M_AXIS_TLAST;
  wire axis_register_slice_0_M_AXIS_TREADY;
  wire axis_register_slice_0_M_AXIS_TVALID;
  wire [255:0]axis_register_slice_1_M_AXIS_TDATA;
  wire axis_register_slice_1_M_AXIS_TREADY;
  wire axis_register_slice_1_M_AXIS_TVALID;
  wire [255:0]axis_register_slice_2_M_AXIS_TDATA;
  wire [31:0]axis_register_slice_2_M_AXIS_TKEEP;
  wire axis_register_slice_2_M_AXIS_TLAST;
  wire axis_register_slice_2_M_AXIS_TREADY;
  wire axis_register_slice_2_M_AXIS_TVALID;
  wire [255:0]axis_register_slice_3_M_AXIS_TDATA;
  wire [31:0]axis_register_slice_3_M_AXIS_TKEEP;
  wire axis_register_slice_3_M_AXIS_TLAST;
  wire axis_register_slice_3_M_AXIS_TREADY;
  wire axis_register_slice_3_M_AXIS_TVALID;
  wire [32:0]channel_ctrl_v5_0_AXI_ARADDR;
  wire [1:0]channel_ctrl_v5_0_AXI_ARBURST;
  wire [5:0]channel_ctrl_v5_0_AXI_ARID;
  wire [3:0]channel_ctrl_v5_0_AXI_ARLEN;
  wire channel_ctrl_v5_0_AXI_ARREADY;
  wire [2:0]channel_ctrl_v5_0_AXI_ARSIZE;
  wire channel_ctrl_v5_0_AXI_ARVALID;
  wire [32:0]channel_ctrl_v5_0_AXI_AWADDR;
  wire [1:0]channel_ctrl_v5_0_AXI_AWBURST;
  wire [5:0]channel_ctrl_v5_0_AXI_AWID;
  wire [3:0]channel_ctrl_v5_0_AXI_AWLEN;
  wire channel_ctrl_v5_0_AXI_AWREADY;
  wire [2:0]channel_ctrl_v5_0_AXI_AWSIZE;
  wire channel_ctrl_v5_0_AXI_AWVALID;
  wire [5:0]channel_ctrl_v5_0_AXI_BID;
  wire channel_ctrl_v5_0_AXI_BREADY;
  wire [1:0]channel_ctrl_v5_0_AXI_BRESP;
  wire channel_ctrl_v5_0_AXI_BVALID;
  wire [255:0]channel_ctrl_v5_0_AXI_RDATA;
  wire [5:0]channel_ctrl_v5_0_AXI_RID;
  wire channel_ctrl_v5_0_AXI_RLAST;
  wire channel_ctrl_v5_0_AXI_RREADY;
  wire [1:0]channel_ctrl_v5_0_AXI_RRESP;
  wire channel_ctrl_v5_0_AXI_RVALID;
  wire [255:0]channel_ctrl_v5_0_AXI_WDATA;
  wire [5:0]channel_ctrl_v5_0_AXI_WID;
  wire channel_ctrl_v5_0_AXI_WLAST;
  wire channel_ctrl_v5_0_AXI_WREADY;
  wire [31:0]channel_ctrl_v5_0_AXI_WSTRB;
  wire channel_ctrl_v5_0_AXI_WVALID;
  wire [255:0]channel_ctrl_v5_0_FIFO_WRITE_TDATA;
  wire [31:0]channel_ctrl_v5_0_FIFO_WRITE_TKEEP;
  wire channel_ctrl_v5_0_FIFO_WRITE_TLAST;
  wire channel_ctrl_v5_0_FIFO_WRITE_TREADY;
  wire channel_ctrl_v5_0_FIFO_WRITE_TVALID;
  wire rd_running_0_1;
  wire user_hbm_channel_ctrl_ports_0_1_busy;
  wire [31:0]user_hbm_channel_ctrl_ports_0_1_rd_blockamount;
  wire [31:0]user_hbm_channel_ctrl_ports_0_1_rd_startaddress;
  wire [31:0]user_hbm_channel_ctrl_ports_0_1_wr_blockamount;
  wire [31:0]user_hbm_channel_ctrl_ports_0_1_wr_startaddress;
  wire wr_running_0_1;
  wire [0:0]xlconstant_2_dout;
  wire NLW_axis_register_slice_1_m_axis_tlast_UNCONNECTED;
  wire [31:0]NLW_axis_register_slice_1_m_axis_tkeep_UNCONNECTED;
  wire [31:0]NLW_channel_ctrl_v5_0_AXI_WDATA_PARITY_UNCONNECTED;

  assign HBM_AXI_araddr[32:0] = axi_register_slice_0_M_AXI_ARADDR;
  assign HBM_AXI_arburst[1:0] = axi_register_slice_0_M_AXI_ARBURST;
  assign HBM_AXI_arcache[3:0] = axi_register_slice_0_M_AXI_ARCACHE;
  assign HBM_AXI_arid[5:0] = axi_register_slice_0_M_AXI_ARID;
  assign HBM_AXI_arlen[3:0] = axi_register_slice_0_M_AXI_ARLEN;
  assign HBM_AXI_arlock[1:0] = axi_register_slice_0_M_AXI_ARLOCK;
  assign HBM_AXI_arprot[2:0] = axi_register_slice_0_M_AXI_ARPROT;
  assign HBM_AXI_arqos[3:0] = axi_register_slice_0_M_AXI_ARQOS;
  assign HBM_AXI_arsize[2:0] = axi_register_slice_0_M_AXI_ARSIZE;
  assign HBM_AXI_arvalid = axi_register_slice_0_M_AXI_ARVALID;
  assign HBM_AXI_awaddr[32:0] = axi_register_slice_0_M_AXI_AWADDR;
  assign HBM_AXI_awburst[1:0] = axi_register_slice_0_M_AXI_AWBURST;
  assign HBM_AXI_awcache[3:0] = axi_register_slice_0_M_AXI_AWCACHE;
  assign HBM_AXI_awid[5:0] = axi_register_slice_0_M_AXI_AWID;
  assign HBM_AXI_awlen[3:0] = axi_register_slice_0_M_AXI_AWLEN;
  assign HBM_AXI_awlock[1:0] = axi_register_slice_0_M_AXI_AWLOCK;
  assign HBM_AXI_awprot[2:0] = axi_register_slice_0_M_AXI_AWPROT;
  assign HBM_AXI_awqos[3:0] = axi_register_slice_0_M_AXI_AWQOS;
  assign HBM_AXI_awsize[2:0] = axi_register_slice_0_M_AXI_AWSIZE;
  assign HBM_AXI_awvalid = axi_register_slice_0_M_AXI_AWVALID;
  assign HBM_AXI_bready = axi_register_slice_0_M_AXI_BREADY;
  assign HBM_AXI_rready = axi_register_slice_0_M_AXI_RREADY;
  assign HBM_AXI_wdata[255:0] = axi_register_slice_0_M_AXI_WDATA;
  assign HBM_AXI_wid[5:0] = axi_register_slice_0_M_AXI_WID;
  assign HBM_AXI_wlast = axi_register_slice_0_M_AXI_WLAST;
  assign HBM_AXI_wstrb[31:0] = axi_register_slice_0_M_AXI_WSTRB;
  assign HBM_AXI_wvalid = axi_register_slice_0_M_AXI_WVALID;
  assign HBM_READ_tdata[255:0] = axis_register_slice_2_M_AXIS_TDATA;
  assign HBM_READ_tkeep[31:0] = axis_register_slice_2_M_AXIS_TKEEP;
  assign HBM_READ_tlast = axis_register_slice_2_M_AXIS_TLAST;
  assign HBM_READ_tvalid = axis_register_slice_2_M_AXIS_TVALID;
  assign HBM_WRITE_1_TDATA = HBM_WRITE_tdata[255:0];
  assign HBM_WRITE_1_TKEEP = HBM_WRITE_tkeep[31:0];
  assign HBM_WRITE_1_TLAST = HBM_WRITE_tlast;
  assign HBM_WRITE_1_TVALID = HBM_WRITE_tvalid;
  assign HBM_WRITE_tready = HBM_WRITE_1_TREADY;
  assign Net = clk_450;
  assign Net1 = clk_312;
  assign axi_register_slice_0_M_AXI_ARREADY = HBM_AXI_arready;
  assign axi_register_slice_0_M_AXI_AWREADY = HBM_AXI_awready;
  assign axi_register_slice_0_M_AXI_BID = HBM_AXI_bid[5:0];
  assign axi_register_slice_0_M_AXI_BRESP = HBM_AXI_bresp[1:0];
  assign axi_register_slice_0_M_AXI_BVALID = HBM_AXI_bvalid;
  assign axi_register_slice_0_M_AXI_RDATA = HBM_AXI_rdata[255:0];
  assign axi_register_slice_0_M_AXI_RID = HBM_AXI_rid[5:0];
  assign axi_register_slice_0_M_AXI_RLAST = HBM_AXI_rlast;
  assign axi_register_slice_0_M_AXI_RRESP = HBM_AXI_rresp[1:0];
  assign axi_register_slice_0_M_AXI_RVALID = HBM_AXI_rvalid;
  assign axi_register_slice_0_M_AXI_WREADY = HBM_AXI_wready;
  assign axis_register_slice_2_M_AXIS_TREADY = HBM_READ_tready;
  assign rd_running_0_1 = rd_running;
  assign user_hbm_channel_ctrl_ports_0_1_rd_blockamount = user_hbm_channel_ctrl_ports_rd_blockamount[31:0];
  assign user_hbm_channel_ctrl_ports_0_1_rd_startaddress = user_hbm_channel_ctrl_ports_rd_startaddress[31:0];
  assign user_hbm_channel_ctrl_ports_0_1_wr_blockamount = user_hbm_channel_ctrl_ports_wr_blockamount[31:0];
  assign user_hbm_channel_ctrl_ports_0_1_wr_startaddress = user_hbm_channel_ctrl_ports_wr_startaddress[31:0];
  assign user_hbm_channel_ctrl_ports_busy = user_hbm_channel_ctrl_ports_0_1_busy;
  assign wr_running_0_1 = wr_running;
  design_hbm_channel_axi_register_slice_0_0 axi_register_slice_0
       (.aclk(Net),
        .aresetn(xlconstant_2_dout),
        .m_axi_araddr(axi_register_slice_0_M_AXI_ARADDR),
        .m_axi_arburst(axi_register_slice_0_M_AXI_ARBURST),
        .m_axi_arcache(axi_register_slice_0_M_AXI_ARCACHE),
        .m_axi_arid(axi_register_slice_0_M_AXI_ARID),
        .m_axi_arlen(axi_register_slice_0_M_AXI_ARLEN),
        .m_axi_arlock(axi_register_slice_0_M_AXI_ARLOCK),
        .m_axi_arprot(axi_register_slice_0_M_AXI_ARPROT),
        .m_axi_arqos(axi_register_slice_0_M_AXI_ARQOS),
        .m_axi_arready(axi_register_slice_0_M_AXI_ARREADY),
        .m_axi_arsize(axi_register_slice_0_M_AXI_ARSIZE),
        .m_axi_arvalid(axi_register_slice_0_M_AXI_ARVALID),
        .m_axi_awaddr(axi_register_slice_0_M_AXI_AWADDR),
        .m_axi_awburst(axi_register_slice_0_M_AXI_AWBURST),
        .m_axi_awcache(axi_register_slice_0_M_AXI_AWCACHE),
        .m_axi_awid(axi_register_slice_0_M_AXI_AWID),
        .m_axi_awlen(axi_register_slice_0_M_AXI_AWLEN),
        .m_axi_awlock(axi_register_slice_0_M_AXI_AWLOCK),
        .m_axi_awprot(axi_register_slice_0_M_AXI_AWPROT),
        .m_axi_awqos(axi_register_slice_0_M_AXI_AWQOS),
        .m_axi_awready(axi_register_slice_0_M_AXI_AWREADY),
        .m_axi_awsize(axi_register_slice_0_M_AXI_AWSIZE),
        .m_axi_awvalid(axi_register_slice_0_M_AXI_AWVALID),
        .m_axi_bid(axi_register_slice_0_M_AXI_BID),
        .m_axi_bready(axi_register_slice_0_M_AXI_BREADY),
        .m_axi_bresp(axi_register_slice_0_M_AXI_BRESP),
        .m_axi_bvalid(axi_register_slice_0_M_AXI_BVALID),
        .m_axi_rdata(axi_register_slice_0_M_AXI_RDATA),
        .m_axi_rid(axi_register_slice_0_M_AXI_RID),
        .m_axi_rlast(axi_register_slice_0_M_AXI_RLAST),
        .m_axi_rready(axi_register_slice_0_M_AXI_RREADY),
        .m_axi_rresp(axi_register_slice_0_M_AXI_RRESP),
        .m_axi_rvalid(axi_register_slice_0_M_AXI_RVALID),
        .m_axi_wdata(axi_register_slice_0_M_AXI_WDATA),
        .m_axi_wid(axi_register_slice_0_M_AXI_WID),
        .m_axi_wlast(axi_register_slice_0_M_AXI_WLAST),
        .m_axi_wready(axi_register_slice_0_M_AXI_WREADY),
        .m_axi_wstrb(axi_register_slice_0_M_AXI_WSTRB),
        .m_axi_wvalid(axi_register_slice_0_M_AXI_WVALID),
        .s_axi_araddr(channel_ctrl_v5_0_AXI_ARADDR),
        .s_axi_arburst(channel_ctrl_v5_0_AXI_ARBURST),
        .s_axi_arcache({1'b0,1'b0,1'b1,1'b1}),
        .s_axi_arid(channel_ctrl_v5_0_AXI_ARID),
        .s_axi_arlen(channel_ctrl_v5_0_AXI_ARLEN),
        .s_axi_arlock({1'b0,1'b0}),
        .s_axi_arprot({1'b0,1'b0,1'b0}),
        .s_axi_arqos({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(channel_ctrl_v5_0_AXI_ARREADY),
        .s_axi_arsize(channel_ctrl_v5_0_AXI_ARSIZE),
        .s_axi_arvalid(channel_ctrl_v5_0_AXI_ARVALID),
        .s_axi_awaddr(channel_ctrl_v5_0_AXI_AWADDR),
        .s_axi_awburst(channel_ctrl_v5_0_AXI_AWBURST),
        .s_axi_awcache({1'b0,1'b0,1'b1,1'b1}),
        .s_axi_awid(channel_ctrl_v5_0_AXI_AWID),
        .s_axi_awlen(channel_ctrl_v5_0_AXI_AWLEN),
        .s_axi_awlock({1'b0,1'b0}),
        .s_axi_awprot({1'b0,1'b0,1'b0}),
        .s_axi_awqos({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(channel_ctrl_v5_0_AXI_AWREADY),
        .s_axi_awsize(channel_ctrl_v5_0_AXI_AWSIZE),
        .s_axi_awvalid(channel_ctrl_v5_0_AXI_AWVALID),
        .s_axi_bid(channel_ctrl_v5_0_AXI_BID),
        .s_axi_bready(channel_ctrl_v5_0_AXI_BREADY),
        .s_axi_bresp(channel_ctrl_v5_0_AXI_BRESP),
        .s_axi_bvalid(channel_ctrl_v5_0_AXI_BVALID),
        .s_axi_rdata(channel_ctrl_v5_0_AXI_RDATA),
        .s_axi_rid(channel_ctrl_v5_0_AXI_RID),
        .s_axi_rlast(channel_ctrl_v5_0_AXI_RLAST),
        .s_axi_rready(channel_ctrl_v5_0_AXI_RREADY),
        .s_axi_rresp(channel_ctrl_v5_0_AXI_RRESP),
        .s_axi_rvalid(channel_ctrl_v5_0_AXI_RVALID),
        .s_axi_wdata(channel_ctrl_v5_0_AXI_WDATA),
        .s_axi_wid(channel_ctrl_v5_0_AXI_WID),
        .s_axi_wlast(channel_ctrl_v5_0_AXI_WLAST),
        .s_axi_wready(channel_ctrl_v5_0_AXI_WREADY),
        .s_axi_wstrb(channel_ctrl_v5_0_AXI_WSTRB),
        .s_axi_wvalid(channel_ctrl_v5_0_AXI_WVALID));
  design_hbm_channel_axis_data_fifo_0_0 axis_data_fifo_0
       (.m_axis_aclk(Net),
        .m_axis_tdata(axis_data_fifo_0_M_AXIS_TDATA),
        .m_axis_tkeep(axis_data_fifo_0_M_AXIS_TKEEP),
        .m_axis_tlast(axis_data_fifo_0_M_AXIS_TLAST),
        .m_axis_tready(axis_data_fifo_0_M_AXIS_TREADY),
        .m_axis_tvalid(axis_data_fifo_0_M_AXIS_TVALID),
        .s_axis_aclk(Net1),
        .s_axis_aresetn(xlconstant_2_dout),
        .s_axis_tdata(axis_register_slice_3_M_AXIS_TDATA),
        .s_axis_tkeep(axis_register_slice_3_M_AXIS_TKEEP),
        .s_axis_tlast(axis_register_slice_3_M_AXIS_TLAST),
        .s_axis_tready(axis_register_slice_3_M_AXIS_TREADY),
        .s_axis_tvalid(axis_register_slice_3_M_AXIS_TVALID));
  design_hbm_channel_axis_data_fifo_0_1 axis_data_fifo_1
       (.m_axis_aclk(Net1),
        .m_axis_tdata(axis_data_fifo_1_M_AXIS_TDATA),
        .m_axis_tkeep(axis_data_fifo_1_M_AXIS_TKEEP),
        .m_axis_tlast(axis_data_fifo_1_M_AXIS_TLAST),
        .m_axis_tready(axis_data_fifo_1_M_AXIS_TREADY),
        .m_axis_tvalid(axis_data_fifo_1_M_AXIS_TVALID),
        .s_axis_aclk(Net),
        .s_axis_aresetn(xlconstant_2_dout),
        .s_axis_tdata(axis_register_slice_0_M_AXIS_TDATA),
        .s_axis_tkeep(axis_register_slice_0_M_AXIS_TKEEP),
        .s_axis_tlast(axis_register_slice_0_M_AXIS_TLAST),
        .s_axis_tready(axis_register_slice_0_M_AXIS_TREADY),
        .s_axis_tvalid(axis_register_slice_0_M_AXIS_TVALID));
  design_hbm_channel_axis_register_slice_0_0 axis_register_slice_0
       (.aclk(Net),
        .aresetn(xlconstant_2_dout),
        .m_axis_tdata(axis_register_slice_0_M_AXIS_TDATA),
        .m_axis_tkeep(axis_register_slice_0_M_AXIS_TKEEP),
        .m_axis_tlast(axis_register_slice_0_M_AXIS_TLAST),
        .m_axis_tready(axis_register_slice_0_M_AXIS_TREADY),
        .m_axis_tvalid(axis_register_slice_0_M_AXIS_TVALID),
        .s_axis_tdata(channel_ctrl_v5_0_FIFO_WRITE_TDATA),
        .s_axis_tkeep(channel_ctrl_v5_0_FIFO_WRITE_TKEEP),
        .s_axis_tlast(channel_ctrl_v5_0_FIFO_WRITE_TLAST),
        .s_axis_tready(channel_ctrl_v5_0_FIFO_WRITE_TREADY),
        .s_axis_tvalid(channel_ctrl_v5_0_FIFO_WRITE_TVALID));
  design_hbm_channel_axis_register_slice_0_1 axis_register_slice_1
       (.aclk(Net),
        .aresetn(xlconstant_2_dout),
        .m_axis_tdata(axis_register_slice_1_M_AXIS_TDATA),
        .m_axis_tready(axis_register_slice_1_M_AXIS_TREADY),
        .m_axis_tvalid(axis_register_slice_1_M_AXIS_TVALID),
        .s_axis_tdata(axis_data_fifo_0_M_AXIS_TDATA),
        .s_axis_tkeep(axis_data_fifo_0_M_AXIS_TKEEP),
        .s_axis_tlast(axis_data_fifo_0_M_AXIS_TLAST),
        .s_axis_tready(axis_data_fifo_0_M_AXIS_TREADY),
        .s_axis_tvalid(axis_data_fifo_0_M_AXIS_TVALID));
  design_hbm_channel_axis_register_slice_2_0 axis_register_slice_2
       (.aclk(Net1),
        .aresetn(xlconstant_2_dout),
        .m_axis_tdata(axis_register_slice_2_M_AXIS_TDATA),
        .m_axis_tkeep(axis_register_slice_2_M_AXIS_TKEEP),
        .m_axis_tlast(axis_register_slice_2_M_AXIS_TLAST),
        .m_axis_tready(axis_register_slice_2_M_AXIS_TREADY),
        .m_axis_tvalid(axis_register_slice_2_M_AXIS_TVALID),
        .s_axis_tdata(axis_data_fifo_1_M_AXIS_TDATA),
        .s_axis_tkeep(axis_data_fifo_1_M_AXIS_TKEEP),
        .s_axis_tlast(axis_data_fifo_1_M_AXIS_TLAST),
        .s_axis_tready(axis_data_fifo_1_M_AXIS_TREADY),
        .s_axis_tvalid(axis_data_fifo_1_M_AXIS_TVALID));
  design_hbm_channel_axis_register_slice_3_0 axis_register_slice_3
       (.aclk(Net1),
        .aresetn(xlconstant_2_dout),
        .m_axis_tdata(axis_register_slice_3_M_AXIS_TDATA),
        .m_axis_tkeep(axis_register_slice_3_M_AXIS_TKEEP),
        .m_axis_tlast(axis_register_slice_3_M_AXIS_TLAST),
        .m_axis_tready(axis_register_slice_3_M_AXIS_TREADY),
        .m_axis_tvalid(axis_register_slice_3_M_AXIS_TVALID),
        .s_axis_tdata(HBM_WRITE_1_TDATA),
        .s_axis_tkeep(HBM_WRITE_1_TKEEP),
        .s_axis_tlast(HBM_WRITE_1_TLAST),
        .s_axis_tready(HBM_WRITE_1_TREADY),
        .s_axis_tvalid(HBM_WRITE_1_TVALID));
  design_hbm_channel_channel_ctrl_v5_0_0 channel_ctrl_v5_0
       (.AXI_ARADDR(channel_ctrl_v5_0_AXI_ARADDR),
        .AXI_ARBURST(channel_ctrl_v5_0_AXI_ARBURST),
        .AXI_ARID(channel_ctrl_v5_0_AXI_ARID),
        .AXI_ARLEN(channel_ctrl_v5_0_AXI_ARLEN),
        .AXI_ARREADY(channel_ctrl_v5_0_AXI_ARREADY),
        .AXI_ARSIZE(channel_ctrl_v5_0_AXI_ARSIZE),
        .AXI_ARVALID(channel_ctrl_v5_0_AXI_ARVALID),
        .AXI_AWADDR(channel_ctrl_v5_0_AXI_AWADDR),
        .AXI_AWBURST(channel_ctrl_v5_0_AXI_AWBURST),
        .AXI_AWID(channel_ctrl_v5_0_AXI_AWID),
        .AXI_AWLEN(channel_ctrl_v5_0_AXI_AWLEN),
        .AXI_AWREADY(channel_ctrl_v5_0_AXI_AWREADY),
        .AXI_AWSIZE(channel_ctrl_v5_0_AXI_AWSIZE),
        .AXI_AWVALID(channel_ctrl_v5_0_AXI_AWVALID),
        .AXI_BID(channel_ctrl_v5_0_AXI_BID),
        .AXI_BREADY(channel_ctrl_v5_0_AXI_BREADY),
        .AXI_BRESP(channel_ctrl_v5_0_AXI_BRESP),
        .AXI_BVALID(channel_ctrl_v5_0_AXI_BVALID),
        .AXI_RDATA(channel_ctrl_v5_0_AXI_RDATA),
        .AXI_RID(channel_ctrl_v5_0_AXI_RID),
        .AXI_RLAST(channel_ctrl_v5_0_AXI_RLAST),
        .AXI_RREADY(channel_ctrl_v5_0_AXI_RREADY),
        .AXI_RRESP(channel_ctrl_v5_0_AXI_RRESP),
        .AXI_RVALID(channel_ctrl_v5_0_AXI_RVALID),
        .AXI_WDATA(channel_ctrl_v5_0_AXI_WDATA),
        .AXI_WID(channel_ctrl_v5_0_AXI_WID),
        .AXI_WLAST(channel_ctrl_v5_0_AXI_WLAST),
        .AXI_WREADY(channel_ctrl_v5_0_AXI_WREADY),
        .AXI_WSTRB(channel_ctrl_v5_0_AXI_WSTRB),
        .AXI_WVALID(channel_ctrl_v5_0_AXI_WVALID),
        .FIFO_READ_tdata(axis_register_slice_1_M_AXIS_TDATA),
        .FIFO_READ_tready(axis_register_slice_1_M_AXIS_TREADY),
        .FIFO_READ_tvalid(axis_register_slice_1_M_AXIS_TVALID),
        .FIFO_WRITE_tdata(channel_ctrl_v5_0_FIFO_WRITE_TDATA),
        .FIFO_WRITE_tkeep(channel_ctrl_v5_0_FIFO_WRITE_TKEEP),
        .FIFO_WRITE_tlast(channel_ctrl_v5_0_FIFO_WRITE_TLAST),
        .FIFO_WRITE_tready(channel_ctrl_v5_0_FIFO_WRITE_TREADY),
        .FIFO_WRITE_tvalid(channel_ctrl_v5_0_FIFO_WRITE_TVALID),
        .busy(user_hbm_channel_ctrl_ports_0_1_busy),
        .clk(Net),
        .rd_blockamount(user_hbm_channel_ctrl_ports_0_1_rd_blockamount),
        .rd_running(rd_running_0_1),
        .rd_startaddress(user_hbm_channel_ctrl_ports_0_1_rd_startaddress),
        .wr_blockamount(user_hbm_channel_ctrl_ports_0_1_wr_blockamount),
        .wr_running(wr_running_0_1),
        .wr_startaddress(user_hbm_channel_ctrl_ports_0_1_wr_startaddress));
  design_hbm_channel_xlconstant_2_0 xlconstant_2
       (.dout(xlconstant_2_dout));
endmodule
