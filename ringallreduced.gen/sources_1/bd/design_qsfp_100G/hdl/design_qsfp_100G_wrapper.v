//Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
//Date        : Tue Dec  6 08:22:21 2022
//Host        : dell-Vostro-3910-China-HDD-Protection running 64-bit Ubuntu 22.04.1 LTS
//Command     : generate_target design_qsfp_100G_wrapper.bd
//Design      : design_qsfp_100G_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_qsfp_100G_wrapper
   (CLK_IN1_D_0_clk_n,
    CLK_IN1_D_0_clk_p,
    gt_ref_clk_0_clk_n,
    gt_ref_clk_0_clk_p,
    gt_ref_clk_1_clk_n,
    gt_ref_clk_1_clk_p,
    gt_serial_port_0_grx_n,
    gt_serial_port_0_grx_p,
    gt_serial_port_0_gtx_n,
    gt_serial_port_0_gtx_p,
    gt_serial_port_1_grx_n,
    gt_serial_port_1_grx_p,
    gt_serial_port_1_gtx_n,
    gt_serial_port_1_gtx_p,
    reset,
    stat_rx_status_0,
    stat_rx_status_1);
  input CLK_IN1_D_0_clk_n;
  input CLK_IN1_D_0_clk_p;
  input gt_ref_clk_0_clk_n;
  input gt_ref_clk_0_clk_p;
  input gt_ref_clk_1_clk_n;
  input gt_ref_clk_1_clk_p;
  input [3:0]gt_serial_port_0_grx_n;
  input [3:0]gt_serial_port_0_grx_p;
  output [3:0]gt_serial_port_0_gtx_n;
  output [3:0]gt_serial_port_0_gtx_p;
  input [3:0]gt_serial_port_1_grx_n;
  input [3:0]gt_serial_port_1_grx_p;
  output [3:0]gt_serial_port_1_gtx_n;
  output [3:0]gt_serial_port_1_gtx_p;
  input reset;
  output stat_rx_status_0;
  output stat_rx_status_1;

  wire CLK_IN1_D_0_clk_n;
  wire CLK_IN1_D_0_clk_p;
  wire gt_ref_clk_0_clk_n;
  wire gt_ref_clk_0_clk_p;
  wire gt_ref_clk_1_clk_n;
  wire gt_ref_clk_1_clk_p;
  wire [3:0]gt_serial_port_0_grx_n;
  wire [3:0]gt_serial_port_0_grx_p;
  wire [3:0]gt_serial_port_0_gtx_n;
  wire [3:0]gt_serial_port_0_gtx_p;
  wire [3:0]gt_serial_port_1_grx_n;
  wire [3:0]gt_serial_port_1_grx_p;
  wire [3:0]gt_serial_port_1_gtx_n;
  wire [3:0]gt_serial_port_1_gtx_p;
  wire reset;
  wire stat_rx_status_0;
  wire stat_rx_status_1;

  design_qsfp_100G design_qsfp_100G_i
       (.CLK_IN1_D_0_clk_n(CLK_IN1_D_0_clk_n),
        .CLK_IN1_D_0_clk_p(CLK_IN1_D_0_clk_p),
        .gt_ref_clk_0_clk_n(gt_ref_clk_0_clk_n),
        .gt_ref_clk_0_clk_p(gt_ref_clk_0_clk_p),
        .gt_ref_clk_1_clk_n(gt_ref_clk_1_clk_n),
        .gt_ref_clk_1_clk_p(gt_ref_clk_1_clk_p),
        .gt_serial_port_0_grx_n(gt_serial_port_0_grx_n),
        .gt_serial_port_0_grx_p(gt_serial_port_0_grx_p),
        .gt_serial_port_0_gtx_n(gt_serial_port_0_gtx_n),
        .gt_serial_port_0_gtx_p(gt_serial_port_0_gtx_p),
        .gt_serial_port_1_grx_n(gt_serial_port_1_grx_n),
        .gt_serial_port_1_grx_p(gt_serial_port_1_grx_p),
        .gt_serial_port_1_gtx_n(gt_serial_port_1_gtx_n),
        .gt_serial_port_1_gtx_p(gt_serial_port_1_gtx_p),
        .reset(reset),
        .stat_rx_status_0(stat_rx_status_0),
        .stat_rx_status_1(stat_rx_status_1));
endmodule
