//Copyright 1986-2021 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2021.2 (win64) Build 3367213 Tue Oct 19 02:48:09 MDT 2021
//Date        : Sat May 21 18:50:35 2022
//Host        : DESKTOP-QFK5R8J running 64-bit major release  (build 9200)
//Command     : generate_target ring_wrapper.bd
//Design      : ring_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module ring_wrapper
   (CLK_IN1_D_clk_n_0,
    CLK_IN1_D_clk_p_0,
    STATUS_0,
    axis_i_0_tdata,
    axis_i_0_tkeep,
    axis_i_0_tlast,
    axis_i_0_tready,
    axis_i_0_tvalid,
    axis_o_0_tdata,
    axis_o_0_tkeep,
    axis_o_0_tlast,
    axis_o_0_tready,
    axis_o_0_tvalid,
    clk,
    ethorring,
    gt_ref_clk_0_clk_n_0,
    gt_ref_clk_0_clk_p_0,
    gt_serial_port_grx_n_0,
    gt_serial_port_grx_p_0,
    gt_serial_port_gtx_n_0,
    gt_serial_port_gtx_p_0,
    reset,
    reset_0,
    sfp_tx_disable_0_0,
    sfp_tx_disable_1_0,
    stat_rx_status_0_0,
    stat_rx_status_1_0,
    statusdata_in_0,
    statusdata_out_0,
    statusdata_wea_0,
    sys_resetn_0);
  input CLK_IN1_D_clk_n_0;
  input CLK_IN1_D_clk_p_0;
  output [13:0]STATUS_0;
  input [63:0]axis_i_0_tdata;
  input [7:0]axis_i_0_tkeep;
  input axis_i_0_tlast;
  output axis_i_0_tready;
  input axis_i_0_tvalid;
  output [63:0]axis_o_0_tdata;
  output [7:0]axis_o_0_tkeep;
  output axis_o_0_tlast;
  input axis_o_0_tready;
  output axis_o_0_tvalid;
  input clk;
  input ethorring;
  input gt_ref_clk_0_clk_n_0;
  input gt_ref_clk_0_clk_p_0;
  input [1:0]gt_serial_port_grx_n_0;
  input [1:0]gt_serial_port_grx_p_0;
  output [1:0]gt_serial_port_gtx_n_0;
  output [1:0]gt_serial_port_gtx_p_0;
  input reset;
  input reset_0;
  output sfp_tx_disable_0_0;
  output sfp_tx_disable_1_0;
  output stat_rx_status_0_0;
  output stat_rx_status_1_0;
  input [7:0]statusdata_in_0;
  output [7:0]statusdata_out_0;
  output statusdata_wea_0;
  input sys_resetn_0;

  wire CLK_IN1_D_clk_n_0;
  wire CLK_IN1_D_clk_p_0;
  wire [13:0]STATUS_0;
  wire [63:0]axis_i_0_tdata;
  wire [7:0]axis_i_0_tkeep;
  wire axis_i_0_tlast;
  wire axis_i_0_tready;
  wire axis_i_0_tvalid;
  wire [63:0]axis_o_0_tdata;
  wire [7:0]axis_o_0_tkeep;
  wire axis_o_0_tlast;
  wire axis_o_0_tready;
  wire axis_o_0_tvalid;
  wire clk;
  wire ethorring;
  wire gt_ref_clk_0_clk_n_0;
  wire gt_ref_clk_0_clk_p_0;
  wire [1:0]gt_serial_port_grx_n_0;
  wire [1:0]gt_serial_port_grx_p_0;
  wire [1:0]gt_serial_port_gtx_n_0;
  wire [1:0]gt_serial_port_gtx_p_0;
  wire reset;
  wire reset_0;
  wire sfp_tx_disable_0_0;
  wire sfp_tx_disable_1_0;
  wire stat_rx_status_0_0;
  wire stat_rx_status_1_0;
  wire [7:0]statusdata_in_0;
  wire [7:0]statusdata_out_0;
  wire statusdata_wea_0;
  wire sys_resetn_0;

  ring ring_i
       (.CLK_IN1_D_clk_n_0(CLK_IN1_D_clk_n_0),
        .CLK_IN1_D_clk_p_0(CLK_IN1_D_clk_p_0),
        .STATUS_0(STATUS_0),
        .axis_i_0_tdata(axis_i_0_tdata),
        .axis_i_0_tkeep(axis_i_0_tkeep),
        .axis_i_0_tlast(axis_i_0_tlast),
        .axis_i_0_tready(axis_i_0_tready),
        .axis_i_0_tvalid(axis_i_0_tvalid),
        .axis_o_0_tdata(axis_o_0_tdata),
        .axis_o_0_tkeep(axis_o_0_tkeep),
        .axis_o_0_tlast(axis_o_0_tlast),
        .axis_o_0_tready(axis_o_0_tready),
        .axis_o_0_tvalid(axis_o_0_tvalid),
        .clk(clk),
        .ethorring(ethorring),
        .gt_ref_clk_0_clk_n_0(gt_ref_clk_0_clk_n_0),
        .gt_ref_clk_0_clk_p_0(gt_ref_clk_0_clk_p_0),
        .gt_serial_port_grx_n_0(gt_serial_port_grx_n_0),
        .gt_serial_port_grx_p_0(gt_serial_port_grx_p_0),
        .gt_serial_port_gtx_n_0(gt_serial_port_gtx_n_0),
        .gt_serial_port_gtx_p_0(gt_serial_port_gtx_p_0),
        .reset(reset),
        .reset_0(reset_0),
        .sfp_tx_disable_0_0(sfp_tx_disable_0_0),
        .sfp_tx_disable_1_0(sfp_tx_disable_1_0),
        .stat_rx_status_0_0(stat_rx_status_0_0),
        .stat_rx_status_1_0(stat_rx_status_1_0),
        .statusdata_in_0(statusdata_in_0),
        .statusdata_out_0(statusdata_out_0),
        .statusdata_wea_0(statusdata_wea_0),
        .sys_resetn_0(sys_resetn_0));
endmodule
