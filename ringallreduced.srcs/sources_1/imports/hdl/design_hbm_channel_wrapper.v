//Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
//Date        : Mon Nov 21 21:54:31 2022
//Host        : dell-Vostro-3910-China-HDD-Protection running 64-bit Ubuntu 22.04.1 LTS
//Command     : generate_target design_hbm_channel_wrapper.bd
//Design      : design_hbm_channel_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_hbm_channel_wrapper
   (HBM_AXI_araddr,
    HBM_AXI_arburst,
    HBM_AXI_arid,
    HBM_AXI_arlen,
    HBM_AXI_arready,
    HBM_AXI_arsize,
    HBM_AXI_arvalid,
    HBM_AXI_awaddr,
    HBM_AXI_awburst,
    HBM_AXI_awid,
    HBM_AXI_awlen,
    HBM_AXI_awready,
    HBM_AXI_awsize,
    HBM_AXI_awvalid,
    HBM_AXI_bid,
    HBM_AXI_bready,
    HBM_AXI_bresp,
    HBM_AXI_bvalid,
    HBM_AXI_rdata,
    HBM_AXI_rid,
    HBM_AXI_rlast,
    HBM_AXI_rready,
    HBM_AXI_rresp,
    HBM_AXI_rvalid,
    HBM_AXI_wdata,
    HBM_AXI_wid,
    HBM_AXI_wlast,
    HBM_AXI_wready,
    HBM_AXI_wstrb,
    HBM_AXI_wvalid,
    HBM_READ_tdata,
    HBM_READ_tkeep,
    HBM_READ_tlast,
    HBM_READ_tready,
    HBM_READ_tvalid,
    HBM_WRITE_tdata,
    HBM_WRITE_tready,
    HBM_WRITE_tvalid,
    clk_312,
    clk_450,
    rd_running,
    user_hbm_channel_ctrl_ports_busy,
    user_hbm_channel_ctrl_ports_rd_blockamount,
    user_hbm_channel_ctrl_ports_rd_startaddress,
    user_hbm_channel_ctrl_ports_wr_blockamount,
    user_hbm_channel_ctrl_ports_wr_startaddress,
    wr_running);
  output [32:0]HBM_AXI_araddr;
  output [1:0]HBM_AXI_arburst;
  
  output [5:0]HBM_AXI_arid;
  output [3:0]HBM_AXI_arlen;
  
  input HBM_AXI_arready;
  output [2:0]HBM_AXI_arsize;
  output HBM_AXI_arvalid;
  output [32:0]HBM_AXI_awaddr;
  output [1:0]HBM_AXI_awburst;
 
  output [5:0]HBM_AXI_awid;
  output [3:0]HBM_AXI_awlen;
  
  input HBM_AXI_awready;
  output [2:0]HBM_AXI_awsize;
  output HBM_AXI_awvalid;
  input [5:0]HBM_AXI_bid;
  output HBM_AXI_bready;
  input [1:0]HBM_AXI_bresp;
  input HBM_AXI_bvalid;
  input [255:0]HBM_AXI_rdata;
  input [5:0]HBM_AXI_rid;
  input HBM_AXI_rlast;
  output HBM_AXI_rready;
  input [1:0]HBM_AXI_rresp;
  input HBM_AXI_rvalid;
  output [255:0]HBM_AXI_wdata;
  output [5:0]HBM_AXI_wid;
  output HBM_AXI_wlast;
  input HBM_AXI_wready;
  output [31:0]HBM_AXI_wstrb;
  output HBM_AXI_wvalid;
  (* CLK_DOMAIN="design_hbm_clk_312MHz" *)
  output [255:0]HBM_READ_tdata;
  output [31:0]HBM_READ_tkeep;
  output HBM_READ_tlast;
  input HBM_READ_tready;
  output HBM_READ_tvalid;
  (* CLK_DOMAIN="design_hbm_clk_312MHz" *)
  input [255:0]HBM_WRITE_tdata;
  output HBM_WRITE_tready;
  input HBM_WRITE_tvalid;
  input clk_312;
  input clk_450;
  
  (* X_INTERFACE_MODE = "slave" *)                                                                                
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports_rtl:1.0 ctrl_ports rd_blockamount"  *)input [31:0]user_hbm_channel_ctrl_ports_rd_blockamount;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports_rtl:1.0 ctrl_ports rd_run"          *)input rd_running;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports_rtl:1.0 ctrl_ports rd_startaddress" *)input [31:0]user_hbm_channel_ctrl_ports_rd_startaddress;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports_rtl:1.0 ctrl_ports wr_blockamount"  *)input [31:0]user_hbm_channel_ctrl_ports_wr_blockamount;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports_rtl:1.0 ctrl_ports wr_run"          *)input wr_running;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports_rtl:1.0 ctrl_ports wr_startaddress" *)input [31:0]user_hbm_channel_ctrl_ports_wr_startaddress;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports_rtl:1.0 ctrl_ports busy" *)output user_hbm_channel_ctrl_ports_busy;

  wire [32:0]HBM_AXI_araddr;
  wire [1:0]HBM_AXI_arburst;
  wire [3:0]HBM_AXI_arcache;
  wire [5:0]HBM_AXI_arid;
  wire [3:0]HBM_AXI_arlen;
  wire [1:0]HBM_AXI_arlock;
  wire [2:0]HBM_AXI_arprot;
  wire [3:0]HBM_AXI_arqos;
  wire HBM_AXI_arready;
  wire [2:0]HBM_AXI_arsize;
  wire HBM_AXI_arvalid;
  wire [32:0]HBM_AXI_awaddr;
  wire [1:0]HBM_AXI_awburst;
  wire [3:0]HBM_AXI_awcache;
  wire [5:0]HBM_AXI_awid;
  wire [3:0]HBM_AXI_awlen;
  wire [1:0]HBM_AXI_awlock;
  wire [2:0]HBM_AXI_awprot;
  wire [3:0]HBM_AXI_awqos;
  wire HBM_AXI_awready;
  wire [2:0]HBM_AXI_awsize;
  wire HBM_AXI_awvalid;
  wire [5:0]HBM_AXI_bid;
  wire HBM_AXI_bready;
  wire [1:0]HBM_AXI_bresp;
  wire HBM_AXI_bvalid;
  wire [255:0]HBM_AXI_rdata;
  wire [5:0]HBM_AXI_rid;
  wire HBM_AXI_rlast;
  wire HBM_AXI_rready;
  wire [1:0]HBM_AXI_rresp;
  wire HBM_AXI_rvalid;
  wire [255:0]HBM_AXI_wdata;
  wire [5:0]HBM_AXI_wid;
  wire HBM_AXI_wlast;
  wire HBM_AXI_wready;
  wire [31:0]HBM_AXI_wstrb;
  wire HBM_AXI_wvalid;
  wire [255:0]HBM_READ_tdata;
  wire [31:0]HBM_READ_tkeep;
  wire HBM_READ_tlast;
  wire HBM_READ_tready;
  wire HBM_READ_tvalid;
  wire [255:0]HBM_WRITE_tdata;
  wire HBM_WRITE_tready;
  wire HBM_WRITE_tvalid;
  wire clk_312;
  wire clk_450;
  wire rd_running;
  wire user_hbm_channel_ctrl_ports_busy;
  wire [31:0]user_hbm_channel_ctrl_ports_rd_blockamount;
  wire [31:0]user_hbm_channel_ctrl_ports_rd_startaddress;
  wire [31:0]user_hbm_channel_ctrl_ports_wr_blockamount;
  wire [31:0]user_hbm_channel_ctrl_ports_wr_startaddress;
  wire wr_running;

  design_hbm_channel design_hbm_channel_i
       (.HBM_AXI_araddr(HBM_AXI_araddr),
        .HBM_AXI_arburst(HBM_AXI_arburst),
        //.HBM_AXI_arcache(HBM_AXI_arcache),
        .HBM_AXI_arid(HBM_AXI_arid),
        .HBM_AXI_arlen(HBM_AXI_arlen),
        //.HBM_AXI_arlock(HBM_AXI_arlock),
       // .HBM_AXI_arprot(HBM_AXI_arprot),
       // .HBM_AXI_arqos(HBM_AXI_arqos),
        .HBM_AXI_arready(HBM_AXI_arready),
        .HBM_AXI_arsize(HBM_AXI_arsize),
        .HBM_AXI_arvalid(HBM_AXI_arvalid),
        .HBM_AXI_awaddr(HBM_AXI_awaddr),
        .HBM_AXI_awburst(HBM_AXI_awburst),
        //.HBM_AXI_awcache(HBM_AXI_awcache),
        .HBM_AXI_awid(HBM_AXI_awid),
        .HBM_AXI_awlen(HBM_AXI_awlen),
        //.HBM_AXI_awlock(HBM_AXI_awlock),
      //  .HBM_AXI_awprot(HBM_AXI_awprot),
       // .HBM_AXI_awqos(HBM_AXI_awqos),
        .HBM_AXI_awready(HBM_AXI_awready),
        .HBM_AXI_awsize(HBM_AXI_awsize),
        .HBM_AXI_awvalid(HBM_AXI_awvalid),
        .HBM_AXI_bid(HBM_AXI_bid),
        .HBM_AXI_bready(HBM_AXI_bready),
        .HBM_AXI_bresp(HBM_AXI_bresp),
        .HBM_AXI_bvalid(HBM_AXI_bvalid),
        .HBM_AXI_rdata(HBM_AXI_rdata),
        .HBM_AXI_rid(HBM_AXI_rid),
        .HBM_AXI_rlast(HBM_AXI_rlast),
        .HBM_AXI_rready(HBM_AXI_rready),
        .HBM_AXI_rresp(HBM_AXI_rresp),
        .HBM_AXI_rvalid(HBM_AXI_rvalid),
        .HBM_AXI_wdata(HBM_AXI_wdata),
        .HBM_AXI_wid(HBM_AXI_wid),
        .HBM_AXI_wlast(HBM_AXI_wlast),
        .HBM_AXI_wready(HBM_AXI_wready),
        .HBM_AXI_wstrb(HBM_AXI_wstrb),
        .HBM_AXI_wvalid(HBM_AXI_wvalid),
        .HBM_READ_tdata(HBM_READ_tdata),
        .HBM_READ_tkeep(HBM_READ_tkeep),
        .HBM_READ_tlast(HBM_READ_tlast),
        .HBM_READ_tready(HBM_READ_tready),
        .HBM_READ_tvalid(HBM_READ_tvalid),
        .HBM_WRITE_tdata(HBM_WRITE_tdata),
        .HBM_WRITE_tready(HBM_WRITE_tready),
        .HBM_WRITE_tvalid(HBM_WRITE_tvalid),
        .clk_312(clk_312),
        .clk_450(clk_450),
        .rd_running(rd_running),
        .user_hbm_channel_ctrl_ports_busy(user_hbm_channel_ctrl_ports_busy),
        .user_hbm_channel_ctrl_ports_rd_blockamount(user_hbm_channel_ctrl_ports_rd_blockamount),
        .user_hbm_channel_ctrl_ports_rd_startaddress(user_hbm_channel_ctrl_ports_rd_startaddress),
        .user_hbm_channel_ctrl_ports_wr_blockamount(user_hbm_channel_ctrl_ports_wr_blockamount),
        .user_hbm_channel_ctrl_ports_wr_startaddress(user_hbm_channel_ctrl_ports_wr_startaddress),
        .wr_running(wr_running));
endmodule
