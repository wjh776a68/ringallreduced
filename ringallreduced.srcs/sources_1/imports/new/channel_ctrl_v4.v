`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08/18/2022 02:59:06 PM
// Design Name: 
// Module Name: crossread_v3
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module channel_ctrl_v4 #(
    parameter BASE_ADDRESS = 33'h0,
    parameter HIGH_ADDRESS = 33'h100
) (
    output reg [32:0]   AXI_ARADDR = 0,//assigned
    output reg [1:0]    AXI_ARBURST = 0,//assigned
    output     [5:0]    AXI_ARID, //assigned
    output reg [3:0]    AXI_ARLEN = 0,    //assigned
    input               AXI_ARREADY,
    output reg [2:0]    AXI_ARSIZE = 0,//assigned
    output reg          AXI_ARVALID = 0,//assigned
    output reg [32:0]   AXI_AWADDR = 0,//assigned
    output reg [1:0]    AXI_AWBURST = 0,//assigned
    output     [5:0]    AXI_AWID, //assigned
    output reg [3:0]    AXI_AWLEN = 0,//assigned
    input               AXI_AWREADY,//assigned
    output reg [2:0]    AXI_AWSIZE = 0,//assigned
    output reg          AXI_AWVALID = 0,//assigned
    input      [5:0]    AXI_BID,
    output              AXI_BREADY,//assigned
    input      [1:0]    AXI_BRESP,
    input               AXI_BVALID,
    input      [255:0]  AXI_RDATA,
    input      [5:0]    AXI_RID,
    input               AXI_RLAST,
    output              AXI_RREADY,//assigned
    input      [1:0]    AXI_RRESP,
    input               AXI_RVALID,
    output     [5:0]    AXI_WID, //assigned
    output reg [255:0]  AXI_WDATA = 0,//assigned
    output reg          AXI_WLAST = 0,//assigned
    input               AXI_WREADY,//assigned
    output [31:0]       AXI_WSTRB,//assigned
    output reg          AXI_WVALID = 0,//assigned
    output     [31:0]   AXI_WDATA_PARITY,
    
    /*output reg w_valid = 0,
    output reg b_valid = 0,
    
    output reg ar_valid = 0,
    output reg r_valid = 0,*/
    
    input FIFO_WRITE_prog_full,
    (* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 FIFO_WRITE FULL" *)
    input FIFO_WRITE_full,
    (* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 FIFO_WRITE WR_DATA" *)
    output reg [255:0] FIFO_WRITE_din = 0,
    (* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 FIFO_WRITE WR_EN" *)
    output reg FIFO_WRITE_wr_en = 0,
    //output read_next,
    
    //input FIFO_READ_prog_empty,
    (* X_INTERFACE_MODE = "MASTER" *)
    (* X_INTERFACE_INFO = "xilinx.com:interface:fifo_read:1.0 FIFO_READ EMPTY" *)
    input FIFO_READ_empty,
    (* X_INTERFACE_INFO = "xilinx.com:interface:fifo_read:1.0 FIFO_READ RD_DATA" *)
    input [255:0] FIFO_READ_dout,
    (* X_INTERFACE_INFO = "xilinx.com:interface:fifo_read:1.0 FIFO_READ RD_EN" *)
    output reg FIFO_READ_rd_en = 0,
    
    input rd_running,
    //input mode,
    input [31:0] rd_startaddress,
    input [31:0] rd_blockamount,
    
    input wr_running,
    //input mode,
    input [31:0] wr_startaddress,
    input [31:0] wr_blockamount,
    
    output reg busy,
    
    input clk
    
    //output aclk, //
    //output reg aresetn //
    );
    
    
    
    //reg [255:0] FIFO_WRITE_din_next, FIFO_WRITE_wr_en_next;
    //assign FIFO_WRITE_din = AXI_RDATA;
    //assign FIFO_WRITE_wr_en = ~FIFO_WRITE_prog_full & AXI_RVALID;
    //reg [16:0] readbuf;
    //assign read_next = readbuf[16];
    
    //assign aclk = clk;
    //assign aresetn = resetn;
    assign AXI_ARID = 0;
    assign AXI_AWID = 0;
    assign AXI_RID = 0;
    //assign AXI_WID = 0;
    assign AXI_BREADY = 1;
    assign AXI_RREADY = ~FIFO_WRITE_prog_full;//~FIFO_WRITE_full;//1;
    assign AXI_WSTRB = 32'hffffffff;
    assign AXI_WDATA_PARITY =  
    {{^{AXI_WDATA[255 : 248]}},
    {^{AXI_WDATA[247 : 240]}},
    {^{AXI_WDATA[239 : 232]}},
    {^{AXI_WDATA[231 : 224]}},
    {^{AXI_WDATA[223 : 216]}},
    {^{AXI_WDATA[215 : 208]}},
    {^{AXI_WDATA[207 : 200]}},
    {^{AXI_WDATA[199 : 192]}},
    {^{AXI_WDATA[191 : 184]}},
    {^{AXI_WDATA[183 : 176]}},
    {^{AXI_WDATA[175 : 168]}},
    {^{AXI_WDATA[167 : 160]}},
    {^{AXI_WDATA[159 : 152]}},
    {^{AXI_WDATA[151 : 144]}},
    {^{AXI_WDATA[143 : 136]}},
    {^{AXI_WDATA[135 : 128]}},
    {^{AXI_WDATA[127 : 120]}},
    {^{AXI_WDATA[119 : 112]}},
    {^{AXI_WDATA[111 : 104]}},
    {^{AXI_WDATA[103 : 96] }},
    {^{AXI_WDATA[95 : 88]  }},
    {^{AXI_WDATA[87 : 80]  }},
    {^{AXI_WDATA[79 : 72]  }},
    {^{AXI_WDATA[71 : 64]  }},
    {^{AXI_WDATA[63 : 56]  }},
    {^{AXI_WDATA[55 : 48]  }},
    {^{AXI_WDATA[47 : 40]  }},
    {^{AXI_WDATA[39 : 32]  }},
    {^{AXI_WDATA[31 : 24]  }},
    {^{AXI_WDATA[23 : 16]  }},
    {^{AXI_WDATA[15 : 8]   }},
    {^{AXI_WDATA[7 : 0]    }} };
    reg [15:0] currentblock, wr_currentblock, wrdata_currentblock, currentblockplus16, wr_currentblockplus16, wrdata_currentblockplus1;
    /*always @ (*) begin
        currentblockplus16 = currentblock + 24'd16;
    end*/
    //reg [8:0] delay;
    reg [23:0] RD_ADDR, RD_ADDRplus512;
    reg [23:0] WR_ADDR, WR_ADDRplus512;
    always @ (*) begin
        RD_ADDRplus512 = RD_ADDR + 1'b1;//34'b1000000000;
    end
    
    always @ (*) begin
        WR_ADDRplus512 = WR_ADDR + 1'b1;//34'b1000000000;
    end
    
    reg [2:0] STAT = 0;
    reg       STAT_wrdata = 0;
    //assign busy = ~(STAT == 'b0);// | ~(STAT_wrdata == 'b0);
    //wire condition_1 = (currentblock == blockamount);
    reg [23:0] write_req_counter, write_req_counterplus1;
    
   // reg [31:0] read_req_counter;
    reg [23:0] bvalid_counter, bvalid_counterplus1;
    reg [23:0] rvalid_counter, rvalid_counterplus1;
    always @ (*) begin
        bvalid_counterplus1 = bvalid_counter + 1;
    end
    
    always @ (posedge clk) begin
        if (/*wr_running && */STAT==0) begin
            bvalid_counter <= 0;
        end
        else if (AXI_BVALID) begin
            bvalid_counter <= bvalid_counterplus1;//bvalid_counter + 1;
        end
    end
    
    always @ (*) begin
        rvalid_counterplus1 = rvalid_counter + 1;
    end
    
    always @ (posedge clk) begin
        if (/*rd_running && */STAT==0) begin
            rvalid_counter <= 0;
        end
        else if (AXI_RVALID & AXI_RREADY) begin
            rvalid_counter <= rvalid_counterplus1;//rvalid_counter + 1;
        end
    end
    
    always @ (*) begin
        write_req_counterplus1 = write_req_counter + 1;
    end
    
    always @ (*) begin
        currentblockplus16 = currentblock + 1;//24'b10000;//24'd16;
    end
    
    always @ (*) begin
        wr_currentblockplus16 = wr_currentblock + 1;//24'b10000;//24'd16;
    end
    
    reg busy_r_r = 0, busy_r = 0;
    reg [255:0] FIFO_WRITE_din_last;
    reg AXI_RVALID_last, FIFO_WRITE_prog_full_last;
    always @ (posedge clk) begin
       /* w_valid  <= AXI_WVALID;
        b_valid  <= AXI_BVALID;        
        ar_valid <= AXI_ARVALID;
        r_valid  <= AXI_RVALID & AXI_RLAST;*/
        busy_r_r <= busy_r;
        busy <= busy_r_r;
    
        FIFO_WRITE_din_last <= AXI_RDATA;
        FIFO_WRITE_din <= FIFO_WRITE_din_last;
        AXI_RVALID_last <= AXI_RVALID;
        FIFO_WRITE_prog_full_last <= FIFO_WRITE_prog_full;
        FIFO_WRITE_wr_en <= ~FIFO_WRITE_prog_full_last & AXI_RVALID_last;
        //readbuf <= {readbuf[15:0],read};
        
        
            case (STAT)
                0: begin
                    RD_ADDR <= rd_startaddress[31:9];//BASE_ADDRESS;
                    currentblock <= 0;
                    WR_ADDR <= wr_startaddress[31:9];//BASE_ADDRESS;
                    wr_currentblock <= 0;
                    write_req_counter <= 0;
                    case ({rd_running, wr_running})
                        2'b01: begin
                            STAT <= 2;
                            busy_r <= 1;
                        end
                        2'b10: begin
                            STAT <= 1;
                            busy_r <= 1;
                        end
                        2'b11: begin
                            STAT <= 0;
                            busy_r <= 0;
                        end
                        2'b00: begin
                            STAT <= 0;
                            busy_r <= 0;
                        end
                    endcase
                    /*if (rd_running) begin
                        RD_ADDR <= rd_startaddress[31:9];//BASE_ADDRESS;
                        currentblock <= 0;
                        STAT <= 1;
                        //read_req_counter <= 0;
                        //AXI_ARVALID <= 0;
                    end
                    else if (wr_running) begin
                        WR_ADDR <= wr_startaddress[31:9];//BASE_ADDRESS;
                        wr_currentblock <= 0;
                        write_req_counter <= 0;
                        STAT <= 2;
                    end*/
                    AXI_ARVALID <= 1'b0;
                    AXI_AWVALID <= 0;
                end
                /*1: begin
                    if(AXI_ARREADY) begin
                        case(mode)
                            0: STAT <= 2;
                            1: STAT <= 3;    
                        endcase
                    end
                end*/
                1: begin
                    /*if (~AXI_ARVALID & AXI_ARREADY) begin
                        AXI_ARVALID <= 1;
                    end
                    else */
                    
                    AXI_ARVALID <= 1'b1;
                    AXI_ARSIZE <= 3'b101;
                    AXI_ARBURST <= 2'b01;
                    
                    if (AXI_ARREADY) begin    
                        AXI_ARADDR <= {RD_ADDR, {9{1'b0}}}; //33'b0;//AXI_ARADDR + 6'b100000;
                        RD_ADDR <= RD_ADDRplus512;//RD_ADDR + {34'b1000000000};//2^4 * 2^5
                        //STAT <= 1;
                        //read_req_counter <= read_req_counter + 1;
                        currentblock <= currentblockplus16;//{currentblock + 32'd16}; 
                        /*if (currentblock >= rd_blockamount) begin
                            STAT <= 0;
                        end                
                        else*/ if ({currentblockplus16, {4{1'b0}}} >= rd_blockamount) begin//RD_ADDR==HIGH_ADDRESS) begin
                            AXI_ARLEN <= rd_blockamount[3:0] - 1;//rd_blockamount - currentblock;//4'b1111;
                            STAT <= 5;
                        end
                        else begin
                            AXI_ARLEN <= 4'b1111;
                            //STAT <= 1;
                        end
                        
                    end
                    
                end
                2: begin
                    
                    AXI_AWVALID <= 1'b1;
                    AXI_AWSIZE <= 3'b101;
                    AXI_AWBURST <= 2'b01;
                    
                    if (/*~FIFO_READ_empty & */AXI_AWREADY) begin
                        write_req_counter <= write_req_counterplus1;//write_req_counter + 1;
                        AXI_AWADDR <= {WR_ADDR, {9{1'b0}}};//wr_startaddress;//RD_ADDR;//33'b0;//AXI_ARADDR + 6'b100000;
                        WR_ADDR <= WR_ADDRplus512;//{RD_ADDR + 34'b1000000000};//2^4 * 2^5
                        //STAT <= 2;
                        wr_currentblock <= wr_currentblockplus16;//{currentblock + 32'd16};
                        if ({wr_currentblockplus16, {4{1'b0}}} >= wr_blockamount) begin//RD_ADDR==HIGH_ADDRESS) begin
                            STAT <= 3;
                            AXI_AWLEN <= wr_blockamount[3:0] - 1;//wr_blockamount - currentblock;
                        end
                        else begin
                            AXI_AWLEN <= 4'b1111;
                            //STAT <= 2;
                        end
                        
                    end
                   
                    //if(delay==16) begin
                    //    delay <= delay + 1;
                    //end
                    //else begin
                    //    STAT <= 1;
                    //end
                    //AXI_ARVALID <= 0;
                    //STAT <= 0;
                end
            3: begin
                 //AXI_AWADDR <= {WR_ADDR, {9{1'b0}}};
                 if (STAT_wrdata == 'b0) begin
                    STAT <= 'd4;
                 end
                 if (AXI_AWREADY) begin 
                    AXI_AWVALID <= 1'b0;
                end
            end
            4: begin
                if (bvalid_counter == write_req_counter) begin
                    STAT <= 'd0;
                end
            end
            5: begin
                if (AXI_ARREADY) begin 
                    AXI_ARVALID <= 0;
                end
                if (rvalid_counter == rd_blockamount) begin
                    STAT <= 'd0;
                end
            end
            endcase
            //if (STAT < 3) begin
            //    STAT <= STAT + 1;
            //end         
        
    end
    
    reg [3:0] writecounter, writecounterplus1;
    reg AXI_WVALID_LAST;//, AXI_WVALID_LAST_LAST;
    reg [1:0] WDATA_BUF_COUNT;
    reg [255:0] WDATA_BUF[1:0];
    reg [255:0] FIFO_READ_dout_last;
    always @ (*) begin
        writecounterplus1 = writecounter + 1;
    end
    
    always @ (*) begin
        wrdata_currentblockplus1 = wrdata_currentblock + 24'd1;
    end
    
    always @ (posedge clk) begin
            case (STAT_wrdata) 
            1'b0: begin
                AXI_WVALID_LAST <= 1'b0;
                FIFO_READ_rd_en <= 1'b0;
                WDATA_BUF_COUNT <= 'b0;
                AXI_WVALID <= 1'b0;
                AXI_WLAST <= 1'b0;
                wrdata_currentblock <= 1;//2;
                writecounter <= 4'b1;
                if (STAT[1] & ~STAT[0]) begin
                    STAT_wrdata <= 1'b1;
                end
            end
            1'b1: begin
                /*if (wrdata_currentblock == wr_blockamount) begin
                    if (AXI_WREADY) begin
                        STAT_wrdata <= 1'b0;
                    end
                    AXI_WLAST <= 1'b1;
                    AXI_WDATA <= FIFO_READ_dout;
                    AXI_WVALID <= 1'b1;
                    //FIFO_READ_rd_en <= 0;
                end
                else begin*/
                //FIFO_READ_dout_last <= FIFO_READ_dout;
                FIFO_READ_rd_en <= (wrdata_currentblockplus1 < wr_blockamount) & AXI_WREADY & ~FIFO_READ_empty;// & (WDATA_BUF_COUNT == 0);
                AXI_WVALID_LAST <= FIFO_READ_rd_en;//(~FIFO_READ_empty & AXI_WREADY);
                //AXI_WVALID_LAST_LAST <= AXI_WVALID_LAST;
                if (AXI_WVALID_LAST & AXI_WREADY) begin
                    /*WDATA_BUF[WDATA_BUF_COUNT] <= FIFO_READ_dout;
                    WDATA_BUF[1] <= WDATA_BUF[2];
                    WDATA_BUF[0] <= WDATA_BUF[1];*/
                    AXI_WVALID <= 1;
                    AXI_WDATA <= FIFO_READ_dout;//WDATA_BUF[0];
                end
                else if (AXI_WVALID_LAST & ~AXI_WREADY) begin
                    WDATA_BUF[WDATA_BUF_COUNT] <= FIFO_READ_dout;//FIFO_READ_dout;
                    //WDATA_BUF[1] <= WDATA_BUF[2];
                    //WDATA_BUF[0] <= WDATA_BUF[1];
                    WDATA_BUF_COUNT <= WDATA_BUF_COUNT + 1;
                    //AXI_WVALID <= 0;
                end
                else if (~AXI_WVALID_LAST & AXI_WREADY & WDATA_BUF_COUNT!=0) begin
                    //WDATA_BUF[WDATA_BUF_COUNT] <= FIFO_READ_dout;
                    WDATA_BUF[1] <= WDATA_BUF[2];
                    WDATA_BUF[0] <= WDATA_BUF[1];
                    AXI_WVALID <= 1;
                    AXI_WDATA <= WDATA_BUF[0];
                    WDATA_BUF_COUNT <= WDATA_BUF_COUNT - 1;
                end
                else begin
                    //AXI_WVALID <= 0;
                end
                
                //AXI_WDATA <= FIFO_READ_dout;
                //AXI_WVALID <= AXI_WVALID_LAST;
                
                if(AXI_WVALID & AXI_WREADY) begin
                    if (wrdata_currentblock == wr_blockamount) begin
                        STAT_wrdata <= 1'b0;
                    end 
                    
                    if (wrdata_currentblock == wr_blockamount || writecounter == 4'd15) begin
                        AXI_WLAST <= 1'b1;
                        writecounter <= 4'd0;
                    end
                    /*if(writecounter == 4'd15) begin
                        writecounter <= 4'd0;
                        AXI_WLAST <= 1'b1;
                    end*/
                    else begin
                        AXI_WLAST <= 1'b0;
                        writecounter <= writecounterplus1;//{writecounter + 4'd1};
                    end
                    wrdata_currentblock <= wrdata_currentblockplus1;//wrdata_currentblock + 1;
                end
                //end
                
            end
            endcase
            /*if(~FIFO_READ_empty & AXI_WREADY & STAT[1] & ~STAT[0]) begin
                FIFO_READ_rd_en <= 1'b1;
                AXI_WVALID_LAST <= 1'b1;
            end
            else begin
                FIFO_READ_rd_en <= 1'b0;
                AXI_WVALID_LAST <= 1'b0;
            end*/
            
        end
        
    
endmodule



/*`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/21/2022 09:29:44 AM
// Design Name: 
// Module Name: channel_ctrl_v4
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module channel_ctrl_v4 #(
    parameter BASE_ADDRESS = 33'h0,
    parameter HIGH_ADDRESS = 33'h100
) (
    output reg [32:0]   AXI_ARADDR,//assigned
    output reg [1:0]    AXI_ARBURST,//assigned
    output     [5:0]    AXI_ARID, //assigned
    output reg [3:0]    AXI_ARLEN,    //assigned
    input               AXI_ARREADY,
    output reg [2:0]    AXI_ARSIZE,//assigned
    output reg          AXI_ARVALID,//assigned
    output reg [32:0]   AXI_AWADDR,//assigned
    output reg [1:0]    AXI_AWBURST,//assigned
    output     [5:0]    AXI_AWID, //assigned
    output reg [3:0]    AXI_AWLEN,//assigned
    input               AXI_AWREADY,//assigned
    output reg [2:0]    AXI_AWSIZE,//assigned
    output reg          AXI_AWVALID,//assigned
    input      [5:0]    AXI_BID,
    output              AXI_BREADY,//assigned
    input      [1:0]    AXI_BRESP,
    input               AXI_BVALID,
    input      [255:0]  AXI_RDATA,
    input      [5:0]    AXI_RID,
    input               AXI_RLAST,
    output              AXI_RREADY,//assigned
    input      [1:0]    AXI_RRESP,
    input               AXI_RVALID,
    output     [5:0]    AXI_WID, //assigned
    output [255:0]  AXI_WDATA,//assigned
    output reg          AXI_WLAST,//assigned
    input               AXI_WREADY,//assigned
    output [31:0]       AXI_WSTRB,//assigned
    output reg          AXI_WVALID,//assigned
    output     [31:0]   AXI_WDATA_PARITY,
    
    output reg w_valid,
    output reg b_valid,
    output reg ar_valid,
    output reg r_valid,
    
    input FIFO_WRITE_prog_full,
    (* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 FIFO_WRITE FULL" *)
    input FIFO_WRITE_full,
    (* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 FIFO_WRITE WR_DATA" *)
    output reg [255:0] FIFO_WRITE_din,
    (* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 FIFO_WRITE WR_EN" *)
    output reg FIFO_WRITE_wr_en,
    
    (* X_INTERFACE_MODE = "MASTER" *)
    (* X_INTERFACE_INFO = "xilinx.com:interface:fifo_read:1.0 FIFO_READ EMPTY" *)
    input FIFO_READ_empty,
    (* X_INTERFACE_INFO = "xilinx.com:interface:fifo_read:1.0 FIFO_READ RD_DATA" *)
    input [255:0] FIFO_READ_dout,
    (* X_INTERFACE_INFO = "xilinx.com:interface:fifo_read:1.0 FIFO_READ RD_EN" *)
    output FIFO_READ_rd_en,
    
    input rd_running,
    input [31:0] rd_startaddress,
    input [31:0] rd_blockamount,
    
    input wr_running,
    input [31:0] wr_startaddress,
    input [31:0] wr_blockamount,
    
    output busy,
    
    input clk
    
    );
    
    assign AXI_ARID = 0;
    assign AXI_AWID = 0;
    assign AXI_RID = 0;
    assign AXI_BREADY = 1;
    assign AXI_RREADY = ~FIFO_WRITE_prog_full;//~FIFO_WRITE_full;//1;
    assign AXI_WDATA = FIFO_READ_dout;
    assign AXI_WSTRB = 32'hffffffff;
    assign AXI_WDATA_PARITY =  
    {{^{AXI_WDATA[255 : 248]}},
    {^{AXI_WDATA[247 : 240]}},
    {^{AXI_WDATA[239 : 232]}},
    {^{AXI_WDATA[231 : 224]}},
    {^{AXI_WDATA[223 : 216]}},
    {^{AXI_WDATA[215 : 208]}},
    {^{AXI_WDATA[207 : 200]}},
    {^{AXI_WDATA[199 : 192]}},
    {^{AXI_WDATA[191 : 184]}},
    {^{AXI_WDATA[183 : 176]}},
    {^{AXI_WDATA[175 : 168]}},
    {^{AXI_WDATA[167 : 160]}},
    {^{AXI_WDATA[159 : 152]}},
    {^{AXI_WDATA[151 : 144]}},
    {^{AXI_WDATA[143 : 136]}},
    {^{AXI_WDATA[135 : 128]}},
    {^{AXI_WDATA[127 : 120]}},
    {^{AXI_WDATA[119 : 112]}},
    {^{AXI_WDATA[111 : 104]}},
    {^{AXI_WDATA[103 : 96] }},
    {^{AXI_WDATA[95 : 88]  }},
    {^{AXI_WDATA[87 : 80]  }},
    {^{AXI_WDATA[79 : 72]  }},
    {^{AXI_WDATA[71 : 64]  }},
    {^{AXI_WDATA[63 : 56]  }},
    {^{AXI_WDATA[55 : 48]  }},
    {^{AXI_WDATA[47 : 40]  }},
    {^{AXI_WDATA[39 : 32]  }},
    {^{AXI_WDATA[31 : 24]  }},
    {^{AXI_WDATA[23 : 16]  }},
    {^{AXI_WDATA[15 : 8]   }},
    {^{AXI_WDATA[7 : 0]    }} };
    reg [15:0] currentblock, wr_currentblock, wrdata_currentblock, currentblockplus16, wr_currentblockplus16;//, wrdata_currentblockplus1;
    
    reg [23:0] RD_ADDR, RD_ADDRplus512;
    reg [23:0] WR_ADDR, WR_ADDRplus512;
    always @ (*) begin
        RD_ADDRplus512 = RD_ADDR + 1'b1;//34'b1000000000;
    end
    
    always @ (*) begin
        WR_ADDRplus512 = WR_ADDR + 1'b1;//34'b1000000000;
    end
    
    reg [2:0] STAT = 0;
    
    assign busy = ~(STAT == 'b0);
    reg [23:0] write_req_counter, write_req_counterplus1;
    
    reg [23:0] bvalid_counter;
    reg [23:0] rvalid_counter;
    
    always @ (posedge clk) begin
        if (wr_running && STAT==0) begin
            bvalid_counter <= 0;
        end
        else if (AXI_BVALID) begin
            bvalid_counter <= bvalid_counter + 1;
        end
    end
    
    always @ (posedge clk) begin
        if (rd_running && STAT==0) begin
            rvalid_counter <= 0;
        end
        else if (AXI_RVALID & AXI_RREADY) begin
            rvalid_counter <= rvalid_counter + 1;
        end
    end
    
    always @ (*) begin
        write_req_counterplus1 = write_req_counter + 1;
    end
    
    always @ (*) begin
        currentblockplus16 = currentblock + 1;//24'b10000;//24'd16;
    end
    
    always @ (*) begin
        wr_currentblockplus16 = wr_currentblock + 1;//24'b10000;//24'd16;
    end
    
    reg [255:0] FIFO_WRITE_din_last;
    reg AXI_RVALID_last, FIFO_WRITE_prog_full_last;
    always @ (posedge clk) begin
        w_valid  <= AXI_WVALID;
        b_valid  <= AXI_BVALID;        
        ar_valid <= AXI_ARVALID;
        r_valid  <= AXI_RVALID & AXI_RLAST;
    
        FIFO_WRITE_din_last <= AXI_RDATA;
        FIFO_WRITE_din <= FIFO_WRITE_din_last;
        AXI_RVALID_last <= AXI_RVALID;
        FIFO_WRITE_prog_full_last <= FIFO_WRITE_prog_full;
        FIFO_WRITE_wr_en <= ~FIFO_WRITE_prog_full_last & AXI_RVALID_last;
        
       
        case (STAT)
            0: begin
                RD_ADDR <= rd_startaddress[31:9];//BASE_ADDRESS;
                currentblock <= 0;
                WR_ADDR <= wr_startaddress[31:9];//BASE_ADDRESS;
                wr_currentblock <= 0;
                write_req_counter <= 0;
                case ({rd_running, wr_running})
                    2'b01: STAT <= 2;
                    2'b10: STAT <= 1;
                    2'b11: STAT <= 0;
                    2'b00: STAT <= 0;
                endcase
                AXI_ARVALID <= 1'b0;
                AXI_AWVALID <= 0;
            end
            1: begin
                
                AXI_ARVALID <= 1'b1;
                AXI_ARSIZE <= 3'b101;
                AXI_ARBURST <= 2'b01;
                
                if (AXI_ARREADY) begin    
                    AXI_ARADDR <= {RD_ADDR, {9{1'b0}}}; //33'b0;//AXI_ARADDR + 6'b100000;
                    RD_ADDR <= RD_ADDRplus512;//RD_ADDR + {34'b1000000000};//2^4 * 2^5
                    currentblock <= currentblockplus16;//{currentblock + 32'd16}; 
                    if ({currentblockplus16, {4{1'b0}}} >= rd_blockamount) begin//RD_ADDR==HIGH_ADDRESS) begin
                        AXI_ARLEN <= rd_blockamount[3:0] - 1;//rd_blockamount - currentblock;//4'b1111;
                        STAT <= 5;
                    end
                    else begin
                        AXI_ARLEN <= 4'b1111;
                    end
                end
            end
            2: begin
                
                AXI_AWVALID <= 1'b1;
                AXI_AWSIZE <= 3'b101;
                AXI_AWBURST <= 2'b01;
                
                if (AXI_AWREADY) begin
                    write_req_counter <= write_req_counterplus1;//write_req_counter + 1;
                    AXI_AWADDR <= {WR_ADDR, {9{1'b0}}};//wr_startaddress;//RD_ADDR;//33'b0;//AXI_ARADDR + 6'b100000;
                    WR_ADDR <= WR_ADDRplus512;//{RD_ADDR + 34'b1000000000};//2^4 * 2^5
                    wr_currentblock <= wr_currentblockplus16;//{currentblock + 32'd16};
                    if ({wr_currentblockplus16, {4{1'b0}}} >= wr_blockamount) begin//RD_ADDR==HIGH_ADDRESS) begin
                        STAT <= 3;
                        AXI_AWLEN <= wr_blockamount[3:0] - 1;//wr_blockamount - currentblock;
                    end
                    else begin
                        AXI_AWLEN <= 4'b1111;
                    end
                    
                end
            end
        3: begin
             if (wrdata_currentblock == wr_blockamount && bvalid_counter == write_req_counter) begin
                STAT <= 'd0;
             end
             if (AXI_AWREADY) begin 
                AXI_AWVALID <= 1'b0;
            end
        end
        5: begin
            if (AXI_ARREADY) begin 
                AXI_ARVALID <= 0;
            end
            if (rvalid_counter == rd_blockamount) begin
                STAT <= 'd0;
            end
        end
        endcase       
    end
    
    reg [3:0] writecounter;
    reg rd_en_tmp;
    
    assign FIFO_READ_rd_en = rd_en_tmp | (AXI_WVALID & AXI_WREADY);
    
    always @ (posedge clk) begin
        if (~rd_en_tmp & ~AXI_WVALID & AXI_WREADY & ~FIFO_READ_empty) begin
            rd_en_tmp <= 1;
        end
        else begin
            rd_en_tmp <= 0;
        end
    end
    
    always @ (posedge clk) begin
        if (STAT == 0) begin
            AXI_WVALID <= 0;
        end
        else begin
            if (rd_en_tmp & ~AXI_WVALID & (~FIFO_READ_empty)) begin
                AXI_WVALID <= 1;
            end
            else if(AXI_WREADY & AXI_WVALID & FIFO_READ_empty) begin
                AXI_WVALID <= 0;
            end
        end
    end
    reg WLAST_STAT = 0;
    always @ (posedge clk) begin
    case (WLAST_STAT)
    0: begin
        if (STAT == 0) begin
            WLAST_STAT <= 1;
            AXI_WLAST <= 0;
            writecounter <= 4'd1;
            wrdata_currentblock <= 'd1;
        end
        else begin
            AXI_WLAST <= 0;
        end
    end
    1: begin
        if(AXI_WVALID & AXI_WREADY) begin
            if (wrdata_currentblock + 1 == wr_blockamount) begin
                AXI_WLAST <= 1'b1;
                WLAST_STAT <= 0;
            end
            else if (writecounter == 4'd15) begin
                AXI_WLAST <= 1'b1;
                writecounter <= 4'd0;
                wrdata_currentblock <= wrdata_currentblock + 24'd1;
            end
            else begin
                AXI_WLAST <= 1'b0;
                writecounter <= writecounter + 1;//{writecounter + 4'd1};
                wrdata_currentblock <= wrdata_currentblock + 24'd1;//wrdata_currentblock + 1;
            end
        end
    end
    endcase
    end

endmodule*/