`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/16/2022 02:20:59 PM
// Design Name: 
// Module Name: puasecontrol_module
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module puasecontrol_module # (
    parameter DATA_WIDTH = 512,
    parameter KEEP_WIDTH = 64,
    parameter PACKET_SIZE_MIN = 64,
    parameter PACKET_SIZE_MAX = 9600
) (

    input clk,
    
    input do_flow_control,
    
    output                          m_axis_tready,
    input      [DATA_WIDTH - 1 : 0] m_axis_tdata,
    input                           m_axis_tvalid,
    input      [KEEP_WIDTH - 1 : 0] m_axis_tkeep,
    input      [0 : 0]              m_axis_tuser,
    input                           m_axis_tlast,
    
    input                           s_axis_tready,
    output     [DATA_WIDTH - 1 : 0] s_axis_tdata,
    output                          s_axis_tvalid,
    output     [KEEP_WIDTH - 1 : 0] s_axis_tkeep,
    output     [0 : 0]              s_axis_tuser,
    output                          s_axis_tlast,    
    
    //output reg [15:0] packet_size_counter = 0,
    output reg flow_control_FSM = 0,
    output reg flow_control_FSM_next = 0
    );
    
    // packet size must between 64 and 9600
    //reg [15:0] packet_size_counter = 0;
    
    /*always @ (posedge clk) begin
        if (s_axis_tvalid & s_axis_tready) begin
            if (s_axis_tlast) begin
                packet_size_counter <= 0;
            end
            else begin
                packet_size_counter <= packet_size_counter + 1;
            end
        end
    end*/
    
    
    // ensure packet size legal
    //reg flow_control_FSM = 0, flow_control_FSM_next = 0;
    
    always @ (posedge clk) begin
        flow_control_FSM <= flow_control_FSM_next;
    end
    
    always @ (*) begin
        case (flow_control_FSM)
        0: begin
            if (do_flow_control) begin
                flow_control_FSM_next <= 1;
            end
            else begin
                flow_control_FSM_next <= 0;
            end
        end
        1: begin
            if (~do_flow_control) begin
                flow_control_FSM_next <= 0;
            end
            else begin
                flow_control_FSM_next <= 1;
            end
        end
        endcase
    end
    
    //reg force_last = 0;
    reg real_do_flow_control_pause = 1;/*, real_do_flow_control_reg = 1;
    always @ (posedge clk) begin
        real_do_flow_control_reg <= real_do_flow_control;
    end*/
    
    always @ (posedge clk) begin
        case (flow_control_FSM_next)
        0: begin
            real_do_flow_control_pause <= 1;
        end
        1: begin
            if (s_axis_tready & m_axis_tvalid & m_axis_tlast) begin
                real_do_flow_control_pause <= 0;
            end
        end
        endcase
    end
    
    
    assign m_axis_tready = s_axis_tready & real_do_flow_control_pause;
    assign s_axis_tdata  = m_axis_tdata;
    assign s_axis_tvalid = m_axis_tvalid & real_do_flow_control_pause;
    assign s_axis_tkeep  = m_axis_tkeep ;
    assign s_axis_tuser  = m_axis_tuser ;
    assign s_axis_tlast  = m_axis_tlast;
    
endmodule
