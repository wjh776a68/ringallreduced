`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/15/2022 05:13:01 PM
// Design Name: 
// Module Name: rx_flow_controller
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module rx_flow_controller #(
    parameter PAUSE_TIME = 500

) (
    input               clk,
    input               flow_control_on,
    input  [8 : 0]       flow_control_type,
    output reg do_flow_control = 0,
    
    output reg ctl_rx_enable_pcp      ,
    output reg ctl_rx_check_etype_ppp ,
    output reg ctl_rx_check_etype_pcp ,
    output reg ctl_rx_check_mcast_gpp ,
    output reg ctl_rx_check_mcast_gcp ,
    output reg ctl_rx_check_etype_gpp ,
    output reg ctl_rx_check_ucast_ppp ,
    output reg ctl_rx_check_ucast_pcp ,
    output reg ctl_rx_check_etype_gcp ,
    output reg ctl_rx_check_sa_ppp    ,
    output reg ctl_rx_check_sa_pcp    ,
    output reg ctl_rx_enable_gpp      ,
    output reg ctl_rx_enable_gcp      ,
    output reg ctl_rx_check_ucast_gpp ,
    output reg ctl_rx_check_ucast_gcp ,
    output reg ctl_rx_check_opcode_ppp,
    output reg ctl_rx_check_opcode_pcp,
    output reg ctl_rx_check_mcast_ppp ,
    output reg ctl_rx_check_mcast_pcp ,
    output reg ctl_rx_check_sa_gpp    ,
    output reg ctl_rx_check_sa_gcp    ,
    output reg ctl_rx_check_opcode_gpp,
    output reg ctl_rx_check_opcode_gcp,
    output reg ctl_rx_enable_ppp      ,
    output reg [8:0] ctl_rx_pause_ack ,
    output reg [8:0] ctl_rx_pause_enable,
    
    output reg pause_FSM = 0, 
    output reg pause_FSM_next = 0
    
    );
    
    
    
    always @ (posedge clk) begin
        ctl_rx_enable_pcp       <= 1;
        ctl_rx_check_etype_ppp  <= 1;
        ctl_rx_check_etype_pcp  <= 1;
        ctl_rx_check_mcast_gpp  <= 1;
        ctl_rx_check_mcast_gcp  <= 1;
        ctl_rx_check_etype_gpp  <= 1;
        ctl_rx_check_ucast_ppp  <= 1;
        ctl_rx_check_ucast_pcp  <= 1;
        ctl_rx_check_etype_gcp  <= 1;
        ctl_rx_check_sa_ppp     <= 1;
        ctl_rx_check_sa_pcp     <= 1;
        ctl_rx_enable_gpp       <= 1;
        ctl_rx_enable_gcp       <= 1;
        ctl_rx_check_ucast_gpp  <= 1;
        ctl_rx_check_ucast_gcp  <= 1;
        ctl_rx_check_opcode_ppp <= 1;
        ctl_rx_check_opcode_pcp <= 1;
        ctl_rx_check_mcast_ppp  <= 1;
        ctl_rx_check_mcast_pcp  <= 1;
        ctl_rx_check_sa_gpp     <= 1;
        ctl_rx_check_sa_gcp     <= 1;
        ctl_rx_check_opcode_gpp <= 1;
        ctl_rx_check_opcode_gcp <= 1;
        ctl_rx_enable_ppp       <= 1;
        
        
    end
    reg [10:0] pause_hold_times_counter = 0;
    
    
    always @ (posedge clk) begin
        pause_FSM <= pause_FSM_next;
    end
    
    always @ (*) begin
        case (pause_FSM)
        0: begin
            if (flow_control_on == 1 && flow_control_type[8] == 1) begin
                pause_FSM_next <= 1;
            end
            else begin
                pause_FSM_next <= 0;
            end
        end
        1: begin
            if (flow_control_on || pause_hold_times_counter < PAUSE_TIME) begin
                pause_FSM_next <= 1;
            end
            else begin
                pause_FSM_next <= 0;
            end
        end
        endcase
    end
    
    always @ (posedge clk) begin
        case (pause_FSM_next) 
        0: begin
            pause_hold_times_counter <= 0;
            ctl_rx_pause_ack  <= 0; 
            ctl_rx_pause_enable <= 0;
            do_flow_control <= 0;
        end
        1: begin
            if (flow_control_on || pause_hold_times_counter == PAUSE_TIME) begin
                pause_hold_times_counter <= 0;
            end
            else begin
                pause_hold_times_counter <= pause_hold_times_counter + 1;
            end
            do_flow_control <= 1;
            ctl_rx_pause_ack[8]    <= 1; 
            ctl_rx_pause_enable[8] <= 1;
        end
        endcase
    end
    
endmodule
