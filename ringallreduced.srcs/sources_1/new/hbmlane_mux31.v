`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/20/2022 02:43:01 PM
// Design Name: 
// Module Name: hbmlane_mux31
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module hbmlane_mux31(
    input clk,
    input [1 : 0] mux_select,
    
    output reg      m_HBM_READ_tvalid  ,
    output reg[255:0]m_HBM_READ_tdata,
    input        m_HBM_READ_tready  ,
    output reg      m_HBM_READ_tlast  ,
    output reg[31:0] m_HBM_READ_tkeep,
    
    output  reg     m_HBM_WRITE_tready   ,
    input[255:0]  m_HBM_WRITE_tdata,
    input        m_HBM_WRITE_tvalid  ,
    
    
    (* X_INTERFACE_MODE = "slave" *)
    (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports_rtl:1.0 m_ctrl_ports rd_blockamount"  *)input [31:0] m_rd_blockamount,
    (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports_rtl:1.0 m_ctrl_ports rd_run"          *)input        m_rd_run,
    (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports_rtl:1.0 m_ctrl_ports rd_startaddress" *)input [31:0] m_rd_startaddress,
    (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports_rtl:1.0 m_ctrl_ports wr_blockamount"  *)input [31:0] m_wr_blockamount,
    (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports_rtl:1.0 m_ctrl_ports wr_run"          *)input        m_wr_run,
    (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports_rtl:1.0 m_ctrl_ports wr_startaddress" *)input [31:0] m_wr_startaddress,
    (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports_rtl:1.0 m_ctrl_ports busy" *)           output reg m_busy,
    
    input       s1_HBM_READ_tvalid  ,
    input [255:0]s1_HBM_READ_tdata,
    output reg     s1_HBM_READ_tready  ,
    input      s1_HBM_READ_tlast  ,
    input[31:0] s1_HBM_READ_tkeep,
   
    input       s1_HBM_WRITE_tready   ,
    output reg [255:0]s1_HBM_WRITE_tdata,
    output reg     s1_HBM_WRITE_tvalid  ,
    
    (* X_INTERFACE_MODE = "master" *)
    (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports_rtl:1.0 s1_ctrl_ports rd_blockamount"  *)output reg [31:0] s1_rd_blockamount,
    (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports_rtl:1.0 s1_ctrl_ports rd_run"          *)output reg        s1_rd_run,
    (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports_rtl:1.0 s1_ctrl_ports rd_startaddress" *)output reg [31:0] s1_rd_startaddress,
    (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports_rtl:1.0 s1_ctrl_ports wr_blockamount"  *)output reg [31:0] s1_wr_blockamount,
    (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports_rtl:1.0 s1_ctrl_ports wr_run"          *)output reg        s1_wr_run,
    (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports_rtl:1.0 s1_ctrl_ports wr_startaddress" *)output reg [31:0] s1_wr_startaddress,
    (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports_rtl:1.0 s1_ctrl_ports busy" *)           input s1_busy,
    
    input       s2_HBM_READ_tvalid,
    input [255:0]s2_HBM_READ_tdata,
    output  reg  s2_HBM_READ_tready  ,
    input      s2_HBM_READ_tlast  ,
    input[31:0] s2_HBM_READ_tkeep,
   
    input       s2_HBM_WRITE_tready   ,
    output reg[255:0]s2_HBM_WRITE_tdata,
    output reg     s2_HBM_WRITE_tvalid ,
    
    (* X_INTERFACE_MODE = "master" *)
    (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports_rtl:1.0 s2_ctrl_ports rd_blockamount"  *)output reg [31:0] s2_rd_blockamount,
    (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports_rtl:1.0 s2_ctrl_ports rd_run"          *)output reg        s2_rd_run,
    (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports_rtl:1.0 s2_ctrl_ports rd_startaddress" *)output reg [31:0] s2_rd_startaddress,
    (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports_rtl:1.0 s2_ctrl_ports wr_blockamount"  *)output reg [31:0] s2_wr_blockamount,
    (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports_rtl:1.0 s2_ctrl_ports wr_run"          *)output reg        s2_wr_run,
    (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports_rtl:1.0 s2_ctrl_ports wr_startaddress" *)output reg [31:0] s2_wr_startaddress,
    (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports_rtl:1.0 s2_ctrl_ports busy" *)           input s2_busy,
    
    input       s3_HBM_READ_tvalid ,
    input [255:0]s3_HBM_READ_tdata,
    output reg     s3_HBM_READ_tready ,
    input      s3_HBM_READ_tlast  ,
    input[31:0] s3_HBM_READ_tkeep,
   
    input       s3_HBM_WRITE_tready,
    output reg[255:0]s3_HBM_WRITE_tdata,
    output reg     s3_HBM_WRITE_tvalid ,
    
    (* X_INTERFACE_MODE = "master" *)
    (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports_rtl:1.0 s3_ctrl_ports rd_blockamount"  *)output reg [31:0] s3_rd_blockamount,
    (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports_rtl:1.0 s3_ctrl_ports rd_run"          *)output reg        s3_rd_run,
    (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports_rtl:1.0 s3_ctrl_ports rd_startaddress" *)output reg [31:0] s3_rd_startaddress,
    (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports_rtl:1.0 s3_ctrl_ports wr_blockamount"  *)output reg [31:0] s3_wr_blockamount,
    (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports_rtl:1.0 s3_ctrl_ports wr_run"          *)output reg        s3_wr_run,
    (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports_rtl:1.0 s3_ctrl_ports wr_startaddress" *)output reg [31:0] s3_wr_startaddress,
    (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports_rtl:1.0 s3_ctrl_ports busy" *)           input s3_busy
    );
    
    always @ (*) begin
        case (mux_select)
        1: begin
            s1_rd_blockamount  <= m_rd_blockamount ;
            s1_rd_run          <= m_rd_run         ;
            s1_rd_startaddress <= m_rd_startaddress;
            s1_wr_blockamount  <= m_wr_blockamount ;
            s1_wr_run          <= m_wr_run         ;
            s1_wr_startaddress <= m_wr_startaddress;
            
            m_HBM_READ_tvalid    <= s1_HBM_READ_tvalid  ;
            m_HBM_READ_tdata     <= s1_HBM_READ_tdata;
            s1_HBM_READ_tready   <= m_HBM_READ_tready   ;
            m_HBM_READ_tkeep     <= s1_HBM_READ_tkeep  ;
            m_HBM_READ_tlast     <= s1_HBM_READ_tlast;
                                                      
            m_HBM_WRITE_tready   <= s1_HBM_WRITE_tready;
            s1_HBM_WRITE_tdata   <= m_HBM_WRITE_tdata;
            s1_HBM_WRITE_tvalid  <= m_HBM_WRITE_tvalid;
            m_busy <= s1_busy;
        end
        2: begin
            s2_rd_blockamount  <= m_rd_blockamount ;
            s2_rd_run          <= m_rd_run         ;
            s2_rd_startaddress <= m_rd_startaddress;
            s2_wr_blockamount  <= m_wr_blockamount ;
            s2_wr_run          <= m_wr_run         ;
            s2_wr_startaddress <= m_wr_startaddress;
            
            m_HBM_READ_tvalid    <= s2_HBM_READ_tvalid  ;
            m_HBM_READ_tdata     <= s2_HBM_READ_tdata;
            s2_HBM_READ_tready   <= m_HBM_READ_tready   ;
            m_HBM_READ_tkeep     <= s2_HBM_READ_tkeep  ;
            m_HBM_READ_tlast     <= s2_HBM_READ_tlast;
                                                      
            m_HBM_WRITE_tready   <= s2_HBM_WRITE_tready;
            s2_HBM_WRITE_tdata   <= m_HBM_WRITE_tdata;
            s2_HBM_WRITE_tvalid  <= m_HBM_WRITE_tvalid;
            m_busy <= s2_busy;
        end
        3: begin
            s3_rd_blockamount  <= m_rd_blockamount ;
            s3_rd_run          <= m_rd_run         ;
            s3_rd_startaddress <= m_rd_startaddress;
            s3_wr_blockamount  <= m_wr_blockamount ;
            s3_wr_run          <= m_wr_run         ;
            s3_wr_startaddress <= m_wr_startaddress;
            
            m_HBM_READ_tvalid    <= s3_HBM_READ_tvalid  ;
            m_HBM_READ_tdata     <= s3_HBM_READ_tdata;
            s3_HBM_READ_tready   <= m_HBM_READ_tready   ;
            m_HBM_READ_tkeep     <= s3_HBM_READ_tkeep  ;
            m_HBM_READ_tlast     <= s3_HBM_READ_tlast;
                                                      
            m_HBM_WRITE_tready   <= s3_HBM_WRITE_tready;
            s3_HBM_WRITE_tdata   <= m_HBM_WRITE_tdata;
            s3_HBM_WRITE_tvalid  <= m_HBM_WRITE_tvalid;
            m_busy <= s3_busy;
        
        end
        0: begin
            s1_wr_run <= 0;
            s1_rd_run <= 0;
            s2_wr_run <= 0;
            s2_rd_run <= 0;
            s3_wr_run <= 0;
            s3_rd_run <= 0;
            
            m_HBM_WRITE_tready <= 0;
            m_HBM_READ_tvalid <= 0;
            s3_HBM_READ_tready <= 0;
            s3_HBM_WRITE_tvalid <= 0;
            s2_HBM_READ_tready <= 0;
            s2_HBM_WRITE_tvalid <= 0;
            s1_HBM_READ_tready <= 0;
            s1_HBM_WRITE_tvalid <= 0;

            m_busy <= 0;
        end
        endcase
    end
    
    
endmodule
