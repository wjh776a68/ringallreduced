module ringallreduce_controller(
    input clk,

    input user_start_io,
    input hw_link_status,

    output reg stat_system_on = 0,
    output reg stat_finish = 0,

    input init_done,
    output reg init_start,
    input recv_done,
    input send_done,
    output reg recv_start,
    output reg send_start,

    output reg transfer_type_switch = 0 //0: init, 1: ringallreduce

);

    reg [2:0] controller_FSM = 0, controller_FSM_next = 0;

    always @ (posedge clk) begin
        controller_FSM <= controller_FSM_next;
    end

    always @ (*) begin
        case (controller_FSM)
        0: begin
            if (hw_link_status && user_start_io) begin
                controller_FSM_next = 1;
            end 
            else begin
                controller_FSM_next = 0;
            end
        end
        1: begin
            if (init_done) begin
                controller_FSM_next = 2;
            end 
            else begin
                controller_FSM_next = 1;
            end
        end
        2: begin
            if (recv_done && send_done) begin
                controller_FSM_next = 3;
            end 
            else begin
                controller_FSM_next = 2;
            end
        end
        3: begin

        end
        endcase
    end

    always @ (posedge clk) begin
        case (controller_FSM_next)
        0: begin
            stat_system_on <= 0;
            stat_finish <= 0;
            init_start <= 0;
            recv_start <= 0;
            send_start <= 0;
            transfer_type_switch <= 0;
        end
        1: begin
            transfer_type_switch <= 0;
            init_start <= 1;
        end
        2: begin
            init_start <= 0;
            transfer_type_switch <= 1;
            recv_start <= 1;
            send_start <= 1;
        end
        3: begin
            stat_finish <= 1;
        end
        endcase
    end

endmodule