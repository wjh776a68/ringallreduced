`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/14/2022 11:31:15 AM
// Design Name: 
// Module Name: send
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module send(
    input clk,
    input reset,
    
    output reg ctl_tx_custom_preamble_enable_0,
    output reg ctl_tx_enable_0,
    output reg ctl_tx_send_lfi_0,
    output reg ctl_tx_send_rfi_0,
    output reg ctl_tx_fcs_ins_enable_0,
    output reg ctl_tx_ignore_fcs_0,
    output reg [3:0] ctl_tx_ipg_value_0,
    output reg ctl_tx_send_idle_0,
    output reg ctl_tx_test_pattern_0
);

    always @ (posedge reset) begin
       
        ctl_tx_custom_preamble_enable_0 <= 0;
        ctl_tx_enable_0 <= 1;   // asserted after receiver is ready          
        ctl_tx_send_lfi_0 <= 0; // Transmit Remote Fault Indication (RFI) code word.            
        ctl_tx_send_rfi_0 <= 0; // Transmit Local Fault Indication (LFI) code word             
        ctl_tx_fcs_ins_enable_0 <= 1; // Enable FCS insertion by the TX core.        
        ctl_tx_ignore_fcs_0 <= 0; // Enable FCS error checking at the AXI4-Stream interfaceby the TX core           
        ctl_tx_ipg_value_0 <= 4'd12;       
        ctl_tx_send_idle_0 <= 0; //Transmit Idle code words.             
        ctl_tx_test_pattern_0 <= 1;          
        
    end

endmodule
