module senddata(
    input clk,

   // input [15 : 0] ring_device_order,
   // input [15 : 0] ring_device_amount,
    input [15 : 0] send_bytes_amount,
    input send_start,
    output reg send_done = 0,

    input      [63 : 0]  hbm_rddata,
    output reg           hbm_rd_en = 0,
    input                hbm_empty,

    output reg [63 : 0]  s_axis_tdata,
    output reg [ 7 : 0]  s_axis_tkeep,
    output reg           s_axis_tvalid = 0,
    output reg           s_axis_tlast,
    input                s_axis_tready

);

    always @ (posedge clk) begin
        if (~hbm_empty) begin
            s_axis_tvalid <= 1;
            s_axis_tkeep <= 8'b1111_1111;
            if (s_axis_tready) begin
                hbm_rd_en <= 1;
            end
            else begin
                hbm_rd_en <= 0;
            end

            if (hbm_rd_en) begin
                s_axis_tdata <= hbm_rddata;
            end

        end
        else if (hbm_empty & hbm_rd_en) begin
            s_axis_tlast <= 1;
            s_axis_tvalid <= 1;
            s_axis_tkeep <= 8'b1111_1111;
            s_axis_tdata <= hbm_rddata;
            hbm_rd_en <= 0;
        end
        else begin
            hbm_rd_en <= 0;
            s_axis_tvalid <= 0;
        end
    end

endmodule
