`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/17/2022 11:24:37 PM
// Design Name: 
// Module Name: bramtoaxis
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module axistobram#(
    parameter STATUS_0 = 13'b0000000000001,
    parameter STATUS_1 = 13'b0000000000010,
    parameter STATUS_2 = 13'b0000000000100,
    parameter STATUS_3 = 13'b0000000001000,
    parameter STATUS_4 = 13'b0000000010000,
    parameter STATUS_5 = 13'b0000000100000,
    parameter STATUS_6 =  13'b0000001000000,
    parameter STATUS_7 =  13'b0000010000000,
    parameter STATUS_8 =  13'b0000100000000,
    parameter STATUS_9 =  13'b0001000000000,
    parameter STATUS_10 = 13'b0010000000000,
    parameter STATUS_11 = 13'b0100000000000,
    parameter STATUS_12 = 13'b1000000000000
)(
    input [63:0] axis_i_tdata,
    output axis_i_tready,
    input axis_i_tlast,
    input axis_i_tvalid,
    input [7:0] axis_i_tkeep,
    
    input clk,
    input reset,

    output reg wr_en,
    output reg [63:0] din,
    input [63:0] dout,
    
    output reg [12:0] RUN,
    //output reg [12:0] next_RUN,
    
    
    output reg statusdata_wea,
    output reg [3:0] statusdata,
    input [3:0] statusdata_in,
    
    output reg startring,
    input ringfinished,
    
    output reg [31:0] perblocksize,
    output reg [31:0] curnode,
    output reg [31:0] totalnode,
    
    output reg [63:0] axis_o_tdata,
    input  axis_o_tready,
    output reg axis_o_tlast,
    output reg axis_o_tvalid,
    output reg [7:0] axis_o_tkeep,
    
    output reg [31:0] curaddr,
    output reg [31:0] maxaddr
    );
    
    
    
    assign axis_i_tready = ~reset;
    
    /*always@(posedge clk) begin
        if(reset) begin
            RUN <= STATUS_0;
        end
        else begin
            RUN <= next_RUN;
        end
    end*/
    
    always@(posedge clk) begin
        if(reset) begin
            RUN <= STATUS_0;
        end
        else begin
            case(RUN) 
            STATUS_0: begin
                statusdata_wea <= 1'b0;
                wr_en <= 1'b0;
                startring <= 1'b0;
                axis_o_tvalid <= 1'b0;
                axis_o_tkeep <= 8'b0;
                axis_o_tlast <= 1'b0;
                axis_o_tdata <= 64'b0;
                statusdata <= 4'b0;
                din <= 64'b0;
                    
                if(statusdata_in[0] == 1'b0 && axis_i_tvalid==1) begin
                    curnode <= axis_i_tdata[31:0];
                    totalnode <= axis_i_tdata[63:32];
                    RUN <= STATUS_1;
                end
                else begin
                    RUN <= STATUS_0;
                    
                end
            end
            STATUS_1: begin
                if(axis_i_tvalid==1) begin
                    maxaddr <= axis_i_tdata[63:32];
                    perblocksize <= axis_i_tdata[31:0];
                    curaddr <= 32'b0 - 32'b1;
                    RUN <= STATUS_2;
                end
                else begin
                    RUN <= STATUS_1;
                end
            end
            STATUS_2: begin
                if(axis_i_tvalid==1) begin
                    curaddr <= curaddr + 32'b1;
                    wr_en <= 1'b1;
                    din <= axis_i_tdata;
                    RUN <= STATUS_2;
                end
                else if(curaddr + 1 == maxaddr) begin
                    wr_en <= 1'b0;
                    startring <= 1'b1;
                    RUN <= STATUS_3;
                end
                else begin
                    wr_en <= 1'b0;
                    RUN <= STATUS_2;
                end
            end
            STATUS_3: begin
                startring <= 0;
                if(ringfinished==1) begin
                    statusdata_wea <= 1'b1;
                    statusdata <= {statusdata[3:1], 1'b1};
                    RUN <= STATUS_4;
                end
                else 
                    RUN <= STATUS_3;
            end
            STATUS_4: begin
                statusdata_wea <= 1'b0;
                curaddr <= 32'b0;
                RUN <= STATUS_5;
            end
            STATUS_5: begin
                if(axis_o_tready==1) begin
                    
                    axis_o_tdata <= dout;
                    axis_o_tvalid <= 1;
                    axis_o_tkeep <= 8'hff;
                    
                    if(curaddr+1 == maxaddr) begin
                        axis_o_tlast <= 1'b1;
                        RUN <= STATUS_0;
                    end
                    else begin
                        axis_o_tlast <= 1'b0;
                        curaddr <= curaddr + 1;
                        RUN <= STATUS_5;
                    end                  
                end
                else begin
                    axis_o_tvalid <= 0;
                    RUN <= STATUS_5;
                end
            end
            endcase
        end
        
    end
    
    
endmodule
