`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/21/2022 05:05:06 PM
// Design Name: 
// Module Name: inputmux12
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module inputmux12(
    input clk,
    
    input  [63:0] axis_i_tdata,
    output  axis_i_tready,
    input  axis_i_tlast,
    input  axis_i_tvalid,
    input  [7:0] axis_i_tkeep,
    
    output [63:0] axis_A_tdata,
    input   axis_A_tready,
    output axis_A_tlast,
    output axis_A_tvalid,
    output [7:0] axis_A_tkeep,
    
    output [63:0] axis_B_tdata,
    input   axis_B_tready,
    output axis_B_tlast,
    output axis_B_tvalid,
    output [7:0] axis_B_tkeep,
    
    input isB //0 is A 1 is B
    );
    
    assign axis_i_tready = (isB==0) ? axis_A_tready : axis_B_tready;
    assign axis_A_tdata  = (isB==0) ? axis_i_tdata : 0;
    assign axis_B_tdata  = (isB==1) ? axis_i_tdata : 0;
    assign axis_A_tlast  = (isB==0) ? axis_i_tlast : 0;
    assign axis_B_tlast  = (isB==1) ? axis_i_tlast : 0;
    assign axis_A_tvalid = (isB==0) ? axis_i_tvalid : 0;
    assign axis_B_tvalid = (isB==1) ? axis_i_tvalid : 0;
    assign axis_A_tkeep  = (isB==0) ? axis_i_tkeep : 0;
    assign axis_B_tkeep  = (isB==1) ? axis_i_tkeep : 0;
endmodule
