`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/26/2022 10:47:10 PM
// Design Name: 
// Module Name: packeddata
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module recdata#(
    parameter STATUS_0 =  14'b00000000000001,
    parameter STATUS_1 =  14'b00000000000010,
    parameter STATUS_2 =  14'b00000000000100,
    parameter STATUS_3 =  14'b00000000001000,
    parameter STATUS_4 =  14'b00000000010000,
    parameter STATUS_5 =  14'b00000000100000,
    parameter STATUS_6 =  14'b00000001000000,
    parameter STATUS_7 =  14'b00000010000000,
    parameter STATUS_8 =  14'b00000100000000,
    parameter STATUS_9 =  14'b00001000000000,
    parameter STATUS_10 = 14'b00010000000000,
    parameter STATUS_11 = 14'b00100000000000,
    parameter STATUS_12 = 14'b01000000000000,
    parameter STATUS_13 = 14'b10000000000000
)(
    input [63:0] din,
    input valid,
    input empty,
    //input valid,
    output reg rd_en,
    /*input [63:0] recv_tdata, //axis
    input [7:0] recv_tkeep,
    input recv_tlast,
    output recv_tready,
    input recv_tvalid,
    input recv_tuser,*/
    
    input clk,
    input reset,

    output fifo_resetn,
    output reg [63:0] eth_tdata, //axis
    output reg [7:0] eth_tkeep,
    output reg eth_tlast,
    input eth_tready,
    output reg eth_tvalid,
    
    //output wire [31:0] statusaddr,
    //output reg [3:0] statuswea,
    //output reg [31:0] statusdata_out,
    //input [31:0] statusdata_in,
    output reg [3:0] eth_statusdata_out,
    output reg eth_statuswea,
    input [3:0] eth_statusdata_in,
    
//    input programfull,
    
 //   input [31:0] currentbuffsize, //inputbuffersize�ĸ�����
//    output reg [3:0] index = 0,
    output reg [13:0] STATUS = STATUS_0,
    
    output reg [63:0] packet_length = 0,
    output reg [63:0] cur_length = 0
   // output reg enable_receive = 0,
    //input enable_receive_ack,
    //input eth_finish
    );
    
    /*reg hold = 0;
    
    assign eth_tdata  = recv_tdata  ;
    assign eth_tkeep  = recv_tkeep  ;
    assign eth_tlast  = recv_tlast  ;    
    assign recv_tready = (hold==1 && recv_tlast==0)?eth_tready : 0;     
    assign eth_tvalid = recv_tvalid ;     */
    assign fifo_resetn = ~reset && ~eth_statusdata_in[3];
        
    always@(posedge clk) begin
        if(reset || eth_statusdata_in[3]==1) begin
            rd_en <= 0;
            eth_tdata <= 64'b0;
            eth_tkeep <= 0;
            eth_tlast <= 0;
            eth_tvalid <= 0;
            STATUS <= STATUS_0;//4'b0;
            eth_statuswea <= 0;//4'b0;
            eth_statusdata_out <= 0;//32'b0;
            //enable_receive <= 0;
        end
        else begin
            case(STATUS)
            STATUS_0: begin
                eth_tdata <= 64'b0;
                eth_tkeep <= 0;
                eth_tlast <= 0;
                eth_tvalid <= 0;
                packet_length <= 0;
                cur_length <= 0;
                if(eth_statusdata_in[0]==0 && empty==0) begin
                    eth_statuswea <= 1;
                    eth_statusdata_out <= {eth_statusdata_in[3:1], 1'b1};
                    STATUS <= STATUS_1;
                end
                else begin
                    STATUS <= STATUS;
                end
            end
            STATUS_1: begin
                eth_statuswea <= 0;
                eth_statusdata_out <= 0;
                if(empty==0) begin
                    rd_en <= 1;
                    STATUS <= STATUS_2;
                end
                else begin
                    rd_en <= 0;
                    STATUS <= STATUS;
                end
            end
            STATUS_2: begin
                rd_en <= 0;
                STATUS <= STATUS_3;
                eth_statuswea <= 0;
                eth_statusdata_out <= 0;
            end
            STATUS_3: begin
                eth_statuswea <= 0;
                eth_statusdata_out <= 0;
                if(valid==0) begin
                    rd_en <= 0;
                    STATUS <= STATUS_1;
//                    if(empty==0) begin
//                        rd_en <= 1;
//                        STATUS <= STATUS;
//                    end
//                    else begin
//                        rd_en <= 0;
//                        STATUS <= STATUS;
//                    end
                end
                else begin //valid==1
                    rd_en <= 0;
                    STATUS <= STATUS_4;
                    cur_length <= 0;//0;//cur_length + 8;
                    packet_length <= din;
                     
//                    if(empty==0) begin
//                        rd_en <= 1;
//                        STATUS <= 4;
//                        cur_length <= 0;//0;//cur_length + 8;
//                        packet_length <= din;
//                    end
//                    else if(empty==1 && valid==1) begin
//                        rd_en <= 0;
//                        STATUS <= 4;
//                        packet_length <= packet_length;
//                        cur_length <= cur_length;
//                    end
//                    else begin
//                        rd_en <= 0;
//                        STATUS <= STATUS;
//                        //packet_length <= packet_length;
//                        //cur_length <= cur_length;
//                    end
                end
            end
            STATUS_4: begin
                if(eth_tready==1) begin
                    eth_tvalid <= 1;
                    eth_tkeep <= 8'hff;
                    eth_tdata <= din;
                    eth_tlast <= 0;
                    STATUS <= STATUS_5;
                end
                else begin
                    eth_tvalid <= 0;
                    STATUS <= STATUS;
                end
            end
            STATUS_5: begin
                eth_tvalid <= 0;
                if(empty==0 && eth_tready==1) begin
                    rd_en <= 1;
                    STATUS <= STATUS_6;
                end
                else begin
                    rd_en <= 0;
                    STATUS <= STATUS;
                end
            end
            STATUS_6: begin
                eth_tvalid <= 0;
                rd_en <= 0;
                STATUS <= STATUS_7;
            end
            STATUS_7: begin
                if(valid==1) begin
                    if(eth_tready==1) begin
                        cur_length <= cur_length + 8;
                        eth_tvalid <= 1;
                        eth_tkeep <= 8'hff;
                        eth_tdata <= din;
                        if(cur_length + 8 >= packet_length) begin
                            eth_tlast <= 1; 
                            STATUS <= STATUS_0;
                        end
                        else begin
                            eth_tlast <= 0; 
                            STATUS <= STATUS_5;
                        end
                    end
                    else begin
                        eth_tvalid <= 0;
                        eth_tkeep <= 8'hff;
                        eth_tdata <= din;
                        eth_tlast <= 0;
                        STATUS <= STATUS_8;
                    end
                end
                else begin
                    eth_tvalid <= 0;
                    eth_tkeep <= 8'hff;
                    eth_tdata <= din;
                    eth_tlast <= 0;
                    STATUS <= STATUS_5;
                end
            end
            STATUS_8: begin
                if(eth_tready==1) begin
                    cur_length <= cur_length + 8;
                    eth_tvalid <= 1;
                    eth_tkeep <= 8'hff;
                    eth_tdata <= din;
                    if(cur_length + 8 >= packet_length) begin
                        eth_tlast <= 1; 
                        STATUS <= STATUS_0;
                    end
                    else begin
                        eth_tlast <= 0; 
                        STATUS <= STATUS_5;
                    end
                    
                end
                else begin
                    eth_tvalid <= 0;
                    eth_tkeep <= 8'hff;
                    eth_tdata <= din;
                    eth_tlast <= 0; 
                    STATUS <= STATUS;
                end
            end
                /*if(cur_length  + 8 >= packet_length) begin
                    cur_length <= cur_length;
                    rd_en <= 0;
                    if(eth_tready==1) begin
                        STATUS <= 0;
                        eth_tvalid <= 1;
                        eth_tkeep <= 8'hff;
                        eth_tdata <= din;
                        eth_tlast <= 1; 
                    end
                    else begin
                        STATUS <= STATUS;
                        eth_tvalid <= 0;
                        eth_tkeep <= 8'hff;
                        eth_tdata <= din;
                        eth_tlast <= 0; 
                    end*/
                    /*if(valid == 1) begin
                        rd_en <= 0;
                        if(eth_tready==1) begin
                            STATUS <= 0;
                            eth_tvalid <= 1;
                            eth_tkeep <= 8'hff;
                            eth_tdata <= din;
                            eth_tlast <= 1; 
                        end
                        else begin
                            STATUS <= STATUS;
                            eth_tvalid <= 0;
                            eth_tkeep <= 8'hff;
                            eth_tdata <= din;
                            eth_tlast <= 0; 
                        end
                    end
                    else begin
                        rd_en <= 1;
                        STATUS <= STATUS;
                        eth_tvalid <= 0;
                        eth_tkeep <= 8'hff;
                        eth_tdata <= din;
                        eth_tlast <= 0; 
                    end*/
                    
                //end
                /*else begin
                    if(eth_tready==1) begin
                        //if(valid==1) begin
                            if(empty==0 && valid == 1) begin
                                cur_length <= cur_length + 8;
                                if(cur_length  + 16 >= packet_length) begin
                                    rd_en <= 0;
                                end
                                else begin
                                    rd_en <= 1;
                                end
                                STATUS <= STATUS;
                                eth_tvalid <= 1;
                                eth_tkeep <= 8'hff;
                                eth_tdata <= din;
                                eth_tlast <= 0; 
                            end
                            else if(empty==0 && valid == 0) begin
                                cur_length <= cur_length;// + 8;
                                rd_en <= 1;
                                STATUS <= STATUS;
                                eth_tvalid <= 0;
                                eth_tkeep <= 8'hff;
                                eth_tdata <= din;
                                eth_tlast <= 0; 
                            end
                            else if(valid==1) begin
                                cur_length <= cur_length + 8;
                                rd_en <= 0;
                                STATUS <= STATUS;
                                eth_tvalid <= 1;
                                eth_tkeep <= 8'hff;
                                eth_tdata <= din;
                                eth_tlast <= 0; 
                            end
                            else begin
                                rd_en <= 0;
                                cur_length <= cur_length;
                                STATUS <= STATUS;
                                eth_tvalid <= 0;
                                eth_tkeep <= 8'hff;
                                eth_tdata <= din;
                                eth_tlast <= 0; 
                            end
                        //end
                         else begin //valid==0
                            if(empty==0) begin
                                rd_en <= 1;
                                cur_length <= cur_length;
                                STATUS <= STATUS;
                                eth_tvalid <= 0;
                                eth_tkeep <= 0;
                                eth_tdata <= 0;
                                eth_tlast <= 0;
                            end
                            else begin
                                rd_en <= 0;
                                cur_length <= cur_length;
                                STATUS <= STATUS;
                                eth_tvalid <= 0;
                                eth_tkeep <= 0;
                                eth_tdata <= 0;
                                eth_tlast <= 0;
                            end
                        end*/
                    /*end
                    else begin // eth_tready==0
                        if(valid==1) begin
                            cur_length <= cur_length;
                            rd_en <= 0;
                            STATUS <= 5;
                            eth_tvalid <= 0;
                            eth_tkeep <= 8'hff;
                            eth_tdata <= din;
                            eth_tlast <= 0; 
                        end
                        else begin
                            rd_en <= 0;
                            cur_length <= cur_length;
                            STATUS <= STATUS;
                            
                            eth_tvalid <= 0;
                            eth_tkeep <= 0;
                            eth_tdata <= 0;
                            eth_tlast <= 0; 
                        end
                        
                    end*/
                    
                    
    //                if(empty==0 && eth_tready==1) begin
    //                    rd_en <= 1;
    //                    if(valid==1) begin
    //                        cur_length <= cur_length + 8;
    //                        STATUS <= STATUS;
    //                        eth_tvalid <= 1;
    //                        eth_tkeep <= 8'hff;
    //                        eth_tdata <= din;
    //                        eth_tlast <= 0; 
    //                    end
    //                    else begin
    //                        cur_length <= cur_length;
    //                        STATUS <= STATUS;
    //                        eth_tvalid <= 0;
    //                        eth_tkeep <= 0;
    //                        eth_tdata <= 0;
    //                        eth_tlast <= 0; 
    //                    end
    //                end
    //                else/* if(empty==0 && eth_tready==0)*/ begin
    //                    rd_en <= 0;
    //                    cur_length <= cur_length;
    //                    STATUS <= STATUS;
    //                    eth_tvalid <= 0;
    //                    eth_tkeep <= 0;
    //                    eth_tdata <= 0;
    //                    eth_tlast <= 0; 
    //                end
                    /*else if(empty==1) begin
                        rd_en <= 0;
                        STATUS <= STATUS;
                        eth_tvalid <= 0;
                        eth_tkeep <= 0;
                        eth_tdata <= 0;
                        eth_tlast <= 0; 
                    end*/
           /*     end
                
            end
            5: begin
                if(eth_tready==1) begin
                    STATUS <= 4;
                    cur_length <= cur_length + 8;
                    rd_en <= 0;
                    eth_tvalid <= 1;
                    eth_tkeep <= 8'hff;
                    eth_tdata <= din;
                    eth_tlast <= 0; 
                end
                else begin
                    rd_en <= 0;
                    cur_length <= cur_length;
                    STATUS <= STATUS;
                    
                    eth_tvalid <= 0;
                    eth_tkeep <= 0;
                    eth_tdata <= 0;
                    eth_tlast <= 0;
                end
            end*/
            endcase
        
        /*case(STATUS[3:0])
        0: begin
            if(eth_statusdata_in==0 && eth_statusdata_out==0) begin
                if(recv_tvalid==1) begin
                    if(recv_tlast==1) begin
                        hold <= 0;
                        STATUS <= 2;
                    end
                    else begin
                        hold <= 1;
                        STATUS <= 1;
                    end
                    eth_statuswea <= 1;//4'b0;
                    eth_statusdata_out <= 1;//32'b0;
                end
                else begin
                    hold <= 1;
                    STATUS <= STATUS;
                    eth_statuswea <= 0;//4'b0;
                    eth_statusdata_out <= 0;//32'b0;
                end
            end
            else begin
                eth_statuswea <= 0;//4'b0;
                eth_statusdata_out <= 0;//32'b0;
                hold <= 0;
                STATUS <= STATUS;
            end
        end
        1: begin
            eth_statuswea <= 0;
            eth_statusdata_out <= 0;
            if(recv_tvalid==1) begin
                if(recv_tlast==0) begin
                    hold <= 1;
                    STATUS <= STATUS;
                end
                else begin
                    hold <= 0;
                    STATUS <= 2;
                end
            end
            else begin
                hold <= 1;
                STATUS<=STATUS;
            end
        end
        2: begin
            STATUS <= 3;
            eth_statuswea <= 0;
            eth_statusdata_out <= 0;
            hold <= 0;
        end
        3: begin
            STATUS <= 4;
            eth_statuswea <= 0;
            eth_statusdata_out <= 0;
            hold <= 0;
        end
        4: begin
            STATUS <= 0;
            eth_statuswea <= 0;
            eth_statusdata_out <= 0;
            hold <= 0;
        end
        endcase*/
        
        end
    end
endmodule
