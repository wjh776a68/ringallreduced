`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/19/2022 04:03:46 PM
// Design Name: 
// Module Name: recv_adjustpacket
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module recv_adjustpacket # (
    parameter TDATA_WIDTH = 64,
    parameter TKEEP_WIDTH = 8
    //parameter packet_min = 64,
    //parameter packet_max = 9600,
    //localparam packet_min_counter = packet_min - 1
    ) (

    (* X_INTERFACE_PARAMETER = "FREQ_HZ 390625000" *)
    output          m_axis_tready,
    input           m_axis_tvalid,
    input [TDATA_WIDTH - 1 : 0]  m_axis_tdata,
    input [TKEEP_WIDTH - 1 : 0]   m_axis_tkeep,
    input           m_axis_tlast,
    input [0 : 0]   m_axis_tuser,
    
    (* X_INTERFACE_PARAMETER = "FREQ_HZ 390625000" *)
    input           s_axis_tready,
    output reg          s_axis_tvalid,
    output reg [TDATA_WIDTH - 1 : 0] s_axis_tdata,
    output reg [TKEEP_WIDTH - 1 : 0]  s_axis_tkeep,
    output           s_axis_tlast,
    output reg [0 : 0]  s_axis_tuser,
    
    input clk
    );
    reg s_axis_tlast_part1;
    always @ (posedge clk) begin
        s_axis_tdata  <= m_axis_tdata;
        s_axis_tvalid <= m_axis_tvalid && (m_axis_tdata[62:0] != 63'h7fffffff_ffffffff);
        s_axis_tkeep  <= {1'b1, m_axis_tkeep[6 : 0]};
        s_axis_tlast_part1 <= m_axis_tlast & m_axis_tkeep[7]; 
        s_axis_tuser  <= m_axis_tuser;
        //m_axis_tready = s_axis_tready;
        
    end
    
    //assign s_axis_tdata  = m_axis_tdata;
    //assign s_axis_tvalid = m_axis_tvalid && (m_axis_tdata != 64'hffffffff_ffffffff);
    //assign s_axis_tkeep  = m_axis_tkeep;
    assign s_axis_tlast  = s_axis_tlast_part1 | (m_axis_tdata == 64'hffffffff_ffffffff);
    //assign s_axis_tuser  = m_axis_tuser;
    //assign m_axis_tready = s_axis_tready;
    
endmodule
