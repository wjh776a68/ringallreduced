module ringallreduce_controller (
    input clk,
    input rar_start,
    input [31:0] sync_bytes_amount,
    output reg rar_done,

    input [15 : 0] ring_device_id,
    (* mark_debug = "true" *) input [15 : 0] ring_device_amount,

    output reg [1:0] mux_select_0, // hbm lane with data need sync
    output reg [1:0] mux_select_1, // hbm lane to save tmp data (send/recv)
    output reg [1:0] mux_select_2, // hbm lane to save tmp data (recv/send)

    output reg [31:0] m_ctrl_ports_0_rd_blockamount = 0 ,
    output reg        m_ctrl_ports_0_rd_run         = 0 ,
    output reg [31:0] m_ctrl_ports_0_rd_startaddress = 0,
    output reg [31:0] m_ctrl_ports_0_wr_blockamount  = 0,
    output reg        m_ctrl_ports_0_wr_run          = 0,
    output reg [31:0] m_ctrl_ports_0_wr_startaddress = 0,
    input        m_ctrl_ports_0_busy,

    output reg [31:0] m_ctrl_ports_1_rd_blockamount  = 0,
    output reg        m_ctrl_ports_1_rd_run          = 0,
    output reg [31:0] m_ctrl_ports_1_rd_startaddress = 0,
    output reg [31:0] m_ctrl_ports_1_wr_blockamount  = 0,
    output reg        m_ctrl_ports_1_wr_run          = 0,
    output reg [31:0] m_ctrl_ports_1_wr_startaddress = 0,
    input        m_ctrl_ports_1_busy,

    output reg [31:0] m_ctrl_ports_2_rd_blockamount  = 0,
    output reg        m_ctrl_ports_2_rd_run          = 0,
    output reg [31:0] m_ctrl_ports_2_rd_startaddress = 0,
    output reg [31:0] m_ctrl_ports_2_wr_blockamount  = 0,
    output reg        m_ctrl_ports_2_wr_run          = 0,
    output reg [31:0] m_ctrl_ports_2_wr_startaddress = 0,
    input        m_ctrl_ports_2_busy,

    output reg skip_calculate_a = 0,
    output reg skip_calculate_b = 0,

    output reg saveto_hbm_part_mux = 0,
    output reg fetchfrom_hbm_part_mux = 0,

    output reg duplicate_on = 0
);

    (* mark_debug = "true" *) reg [15:0] cur_send_part = 0;
    reg [15:0] cur_recv_part = 0;
    reg [31:0] cur_send_part_address = 0;
    reg [31:0] cur_recv_part_address = 0;
    (* mark_debug = "true" *) reg [31:0] part_step = 0;
    reg [31:0] tail_part_step = 0;
    reg [3:0] rar_FSM = 0, rar_FSM_next = 0;
    reg [15 : 0] ring_device_amount_real = 1;
    reg [15 : 0] scatter_cycle_times = 0;
    
    
    
    
    
    
    reg sync_bytes_amount_valid = 0, ring_device_amount_real_valid = 0;
    wire [47 : 0] tmp_part_step;
    wire tmp_part_step_valid;
    
    reg [1 : 0] init_constant_FSM = 0, init_constant_FSM_next = 0;
    reg init_constant_start = 0;
    reg init_constant_done = 0;
    reg [31 : 0] div_result_last_valid;
    
    always @ (posedge clk) begin
        init_constant_FSM <= init_constant_FSM_next;
    end
    
    always @ (*) begin
        case (init_constant_FSM)
        0: begin
            if (init_constant_start) begin
                init_constant_FSM_next <= 1;
            end
        end
        1: begin
            init_constant_FSM_next <= 2;
        end
        2: begin
            init_constant_FSM_next <= 3;
        end
        3: begin
            if (~init_constant_start) begin
                init_constant_FSM_next <= 0;
            end
        end
        endcase
    end
    
    always @ (posedge clk) begin
        case (init_constant_FSM_next)
        0: begin
            init_constant_done <= 0;
        end
        1: begin
            ring_device_amount_real <= ring_device_amount + 1;
        end
        2: begin
            sync_bytes_amount_valid <= 1;
            ring_device_amount_real_valid <= 1;
        end
        3: begin
            sync_bytes_amount_valid <= 0;
            ring_device_amount_real_valid <= 0;
            if (tmp_part_step_valid) begin
                init_constant_done <= 1;
                div_result_last_valid <= tmp_part_step;
            end
        end
        endcase
    end
    
    
    reg [1:0] init_constant2_FSM = 0, init_constant2_FSM_next = 0;
    reg init_constant2_FSM_start = 0;
    reg init_constant2_FSM_done = 0;
    
    always @ (posedge clk) begin
        init_constant2_FSM <= init_constant2_FSM_next;
    end
    (* mark_debug = "true" *) reg mul_gen_s_valid = 0; 
    localparam mul_latency = 3;
    (* mark_debug = "true" *) reg [mul_latency:0] mul_gen_m_valid = 0;
    always @ (posedge clk) begin
        mul_gen_m_valid <= {mul_gen_m_valid[mul_latency - 1:0], mul_gen_s_valid};
    end
    /*reg mul_gen_inst_0_a_tvalid = 0, mul_gen_inst_1_a_tvalid = 0;
    reg mul_gen_inst_0_b_tvalid = 0, mul_gen_inst_1_b_tvalid = 0;
    wire mul_gen_inst_0_m_tvalid, mul_gen_inst_1_m_tvalid;*/
    (* mark_debug = "true" *) wire [31:0] mul_gen_inst_0_m_tdata, mul_gen_inst_1_m_tdata;
    
    always @ (*) begin
        case (init_constant2_FSM)
        0: begin
            if (init_constant2_FSM_start) begin
                init_constant2_FSM_next = 1;
            end
        end
        1: begin
            init_constant2_FSM_next = 2;
        end
        2: begin
            if (mul_gen_m_valid[mul_latency]) begin // == mul_gen_inst_1_m_tvalid
                init_constant2_FSM_next = 3;
            end
        end
        3: begin
            if (~init_constant2_FSM_start) begin
                init_constant2_FSM_next = 0;
            end
        end
        endcase
    end
    
    //            tail_part_step <= sync_bytes_amount - part_step * ring_device_amount; // tail_part_step <= sync_bytes_amount - part_step * ring_device_amount_real + part_step;
//            cur_send_part_address <= (ring_device_amount - ring_device_id) * part_step; // as followed
//            cur_send_part <= ring_device_amount - ring_device_id; // ring_device_id is counterclockwise,  ring_device_amount - ring_device_id is clockwise
    /*mul_gen_0 mul_gen_inst_0 (
        .s_axis_a_tdata(part_step),
        .s_axis_a_tvalid(mul_gen_inst_0_a_tvalid),
        .s_axis_b_tdata(ring_device_amount),
        .s_axis_b_tvalid(mul_gen_inst_0_b_tvalid),
        .aclk(clk),
        .m_axis_dout_tdata(mul_gen_inst_0_m_tdata),
        .m_axis_dout_tvalid(mul_gen_inst_0_m_tvalid)
    );
    
    mul_gen_0 mul_gen_inst_1 (
        .s_axis_a_tdata(cur_send_part),
        .s_axis_a_tvalid(mul_gen_inst_1_a_tvalid),
        .s_axis_b_tdata(part_step),
        .s_axis_b_tvalid(mul_gen_inst_1_b_tvalid),
        .aclk(clk),
        .m_axis_dout_tdata(mul_gen_inst_1_m_tdata),
        .m_axis_dout_tvalid(mul_gen_inst_1_m_tvalid)
    );*/
    mult_gen_0 mult_gen_inst_0 (
        .CLK(clk),
        .A(part_step),
        .B(ring_device_amount),
        .P(mul_gen_inst_0_m_tdata)
    );
    
    mult_gen_0 mult_gen_inst_1 (
        .CLK(clk),
        .A(cur_send_part),
        .B(part_step),
        .P(mul_gen_inst_1_m_tdata)
    );
    
    reg [31:0] tmp_tail_part_step, tmp_cur_send_part_address;
    
    always @ (posedge clk) begin
        case (init_constant2_FSM_next)
        0: begin
            init_constant2_FSM_done <= 0;
        end
        1: begin
            mul_gen_s_valid <= 1;
            
        end
        2: begin
            mul_gen_s_valid <= 0;
            if (mul_gen_m_valid[mul_latency]) begin
                tmp_tail_part_step <= sync_bytes_amount - mul_gen_inst_0_m_tdata;
                tmp_cur_send_part_address <= mul_gen_inst_1_m_tdata;
            end
        end
        3: begin
            init_constant2_FSM_done <= 1;
        end
        endcase
    end
    
    
    
    always @ (posedge clk) begin
        rar_FSM <= rar_FSM_next;
    end

    always @ (*) begin
        case (rar_FSM)
        0: begin
            if (rar_start) begin
                rar_FSM_next = 1;
            end 
            else begin
                rar_FSM_next = 0;
            end
        end
        1: begin // prepare constants
            if (init_constant_done) begin
                rar_FSM_next = 2;
            end
        end
        2: begin // prepare constants part 2
            if (init_constant2_FSM_done) begin
                rar_FSM_next = 3;
            end
        end
        3: begin
            rar_FSM_next = 4;
        end
        4: begin // sync start special first time, scatter data odd
            rar_FSM_next = 5;
        end
        5: begin // wait
            if (m_ctrl_ports_0_busy == 0&& m_ctrl_ports_1_busy == 0) begin
                rar_FSM_next = 6;
            end
        end
        6: begin // scatter data even
            rar_FSM_next = 7;
        end
        7: begin // wait
            if (m_ctrl_ports_0_busy == 0 && m_ctrl_ports_1_busy == 0 && m_ctrl_ports_2_busy == 0) begin
                if (scatter_cycle_times == ring_device_amount) begin // step to gather
                    rar_FSM_next = 10;
                end
                else begin
                    rar_FSM_next = 8;
                end
            end
        end
        8: begin // scatter data odd
            rar_FSM_next = 9;
        end
        9: begin // wait
            if (m_ctrl_ports_0_busy == 0 && m_ctrl_ports_1_busy == 0 && m_ctrl_ports_2_busy == 0) begin
                if (scatter_cycle_times == ring_device_amount) begin // step to gather
                    rar_FSM_next = 10;
                end
                else begin
                    rar_FSM_next = 6;
                end
            end
        end
        10: begin // gather data init
            if (saveto_hbm_part_mux == 0) begin
                rar_FSM_next = 11; // last scatter data saved to part 0, so next scatter data would save to part 1.
            end
            else begin
                rar_FSM_next = 13;
            end
        end
        11: begin // gather data odd
            rar_FSM_next = 12;
        end 
        12: begin // wait
            if (m_ctrl_ports_0_busy == 0 && m_ctrl_ports_1_busy == 0 && m_ctrl_ports_2_busy == 0) begin
                if (scatter_cycle_times == ring_device_amount_real) begin // step to gather
                    rar_FSM_next = 15;
                end
                else begin
                    rar_FSM_next = 13;
                end
            end
        end
        13: begin // gather data even
            rar_FSM_next = 14;
        end
        14: begin // wait
            if (m_ctrl_ports_0_busy == 0 && m_ctrl_ports_1_busy == 0 && m_ctrl_ports_2_busy == 0) begin
                if (scatter_cycle_times == ring_device_amount_real) begin // step to gather
                    rar_FSM_next = 15;
                end
                else begin
                    rar_FSM_next = 11;
                end
            end
        end
        15: begin // finish
            if (~rar_start) begin
                rar_FSM_next = 0;
            end
            else begin
                rar_FSM_next = 15;
            end
        end
        endcase
    end
    
    
    
    div_gen_0 div_gen_inst_0 (
        .s_axis_divisor_tdata(ring_device_amount_real),
        .s_axis_divisor_tvalid(ring_device_amount_real_valid),
        .s_axis_dividend_tdata(sync_bytes_amount),
        .s_axis_dividend_tvalid(sync_bytes_amount_valid),
        .aclk(clk),
        .m_axis_dout_tdata(tmp_part_step),
        .m_axis_dout_tvalid(tmp_part_step_valid)
    );
    
    reg [15 : 0] ring_device_id_plus1 = 0;
    always @ (posedge clk) begin
        ring_device_id_plus1 <= ring_device_id + 1;
    end

    always @ (posedge clk) begin
        case (rar_FSM_next)
        0: begin
            mux_select_0 <= 2'b0;
            mux_select_1 <= 2'b0;
            mux_select_2 <= 2'b0;
            scatter_cycle_times <= 0;
            saveto_hbm_part_mux <= 0;
            fetchfrom_hbm_part_mux <= 1;
            rar_done <= 0;
            duplicate_on <= 0;
        end
        1: begin
            init_constant_start <= 1;
            //ring_device_amount_real <= ring_device_amount + 1;
        end
        2: begin
            init_constant_start <= 0;
            init_constant2_FSM_start <= 1;
            part_step <= div_result_last_valid;
            cur_send_part <= ring_device_amount - ring_device_id;
            //sync_bytes_amount_valid <= 1;
            //ring_device_amount_real_valid <= 1;
            //part_step <= tmp_part_step;
            //part_step <= sync_bytes_amount / ring_device_amount_real;
        end
        3: begin
            init_constant2_FSM_start <= 0;
            tail_part_step <= tmp_tail_part_step;
            cur_send_part_address <= tmp_cur_send_part_address;
//            tail_part_step <= sync_bytes_amount - part_step * ring_device_amount; // tail_part_step <= sync_bytes_amount - part_step * ring_device_amount_real + part_step;
//            cur_send_part_address <= (ring_device_amount - ring_device_id) * part_step; // as followed
//            cur_send_part <= ring_device_amount - ring_device_id; // ring_device_id is counterclockwise,  ring_device_amount - ring_device_id is clockwise
            
            /* if (ring_device_id == ring_device_amount) begin
                cur_recv_part_address = 0;
                cur_recv_part = 0;
            end
            else begin
                cur_recv_part_address <= ring_device_id_plus1 * part_step; // recv = send + 1 (next)
                cur_recv_part <= ring_device_id_plus1;
            end */
        end
        4: begin // duplicate data need sync into HBM lane tmp_1
            mux_select_0 <= 2'b01;
            mux_select_1 <= 2'b01;
            mux_select_2 <= 2'b01;
            
            saveto_hbm_part_mux <= 0;
            fetchfrom_hbm_part_mux <= 1;

            skip_calculate_a <= 1;
            //skip_calculate_b <= 0;

            m_ctrl_ports_0_rd_run <= 1;
            m_ctrl_ports_0_rd_startaddress <= cur_send_part_address;
            m_ctrl_ports_1_wr_run <= 1;
            m_ctrl_ports_1_wr_startaddress <= cur_send_part_address;

            cur_recv_part <= cur_send_part;                              //recv is last send
            cur_recv_part_address <= cur_send_part_address;

            if (cur_send_part == ring_device_amount) begin
                m_ctrl_ports_0_rd_blockamount <= tail_part_step;
                cur_send_part_address <= 0;//cur_send_part_address + tail_part_step;
                cur_send_part <= 0;

                m_ctrl_ports_1_wr_blockamount <= tail_part_step;
            end
            else begin
                m_ctrl_ports_0_rd_blockamount <= part_step;
                cur_send_part_address <= cur_send_part_address + part_step;
                cur_send_part <= cur_send_part + 1;

                m_ctrl_ports_1_wr_blockamount <= part_step;
            end

            
           /*  if (cur_send_part == ring_device_amount) begin
                
                //cur_recv_part_address <= cur_recv_part_address + tail_part_step;
                //cur_recv_part <= 0;
            end
            else begin
                
                //cur_recv_part_address <= cur_recv_part_address + part_step;
                //cur_recv_part <= cur_recv_part + 1;
            end
             */

        end
        5: begin
            m_ctrl_ports_0_rd_run <= 0;
            m_ctrl_ports_1_wr_run <= 0;
        end
        6: begin // send data from HBM lane tmp_1 while recv data into HBM lane tmp_2
            skip_calculate_a <= 0;
            //skip_calculate_b <= 0;

            saveto_hbm_part_mux <= 1;
            fetchfrom_hbm_part_mux <= 0;

            m_ctrl_ports_0_rd_run <= 1;
            m_ctrl_ports_0_rd_startaddress <= cur_send_part_address;
            m_ctrl_ports_2_wr_run <= 1;
            m_ctrl_ports_2_wr_startaddress <= cur_send_part_address;

            if (cur_send_part == ring_device_amount) begin
                m_ctrl_ports_0_rd_blockamount <= tail_part_step;
                //cur_send_part <= 0;
                m_ctrl_ports_2_wr_blockamount <= tail_part_step;
                cur_send_part_address <= 0;//cur_send_part_address + tail_part_step;
                cur_send_part <= 0;
            end
            else begin
                m_ctrl_ports_0_rd_blockamount <= part_step;
                //cur_send_part <= cur_send_part + 1;
                m_ctrl_ports_2_wr_blockamount <= part_step;
                cur_send_part_address <= cur_send_part_address + part_step;
                cur_send_part <= cur_send_part + 1;
            end


            cur_recv_part <= cur_send_part;                              //recv is last send
            cur_recv_part_address <= cur_send_part_address;

            m_ctrl_ports_1_rd_run <= 1;
            m_ctrl_ports_1_rd_startaddress <= cur_recv_part_address;
            if (cur_recv_part == ring_device_amount) begin
                m_ctrl_ports_1_rd_blockamount <= tail_part_step;
                //cur_send_part <= 0;
                //cur_send_part_address <= cur_send_part_address + tail_part_step;
            end
            else begin
                m_ctrl_ports_1_rd_blockamount <= part_step;
                //cur_send_part <= cur_send_part + 1;
                //cur_send_part_address <= cur_send_part_address + part_step;
            end

            
            /* if (cur_send_part == ring_device_amount) begin
                
            end
            else begin
                
            end */

            scatter_cycle_times <= scatter_cycle_times + 1;
        end
        7: begin
            m_ctrl_ports_0_rd_run <= 0;
            m_ctrl_ports_1_rd_run <= 0;
            m_ctrl_ports_2_wr_run <= 0;
        end
        8: begin
            saveto_hbm_part_mux <= 0;
            fetchfrom_hbm_part_mux <= 1;
        
            m_ctrl_ports_0_rd_run <= 1;
            m_ctrl_ports_0_rd_startaddress <= cur_send_part_address;
            m_ctrl_ports_1_wr_run <= 1;
            m_ctrl_ports_1_wr_startaddress <= cur_send_part_address;

            if (cur_send_part == ring_device_amount) begin
                m_ctrl_ports_0_rd_blockamount <= tail_part_step;
                m_ctrl_ports_1_wr_blockamount <= tail_part_step;
                cur_send_part <= 0;
                cur_send_part_address <= 0;//cur_send_part_address + tail_part_step;
            end
            else begin
                m_ctrl_ports_0_rd_blockamount <= part_step;
                m_ctrl_ports_1_wr_blockamount <= part_step;
                cur_send_part <= cur_send_part + 1;
                cur_send_part_address <= cur_send_part_address + part_step;
            end

            cur_recv_part <= cur_send_part;                              //recv is last send
            cur_recv_part_address <= cur_send_part_address;

            m_ctrl_ports_2_rd_run <= 1;
            m_ctrl_ports_2_rd_startaddress <= cur_recv_part_address;
            if (cur_recv_part == ring_device_amount) begin
                m_ctrl_ports_2_rd_blockamount <= tail_part_step;
                //cur_recv_part <= 0;
                //cur_send_part_address <= cur_send_part_address + tail_part_step;
            end
            else begin
                m_ctrl_ports_2_rd_blockamount <= part_step;
                //cur_send_part <= cur_send_part + 1;
                //cur_send_part_address <= cur_send_part_address + part_step;
            end

            
            /* if (cur_send_part == ring_device_amount) begin
                
            end
            else begin
                
            end */

            scatter_cycle_times <= scatter_cycle_times + 1;
        end
        9: begin
            m_ctrl_ports_0_rd_run <= 0;
            m_ctrl_ports_2_rd_run <= 0;
            m_ctrl_ports_1_wr_run <= 0;
        end
        10: begin
            skip_calculate_b <= 1;
            //skip_calculate_a <= 0;
            scatter_cycle_times <= 0;
            duplicate_on <= 1;
        end
        11: begin
            saveto_hbm_part_mux <= 1;
            fetchfrom_hbm_part_mux <= 0;

            m_ctrl_ports_0_wr_run <= 1;
            m_ctrl_ports_0_wr_startaddress <= cur_send_part_address;
            m_ctrl_ports_2_wr_run <= 1;
            m_ctrl_ports_2_wr_startaddress <= cur_send_part_address;

            if (cur_send_part == ring_device_amount) begin
                m_ctrl_ports_0_wr_blockamount <= tail_part_step;
                m_ctrl_ports_2_wr_blockamount <= tail_part_step;
                cur_send_part <= 0;
                cur_send_part_address <= 0;
            end
            else begin
                m_ctrl_ports_0_wr_blockamount <= part_step;
                m_ctrl_ports_2_wr_blockamount <= part_step;
                cur_send_part <= cur_send_part + 1;
                cur_send_part_address <= cur_send_part_address + part_step;
            end

            cur_recv_part <= cur_send_part;                              //recv is last send
            cur_recv_part_address <= cur_send_part_address;

            m_ctrl_ports_1_rd_run <= 1;
            m_ctrl_ports_1_rd_startaddress <= cur_recv_part_address;
            if (cur_recv_part == ring_device_amount) begin
                m_ctrl_ports_1_rd_blockamount <= tail_part_step;
                //cur_send_part <= 0;
            end
            else begin
                m_ctrl_ports_1_rd_blockamount <= part_step;
                //cur_send_part <= cur_send_part + 1;
            end

            
            /* if (cur_send_part == ring_device_amount) begin
                
            end
            else begin
                
            end */

            scatter_cycle_times <= scatter_cycle_times + 1;
        end
        12: begin
            m_ctrl_ports_0_wr_run <= 0;
            m_ctrl_ports_1_rd_run <= 0;
            m_ctrl_ports_2_wr_run <= 0;
        end
        13: begin
            saveto_hbm_part_mux <= 0;
            fetchfrom_hbm_part_mux <= 1;
        
            m_ctrl_ports_0_wr_run <= 1;
            m_ctrl_ports_0_wr_startaddress <= cur_send_part_address;
            m_ctrl_ports_1_wr_run <= 1;
            m_ctrl_ports_1_wr_startaddress <= cur_send_part_address;

            if (cur_send_part == ring_device_amount) begin
                m_ctrl_ports_0_wr_blockamount <= tail_part_step;
                m_ctrl_ports_1_wr_blockamount <= tail_part_step;
                cur_send_part <= 0;
                cur_send_part_address <= 0;
            end
            else begin
                m_ctrl_ports_0_wr_blockamount <= part_step;
                m_ctrl_ports_1_wr_blockamount <= part_step;
                cur_send_part <= cur_send_part + 1;
                cur_send_part_address <= cur_send_part_address + part_step;
            end

            cur_recv_part <= cur_send_part;                              //recv is last send
            cur_recv_part_address <= cur_send_part_address;

            m_ctrl_ports_2_rd_run <= 1;
            m_ctrl_ports_2_rd_startaddress <= cur_recv_part_address;
            if (cur_recv_part == ring_device_amount) begin
                m_ctrl_ports_2_rd_blockamount <= tail_part_step;
                //cur_send_part <= 0;
            end
            else begin
                m_ctrl_ports_2_rd_blockamount <= part_step;
                //cur_send_part <= cur_send_part + 1;
            end

            
            /* if (cur_recv_part == ring_device_amount) begin
                
            end
            else begin
                
            end */

            scatter_cycle_times <= scatter_cycle_times + 1;
        end
        14: begin
            m_ctrl_ports_0_wr_run <= 0;
            m_ctrl_ports_2_rd_run <= 0;
            m_ctrl_ports_1_wr_run <= 0;
        end
        15: begin
            rar_done <= 1;
        end
        endcase
    end





endmodule
