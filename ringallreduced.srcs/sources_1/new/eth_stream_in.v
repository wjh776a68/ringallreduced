`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/11/21 15:30:48
// Design Name: 
// Module Name: stream
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//
//
//
//
//
//////////////////////////////////////////////////////////////////////////////////


module eth_stream_in(
input clk,

input [63:0] s_axis_tdata,
input [7:0] s_axis_tkeep,
input s_axis_tlast,
output s_axis_tready,
input s_axis_tvalid,

input [10:0] data_count,
output [63:0] din,
output wr_en,

input full

    );
    
    assign din = {s_axis_tdata[63:32], s_axis_tdata[31:0]};
    assign wr_en = (full==0 && data_count < 512) & s_axis_tvalid & (&s_axis_tkeep[7:0]);
    assign s_axis_tready = ((full==0 && data_count < 512)? 1 : 0);

endmodule

















/*module stream_in(
input clk,
input rst,

input syoreth,

input [63:0] s_axis_tdata,
input [7:0] s_axis_tkeep,
input s_axis_tlast,
output reg s_axis_tready,
input s_axis_tvalid,


input [10:0] data_count,
output reg [63:0] din,
output reg wr_en,

input [10:0] data_count_eth,
output reg [63:0] din_eth,
output reg wr_en_eth,

input sy_full,
input eth_full

*//*output [63:0] eth_tdata,
output eth_tvalid,
output [7:0] eth_tkeep,
output eth_tuser,
output eth_tlast,
input eth_tready*//*

//output reg [3:0] RUN = 0
    );

*//*assign eth_tdata = s_axis_tdata;
assign eth_tvalid = (syoreth==0) ? 0:s_axis_tvalid;
assign eth_tkeep = s_axis_tkeep;
assign eth_tuser = 0;
assign eth_tlast = s_axis_tlast;
assign s_axis_tready = (syoreth==0) ? (data_count<512?1:0) : eth_tready;*//*

always@(posedge clk or posedge rst)
begin
    if(rst)
        begin
            din <= 0;
            wr_en <= 0;
            s_axis_tready <= 0;
        end
    else
        begin
            if(syoreth==0) begin
                din <= {s_axis_tdata[31:0],s_axis_tdata[63:32]};
                wr_en <= s_axis_tready & s_axis_tvalid & (&s_axis_tkeep[7:0]);
                if(sy_full==0 && data_count < 512) begin
                    s_axis_tready <= 1;
                end
                else begin
                    s_axis_tready <= 0;
                end
            end
            else begin
                
                din_eth <= {s_axis_tdata[63:32],s_axis_tdata[31:0]}; //咱也不知道eth模块里面是怎么把位置换回来的，反正不用换了
                wr_en_eth <= s_axis_tready & s_axis_tvalid & (&s_axis_tkeep[7:0]);
                if(eth_full==0 && data_count_eth < 512) begin
                    s_axis_tready <= 1;
                end
                else begin
                    s_axis_tready <= 0;
                end
                
            end
            
        end
end





*//*reg [8:0] tkeep;
reg [63:0] tdata;
reg twoclk;


reg done = 1;


always@(posedge clk)
begin
    if(rst) begin
        RUN <= 0;
        s_axis_tready <= 0;
    end
    
    case(RUN) 
    4'd0: begin
        if(s_axis_tvalid == 1) begin
            tkeep <= s_axis_tkeep;
            tdata <= s_axis_tdata;
        end
        else begin
            tkeep <= 0;
            tdata <= 0;
        end
        
        if(s_axis_tkeep[3:0]==4'hf) begin
            s_axis_tready <= 1;
            din <= s_axis_tdata[31:0];
            wr_en <= 1;
            RUN <= 1;
        end
        else begin
            s_axis_tready <= 1;
            RUN <= 1;
            wr_en <= 0;
        end

    end
    4'd1: begin
        if(tkeep[7:4]==4'hf) begin
            s_axis_tready <= 0;
            din <= tdata[63:32];
            RUN <= 0;
            wr_en <= 1;
        end
        else begin
            s_axis_tready <= 0;
            RUN <= 0;
            wr_en <= 0;
        end
    end
    endcase
end*//*
   
endmodule*/
