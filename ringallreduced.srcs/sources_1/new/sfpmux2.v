`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/21/2022 05:19:30 PM
// Design Name: 
// Module Name: sfpmux2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module sfpmux2(
    //input           A_clk,
    input           A_rd_en,
    input           A_wr_en,
    input [63:0]    A_din_0,
    
    //input           B_clk,
    input           B_rd_en,
    input           B_wr_en,
    input [63:0]    B_din_0,
    
    input           isB,
    
    //output           o_clk,
    output           o_rd_en,
    output           o_wr_en,
    output [63:0]    o_din_0
    
    );
    
    //assign o_clk = (isB == 0)? A_clk : B_clk;
    assign o_rd_en = (isB == 0)? A_rd_en : B_rd_en;
    assign o_wr_en = (isB == 0)? A_wr_en : B_wr_en;
    assign o_din_0 = (isB == 0)? A_din_0 : B_din_0;
    
endmodule
