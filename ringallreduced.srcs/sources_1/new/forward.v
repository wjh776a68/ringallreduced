`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/15/2022 09:57:26 AM
// Design Name: 
// Module Name: forward
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module forward # ( // adjust axis stream
    parameter TDATA_WIDTH = 64,
    parameter TKEEP_WIDTH = 8,
    parameter packet_min = 8,//64,(byte)
    parameter packet_max = 1200,//9600,(byte)
    localparam packet_min_counter = packet_min - 1
) (
    (* X_INTERFACE_PARAMETER = "FREQ_HZ 390625000" *)
    output          m_axis_tready,
    input           m_axis_tvalid,
    input [TDATA_WIDTH - 1 : 0]  m_axis_tdata,
    input [TKEEP_WIDTH - 1 : 0]   m_axis_tkeep,
    input           m_axis_tlast,
    input [0 : 0]   m_axis_tuser,
    
    (* X_INTERFACE_PARAMETER = "FREQ_HZ 390625000" *)
    input           s_axis_tready,
    output          s_axis_tvalid,
    output [TDATA_WIDTH - 1 : 0] s_axis_tdata,
    output [TKEEP_WIDTH - 1 : 0]  s_axis_tkeep,
    output          s_axis_tlast,
    output [0 : 0]  s_axis_tuser,
    
    input clk
    );
    
    reg [15:0] packet_size_counter = 0; //2^16 = 65536 > 9600
    reg stat_FSM = 0, stat_FSM_next = 0;
    
    always @ (posedge clk) begin
        stat_FSM <= stat_FSM_next;
    end
    reg s_axis_tvalid_packetnouf = 0;
    always @ (posedge clk) begin
        if (s_axis_tready & s_axis_tvalid) begin
            if (s_axis_tlast) begin
                s_axis_tvalid_packetnouf <= 0;
                packet_size_counter <= 0;
            end
            else begin
                s_axis_tvalid_packetnouf <= 1;
                packet_size_counter <= packet_size_counter + 1;
            end
        end
    end
    
    always @ (*) begin
        case (stat_FSM)
        0: begin
            if (m_axis_tready & m_axis_tvalid & m_axis_tlast & packet_size_counter < packet_min_counter) begin // divide large packets
                stat_FSM_next = 1;
            end
            else begin
                stat_FSM_next = 0;
            end
        end
        1: begin
            if (s_axis_tready & s_axis_tvalid & packet_size_counter == packet_min_counter) begin
                stat_FSM_next = 0;
            end
            else begin
                stat_FSM_next = 1;
            end
        end
        endcase
    end
    reg focus_tlast = 0;
    reg focus_tkeep = 0;
    always @ (posedge clk) begin
        case (stat_FSM_next)
        0: begin
            if (packet_size_counter == packet_max - 2) begin
                focus_tlast <= 1;
                focus_tkeep <= 1;
            end
            else begin
                focus_tlast <= 0;
                focus_tkeep <= 0;
            end
        end
        1: begin
            if (packet_size_counter == packet_min_counter) begin
                focus_tlast <= 1;
            end
            else begin
                focus_tlast <= 0;
            end
        end
        endcase
    end

    /*************************************************************************************************************************************************
    * if m_axis_tvalid is invalid while packet frame is not finished, the s_axis_tdata will be set to 7ffff...f to avoid underflow situation,
    * if packet size greater than max packet size, tlast will be forced set to 1 to avoid overflow situation, while the high bit of tkeep be set to 0
    *    to indicate the packet transfer haven't finished.   
    *************************************************************************************************************************************************/

    assign s_axis_tdata  = (stat_FSM) ? 64'hffffffff_ffffffff : (~m_axis_tvalid ? 64'h7fffffff_ffffffff : m_axis_tdata);
    assign s_axis_tvalid = m_axis_tvalid | s_axis_tvalid_packetnouf; //(m_axis_tready & m_axis_tvalid & m_axis_tlast & packet_size_counter < packet_min_counter) | stat_FSM;
    assign s_axis_tkeep  = {~focus_tkeep, m_axis_tkeep[TKEEP_WIDTH - 2 : 0]};/*stat_FSM ? 0 : *///focus_tlast ? {TKEEP_WIDTH{1'b0}}: m_axis_tkeep;//{TKEEP_WIDTH{1'b1}};//m_axis_tkeep;//m_axis_tvalid ? m_axis_tkeep : 0;
    assign s_axis_tlast  = (m_axis_tlast && packet_size_counter >= packet_min_counter) | focus_tlast;
    assign s_axis_tuser  = m_axis_tuser;
    assign m_axis_tready = s_axis_tready && ~stat_FSM;
    
    
    
endmodule
