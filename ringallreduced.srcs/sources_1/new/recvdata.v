module recvdata(
    input clk,

   // input [15 : 0] ring_device_order,
   // input [15 : 0] ring_device_amount,
    input [15 : 0] recv_bytes_amount,
    input recv_start,
    output reg recv_done = 0,

    output reg [63 : 0]  hbm_wrdata,
    output reg           hbm_wr_en = 0,
    input                hbm_full,

    input [63 : 0]  m_axis_tdata,
    input [ 7 : 0]  m_axis_tkeep,
    input           m_axis_tvalid,
    input           m_axis_tlast,
    output reg      m_axis_tready = 0


);

    always @ (posedge clk) begin
        if (~hbm_full) begin
            m_axis_tready <= 1;
        end
        else begin
            m_axis_tready <= 0;
        end
    end

    always @ (posedge clk) begin
        if (m_axis_tvalid & m_axis_tready) begin
            hbm_wr_en <= 1;
            hbm_wrdata <= m_axis_tdata;
        end
        else begin
            hbm_wr_en <= 0;
        end
    end
    
endmodule