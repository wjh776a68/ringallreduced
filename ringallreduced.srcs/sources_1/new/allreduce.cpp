#include <iostream>

using namespace std;

#define MACHINE_NUM 5
#define DATA_NUM 10

int alldata[MACHINE_NUM][DATA_NUM];

void gendata() {
    for (int i = 0; i < MACHINE_NUM; i++) {
        for (int j = 0; j < DATA_NUM; j++) {
            alldata[i][j] = i * DATA_NUM + j;
        }
    }
}

void printdata() {
    for (int i = 0; i < MACHINE_NUM; i++) {
        for (int j = 0; j < DATA_NUM; j++) {
            cout << alldata[i][j] << " ";
        }
        cout << endl;
    }
    cout << endl;
}

void scatterreduce() {

}

void allgather() {

}

void allreduce() {
    scatterreduce();
    allgather();
}

int main() {
    gendata();
    cout << "before allreduce:" << endl;
    printdata();
    allreduce();
    cout << "after allreduce:" << endl;
    printdata();
    return 0;
}