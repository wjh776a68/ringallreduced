module init # (
   parameter sim_id = 'h123456789abcdef
) (
    input clk,
    input init_start,
    output reg init_done,
    output reg [15 : 0] ring_device_amount = 0,
    output reg [15 : 0] ring_device_id = 0,
    
    output reg [97 : 0] current_min_id,
    //output reg [64 - 1 : 0] current_order = 0,

    output reg [255 : 0] s_axis_tdata, // communicate device ids to setup ring order
    output reg [31 : 0]  s_axis_tkeep,
    output reg          s_axis_tvalid,
    output reg          s_axis_tlast,
    input               s_axis_tready,

    input      [255 : 0] m_axis_tdata,
    input      [31 : 0]  m_axis_tkeep,
    input               m_axis_tvalid,
    input               m_axis_tlast,
    output reg          m_axis_tready
);
    reg [64 * 2 - 1 : 0] ring_currentid = 0;
    
    
    reg getid_start = 0;
    wire getid_done;
    wire [97 : 0] device_id;
    getid #(
        .sim_id(sim_id)
    ) getid_inst(
        .clk(clk),
        .getid_start(getid_start),
        .getid_done(getid_done),
        .device_id(device_id)
    );

    reg [2 : 0] init_FSM = 0, init_FSM_next = 0;
    reg last_m_axis_tvalid, last_m_axis_tready;
    always @ (posedge clk) begin
        init_FSM <= init_FSM_next;
    end

    /*************************************************************************
    * 发送和接收转发操作必须保证发送操作优先于接受转发操作，这样才能确定环结束的时间。
    * 因此必须保证fifo的大小能够一次性容下所有的deviceid。即当fifo深度为64时，环连
    * 接的设备数量不能超过64/2=32个。进而收发异步是可行的。
    *************************************************************************/

    always @ (*) begin
        case (init_FSM)
        0: begin
            if (init_start) begin
                init_FSM_next = 1;
            end
        end
        1: begin // get device id
            if (getid_done) begin
                init_FSM_next = 2;
            end
        end
        2: begin // send device id (since device id is 98 bits, we need to send 2 times). as fifo is imported, send and recv operation can be asynchronous
            if (s_axis_tready & s_axis_tvalid) begin
                init_FSM_next = 3;
            end
        end
        3: begin // send device id end
            if (s_axis_tready & s_axis_tvalid) begin
                init_FSM_next = 4;
            end
        end
        4: begin // send  order 
            if (s_axis_tready & s_axis_tvalid) begin
                init_FSM_next = 5;
            end
        end
        5: begin // recv and resend device id
            if (device_id == ring_currentid[97 : 0]) begin
                init_FSM_next = 6;
            end
        end
        6: begin // cleanup data in networks
            if (s_axis_tready & s_axis_tvalid & s_axis_tlast) begin
                init_FSM_next = 7;
            end
        end
        7: begin // forever loop
            /*if (~init_start) begin
                init_FSM_next = 0;
            end*/
        end
        endcase
    end

    always @ (posedge clk) begin
        case (init_FSM_next)
        0: begin 
            init_done <= 0;
            ring_device_amount <= 0;
            ring_device_id <= 0;
            s_axis_tvalid <= 0;
            m_axis_tready <= 0;
            //ring_getmyid_times <= 0;
            getid_start <= 0;
        end
        1: begin
            getid_start <= 1;
        end
        2: begin
            current_min_id <= device_id;
            ring_device_id <= 0;
            getid_start <= 0;
            if (s_axis_tready) begin
                s_axis_tdata <= {{(64 - (97 - 64 + 1)){1'b0}}, device_id[97 : 64]};
                s_axis_tkeep <= {32{1'b1}};
                s_axis_tlast <= 0;
                s_axis_tvalid <= 1;
            end
        end
        3: begin
            if (s_axis_tready) begin
                s_axis_tdata <= device_id[63 : 0];
                s_axis_tkeep <= {32{1'b1}};
                s_axis_tlast <= 0;
                s_axis_tvalid <= 1;
            end
        end
        4: begin
            if (s_axis_tready) begin
                s_axis_tdata <= 0;//device_id[63 : 0];
                s_axis_tkeep <= {32{1'b1}};
                s_axis_tlast <= 1;
                s_axis_tvalid <= 1;
            end
        end
        5: begin
            
            m_axis_tready <= s_axis_tready;
            
            s_axis_tkeep  <= m_axis_tkeep ;
            s_axis_tlast  <= m_axis_tlast ;
            
            if (m_axis_tvalid & m_axis_tready) begin
                ring_currentid <= {ring_currentid[64 : 0], m_axis_tdata[63:0]};
                if (m_axis_tlast == 1) begin
                    if (ring_currentid[97 : 0] == device_id) begin
                        s_axis_tvalid <= 1;
                    end
                    else if (ring_currentid[97 : 0] < current_min_id) begin
                        ring_device_id <= m_axis_tdata + 1;
                        current_min_id <= ring_currentid[97 : 0];
                        //s_axis_tdata <= m_axis_tdata + 1;
                        ring_device_amount <= ring_device_amount + 1;
                        s_axis_tvalid <= 1;
                    end
                    else begin
                        //s_axis_tdata <= m_axis_tdata;// || m_axis_tdata + 1;
                        ring_device_amount <= ring_device_amount + 1;
                        s_axis_tvalid <= 1;
                    end
                    s_axis_tdata <= m_axis_tdata + 1;
                end
                else begin
                    s_axis_tvalid <= m_axis_tvalid;
                    s_axis_tdata  <= m_axis_tdata ;
                end
            end
            else begin
                s_axis_tvalid <= m_axis_tvalid;
                s_axis_tdata  <= m_axis_tdata ;
            end
            
        end
        6: begin
            m_axis_tready <= s_axis_tready;
            s_axis_tdata  <= m_axis_tdata;
            s_axis_tkeep  <= m_axis_tkeep ;
            s_axis_tlast  <= m_axis_tlast ;
            s_axis_tvalid <= m_axis_tvalid;
        end
        7: begin
            s_axis_tvalid <= 0;
            last_m_axis_tready <= m_axis_tready;
            last_m_axis_tvalid <= m_axis_tvalid;
            if (last_m_axis_tready & last_m_axis_tvalid & ~m_axis_tvalid) begin
                init_done <= 1;
                m_axis_tready <= 0;
            end
            else begin
                m_axis_tready <= 1;
            end
        end

        endcase
    end


endmodule


module getid # (
    parameter sim_id = 'h123456789abcdef
) (
    input clk,
    input getid_start,
    output reg getid_done,
    output reg [97:0] device_id = 0
);
    
    wire DOUT;
    reg READ = 0;
    reg SHIFT = 0;

    DNA_PORTE2 #(
      .SIM_DNA_VALUE(sim_id)  // Specifies a sample 96-bit DNA value for simulation.
   )DNA_PORTE2_inst (
        .DOUT(DOUT),   // 1-bit output: DNA output data.
        .CLK(clk),     // 1-bit input: Clock input.
        .DIN(0),     // 1-bit input: User data input pin.
        .READ(READ),   // 1-bit input: Active-High load DNA, active-Low read input.
        .SHIFT(SHIFT)  // 1-bit input: Active-High shift enable input.
    );

    reg [1 : 0] init_FSM = 0, init_FSM_next = 0;
    reg [10 : 0] DNA_read_counter = 0;
    always @ (posedge clk) begin
        init_FSM <= init_FSM_next;
    end

    always @ (*) begin
        case (init_FSM)
        0: begin
            if (getid_start) begin
                init_FSM_next = 1;
            end 
            else begin
                init_FSM_next = 0;
            end
        end
        1: begin
            init_FSM_next = 2;
        end
        2: begin
            if (DNA_read_counter == 99) begin
                init_FSM_next = 3;
            end 
            else begin
                init_FSM_next = 2;
            end
        end
        3: begin
            if (~getid_start) begin
                init_FSM_next = 0;
            end
        end
        endcase
    end

    always @ (posedge clk) begin
        case (init_FSM_next)
        0: begin
            getid_done <= 0;
            DNA_read_counter <= 0;
            READ <= 0;
            SHIFT <= 0;
        end
        1: begin
            //DNA_read_counter <= DNA_read_counter + 1;
            READ <= 1;
            
        end
        2: begin
            DNA_read_counter <= DNA_read_counter + 1;
            device_id <= {DOUT, device_id[97 : 1]};
            SHIFT <= 1;
            READ  <= 0;
        end
        3: begin
            getid_done <= 1;
        end
        endcase

    end


endmodule