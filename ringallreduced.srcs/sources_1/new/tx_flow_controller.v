`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/15/2022 05:35:14 PM
// Design Name: 
// Module Name: tx_flow_controller
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tx_flow_controller(
    input               clk,
    //input      [ 1 : 0] flow_control_args,
    input               flow_control_pause,//max_limited
    input               flow_control_resume, //min_limited
    
    output reg [ 8 : 0] ctl_tx_pause_enable, 
    output reg [ 8 : 0] ctl_tx_pause_req, 
    output reg [15 : 0] ctl_tx_pause_refresh_timer8, 
    output reg [15 : 0] ctl_tx_pause_refresh_timer7, 
    output reg [15 : 0] ctl_tx_pause_refresh_timer6, 
    output reg [15 : 0] ctl_tx_pause_refresh_timer5, 
    output reg [15 : 0] ctl_tx_pause_refresh_timer4, 
    output reg [15 : 0] ctl_tx_pause_refresh_timer3, 
    output reg [15 : 0] ctl_tx_pause_refresh_timer2, 
    output reg [15 : 0] ctl_tx_pause_refresh_timer1, 
    output reg [15 : 0] ctl_tx_pause_refresh_timer0, 
    output reg [15 : 0] ctl_tx_pause_quanta8,
    output reg [15 : 0] ctl_tx_pause_quanta7,  
    output reg [15 : 0] ctl_tx_pause_quanta6, 
    output reg [15 : 0] ctl_tx_pause_quanta5, 
    output reg [15 : 0] ctl_tx_pause_quanta4, 
    output reg [15 : 0] ctl_tx_pause_quanta3,
    output reg [15 : 0] ctl_tx_pause_quanta2, 
    output reg [15 : 0] ctl_tx_pause_quanta1,  
    output reg [15 : 0] ctl_tx_pause_quanta0, 
    output reg          ctl_tx_resend_pause,
    
    output reg pause_FSM = 0,
    output reg pause_FSM_next = 0
    );
    
//    wire flow_control_pause = flow_control_args[1];
//    wire flow_control_resume= flow_control_args[0];
    
    always @ (posedge clk) begin
        ctl_tx_pause_refresh_timer7 <= 0;
        ctl_tx_pause_refresh_timer6 <= 0;
        ctl_tx_pause_refresh_timer5 <= 0;
        ctl_tx_pause_refresh_timer4 <= 0;
        ctl_tx_pause_refresh_timer3 <= 0;
        ctl_tx_pause_refresh_timer2 <= 0;
        ctl_tx_pause_refresh_timer1 <= 0;
        ctl_tx_pause_refresh_timer0 <= 0;
    
        ctl_tx_pause_quanta7 <= 0;
        ctl_tx_pause_quanta6 <= 0;
        ctl_tx_pause_quanta5 <= 0;
        ctl_tx_pause_quanta4 <= 0;
        ctl_tx_pause_quanta3 <= 0;
        ctl_tx_pause_quanta2 <= 0;
        ctl_tx_pause_quanta1 <= 0;
        ctl_tx_pause_quanta0 <= 0;
    
        ctl_tx_resend_pause <= 0;
        
        
    end
    //reg [10:0] pause_hold_times_counter = 0;
    //reg pause_FSM = 0, pause_FSM_next = 0;
    
    always @ (posedge clk) begin
        pause_FSM <= pause_FSM_next;
    end
    
    always @ (*) begin
        case (pause_FSM)
        0: begin
            if (flow_control_pause) begin
                pause_FSM_next <= 1;
            end
            else begin
                pause_FSM_next <= 0;
            end
        end
        1: begin
            /*if (pause_hold_times_counter < 16) begin
                pause_FSM_next <= 1;
            end
            else */if (flow_control_resume) begin
                pause_FSM_next <= 0;
            end
            else begin
                pause_FSM_next <= 1;
            end
        end
        endcase
    end
    
    always @ (posedge clk) begin
        case (pause_FSM_next) 
        0: begin
            //pause_hold_times_counter <= 0;
            ctl_tx_pause_enable <= 0;
            ctl_tx_pause_req <= 0;
        end
        1: begin
            /*if (pause_hold_times_counter == 16) begin
                pause_hold_times_counter <= 0;
            end
            else begin
                pause_hold_times_counter <= pause_hold_times_counter + 1;
            end*/
            ctl_tx_pause_enable[8] <= 1;
            ctl_tx_pause_req[8] <= 1;
            ctl_tx_pause_refresh_timer8 <= 'h80;
            ctl_tx_pause_quanta8 <= 'h80;
        end
        endcase
    end
endmodule
