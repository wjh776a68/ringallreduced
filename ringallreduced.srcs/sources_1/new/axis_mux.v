module axis_mux21 (
    input clk,
    input transfer_type_switch,

    input [255:0] m1_axis_tdata,
    input [31:0]  m1_axis_tkeep,
    input        m1_axis_tvalid,
    input        m1_axis_tlast,
    output reg   m1_axis_tready,

    input [255:0] m2_axis_tdata,
    input [31:0]  m2_axis_tkeep,
    input        m2_axis_tvalid,
    input        m2_axis_tlast,
    output reg   m2_axis_tready,

    output reg [255:0] s_axis_tdata,
    output reg [31:0]  s_axis_tkeep,
    output reg        s_axis_tvalid,
    output reg        s_axis_tlast,
    input             s_axis_tready

);

    always @(posedge clk) begin
        if (transfer_type_switch) begin
            s_axis_tdata <= m2_axis_tdata;
            s_axis_tkeep <= m2_axis_tkeep;
            s_axis_tvalid <= m2_axis_tvalid;
            s_axis_tlast <= m2_axis_tlast;
            m2_axis_tready <= s_axis_tready;
            m1_axis_tready <= 0;
        end
        else begin
            s_axis_tdata <= m1_axis_tdata;
            s_axis_tkeep <= m1_axis_tkeep;
            s_axis_tvalid <= m1_axis_tvalid;
            s_axis_tlast <= m1_axis_tlast;
            m1_axis_tready <= s_axis_tready;
            m2_axis_tready <= 0;
        end
    end

endmodule

module axis_mux12 (
    input clk,
    input transfer_type_switch,


    input [255:0] m_axis_tdata,
    input [31:0]  m_axis_tkeep,
    input        m_axis_tvalid,
    input        m_axis_tlast,
    output reg   m_axis_tready,

    output reg [255:0] s1_axis_tdata,
    output reg [31:0]  s1_axis_tkeep,
    output reg        s1_axis_tvalid,
    output reg        s1_axis_tlast,
    input             s1_axis_tready,

    output reg [255:0] s2_axis_tdata,
    output reg [31:0]  s2_axis_tkeep,
    output reg        s2_axis_tvalid,
    output reg        s2_axis_tlast,
    input             s2_axis_tready

);

    always @(posedge clk) begin
        if (transfer_type_switch) begin
            s2_axis_tdata <= m_axis_tdata;
            s2_axis_tkeep <= m_axis_tkeep;
            s2_axis_tvalid <= m_axis_tvalid;
            s2_axis_tlast <= m_axis_tlast;
            m_axis_tready <= s2_axis_tready;
            //s1_axis_tready <= 0;
        end
        else begin
            s1_axis_tdata <= m_axis_tdata;
            s1_axis_tkeep <= m_axis_tkeep;
            s1_axis_tvalid <= m_axis_tvalid;
            s1_axis_tlast <= m_axis_tlast;
            m_axis_tready <= s1_axis_tready;
            //s2_axis_tready <= 0;
            
        end
    end

endmodule
