
module channel_ctrl_v5 #(
    parameter BASE_ADDRESS = 33'h0,
    parameter HIGH_ADDRESS = 33'h100
) (
    output reg [32:0]   AXI_ARADDR,//assigned
    output     [1:0]    AXI_ARBURST,//assigned
    output     [5:0]    AXI_ARID, //assigned
    output reg [3:0]    AXI_ARLEN,    //assigned
    input               AXI_ARREADY,
    output     [2:0]    AXI_ARSIZE,//assigned
    output reg          AXI_ARVALID,//assigned
    output reg [32:0]   AXI_AWADDR,//assigned
    output     [1:0]    AXI_AWBURST,//assigned
    output     [5:0]    AXI_AWID, //assigned
    output reg [3:0]    AXI_AWLEN,//assigned
    input               AXI_AWREADY,//assigned
    output     [2:0]    AXI_AWSIZE,//assigned
    output reg          AXI_AWVALID,//assigned
    input      [5:0]    AXI_BID,
    output              AXI_BREADY,//assigned
    input      [1:0]    AXI_BRESP,
    input               AXI_BVALID,
    input      [255:0]  AXI_RDATA,
    input      [5:0]    AXI_RID,
    input               AXI_RLAST,
    output              AXI_RREADY,//assigned
    input      [1:0]    AXI_RRESP,
    input               AXI_RVALID,
    output     [5:0]    AXI_WID, //assigned
    output     [255:0]  AXI_WDATA,//assigned
    output              AXI_WLAST,//assigned
    input               AXI_WREADY,//assigned
    output [31:0]       AXI_WSTRB,//assigned
    output              AXI_WVALID,//assigned
    output     [31:0]   AXI_WDATA_PARITY,
    
    /*output reg w_valid = 0,
    output reg b_valid = 0,
    
    output reg ar_valid = 0,
    output reg r_valid = 0,*/

    input FIFO_WRITE_tready,
    output     [255:0] FIFO_WRITE_tdata,
    output     FIFO_WRITE_tvalid,
    output  [31 : 0]   FIFO_WRITE_tkeep,
    output     FIFO_WRITE_tlast,
    //output read_next,
    
    //input FIFO_READ_prog_empty,
   
    input FIFO_READ_tvalid,
    input [255:0] FIFO_READ_tdata,
    output     FIFO_READ_tready,
    
    input rd_running,
    //input mode,
    input [31:0] rd_startaddress,
    input [31:0] rd_blockamount,
    
    input wr_running,
    //input mode,
    input [31:0] wr_startaddress,
    input [31:0] wr_blockamount,
    
    output reg busy,
    
    input clk
    
    );
    //always @ (*) begin
    assign    AXI_ARBURST = 1;
    assign    AXI_AWBURST = 1;
    assign    AXI_ARSIZE  = 5;
    assign    AXI_AWSIZE  = 5;
    //end
    assign AXI_ARID = 0;
    assign AXI_AWID = 0;
    assign AXI_RID = 0;
    //assign AXI_WID = 0;
    assign AXI_BREADY = 1;
    //assign AXI_RREADY = FIFO_WRITE_tready;
    assign AXI_WSTRB = 32'hffffffff;
    assign AXI_WDATA_PARITY =  
    {{^{AXI_WDATA[255 : 248]}},
    {^{AXI_WDATA[247 : 240]}},
    {^{AXI_WDATA[239 : 232]}},
    {^{AXI_WDATA[231 : 224]}},
    {^{AXI_WDATA[223 : 216]}},
    {^{AXI_WDATA[215 : 208]}},
    {^{AXI_WDATA[207 : 200]}},
    {^{AXI_WDATA[199 : 192]}},
    {^{AXI_WDATA[191 : 184]}},
    {^{AXI_WDATA[183 : 176]}},
    {^{AXI_WDATA[175 : 168]}},
    {^{AXI_WDATA[167 : 160]}},
    {^{AXI_WDATA[159 : 152]}},
    {^{AXI_WDATA[151 : 144]}},
    {^{AXI_WDATA[143 : 136]}},
    {^{AXI_WDATA[135 : 128]}},
    {^{AXI_WDATA[127 : 120]}},
    {^{AXI_WDATA[119 : 112]}},
    {^{AXI_WDATA[111 : 104]}},
    {^{AXI_WDATA[103 : 96] }},
    {^{AXI_WDATA[95 : 88]  }},
    {^{AXI_WDATA[87 : 80]  }},
    {^{AXI_WDATA[79 : 72]  }},
    {^{AXI_WDATA[71 : 64]  }},
    {^{AXI_WDATA[63 : 56]  }},
    {^{AXI_WDATA[55 : 48]  }},
    {^{AXI_WDATA[47 : 40]  }},
    {^{AXI_WDATA[39 : 32]  }},
    {^{AXI_WDATA[31 : 24]  }},
    {^{AXI_WDATA[23 : 16]  }},
    {^{AXI_WDATA[15 : 8]   }},
    {^{AXI_WDATA[7 : 0]    }} };

    reg [2:0] FSM = 0, FSM_next = 0;
    reg write_start = 0, writeaddr_done = 0, writedata_done = 0;
    reg read_start = 0, readaddr_done = 0, readdata_done = 0;

    reg [31:0] read_startaddress;
    reg [31:0] read_blockamount;

    reg [31:0] write_startaddress;
    reg [31:0] write_blockamount;

    always @ (posedge clk) begin
        FSM <= FSM_next;
    end

    always @ (*) begin
        case (FSM)
        0: begin
            case ({rd_running, wr_running})
            2'b00: begin
                FSM_next = 0;
            end
            2'b01: begin
                FSM_next = 1;
            end
            2'b10: begin
                FSM_next = 3;
            end
            2'b11: begin
                FSM_next = 0;
            end
            endcase
        end
        1: begin
            FSM_next = 2;
        end
        2: begin
            if (writedata_done & writeaddr_done) begin
                FSM_next = 5;
            end
        end
        3: begin
            FSM_next = 4;
        end
        4: begin
            if (readaddr_done & readdata_done) begin
                FSM_next = 5;
            end
        end
        5: begin
            FSM_next = 0;
        end
        endcase
    end

    always @ (posedge clk) begin
        case (FSM_next)
        0: begin
            busy <= 0;
        end
        1: begin
            busy <= 1;
            write_start <= 1;
            write_startaddress <= wr_startaddress;
            write_blockamount <= wr_blockamount;
        end
        2: begin
            //write_start <= 0;
        end
        3: begin
            busy <= 1;
            read_start <= 1;
            read_startaddress <= rd_startaddress;
            read_blockamount <= rd_blockamount;
        end
        4: begin
            //read_start <= 0;
        end
        5: begin
            write_start <= 0;
            read_start <= 0;
        end
        endcase
    end
    
    reg [31 : 0] readaddr_count = 0;
    reg [31 : 0] readdata_count = 0;

    reg [2:0] readdata_FSM = 0, readdata_FSM_next = 0;
    always @ (posedge clk) begin
        readdata_FSM <= readdata_FSM_next;
    end

    always @ (*) begin
        case (readdata_FSM)
        0: begin
            if (read_start) begin
                readdata_FSM_next = 1;
            end
        end
        1: begin
            if (AXI_RVALID && AXI_RREADY && readdata_count == read_blockamount - 1) begin
                readdata_FSM_next = 2;
            end
        end
        2: begin
            readdata_FSM_next = 3;
        end
        3: begin
            if (~read_start) begin
                readdata_FSM_next = 0;
            end
        end
        endcase
    end

    always @ (posedge clk) begin
        case (readdata_FSM_next)
        0: begin
            readdata_done <= 0;
            if (AXI_RVALID & AXI_RREADY) begin
                readdata_count <= readdata_count + 1;
            end
        end
        1: begin
            if (AXI_RVALID & AXI_RREADY) begin
                readdata_count <= readdata_count + 1;
            end
        end
        2: begin
            readdata_done <= 1;
            /* if (AXI_RVALID & AXI_RREADY) begin
                readdata_count <= 1;
            end
            else begin */
            readdata_count <= 0;
            //end
        end
        3: begin
            if (AXI_RVALID & AXI_RREADY) begin
                readdata_count <= readdata_count + 1;
            end
        end
        endcase
    end

    //always @ (*) begin
    assign    FIFO_WRITE_tdata = AXI_RDATA;
    assign    FIFO_WRITE_tvalid = AXI_RVALID;
    assign    FIFO_WRITE_tkeep  = {32{1'b1}};
    assign    FIFO_WRITE_tlast  = (AXI_RVALID && AXI_RREADY && readdata_count == read_blockamount - 1);
    assign    AXI_RREADY = FIFO_WRITE_tready;
    //end

    reg [2:0] readaddr_FSM = 0, readaddr_FSM_next = 0;
    always @ (posedge clk) begin
        readaddr_FSM <= readaddr_FSM_next;
    end

    always @ (*) begin
        case (readaddr_FSM)
        0: begin
            if (read_start) begin
                if (16 >= read_blockamount) begin
                    readaddr_FSM_next = 2;
                end
                else begin
                    readaddr_FSM_next = 1;
                end
            end
        end
        1: begin
            if (AXI_ARVALID && AXI_ARREADY && readaddr_count + 16 >= read_blockamount) begin
                readaddr_FSM_next = 2;
            end
        end
        2: begin
            if (AXI_ARVALID & AXI_ARREADY) begin
                readaddr_FSM_next = 3;
            end
        end
        3: begin
            if (~read_start) begin
                readaddr_FSM_next = 0;
            end
        end
        endcase
    end


    always @ (posedge clk) begin
        case (readaddr_FSM_next)
        0: begin
            readaddr_done <= 0;
            AXI_ARADDR <= 0;
            AXI_ARLEN <= 0;
            AXI_ARVALID <= 0;
            readaddr_count <= 0;
        end
        1: begin
            AXI_ARVALID <= 1;
            AXI_ARLEN <= 15;
            AXI_ARADDR <= read_startaddress + readaddr_count;
            if (AXI_ARVALID & AXI_ARREADY) begin
                readaddr_count <= readaddr_count + 16;
            end
        end
        2: begin
            AXI_ARVALID <= 1;
            AXI_ARLEN <= read_blockamount[3 : 0] - 1;
            AXI_ARADDR <= read_startaddress + readaddr_count;
            /* if (AXI_ARVALID & AXI_ARREADY) begin
                //readaddr_count <= readaddr_count + 16;
            end */
        end
        3: begin
            AXI_ARVALID <= 0;
            readaddr_done <= 1;
        end
        endcase
    end

    reg [2:0] writeaddr_FSM = 0, writeaddr_FSM_next = 0;
    reg [2:0] writedata_FSM = 0, writedata_FSM_next = 0;
    reg [31 : 0] writedata_count = 0;
    reg [31 : 0] writeaddr_count = 0;
    always @ (posedge clk) begin
        writeaddr_FSM <= writeaddr_FSM_next;
        writedata_FSM <= writedata_FSM_next;
    end

    always @ (*) begin
        case (writeaddr_FSM)
        0: begin
            if (write_start) begin
                if (16 >= write_blockamount) begin
                    writeaddr_FSM_next = 2;
                end
                else begin
                    writeaddr_FSM_next = 1;
                end
            end
        end
        1: begin
            if (AXI_AWREADY && AXI_AWVALID && writeaddr_count + 16 >= write_blockamount) begin
                writeaddr_FSM_next = 2;
            end
        end
        2: begin
            if (AXI_AWREADY & AXI_AWVALID) begin
                writeaddr_FSM_next = 3;
            end
        end
        3: begin
            if (~write_start) begin
                writeaddr_FSM_next = 0;
            end
        end
        endcase

        case (writedata_FSM)
        0: begin
            if (write_start) begin
                writedata_FSM_next = 1;
            end
        end
        1: begin
            if (FIFO_READ_tvalid && FIFO_READ_tready && writedata_count == write_blockamount - 1) begin
                writedata_FSM_next = 2;
            end
        end
        2: begin
            writedata_FSM_next = 3;
        end
        3: begin
            if (~write_start) begin
                writedata_FSM_next = 0;
            end
        end
        endcase
    end
    
    
    
    always @ (posedge clk) begin
        case (writedata_FSM_next)
        0: begin
            writedata_done <= 0;
            if (FIFO_READ_tvalid & FIFO_READ_tready) begin
                writedata_count <= writedata_count + 1;
            end
        end
        1: begin
            
            if (FIFO_READ_tvalid & FIFO_READ_tready) begin
                writedata_count <= writedata_count + 1;
            end
        end
        2: begin
            writedata_done <= 1;
            
            /* if (FIFO_READ_tvalid & FIFO_READ_tready) begin
                writedata_count <= 1;
            end
            else begin */
            writedata_count <= 0;
            //end
        end
        3: begin
            if (FIFO_READ_tvalid & FIFO_READ_tready) begin
                writedata_count <= writedata_count + 1;
            end
        end
        endcase

        case (writeaddr_FSM_next)
        0: begin
            writeaddr_done <= 0;
            writeaddr_count <= 0;
            AXI_AWADDR <= 0;
            AXI_AWLEN <= 0;
            AXI_AWVALID <= 0;
        end
        1: begin
            AXI_AWVALID <= 1;
            AXI_AWADDR <= write_startaddress + writeaddr_count;
            AXI_AWLEN <= 15;
            if (AXI_AWREADY & AXI_AWVALID) begin
                writeaddr_count <= writeaddr_count + 16;
            end
        end
        2: begin
            AXI_AWVALID <= 1;
            AXI_AWADDR <= write_startaddress + writeaddr_count;
            AXI_AWLEN <= write_blockamount[3 : 0] - 1;
            /* if (AXI_AWREADY & AXI_AWVALID) begin
                //writeaddr_count <= writeaddr_count + 16;
            end */
        end
        3: begin
            AXI_AWVALID <= 0;
            writeaddr_done <= 1;
        end
        endcase
    end

    //always @ (*) begin
    assign    FIFO_READ_tready = AXI_WREADY;// & (writedata_FSM_next == 1);
    assign    AXI_WVALID = FIFO_READ_tvalid;// & (writedata_FSM_next == 1);
    assign    AXI_WDATA = FIFO_READ_tdata;
    assign    AXI_WLAST = (writedata_count[3 : 0] == 15) || (writedata_count == write_blockamount - 1);
    //end
endmodule