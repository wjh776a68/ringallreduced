`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/18/2022 12:16:17 AM
// Design Name: 
// Module Name: dealring
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module dealring#(
   parameter STATUS_0 = 13'b0000000000001,
   parameter STATUS_1 = 13'b0000000000010,
   parameter STATUS_2 = 13'b0000000000100,
   parameter STATUS_3 = 13'b0000000001000,
   parameter STATUS_4 = 13'b0000000010000,
   parameter STATUS_5 = 13'b0000000100000,
   parameter STATUS_6 =  13'b0000001000000,
   parameter STATUS_7 =  13'b0000010000000,
   parameter STATUS_8 =  13'b0000100000000,
   parameter STATUS_9 =  13'b0001000000000,
   parameter STATUS_10 = 13'b0010000000000,
   parameter STATUS_11 = 13'b0100000000000,
   parameter STATUS_12 = 13'b1000000000000
)(
    input startring,
    
    input [31:0] perblocksize,
    input [31:0] curnode,
    input [31:0] totalnode,
    
    output reg [31:0] bram_addr,
    input [63:0] bram_in,
    
    output reg bram_wea,
    output reg [63:0] bram_out,
    
    
    output reg wr_en,
    output reg [63:0] din,
    input full,
    
    output reg rd_en,
    input [63:0] dout,
    input empty,
    
    output reg ringfinished,
    
    input clk,
    input reset,
    
    output reg [12:0] RUN,
    //output reg [13:0] next_RUN,
    
    input [31:0] maxaddr
    );
    reg [31:0] tosendmaxaddr;
    reg [31:0] torecvmaxaddr;
    reg [31:0] times;
    reg [31:0] tosend;
    reg [31:0] torecv;
    reg isgather;
    
    wire [31:0] bram_out_low;
    wire [31:0] bram_out_high;
    
    /*always@(posedge clk) begin
        if(reset) begin
            RUN <= STATUS_0;
        end
        else begin
            RUN <= next_RUN;
        end
    end*/
    
    always@(posedge clk) begin
        if(reset) begin
            RUN <= STATUS_0;
        end
        else begin
            case(RUN)
            STATUS_0: begin
                ringfinished <= 0;
                if(startring==1) begin
                    isgather <= 0;
                    RUN <= STATUS_1;
                    times <= 1;
                    
                    tosend <= curnode + 1; //比预计值多1，后面会减
                    
                    //if(curnode==0) begin //
                    //    torecv <= totalnode;
                    //end
                    //else begin
                        torecv <= curnode; //比预计值多1，后面会减
                   // end
                end
                else begin
                    RUN <= STATUS_0;
                end
            end
            STATUS_1: begin
                if(times <= totalnode) begin
                    times <=  times + 1;
                    if(tosend == 0) begin
                        tosend <= totalnode - 1;
                    end
                    else begin
                        tosend <= tosend - 1;
                    end
                    if(torecv == 0) begin
                        torecv <= totalnode - 1;
                    end
                    else begin
                        torecv <= torecv - 1;
                    end
                    RUN <= STATUS_2;
                end
                else begin //gather
                    if(isgather==1'b0) begin
                        isgather <= 1'b1;
                        times <= 1;
                        if(curnode + 1 == totalnode) begin
                            tosend <= 1;//totalnode + 2;(curnode + 2)%totalnode
                        end
                        else begin
                            tosend <= curnode + 2;
                        end
                        
                        torecv <= curnode + 1;
                        RUN <= STATUS_1;
                    end
                    else begin
                        ringfinished <= 1;
                        RUN <= STATUS_0;
                    end
                end
            end
            STATUS_2: begin
                bram_addr <= tosend * perblocksize;
                if(maxaddr < tosend * perblocksize + perblocksize)
                    tosendmaxaddr <= maxaddr;
                else
                    tosendmaxaddr <= tosend * perblocksize + perblocksize;
                RUN <= STATUS_3;
            end
            STATUS_3: begin //发送
                if(full==0) begin
                    if(bram_addr == tosendmaxaddr) begin
                        wr_en <= 0;
                        RUN <= STATUS_4;
                    end
                    else begin
                        wr_en <= 1;
                        din <= bram_in;
                        bram_addr <= bram_addr + 1;
                        RUN <= STATUS_3;
                    end
                end
                else begin
                    wr_en <= 0;
                    RUN <= STATUS_3;
                end
            end
            STATUS_4: begin //接收
                //if(empty!=0) begin
                    bram_addr <= torecv * perblocksize - 1;
                    
                    if(maxaddr < torecv * perblocksize + perblocksize)
                        torecvmaxaddr <= maxaddr;
                    else
                        torecvmaxaddr <= torecv * perblocksize + perblocksize;
                    //rd_en <= 1;
                    RUN <= STATUS_5;
                //end
                //else begin
                //    next_RUN <= STATUS_4;
                //end
            end
            STATUS_5: begin
                bram_wea <= 0;
                if(empty==0) begin
                    bram_addr <= bram_addr + 1;
                    rd_en <= 1;
                    
                    RUN <= STATUS_6;
                    
                end
                else if(bram_addr + 1==torecvmaxaddr) begin
                    rd_en <= 0;
                    RUN <= STATUS_1;
                end
                else begin
                    rd_en <= 0;
                    RUN <= STATUS_5;
                end
            end
            STATUS_6: begin
                rd_en <= 0;
                if(isgather==0) begin
                    RUN <= STATUS_7; //fifo取出数据 floatingpoint时延为1
                end
                else begin //直接替换
                    bram_out <= dout;
                    bram_wea <= 1;
                    RUN <= STATUS_5;
                end
            end
            STATUS_7: begin //floating point 计算完
                bram_out <= {bram_out_high, bram_out_low};
                bram_wea <= 1;
                /*if(bram_addr + 1 == torecvmaxaddr) begin
                    next_RUN <= STATUS_8;
                end
                else begin*/
                    //bram_addr <= bram_addr + 1;
                    RUN <= STATUS_5;
                //end
                
            end
            
            endcase
        end
        
    end
    
    floating_point doubleadd_inst_low (
        .s_axis_a_tdata(dout[31:0]),
        .s_axis_a_tvalid(1),
        
        .s_axis_b_tdata(bram_in[31:0]),
        .s_axis_b_tvalid(1),
        
        .aclk(clk),
        
        .m_axis_result_tdata(bram_out_low)/*,
        .m_axis_result_valid()*/
    );
    
    floating_point doubleadd_inst_high (
        .s_axis_a_tdata(dout[63:32]),
        .s_axis_a_tvalid(1),
        
        .s_axis_b_tdata(bram_in[63:32]),
        .s_axis_b_tvalid(1),
        
        .aclk(clk),
        
        .m_axis_result_tdata(bram_out_high)/*,
        .m_axis_result_valid()*/
    );
    
endmodule




