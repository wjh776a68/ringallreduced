`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/14/2022 12:48:18 PM
// Design Name: 
// Module Name: recv_ctrl
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module recv_ctrl(
    input clk,
    input reset,
    
    output reg [7 : 0]  ctl_rx_min_packet_len_0,
    output reg          ctl_rx_process_lfi_0, 
    output reg          ctl_rx_enable_0,
    output reg          ctl_rx_force_resync_0, 
    output reg          ctl_rx_ignore_fcs_0, 
    output reg [14 : 0] ctl_rx_max_packet_len_0, 
    output reg          ctl_rx_check_preamble_0, 
    output reg          ctl_rx_check_sfd_0, 
    output reg          ctl_rx_custom_preamble_enable_0, 
    output reg          ctl_rx_delete_fcs_0, 
    output reg          ctl_rx_test_pattern_0
    
    );
    
    always @ (*) begin
        
        ctl_rx_min_packet_len_0 <= 8'd64; //64B?
        ctl_rx_process_lfi_0 <= 0;
        ctl_rx_enable_0 <= 1;
        ctl_rx_force_resync_0 <= 0;
        ctl_rx_ignore_fcs_0 <= 0;
        ctl_rx_max_packet_len_0 <= 15'd16383;
        ctl_rx_check_preamble_0 <= 0;
        ctl_rx_check_sfd_0 <= 0;
        ctl_rx_custom_preamble_enable_0 <= 0;
        ctl_rx_delete_fcs_0 <= 1;
        ctl_rx_test_pattern_0 <= 1;
        
    end
    
    
endmodule
