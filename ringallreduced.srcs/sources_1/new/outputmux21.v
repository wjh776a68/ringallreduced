`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/21/2022 05:05:06 PM
// Design Name: 
// Module Name: inputmux12
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module outputmux21(
    input clk,

    input [63:0] axis_A_tdata,
    output axis_A_tready,
    input axis_A_tlast,
    input axis_A_tvalid,
    input [7:0] axis_A_tkeep,
    
    input [63:0] axis_B_tdata,
    output axis_B_tready,
    input axis_B_tlast,
    input axis_B_tvalid,
    input [7:0] axis_B_tkeep,
    
    output  [63:0] axis_o_tdata,
    input  axis_o_tready,
    output  axis_o_tlast,
    output  axis_o_tvalid,
    output  [7:0] axis_o_tkeep,
    
    input isB //0 is A 1 is B
    );
    
    assign axis_A_tready = (isB==0)? axis_o_tready : 0;
    assign axis_B_tready = (isB==1)? axis_o_tready : 0;
    assign axis_o_tdata = (isB==0)? axis_A_tdata : axis_B_tdata;
    assign axis_o_tlast = (isB==0)? axis_A_tlast : axis_B_tlast;
    assign axis_o_tvalid = (isB==0)? axis_A_tvalid : axis_B_tvalid;
    assign axis_o_tkeep = (isB==0)? axis_A_tkeep : axis_B_tkeep;
    
endmodule
