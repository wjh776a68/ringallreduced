`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/14/2022 10:48:06 AM
// Design Name: 
// Module Name: axis_traffic_gen
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module axis_traffic_gen # (
    parameter DATA_WIDTH = 256,
    parameter KEEP_WIDTH = 32,
    parameter packet_length = 100,
    localparam packet_counter = packet_length - 1
    //parameter repeatly = "true"
) (
    input                clk,
    input                reset,
    
    input                s_axis_tready,
    output reg           s_axis_tvalid,
    output reg [DATA_WIDTH - 1 : 0] s_axis_tdata,
    output reg [KEEP_WIDTH - 1 : 0]  s_axis_tkeep,
    output reg           s_axis_tlast,
    output reg [0 : 0]   s_axis_tuser
    
    );
    
    always @ (*) begin
        s_axis_tuser <= 0; //asserted while tlast high to abort current transmission (served as a bad packet)
    end
    
    reg [9:0] counter = 0;
    
    always @ (posedge clk, posedge reset) begin
        if (reset) begin
            counter <= 0;
            s_axis_tvalid <= 0;
            
        end
        else if (s_axis_tready) begin
            if (counter == packet_counter) begin
                counter <= 0;
            end
            else begin
                counter <= counter + 1;
            end
            
            if (counter == packet_counter) begin
                s_axis_tlast <= 1;
            end
            else begin
                s_axis_tlast <= 0;
            end
            
            s_axis_tvalid <= 1;
            s_axis_tdata <= counter;//{{54{1'b0}}, counter, {54{1'b0}}, counter, {54{1'b0}}, counter, {54{1'b0}}, counter};
            s_axis_tkeep <= {KEEP_WIDTH{1'b1}};
        end
    end
    
    
endmodule
