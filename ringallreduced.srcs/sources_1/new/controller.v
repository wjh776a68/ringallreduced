`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/21/2022 05:49:28 PM
// Design Name: 
// Module Name: controller
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module controller(
    input A_wea,
    input [3:0] A_data, //3_0 eth
    input B_wea,
    input [3:0] B_data, //7_4 ring
    
    input [7:0] statusdata_in,
    output statusdata_wea,
    output [7:0] statusdata_out,
    
    output [3:0] statusdata_in_A,
    output [3:0] statusdata_in_B
    );
    
    assign statusdata_wea = A_wea || B_wea;
    assign statusdata_out[7:4] = (B_wea == 1) ? B_data : statusdata_in[7:4];
    assign statusdata_out[3:0] = (A_wea == 1) ? A_data : statusdata_in[3:0];
    assign statusdata_in_A = statusdata_in[3:0];
    assign statusdata_in_B = statusdata_in[7:4];
endmodule
