`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/14/2022 08:10:54 PM
// Design Name: 
// Module Name: axis_combiner_64to256
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module axis_combiner_64to256(
    input                clk,
    input                reset,
    
    output reg           m1_axis_tready,
    input      [63 : 0]  m1_axis_tdata,
    input                m1_axis_tvalid,
    input      [7 : 0]   m1_axis_tkeep,
    input      [0 : 0]   m1_axis_tuser,
    input                m1_axis_tlast,
    
    output reg           m2_axis_tready,
    input      [63 : 0]  m2_axis_tdata,
    input                m2_axis_tvalid,
    input      [7 : 0]   m2_axis_tkeep,
    input      [0 : 0]   m2_axis_tuser,
    input                m2_axis_tlast,
    
    output reg           m3_axis_tready,
    input      [63 : 0]  m3_axis_tdata,
    input                m3_axis_tvalid,
    input      [7 : 0]   m3_axis_tkeep,
    input      [0 : 0]   m3_axis_tuser,
    input                m3_axis_tlast,
    
    output reg           m4_axis_tready,
    input      [63 : 0]  m4_axis_tdata,
    input                m4_axis_tvalid,
    input      [7 : 0]   m4_axis_tkeep,
    input      [0 : 0]   m4_axis_tuser,
    input                m4_axis_tlast,
    
    //input                s_axis_tready,
    output reg [255 : 0] s_axis_tdata,
    output reg           s_axis_tvalid,
    output reg [31 : 0]  s_axis_tkeep,
    output reg [0 : 0]   s_axis_tuser,
    output reg           s_axis_tlast
    );
    
    always @ (*) begin
        m1_axis_tready <= 1;
        m2_axis_tready <= 1;
        m3_axis_tready <= 1;
        m4_axis_tready <= 1;
        s_axis_tdata  <= {m4_axis_tdata, m3_axis_tdata, m2_axis_tdata, m1_axis_tdata};
        //s_axis_tvalid <= m4_axis_tvalid | m3_axis_tdata, m2_axis_tdata, m1_axis_tdata
    end
    
    
endmodule
