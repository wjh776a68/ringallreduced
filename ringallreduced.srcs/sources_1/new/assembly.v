module assembly # (
    parameter sim_id = 'h123456789abcdef
) (
    //input clk,
    input reset,

    input CLK_IN1_D_clk_n,
    input CLK_IN1_D_clk_p,
    //wire clk;
    input gt_ref_clk_0_clk_n,
    input gt_ref_clk_0_clk_p,
    input gt_ref_clk_1_clk_n,
    input gt_ref_clk_1_clk_p,
    input [3:0] gt_serial_port_0_grx_n,
    input [3:0] gt_serial_port_0_grx_p,
    output [3:0] gt_serial_port_0_gtx_n,
    output [3:0] gt_serial_port_0_gtx_p,
    input [3:0] gt_serial_port_1_grx_n,
    input [3:0] gt_serial_port_1_grx_p,
    output [3:0] gt_serial_port_1_gtx_n,
    output [3:0] gt_serial_port_1_gtx_p,
    output stat_rx_status_sfp0,
    output stat_rx_status_sfp1
    //wire reset;
    

);
    wire clk;
    clk_wiz_assembly clk_wiz_assembly_inst (
        .clk_in1_n(CLK_IN1_D_clk_n),
        .clk_in1_p(CLK_IN1_D_clk_p),
        .reset(reset),
        .clk_out1(clk)
    );

    wire user_start_io = 1;
    wire [31:0] sync_bytes_amount = 20;

    wire hw_link_status = stat_rx_status_sfp0 & stat_rx_status_sfp1;

    wire stat_system_on;
    wire stat_finish;

    wire init_done;
    wire init_start;
    wire rar_done;
    wire rar_start;
    //wire recv_done;
    //wire send_done;
    //wire recv_start;
    //wire send_start;
    wire transfer_type_switch;
    wire HBM_STATUS_ready;
    wire [31 : 0] sync_bytes_amount_reg;

    global_controller global_controller_inst(
        clk,

        sync_bytes_amount,
        HBM_STATUS_ready,
        user_start_io,
        hw_link_status,

        stat_system_on,
        stat_finish,

        init_done,
        init_start,
        rar_done,
        rar_start,

        sync_bytes_amount_reg,

        transfer_type_switch

    );

    wire [255 : 0] init_s_axis_tdata;
    wire [31 : 0]  init_s_axis_tkeep;
    wire          init_s_axis_tvalid;
    wire          init_s_axis_tlast;
    wire          init_s_axis_tready;

    wire [255 : 0] init_m_axis_tdata;
    wire [31 : 0]  init_m_axis_tkeep;
    wire          init_m_axis_tvalid;
    wire          init_m_axis_tlast;
    wire          init_m_axis_tread;

    wire [15 : 0] ring_device_amount, ring_device_id;

    wire [97 : 0] current_min_id;

    init # (
        .sim_id(sim_id)
    ) init_inst(
        clk,
        init_start,
        init_done,
        ring_device_amount,
        ring_device_id,
        
        current_min_id,

        init_s_axis_tdata, // communicate device ids to setup ring order
        init_s_axis_tkeep,
        init_s_axis_tvalid,
        init_s_axis_tlast,
        init_s_axis_tready,

        init_m_axis_tdata,
        init_m_axis_tkeep,
        init_m_axis_tvalid,
        init_m_axis_tlast,
        init_m_axis_tready
    );

    wire [255:0] sfp_s_axis_tdata; 
    wire [31:0]  sfp_s_axis_tkeep;
    wire        sfp_s_axis_tvalid;
    wire        sfp_s_axis_tlast;
    wire        sfp_s_axis_tready;

    wire [255:0] sfp_m_axis_tdata; 
    wire [31:0]  sfp_m_axis_tkeep;
    wire        sfp_m_axis_tvalid;
    wire        sfp_m_axis_tlast;
    wire        sfp_m_axis_tready;

    wire [255:0] send_s_axis_tdata; 
    wire [31:0]  send_s_axis_tkeep;
    wire        send_s_axis_tvalid;
    wire        send_s_axis_tlast;
    wire        send_s_axis_tready;

    wire [255:0] recv_m_axis_tdata; 
    wire [31:0]  recv_m_axis_tkeep;
    wire        recv_m_axis_tvalid;
    wire        recv_m_axis_tlast;
    wire        recv_m_axis_tready;


    rar_axis_mux21 axis_mux21_inst (
        clk,

        init_s_axis_tdata,
        init_s_axis_tready,
        init_s_axis_tlast,
        init_s_axis_tvalid,
        init_s_axis_tkeep,
        

        send_s_axis_tdata,
        send_s_axis_tready,
        send_s_axis_tlast,
        send_s_axis_tvalid,
        send_s_axis_tkeep,

        sfp_s_axis_tdata,
        sfp_s_axis_tready,
        sfp_s_axis_tlast,
        sfp_s_axis_tvalid,
        sfp_s_axis_tkeep,
        
        transfer_type_switch
    );

    rar_axis_mux12 axis_mux12_inst (
        clk,

        sfp_m_axis_tdata,
        sfp_m_axis_tready,
        sfp_m_axis_tlast,
        sfp_m_axis_tvalid,
        sfp_m_axis_tkeep,

        init_m_axis_tdata,
        init_m_axis_tready,
        init_m_axis_tlast,
        init_m_axis_tvalid,
        init_m_axis_tkeep,

        recv_m_axis_tdata,
        recv_m_axis_tready,
        recv_m_axis_tlast,
        recv_m_axis_tvalid,
        recv_m_axis_tkeep,
        
        
        transfer_type_switch

    );


    
    // wire [63:0]sfp_m_axis_tdata;
    // wire [7:0]sfp_m_axis_tkeep;
    // wire sfp_m_axis_tlast;
    // wire sfp_m_axis_tready;
    wire [0:0] sfp_m_axis_tuser;
    // wire sfp_m_axis_tvalid;
    // wire [63:0]sfp_s_axis_tdata;
    // wire [7:0]sfp_s_axis_tkeep;
    // wire sfp_s_axis_tlast;
    // wire sfp_s_axis_tready;
    wire [0:0] sfp_s_axis_tuser = 0;
    // wire sfp_s_axis_tvalid;
    
    design_qsfp_40G_wrapper design_qsfp_40G_wrapper_inst (
        
        clk,
        clk,
        gt_ref_clk_0_clk_n,
        gt_ref_clk_0_clk_p,
        gt_ref_clk_1_clk_n,
        gt_ref_clk_1_clk_p,
        gt_serial_port_0_grx_n,
        gt_serial_port_0_grx_p,
        gt_serial_port_0_gtx_n,
        gt_serial_port_0_gtx_p,
        gt_serial_port_1_grx_n,
        gt_serial_port_1_grx_p,
        gt_serial_port_1_gtx_n,
        gt_serial_port_1_gtx_p,
        reset,
        sfp_m_axis_tdata,
        sfp_m_axis_tkeep,
        sfp_m_axis_tlast,
        sfp_m_axis_tready,
        sfp_m_axis_tuser,
        sfp_m_axis_tvalid,
        sfp_s_axis_tdata,
        sfp_s_axis_tkeep,
        sfp_s_axis_tlast,
        sfp_s_axis_tready,
        sfp_s_axis_tuser,
        sfp_s_axis_tvalid,
        stat_rx_status_sfp0,
        stat_rx_status_sfp1
    );
    /* design_qsfp_25G_wrapper design_qsfp_25G_wrapper_inst (
        CLK_IN1_D_clk_n,
        CLK_IN1_D_clk_p,
        clk,
        gt_ref_clk_clk_n,
        gt_ref_clk_clk_p,
        gt_serial_port_grx_n,
        gt_serial_port_grx_p,
        gt_serial_port_gtx_n,
        gt_serial_port_gtx_p,
        reset,
        sfp_m_axis_tdata,
        sfp_m_axis_tkeep,
        sfp_m_axis_tlast,
        sfp_m_axis_tready,
        sfp_m_axis_tuser,
        sfp_m_axis_tvalid,
        sfp_s_axis_tdata,
        sfp_s_axis_tkeep,
        sfp_s_axis_tlast,
        sfp_s_axis_tready,
        sfp_s_axis_tuser,
        sfp_s_axis_tvalid,
        stat_rx_status_sfp0,
        stat_rx_status_sfp1
    ); */

    wire [1 : 0] mux_select_0;
    wire [1 : 0] mux_select_1;
    wire [1 : 0] mux_select_2;

    wire m_ctrl_ports_0_busy;
    wire m_ctrl_ports_1_busy;
    wire m_ctrl_ports_2_busy;
    wire [31 : 0] m_ctrl_ports_0_rd_blockamount ;
    wire m_ctrl_ports_0_rd_run         ;
    wire [31 : 0] m_ctrl_ports_0_rd_startaddress;
    wire [31 : 0] m_ctrl_ports_0_wr_blockamount ;
    wire m_ctrl_ports_0_wr_run         ;
    wire [31 : 0] m_ctrl_ports_0_wr_startaddress;
    wire [31 : 0] m_ctrl_ports_1_rd_blockamount ;
    wire m_ctrl_ports_1_rd_run         ;
    wire [31 : 0] m_ctrl_ports_1_rd_startaddress;
    wire [31 : 0] m_ctrl_ports_1_wr_blockamount ;
    wire m_ctrl_ports_1_wr_run         ;
    wire [31 : 0] m_ctrl_ports_1_wr_startaddress;
    wire [31 : 0] m_ctrl_ports_2_rd_blockamount ;
    wire m_ctrl_ports_2_rd_run         ;
    wire [31 : 0] m_ctrl_ports_2_rd_startaddress;
    wire [31 : 0] m_ctrl_ports_2_wr_blockamount ;
    wire m_ctrl_ports_2_wr_run         ;
    wire [31 : 0] m_ctrl_ports_2_wr_startaddress;


    /* wire fifo_read_m_0_empty   ;
    wire [63 : 0] fifo_read_m_0_rd_data ;
    wire fifo_read_m_0_rd_en   ;
    wire fifo_read_m_1_empty   ;
    wire [63 : 0] fifo_read_m_1_rd_data ;
    wire fifo_read_m_1_rd_en   ;
    wire fifo_read_m_2_empty   ;
    wire [63 : 0] fifo_read_m_2_rd_data ;
    wire fifo_read_m_2_rd_en   ;
    wire fifo_write_m_0_full   ;
    wire [63 : 0] fifo_write_m_0_wr_data;
    wire fifo_write_m_0_wr_en  ;
    wire fifo_write_m_1_full   ;
    wire [63 : 0] fifo_write_m_1_wr_data;
    wire fifo_write_m_1_wr_en  ;
    wire fifo_write_m_2_full   ;
    wire [63 : 0] fifo_write_m_2_wr_data;
    wire fifo_write_m_2_wr_en  ; */

    wire [255 : 0] HBM_READ_m_0_tdata;
    wire HBM_READ_m_0_tready;
    wire HBM_READ_m_0_tvalid;
    wire [255 : 0] HBM_READ_m_1_tdata;
    wire HBM_READ_m_1_tready;
    wire HBM_READ_m_1_tvalid;
    wire [255 : 0] HBM_READ_m_2_tdata;
    wire HBM_READ_m_2_tready;
    wire HBM_READ_m_2_tvalid;

    wire [255 : 0] HBM_WRITE_m_0_tdata;
    wire HBM_WRITE_m_0_tready;
    wire HBM_WRITE_m_0_tvalid;
    wire [255 : 0] HBM_WRITE_m_1_tdata;
    wire HBM_WRITE_m_1_tready;
    wire HBM_WRITE_m_1_tvalid;
    wire [255 : 0] HBM_WRITE_m_2_tdata;
    wire HBM_WRITE_m_2_tready;
    wire HBM_WRITE_m_2_tvalid;

    wire skip_calculate_a, skip_calculate_b;
    wire [255 : 0] fadd_s_axis_a_tdata;
    wire fadd_s_axis_a_tready;
    wire fadd_s_axis_a_tvalid;

    wire [255 : 0] fadd_final_m_axis_tdata;
    wire fadd_final_m_axis_tready;
    wire fadd_final_m_axis_tvalid;

    wire [255 : 0] rar_fadd_wrdata;
    wire rar_fadd_wr_en;
    wire rar_fadd_full;

    wire saveto_hbm_part_mux, fetchfrom_hbm_part_mux;

    wire [255 : 0] fifo_write_m_1_rd_data;
    wire          fifo_write_m_1_rd_en;
    wire          fifo_write_m_1_empty;

    wire [255 : 0] fifo_write_m_2_rd_data;
    wire          fifo_write_m_2_rd_en;
    wire          fifo_write_m_2_empty;

    wire [255 : 0] tosendfifo_rddata;
    wire          tosendfifo_rd_en;
    wire          tosendfifo_empty;

    wire [31 : 0] fadd_s_axis_a_tkeep;
    wire fadd_s_axis_a_tlast;
    wire [31 : 0] HBM_READ_m_1_tkeep;
    wire HBM_READ_m_1_tlast;
    wire [31 : 0] HBM_READ_m_2_tkeep;
    wire HBM_READ_m_2_tlast;

    design_hbm_wrapper design_hbm_wrapper_inst (
        fadd_s_axis_a_tdata,
        fadd_s_axis_a_tkeep,
        fadd_s_axis_a_tlast,
        fadd_s_axis_a_tready,
        fadd_s_axis_a_tvalid,
        HBM_READ_m_1_tdata,
        HBM_READ_m_1_tkeep,
        HBM_READ_m_1_tlast,
        HBM_READ_m_1_tready,
        HBM_READ_m_1_tvalid,
        HBM_READ_m_2_tdata,
        HBM_READ_m_2_tkeep,
        HBM_READ_m_2_tlast,
        HBM_READ_m_2_tready,
        HBM_READ_m_2_tvalid,
        HBM_STATUS_ready,
        HBM_WRITE_m_0_tdata,
        HBM_WRITE_m_0_tready,
        HBM_WRITE_m_0_tvalid,
        HBM_WRITE_m_1_tdata,
        HBM_WRITE_m_1_tready,
        HBM_WRITE_m_1_tvalid,
        HBM_WRITE_m_2_tdata,
        HBM_WRITE_m_2_tready,
        HBM_WRITE_m_2_tvalid,
        clk,
        clk,
        m_ctrl_ports_0_busy,
        m_ctrl_ports_1_busy,
        m_ctrl_ports_2_busy,
        m_ctrl_ports_0_rd_blockamount,
        m_ctrl_ports_0_rd_run,
        m_ctrl_ports_0_rd_startaddress,
        m_ctrl_ports_0_wr_blockamount,
        m_ctrl_ports_0_wr_run,
        m_ctrl_ports_0_wr_startaddress,
        m_ctrl_ports_1_rd_blockamount,
        m_ctrl_ports_1_rd_run,
        m_ctrl_ports_1_rd_startaddress,
        m_ctrl_ports_1_wr_blockamount,
        m_ctrl_ports_1_wr_run,
        m_ctrl_ports_1_wr_startaddress,
        m_ctrl_ports_2_rd_blockamount,
        m_ctrl_ports_2_rd_run,
        m_ctrl_ports_2_rd_startaddress,
        m_ctrl_ports_2_wr_blockamount,
        m_ctrl_ports_2_wr_run,
        m_ctrl_ports_2_wr_startaddress,
        mux_select_0,
        mux_select_1,
        mux_select_2,
        reset
    );

    





    
    

    /* rar_rdfifo2axis rar_rdfifo2axis_inst ( // HBM argument - | - double_fadd
        clk,

        fifo_read_m_0_rd_data,
        fifo_read_m_0_rd_en,
        fifo_read_m_0_empty,

        fadd_s_axis_a_tready,
        fadd_s_axis_a_tvalid,
        fadd_s_axis_a_tdata,
        //output reg [7:0]    s_axis_tkeep,
        //output reg          s_axis_tlast
    ); */

    wire [255 : 0] fadd_recv_m_axis_tdata;
    wire          fadd_recv_m_axis_tready;
    wire          fadd_recv_m_axis_tvalid;

    rar_double_fadd rar_double_fadd_inst ( // fifo2axis - | - axis2fifo
        clk,

        fadd_s_axis_a_tdata,
        fadd_s_axis_a_tready,
        fadd_s_axis_a_tvalid,

        fadd_recv_m_axis_tdata,
        fadd_recv_m_axis_tready,
        fadd_recv_m_axis_tvalid,

        fadd_final_m_axis_tdata,
        fadd_final_m_axis_tready,
        fadd_final_m_axis_tvalid,

        skip_calculate_a,
        skip_calculate_b
    );

    

    rar_axis_mux12 rar_axis_mux12_inst(
        clk,

        fadd_final_m_axis_tdata,
        fadd_final_m_axis_tready,
        1'b0,//input        axis_i1_tlast,
        fadd_final_m_axis_tvalid,
        32'hffffffff,//255,//input [7:0]  axis_i1_tkeep,
        
        
        HBM_WRITE_m_1_tdata,
        HBM_WRITE_m_1_tready,
        ,//output reg        axis_o1_tlast,
        HBM_WRITE_m_1_tvalid,
        ,//output reg [7:0]  axis_o1_tkeep,

        HBM_WRITE_m_2_tdata,
        HBM_WRITE_m_2_tready,
        ,//output reg        axis_o2_tlast,
        HBM_WRITE_m_2_tvalid,
        ,//output reg [7:0]  axis_o2_tkeep,
        
        saveto_hbm_part_mux
    );

    

    rar_axis_mux21 rar_axis_mux21_inst(
        clk,

        HBM_READ_m_1_tdata,
        HBM_READ_m_1_tready,
        HBM_READ_m_1_tlast,
        HBM_READ_m_1_tvalid,
        HBM_READ_m_1_tkeep,
        
        HBM_READ_m_2_tdata,
        HBM_READ_m_2_tready,
        HBM_READ_m_2_tlast,
        HBM_READ_m_2_tvalid,
        HBM_READ_m_2_tkeep,
        
        send_s_axis_tdata,
        send_s_axis_tready,
        send_s_axis_tlast,
        send_s_axis_tvalid,
        send_s_axis_tkeep,

        fetchfrom_hbm_part_mux

    );

    wire duplicate_on;

    duplicate_mux duplicate_mux_inst (
        clk,

        recv_m_axis_tdata,
        recv_m_axis_tkeep,
        recv_m_axis_tvalid,
        recv_m_axis_tlast,
        recv_m_axis_tready,

        fadd_recv_m_axis_tdata,
        fadd_recv_m_axis_tready,
        fadd_recv_m_axis_tvalid,

        HBM_WRITE_m_0_tdata,
        HBM_WRITE_m_0_tready,
        HBM_WRITE_m_0_tvalid,

        duplicate_on
    );

    ringallreduce_controller ringallreduce_controller_inst(
        clk,
        rar_start,
        sync_bytes_amount_reg,
        rar_done,

        ring_device_id,
        ring_device_amount,

        mux_select_0, 
        mux_select_1, 
        mux_select_2, 

        m_ctrl_ports_0_rd_blockamount ,
        m_ctrl_ports_0_rd_run         ,
        m_ctrl_ports_0_rd_startaddress,
        m_ctrl_ports_0_wr_blockamount ,
        m_ctrl_ports_0_wr_run         ,
        m_ctrl_ports_0_wr_startaddress,
        m_ctrl_ports_0_busy,

        m_ctrl_ports_1_rd_blockamount ,
        m_ctrl_ports_1_rd_run         ,
        m_ctrl_ports_1_rd_startaddress,
        m_ctrl_ports_1_wr_blockamount ,
        m_ctrl_ports_1_wr_run         ,
        m_ctrl_ports_1_wr_startaddress,
        m_ctrl_ports_1_busy,

        m_ctrl_ports_2_rd_blockamount ,
        m_ctrl_ports_2_rd_run         ,
        m_ctrl_ports_2_rd_startaddress,
        m_ctrl_ports_2_wr_blockamount ,
        m_ctrl_ports_2_wr_run         ,
        m_ctrl_ports_2_wr_startaddress,
        m_ctrl_ports_2_busy,

        skip_calculate_a,
        skip_calculate_b,

        saveto_hbm_part_mux,
        fetchfrom_hbm_part_mux,

        duplicate_on
    );

endmodule




module rar_axis_mux12 (
    input clk,

    input [255:0] axis_i1_tdata,
    output     axis_i1_tready,
    input        axis_i1_tlast,
    input        axis_i1_tvalid,
    input [31:0]  axis_i1_tkeep,
    
    
    output reg [255:0] axis_o1_tdata,
    input             axis_o1_tready,
    output reg        axis_o1_tlast,
    output reg        axis_o1_tvalid,
    output reg [31:0]  axis_o1_tkeep,

    output reg [255:0] axis_o2_tdata,
    input             axis_o2_tready,
    output reg        axis_o2_tlast,
    output reg        axis_o2_tvalid,
    output reg [31:0]  axis_o2_tkeep,
    
    input output_mux_select
);

    wire [255:0]axis_i1_tdata_slice;
    reg        axis_i1_tready_slice;// = output_mux_select ? axis_o2_tready : axis_o1_tready;
    wire        axis_i1_tlast_slice;
    wire        axis_i1_tvalid_slice;
    wire [31:0] axis_i1_tkeep_slice;



    axis_register_slice_0 axis_register_slice_0_inst (
        .s_axis_tvalid(axis_i1_tvalid),
        .s_axis_tdata(axis_i1_tdata),
        .s_axis_tkeep(axis_i1_tkeep),
        .s_axis_tlast(axis_i1_tlast),
        .s_axis_tready(axis_i1_tready),
        .m_axis_tvalid(axis_i1_tvalid_slice),
        .m_axis_tdata(axis_i1_tdata_slice),
        .m_axis_tkeep(axis_i1_tkeep_slice),
        .m_axis_tlast(axis_i1_tlast_slice),
        .m_axis_tready(axis_i1_tready_slice),
        .aclk(clk),
        .aresetn(1'b1)
    );

    always @ (*) begin
        case (output_mux_select)
        0: begin
            axis_o1_tdata  <= axis_i1_tdata_slice;
            axis_i1_tready_slice <= axis_o1_tready;
            axis_o1_tlast  <= axis_i1_tlast_slice;
            axis_o1_tvalid <= axis_i1_tvalid_slice;
            axis_o1_tkeep  <= axis_i1_tkeep_slice;
            axis_o2_tvalid <= 0;
        end
        1: begin
            axis_o2_tdata  <= axis_i1_tdata_slice;
            axis_i1_tready_slice <= axis_o2_tready;
            axis_o2_tlast  <= axis_i1_tlast_slice;
            axis_o2_tvalid <= axis_i1_tvalid_slice;
            axis_o2_tkeep  <= axis_i1_tkeep_slice;
            axis_o1_tvalid <= 0;
        end
        endcase
    end
endmodule

module rar_axis_mux21 (
    input clk,

    input [255:0] axis_i1_tdata,
    output /*reg*/      axis_i1_tready,
    input        axis_i1_tlast,
    input        axis_i1_tvalid,
    input [31:0]  axis_i1_tkeep,
    
    input [255:0] axis_i2_tdata,
    output /*reg*/      axis_i2_tready,
    input        axis_i2_tlast,
    input        axis_i2_tvalid,
    input [31:0]  axis_i2_tkeep,
    
    output reg [255:0] axis_o1_tdata,
    input         axis_o1_tready,
    output reg        axis_o1_tlast,
    output reg        axis_o1_tvalid,
    output reg [31:0]  axis_o1_tkeep,

    input input_mux_select

);
    wire [255:0]axis_i1_tdata_slice;
    reg        axis_i1_tready_slice;
    wire        axis_i1_tlast_slice;
    wire        axis_i1_tvalid_slice;
    wire [31:0] axis_i1_tkeep_slice;

    wire [255:0]axis_i2_tdata_slice;
    reg        axis_i2_tready_slice;
    wire        axis_i2_tlast_slice;
    wire        axis_i2_tvalid_slice;
    wire [31:0] axis_i2_tkeep_slice;

    axis_register_slice_0 axis_register_slice_0_inst (
        .s_axis_tvalid(axis_i1_tvalid),
        .s_axis_tdata(axis_i1_tdata),
        .s_axis_tkeep(axis_i1_tkeep),
        .s_axis_tlast(axis_i1_tlast),
        .s_axis_tready(axis_i1_tready),
        .m_axis_tvalid(axis_i1_tvalid_slice),
        .m_axis_tdata(axis_i1_tdata_slice),
        .m_axis_tkeep(axis_i1_tkeep_slice),
        .m_axis_tlast(axis_i1_tlast_slice),
        .m_axis_tready(axis_i1_tready_slice),
        .aclk(clk),
        .aresetn(1'b1)
    );

    axis_register_slice_0 axis_register_slice_1_inst (
        .s_axis_tvalid(axis_i2_tvalid),
        .s_axis_tdata(axis_i2_tdata),
        .s_axis_tkeep(axis_i2_tkeep),
        .s_axis_tlast(axis_i2_tlast),
        .s_axis_tready(axis_i2_tready),
        .m_axis_tvalid(axis_i2_tvalid_slice),
        .m_axis_tdata(axis_i2_tdata_slice),
        .m_axis_tkeep(axis_i2_tkeep_slice),
        .m_axis_tlast(axis_i2_tlast_slice),
        .m_axis_tready(axis_i2_tready_slice),
        .aclk(clk),
        .aresetn(1'b1)
    );

    always @ (*) begin
        case (input_mux_select)
        0: begin
            axis_o1_tdata  <= axis_i1_tdata_slice;
            axis_i1_tready_slice <= axis_o1_tready;
            axis_o1_tlast  <= axis_i1_tlast_slice;
            axis_o1_tvalid <= axis_i1_tvalid_slice;
            axis_o1_tkeep  <= axis_i1_tkeep_slice;
            axis_i2_tready_slice <= 0;
        end
        1: begin
            axis_o1_tdata  <= axis_i2_tdata_slice;
            axis_i2_tready_slice <= axis_o1_tready;
            axis_o1_tlast  <= axis_i2_tlast_slice;
            axis_o1_tvalid <= axis_i2_tvalid_slice;
            axis_o1_tkeep  <= axis_i2_tkeep_slice;
            axis_i1_tready_slice <= 0;
        end
        endcase
    end

endmodule

module rar_fifomux12 (
    input clk,

    input [255:0] fifo_i_wrdata,
    input        fifo_i_wr_en,
    output reg   fifo_i_full,

    output reg [255:0] fifo_o_1_wrdata,
    output reg        fifo_o_1_wr_en,
    input             fifo_o_1_full,

    output reg [255:0] fifo_o_2_wrdata,
    output reg        fifo_o_2_wr_en,
    input             fifo_o_2_full,

    input output_mux_select
);
    always @ (*) begin
        case (output_mux_select)
        0: begin
            fifo_o_1_wrdata <= fifo_i_wrdata;
            fifo_o_1_wr_en  <= fifo_i_wr_en;
            fifo_i_full     <= fifo_o_1_full;
            fifo_o_2_wr_en  <= 0;
        end
        1: begin
            fifo_o_2_wrdata <= fifo_i_wrdata;
            fifo_o_2_wr_en  <= fifo_i_wr_en;
            fifo_i_full     <= fifo_o_2_full;
            fifo_o_1_wr_en  <= 0;
        end
        endcase
    end

endmodule

module rar_fifo_mux21 (
    input clk,

    input  [255 : 0]  hbm_1_rddata,
    input            hbm_1_rd_en,
    output reg       hbm_1_empty,

    input  [255 : 0]  hbm_2_rddata,
    input            hbm_2_rd_en,
    output reg       hbm_2_empty,

    output reg [25 : 0] o_rddata,
    output reg          o_rd_en,
    input               o_empty,

    input              input_mux_select
);
    always @ (*) begin
        case (input_mux_select)
        0: begin
            o_rddata <= hbm_1_rddata;
            o_rd_en  <= hbm_1_rd_en;
            hbm_1_empty <= o_empty;
        end
        1: begin
            o_rddata <= hbm_2_rddata;
            o_rd_en  <= hbm_2_rd_en;
            hbm_2_empty <= o_empty;
        end
        endcase
    end

endmodule


module rar_double_fadd(
    input clk,
    input [255 : 0]  s_axis_a_tdata,
    output reg      s_axis_a_tready,
    input           s_axis_a_tvalid,

    input [255 : 0]  s_axis_b_tdata,
    output reg      s_axis_b_tready,
    input           s_axis_b_tvalid,

    output reg [255 : 0] final_m_axis_tdata,
    input           final_m_axis_tready,
    output reg         final_m_axis_tvalid,

    input skip_calculate_a,
    input skip_calculate_b

);

    wire [31 : 0] f1_s_axis_a_tdata, f2_s_axis_a_tdata, f3_s_axis_a_tdata, f4_s_axis_a_tdata;
    wire [31 : 0] f5_s_axis_a_tdata, f6_s_axis_a_tdata, f7_s_axis_a_tdata, f8_s_axis_a_tdata;
    assign f1_s_axis_a_tdata = s_axis_a_tdata[255 : 224];
    assign f2_s_axis_a_tdata = s_axis_a_tdata[223 : 192];
    assign f3_s_axis_a_tdata = s_axis_a_tdata[191 : 160];
    assign f4_s_axis_a_tdata = s_axis_a_tdata[159 : 128];
    assign f5_s_axis_a_tdata = s_axis_a_tdata[127 : 96];
    assign f6_s_axis_a_tdata = s_axis_a_tdata[95 : 64];
    assign f7_s_axis_a_tdata = s_axis_a_tdata[63 : 32];
    assign f8_s_axis_a_tdata = s_axis_a_tdata[31 : 0];
    wire f1_s_axis_a_tready, f2_s_axis_a_tready;
    wire f3_s_axis_a_tready, f4_s_axis_a_tready;
    wire f5_s_axis_a_tready, f6_s_axis_a_tready;
    wire f7_s_axis_a_tready, f8_s_axis_a_tready;
    wire f1_s_axis_a_tvalid, f2_s_axis_a_tvalid;
    wire f3_s_axis_a_tvalid, f4_s_axis_a_tvalid;
    wire f5_s_axis_a_tvalid, f6_s_axis_a_tvalid;
    wire f7_s_axis_a_tvalid, f8_s_axis_a_tvalid;
    //assign s_axis_a_tready = f1_s_axis_a_tready & f2_s_axis_a_tready;
    assign f1_s_axis_a_tvalid = s_axis_a_tvalid & ~skip_calculate_a & ~skip_calculate_b;
    assign f2_s_axis_a_tvalid = s_axis_a_tvalid & ~skip_calculate_a & ~skip_calculate_b;
    assign f3_s_axis_a_tvalid = s_axis_a_tvalid & ~skip_calculate_a & ~skip_calculate_b;
    assign f4_s_axis_a_tvalid = s_axis_a_tvalid & ~skip_calculate_a & ~skip_calculate_b;
    assign f5_s_axis_a_tvalid = s_axis_a_tvalid & ~skip_calculate_a & ~skip_calculate_b;
    assign f6_s_axis_a_tvalid = s_axis_a_tvalid & ~skip_calculate_a & ~skip_calculate_b;
    assign f7_s_axis_a_tvalid = s_axis_a_tvalid & ~skip_calculate_a & ~skip_calculate_b;
    assign f8_s_axis_a_tvalid = s_axis_a_tvalid & ~skip_calculate_a & ~skip_calculate_b;

    wire [31 : 0] f1_s_axis_b_tdata, f2_s_axis_b_tdata;
    wire [31 : 0] f3_s_axis_b_tdata, f4_s_axis_b_tdata;
    wire [31 : 0] f5_s_axis_b_tdata, f6_s_axis_b_tdata;
    wire [31 : 0] f7_s_axis_b_tdata, f8_s_axis_b_tdata;

    assign f1_s_axis_b_tdata = s_axis_b_tdata[255 : 224];
    assign f2_s_axis_b_tdata = s_axis_b_tdata[223 : 192];
    assign f3_s_axis_b_tdata = s_axis_b_tdata[191 : 160];
    assign f4_s_axis_b_tdata = s_axis_b_tdata[159 : 128];
    assign f5_s_axis_b_tdata = s_axis_b_tdata[127 : 96];
    assign f6_s_axis_b_tdata = s_axis_b_tdata[95 : 64];
    assign f7_s_axis_b_tdata = s_axis_b_tdata[63 : 32];
    assign f8_s_axis_b_tdata = s_axis_b_tdata[31 : 0];
    
    wire f1_s_axis_b_tready, f2_s_axis_b_tready;
    wire f3_s_axis_b_tready, f4_s_axis_b_tready;
    wire f5_s_axis_b_tready, f6_s_axis_b_tready;
    wire f7_s_axis_b_tready, f8_s_axis_b_tready;
    wire f1_s_axis_b_tvalid, f2_s_axis_b_tvalid;
    wire f3_s_axis_b_tvalid, f4_s_axis_b_tvalid;
    wire f5_s_axis_b_tvalid, f6_s_axis_b_tvalid;
    wire f7_s_axis_b_tvalid, f8_s_axis_b_tvalid;
    assign f1_s_axis_b_tvalid = s_axis_b_tvalid & ~skip_calculate_a & ~skip_calculate_b;
    assign f2_s_axis_b_tvalid = s_axis_b_tvalid & ~skip_calculate_a & ~skip_calculate_b;
    assign f3_s_axis_b_tvalid = s_axis_b_tvalid & ~skip_calculate_a & ~skip_calculate_b;
    assign f4_s_axis_b_tvalid = s_axis_b_tvalid & ~skip_calculate_a & ~skip_calculate_b;
    assign f5_s_axis_b_tvalid = s_axis_b_tvalid & ~skip_calculate_a & ~skip_calculate_b;
    assign f6_s_axis_b_tvalid = s_axis_b_tvalid & ~skip_calculate_a & ~skip_calculate_b;
    assign f7_s_axis_b_tvalid = s_axis_b_tvalid & ~skip_calculate_a & ~skip_calculate_b;
    assign f8_s_axis_b_tvalid = s_axis_b_tvalid & ~skip_calculate_a & ~skip_calculate_b;



    wire [255 : 0] m_axis_tdata;
    reg m_axis_tready;
    wire m_axis_tvalid;
    wire [31 : 0] f1_m_axis_tdata, f2_m_axis_tdata;
    wire [31 : 0] f3_m_axis_tdata, f4_m_axis_tdata;
    wire [31 : 0] f5_m_axis_tdata, f6_m_axis_tdata;
    wire [31 : 0] f7_m_axis_tdata, f8_m_axis_tdata;
    wire f1_m_axis_tready, f2_m_axis_tready;
    wire f3_m_axis_tready, f4_m_axis_tready;
    wire f5_m_axis_tready, f6_m_axis_tready;
    wire f7_m_axis_tready, f8_m_axis_tready;
    wire f1_m_axis_tvalid, f2_m_axis_tvalid;
    wire f3_m_axis_tvalid, f4_m_axis_tvalid;
    wire f5_m_axis_tvalid, f6_m_axis_tvalid;
    wire f7_m_axis_tvalid, f8_m_axis_tvalid;
    assign m_axis_tdata = {f1_m_axis_tdata, f2_m_axis_tdata, f3_m_axis_tdata, f4_m_axis_tdata, f5_m_axis_tdata, f6_m_axis_tdata, f7_m_axis_tdata, f8_m_axis_tdata};
    assign m_axis_tvalid = f1_m_axis_tvalid;//{f1_m_axis_tvalid, f2_m_axis_tvalid, f3_m_axis_tvalid, f4_m_axis_tvalid, f5_m_axis_tvalid, f6_m_axis_tvalid, f7_m_axis_tvalid, f8_m_axis_tvalid} == 8'b11111111;
    assign f1_m_axis_tready = m_axis_tready;
    assign f2_m_axis_tready = m_axis_tready;
    assign f3_m_axis_tready = m_axis_tready;
    assign f4_m_axis_tready = m_axis_tready;
    assign f5_m_axis_tready = m_axis_tready;
    assign f6_m_axis_tready = m_axis_tready;
    assign f7_m_axis_tready = m_axis_tready;
    assign f8_m_axis_tready = m_axis_tready;

    always @ (*) begin
        case ({skip_calculate_a, skip_calculate_b})
        2'b00: begin
            final_m_axis_tdata  <= m_axis_tdata ; 
            m_axis_tready <= final_m_axis_tready;
            final_m_axis_tvalid <= m_axis_tvalid;
            s_axis_a_tready <= f1_s_axis_a_tready;// == 1 && f2_s_axis_a_tready == 1 && f3_s_axis_a_tready == 1 && f4_s_axis_a_tready == 1 && f5_s_axis_a_tready == 1 && f6_s_axis_a_tready == 1 && f7_s_axis_a_tready == 1 && f8_s_axis_a_tready == 1;
            s_axis_b_tready <= f1_s_axis_b_tready;// == 1 && f2_s_axis_b_tready == 1 && f3_s_axis_b_tready == 1 && f4_s_axis_b_tready == 1 && f5_s_axis_b_tready == 1 && f6_s_axis_b_tready == 1 && f7_s_axis_b_tready == 1 && f8_s_axis_b_tready == 1;
        end
        2'b10: begin
            final_m_axis_tdata  <= s_axis_a_tdata;
            m_axis_tready <= 0;//final_m_axis_tready;
            final_m_axis_tvalid <= s_axis_a_tvalid;
            s_axis_a_tready <= (final_m_axis_tready == 1);
            s_axis_b_tready <= 0;
        end
        2'b01: begin
            final_m_axis_tdata  <= s_axis_b_tdata;
            m_axis_tready <= 0;//final_m_axis_tready;
            final_m_axis_tvalid <= s_axis_b_tvalid;
            s_axis_a_tready <= 0;
            s_axis_b_tready <= (final_m_axis_tready == 1);
        end
        2'b11: begin // impossible error
            //final_m_axis_tdata <= 0;
            m_axis_tready <= 0;
            //final_m_axis_tvalid <= 0;
            s_axis_a_tready <= 0;
            s_axis_b_tready <= 0;
        end
        endcase
    end

    reg aresetn = 1;
    reg [1:0] aresetn_reg = 2'b11;
    always @ (posedge clk) begin
        aresetn_reg <= {aresetn_reg[0], skip_calculate_a | skip_calculate_b};
        if (aresetn_reg != 2'b00) begin
            aresetn <= 0;
        end 
        else begin
            aresetn <= 1;
        end
    end


    floating_point_fadd floating_point_fadd_1(
        .s_axis_a_tdata (f1_s_axis_a_tdata),
        .s_axis_a_tready(f1_s_axis_a_tready),
        .s_axis_a_tvalid(f1_s_axis_a_tvalid),
        .s_axis_b_tdata (f1_s_axis_b_tdata),
        .s_axis_b_tready(f1_s_axis_b_tready),
        .s_axis_b_tvalid(f1_s_axis_b_tvalid),
        .m_axis_result_tdata (f1_m_axis_tdata),
        .m_axis_result_tready(f1_m_axis_tready),
        .m_axis_result_tvalid(f1_m_axis_tvalid),
        .aclk(clk),
        .aresetn(aresetn)
    );

    floating_point_fadd floating_point_fadd_2(
        .s_axis_a_tdata (f2_s_axis_a_tdata),
        .s_axis_a_tready(f2_s_axis_a_tready),
        .s_axis_a_tvalid(f2_s_axis_a_tvalid),
        .s_axis_b_tdata (f2_s_axis_b_tdata),
        .s_axis_b_tready(f2_s_axis_b_tready),
        .s_axis_b_tvalid(f2_s_axis_b_tvalid),
        .m_axis_result_tdata (f2_m_axis_tdata),
        .m_axis_result_tready(f2_m_axis_tready),
        .m_axis_result_tvalid(f2_m_axis_tvalid),
        .aclk(clk),
        .aresetn(aresetn)
    );

    floating_point_fadd floating_point_fadd_3(
        .s_axis_a_tdata (f3_s_axis_a_tdata),
        .s_axis_a_tready(f3_s_axis_a_tready),
        .s_axis_a_tvalid(f3_s_axis_a_tvalid),
        .s_axis_b_tdata (f3_s_axis_b_tdata),
        .s_axis_b_tready(f3_s_axis_b_tready),
        .s_axis_b_tvalid(f3_s_axis_b_tvalid),
        .m_axis_result_tdata (f3_m_axis_tdata),
        .m_axis_result_tready(f3_m_axis_tready),
        .m_axis_result_tvalid(f3_m_axis_tvalid),
        .aclk(clk),
        .aresetn(aresetn)
    );

    floating_point_fadd floating_point_fadd_4(
        .s_axis_a_tdata (f4_s_axis_a_tdata),
        .s_axis_a_tready(f4_s_axis_a_tready),
        .s_axis_a_tvalid(f4_s_axis_a_tvalid),
        .s_axis_b_tdata (f4_s_axis_b_tdata),
        .s_axis_b_tready(f4_s_axis_b_tready),
        .s_axis_b_tvalid(f4_s_axis_b_tvalid),
        .m_axis_result_tdata (f4_m_axis_tdata),
        .m_axis_result_tready(f4_m_axis_tready),
        .m_axis_result_tvalid(f4_m_axis_tvalid),
        .aclk(clk),
        .aresetn(aresetn)
    );

    floating_point_fadd floating_point_fadd_5(
        .s_axis_a_tdata (f5_s_axis_a_tdata),
        .s_axis_a_tready(f5_s_axis_a_tready),
        .s_axis_a_tvalid(f5_s_axis_a_tvalid),
        .s_axis_b_tdata (f5_s_axis_b_tdata),
        .s_axis_b_tready(f5_s_axis_b_tready),
        .s_axis_b_tvalid(f5_s_axis_b_tvalid),
        .m_axis_result_tdata (f5_m_axis_tdata),
        .m_axis_result_tready(f5_m_axis_tready),
        .m_axis_result_tvalid(f5_m_axis_tvalid),
        .aclk(clk),
        .aresetn(aresetn)
    );

    floating_point_fadd floating_point_fadd_6(
        .s_axis_a_tdata (f6_s_axis_a_tdata),
        .s_axis_a_tready(f6_s_axis_a_tready),
        .s_axis_a_tvalid(f6_s_axis_a_tvalid),
        .s_axis_b_tdata (f6_s_axis_b_tdata),
        .s_axis_b_tready(f6_s_axis_b_tready),
        .s_axis_b_tvalid(f6_s_axis_b_tvalid),
        .m_axis_result_tdata (f6_m_axis_tdata),
        .m_axis_result_tready(f6_m_axis_tready),
        .m_axis_result_tvalid(f6_m_axis_tvalid),
        .aclk(clk),
        .aresetn(aresetn)
    );

    floating_point_fadd floating_point_fadd_7(
        .s_axis_a_tdata (f7_s_axis_a_tdata),
        .s_axis_a_tready(f7_s_axis_a_tready),
        .s_axis_a_tvalid(f7_s_axis_a_tvalid),
        .s_axis_b_tdata (f7_s_axis_b_tdata),
        .s_axis_b_tready(f7_s_axis_b_tready),
        .s_axis_b_tvalid(f7_s_axis_b_tvalid),
        .m_axis_result_tdata (f7_m_axis_tdata),
        .m_axis_result_tready(f7_m_axis_tready),
        .m_axis_result_tvalid(f7_m_axis_tvalid),
        .aclk(clk),
        .aresetn(aresetn)
    );

    floating_point_fadd floating_point_fadd_8(
        .s_axis_a_tdata (f8_s_axis_a_tdata),
        .s_axis_a_tready(f8_s_axis_a_tready),
        .s_axis_a_tvalid(f8_s_axis_a_tvalid),
        .s_axis_b_tdata (f8_s_axis_b_tdata),
        .s_axis_b_tready(f8_s_axis_b_tready),
        .s_axis_b_tvalid(f8_s_axis_b_tvalid),
        .m_axis_result_tdata (f8_m_axis_tdata),
        .m_axis_result_tready(f8_m_axis_tready),
        .m_axis_result_tvalid(f8_m_axis_tvalid),
        .aclk(clk),
        .aresetn(aresetn)
    );

endmodule


module rar_rdfifo2axis (
    input clk,

    input [255 : 0]  fifo_rddata,
    output reg      fifo_rd_en,
    input           fifo_empty,

    input               s_axis_tready,
    output reg          s_axis_tvalid,
    output reg [255 : 0] s_axis_tdata,
    output reg [31:0]    s_axis_tkeep,
    output reg          s_axis_tlast
);

    always @ (*) begin
        s_axis_tdata <= fifo_rddata;
        s_axis_tkeep <= 32'hffffffff;//255;
        s_axis_tlast <= fifo_empty;
        s_axis_tvalid <= (fifo_empty == 0) && (s_axis_tready == 1);
        fifo_rd_en <= (fifo_empty == 0) && (s_axis_tready == 1);
    end

endmodule

module rar_wraxis2fifo (
    input clk,


    output reg      m_axis_tready,
    input           m_axis_tvalid,
    input [255 : 0]  m_axis_tdata,
    input [31 : 0]   m_axis_tkeep,
    input           m_axis_tlast,

    output reg [255 : 0] fifo_wrdata,
    output reg          fifo_wr_en,
    input               fifo_full
);

    always @ (*) begin
        fifo_wrdata <= m_axis_tdata;
        fifo_wr_en <= (m_axis_tvalid == 1) && (m_axis_tready == 1);
        m_axis_tready <= (fifo_full == 0);
    end

endmodule


module duplicate_mux (
    input clk,

    input [255 : 0] recv_m_axis_tdata,
    input [ 31 : 0] recv_m_axis_tkeep,
    input          recv_m_axis_tvalid,
    input          recv_m_axis_tlast,
    output /*reg*/     recv_m_axis_tready,

    output reg [255 : 0] fadd_recv_m_axis_tdata,
    input               fadd_recv_m_axis_tready,
    output reg          fadd_recv_m_axis_tvalid,

    output reg [255 : 0]  HBM_WRITE_m_0_tdata,
    input                HBM_WRITE_m_0_tready,
    output reg           HBM_WRITE_m_0_tvalid,

    input duplicate_on
);
    wire [255 : 0] recv_m_axis_tdata_slice;
    wire [ 31 : 0] recv_m_axis_tkeep_slice;
    wire          recv_m_axis_tvalid_slice;
    wire          recv_m_axis_tlast_slice;
    reg          recv_m_axis_tready_slice;

    axis_register_slice_0 axis_register_slice_inst_0 (
        .aclk(clk),
        .aresetn(1'b1),
        .s_axis_tvalid(recv_m_axis_tvalid),
        .s_axis_tdata (recv_m_axis_tdata),
        .s_axis_tkeep (recv_m_axis_tkeep),
        .s_axis_tlast (recv_m_axis_tlast),
        .s_axis_tready(recv_m_axis_tready),
        .m_axis_tvalid(recv_m_axis_tvalid_slice),
        .m_axis_tdata (recv_m_axis_tdata_slice),
        .m_axis_tkeep (recv_m_axis_tkeep_slice),
        .m_axis_tlast (recv_m_axis_tlast_slice),
        .m_axis_tready(recv_m_axis_tready_slice) 
    );
    


    always @ (*) begin
        case (duplicate_on)
        1'b0: begin
            fadd_recv_m_axis_tdata  <= recv_m_axis_tdata_slice;
            recv_m_axis_tready_slice      <= fadd_recv_m_axis_tready;
            fadd_recv_m_axis_tvalid <= recv_m_axis_tvalid_slice;
        end
        1'b1: begin
            fadd_recv_m_axis_tdata  <= recv_m_axis_tdata_slice;
            recv_m_axis_tready_slice      <= fadd_recv_m_axis_tready & HBM_WRITE_m_0_tready;
            fadd_recv_m_axis_tvalid <= recv_m_axis_tvalid_slice & fadd_recv_m_axis_tready & HBM_WRITE_m_0_tready;

            HBM_WRITE_m_0_tdata  <= recv_m_axis_tdata_slice;
            //recv_m_axis_tready   <= HBM_WRITE_m_0_tready;
            HBM_WRITE_m_0_tvalid <= recv_m_axis_tvalid_slice & fadd_recv_m_axis_tready & HBM_WRITE_m_0_tready;
        end
        endcase
    end

endmodule