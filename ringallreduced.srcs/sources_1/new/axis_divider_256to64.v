`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/14/2022 07:49:43 PM
// Design Name: 
// Module Name: axis_divider_256to64
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module axis_divider_256to64(
    input                clk,
    input                reset,
    
    output reg      m_axis_tready,
    input           m_axis_tvalid,
    input [255 : 0] m_axis_tdata,
    input [31 : 0]  m_axis_tkeep,
    input           m_axis_tlast,
    input [0 : 0]   m_axis_tuser,
    
    input                s1_axis_tready,
    output reg           s1_axis_tvalid,
    output reg [63 : 0] s1_axis_tdata,
    output reg [7 : 0]  s1_axis_tkeep,
    output reg           s1_axis_tlast,
    output reg [0 : 0]   s1_axis_tuser,
    
    input                s2_axis_tready,
    output reg           s2_axis_tvalid,
    output reg [63 : 0] s2_axis_tdata,
    output reg [7 : 0]  s2_axis_tkeep,
    output reg           s2_axis_tlast,
    output reg [0 : 0]   s2_axis_tuser,
    
    input                s3_axis_tready,
    output reg           s3_axis_tvalid,
    output reg [63 : 0] s3_axis_tdata,
    output reg [7 : 0]  s3_axis_tkeep,
    output reg           s3_axis_tlast,
    output reg [0 : 0]   s3_axis_tuser,
    
    input                s4_axis_tready,
    output reg           s4_axis_tvalid,
    output reg [63 : 0] s4_axis_tdata,
    output reg [7 : 0]  s4_axis_tkeep,
    output reg           s4_axis_tlast,
    output reg [0 : 0]   s4_axis_tuser
    
    );
    
    always @ (*) begin
        m_axis_tready <= s1_axis_tready & s2_axis_tready & s3_axis_tready & s4_axis_tready;
        s1_axis_tdata <= m_axis_tdata[ 63 :  0];
        s2_axis_tdata <= m_axis_tdata[127 : 64];
        s3_axis_tdata <= m_axis_tdata[191 : 128];
        s4_axis_tdata <= m_axis_tdata[255 : 192];
        s1_axis_tvalid <= m_axis_tvalid;
        s2_axis_tvalid <= m_axis_tvalid;
        s3_axis_tvalid <= m_axis_tvalid;
        s4_axis_tvalid <= m_axis_tvalid;
        s1_axis_tuser <= m_axis_tuser;
        s2_axis_tuser <= m_axis_tuser;
        s3_axis_tuser <= m_axis_tuser;
        s4_axis_tuser <= m_axis_tuser;
        s1_axis_tlast <= m_axis_tlast;
        s2_axis_tlast <= m_axis_tlast;
        s3_axis_tlast <= m_axis_tlast;
        s4_axis_tlast <= m_axis_tlast;
        s1_axis_tkeep <= m_axis_tkeep[ 7 : 0];
        s2_axis_tkeep <= m_axis_tkeep[15 : 8];
        s3_axis_tkeep <= m_axis_tkeep[23 : 16];
        s4_axis_tkeep <= m_axis_tkeep[31 : 24];
    end
    
    
endmodule
