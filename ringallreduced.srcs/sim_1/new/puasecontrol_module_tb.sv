`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/17/2022 10:50:33 AM
// Design Name: 
// Module Name: puasecontrol_module_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module puasecontrol_module_tb(

    );
    reg clk = 0;
    
    reg do_flow_control = 0;
    
    wire                          m_axis_tready;
    reg      [512 - 1 : 0]        m_axis_tdata = 0;
    reg                           m_axis_tvalid;
    reg      [64 - 1 : 0]         m_axis_tkeep ;
    reg      [0 : 0]              m_axis_tuser ;
    reg                           m_axis_tlast ;
    
    reg                           s_axis_tready;
    wire     [512 - 1 : 0]        s_axis_tdata ;
    wire                          s_axis_tvalid;
    wire     [64 - 1 : 0]         s_axis_tkeep ;
    wire     [0 : 0]              s_axis_tuser ;
    wire                          s_axis_tlast ; 
    
    task automatic send();
        for (int i = 0; i < 10; i++) begin
            #2;//@ (posedge clk);
            @ (m_axis_tready == 1);
            m_axis_tvalid <= 1;
            m_axis_tdata <= m_axis_tdata + 1;
            m_axis_tkeep <= {64{1'b1}};
            m_axis_tuser <= 'd0;
            m_axis_tlast <= 0;
        end
        m_axis_tlast <= 1;
        for (int i = 0; i < 10; i++) begin
            #2;//@ (posedge clk);
            @ (m_axis_tready == 1);
            m_axis_tdata <= m_axis_tdata + 1;
            m_axis_tkeep <= {64{1'b1}};
            m_axis_tuser <= 'd0;
            m_axis_tlast <= 0;
        end
        m_axis_tlast <= 1;
        for (int i = 0; i < 10; i++) begin
            #2;//@ (posedge clk);
            @ (m_axis_tready == 1);
            m_axis_tdata <= m_axis_tdata + 1;
            m_axis_tkeep <= {64{1'b1}};
            m_axis_tuser <= 'd0;
            m_axis_tlast <= 0;
        end
        m_axis_tlast <= 1;
    endtask
    
    task automatic waitcycle(cycle);
    
        for (int i = 0; i < cycle; i++) begin
            #2;
            //@ (posedge clk);
        end
    
    endtask
    
    task automatic fc();
        do_flow_control <= 0;
        waitcycle(5);
        do_flow_control <= 1;
        waitcycle(10);
        do_flow_control <= 0;
        waitcycle(2);
        do_flow_control <= 1;
        waitcycle(20);
        do_flow_control <= 0;
    endtask
    

    
    
    initial begin
        s_axis_tready <= 1;
        
        /*fork
            send();
            fc();
        join_none*/
        forever #1 clk = ~clk;
    end
    
    initial begin
        send();
    end
    
    initial begin
        fc();
    end
    
    
    puasecontrol_module #(
        .PACKET_SIZE_MIN(5),
        .PACKET_SIZE_MAX(20)
    ) puasecontrol_module_inst(

    clk,
    
    do_flow_control,
    
    m_axis_tready,
    m_axis_tdata,
    m_axis_tvalid,
    m_axis_tkeep,
    m_axis_tuser,
    m_axis_tlast,
    
    s_axis_tready,
    s_axis_tdata,
    s_axis_tvalid,
    s_axis_tkeep,
    s_axis_tuser,
    s_axis_tlast    
    );
endmodule
