`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/19/2022 11:48:53 AM
// Design Name: 
// Module Name: forward_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module forward_tb(

    );
    
    wire          m_axis_tready;
    wire           m_axis_tvalid;
    wire [63 : 0]  m_axis_tdata;
    wire [7 : 0]   m_axis_tkeep;
    wire           m_axis_tlast;
    wire [0 : 0]   m_axis_tuser;
                           
    wire          s_axis_tready;
    wire          s_axis_tvalid;
    wire [63 : 0] s_axis_tdata;
    wire [7 : 0]  s_axis_tkeep;
    wire          s_axis_tlast;
    wire [0 : 0]  s_axis_tuser;
                           
    reg clk  ;                  
    
    initial begin
        clk = 0;
        forever #2 clk = ~clk;
    end
    
    axis_traffic_gen # (
        64,
        8,
        11
        //parameter repeatly = "true"
    ) axis_traffic_gen_inst (
        clk,
        0,
       
        m_axis_tready,
        m_axis_tvalid,
        m_axis_tdata,
        m_axis_tkeep,
        m_axis_tlast,
        m_axis_tuser
        
    );
    
    axis_traffic_monitor #(
        64,
        8
    ) axis_traffic_monitor_inst (
        clk,
        0,
        
        s_axis_tready,
        s_axis_tdata,
        s_axis_tvalid,
        s_axis_tkeep,
        s_axis_tuser,
        s_axis_tlast
        
    );
    
    
    
    
    forward # ( // adjust axis stream
        .packet_min(5 ),
        .packet_max(10)
    ) forward_inst (
         m_axis_tready,
         m_axis_tvalid,
         m_axis_tdata,
         m_axis_tkeep,
         m_axis_tlast,
         m_axis_tuser,
       
         s_axis_tready,
         s_axis_tvalid,
         s_axis_tdata,
         s_axis_tkeep,
         s_axis_tlast,
         s_axis_tuser,
        
         clk
    );
endmodule
