`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/18/2022 08:24:20 PM
// Design Name: 
// Module Name: writeaxis_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module writeaxis_tb(
        output reg clk = 0,
        
        output reg axis_i_tready,
        output  [7:0] axis_i_tkeep,
        output  axis_i_tvalid,
        output  [63:0] axis_i_tdata,
        output  axis_i_tlast,
        
        output reg [7:0] statusdata_in,
        output reg statusdata_wea,
        output reg reset_button,
        output [7:0] statusdata
    );
    
    initial begin
        forever #5 clk = ~clk;
    end
    
    //reg [13:0] STAT = 0;
    
    always@(posedge clk) begin
   //     case(STAT)
            axis_i_tready <= 1;
     //   endcase
    end
    
    writeaxis writeaxis_inst(
        .clk( clk),
    
        .axis_i_tready( axis_i_tready),
        .axis_i_tkeep( axis_i_tkeep),
        .axis_i_tvalid( axis_i_tvalid),
        .axis_i_tdata( axis_i_tdata),
        .axis_i_tlast( axis_i_tlast),
        
        .statusdata_in( statusdata_in),
        .statusdata_wea( statusdata_wea),
        .reset_button( reset_button),
        .statusdata( statusdata)
    );
endmodule
