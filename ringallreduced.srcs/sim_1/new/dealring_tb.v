`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/18/2022 11:27:42 AM
// Design Name: 
// Module Name: dealring_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module dealring_tb(
    output reg startring,
    
    output reg [31:0] perblocksize,
    output reg [31:0] curnode,
    output reg [31:0] totalnode,
    
    output [31:0] bram_addr,
    output reg [63:0] bram_in,
    
    output bram_wea,
    output [63:0] bram_out,
    
    output wr_en,
    output [63:0] din,
    output full,
    
    output rd_en,
    output [63:0] dout,
    output empty,
    
    output ringfinished,
    
    output reg clk = 0,
    output reg reset,
    
    output [13:0] RUN,
    //output reg [13:0] next_RUN,
    
    output reg [31:0] maxaddr
    );
    
    initial begin
        forever #5 clk = ~clk;
    end
    
    
    
    reg [13:0] STAT = 0;
    
    always@(posedge clk) begin
        case(STAT)
        0: begin
            reset <= 1;
        end
        1: begin
            reset <= 0;
        end
        2: begin
            startring <= 1;
            perblocksize <= 5;
            maxaddr <= 20;
            curnode <= 0;
            totalnode <= 4;
        end
        3: begin
            startring <= 0;
        end
        4: begin
            bram_in <= 64'h0001000100010001;
        end
        5: begin
            bram_in <= 64'h0002000200020002;
        end
        6: begin
            bram_in <= 64'h0003000300030003;
        end
        7: begin
            bram_in <= 64'h0004000400040004;
        end
        8: begin
            bram_in <= 64'h0005000500050005;
        end
        9: begin
            bram_in <= 64'h0006000600060006;
        end
        10: begin
            bram_in <= 64'h0007000700070007;
        end
        endcase
        STAT <= STAT + 1;
    end
    
   dealring dealring_inst(
        .startring(startring),
    
        .perblocksize(perblocksize),
        .curnode(curnode),
        .totalnode(totalnode),
        
        .bram_addr(bram_addr),
        .bram_in(bram_in),
        
        .bram_wea(bram_wea),
        .bram_out(bram_out),
        
        
        .wr_en(wr_en),
        .din(din),
        .full(full),
        
        .rd_en(rd_en),
        .dout(dout),
        .empty(empty),
        
        .ringfinished(ringfinished),
        
        .clk(clk),
        .reset(reset),
        
        .RUN(RUN),
        //output reg [13:0] next_RUN,
        
        .maxaddr(maxaddr)
   );
   
   fifo_generator fifo_generator_inst(
        .full(full),
        .din(din),
        .wr_en(wr_en),
        
        .empty(empty),
        .dout(dout),
        .rd_en(rd_en),
        
        .clk(clk),
        .srst(reset)
        
    );
endmodule
