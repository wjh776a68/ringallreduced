`timescale 1ns / 1ps
module axistobram_tb(

);
    reg clk;
    wire  statusdata_wea;
    reg [7:0] statusdata = 8'b0;
    wire [7:0] statusdata_in;
    wire wr_en;
    wire [63:0] din;
    reg [63:0] dout = 64'b0;
    
    wire [12:0] RUN;
//    wire [12:0] next_RUN;
    wire startring;
    
    wire [31:0] perblocksize;
    wire [31:0] curnode;
    wire [31:0] totalnode;
    
    wire [31:0] curaddr;
    wire [31:0] maxaddr;
    
    reg [63:0] axis_i_tdata;
    wire axis_i_tready;
    reg axis_i_tlast;
    reg axis_i_tvalid;
    reg [7:0] axis_i_tkeep;
    
    wire [63:0] axis_o_tdata;
    reg axis_o_tready = 1;
    wire  axis_o_tlast;
    wire  axis_o_tvalid;
    wire  [7:0] axis_o_tkeep;
    
    reg reset;
    reg ringfinished = 0;
    initial begin
        clk = 0;
    end
    always #5 clk = ~clk;
    reg [7:0] STAT = 0;
    
    always@(posedge clk) begin
        if(statusdata_wea==1) begin
            statusdata <= statusdata_in;
        end
        
        //if(axis_i_tready==1) begin
            case(STAT)
            0: begin
                reset <= 1;
            end
            1: begin
                reset <= 0;
            end 
            2: begin
                reset <= 0;
                axis_i_tvalid <= 1;
                axis_i_tkeep <= 8'hff;
                axis_i_tlast <= 0;
                axis_i_tdata <= 64'h0000000100000000;
            end
            3:begin
                axis_i_tvalid <= 1;
                axis_i_tkeep <= 8'hff;
                axis_i_tlast <= 0;
                axis_i_tdata <= 64'h0000000500000005;
            end
            4:begin
                axis_i_tvalid <= 1;
                axis_i_tkeep <= 8'hff;
                axis_i_tlast <= 0;
                axis_i_tdata <= 64'h0000000100000001;
            end
            5:begin
                axis_i_tvalid <= 0;
                axis_i_tkeep <= 8'hff;
                axis_i_tlast <= 0;
                axis_i_tdata <= 64'h0000000200000002;
            end
            6:begin
                axis_i_tvalid <= 1;
                axis_i_tkeep <= 8'hff;
                axis_i_tlast <= 0;
                axis_i_tdata <= 64'h0000000200000002;
            end
            7:begin
                axis_i_tvalid <= 1;
                axis_i_tkeep <= 8'hff;
                axis_i_tlast <= 0;
                axis_i_tdata <= 64'h0000000300000003;
            end
            8:begin
                axis_i_tvalid <= 1;
                axis_i_tkeep <= 8'hff;
                axis_i_tlast <= 0;
                axis_i_tdata <= 64'h0000000400000004;
            end
            9:begin
                axis_i_tvalid <= 1;
                axis_i_tkeep <= 8'hff;
                axis_i_tlast <= 1;
                axis_i_tdata <= 64'h0000000500000005;
            end
            10:begin
                axis_i_tvalid <= 0;
                axis_i_tkeep <= 8'hff;
                axis_i_tlast <= 0;
                axis_i_tdata <= 64'h0000000200000020;
            end
            endcase
            STAT <= STAT + 1;
        //end
        
        
    end

axistobram axistobram_inst(
    .axis_i_tdata(axis_i_tdata),
    .axis_i_tready(axis_i_tready),
    .axis_i_tlast(axis_i_tlast),
    .axis_i_tvalid(axis_i_tvalid),
    .axis_i_tkeep(axis_i_tkeep),
    
    .clk(clk),
    .reset(reset),

    .wr_en(wr_en),
    .din(din),
    .dout(dout),
    
    .RUN(RUN),
//    .next_RUN(next_RUN),
    
    
    .statusdata_wea(statusdata_wea),
    .statusdata(statusdata_in),
    .statusdata_in(statusdata),
    
    .startring(startring),
    .ringfinished(ringfinished),
    
    .perblocksize(perblocksize),
    .curnode(curnode),
    .totalnode(totalnode),
    
    .axis_o_tdata(axis_o_tdata),
    .axis_o_tready(axis_o_tready),
    .axis_o_tlast(axis_o_tlast),
    .axis_o_tvalid(axis_o_tvalid),
    .axis_o_tkeep(axis_o_tkeep),
    
    .curaddr(curaddr),
    .maxaddr(maxaddr)
);

endmodule