`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/14/2022 10:58:33 AM
// Design Name: 
// Module Name: axis_traffic_gen_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module axis_traffic_gen_tb(

    );
    reg              clk;
    reg              reset;
    
    reg            s_axis_tready;
    wire           s_axis_tvalid;
    wire [255 : 0] s_axis_tdata;
    wire [31 : 0]  s_axis_tkeep;
    wire           s_axis_tlast;
    
    initial begin
        clk = 0;
        
        s_axis_tready = 1;
        
        forever #1 clk = ~clk;
    end
    
    initial begin
        reset = 1;
        repeat(3)
            @ (posedge clk);
        reset = 0;
    end
    
    
    axis_traffic_gen axis_traffic_gen_inst(
        clk,
        reset,
        
        s_axis_tready,
        s_axis_tvalid,
        s_axis_tdata,
        s_axis_tkeep,
        s_axis_tlast
    
    );
endmodule
