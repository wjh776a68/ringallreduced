`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/18/2022 08:33:09 PM
// Design Name: 
// Module Name: assembly_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module assembly_tb(

    );
    reg clk;
    reg reset;
    
    reg CLK_IN1_D_clk_n;
    reg CLK_IN1_D_clk_p;
    reg gt_ref_clk_clk_n;
    reg gt_ref_clk_clk_p;
    wire [3:0] gt_serial_port_0_grx_n_fpga1;
    wire [3:0] gt_serial_port_0_grx_p_fpga1;
    wire [3:0] gt_serial_port_0_gtx_n_fpga1;
    wire [3:0] gt_serial_port_0_gtx_p_fpga1;
    wire [3:0] gt_serial_port_0_grx_n_fpga2;
    wire [3:0] gt_serial_port_0_grx_p_fpga2;
    wire [3:0] gt_serial_port_0_gtx_n_fpga2;
    wire [3:0] gt_serial_port_0_gtx_p_fpga2;
    wire [3:0] gt_serial_port_0_grx_n_fpga3;
    wire [3:0] gt_serial_port_0_grx_p_fpga3;
    wire [3:0] gt_serial_port_0_gtx_n_fpga3;
    wire [3:0] gt_serial_port_0_gtx_p_fpga3;
    wire [3:0] gt_serial_port_1_grx_n_fpga1;
    wire [3:0] gt_serial_port_1_grx_p_fpga1;
    wire [3:0] gt_serial_port_1_gtx_n_fpga1;
    wire [3:0] gt_serial_port_1_gtx_p_fpga1;
    wire [3:0] gt_serial_port_1_grx_n_fpga2;
    wire [3:0] gt_serial_port_1_grx_p_fpga2;
    wire [3:0] gt_serial_port_1_gtx_n_fpga2;
    wire [3:0] gt_serial_port_1_gtx_p_fpga2;
    wire [3:0] gt_serial_port_1_grx_n_fpga3;
    wire [3:0] gt_serial_port_1_grx_p_fpga3;
    wire [3:0] gt_serial_port_1_gtx_n_fpga3;
    wire [3:0] gt_serial_port_1_gtx_p_fpga3;
    wire stat_rx_status_sfp0_fpga1;
    wire stat_rx_status_sfp1_fpga1;
    wire stat_rx_status_sfp0_fpga2;
    wire stat_rx_status_sfp1_fpga2;
    wire stat_rx_status_sfp0_fpga3;
    wire stat_rx_status_sfp1_fpga3;
    
    initial begin
        clk = 0;
        
        forever #5 clk = ~clk;   
    end
    
    initial begin
        repeat(4) begin
            @ (posedge clk);
            reset = 1;
        end
        reset = 0;
    end
    
    initial begin
        CLK_IN1_D_clk_n = 0;
        forever #1.6 CLK_IN1_D_clk_n = ~CLK_IN1_D_clk_n;
    end
    initial begin
        CLK_IN1_D_clk_p = 1;
        forever #1.6 CLK_IN1_D_clk_p = ~CLK_IN1_D_clk_p;
    end
    
    initial begin
        gt_ref_clk_clk_n = 0;
        forever #5 gt_ref_clk_clk_n = ~gt_ref_clk_clk_n;
    end
    
    initial begin
        gt_ref_clk_clk_p = 1;
        forever #5 gt_ref_clk_clk_p = ~gt_ref_clk_clk_p;
    end
    
    // fpga3 self loop; fpga1 & fpga2 loop
    /*assign gt_serial_port_grx_n_fpga3[1] = gt_serial_port_gtx_n_fpga3[0];
    assign gt_serial_port_grx_p_fpga3[1] = gt_serial_port_gtx_p_fpga3[0];
    assign gt_serial_port_grx_n_fpga3[0] = gt_serial_port_gtx_n_fpga3[1];
    assign gt_serial_port_grx_p_fpga3[0] = gt_serial_port_gtx_p_fpga3[1];
    
    assign gt_serial_port_grx_n_fpga2[1] = gt_serial_port_gtx_n_fpga1[0];
    assign gt_serial_port_grx_p_fpga2[1] = gt_serial_port_gtx_p_fpga1[0];
    assign gt_serial_port_grx_n_fpga1[0] = gt_serial_port_gtx_n_fpga2[1];
    assign gt_serial_port_grx_p_fpga1[0] = gt_serial_port_gtx_p_fpga2[1];
    
    assign gt_serial_port_grx_n_fpga2[0] = gt_serial_port_gtx_n_fpga1[1];
    assign gt_serial_port_grx_p_fpga2[0] = gt_serial_port_gtx_p_fpga1[1];
    assign gt_serial_port_grx_n_fpga1[1] = gt_serial_port_gtx_n_fpga2[0];
    assign gt_serial_port_grx_p_fpga1[1] = gt_serial_port_gtx_p_fpga2[0];*/
    
    // fpga1 & fpga2 & fpga3 loop
    assign gt_serial_port_1_grx_n_fpga3 = gt_serial_port_0_gtx_n_fpga2;
    assign gt_serial_port_1_grx_p_fpga3 = gt_serial_port_0_gtx_p_fpga2;
    assign gt_serial_port_0_grx_n_fpga3 = gt_serial_port_1_gtx_n_fpga1;
    assign gt_serial_port_0_grx_p_fpga3 = gt_serial_port_1_gtx_p_fpga1;
    
    assign gt_serial_port_1_grx_n_fpga2 = gt_serial_port_0_gtx_n_fpga1;
    assign gt_serial_port_1_grx_p_fpga2 = gt_serial_port_0_gtx_p_fpga1;
    assign gt_serial_port_0_grx_n_fpga1 = gt_serial_port_1_gtx_n_fpga2;
    assign gt_serial_port_0_grx_p_fpga1 = gt_serial_port_1_gtx_p_fpga2;
    
    assign gt_serial_port_0_grx_n_fpga2 = gt_serial_port_1_gtx_n_fpga3;
    assign gt_serial_port_0_grx_p_fpga2 = gt_serial_port_1_gtx_p_fpga3;
    assign gt_serial_port_1_grx_n_fpga1 = gt_serial_port_0_gtx_n_fpga3;
    assign gt_serial_port_1_grx_p_fpga1 = gt_serial_port_0_gtx_p_fpga3;
    
    
    
        
    assembly # (
        .sim_id('h123456789abcdef)
    ) assembly_inst_fpga1(
        //clk,
        reset,
    
        CLK_IN1_D_clk_n,
        CLK_IN1_D_clk_p,
        gt_ref_clk_clk_n,
        gt_ref_clk_clk_p,
        gt_ref_clk_clk_n,
        gt_ref_clk_clk_p,
        gt_serial_port_0_grx_n_fpga1,
        gt_serial_port_0_grx_p_fpga1,
        gt_serial_port_0_gtx_n_fpga1,
        gt_serial_port_0_gtx_p_fpga1,
        gt_serial_port_1_grx_n_fpga1,
        gt_serial_port_1_grx_p_fpga1,
        gt_serial_port_1_gtx_n_fpga1,
        gt_serial_port_1_gtx_p_fpga1,
        stat_rx_status_sfp0_fpga1,
        stat_rx_status_sfp1_fpga1
    
    );
    
    
    assembly # (
        .sim_id('h111111111111111)
    ) assembly_inst_fpga2(
        //clk,
        reset,
    
        CLK_IN1_D_clk_n,
        CLK_IN1_D_clk_p,
        gt_ref_clk_clk_n,
        gt_ref_clk_clk_p,
        gt_ref_clk_clk_n,
        gt_ref_clk_clk_p,
        gt_serial_port_0_grx_n_fpga2,
        gt_serial_port_0_grx_p_fpga2,
        gt_serial_port_0_gtx_n_fpga2,
        gt_serial_port_0_gtx_p_fpga2,
        gt_serial_port_1_grx_n_fpga2,
        gt_serial_port_1_grx_p_fpga2,
        gt_serial_port_1_gtx_n_fpga2,
        gt_serial_port_1_gtx_p_fpga2,
        stat_rx_status_sfp0_fpga2,
        stat_rx_status_sfp1_fpga2
    
    );
    
    assembly # (
        .sim_id('h111111111111129)
    ) assembly_inst_fpga3(
        //clk,
        reset,
    
        CLK_IN1_D_clk_n,
        CLK_IN1_D_clk_p,
        gt_ref_clk_clk_n,
        gt_ref_clk_clk_p,
        gt_ref_clk_clk_n,
        gt_ref_clk_clk_p,
        gt_serial_port_0_grx_n_fpga3,
        gt_serial_port_0_grx_p_fpga3,
        gt_serial_port_0_gtx_n_fpga3,
        gt_serial_port_0_gtx_p_fpga3,
        gt_serial_port_1_grx_n_fpga3,
        gt_serial_port_1_grx_p_fpga3,
        gt_serial_port_1_gtx_n_fpga3,
        gt_serial_port_1_gtx_p_fpga3,
        stat_rx_status_sfp0_fpga3,
        stat_rx_status_sfp1_fpga3
    
    );
endmodule
