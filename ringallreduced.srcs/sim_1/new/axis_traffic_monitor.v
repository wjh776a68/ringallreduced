`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/14/2022 11:12:13 AM
// Design Name: 
// Module Name: axis_traffic_monitor
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module axis_traffic_monitor #(
    parameter DATA_WIDTH = 256,
    parameter KEEP_WIDTH = 32//(TDATA_WIDTH / 8)
) (
    input                clk,
    input                reset,
    
    output reg           m_axis_tready,
    input      [DATA_WIDTH - 1 : 0] m_axis_tdata,
    input                m_axis_tvalid,
    input      [KEEP_WIDTH - 1 : 0]  m_axis_tkeep,
    input      [0 : 0]   m_axis_tuser,
    input                m_axis_tlast,
    
    output reg [10:0] counter = 0,
    output reg [10:0] sleepcounter = 0
    );
    
    reg [1:0] recovery_FSM = 0, recovery_FSM_next = 0; // TODO: running after tready be asserted
    
    
    
    always @ (posedge clk) begin
        //m_axis_tready <= 1;
        /*if (counter >= 100) begin
            m_axis_tready <= 0;
            sleepcounter <= sleepcounter + 1;
        end
        else */begin
            m_axis_tready <= 1;
            sleepcounter <= 0;
        end
        
        /*if (sleepcounter >= 100) begin
            counter <= 0;
        end
        else begin
            if (m_axis_tready & m_axis_tvalid) begin
                
                counter <= counter + 1;
                
            end
        end*/
    end
    
endmodule
