-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
-- Date        : Tue Dec  6 16:16:38 2022
-- Host        : dell-Vostro-3910-China-HDD-Protection running 64-bit Ubuntu 22.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim
--               /media/dell/workspace/Xilinx_projects/ringallreduced/ringallreduced.gen/sources_1/bd/design_qsfp_40G/ip/design_qsfp_40G_forward_0_0/design_qsfp_40G_forward_0_0_sim_netlist.vhdl
-- Design      : design_qsfp_40G_forward_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xcvu37p-fsvh2892-2L-e
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_qsfp_40G_forward_0_0_forward is
  port (
    s_axis_tdata : out STD_LOGIC_VECTOR ( 255 downto 0 );
    s_axis_tlast : out STD_LOGIC;
    s_axis_tvalid : out STD_LOGIC;
    m_axis_tready : out STD_LOGIC;
    s_axis_tkeep : out STD_LOGIC_VECTOR ( 0 to 0 );
    m_axis_tvalid : in STD_LOGIC;
    m_axis_tdata : in STD_LOGIC_VECTOR ( 255 downto 0 );
    clk : in STD_LOGIC;
    m_axis_tlast : in STD_LOGIC;
    s_axis_tready : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_qsfp_40G_forward_0_0_forward : entity is "forward";
end design_qsfp_40G_forward_0_0_forward;

architecture STRUCTURE of design_qsfp_40G_forward_0_0_forward is
  signal focus_tkeep : STD_LOGIC;
  signal focus_tkeep_i_1_n_0 : STD_LOGIC;
  signal focus_tkeep_i_2_n_0 : STD_LOGIC;
  signal focus_tlast0 : STD_LOGIC;
  signal focus_tlast_i_2_n_0 : STD_LOGIC;
  signal focus_tlast_i_3_n_0 : STD_LOGIC;
  signal focus_tlast_i_4_n_0 : STD_LOGIC;
  signal focus_tlast_i_5_n_0 : STD_LOGIC;
  signal focus_tlast_i_6_n_0 : STD_LOGIC;
  signal focus_tlast_reg_n_0 : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 15 downto 1 );
  signal p_3_in : STD_LOGIC;
  signal \packet_size_counter0_carry__0_n_2\ : STD_LOGIC;
  signal \packet_size_counter0_carry__0_n_3\ : STD_LOGIC;
  signal \packet_size_counter0_carry__0_n_4\ : STD_LOGIC;
  signal \packet_size_counter0_carry__0_n_5\ : STD_LOGIC;
  signal \packet_size_counter0_carry__0_n_6\ : STD_LOGIC;
  signal \packet_size_counter0_carry__0_n_7\ : STD_LOGIC;
  signal packet_size_counter0_carry_n_0 : STD_LOGIC;
  signal packet_size_counter0_carry_n_1 : STD_LOGIC;
  signal packet_size_counter0_carry_n_2 : STD_LOGIC;
  signal packet_size_counter0_carry_n_3 : STD_LOGIC;
  signal packet_size_counter0_carry_n_4 : STD_LOGIC;
  signal packet_size_counter0_carry_n_5 : STD_LOGIC;
  signal packet_size_counter0_carry_n_6 : STD_LOGIC;
  signal packet_size_counter0_carry_n_7 : STD_LOGIC;
  signal \packet_size_counter[0]_i_1_n_0\ : STD_LOGIC;
  signal \packet_size_counter[15]_i_3_n_0\ : STD_LOGIC;
  signal \packet_size_counter[15]_i_4_n_0\ : STD_LOGIC;
  signal packet_size_counter_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal s_axis_tlast_INST_0_i_1_n_0 : STD_LOGIC;
  signal s_axis_tlast_INST_0_i_2_n_0 : STD_LOGIC;
  signal s_axis_tlast_INST_0_i_3_n_0 : STD_LOGIC;
  signal s_axis_tvalid_packetnouf : STD_LOGIC;
  signal s_axis_tvalid_packetnouf_i_1_n_0 : STD_LOGIC;
  signal s_axis_tvalid_packetnouf_reg_n_0 : STD_LOGIC;
  signal stat_FSM : STD_LOGIC;
  signal stat_FSM_i_2_n_0 : STD_LOGIC;
  signal stat_FSM_next : STD_LOGIC;
  signal \NLW_packet_size_counter0_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 6 );
  signal \NLW_packet_size_counter0_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 to 7 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of focus_tlast_i_2 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of focus_tlast_i_4 : label is "soft_lutpair129";
  attribute SOFT_HLUTNM of focus_tlast_i_5 : label is "soft_lutpair130";
  attribute SOFT_HLUTNM of focus_tlast_i_6 : label is "soft_lutpair129";
  attribute SOFT_HLUTNM of m_axis_tready_INST_0 : label is "soft_lutpair131";
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of packet_size_counter0_carry : label is 35;
  attribute ADDER_THRESHOLD of \packet_size_counter0_carry__0\ : label is 35;
  attribute SOFT_HLUTNM of \s_axis_tdata[0]_INST_0\ : label is "soft_lutpair128";
  attribute SOFT_HLUTNM of \s_axis_tdata[100]_INST_0\ : label is "soft_lutpair78";
  attribute SOFT_HLUTNM of \s_axis_tdata[101]_INST_0\ : label is "soft_lutpair78";
  attribute SOFT_HLUTNM of \s_axis_tdata[102]_INST_0\ : label is "soft_lutpair77";
  attribute SOFT_HLUTNM of \s_axis_tdata[103]_INST_0\ : label is "soft_lutpair77";
  attribute SOFT_HLUTNM of \s_axis_tdata[104]_INST_0\ : label is "soft_lutpair76";
  attribute SOFT_HLUTNM of \s_axis_tdata[105]_INST_0\ : label is "soft_lutpair76";
  attribute SOFT_HLUTNM of \s_axis_tdata[106]_INST_0\ : label is "soft_lutpair75";
  attribute SOFT_HLUTNM of \s_axis_tdata[107]_INST_0\ : label is "soft_lutpair75";
  attribute SOFT_HLUTNM of \s_axis_tdata[108]_INST_0\ : label is "soft_lutpair74";
  attribute SOFT_HLUTNM of \s_axis_tdata[109]_INST_0\ : label is "soft_lutpair74";
  attribute SOFT_HLUTNM of \s_axis_tdata[10]_INST_0\ : label is "soft_lutpair123";
  attribute SOFT_HLUTNM of \s_axis_tdata[110]_INST_0\ : label is "soft_lutpair73";
  attribute SOFT_HLUTNM of \s_axis_tdata[111]_INST_0\ : label is "soft_lutpair73";
  attribute SOFT_HLUTNM of \s_axis_tdata[112]_INST_0\ : label is "soft_lutpair72";
  attribute SOFT_HLUTNM of \s_axis_tdata[113]_INST_0\ : label is "soft_lutpair72";
  attribute SOFT_HLUTNM of \s_axis_tdata[114]_INST_0\ : label is "soft_lutpair71";
  attribute SOFT_HLUTNM of \s_axis_tdata[115]_INST_0\ : label is "soft_lutpair71";
  attribute SOFT_HLUTNM of \s_axis_tdata[116]_INST_0\ : label is "soft_lutpair70";
  attribute SOFT_HLUTNM of \s_axis_tdata[117]_INST_0\ : label is "soft_lutpair70";
  attribute SOFT_HLUTNM of \s_axis_tdata[118]_INST_0\ : label is "soft_lutpair69";
  attribute SOFT_HLUTNM of \s_axis_tdata[119]_INST_0\ : label is "soft_lutpair69";
  attribute SOFT_HLUTNM of \s_axis_tdata[11]_INST_0\ : label is "soft_lutpair122";
  attribute SOFT_HLUTNM of \s_axis_tdata[120]_INST_0\ : label is "soft_lutpair68";
  attribute SOFT_HLUTNM of \s_axis_tdata[121]_INST_0\ : label is "soft_lutpair68";
  attribute SOFT_HLUTNM of \s_axis_tdata[122]_INST_0\ : label is "soft_lutpair67";
  attribute SOFT_HLUTNM of \s_axis_tdata[123]_INST_0\ : label is "soft_lutpair67";
  attribute SOFT_HLUTNM of \s_axis_tdata[124]_INST_0\ : label is "soft_lutpair66";
  attribute SOFT_HLUTNM of \s_axis_tdata[125]_INST_0\ : label is "soft_lutpair66";
  attribute SOFT_HLUTNM of \s_axis_tdata[126]_INST_0\ : label is "soft_lutpair65";
  attribute SOFT_HLUTNM of \s_axis_tdata[127]_INST_0\ : label is "soft_lutpair65";
  attribute SOFT_HLUTNM of \s_axis_tdata[128]_INST_0\ : label is "soft_lutpair64";
  attribute SOFT_HLUTNM of \s_axis_tdata[129]_INST_0\ : label is "soft_lutpair64";
  attribute SOFT_HLUTNM of \s_axis_tdata[12]_INST_0\ : label is "soft_lutpair122";
  attribute SOFT_HLUTNM of \s_axis_tdata[130]_INST_0\ : label is "soft_lutpair63";
  attribute SOFT_HLUTNM of \s_axis_tdata[131]_INST_0\ : label is "soft_lutpair63";
  attribute SOFT_HLUTNM of \s_axis_tdata[132]_INST_0\ : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of \s_axis_tdata[133]_INST_0\ : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of \s_axis_tdata[134]_INST_0\ : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of \s_axis_tdata[135]_INST_0\ : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of \s_axis_tdata[136]_INST_0\ : label is "soft_lutpair60";
  attribute SOFT_HLUTNM of \s_axis_tdata[137]_INST_0\ : label is "soft_lutpair60";
  attribute SOFT_HLUTNM of \s_axis_tdata[138]_INST_0\ : label is "soft_lutpair59";
  attribute SOFT_HLUTNM of \s_axis_tdata[139]_INST_0\ : label is "soft_lutpair59";
  attribute SOFT_HLUTNM of \s_axis_tdata[13]_INST_0\ : label is "soft_lutpair121";
  attribute SOFT_HLUTNM of \s_axis_tdata[140]_INST_0\ : label is "soft_lutpair58";
  attribute SOFT_HLUTNM of \s_axis_tdata[141]_INST_0\ : label is "soft_lutpair58";
  attribute SOFT_HLUTNM of \s_axis_tdata[142]_INST_0\ : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of \s_axis_tdata[143]_INST_0\ : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of \s_axis_tdata[144]_INST_0\ : label is "soft_lutpair56";
  attribute SOFT_HLUTNM of \s_axis_tdata[145]_INST_0\ : label is "soft_lutpair56";
  attribute SOFT_HLUTNM of \s_axis_tdata[146]_INST_0\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \s_axis_tdata[147]_INST_0\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \s_axis_tdata[148]_INST_0\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \s_axis_tdata[149]_INST_0\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \s_axis_tdata[14]_INST_0\ : label is "soft_lutpair121";
  attribute SOFT_HLUTNM of \s_axis_tdata[150]_INST_0\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of \s_axis_tdata[151]_INST_0\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of \s_axis_tdata[152]_INST_0\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \s_axis_tdata[153]_INST_0\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \s_axis_tdata[154]_INST_0\ : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of \s_axis_tdata[155]_INST_0\ : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of \s_axis_tdata[156]_INST_0\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \s_axis_tdata[157]_INST_0\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \s_axis_tdata[158]_INST_0\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \s_axis_tdata[159]_INST_0\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \s_axis_tdata[15]_INST_0\ : label is "soft_lutpair120";
  attribute SOFT_HLUTNM of \s_axis_tdata[160]_INST_0\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \s_axis_tdata[161]_INST_0\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \s_axis_tdata[162]_INST_0\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \s_axis_tdata[163]_INST_0\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \s_axis_tdata[164]_INST_0\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \s_axis_tdata[165]_INST_0\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \s_axis_tdata[166]_INST_0\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \s_axis_tdata[167]_INST_0\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \s_axis_tdata[168]_INST_0\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \s_axis_tdata[169]_INST_0\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \s_axis_tdata[16]_INST_0\ : label is "soft_lutpair120";
  attribute SOFT_HLUTNM of \s_axis_tdata[170]_INST_0\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of \s_axis_tdata[171]_INST_0\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of \s_axis_tdata[172]_INST_0\ : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of \s_axis_tdata[173]_INST_0\ : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of \s_axis_tdata[174]_INST_0\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \s_axis_tdata[175]_INST_0\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \s_axis_tdata[176]_INST_0\ : label is "soft_lutpair40";
  attribute SOFT_HLUTNM of \s_axis_tdata[177]_INST_0\ : label is "soft_lutpair40";
  attribute SOFT_HLUTNM of \s_axis_tdata[178]_INST_0\ : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of \s_axis_tdata[179]_INST_0\ : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of \s_axis_tdata[17]_INST_0\ : label is "soft_lutpair119";
  attribute SOFT_HLUTNM of \s_axis_tdata[180]_INST_0\ : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of \s_axis_tdata[181]_INST_0\ : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of \s_axis_tdata[182]_INST_0\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \s_axis_tdata[183]_INST_0\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \s_axis_tdata[184]_INST_0\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \s_axis_tdata[185]_INST_0\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \s_axis_tdata[186]_INST_0\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \s_axis_tdata[187]_INST_0\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \s_axis_tdata[188]_INST_0\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \s_axis_tdata[189]_INST_0\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \s_axis_tdata[18]_INST_0\ : label is "soft_lutpair119";
  attribute SOFT_HLUTNM of \s_axis_tdata[190]_INST_0\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \s_axis_tdata[191]_INST_0\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \s_axis_tdata[192]_INST_0\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \s_axis_tdata[193]_INST_0\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \s_axis_tdata[194]_INST_0\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \s_axis_tdata[195]_INST_0\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \s_axis_tdata[196]_INST_0\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \s_axis_tdata[197]_INST_0\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \s_axis_tdata[198]_INST_0\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \s_axis_tdata[199]_INST_0\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \s_axis_tdata[19]_INST_0\ : label is "soft_lutpair118";
  attribute SOFT_HLUTNM of \s_axis_tdata[1]_INST_0\ : label is "soft_lutpair127";
  attribute SOFT_HLUTNM of \s_axis_tdata[200]_INST_0\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \s_axis_tdata[201]_INST_0\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \s_axis_tdata[202]_INST_0\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \s_axis_tdata[203]_INST_0\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \s_axis_tdata[204]_INST_0\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \s_axis_tdata[205]_INST_0\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \s_axis_tdata[206]_INST_0\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \s_axis_tdata[207]_INST_0\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \s_axis_tdata[208]_INST_0\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \s_axis_tdata[209]_INST_0\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \s_axis_tdata[20]_INST_0\ : label is "soft_lutpair118";
  attribute SOFT_HLUTNM of \s_axis_tdata[210]_INST_0\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \s_axis_tdata[211]_INST_0\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \s_axis_tdata[212]_INST_0\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \s_axis_tdata[213]_INST_0\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \s_axis_tdata[214]_INST_0\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \s_axis_tdata[215]_INST_0\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \s_axis_tdata[216]_INST_0\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \s_axis_tdata[217]_INST_0\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \s_axis_tdata[218]_INST_0\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \s_axis_tdata[219]_INST_0\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \s_axis_tdata[21]_INST_0\ : label is "soft_lutpair117";
  attribute SOFT_HLUTNM of \s_axis_tdata[220]_INST_0\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \s_axis_tdata[221]_INST_0\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \s_axis_tdata[222]_INST_0\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \s_axis_tdata[223]_INST_0\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \s_axis_tdata[224]_INST_0\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \s_axis_tdata[225]_INST_0\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \s_axis_tdata[226]_INST_0\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \s_axis_tdata[227]_INST_0\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \s_axis_tdata[228]_INST_0\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \s_axis_tdata[229]_INST_0\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \s_axis_tdata[22]_INST_0\ : label is "soft_lutpair117";
  attribute SOFT_HLUTNM of \s_axis_tdata[230]_INST_0\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \s_axis_tdata[231]_INST_0\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \s_axis_tdata[232]_INST_0\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \s_axis_tdata[233]_INST_0\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \s_axis_tdata[234]_INST_0\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \s_axis_tdata[235]_INST_0\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \s_axis_tdata[236]_INST_0\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \s_axis_tdata[237]_INST_0\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \s_axis_tdata[238]_INST_0\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \s_axis_tdata[239]_INST_0\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \s_axis_tdata[23]_INST_0\ : label is "soft_lutpair116";
  attribute SOFT_HLUTNM of \s_axis_tdata[240]_INST_0\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \s_axis_tdata[241]_INST_0\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \s_axis_tdata[242]_INST_0\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \s_axis_tdata[243]_INST_0\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \s_axis_tdata[244]_INST_0\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \s_axis_tdata[245]_INST_0\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \s_axis_tdata[246]_INST_0\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \s_axis_tdata[247]_INST_0\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \s_axis_tdata[248]_INST_0\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \s_axis_tdata[249]_INST_0\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \s_axis_tdata[24]_INST_0\ : label is "soft_lutpair116";
  attribute SOFT_HLUTNM of \s_axis_tdata[250]_INST_0\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \s_axis_tdata[251]_INST_0\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \s_axis_tdata[252]_INST_0\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \s_axis_tdata[253]_INST_0\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \s_axis_tdata[254]_INST_0\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \s_axis_tdata[255]_INST_0\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \s_axis_tdata[25]_INST_0\ : label is "soft_lutpair115";
  attribute SOFT_HLUTNM of \s_axis_tdata[26]_INST_0\ : label is "soft_lutpair115";
  attribute SOFT_HLUTNM of \s_axis_tdata[27]_INST_0\ : label is "soft_lutpair114";
  attribute SOFT_HLUTNM of \s_axis_tdata[28]_INST_0\ : label is "soft_lutpair114";
  attribute SOFT_HLUTNM of \s_axis_tdata[29]_INST_0\ : label is "soft_lutpair113";
  attribute SOFT_HLUTNM of \s_axis_tdata[2]_INST_0\ : label is "soft_lutpair127";
  attribute SOFT_HLUTNM of \s_axis_tdata[30]_INST_0\ : label is "soft_lutpair113";
  attribute SOFT_HLUTNM of \s_axis_tdata[31]_INST_0\ : label is "soft_lutpair112";
  attribute SOFT_HLUTNM of \s_axis_tdata[32]_INST_0\ : label is "soft_lutpair112";
  attribute SOFT_HLUTNM of \s_axis_tdata[33]_INST_0\ : label is "soft_lutpair111";
  attribute SOFT_HLUTNM of \s_axis_tdata[34]_INST_0\ : label is "soft_lutpair111";
  attribute SOFT_HLUTNM of \s_axis_tdata[35]_INST_0\ : label is "soft_lutpair110";
  attribute SOFT_HLUTNM of \s_axis_tdata[36]_INST_0\ : label is "soft_lutpair110";
  attribute SOFT_HLUTNM of \s_axis_tdata[37]_INST_0\ : label is "soft_lutpair109";
  attribute SOFT_HLUTNM of \s_axis_tdata[38]_INST_0\ : label is "soft_lutpair109";
  attribute SOFT_HLUTNM of \s_axis_tdata[39]_INST_0\ : label is "soft_lutpair108";
  attribute SOFT_HLUTNM of \s_axis_tdata[3]_INST_0\ : label is "soft_lutpair126";
  attribute SOFT_HLUTNM of \s_axis_tdata[40]_INST_0\ : label is "soft_lutpair108";
  attribute SOFT_HLUTNM of \s_axis_tdata[41]_INST_0\ : label is "soft_lutpair107";
  attribute SOFT_HLUTNM of \s_axis_tdata[42]_INST_0\ : label is "soft_lutpair107";
  attribute SOFT_HLUTNM of \s_axis_tdata[43]_INST_0\ : label is "soft_lutpair106";
  attribute SOFT_HLUTNM of \s_axis_tdata[44]_INST_0\ : label is "soft_lutpair106";
  attribute SOFT_HLUTNM of \s_axis_tdata[45]_INST_0\ : label is "soft_lutpair105";
  attribute SOFT_HLUTNM of \s_axis_tdata[46]_INST_0\ : label is "soft_lutpair105";
  attribute SOFT_HLUTNM of \s_axis_tdata[47]_INST_0\ : label is "soft_lutpair104";
  attribute SOFT_HLUTNM of \s_axis_tdata[48]_INST_0\ : label is "soft_lutpair104";
  attribute SOFT_HLUTNM of \s_axis_tdata[49]_INST_0\ : label is "soft_lutpair103";
  attribute SOFT_HLUTNM of \s_axis_tdata[4]_INST_0\ : label is "soft_lutpair126";
  attribute SOFT_HLUTNM of \s_axis_tdata[50]_INST_0\ : label is "soft_lutpair103";
  attribute SOFT_HLUTNM of \s_axis_tdata[51]_INST_0\ : label is "soft_lutpair102";
  attribute SOFT_HLUTNM of \s_axis_tdata[52]_INST_0\ : label is "soft_lutpair102";
  attribute SOFT_HLUTNM of \s_axis_tdata[53]_INST_0\ : label is "soft_lutpair101";
  attribute SOFT_HLUTNM of \s_axis_tdata[54]_INST_0\ : label is "soft_lutpair101";
  attribute SOFT_HLUTNM of \s_axis_tdata[55]_INST_0\ : label is "soft_lutpair100";
  attribute SOFT_HLUTNM of \s_axis_tdata[56]_INST_0\ : label is "soft_lutpair100";
  attribute SOFT_HLUTNM of \s_axis_tdata[57]_INST_0\ : label is "soft_lutpair99";
  attribute SOFT_HLUTNM of \s_axis_tdata[58]_INST_0\ : label is "soft_lutpair99";
  attribute SOFT_HLUTNM of \s_axis_tdata[59]_INST_0\ : label is "soft_lutpair98";
  attribute SOFT_HLUTNM of \s_axis_tdata[5]_INST_0\ : label is "soft_lutpair125";
  attribute SOFT_HLUTNM of \s_axis_tdata[60]_INST_0\ : label is "soft_lutpair98";
  attribute SOFT_HLUTNM of \s_axis_tdata[61]_INST_0\ : label is "soft_lutpair97";
  attribute SOFT_HLUTNM of \s_axis_tdata[62]_INST_0\ : label is "soft_lutpair97";
  attribute SOFT_HLUTNM of \s_axis_tdata[63]_INST_0\ : label is "soft_lutpair128";
  attribute SOFT_HLUTNM of \s_axis_tdata[64]_INST_0\ : label is "soft_lutpair96";
  attribute SOFT_HLUTNM of \s_axis_tdata[65]_INST_0\ : label is "soft_lutpair96";
  attribute SOFT_HLUTNM of \s_axis_tdata[66]_INST_0\ : label is "soft_lutpair95";
  attribute SOFT_HLUTNM of \s_axis_tdata[67]_INST_0\ : label is "soft_lutpair95";
  attribute SOFT_HLUTNM of \s_axis_tdata[68]_INST_0\ : label is "soft_lutpair94";
  attribute SOFT_HLUTNM of \s_axis_tdata[69]_INST_0\ : label is "soft_lutpair94";
  attribute SOFT_HLUTNM of \s_axis_tdata[6]_INST_0\ : label is "soft_lutpair125";
  attribute SOFT_HLUTNM of \s_axis_tdata[70]_INST_0\ : label is "soft_lutpair93";
  attribute SOFT_HLUTNM of \s_axis_tdata[71]_INST_0\ : label is "soft_lutpair93";
  attribute SOFT_HLUTNM of \s_axis_tdata[72]_INST_0\ : label is "soft_lutpair92";
  attribute SOFT_HLUTNM of \s_axis_tdata[73]_INST_0\ : label is "soft_lutpair92";
  attribute SOFT_HLUTNM of \s_axis_tdata[74]_INST_0\ : label is "soft_lutpair91";
  attribute SOFT_HLUTNM of \s_axis_tdata[75]_INST_0\ : label is "soft_lutpair91";
  attribute SOFT_HLUTNM of \s_axis_tdata[76]_INST_0\ : label is "soft_lutpair90";
  attribute SOFT_HLUTNM of \s_axis_tdata[77]_INST_0\ : label is "soft_lutpair90";
  attribute SOFT_HLUTNM of \s_axis_tdata[78]_INST_0\ : label is "soft_lutpair89";
  attribute SOFT_HLUTNM of \s_axis_tdata[79]_INST_0\ : label is "soft_lutpair89";
  attribute SOFT_HLUTNM of \s_axis_tdata[7]_INST_0\ : label is "soft_lutpair124";
  attribute SOFT_HLUTNM of \s_axis_tdata[80]_INST_0\ : label is "soft_lutpair88";
  attribute SOFT_HLUTNM of \s_axis_tdata[81]_INST_0\ : label is "soft_lutpair88";
  attribute SOFT_HLUTNM of \s_axis_tdata[82]_INST_0\ : label is "soft_lutpair87";
  attribute SOFT_HLUTNM of \s_axis_tdata[83]_INST_0\ : label is "soft_lutpair87";
  attribute SOFT_HLUTNM of \s_axis_tdata[84]_INST_0\ : label is "soft_lutpair86";
  attribute SOFT_HLUTNM of \s_axis_tdata[85]_INST_0\ : label is "soft_lutpair86";
  attribute SOFT_HLUTNM of \s_axis_tdata[86]_INST_0\ : label is "soft_lutpair85";
  attribute SOFT_HLUTNM of \s_axis_tdata[87]_INST_0\ : label is "soft_lutpair85";
  attribute SOFT_HLUTNM of \s_axis_tdata[88]_INST_0\ : label is "soft_lutpair84";
  attribute SOFT_HLUTNM of \s_axis_tdata[89]_INST_0\ : label is "soft_lutpair84";
  attribute SOFT_HLUTNM of \s_axis_tdata[8]_INST_0\ : label is "soft_lutpair124";
  attribute SOFT_HLUTNM of \s_axis_tdata[90]_INST_0\ : label is "soft_lutpair83";
  attribute SOFT_HLUTNM of \s_axis_tdata[91]_INST_0\ : label is "soft_lutpair83";
  attribute SOFT_HLUTNM of \s_axis_tdata[92]_INST_0\ : label is "soft_lutpair82";
  attribute SOFT_HLUTNM of \s_axis_tdata[93]_INST_0\ : label is "soft_lutpair82";
  attribute SOFT_HLUTNM of \s_axis_tdata[94]_INST_0\ : label is "soft_lutpair81";
  attribute SOFT_HLUTNM of \s_axis_tdata[95]_INST_0\ : label is "soft_lutpair81";
  attribute SOFT_HLUTNM of \s_axis_tdata[96]_INST_0\ : label is "soft_lutpair80";
  attribute SOFT_HLUTNM of \s_axis_tdata[97]_INST_0\ : label is "soft_lutpair80";
  attribute SOFT_HLUTNM of \s_axis_tdata[98]_INST_0\ : label is "soft_lutpair79";
  attribute SOFT_HLUTNM of \s_axis_tdata[99]_INST_0\ : label is "soft_lutpair79";
  attribute SOFT_HLUTNM of \s_axis_tdata[9]_INST_0\ : label is "soft_lutpair123";
  attribute SOFT_HLUTNM of s_axis_tlast_INST_0_i_2 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of s_axis_tvalid_INST_0 : label is "soft_lutpair130";
  attribute SOFT_HLUTNM of stat_FSM_i_2 : label is "soft_lutpair131";
begin
focus_tkeep_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2F222F2F20222020"
    )
        port map (
      I0 => focus_tlast_i_3_n_0,
      I1 => focus_tlast_i_2_n_0,
      I2 => focus_tkeep_i_2_n_0,
      I3 => stat_FSM,
      I4 => stat_FSM_i_2_n_0,
      I5 => focus_tkeep,
      O => focus_tkeep_i_1_n_0
    );
focus_tkeep_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5555555055555553"
    )
        port map (
      I0 => stat_FSM,
      I1 => focus_tlast_i_5_n_0,
      I2 => s_axis_tlast_INST_0_i_3_n_0,
      I3 => s_axis_tlast_INST_0_i_2_n_0,
      I4 => \packet_size_counter[15]_i_4_n_0\,
      I5 => focus_tlast_i_4_n_0,
      O => focus_tkeep_i_2_n_0
    );
focus_tkeep_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => focus_tkeep_i_1_n_0,
      Q => focus_tkeep,
      R => '0'
    );
focus_tlast_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000F000044444444"
    )
        port map (
      I0 => focus_tlast_i_2_n_0,
      I1 => focus_tlast_i_3_n_0,
      I2 => s_axis_tlast_INST_0_i_1_n_0,
      I3 => focus_tlast_i_4_n_0,
      I4 => focus_tlast_i_5_n_0,
      I5 => stat_FSM,
      O => focus_tlast0
    );
focus_tlast_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => s_axis_tlast_INST_0_i_3_n_0,
      I1 => packet_size_counter_reg(12),
      I2 => packet_size_counter_reg(13),
      I3 => packet_size_counter_reg(9),
      I4 => packet_size_counter_reg(11),
      O => focus_tlast_i_2_n_0
    );
focus_tlast_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0040000000000000"
    )
        port map (
      I0 => focus_tlast_i_6_n_0,
      I1 => packet_size_counter_reg(7),
      I2 => packet_size_counter_reg(10),
      I3 => packet_size_counter_reg(0),
      I4 => packet_size_counter_reg(3),
      I5 => packet_size_counter_reg(5),
      O => focus_tlast_i_3_n_0
    );
focus_tlast_i_4: unisim.vcomponents.LUT3
    generic map(
      INIT => X"7F"
    )
        port map (
      I0 => packet_size_counter_reg(2),
      I1 => packet_size_counter_reg(1),
      I2 => packet_size_counter_reg(0),
      O => focus_tlast_i_4_n_0
    );
focus_tlast_i_5: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1F"
    )
        port map (
      I0 => s_axis_tvalid_packetnouf_reg_n_0,
      I1 => m_axis_tvalid,
      I2 => s_axis_tready,
      O => focus_tlast_i_5_n_0
    );
focus_tlast_i_6: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => packet_size_counter_reg(1),
      I1 => packet_size_counter_reg(2),
      O => focus_tlast_i_6_n_0
    );
focus_tlast_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => focus_tlast0,
      Q => focus_tlast_reg_n_0,
      R => '0'
    );
m_axis_tready_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => s_axis_tready,
      I1 => stat_FSM,
      O => m_axis_tready
    );
packet_size_counter0_carry: unisim.vcomponents.CARRY8
     port map (
      CI => packet_size_counter_reg(0),
      CI_TOP => '0',
      CO(7) => packet_size_counter0_carry_n_0,
      CO(6) => packet_size_counter0_carry_n_1,
      CO(5) => packet_size_counter0_carry_n_2,
      CO(4) => packet_size_counter0_carry_n_3,
      CO(3) => packet_size_counter0_carry_n_4,
      CO(2) => packet_size_counter0_carry_n_5,
      CO(1) => packet_size_counter0_carry_n_6,
      CO(0) => packet_size_counter0_carry_n_7,
      DI(7 downto 0) => B"00000000",
      O(7 downto 0) => p_0_in(8 downto 1),
      S(7 downto 0) => packet_size_counter_reg(8 downto 1)
    );
\packet_size_counter0_carry__0\: unisim.vcomponents.CARRY8
     port map (
      CI => packet_size_counter0_carry_n_0,
      CI_TOP => '0',
      CO(7 downto 6) => \NLW_packet_size_counter0_carry__0_CO_UNCONNECTED\(7 downto 6),
      CO(5) => \packet_size_counter0_carry__0_n_2\,
      CO(4) => \packet_size_counter0_carry__0_n_3\,
      CO(3) => \packet_size_counter0_carry__0_n_4\,
      CO(2) => \packet_size_counter0_carry__0_n_5\,
      CO(1) => \packet_size_counter0_carry__0_n_6\,
      CO(0) => \packet_size_counter0_carry__0_n_7\,
      DI(7 downto 0) => B"00000000",
      O(7) => \NLW_packet_size_counter0_carry__0_O_UNCONNECTED\(7),
      O(6 downto 0) => p_0_in(15 downto 9),
      S(7) => '0',
      S(6 downto 0) => packet_size_counter_reg(15 downto 9)
    );
\packet_size_counter[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => packet_size_counter_reg(0),
      O => \packet_size_counter[0]_i_1_n_0\
    );
\packet_size_counter[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EA00EA00EA000000"
    )
        port map (
      I0 => focus_tlast_reg_n_0,
      I1 => \packet_size_counter[15]_i_3_n_0\,
      I2 => m_axis_tlast,
      I3 => s_axis_tready,
      I4 => m_axis_tvalid,
      I5 => s_axis_tvalid_packetnouf_reg_n_0,
      O => s_axis_tvalid_packetnouf
    );
\packet_size_counter[15]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A8"
    )
        port map (
      I0 => s_axis_tready,
      I1 => m_axis_tvalid,
      I2 => s_axis_tvalid_packetnouf_reg_n_0,
      O => p_3_in
    );
\packet_size_counter[15]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFEFEFEFEFEFE"
    )
        port map (
      I0 => s_axis_tlast_INST_0_i_3_n_0,
      I1 => s_axis_tlast_INST_0_i_2_n_0,
      I2 => \packet_size_counter[15]_i_4_n_0\,
      I3 => packet_size_counter_reg(0),
      I4 => packet_size_counter_reg(1),
      I5 => packet_size_counter_reg(2),
      O => \packet_size_counter[15]_i_3_n_0\
    );
\packet_size_counter[15]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => packet_size_counter_reg(5),
      I1 => packet_size_counter_reg(3),
      I2 => packet_size_counter_reg(10),
      I3 => packet_size_counter_reg(7),
      O => \packet_size_counter[15]_i_4_n_0\
    );
\packet_size_counter_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => p_3_in,
      D => \packet_size_counter[0]_i_1_n_0\,
      Q => packet_size_counter_reg(0),
      R => s_axis_tvalid_packetnouf
    );
\packet_size_counter_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => p_3_in,
      D => p_0_in(10),
      Q => packet_size_counter_reg(10),
      R => s_axis_tvalid_packetnouf
    );
\packet_size_counter_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => p_3_in,
      D => p_0_in(11),
      Q => packet_size_counter_reg(11),
      R => s_axis_tvalid_packetnouf
    );
\packet_size_counter_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => p_3_in,
      D => p_0_in(12),
      Q => packet_size_counter_reg(12),
      R => s_axis_tvalid_packetnouf
    );
\packet_size_counter_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => p_3_in,
      D => p_0_in(13),
      Q => packet_size_counter_reg(13),
      R => s_axis_tvalid_packetnouf
    );
\packet_size_counter_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => p_3_in,
      D => p_0_in(14),
      Q => packet_size_counter_reg(14),
      R => s_axis_tvalid_packetnouf
    );
\packet_size_counter_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => p_3_in,
      D => p_0_in(15),
      Q => packet_size_counter_reg(15),
      R => s_axis_tvalid_packetnouf
    );
\packet_size_counter_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => p_3_in,
      D => p_0_in(1),
      Q => packet_size_counter_reg(1),
      R => s_axis_tvalid_packetnouf
    );
\packet_size_counter_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => p_3_in,
      D => p_0_in(2),
      Q => packet_size_counter_reg(2),
      R => s_axis_tvalid_packetnouf
    );
\packet_size_counter_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => p_3_in,
      D => p_0_in(3),
      Q => packet_size_counter_reg(3),
      R => s_axis_tvalid_packetnouf
    );
\packet_size_counter_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => p_3_in,
      D => p_0_in(4),
      Q => packet_size_counter_reg(4),
      R => s_axis_tvalid_packetnouf
    );
\packet_size_counter_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => p_3_in,
      D => p_0_in(5),
      Q => packet_size_counter_reg(5),
      R => s_axis_tvalid_packetnouf
    );
\packet_size_counter_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => p_3_in,
      D => p_0_in(6),
      Q => packet_size_counter_reg(6),
      R => s_axis_tvalid_packetnouf
    );
\packet_size_counter_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => p_3_in,
      D => p_0_in(7),
      Q => packet_size_counter_reg(7),
      R => s_axis_tvalid_packetnouf
    );
\packet_size_counter_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => p_3_in,
      D => p_0_in(8),
      Q => packet_size_counter_reg(8),
      R => s_axis_tvalid_packetnouf
    );
\packet_size_counter_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => p_3_in,
      D => p_0_in(9),
      Q => packet_size_counter_reg(9),
      R => s_axis_tvalid_packetnouf
    );
\s_axis_tdata[0]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(0),
      O => s_axis_tdata(0)
    );
\s_axis_tdata[100]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(100),
      O => s_axis_tdata(100)
    );
\s_axis_tdata[101]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(101),
      O => s_axis_tdata(101)
    );
\s_axis_tdata[102]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(102),
      O => s_axis_tdata(102)
    );
\s_axis_tdata[103]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(103),
      O => s_axis_tdata(103)
    );
\s_axis_tdata[104]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(104),
      O => s_axis_tdata(104)
    );
\s_axis_tdata[105]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(105),
      O => s_axis_tdata(105)
    );
\s_axis_tdata[106]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(106),
      O => s_axis_tdata(106)
    );
\s_axis_tdata[107]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(107),
      O => s_axis_tdata(107)
    );
\s_axis_tdata[108]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(108),
      O => s_axis_tdata(108)
    );
\s_axis_tdata[109]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(109),
      O => s_axis_tdata(109)
    );
\s_axis_tdata[10]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(10),
      O => s_axis_tdata(10)
    );
\s_axis_tdata[110]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(110),
      O => s_axis_tdata(110)
    );
\s_axis_tdata[111]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(111),
      O => s_axis_tdata(111)
    );
\s_axis_tdata[112]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(112),
      O => s_axis_tdata(112)
    );
\s_axis_tdata[113]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(113),
      O => s_axis_tdata(113)
    );
\s_axis_tdata[114]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(114),
      O => s_axis_tdata(114)
    );
\s_axis_tdata[115]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(115),
      O => s_axis_tdata(115)
    );
\s_axis_tdata[116]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(116),
      O => s_axis_tdata(116)
    );
\s_axis_tdata[117]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(117),
      O => s_axis_tdata(117)
    );
\s_axis_tdata[118]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(118),
      O => s_axis_tdata(118)
    );
\s_axis_tdata[119]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(119),
      O => s_axis_tdata(119)
    );
\s_axis_tdata[11]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(11),
      O => s_axis_tdata(11)
    );
\s_axis_tdata[120]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(120),
      O => s_axis_tdata(120)
    );
\s_axis_tdata[121]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(121),
      O => s_axis_tdata(121)
    );
\s_axis_tdata[122]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(122),
      O => s_axis_tdata(122)
    );
\s_axis_tdata[123]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(123),
      O => s_axis_tdata(123)
    );
\s_axis_tdata[124]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(124),
      O => s_axis_tdata(124)
    );
\s_axis_tdata[125]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(125),
      O => s_axis_tdata(125)
    );
\s_axis_tdata[126]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(126),
      O => s_axis_tdata(126)
    );
\s_axis_tdata[127]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(127),
      O => s_axis_tdata(127)
    );
\s_axis_tdata[128]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(128),
      O => s_axis_tdata(128)
    );
\s_axis_tdata[129]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(129),
      O => s_axis_tdata(129)
    );
\s_axis_tdata[12]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(12),
      O => s_axis_tdata(12)
    );
\s_axis_tdata[130]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(130),
      O => s_axis_tdata(130)
    );
\s_axis_tdata[131]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(131),
      O => s_axis_tdata(131)
    );
\s_axis_tdata[132]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(132),
      O => s_axis_tdata(132)
    );
\s_axis_tdata[133]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(133),
      O => s_axis_tdata(133)
    );
\s_axis_tdata[134]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(134),
      O => s_axis_tdata(134)
    );
\s_axis_tdata[135]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(135),
      O => s_axis_tdata(135)
    );
\s_axis_tdata[136]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(136),
      O => s_axis_tdata(136)
    );
\s_axis_tdata[137]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(137),
      O => s_axis_tdata(137)
    );
\s_axis_tdata[138]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(138),
      O => s_axis_tdata(138)
    );
\s_axis_tdata[139]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(139),
      O => s_axis_tdata(139)
    );
\s_axis_tdata[13]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(13),
      O => s_axis_tdata(13)
    );
\s_axis_tdata[140]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(140),
      O => s_axis_tdata(140)
    );
\s_axis_tdata[141]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(141),
      O => s_axis_tdata(141)
    );
\s_axis_tdata[142]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(142),
      O => s_axis_tdata(142)
    );
\s_axis_tdata[143]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(143),
      O => s_axis_tdata(143)
    );
\s_axis_tdata[144]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(144),
      O => s_axis_tdata(144)
    );
\s_axis_tdata[145]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(145),
      O => s_axis_tdata(145)
    );
\s_axis_tdata[146]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(146),
      O => s_axis_tdata(146)
    );
\s_axis_tdata[147]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(147),
      O => s_axis_tdata(147)
    );
\s_axis_tdata[148]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(148),
      O => s_axis_tdata(148)
    );
\s_axis_tdata[149]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(149),
      O => s_axis_tdata(149)
    );
\s_axis_tdata[14]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(14),
      O => s_axis_tdata(14)
    );
\s_axis_tdata[150]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(150),
      O => s_axis_tdata(150)
    );
\s_axis_tdata[151]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(151),
      O => s_axis_tdata(151)
    );
\s_axis_tdata[152]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(152),
      O => s_axis_tdata(152)
    );
\s_axis_tdata[153]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(153),
      O => s_axis_tdata(153)
    );
\s_axis_tdata[154]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(154),
      O => s_axis_tdata(154)
    );
\s_axis_tdata[155]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(155),
      O => s_axis_tdata(155)
    );
\s_axis_tdata[156]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(156),
      O => s_axis_tdata(156)
    );
\s_axis_tdata[157]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(157),
      O => s_axis_tdata(157)
    );
\s_axis_tdata[158]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(158),
      O => s_axis_tdata(158)
    );
\s_axis_tdata[159]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(159),
      O => s_axis_tdata(159)
    );
\s_axis_tdata[15]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(15),
      O => s_axis_tdata(15)
    );
\s_axis_tdata[160]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(160),
      O => s_axis_tdata(160)
    );
\s_axis_tdata[161]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(161),
      O => s_axis_tdata(161)
    );
\s_axis_tdata[162]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(162),
      O => s_axis_tdata(162)
    );
\s_axis_tdata[163]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(163),
      O => s_axis_tdata(163)
    );
\s_axis_tdata[164]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(164),
      O => s_axis_tdata(164)
    );
\s_axis_tdata[165]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(165),
      O => s_axis_tdata(165)
    );
\s_axis_tdata[166]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(166),
      O => s_axis_tdata(166)
    );
\s_axis_tdata[167]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(167),
      O => s_axis_tdata(167)
    );
\s_axis_tdata[168]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(168),
      O => s_axis_tdata(168)
    );
\s_axis_tdata[169]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(169),
      O => s_axis_tdata(169)
    );
\s_axis_tdata[16]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(16),
      O => s_axis_tdata(16)
    );
\s_axis_tdata[170]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(170),
      O => s_axis_tdata(170)
    );
\s_axis_tdata[171]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(171),
      O => s_axis_tdata(171)
    );
\s_axis_tdata[172]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(172),
      O => s_axis_tdata(172)
    );
\s_axis_tdata[173]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(173),
      O => s_axis_tdata(173)
    );
\s_axis_tdata[174]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(174),
      O => s_axis_tdata(174)
    );
\s_axis_tdata[175]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(175),
      O => s_axis_tdata(175)
    );
\s_axis_tdata[176]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(176),
      O => s_axis_tdata(176)
    );
\s_axis_tdata[177]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(177),
      O => s_axis_tdata(177)
    );
\s_axis_tdata[178]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(178),
      O => s_axis_tdata(178)
    );
\s_axis_tdata[179]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(179),
      O => s_axis_tdata(179)
    );
\s_axis_tdata[17]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(17),
      O => s_axis_tdata(17)
    );
\s_axis_tdata[180]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(180),
      O => s_axis_tdata(180)
    );
\s_axis_tdata[181]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(181),
      O => s_axis_tdata(181)
    );
\s_axis_tdata[182]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(182),
      O => s_axis_tdata(182)
    );
\s_axis_tdata[183]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(183),
      O => s_axis_tdata(183)
    );
\s_axis_tdata[184]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(184),
      O => s_axis_tdata(184)
    );
\s_axis_tdata[185]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(185),
      O => s_axis_tdata(185)
    );
\s_axis_tdata[186]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(186),
      O => s_axis_tdata(186)
    );
\s_axis_tdata[187]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(187),
      O => s_axis_tdata(187)
    );
\s_axis_tdata[188]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(188),
      O => s_axis_tdata(188)
    );
\s_axis_tdata[189]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(189),
      O => s_axis_tdata(189)
    );
\s_axis_tdata[18]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(18),
      O => s_axis_tdata(18)
    );
\s_axis_tdata[190]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(190),
      O => s_axis_tdata(190)
    );
\s_axis_tdata[191]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(191),
      O => s_axis_tdata(191)
    );
\s_axis_tdata[192]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(192),
      O => s_axis_tdata(192)
    );
\s_axis_tdata[193]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(193),
      O => s_axis_tdata(193)
    );
\s_axis_tdata[194]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(194),
      O => s_axis_tdata(194)
    );
\s_axis_tdata[195]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(195),
      O => s_axis_tdata(195)
    );
\s_axis_tdata[196]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(196),
      O => s_axis_tdata(196)
    );
\s_axis_tdata[197]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(197),
      O => s_axis_tdata(197)
    );
\s_axis_tdata[198]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(198),
      O => s_axis_tdata(198)
    );
\s_axis_tdata[199]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(199),
      O => s_axis_tdata(199)
    );
\s_axis_tdata[19]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(19),
      O => s_axis_tdata(19)
    );
\s_axis_tdata[1]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(1),
      O => s_axis_tdata(1)
    );
\s_axis_tdata[200]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(200),
      O => s_axis_tdata(200)
    );
\s_axis_tdata[201]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(201),
      O => s_axis_tdata(201)
    );
\s_axis_tdata[202]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(202),
      O => s_axis_tdata(202)
    );
\s_axis_tdata[203]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(203),
      O => s_axis_tdata(203)
    );
\s_axis_tdata[204]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(204),
      O => s_axis_tdata(204)
    );
\s_axis_tdata[205]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(205),
      O => s_axis_tdata(205)
    );
\s_axis_tdata[206]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(206),
      O => s_axis_tdata(206)
    );
\s_axis_tdata[207]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(207),
      O => s_axis_tdata(207)
    );
\s_axis_tdata[208]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(208),
      O => s_axis_tdata(208)
    );
\s_axis_tdata[209]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(209),
      O => s_axis_tdata(209)
    );
\s_axis_tdata[20]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(20),
      O => s_axis_tdata(20)
    );
\s_axis_tdata[210]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(210),
      O => s_axis_tdata(210)
    );
\s_axis_tdata[211]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(211),
      O => s_axis_tdata(211)
    );
\s_axis_tdata[212]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(212),
      O => s_axis_tdata(212)
    );
\s_axis_tdata[213]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(213),
      O => s_axis_tdata(213)
    );
\s_axis_tdata[214]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(214),
      O => s_axis_tdata(214)
    );
\s_axis_tdata[215]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(215),
      O => s_axis_tdata(215)
    );
\s_axis_tdata[216]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(216),
      O => s_axis_tdata(216)
    );
\s_axis_tdata[217]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(217),
      O => s_axis_tdata(217)
    );
\s_axis_tdata[218]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(218),
      O => s_axis_tdata(218)
    );
\s_axis_tdata[219]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(219),
      O => s_axis_tdata(219)
    );
\s_axis_tdata[21]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(21),
      O => s_axis_tdata(21)
    );
\s_axis_tdata[220]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(220),
      O => s_axis_tdata(220)
    );
\s_axis_tdata[221]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(221),
      O => s_axis_tdata(221)
    );
\s_axis_tdata[222]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(222),
      O => s_axis_tdata(222)
    );
\s_axis_tdata[223]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(223),
      O => s_axis_tdata(223)
    );
\s_axis_tdata[224]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(224),
      O => s_axis_tdata(224)
    );
\s_axis_tdata[225]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(225),
      O => s_axis_tdata(225)
    );
\s_axis_tdata[226]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(226),
      O => s_axis_tdata(226)
    );
\s_axis_tdata[227]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(227),
      O => s_axis_tdata(227)
    );
\s_axis_tdata[228]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(228),
      O => s_axis_tdata(228)
    );
\s_axis_tdata[229]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(229),
      O => s_axis_tdata(229)
    );
\s_axis_tdata[22]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(22),
      O => s_axis_tdata(22)
    );
\s_axis_tdata[230]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(230),
      O => s_axis_tdata(230)
    );
\s_axis_tdata[231]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(231),
      O => s_axis_tdata(231)
    );
\s_axis_tdata[232]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(232),
      O => s_axis_tdata(232)
    );
\s_axis_tdata[233]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(233),
      O => s_axis_tdata(233)
    );
\s_axis_tdata[234]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(234),
      O => s_axis_tdata(234)
    );
\s_axis_tdata[235]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(235),
      O => s_axis_tdata(235)
    );
\s_axis_tdata[236]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(236),
      O => s_axis_tdata(236)
    );
\s_axis_tdata[237]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(237),
      O => s_axis_tdata(237)
    );
\s_axis_tdata[238]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(238),
      O => s_axis_tdata(238)
    );
\s_axis_tdata[239]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(239),
      O => s_axis_tdata(239)
    );
\s_axis_tdata[23]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(23),
      O => s_axis_tdata(23)
    );
\s_axis_tdata[240]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(240),
      O => s_axis_tdata(240)
    );
\s_axis_tdata[241]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(241),
      O => s_axis_tdata(241)
    );
\s_axis_tdata[242]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(242),
      O => s_axis_tdata(242)
    );
\s_axis_tdata[243]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(243),
      O => s_axis_tdata(243)
    );
\s_axis_tdata[244]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(244),
      O => s_axis_tdata(244)
    );
\s_axis_tdata[245]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(245),
      O => s_axis_tdata(245)
    );
\s_axis_tdata[246]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(246),
      O => s_axis_tdata(246)
    );
\s_axis_tdata[247]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(247),
      O => s_axis_tdata(247)
    );
\s_axis_tdata[248]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(248),
      O => s_axis_tdata(248)
    );
\s_axis_tdata[249]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(249),
      O => s_axis_tdata(249)
    );
\s_axis_tdata[24]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(24),
      O => s_axis_tdata(24)
    );
\s_axis_tdata[250]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(250),
      O => s_axis_tdata(250)
    );
\s_axis_tdata[251]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(251),
      O => s_axis_tdata(251)
    );
\s_axis_tdata[252]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(252),
      O => s_axis_tdata(252)
    );
\s_axis_tdata[253]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(253),
      O => s_axis_tdata(253)
    );
\s_axis_tdata[254]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(254),
      O => s_axis_tdata(254)
    );
\s_axis_tdata[255]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(255),
      O => s_axis_tdata(255)
    );
\s_axis_tdata[25]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(25),
      O => s_axis_tdata(25)
    );
\s_axis_tdata[26]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(26),
      O => s_axis_tdata(26)
    );
\s_axis_tdata[27]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(27),
      O => s_axis_tdata(27)
    );
\s_axis_tdata[28]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(28),
      O => s_axis_tdata(28)
    );
\s_axis_tdata[29]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(29),
      O => s_axis_tdata(29)
    );
\s_axis_tdata[2]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(2),
      O => s_axis_tdata(2)
    );
\s_axis_tdata[30]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(30),
      O => s_axis_tdata(30)
    );
\s_axis_tdata[31]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(31),
      O => s_axis_tdata(31)
    );
\s_axis_tdata[32]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(32),
      O => s_axis_tdata(32)
    );
\s_axis_tdata[33]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(33),
      O => s_axis_tdata(33)
    );
\s_axis_tdata[34]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(34),
      O => s_axis_tdata(34)
    );
\s_axis_tdata[35]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(35),
      O => s_axis_tdata(35)
    );
\s_axis_tdata[36]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(36),
      O => s_axis_tdata(36)
    );
\s_axis_tdata[37]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(37),
      O => s_axis_tdata(37)
    );
\s_axis_tdata[38]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(38),
      O => s_axis_tdata(38)
    );
\s_axis_tdata[39]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(39),
      O => s_axis_tdata(39)
    );
\s_axis_tdata[3]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(3),
      O => s_axis_tdata(3)
    );
\s_axis_tdata[40]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(40),
      O => s_axis_tdata(40)
    );
\s_axis_tdata[41]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(41),
      O => s_axis_tdata(41)
    );
\s_axis_tdata[42]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(42),
      O => s_axis_tdata(42)
    );
\s_axis_tdata[43]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(43),
      O => s_axis_tdata(43)
    );
\s_axis_tdata[44]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(44),
      O => s_axis_tdata(44)
    );
\s_axis_tdata[45]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(45),
      O => s_axis_tdata(45)
    );
\s_axis_tdata[46]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(46),
      O => s_axis_tdata(46)
    );
\s_axis_tdata[47]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(47),
      O => s_axis_tdata(47)
    );
\s_axis_tdata[48]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(48),
      O => s_axis_tdata(48)
    );
\s_axis_tdata[49]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(49),
      O => s_axis_tdata(49)
    );
\s_axis_tdata[4]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(4),
      O => s_axis_tdata(4)
    );
\s_axis_tdata[50]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(50),
      O => s_axis_tdata(50)
    );
\s_axis_tdata[51]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(51),
      O => s_axis_tdata(51)
    );
\s_axis_tdata[52]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(52),
      O => s_axis_tdata(52)
    );
\s_axis_tdata[53]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(53),
      O => s_axis_tdata(53)
    );
\s_axis_tdata[54]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(54),
      O => s_axis_tdata(54)
    );
\s_axis_tdata[55]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(55),
      O => s_axis_tdata(55)
    );
\s_axis_tdata[56]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(56),
      O => s_axis_tdata(56)
    );
\s_axis_tdata[57]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(57),
      O => s_axis_tdata(57)
    );
\s_axis_tdata[58]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(58),
      O => s_axis_tdata(58)
    );
\s_axis_tdata[59]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(59),
      O => s_axis_tdata(59)
    );
\s_axis_tdata[5]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(5),
      O => s_axis_tdata(5)
    );
\s_axis_tdata[60]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(60),
      O => s_axis_tdata(60)
    );
\s_axis_tdata[61]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(61),
      O => s_axis_tdata(61)
    );
\s_axis_tdata[62]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(62),
      O => s_axis_tdata(62)
    );
\s_axis_tdata[63]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F8"
    )
        port map (
      I0 => m_axis_tvalid,
      I1 => m_axis_tdata(63),
      I2 => stat_FSM,
      O => s_axis_tdata(63)
    );
\s_axis_tdata[64]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(64),
      O => s_axis_tdata(64)
    );
\s_axis_tdata[65]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(65),
      O => s_axis_tdata(65)
    );
\s_axis_tdata[66]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(66),
      O => s_axis_tdata(66)
    );
\s_axis_tdata[67]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(67),
      O => s_axis_tdata(67)
    );
\s_axis_tdata[68]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(68),
      O => s_axis_tdata(68)
    );
\s_axis_tdata[69]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(69),
      O => s_axis_tdata(69)
    );
\s_axis_tdata[6]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(6),
      O => s_axis_tdata(6)
    );
\s_axis_tdata[70]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(70),
      O => s_axis_tdata(70)
    );
\s_axis_tdata[71]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(71),
      O => s_axis_tdata(71)
    );
\s_axis_tdata[72]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(72),
      O => s_axis_tdata(72)
    );
\s_axis_tdata[73]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(73),
      O => s_axis_tdata(73)
    );
\s_axis_tdata[74]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(74),
      O => s_axis_tdata(74)
    );
\s_axis_tdata[75]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(75),
      O => s_axis_tdata(75)
    );
\s_axis_tdata[76]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(76),
      O => s_axis_tdata(76)
    );
\s_axis_tdata[77]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(77),
      O => s_axis_tdata(77)
    );
\s_axis_tdata[78]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(78),
      O => s_axis_tdata(78)
    );
\s_axis_tdata[79]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(79),
      O => s_axis_tdata(79)
    );
\s_axis_tdata[7]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(7),
      O => s_axis_tdata(7)
    );
\s_axis_tdata[80]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(80),
      O => s_axis_tdata(80)
    );
\s_axis_tdata[81]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(81),
      O => s_axis_tdata(81)
    );
\s_axis_tdata[82]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(82),
      O => s_axis_tdata(82)
    );
\s_axis_tdata[83]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(83),
      O => s_axis_tdata(83)
    );
\s_axis_tdata[84]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(84),
      O => s_axis_tdata(84)
    );
\s_axis_tdata[85]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(85),
      O => s_axis_tdata(85)
    );
\s_axis_tdata[86]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(86),
      O => s_axis_tdata(86)
    );
\s_axis_tdata[87]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(87),
      O => s_axis_tdata(87)
    );
\s_axis_tdata[88]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(88),
      O => s_axis_tdata(88)
    );
\s_axis_tdata[89]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(89),
      O => s_axis_tdata(89)
    );
\s_axis_tdata[8]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(8),
      O => s_axis_tdata(8)
    );
\s_axis_tdata[90]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(90),
      O => s_axis_tdata(90)
    );
\s_axis_tdata[91]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(91),
      O => s_axis_tdata(91)
    );
\s_axis_tdata[92]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(92),
      O => s_axis_tdata(92)
    );
\s_axis_tdata[93]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(93),
      O => s_axis_tdata(93)
    );
\s_axis_tdata[94]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(94),
      O => s_axis_tdata(94)
    );
\s_axis_tdata[95]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(95),
      O => s_axis_tdata(95)
    );
\s_axis_tdata[96]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(96),
      O => s_axis_tdata(96)
    );
\s_axis_tdata[97]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(97),
      O => s_axis_tdata(97)
    );
\s_axis_tdata[98]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(98),
      O => s_axis_tdata(98)
    );
\s_axis_tdata[99]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(99),
      O => s_axis_tdata(99)
    );
\s_axis_tdata[9]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => stat_FSM,
      I1 => m_axis_tvalid,
      I2 => m_axis_tdata(9),
      O => s_axis_tdata(9)
    );
\s_axis_tkeep[31]_INST_0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => focus_tkeep,
      O => s_axis_tkeep(0)
    );
s_axis_tlast_INST_0: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEAEAEAEAEAEAEA"
    )
        port map (
      I0 => focus_tlast_reg_n_0,
      I1 => m_axis_tlast,
      I2 => s_axis_tlast_INST_0_i_1_n_0,
      I3 => packet_size_counter_reg(0),
      I4 => packet_size_counter_reg(1),
      I5 => packet_size_counter_reg(2),
      O => s_axis_tlast
    );
s_axis_tlast_INST_0_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => packet_size_counter_reg(7),
      I1 => packet_size_counter_reg(10),
      I2 => packet_size_counter_reg(3),
      I3 => packet_size_counter_reg(5),
      I4 => s_axis_tlast_INST_0_i_2_n_0,
      I5 => s_axis_tlast_INST_0_i_3_n_0,
      O => s_axis_tlast_INST_0_i_1_n_0
    );
s_axis_tlast_INST_0_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => packet_size_counter_reg(11),
      I1 => packet_size_counter_reg(9),
      I2 => packet_size_counter_reg(13),
      I3 => packet_size_counter_reg(12),
      O => s_axis_tlast_INST_0_i_2_n_0
    );
s_axis_tlast_INST_0_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => packet_size_counter_reg(4),
      I1 => packet_size_counter_reg(14),
      I2 => packet_size_counter_reg(15),
      I3 => packet_size_counter_reg(8),
      I4 => packet_size_counter_reg(6),
      O => s_axis_tlast_INST_0_i_3_n_0
    );
s_axis_tvalid_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => m_axis_tvalid,
      I1 => s_axis_tvalid_packetnouf_reg_n_0,
      O => s_axis_tvalid
    );
s_axis_tvalid_packetnouf_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"07FF07FF07000000"
    )
        port map (
      I0 => \packet_size_counter[15]_i_3_n_0\,
      I1 => m_axis_tlast,
      I2 => focus_tlast_reg_n_0,
      I3 => s_axis_tready,
      I4 => m_axis_tvalid,
      I5 => s_axis_tvalid_packetnouf_reg_n_0,
      O => s_axis_tvalid_packetnouf_i_1_n_0
    );
s_axis_tvalid_packetnouf_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => s_axis_tvalid_packetnouf_i_1_n_0,
      Q => s_axis_tvalid_packetnouf_reg_n_0,
      R => '0'
    );
stat_FSM_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAA8AFA8"
    )
        port map (
      I0 => stat_FSM,
      I1 => focus_tlast_i_5_n_0,
      I2 => s_axis_tlast_INST_0_i_1_n_0,
      I3 => focus_tlast_i_4_n_0,
      I4 => stat_FSM_i_2_n_0,
      O => stat_FSM_next
    );
stat_FSM_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"7F"
    )
        port map (
      I0 => m_axis_tvalid,
      I1 => s_axis_tready,
      I2 => m_axis_tlast,
      O => stat_FSM_i_2_n_0
    );
stat_FSM_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => stat_FSM_next,
      Q => stat_FSM,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_qsfp_40G_forward_0_0 is
  port (
    m_axis_tready : out STD_LOGIC;
    m_axis_tvalid : in STD_LOGIC;
    m_axis_tdata : in STD_LOGIC_VECTOR ( 255 downto 0 );
    m_axis_tkeep : in STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axis_tlast : in STD_LOGIC;
    m_axis_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_tready : in STD_LOGIC;
    s_axis_tvalid : out STD_LOGIC;
    s_axis_tdata : out STD_LOGIC_VECTOR ( 255 downto 0 );
    s_axis_tkeep : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axis_tlast : out STD_LOGIC;
    s_axis_tuser : out STD_LOGIC_VECTOR ( 0 to 0 );
    clk : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_qsfp_40G_forward_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_qsfp_40G_forward_0_0 : entity is "design_qsfp_40G_forward_0_0,forward,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of design_qsfp_40G_forward_0_0 : entity is "yes";
  attribute IP_DEFINITION_SOURCE : string;
  attribute IP_DEFINITION_SOURCE of design_qsfp_40G_forward_0_0 : entity is "module_ref";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of design_qsfp_40G_forward_0_0 : entity is "forward,Vivado 2022.2";
end design_qsfp_40G_forward_0_0;

architecture STRUCTURE of design_qsfp_40G_forward_0_0 is
  signal \^m_axis_tkeep\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \^m_axis_tuser\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^s_axis_tkeep\ : STD_LOGIC_VECTOR ( 31 to 31 );
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of clk : signal is "xilinx.com:signal:clock:1.0 clk CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of clk : signal is "XIL_INTERFACENAME clk, ASSOCIATED_BUSIF m_axis:s_axis, FREQ_HZ 312500000, FREQ_TOLERANCE_HZ 0, PHASE 0, CLK_DOMAIN design_qsfp_40G_l_ethernet_0_0_tx_clk_out_0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m_axis_tlast : signal is "xilinx.com:interface:axis:1.0 m_axis TLAST";
  attribute X_INTERFACE_INFO of m_axis_tready : signal is "xilinx.com:interface:axis:1.0 m_axis TREADY";
  attribute X_INTERFACE_INFO of m_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 m_axis TVALID";
  attribute X_INTERFACE_INFO of s_axis_tlast : signal is "xilinx.com:interface:axis:1.0 s_axis TLAST";
  attribute X_INTERFACE_INFO of s_axis_tready : signal is "xilinx.com:interface:axis:1.0 s_axis TREADY";
  attribute X_INTERFACE_PARAMETER of s_axis_tready : signal is "FREQ_HZ 390625000";
  attribute X_INTERFACE_INFO of s_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 s_axis TVALID";
  attribute X_INTERFACE_INFO of m_axis_tdata : signal is "xilinx.com:interface:axis:1.0 m_axis TDATA";
  attribute X_INTERFACE_INFO of m_axis_tkeep : signal is "xilinx.com:interface:axis:1.0 m_axis TKEEP";
  attribute X_INTERFACE_INFO of m_axis_tuser : signal is "xilinx.com:interface:axis:1.0 m_axis TUSER";
  attribute X_INTERFACE_PARAMETER of m_axis_tuser : signal is "XIL_INTERFACENAME m_axis, FREQ_HZ 312500000, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 1, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, PHASE 0, CLK_DOMAIN design_qsfp_40G_l_ethernet_0_0_tx_clk_out_0, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s_axis_tdata : signal is "xilinx.com:interface:axis:1.0 s_axis TDATA";
  attribute X_INTERFACE_INFO of s_axis_tkeep : signal is "xilinx.com:interface:axis:1.0 s_axis TKEEP";
  attribute X_INTERFACE_INFO of s_axis_tuser : signal is "xilinx.com:interface:axis:1.0 s_axis TUSER";
  attribute X_INTERFACE_PARAMETER of s_axis_tuser : signal is "XIL_INTERFACENAME s_axis, FREQ_HZ 312500000, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 1, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, PHASE 0, CLK_DOMAIN design_qsfp_40G_l_ethernet_0_0_tx_clk_out_0, LAYERED_METADATA undef, INSERT_VIP 0";
begin
  \^m_axis_tkeep\(30 downto 0) <= m_axis_tkeep(30 downto 0);
  \^m_axis_tuser\(0) <= m_axis_tuser(0);
  s_axis_tkeep(31) <= \^s_axis_tkeep\(31);
  s_axis_tkeep(30 downto 0) <= \^m_axis_tkeep\(30 downto 0);
  s_axis_tuser(0) <= \^m_axis_tuser\(0);
inst: entity work.design_qsfp_40G_forward_0_0_forward
     port map (
      clk => clk,
      m_axis_tdata(255 downto 0) => m_axis_tdata(255 downto 0),
      m_axis_tlast => m_axis_tlast,
      m_axis_tready => m_axis_tready,
      m_axis_tvalid => m_axis_tvalid,
      s_axis_tdata(255 downto 0) => s_axis_tdata(255 downto 0),
      s_axis_tkeep(0) => \^s_axis_tkeep\(31),
      s_axis_tlast => s_axis_tlast,
      s_axis_tready => s_axis_tready,
      s_axis_tvalid => s_axis_tvalid
    );
end STRUCTURE;
