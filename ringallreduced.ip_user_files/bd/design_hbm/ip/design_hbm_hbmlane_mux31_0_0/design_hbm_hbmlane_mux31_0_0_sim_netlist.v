// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
// Date        : Tue Dec  6 16:27:57 2022
// Host        : dell-Vostro-3910-China-HDD-Protection running 64-bit Ubuntu 22.04.1 LTS
// Command     : write_verilog -force -mode funcsim
//               /media/dell/workspace/Xilinx_projects/ringallreduced/ringallreduced.gen/sources_1/bd/design_hbm/ip/design_hbm_hbmlane_mux31_0_0/design_hbm_hbmlane_mux31_0_0_sim_netlist.v
// Design      : design_hbm_hbmlane_mux31_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcvu37p-fsvh2892-2L-e
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_hbm_hbmlane_mux31_0_0,hbmlane_mux31,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* IP_DEFINITION_SOURCE = "module_ref" *) 
(* X_CORE_INFO = "hbmlane_mux31,Vivado 2022.2" *) 
(* NotValidForBitStream *)
module design_hbm_hbmlane_mux31_0_0
   (clk,
    mux_select,
    m_HBM_READ_tvalid,
    m_HBM_READ_tdata,
    m_HBM_READ_tready,
    m_HBM_READ_tlast,
    m_HBM_READ_tkeep,
    m_HBM_WRITE_tready,
    m_HBM_WRITE_tdata,
    m_HBM_WRITE_tvalid,
    m_rd_blockamount,
    m_rd_run,
    m_rd_startaddress,
    m_wr_blockamount,
    m_wr_run,
    m_wr_startaddress,
    m_busy,
    s1_HBM_READ_tvalid,
    s1_HBM_READ_tdata,
    s1_HBM_READ_tready,
    s1_HBM_READ_tlast,
    s1_HBM_READ_tkeep,
    s1_HBM_WRITE_tready,
    s1_HBM_WRITE_tdata,
    s1_HBM_WRITE_tvalid,
    s1_rd_blockamount,
    s1_rd_run,
    s1_rd_startaddress,
    s1_wr_blockamount,
    s1_wr_run,
    s1_wr_startaddress,
    s1_busy,
    s2_HBM_READ_tvalid,
    s2_HBM_READ_tdata,
    s2_HBM_READ_tready,
    s2_HBM_READ_tlast,
    s2_HBM_READ_tkeep,
    s2_HBM_WRITE_tready,
    s2_HBM_WRITE_tdata,
    s2_HBM_WRITE_tvalid,
    s2_rd_blockamount,
    s2_rd_run,
    s2_rd_startaddress,
    s2_wr_blockamount,
    s2_wr_run,
    s2_wr_startaddress,
    s2_busy,
    s3_HBM_READ_tvalid,
    s3_HBM_READ_tdata,
    s3_HBM_READ_tready,
    s3_HBM_READ_tlast,
    s3_HBM_READ_tkeep,
    s3_HBM_WRITE_tready,
    s3_HBM_WRITE_tdata,
    s3_HBM_WRITE_tvalid,
    s3_rd_blockamount,
    s3_rd_run,
    s3_rd_startaddress,
    s3_wr_blockamount,
    s3_wr_run,
    s3_wr_startaddress,
    s3_busy);
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 clk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME clk, ASSOCIATED_BUSIF m_HBM_READ:m_HBM_WRITE:s1_HBM_READ:s1_HBM_WRITE:s2_HBM_READ:s2_HBM_WRITE:s3_HBM_READ:s3_HBM_WRITE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN design_hbm_clk_312MHz, INSERT_VIP 0" *) input clk;
  input [1:0]mux_select;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_HBM_READ TVALID" *) output m_HBM_READ_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_HBM_READ TDATA" *) output [255:0]m_HBM_READ_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_HBM_READ TREADY" *) input m_HBM_READ_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_HBM_READ TLAST" *) output m_HBM_READ_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_HBM_READ TKEEP" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m_HBM_READ, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN design_hbm_clk_312MHz, LAYERED_METADATA undef, INSERT_VIP 0" *) output [31:0]m_HBM_READ_tkeep;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_HBM_WRITE TREADY" *) output m_HBM_WRITE_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_HBM_WRITE TDATA" *) input [255:0]m_HBM_WRITE_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_HBM_WRITE TVALID" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m_HBM_WRITE, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN design_hbm_clk_312MHz, LAYERED_METADATA undef, INSERT_VIP 0" *) input m_HBM_WRITE_tvalid;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 m_ctrl_ports rd_blockamount" *) (* X_INTERFACE_MODE = "slave" *) input [31:0]m_rd_blockamount;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 m_ctrl_ports rd_run" *) input m_rd_run;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 m_ctrl_ports rd_startaddress" *) input [31:0]m_rd_startaddress;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 m_ctrl_ports wr_blockamount" *) input [31:0]m_wr_blockamount;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 m_ctrl_ports wr_run" *) input m_wr_run;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 m_ctrl_ports wr_startaddress" *) input [31:0]m_wr_startaddress;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 m_ctrl_ports busy" *) output m_busy;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s1_HBM_READ TVALID" *) input s1_HBM_READ_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s1_HBM_READ TDATA" *) input [255:0]s1_HBM_READ_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s1_HBM_READ TREADY" *) output s1_HBM_READ_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s1_HBM_READ TLAST" *) input s1_HBM_READ_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s1_HBM_READ TKEEP" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s1_HBM_READ, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN design_hbm_clk_312MHz, LAYERED_METADATA undef, INSERT_VIP 0" *) input [31:0]s1_HBM_READ_tkeep;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s1_HBM_WRITE TREADY" *) input s1_HBM_WRITE_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s1_HBM_WRITE TDATA" *) output [255:0]s1_HBM_WRITE_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s1_HBM_WRITE TVALID" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s1_HBM_WRITE, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN design_hbm_clk_312MHz, LAYERED_METADATA undef, INSERT_VIP 0" *) output s1_HBM_WRITE_tvalid;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 s1_ctrl_ports rd_blockamount" *) output [31:0]s1_rd_blockamount;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 s1_ctrl_ports rd_run" *) output s1_rd_run;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 s1_ctrl_ports rd_startaddress" *) output [31:0]s1_rd_startaddress;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 s1_ctrl_ports wr_blockamount" *) output [31:0]s1_wr_blockamount;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 s1_ctrl_ports wr_run" *) output s1_wr_run;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 s1_ctrl_ports wr_startaddress" *) output [31:0]s1_wr_startaddress;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 s1_ctrl_ports busy" *) input s1_busy;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s2_HBM_READ TVALID" *) input s2_HBM_READ_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s2_HBM_READ TDATA" *) input [255:0]s2_HBM_READ_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s2_HBM_READ TREADY" *) output s2_HBM_READ_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s2_HBM_READ TLAST" *) input s2_HBM_READ_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s2_HBM_READ TKEEP" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s2_HBM_READ, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN design_hbm_clk_312MHz, LAYERED_METADATA undef, INSERT_VIP 0" *) input [31:0]s2_HBM_READ_tkeep;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s2_HBM_WRITE TREADY" *) input s2_HBM_WRITE_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s2_HBM_WRITE TDATA" *) output [255:0]s2_HBM_WRITE_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s2_HBM_WRITE TVALID" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s2_HBM_WRITE, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN design_hbm_clk_312MHz, LAYERED_METADATA undef, INSERT_VIP 0" *) output s2_HBM_WRITE_tvalid;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 s2_ctrl_ports rd_blockamount" *) output [31:0]s2_rd_blockamount;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 s2_ctrl_ports rd_run" *) output s2_rd_run;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 s2_ctrl_ports rd_startaddress" *) output [31:0]s2_rd_startaddress;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 s2_ctrl_ports wr_blockamount" *) output [31:0]s2_wr_blockamount;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 s2_ctrl_ports wr_run" *) output s2_wr_run;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 s2_ctrl_ports wr_startaddress" *) output [31:0]s2_wr_startaddress;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 s2_ctrl_ports busy" *) input s2_busy;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s3_HBM_READ TVALID" *) input s3_HBM_READ_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s3_HBM_READ TDATA" *) input [255:0]s3_HBM_READ_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s3_HBM_READ TREADY" *) output s3_HBM_READ_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s3_HBM_READ TLAST" *) input s3_HBM_READ_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s3_HBM_READ TKEEP" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s3_HBM_READ, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN design_hbm_clk_312MHz, LAYERED_METADATA undef, INSERT_VIP 0" *) input [31:0]s3_HBM_READ_tkeep;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s3_HBM_WRITE TREADY" *) input s3_HBM_WRITE_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s3_HBM_WRITE TDATA" *) output [255:0]s3_HBM_WRITE_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s3_HBM_WRITE TVALID" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s3_HBM_WRITE, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN design_hbm_clk_312MHz, LAYERED_METADATA undef, INSERT_VIP 0" *) output s3_HBM_WRITE_tvalid;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 s3_ctrl_ports rd_blockamount" *) output [31:0]s3_rd_blockamount;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 s3_ctrl_ports rd_run" *) output s3_rd_run;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 s3_ctrl_ports rd_startaddress" *) output [31:0]s3_rd_startaddress;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 s3_ctrl_ports wr_blockamount" *) output [31:0]s3_wr_blockamount;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 s3_ctrl_ports wr_run" *) output s3_wr_run;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 s3_ctrl_ports wr_startaddress" *) output [31:0]s3_wr_startaddress;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 s3_ctrl_ports busy" *) input s3_busy;

  wire [255:0]m_HBM_READ_tdata;
  wire [31:0]m_HBM_READ_tkeep;
  wire m_HBM_READ_tlast;
  wire m_HBM_READ_tready;
  wire m_HBM_READ_tvalid;
  wire [255:0]m_HBM_WRITE_tdata;
  wire m_HBM_WRITE_tready;
  wire m_HBM_WRITE_tvalid;
  wire m_busy;
  wire [31:0]m_rd_blockamount;
  wire m_rd_run;
  wire [31:0]m_rd_startaddress;
  wire [31:0]m_wr_blockamount;
  wire m_wr_run;
  wire [31:0]m_wr_startaddress;
  wire [1:0]mux_select;
  wire [255:0]s1_HBM_READ_tdata;
  wire [31:0]s1_HBM_READ_tkeep;
  wire s1_HBM_READ_tlast;
  wire s1_HBM_READ_tready;
  wire s1_HBM_READ_tvalid;
  wire [255:0]s1_HBM_WRITE_tdata;
  wire s1_HBM_WRITE_tready;
  wire s1_HBM_WRITE_tvalid;
  wire s1_busy;
  wire [31:0]s1_rd_blockamount;
  wire s1_rd_run;
  wire [31:0]s1_rd_startaddress;
  wire [31:0]s1_wr_blockamount;
  wire s1_wr_run;
  wire [31:0]s1_wr_startaddress;
  wire [255:0]s2_HBM_READ_tdata;
  wire [31:0]s2_HBM_READ_tkeep;
  wire s2_HBM_READ_tlast;
  wire s2_HBM_READ_tready;
  wire s2_HBM_READ_tvalid;
  wire [255:0]s2_HBM_WRITE_tdata;
  wire s2_HBM_WRITE_tready;
  wire s2_HBM_WRITE_tvalid;
  wire s2_busy;
  wire [31:0]s2_rd_blockamount;
  wire s2_rd_run;
  wire [31:0]s2_rd_startaddress;
  wire [31:0]s2_wr_blockamount;
  wire s2_wr_run;
  wire [31:0]s2_wr_startaddress;
  wire [255:0]s3_HBM_READ_tdata;
  wire [31:0]s3_HBM_READ_tkeep;
  wire s3_HBM_READ_tlast;
  wire s3_HBM_READ_tready;
  wire s3_HBM_READ_tvalid;
  wire [255:0]s3_HBM_WRITE_tdata;
  wire s3_HBM_WRITE_tready;
  wire s3_HBM_WRITE_tvalid;
  wire s3_busy;
  wire [31:0]s3_rd_blockamount;
  wire s3_rd_run;
  wire [31:0]s3_rd_startaddress;
  wire [31:0]s3_wr_blockamount;
  wire s3_wr_run;
  wire [31:0]s3_wr_startaddress;

  design_hbm_hbmlane_mux31_0_0_hbmlane_mux31 inst
       (.m_HBM_READ_tdata(m_HBM_READ_tdata),
        .m_HBM_READ_tkeep(m_HBM_READ_tkeep),
        .m_HBM_READ_tlast(m_HBM_READ_tlast),
        .m_HBM_READ_tready(m_HBM_READ_tready),
        .m_HBM_READ_tvalid(m_HBM_READ_tvalid),
        .m_HBM_WRITE_tdata(m_HBM_WRITE_tdata),
        .m_HBM_WRITE_tready(m_HBM_WRITE_tready),
        .m_HBM_WRITE_tvalid(m_HBM_WRITE_tvalid),
        .m_busy(m_busy),
        .m_rd_blockamount(m_rd_blockamount),
        .m_rd_run(m_rd_run),
        .m_rd_startaddress(m_rd_startaddress),
        .m_wr_blockamount(m_wr_blockamount),
        .m_wr_run(m_wr_run),
        .m_wr_startaddress(m_wr_startaddress),
        .mux_select(mux_select),
        .s1_HBM_READ_tdata(s1_HBM_READ_tdata),
        .s1_HBM_READ_tkeep(s1_HBM_READ_tkeep),
        .s1_HBM_READ_tlast(s1_HBM_READ_tlast),
        .s1_HBM_READ_tready(s1_HBM_READ_tready),
        .s1_HBM_READ_tvalid(s1_HBM_READ_tvalid),
        .s1_HBM_WRITE_tdata(s1_HBM_WRITE_tdata),
        .s1_HBM_WRITE_tready(s1_HBM_WRITE_tready),
        .s1_HBM_WRITE_tvalid(s1_HBM_WRITE_tvalid),
        .s1_busy(s1_busy),
        .s1_rd_blockamount(s1_rd_blockamount),
        .s1_rd_run(s1_rd_run),
        .s1_rd_startaddress(s1_rd_startaddress),
        .s1_wr_blockamount(s1_wr_blockamount),
        .s1_wr_run(s1_wr_run),
        .s1_wr_startaddress(s1_wr_startaddress),
        .s2_HBM_READ_tdata(s2_HBM_READ_tdata),
        .s2_HBM_READ_tkeep(s2_HBM_READ_tkeep),
        .s2_HBM_READ_tlast(s2_HBM_READ_tlast),
        .s2_HBM_READ_tready(s2_HBM_READ_tready),
        .s2_HBM_READ_tvalid(s2_HBM_READ_tvalid),
        .s2_HBM_WRITE_tdata(s2_HBM_WRITE_tdata),
        .s2_HBM_WRITE_tready(s2_HBM_WRITE_tready),
        .s2_HBM_WRITE_tvalid(s2_HBM_WRITE_tvalid),
        .s2_busy(s2_busy),
        .s2_rd_blockamount(s2_rd_blockamount),
        .s2_rd_run(s2_rd_run),
        .s2_rd_startaddress(s2_rd_startaddress),
        .s2_wr_blockamount(s2_wr_blockamount),
        .s2_wr_run(s2_wr_run),
        .s2_wr_startaddress(s2_wr_startaddress),
        .s3_HBM_READ_tdata(s3_HBM_READ_tdata),
        .s3_HBM_READ_tkeep(s3_HBM_READ_tkeep),
        .s3_HBM_READ_tlast(s3_HBM_READ_tlast),
        .s3_HBM_READ_tready(s3_HBM_READ_tready),
        .s3_HBM_READ_tvalid(s3_HBM_READ_tvalid),
        .s3_HBM_WRITE_tdata(s3_HBM_WRITE_tdata),
        .s3_HBM_WRITE_tready(s3_HBM_WRITE_tready),
        .s3_HBM_WRITE_tvalid(s3_HBM_WRITE_tvalid),
        .s3_busy(s3_busy),
        .s3_rd_blockamount(s3_rd_blockamount),
        .s3_rd_run(s3_rd_run),
        .s3_rd_startaddress(s3_rd_startaddress),
        .s3_wr_blockamount(s3_wr_blockamount),
        .s3_wr_run(s3_wr_run),
        .s3_wr_startaddress(s3_wr_startaddress));
endmodule

(* ORIG_REF_NAME = "hbmlane_mux31" *) 
module design_hbm_hbmlane_mux31_0_0_hbmlane_mux31
   (m_HBM_READ_tdata,
    m_HBM_READ_tlast,
    m_HBM_READ_tkeep,
    s1_HBM_READ_tready,
    s1_HBM_WRITE_tdata,
    s1_HBM_WRITE_tvalid,
    s1_rd_blockamount,
    s1_rd_run,
    s1_rd_startaddress,
    s1_wr_blockamount,
    s1_wr_run,
    s1_wr_startaddress,
    s2_HBM_READ_tready,
    s2_HBM_WRITE_tdata,
    s2_HBM_WRITE_tvalid,
    s2_rd_blockamount,
    s2_rd_run,
    s2_rd_startaddress,
    s2_wr_blockamount,
    s2_wr_run,
    s2_wr_startaddress,
    s3_HBM_READ_tready,
    s3_HBM_WRITE_tdata,
    s3_HBM_WRITE_tvalid,
    s3_rd_blockamount,
    s3_rd_run,
    s3_rd_startaddress,
    s3_wr_blockamount,
    s3_wr_run,
    s3_wr_startaddress,
    m_HBM_READ_tvalid,
    m_HBM_WRITE_tready,
    m_busy,
    mux_select,
    m_HBM_WRITE_tdata,
    m_rd_blockamount,
    m_rd_startaddress,
    m_wr_blockamount,
    m_wr_startaddress,
    s3_HBM_READ_tvalid,
    s1_HBM_READ_tvalid,
    s2_HBM_READ_tvalid,
    s3_HBM_WRITE_tready,
    s1_HBM_WRITE_tready,
    s2_HBM_WRITE_tready,
    s3_busy,
    s1_busy,
    s2_busy,
    s3_HBM_READ_tlast,
    s2_HBM_READ_tlast,
    s1_HBM_READ_tlast,
    s3_HBM_READ_tdata,
    s2_HBM_READ_tdata,
    s1_HBM_READ_tdata,
    s3_HBM_READ_tkeep,
    s2_HBM_READ_tkeep,
    s1_HBM_READ_tkeep,
    m_HBM_READ_tready,
    m_HBM_WRITE_tvalid,
    m_rd_run,
    m_wr_run);
  output [255:0]m_HBM_READ_tdata;
  output m_HBM_READ_tlast;
  output [31:0]m_HBM_READ_tkeep;
  output s1_HBM_READ_tready;
  output [255:0]s1_HBM_WRITE_tdata;
  output s1_HBM_WRITE_tvalid;
  output [31:0]s1_rd_blockamount;
  output s1_rd_run;
  output [31:0]s1_rd_startaddress;
  output [31:0]s1_wr_blockamount;
  output s1_wr_run;
  output [31:0]s1_wr_startaddress;
  output s2_HBM_READ_tready;
  output [255:0]s2_HBM_WRITE_tdata;
  output s2_HBM_WRITE_tvalid;
  output [31:0]s2_rd_blockamount;
  output s2_rd_run;
  output [31:0]s2_rd_startaddress;
  output [31:0]s2_wr_blockamount;
  output s2_wr_run;
  output [31:0]s2_wr_startaddress;
  output s3_HBM_READ_tready;
  output [255:0]s3_HBM_WRITE_tdata;
  output s3_HBM_WRITE_tvalid;
  output [31:0]s3_rd_blockamount;
  output s3_rd_run;
  output [31:0]s3_rd_startaddress;
  output [31:0]s3_wr_blockamount;
  output s3_wr_run;
  output [31:0]s3_wr_startaddress;
  output m_HBM_READ_tvalid;
  output m_HBM_WRITE_tready;
  output m_busy;
  input [1:0]mux_select;
  input [255:0]m_HBM_WRITE_tdata;
  input [31:0]m_rd_blockamount;
  input [31:0]m_rd_startaddress;
  input [31:0]m_wr_blockamount;
  input [31:0]m_wr_startaddress;
  input s3_HBM_READ_tvalid;
  input s1_HBM_READ_tvalid;
  input s2_HBM_READ_tvalid;
  input s3_HBM_WRITE_tready;
  input s1_HBM_WRITE_tready;
  input s2_HBM_WRITE_tready;
  input s3_busy;
  input s1_busy;
  input s2_busy;
  input s3_HBM_READ_tlast;
  input s2_HBM_READ_tlast;
  input s1_HBM_READ_tlast;
  input [255:0]s3_HBM_READ_tdata;
  input [255:0]s2_HBM_READ_tdata;
  input [255:0]s1_HBM_READ_tdata;
  input [31:0]s3_HBM_READ_tkeep;
  input [31:0]s2_HBM_READ_tkeep;
  input [31:0]s1_HBM_READ_tkeep;
  input m_HBM_READ_tready;
  input m_HBM_WRITE_tvalid;
  input m_rd_run;
  input m_wr_run;

  wire [255:0]m_HBM_READ_tdata;
  wire \m_HBM_READ_tdata_reg[0]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[100]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[101]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[102]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[103]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[104]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[105]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[106]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[107]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[108]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[109]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[10]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[110]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[111]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[112]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[113]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[114]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[115]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[116]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[117]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[118]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[119]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[11]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[120]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[121]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[122]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[123]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[124]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[125]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[126]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[127]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[128]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[129]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[12]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[130]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[131]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[132]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[133]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[134]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[135]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[136]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[137]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[138]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[139]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[13]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[140]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[141]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[142]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[143]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[144]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[145]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[146]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[147]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[148]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[149]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[14]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[150]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[151]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[152]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[153]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[154]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[155]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[156]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[157]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[158]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[159]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[15]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[160]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[161]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[162]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[163]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[164]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[165]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[166]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[167]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[168]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[169]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[16]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[170]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[171]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[172]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[173]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[174]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[175]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[176]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[177]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[178]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[179]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[17]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[180]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[181]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[182]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[183]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[184]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[185]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[186]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[187]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[188]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[189]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[18]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[190]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[191]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[192]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[193]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[194]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[195]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[196]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[197]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[198]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[199]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[19]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[1]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[200]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[201]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[202]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[203]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[204]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[205]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[206]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[207]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[208]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[209]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[20]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[210]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[211]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[212]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[213]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[214]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[215]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[216]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[217]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[218]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[219]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[21]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[220]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[221]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[222]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[223]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[224]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[225]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[226]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[227]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[228]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[229]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[22]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[230]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[231]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[232]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[233]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[234]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[235]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[236]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[237]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[238]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[239]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[23]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[240]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[241]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[242]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[243]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[244]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[245]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[246]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[247]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[248]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[249]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[24]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[250]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[251]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[252]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[253]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[254]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[255]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[255]_i_2_n_0 ;
  wire \m_HBM_READ_tdata_reg[25]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[26]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[27]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[28]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[29]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[2]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[30]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[31]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[32]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[33]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[34]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[35]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[36]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[37]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[38]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[39]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[3]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[40]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[41]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[42]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[43]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[44]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[45]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[46]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[47]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[48]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[49]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[4]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[50]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[51]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[52]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[53]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[54]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[55]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[56]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[57]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[58]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[59]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[5]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[60]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[61]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[62]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[63]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[64]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[65]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[66]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[67]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[68]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[69]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[6]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[70]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[71]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[72]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[73]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[74]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[75]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[76]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[77]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[78]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[79]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[7]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[80]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[81]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[82]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[83]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[84]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[85]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[86]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[87]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[88]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[89]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[8]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[90]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[91]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[92]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[93]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[94]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[95]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[96]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[97]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[98]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[99]_i_1_n_0 ;
  wire \m_HBM_READ_tdata_reg[9]_i_1_n_0 ;
  wire [31:0]m_HBM_READ_tkeep;
  wire \m_HBM_READ_tkeep_reg[0]_i_1_n_0 ;
  wire \m_HBM_READ_tkeep_reg[10]_i_1_n_0 ;
  wire \m_HBM_READ_tkeep_reg[11]_i_1_n_0 ;
  wire \m_HBM_READ_tkeep_reg[12]_i_1_n_0 ;
  wire \m_HBM_READ_tkeep_reg[13]_i_1_n_0 ;
  wire \m_HBM_READ_tkeep_reg[14]_i_1_n_0 ;
  wire \m_HBM_READ_tkeep_reg[15]_i_1_n_0 ;
  wire \m_HBM_READ_tkeep_reg[16]_i_1_n_0 ;
  wire \m_HBM_READ_tkeep_reg[17]_i_1_n_0 ;
  wire \m_HBM_READ_tkeep_reg[18]_i_1_n_0 ;
  wire \m_HBM_READ_tkeep_reg[19]_i_1_n_0 ;
  wire \m_HBM_READ_tkeep_reg[1]_i_1_n_0 ;
  wire \m_HBM_READ_tkeep_reg[20]_i_1_n_0 ;
  wire \m_HBM_READ_tkeep_reg[21]_i_1_n_0 ;
  wire \m_HBM_READ_tkeep_reg[22]_i_1_n_0 ;
  wire \m_HBM_READ_tkeep_reg[23]_i_1_n_0 ;
  wire \m_HBM_READ_tkeep_reg[24]_i_1_n_0 ;
  wire \m_HBM_READ_tkeep_reg[25]_i_1_n_0 ;
  wire \m_HBM_READ_tkeep_reg[26]_i_1_n_0 ;
  wire \m_HBM_READ_tkeep_reg[27]_i_1_n_0 ;
  wire \m_HBM_READ_tkeep_reg[28]_i_1_n_0 ;
  wire \m_HBM_READ_tkeep_reg[29]_i_1_n_0 ;
  wire \m_HBM_READ_tkeep_reg[2]_i_1_n_0 ;
  wire \m_HBM_READ_tkeep_reg[30]_i_1_n_0 ;
  wire \m_HBM_READ_tkeep_reg[31]_i_1_n_0 ;
  wire \m_HBM_READ_tkeep_reg[3]_i_1_n_0 ;
  wire \m_HBM_READ_tkeep_reg[4]_i_1_n_0 ;
  wire \m_HBM_READ_tkeep_reg[5]_i_1_n_0 ;
  wire \m_HBM_READ_tkeep_reg[6]_i_1_n_0 ;
  wire \m_HBM_READ_tkeep_reg[7]_i_1_n_0 ;
  wire \m_HBM_READ_tkeep_reg[8]_i_1_n_0 ;
  wire \m_HBM_READ_tkeep_reg[9]_i_1_n_0 ;
  wire m_HBM_READ_tlast;
  wire m_HBM_READ_tlast__0_n_0;
  wire m_HBM_READ_tready;
  wire m_HBM_READ_tvalid;
  wire [255:0]m_HBM_WRITE_tdata;
  wire m_HBM_WRITE_tready;
  wire m_HBM_WRITE_tvalid;
  wire m_busy;
  wire [31:0]m_rd_blockamount;
  wire m_rd_run;
  wire [31:0]m_rd_startaddress;
  wire [31:0]m_wr_blockamount;
  wire m_wr_run;
  wire [31:0]m_wr_startaddress;
  wire [1:0]mux_select;
  wire [255:0]s1_HBM_READ_tdata;
  wire [31:0]s1_HBM_READ_tkeep;
  wire s1_HBM_READ_tlast;
  wire s1_HBM_READ_tready;
  wire s1_HBM_READ_tready_reg_i_1_n_0;
  wire s1_HBM_READ_tvalid;
  wire [255:0]s1_HBM_WRITE_tdata;
  wire \s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ;
  wire s1_HBM_WRITE_tready;
  wire s1_HBM_WRITE_tvalid;
  wire s1_HBM_WRITE_tvalid_reg_i_1_n_0;
  wire s1_busy;
  wire [31:0]s1_rd_blockamount;
  wire s1_rd_run;
  wire s1_rd_run_reg_i_1_n_0;
  wire [31:0]s1_rd_startaddress;
  wire [31:0]s1_wr_blockamount;
  wire s1_wr_run;
  wire s1_wr_run_reg_i_1_n_0;
  wire [31:0]s1_wr_startaddress;
  wire [255:0]s2_HBM_READ_tdata;
  wire [31:0]s2_HBM_READ_tkeep;
  wire s2_HBM_READ_tlast;
  wire s2_HBM_READ_tready;
  wire s2_HBM_READ_tready_reg_i_1_n_0;
  wire s2_HBM_READ_tvalid;
  wire [255:0]s2_HBM_WRITE_tdata;
  wire \s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ;
  wire s2_HBM_WRITE_tready;
  wire s2_HBM_WRITE_tvalid;
  wire s2_HBM_WRITE_tvalid_reg_i_1_n_0;
  wire s2_busy;
  wire [31:0]s2_rd_blockamount;
  wire s2_rd_run;
  wire s2_rd_run_reg_i_1_n_0;
  wire [31:0]s2_rd_startaddress;
  wire [31:0]s2_wr_blockamount;
  wire s2_wr_run;
  wire s2_wr_run_reg_i_1_n_0;
  wire [31:0]s2_wr_startaddress;
  wire [255:0]s3_HBM_READ_tdata;
  wire [31:0]s3_HBM_READ_tkeep;
  wire s3_HBM_READ_tlast;
  wire s3_HBM_READ_tready;
  wire s3_HBM_READ_tready_reg_i_1_n_0;
  wire s3_HBM_READ_tvalid;
  wire [255:0]s3_HBM_WRITE_tdata;
  wire \s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ;
  wire s3_HBM_WRITE_tready;
  wire s3_HBM_WRITE_tvalid;
  wire s3_busy;
  wire [31:0]s3_rd_blockamount;
  wire s3_rd_run;
  wire [31:0]s3_rd_startaddress;
  wire [31:0]s3_wr_blockamount;
  wire s3_wr_run;
  wire [31:0]s3_wr_startaddress;

  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[0] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[0]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[0]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[0]_i_1 
       (.I0(s3_HBM_READ_tdata[0]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[0]),
        .I3(s1_HBM_READ_tdata[0]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[0]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[100] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[100]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[100]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[100]_i_1 
       (.I0(s3_HBM_READ_tdata[100]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[100]),
        .I3(s1_HBM_READ_tdata[100]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[100]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[101] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[101]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[101]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[101]_i_1 
       (.I0(s3_HBM_READ_tdata[101]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[101]),
        .I3(s1_HBM_READ_tdata[101]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[101]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[102] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[102]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[102]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[102]_i_1 
       (.I0(s3_HBM_READ_tdata[102]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[102]),
        .I3(s1_HBM_READ_tdata[102]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[102]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[103] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[103]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[103]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[103]_i_1 
       (.I0(s3_HBM_READ_tdata[103]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[103]),
        .I3(s1_HBM_READ_tdata[103]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[103]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[104] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[104]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[104]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[104]_i_1 
       (.I0(s3_HBM_READ_tdata[104]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[104]),
        .I3(s1_HBM_READ_tdata[104]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[104]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[105] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[105]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[105]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[105]_i_1 
       (.I0(s3_HBM_READ_tdata[105]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[105]),
        .I3(s1_HBM_READ_tdata[105]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[105]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[106] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[106]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[106]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[106]_i_1 
       (.I0(s3_HBM_READ_tdata[106]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[106]),
        .I3(s1_HBM_READ_tdata[106]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[106]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[107] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[107]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[107]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[107]_i_1 
       (.I0(s3_HBM_READ_tdata[107]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[107]),
        .I3(s1_HBM_READ_tdata[107]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[107]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[108] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[108]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[108]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[108]_i_1 
       (.I0(s3_HBM_READ_tdata[108]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[108]),
        .I3(s1_HBM_READ_tdata[108]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[108]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[109] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[109]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[109]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[109]_i_1 
       (.I0(s3_HBM_READ_tdata[109]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[109]),
        .I3(s1_HBM_READ_tdata[109]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[109]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[10] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[10]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[10]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[10]_i_1 
       (.I0(s3_HBM_READ_tdata[10]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[10]),
        .I3(s1_HBM_READ_tdata[10]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[10]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[110] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[110]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[110]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[110]_i_1 
       (.I0(s3_HBM_READ_tdata[110]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[110]),
        .I3(s1_HBM_READ_tdata[110]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[110]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[111] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[111]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[111]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[111]_i_1 
       (.I0(s3_HBM_READ_tdata[111]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[111]),
        .I3(s1_HBM_READ_tdata[111]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[111]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[112] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[112]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[112]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[112]_i_1 
       (.I0(s3_HBM_READ_tdata[112]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[112]),
        .I3(s1_HBM_READ_tdata[112]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[112]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[113] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[113]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[113]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[113]_i_1 
       (.I0(s3_HBM_READ_tdata[113]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[113]),
        .I3(s1_HBM_READ_tdata[113]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[113]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[114] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[114]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[114]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[114]_i_1 
       (.I0(s3_HBM_READ_tdata[114]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[114]),
        .I3(s1_HBM_READ_tdata[114]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[114]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[115] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[115]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[115]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[115]_i_1 
       (.I0(s3_HBM_READ_tdata[115]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[115]),
        .I3(s1_HBM_READ_tdata[115]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[115]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[116] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[116]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[116]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[116]_i_1 
       (.I0(s3_HBM_READ_tdata[116]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[116]),
        .I3(s1_HBM_READ_tdata[116]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[116]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[117] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[117]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[117]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[117]_i_1 
       (.I0(s3_HBM_READ_tdata[117]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[117]),
        .I3(s1_HBM_READ_tdata[117]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[117]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[118] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[118]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[118]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[118]_i_1 
       (.I0(s3_HBM_READ_tdata[118]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[118]),
        .I3(s1_HBM_READ_tdata[118]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[118]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[119] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[119]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[119]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[119]_i_1 
       (.I0(s3_HBM_READ_tdata[119]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[119]),
        .I3(s1_HBM_READ_tdata[119]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[119]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[11] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[11]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[11]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[11]_i_1 
       (.I0(s3_HBM_READ_tdata[11]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[11]),
        .I3(s1_HBM_READ_tdata[11]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[11]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[120] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[120]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[120]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[120]_i_1 
       (.I0(s3_HBM_READ_tdata[120]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[120]),
        .I3(s1_HBM_READ_tdata[120]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[120]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[121] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[121]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[121]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[121]_i_1 
       (.I0(s3_HBM_READ_tdata[121]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[121]),
        .I3(s1_HBM_READ_tdata[121]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[121]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[122] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[122]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[122]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[122]_i_1 
       (.I0(s3_HBM_READ_tdata[122]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[122]),
        .I3(s1_HBM_READ_tdata[122]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[122]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[123] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[123]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[123]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[123]_i_1 
       (.I0(s3_HBM_READ_tdata[123]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[123]),
        .I3(s1_HBM_READ_tdata[123]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[123]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[124] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[124]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[124]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[124]_i_1 
       (.I0(s3_HBM_READ_tdata[124]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[124]),
        .I3(s1_HBM_READ_tdata[124]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[124]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[125] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[125]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[125]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[125]_i_1 
       (.I0(s3_HBM_READ_tdata[125]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[125]),
        .I3(s1_HBM_READ_tdata[125]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[125]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[126] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[126]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[126]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[126]_i_1 
       (.I0(s3_HBM_READ_tdata[126]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[126]),
        .I3(s1_HBM_READ_tdata[126]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[126]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[127] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[127]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[127]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[127]_i_1 
       (.I0(s3_HBM_READ_tdata[127]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[127]),
        .I3(s1_HBM_READ_tdata[127]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[127]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[128] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[128]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[128]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[128]_i_1 
       (.I0(s3_HBM_READ_tdata[128]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[128]),
        .I3(s1_HBM_READ_tdata[128]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[128]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[129] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[129]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[129]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[129]_i_1 
       (.I0(s3_HBM_READ_tdata[129]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[129]),
        .I3(s1_HBM_READ_tdata[129]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[129]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[12] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[12]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[12]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[12]_i_1 
       (.I0(s3_HBM_READ_tdata[12]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[12]),
        .I3(s1_HBM_READ_tdata[12]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[12]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[130] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[130]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[130]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[130]_i_1 
       (.I0(s3_HBM_READ_tdata[130]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[130]),
        .I3(s1_HBM_READ_tdata[130]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[130]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[131] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[131]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[131]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[131]_i_1 
       (.I0(s3_HBM_READ_tdata[131]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[131]),
        .I3(s1_HBM_READ_tdata[131]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[131]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[132] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[132]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[132]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[132]_i_1 
       (.I0(s3_HBM_READ_tdata[132]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[132]),
        .I3(s1_HBM_READ_tdata[132]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[132]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[133] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[133]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[133]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[133]_i_1 
       (.I0(s3_HBM_READ_tdata[133]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[133]),
        .I3(s1_HBM_READ_tdata[133]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[133]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[134] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[134]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[134]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[134]_i_1 
       (.I0(s3_HBM_READ_tdata[134]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[134]),
        .I3(s1_HBM_READ_tdata[134]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[134]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[135] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[135]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[135]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[135]_i_1 
       (.I0(s3_HBM_READ_tdata[135]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[135]),
        .I3(s1_HBM_READ_tdata[135]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[135]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[136] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[136]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[136]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[136]_i_1 
       (.I0(s3_HBM_READ_tdata[136]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[136]),
        .I3(s1_HBM_READ_tdata[136]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[136]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[137] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[137]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[137]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[137]_i_1 
       (.I0(s3_HBM_READ_tdata[137]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[137]),
        .I3(s1_HBM_READ_tdata[137]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[137]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[138] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[138]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[138]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[138]_i_1 
       (.I0(s3_HBM_READ_tdata[138]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[138]),
        .I3(s1_HBM_READ_tdata[138]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[138]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[139] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[139]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[139]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[139]_i_1 
       (.I0(s3_HBM_READ_tdata[139]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[139]),
        .I3(s1_HBM_READ_tdata[139]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[139]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[13] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[13]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[13]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[13]_i_1 
       (.I0(s3_HBM_READ_tdata[13]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[13]),
        .I3(s1_HBM_READ_tdata[13]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[13]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[140] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[140]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[140]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[140]_i_1 
       (.I0(s3_HBM_READ_tdata[140]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[140]),
        .I3(s1_HBM_READ_tdata[140]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[140]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[141] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[141]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[141]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[141]_i_1 
       (.I0(s3_HBM_READ_tdata[141]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[141]),
        .I3(s1_HBM_READ_tdata[141]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[141]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[142] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[142]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[142]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[142]_i_1 
       (.I0(s3_HBM_READ_tdata[142]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[142]),
        .I3(s1_HBM_READ_tdata[142]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[142]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[143] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[143]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[143]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[143]_i_1 
       (.I0(s3_HBM_READ_tdata[143]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[143]),
        .I3(s1_HBM_READ_tdata[143]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[143]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[144] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[144]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[144]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[144]_i_1 
       (.I0(s3_HBM_READ_tdata[144]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[144]),
        .I3(s1_HBM_READ_tdata[144]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[144]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[145] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[145]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[145]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[145]_i_1 
       (.I0(s3_HBM_READ_tdata[145]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[145]),
        .I3(s1_HBM_READ_tdata[145]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[145]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[146] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[146]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[146]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[146]_i_1 
       (.I0(s3_HBM_READ_tdata[146]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[146]),
        .I3(s1_HBM_READ_tdata[146]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[146]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[147] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[147]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[147]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[147]_i_1 
       (.I0(s3_HBM_READ_tdata[147]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[147]),
        .I3(s1_HBM_READ_tdata[147]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[147]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[148] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[148]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[148]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[148]_i_1 
       (.I0(s3_HBM_READ_tdata[148]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[148]),
        .I3(s1_HBM_READ_tdata[148]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[148]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[149] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[149]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[149]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[149]_i_1 
       (.I0(s3_HBM_READ_tdata[149]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[149]),
        .I3(s1_HBM_READ_tdata[149]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[149]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[14] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[14]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[14]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[14]_i_1 
       (.I0(s3_HBM_READ_tdata[14]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[14]),
        .I3(s1_HBM_READ_tdata[14]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[14]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[150] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[150]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[150]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[150]_i_1 
       (.I0(s3_HBM_READ_tdata[150]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[150]),
        .I3(s1_HBM_READ_tdata[150]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[150]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[151] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[151]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[151]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[151]_i_1 
       (.I0(s3_HBM_READ_tdata[151]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[151]),
        .I3(s1_HBM_READ_tdata[151]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[151]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[152] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[152]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[152]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[152]_i_1 
       (.I0(s3_HBM_READ_tdata[152]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[152]),
        .I3(s1_HBM_READ_tdata[152]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[152]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[153] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[153]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[153]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[153]_i_1 
       (.I0(s3_HBM_READ_tdata[153]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[153]),
        .I3(s1_HBM_READ_tdata[153]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[153]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[154] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[154]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[154]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[154]_i_1 
       (.I0(s3_HBM_READ_tdata[154]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[154]),
        .I3(s1_HBM_READ_tdata[154]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[154]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[155] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[155]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[155]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[155]_i_1 
       (.I0(s3_HBM_READ_tdata[155]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[155]),
        .I3(s1_HBM_READ_tdata[155]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[155]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[156] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[156]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[156]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[156]_i_1 
       (.I0(s3_HBM_READ_tdata[156]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[156]),
        .I3(s1_HBM_READ_tdata[156]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[156]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[157] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[157]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[157]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[157]_i_1 
       (.I0(s3_HBM_READ_tdata[157]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[157]),
        .I3(s1_HBM_READ_tdata[157]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[157]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[158] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[158]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[158]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[158]_i_1 
       (.I0(s3_HBM_READ_tdata[158]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[158]),
        .I3(s1_HBM_READ_tdata[158]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[158]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[159] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[159]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[159]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[159]_i_1 
       (.I0(s3_HBM_READ_tdata[159]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[159]),
        .I3(s1_HBM_READ_tdata[159]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[159]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[15] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[15]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[15]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[15]_i_1 
       (.I0(s3_HBM_READ_tdata[15]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[15]),
        .I3(s1_HBM_READ_tdata[15]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[15]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[160] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[160]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[160]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[160]_i_1 
       (.I0(s3_HBM_READ_tdata[160]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[160]),
        .I3(s1_HBM_READ_tdata[160]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[160]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[161] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[161]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[161]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[161]_i_1 
       (.I0(s3_HBM_READ_tdata[161]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[161]),
        .I3(s1_HBM_READ_tdata[161]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[161]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[162] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[162]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[162]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[162]_i_1 
       (.I0(s3_HBM_READ_tdata[162]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[162]),
        .I3(s1_HBM_READ_tdata[162]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[162]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[163] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[163]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[163]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[163]_i_1 
       (.I0(s3_HBM_READ_tdata[163]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[163]),
        .I3(s1_HBM_READ_tdata[163]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[163]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[164] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[164]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[164]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[164]_i_1 
       (.I0(s3_HBM_READ_tdata[164]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[164]),
        .I3(s1_HBM_READ_tdata[164]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[164]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[165] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[165]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[165]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[165]_i_1 
       (.I0(s3_HBM_READ_tdata[165]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[165]),
        .I3(s1_HBM_READ_tdata[165]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[165]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[166] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[166]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[166]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[166]_i_1 
       (.I0(s3_HBM_READ_tdata[166]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[166]),
        .I3(s1_HBM_READ_tdata[166]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[166]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[167] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[167]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[167]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[167]_i_1 
       (.I0(s3_HBM_READ_tdata[167]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[167]),
        .I3(s1_HBM_READ_tdata[167]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[167]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[168] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[168]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[168]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[168]_i_1 
       (.I0(s3_HBM_READ_tdata[168]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[168]),
        .I3(s1_HBM_READ_tdata[168]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[168]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[169] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[169]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[169]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[169]_i_1 
       (.I0(s3_HBM_READ_tdata[169]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[169]),
        .I3(s1_HBM_READ_tdata[169]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[169]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[16] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[16]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[16]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[16]_i_1 
       (.I0(s3_HBM_READ_tdata[16]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[16]),
        .I3(s1_HBM_READ_tdata[16]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[16]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[170] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[170]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[170]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[170]_i_1 
       (.I0(s3_HBM_READ_tdata[170]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[170]),
        .I3(s1_HBM_READ_tdata[170]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[170]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[171] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[171]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[171]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[171]_i_1 
       (.I0(s3_HBM_READ_tdata[171]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[171]),
        .I3(s1_HBM_READ_tdata[171]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[171]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[172] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[172]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[172]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[172]_i_1 
       (.I0(s3_HBM_READ_tdata[172]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[172]),
        .I3(s1_HBM_READ_tdata[172]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[172]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[173] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[173]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[173]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[173]_i_1 
       (.I0(s3_HBM_READ_tdata[173]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[173]),
        .I3(s1_HBM_READ_tdata[173]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[173]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[174] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[174]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[174]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[174]_i_1 
       (.I0(s3_HBM_READ_tdata[174]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[174]),
        .I3(s1_HBM_READ_tdata[174]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[174]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[175] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[175]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[175]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[175]_i_1 
       (.I0(s3_HBM_READ_tdata[175]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[175]),
        .I3(s1_HBM_READ_tdata[175]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[175]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[176] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[176]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[176]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[176]_i_1 
       (.I0(s3_HBM_READ_tdata[176]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[176]),
        .I3(s1_HBM_READ_tdata[176]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[176]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[177] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[177]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[177]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[177]_i_1 
       (.I0(s3_HBM_READ_tdata[177]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[177]),
        .I3(s1_HBM_READ_tdata[177]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[177]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[178] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[178]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[178]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[178]_i_1 
       (.I0(s3_HBM_READ_tdata[178]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[178]),
        .I3(s1_HBM_READ_tdata[178]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[178]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[179] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[179]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[179]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[179]_i_1 
       (.I0(s3_HBM_READ_tdata[179]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[179]),
        .I3(s1_HBM_READ_tdata[179]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[179]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[17] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[17]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[17]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[17]_i_1 
       (.I0(s3_HBM_READ_tdata[17]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[17]),
        .I3(s1_HBM_READ_tdata[17]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[17]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[180] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[180]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[180]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[180]_i_1 
       (.I0(s3_HBM_READ_tdata[180]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[180]),
        .I3(s1_HBM_READ_tdata[180]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[180]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[181] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[181]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[181]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[181]_i_1 
       (.I0(s3_HBM_READ_tdata[181]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[181]),
        .I3(s1_HBM_READ_tdata[181]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[181]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[182] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[182]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[182]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[182]_i_1 
       (.I0(s3_HBM_READ_tdata[182]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[182]),
        .I3(s1_HBM_READ_tdata[182]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[182]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[183] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[183]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[183]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[183]_i_1 
       (.I0(s3_HBM_READ_tdata[183]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[183]),
        .I3(s1_HBM_READ_tdata[183]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[183]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[184] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[184]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[184]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[184]_i_1 
       (.I0(s3_HBM_READ_tdata[184]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[184]),
        .I3(s1_HBM_READ_tdata[184]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[184]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[185] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[185]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[185]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[185]_i_1 
       (.I0(s3_HBM_READ_tdata[185]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[185]),
        .I3(s1_HBM_READ_tdata[185]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[185]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[186] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[186]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[186]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[186]_i_1 
       (.I0(s3_HBM_READ_tdata[186]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[186]),
        .I3(s1_HBM_READ_tdata[186]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[186]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[187] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[187]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[187]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[187]_i_1 
       (.I0(s3_HBM_READ_tdata[187]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[187]),
        .I3(s1_HBM_READ_tdata[187]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[187]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[188] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[188]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[188]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[188]_i_1 
       (.I0(s3_HBM_READ_tdata[188]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[188]),
        .I3(s1_HBM_READ_tdata[188]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[188]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[189] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[189]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[189]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[189]_i_1 
       (.I0(s3_HBM_READ_tdata[189]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[189]),
        .I3(s1_HBM_READ_tdata[189]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[189]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[18] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[18]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[18]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[18]_i_1 
       (.I0(s3_HBM_READ_tdata[18]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[18]),
        .I3(s1_HBM_READ_tdata[18]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[18]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[190] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[190]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[190]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[190]_i_1 
       (.I0(s3_HBM_READ_tdata[190]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[190]),
        .I3(s1_HBM_READ_tdata[190]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[190]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[191] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[191]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[191]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[191]_i_1 
       (.I0(s3_HBM_READ_tdata[191]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[191]),
        .I3(s1_HBM_READ_tdata[191]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[191]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[192] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[192]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[192]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[192]_i_1 
       (.I0(s3_HBM_READ_tdata[192]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[192]),
        .I3(s1_HBM_READ_tdata[192]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[192]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[193] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[193]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[193]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[193]_i_1 
       (.I0(s3_HBM_READ_tdata[193]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[193]),
        .I3(s1_HBM_READ_tdata[193]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[193]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[194] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[194]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[194]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[194]_i_1 
       (.I0(s3_HBM_READ_tdata[194]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[194]),
        .I3(s1_HBM_READ_tdata[194]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[194]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[195] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[195]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[195]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[195]_i_1 
       (.I0(s3_HBM_READ_tdata[195]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[195]),
        .I3(s1_HBM_READ_tdata[195]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[195]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[196] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[196]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[196]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[196]_i_1 
       (.I0(s3_HBM_READ_tdata[196]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[196]),
        .I3(s1_HBM_READ_tdata[196]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[196]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[197] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[197]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[197]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[197]_i_1 
       (.I0(s3_HBM_READ_tdata[197]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[197]),
        .I3(s1_HBM_READ_tdata[197]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[197]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[198] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[198]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[198]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[198]_i_1 
       (.I0(s3_HBM_READ_tdata[198]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[198]),
        .I3(s1_HBM_READ_tdata[198]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[198]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[199] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[199]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[199]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[199]_i_1 
       (.I0(s3_HBM_READ_tdata[199]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[199]),
        .I3(s1_HBM_READ_tdata[199]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[199]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[19] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[19]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[19]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[19]_i_1 
       (.I0(s3_HBM_READ_tdata[19]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[19]),
        .I3(s1_HBM_READ_tdata[19]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[19]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[1] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[1]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[1]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[1]_i_1 
       (.I0(s3_HBM_READ_tdata[1]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[1]),
        .I3(s1_HBM_READ_tdata[1]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[1]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[200] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[200]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[200]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[200]_i_1 
       (.I0(s3_HBM_READ_tdata[200]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[200]),
        .I3(s1_HBM_READ_tdata[200]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[200]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[201] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[201]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[201]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[201]_i_1 
       (.I0(s3_HBM_READ_tdata[201]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[201]),
        .I3(s1_HBM_READ_tdata[201]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[201]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[202] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[202]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[202]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[202]_i_1 
       (.I0(s3_HBM_READ_tdata[202]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[202]),
        .I3(s1_HBM_READ_tdata[202]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[202]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[203] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[203]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[203]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[203]_i_1 
       (.I0(s3_HBM_READ_tdata[203]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[203]),
        .I3(s1_HBM_READ_tdata[203]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[203]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[204] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[204]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[204]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[204]_i_1 
       (.I0(s3_HBM_READ_tdata[204]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[204]),
        .I3(s1_HBM_READ_tdata[204]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[204]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[205] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[205]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[205]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[205]_i_1 
       (.I0(s3_HBM_READ_tdata[205]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[205]),
        .I3(s1_HBM_READ_tdata[205]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[205]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[206] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[206]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[206]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[206]_i_1 
       (.I0(s3_HBM_READ_tdata[206]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[206]),
        .I3(s1_HBM_READ_tdata[206]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[206]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[207] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[207]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[207]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[207]_i_1 
       (.I0(s3_HBM_READ_tdata[207]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[207]),
        .I3(s1_HBM_READ_tdata[207]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[207]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[208] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[208]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[208]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[208]_i_1 
       (.I0(s3_HBM_READ_tdata[208]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[208]),
        .I3(s1_HBM_READ_tdata[208]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[208]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[209] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[209]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[209]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[209]_i_1 
       (.I0(s3_HBM_READ_tdata[209]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[209]),
        .I3(s1_HBM_READ_tdata[209]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[209]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[20] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[20]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[20]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[20]_i_1 
       (.I0(s3_HBM_READ_tdata[20]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[20]),
        .I3(s1_HBM_READ_tdata[20]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[20]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[210] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[210]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[210]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[210]_i_1 
       (.I0(s3_HBM_READ_tdata[210]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[210]),
        .I3(s1_HBM_READ_tdata[210]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[210]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[211] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[211]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[211]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[211]_i_1 
       (.I0(s3_HBM_READ_tdata[211]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[211]),
        .I3(s1_HBM_READ_tdata[211]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[211]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[212] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[212]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[212]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[212]_i_1 
       (.I0(s3_HBM_READ_tdata[212]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[212]),
        .I3(s1_HBM_READ_tdata[212]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[212]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[213] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[213]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[213]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[213]_i_1 
       (.I0(s3_HBM_READ_tdata[213]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[213]),
        .I3(s1_HBM_READ_tdata[213]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[213]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[214] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[214]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[214]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[214]_i_1 
       (.I0(s3_HBM_READ_tdata[214]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[214]),
        .I3(s1_HBM_READ_tdata[214]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[214]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[215] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[215]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[215]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[215]_i_1 
       (.I0(s3_HBM_READ_tdata[215]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[215]),
        .I3(s1_HBM_READ_tdata[215]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[215]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[216] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[216]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[216]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[216]_i_1 
       (.I0(s3_HBM_READ_tdata[216]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[216]),
        .I3(s1_HBM_READ_tdata[216]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[216]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[217] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[217]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[217]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[217]_i_1 
       (.I0(s3_HBM_READ_tdata[217]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[217]),
        .I3(s1_HBM_READ_tdata[217]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[217]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[218] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[218]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[218]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[218]_i_1 
       (.I0(s3_HBM_READ_tdata[218]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[218]),
        .I3(s1_HBM_READ_tdata[218]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[218]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[219] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[219]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[219]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[219]_i_1 
       (.I0(s3_HBM_READ_tdata[219]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[219]),
        .I3(s1_HBM_READ_tdata[219]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[219]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[21] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[21]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[21]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[21]_i_1 
       (.I0(s3_HBM_READ_tdata[21]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[21]),
        .I3(s1_HBM_READ_tdata[21]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[21]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[220] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[220]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[220]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[220]_i_1 
       (.I0(s3_HBM_READ_tdata[220]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[220]),
        .I3(s1_HBM_READ_tdata[220]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[220]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[221] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[221]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[221]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[221]_i_1 
       (.I0(s3_HBM_READ_tdata[221]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[221]),
        .I3(s1_HBM_READ_tdata[221]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[221]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[222] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[222]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[222]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[222]_i_1 
       (.I0(s3_HBM_READ_tdata[222]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[222]),
        .I3(s1_HBM_READ_tdata[222]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[222]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[223] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[223]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[223]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[223]_i_1 
       (.I0(s3_HBM_READ_tdata[223]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[223]),
        .I3(s1_HBM_READ_tdata[223]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[223]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[224] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[224]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[224]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[224]_i_1 
       (.I0(s3_HBM_READ_tdata[224]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[224]),
        .I3(s1_HBM_READ_tdata[224]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[224]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[225] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[225]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[225]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[225]_i_1 
       (.I0(s3_HBM_READ_tdata[225]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[225]),
        .I3(s1_HBM_READ_tdata[225]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[225]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[226] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[226]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[226]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[226]_i_1 
       (.I0(s3_HBM_READ_tdata[226]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[226]),
        .I3(s1_HBM_READ_tdata[226]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[226]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[227] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[227]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[227]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[227]_i_1 
       (.I0(s3_HBM_READ_tdata[227]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[227]),
        .I3(s1_HBM_READ_tdata[227]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[227]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[228] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[228]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[228]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[228]_i_1 
       (.I0(s3_HBM_READ_tdata[228]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[228]),
        .I3(s1_HBM_READ_tdata[228]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[228]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[229] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[229]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[229]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[229]_i_1 
       (.I0(s3_HBM_READ_tdata[229]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[229]),
        .I3(s1_HBM_READ_tdata[229]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[229]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[22] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[22]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[22]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[22]_i_1 
       (.I0(s3_HBM_READ_tdata[22]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[22]),
        .I3(s1_HBM_READ_tdata[22]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[22]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[230] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[230]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[230]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[230]_i_1 
       (.I0(s3_HBM_READ_tdata[230]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[230]),
        .I3(s1_HBM_READ_tdata[230]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[230]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[231] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[231]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[231]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[231]_i_1 
       (.I0(s3_HBM_READ_tdata[231]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[231]),
        .I3(s1_HBM_READ_tdata[231]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[231]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[232] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[232]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[232]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[232]_i_1 
       (.I0(s3_HBM_READ_tdata[232]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[232]),
        .I3(s1_HBM_READ_tdata[232]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[232]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[233] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[233]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[233]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[233]_i_1 
       (.I0(s3_HBM_READ_tdata[233]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[233]),
        .I3(s1_HBM_READ_tdata[233]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[233]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[234] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[234]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[234]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[234]_i_1 
       (.I0(s3_HBM_READ_tdata[234]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[234]),
        .I3(s1_HBM_READ_tdata[234]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[234]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[235] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[235]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[235]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[235]_i_1 
       (.I0(s3_HBM_READ_tdata[235]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[235]),
        .I3(s1_HBM_READ_tdata[235]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[235]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[236] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[236]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[236]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[236]_i_1 
       (.I0(s3_HBM_READ_tdata[236]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[236]),
        .I3(s1_HBM_READ_tdata[236]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[236]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[237] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[237]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[237]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[237]_i_1 
       (.I0(s3_HBM_READ_tdata[237]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[237]),
        .I3(s1_HBM_READ_tdata[237]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[237]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[238] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[238]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[238]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[238]_i_1 
       (.I0(s3_HBM_READ_tdata[238]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[238]),
        .I3(s1_HBM_READ_tdata[238]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[238]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[239] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[239]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[239]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[239]_i_1 
       (.I0(s3_HBM_READ_tdata[239]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[239]),
        .I3(s1_HBM_READ_tdata[239]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[239]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[23] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[23]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[23]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[23]_i_1 
       (.I0(s3_HBM_READ_tdata[23]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[23]),
        .I3(s1_HBM_READ_tdata[23]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[23]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[240] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[240]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[240]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[240]_i_1 
       (.I0(s3_HBM_READ_tdata[240]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[240]),
        .I3(s1_HBM_READ_tdata[240]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[240]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[241] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[241]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[241]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[241]_i_1 
       (.I0(s3_HBM_READ_tdata[241]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[241]),
        .I3(s1_HBM_READ_tdata[241]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[241]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[242] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[242]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[242]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[242]_i_1 
       (.I0(s3_HBM_READ_tdata[242]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[242]),
        .I3(s1_HBM_READ_tdata[242]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[242]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[243] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[243]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[243]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[243]_i_1 
       (.I0(s3_HBM_READ_tdata[243]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[243]),
        .I3(s1_HBM_READ_tdata[243]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[243]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[244] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[244]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[244]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[244]_i_1 
       (.I0(s3_HBM_READ_tdata[244]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[244]),
        .I3(s1_HBM_READ_tdata[244]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[244]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[245] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[245]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[245]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[245]_i_1 
       (.I0(s3_HBM_READ_tdata[245]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[245]),
        .I3(s1_HBM_READ_tdata[245]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[245]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[246] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[246]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[246]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[246]_i_1 
       (.I0(s3_HBM_READ_tdata[246]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[246]),
        .I3(s1_HBM_READ_tdata[246]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[246]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[247] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[247]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[247]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[247]_i_1 
       (.I0(s3_HBM_READ_tdata[247]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[247]),
        .I3(s1_HBM_READ_tdata[247]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[247]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[248] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[248]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[248]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[248]_i_1 
       (.I0(s3_HBM_READ_tdata[248]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[248]),
        .I3(s1_HBM_READ_tdata[248]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[248]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[249] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[249]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[249]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[249]_i_1 
       (.I0(s3_HBM_READ_tdata[249]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[249]),
        .I3(s1_HBM_READ_tdata[249]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[249]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[24] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[24]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[24]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[24]_i_1 
       (.I0(s3_HBM_READ_tdata[24]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[24]),
        .I3(s1_HBM_READ_tdata[24]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[24]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[250] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[250]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[250]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[250]_i_1 
       (.I0(s3_HBM_READ_tdata[250]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[250]),
        .I3(s1_HBM_READ_tdata[250]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[250]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[251] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[251]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[251]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[251]_i_1 
       (.I0(s3_HBM_READ_tdata[251]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[251]),
        .I3(s1_HBM_READ_tdata[251]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[251]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[252] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[252]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[252]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[252]_i_1 
       (.I0(s3_HBM_READ_tdata[252]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[252]),
        .I3(s1_HBM_READ_tdata[252]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[252]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[253] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[253]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[253]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[253]_i_1 
       (.I0(s3_HBM_READ_tdata[253]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[253]),
        .I3(s1_HBM_READ_tdata[253]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[253]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[254] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[254]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[254]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[254]_i_1 
       (.I0(s3_HBM_READ_tdata[254]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[254]),
        .I3(s1_HBM_READ_tdata[254]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[254]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[255] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[255]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[255]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[255]_i_1 
       (.I0(s3_HBM_READ_tdata[255]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[255]),
        .I3(s1_HBM_READ_tdata[255]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[255]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \m_HBM_READ_tdata_reg[255]_i_2 
       (.I0(mux_select[0]),
        .I1(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[25] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[25]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[25]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[25]_i_1 
       (.I0(s3_HBM_READ_tdata[25]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[25]),
        .I3(s1_HBM_READ_tdata[25]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[25]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[26] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[26]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[26]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[26]_i_1 
       (.I0(s3_HBM_READ_tdata[26]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[26]),
        .I3(s1_HBM_READ_tdata[26]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[26]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[27] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[27]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[27]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[27]_i_1 
       (.I0(s3_HBM_READ_tdata[27]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[27]),
        .I3(s1_HBM_READ_tdata[27]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[27]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[28] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[28]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[28]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[28]_i_1 
       (.I0(s3_HBM_READ_tdata[28]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[28]),
        .I3(s1_HBM_READ_tdata[28]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[28]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[29] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[29]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[29]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[29]_i_1 
       (.I0(s3_HBM_READ_tdata[29]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[29]),
        .I3(s1_HBM_READ_tdata[29]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[29]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[2] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[2]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[2]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[2]_i_1 
       (.I0(s3_HBM_READ_tdata[2]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[2]),
        .I3(s1_HBM_READ_tdata[2]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[2]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[30] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[30]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[30]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[30]_i_1 
       (.I0(s3_HBM_READ_tdata[30]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[30]),
        .I3(s1_HBM_READ_tdata[30]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[30]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[31] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[31]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[31]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[31]_i_1 
       (.I0(s3_HBM_READ_tdata[31]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[31]),
        .I3(s1_HBM_READ_tdata[31]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[31]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[32] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[32]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[32]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[32]_i_1 
       (.I0(s3_HBM_READ_tdata[32]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[32]),
        .I3(s1_HBM_READ_tdata[32]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[32]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[33] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[33]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[33]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[33]_i_1 
       (.I0(s3_HBM_READ_tdata[33]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[33]),
        .I3(s1_HBM_READ_tdata[33]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[33]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[34] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[34]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[34]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[34]_i_1 
       (.I0(s3_HBM_READ_tdata[34]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[34]),
        .I3(s1_HBM_READ_tdata[34]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[34]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[35] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[35]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[35]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[35]_i_1 
       (.I0(s3_HBM_READ_tdata[35]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[35]),
        .I3(s1_HBM_READ_tdata[35]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[35]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[36] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[36]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[36]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[36]_i_1 
       (.I0(s3_HBM_READ_tdata[36]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[36]),
        .I3(s1_HBM_READ_tdata[36]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[36]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[37] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[37]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[37]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[37]_i_1 
       (.I0(s3_HBM_READ_tdata[37]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[37]),
        .I3(s1_HBM_READ_tdata[37]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[37]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[38] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[38]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[38]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[38]_i_1 
       (.I0(s3_HBM_READ_tdata[38]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[38]),
        .I3(s1_HBM_READ_tdata[38]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[38]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[39] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[39]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[39]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[39]_i_1 
       (.I0(s3_HBM_READ_tdata[39]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[39]),
        .I3(s1_HBM_READ_tdata[39]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[39]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[3] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[3]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[3]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[3]_i_1 
       (.I0(s3_HBM_READ_tdata[3]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[3]),
        .I3(s1_HBM_READ_tdata[3]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[3]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[40] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[40]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[40]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[40]_i_1 
       (.I0(s3_HBM_READ_tdata[40]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[40]),
        .I3(s1_HBM_READ_tdata[40]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[40]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[41] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[41]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[41]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[41]_i_1 
       (.I0(s3_HBM_READ_tdata[41]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[41]),
        .I3(s1_HBM_READ_tdata[41]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[41]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[42] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[42]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[42]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[42]_i_1 
       (.I0(s3_HBM_READ_tdata[42]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[42]),
        .I3(s1_HBM_READ_tdata[42]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[42]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[43] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[43]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[43]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[43]_i_1 
       (.I0(s3_HBM_READ_tdata[43]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[43]),
        .I3(s1_HBM_READ_tdata[43]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[43]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[44] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[44]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[44]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[44]_i_1 
       (.I0(s3_HBM_READ_tdata[44]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[44]),
        .I3(s1_HBM_READ_tdata[44]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[44]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[45] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[45]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[45]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[45]_i_1 
       (.I0(s3_HBM_READ_tdata[45]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[45]),
        .I3(s1_HBM_READ_tdata[45]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[45]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[46] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[46]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[46]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[46]_i_1 
       (.I0(s3_HBM_READ_tdata[46]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[46]),
        .I3(s1_HBM_READ_tdata[46]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[46]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[47] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[47]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[47]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[47]_i_1 
       (.I0(s3_HBM_READ_tdata[47]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[47]),
        .I3(s1_HBM_READ_tdata[47]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[47]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[48] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[48]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[48]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[48]_i_1 
       (.I0(s3_HBM_READ_tdata[48]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[48]),
        .I3(s1_HBM_READ_tdata[48]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[48]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[49] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[49]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[49]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[49]_i_1 
       (.I0(s3_HBM_READ_tdata[49]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[49]),
        .I3(s1_HBM_READ_tdata[49]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[49]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[4] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[4]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[4]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[4]_i_1 
       (.I0(s3_HBM_READ_tdata[4]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[4]),
        .I3(s1_HBM_READ_tdata[4]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[4]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[50] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[50]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[50]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[50]_i_1 
       (.I0(s3_HBM_READ_tdata[50]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[50]),
        .I3(s1_HBM_READ_tdata[50]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[50]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[51] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[51]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[51]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[51]_i_1 
       (.I0(s3_HBM_READ_tdata[51]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[51]),
        .I3(s1_HBM_READ_tdata[51]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[51]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[52] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[52]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[52]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[52]_i_1 
       (.I0(s3_HBM_READ_tdata[52]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[52]),
        .I3(s1_HBM_READ_tdata[52]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[52]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[53] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[53]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[53]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[53]_i_1 
       (.I0(s3_HBM_READ_tdata[53]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[53]),
        .I3(s1_HBM_READ_tdata[53]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[53]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[54] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[54]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[54]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[54]_i_1 
       (.I0(s3_HBM_READ_tdata[54]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[54]),
        .I3(s1_HBM_READ_tdata[54]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[54]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[55] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[55]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[55]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[55]_i_1 
       (.I0(s3_HBM_READ_tdata[55]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[55]),
        .I3(s1_HBM_READ_tdata[55]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[55]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[56] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[56]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[56]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[56]_i_1 
       (.I0(s3_HBM_READ_tdata[56]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[56]),
        .I3(s1_HBM_READ_tdata[56]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[56]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[57] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[57]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[57]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[57]_i_1 
       (.I0(s3_HBM_READ_tdata[57]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[57]),
        .I3(s1_HBM_READ_tdata[57]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[57]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[58] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[58]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[58]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[58]_i_1 
       (.I0(s3_HBM_READ_tdata[58]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[58]),
        .I3(s1_HBM_READ_tdata[58]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[58]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[59] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[59]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[59]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[59]_i_1 
       (.I0(s3_HBM_READ_tdata[59]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[59]),
        .I3(s1_HBM_READ_tdata[59]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[59]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[5] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[5]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[5]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[5]_i_1 
       (.I0(s3_HBM_READ_tdata[5]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[5]),
        .I3(s1_HBM_READ_tdata[5]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[5]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[60] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[60]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[60]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[60]_i_1 
       (.I0(s3_HBM_READ_tdata[60]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[60]),
        .I3(s1_HBM_READ_tdata[60]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[60]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[61] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[61]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[61]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[61]_i_1 
       (.I0(s3_HBM_READ_tdata[61]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[61]),
        .I3(s1_HBM_READ_tdata[61]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[61]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[62] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[62]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[62]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[62]_i_1 
       (.I0(s3_HBM_READ_tdata[62]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[62]),
        .I3(s1_HBM_READ_tdata[62]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[62]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[63] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[63]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[63]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[63]_i_1 
       (.I0(s3_HBM_READ_tdata[63]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[63]),
        .I3(s1_HBM_READ_tdata[63]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[63]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[64] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[64]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[64]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[64]_i_1 
       (.I0(s3_HBM_READ_tdata[64]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[64]),
        .I3(s1_HBM_READ_tdata[64]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[64]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[65] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[65]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[65]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[65]_i_1 
       (.I0(s3_HBM_READ_tdata[65]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[65]),
        .I3(s1_HBM_READ_tdata[65]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[65]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[66] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[66]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[66]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[66]_i_1 
       (.I0(s3_HBM_READ_tdata[66]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[66]),
        .I3(s1_HBM_READ_tdata[66]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[66]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[67] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[67]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[67]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[67]_i_1 
       (.I0(s3_HBM_READ_tdata[67]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[67]),
        .I3(s1_HBM_READ_tdata[67]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[67]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[68] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[68]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[68]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[68]_i_1 
       (.I0(s3_HBM_READ_tdata[68]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[68]),
        .I3(s1_HBM_READ_tdata[68]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[68]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[69] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[69]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[69]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[69]_i_1 
       (.I0(s3_HBM_READ_tdata[69]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[69]),
        .I3(s1_HBM_READ_tdata[69]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[69]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[6] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[6]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[6]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[6]_i_1 
       (.I0(s3_HBM_READ_tdata[6]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[6]),
        .I3(s1_HBM_READ_tdata[6]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[6]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[70] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[70]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[70]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[70]_i_1 
       (.I0(s3_HBM_READ_tdata[70]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[70]),
        .I3(s1_HBM_READ_tdata[70]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[70]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[71] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[71]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[71]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[71]_i_1 
       (.I0(s3_HBM_READ_tdata[71]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[71]),
        .I3(s1_HBM_READ_tdata[71]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[71]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[72] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[72]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[72]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[72]_i_1 
       (.I0(s3_HBM_READ_tdata[72]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[72]),
        .I3(s1_HBM_READ_tdata[72]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[72]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[73] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[73]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[73]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[73]_i_1 
       (.I0(s3_HBM_READ_tdata[73]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[73]),
        .I3(s1_HBM_READ_tdata[73]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[73]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[74] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[74]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[74]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[74]_i_1 
       (.I0(s3_HBM_READ_tdata[74]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[74]),
        .I3(s1_HBM_READ_tdata[74]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[74]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[75] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[75]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[75]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[75]_i_1 
       (.I0(s3_HBM_READ_tdata[75]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[75]),
        .I3(s1_HBM_READ_tdata[75]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[75]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[76] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[76]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[76]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[76]_i_1 
       (.I0(s3_HBM_READ_tdata[76]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[76]),
        .I3(s1_HBM_READ_tdata[76]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[76]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[77] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[77]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[77]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[77]_i_1 
       (.I0(s3_HBM_READ_tdata[77]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[77]),
        .I3(s1_HBM_READ_tdata[77]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[77]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[78] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[78]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[78]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[78]_i_1 
       (.I0(s3_HBM_READ_tdata[78]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[78]),
        .I3(s1_HBM_READ_tdata[78]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[78]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[79] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[79]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[79]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[79]_i_1 
       (.I0(s3_HBM_READ_tdata[79]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[79]),
        .I3(s1_HBM_READ_tdata[79]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[79]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[7] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[7]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[7]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[7]_i_1 
       (.I0(s3_HBM_READ_tdata[7]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[7]),
        .I3(s1_HBM_READ_tdata[7]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[7]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[80] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[80]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[80]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[80]_i_1 
       (.I0(s3_HBM_READ_tdata[80]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[80]),
        .I3(s1_HBM_READ_tdata[80]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[80]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[81] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[81]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[81]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[81]_i_1 
       (.I0(s3_HBM_READ_tdata[81]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[81]),
        .I3(s1_HBM_READ_tdata[81]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[81]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[82] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[82]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[82]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[82]_i_1 
       (.I0(s3_HBM_READ_tdata[82]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[82]),
        .I3(s1_HBM_READ_tdata[82]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[82]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[83] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[83]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[83]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[83]_i_1 
       (.I0(s3_HBM_READ_tdata[83]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[83]),
        .I3(s1_HBM_READ_tdata[83]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[83]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[84] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[84]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[84]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[84]_i_1 
       (.I0(s3_HBM_READ_tdata[84]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[84]),
        .I3(s1_HBM_READ_tdata[84]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[84]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[85] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[85]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[85]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[85]_i_1 
       (.I0(s3_HBM_READ_tdata[85]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[85]),
        .I3(s1_HBM_READ_tdata[85]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[85]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[86] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[86]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[86]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[86]_i_1 
       (.I0(s3_HBM_READ_tdata[86]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[86]),
        .I3(s1_HBM_READ_tdata[86]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[86]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[87] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[87]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[87]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[87]_i_1 
       (.I0(s3_HBM_READ_tdata[87]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[87]),
        .I3(s1_HBM_READ_tdata[87]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[87]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[88] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[88]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[88]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[88]_i_1 
       (.I0(s3_HBM_READ_tdata[88]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[88]),
        .I3(s1_HBM_READ_tdata[88]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[88]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[89] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[89]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[89]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[89]_i_1 
       (.I0(s3_HBM_READ_tdata[89]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[89]),
        .I3(s1_HBM_READ_tdata[89]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[89]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[8] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[8]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[8]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[8]_i_1 
       (.I0(s3_HBM_READ_tdata[8]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[8]),
        .I3(s1_HBM_READ_tdata[8]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[8]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[90] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[90]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[90]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[90]_i_1 
       (.I0(s3_HBM_READ_tdata[90]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[90]),
        .I3(s1_HBM_READ_tdata[90]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[90]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[91] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[91]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[91]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[91]_i_1 
       (.I0(s3_HBM_READ_tdata[91]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[91]),
        .I3(s1_HBM_READ_tdata[91]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[91]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[92] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[92]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[92]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[92]_i_1 
       (.I0(s3_HBM_READ_tdata[92]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[92]),
        .I3(s1_HBM_READ_tdata[92]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[92]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[93] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[93]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[93]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[93]_i_1 
       (.I0(s3_HBM_READ_tdata[93]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[93]),
        .I3(s1_HBM_READ_tdata[93]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[93]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[94] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[94]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[94]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[94]_i_1 
       (.I0(s3_HBM_READ_tdata[94]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[94]),
        .I3(s1_HBM_READ_tdata[94]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[94]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[95] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[95]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[95]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[95]_i_1 
       (.I0(s3_HBM_READ_tdata[95]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[95]),
        .I3(s1_HBM_READ_tdata[95]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[95]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[96] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[96]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[96]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[96]_i_1 
       (.I0(s3_HBM_READ_tdata[96]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[96]),
        .I3(s1_HBM_READ_tdata[96]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[96]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[97] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[97]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[97]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[97]_i_1 
       (.I0(s3_HBM_READ_tdata[97]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[97]),
        .I3(s1_HBM_READ_tdata[97]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[97]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[98] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[98]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[98]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[98]_i_1 
       (.I0(s3_HBM_READ_tdata[98]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[98]),
        .I3(s1_HBM_READ_tdata[98]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[98]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[99] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[99]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[99]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[99]_i_1 
       (.I0(s3_HBM_READ_tdata[99]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[99]),
        .I3(s1_HBM_READ_tdata[99]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[99]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tdata_reg[9] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tdata_reg[9]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tdata[9]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tdata_reg[9]_i_1 
       (.I0(s3_HBM_READ_tdata[9]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tdata[9]),
        .I3(s1_HBM_READ_tdata[9]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tdata_reg[9]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tkeep_reg[0] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tkeep_reg[0]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tkeep[0]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tkeep_reg[0]_i_1 
       (.I0(s3_HBM_READ_tkeep[0]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tkeep[0]),
        .I3(s1_HBM_READ_tkeep[0]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tkeep_reg[0]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tkeep_reg[10] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tkeep_reg[10]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tkeep[10]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tkeep_reg[10]_i_1 
       (.I0(s3_HBM_READ_tkeep[10]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tkeep[10]),
        .I3(s1_HBM_READ_tkeep[10]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tkeep_reg[10]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tkeep_reg[11] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tkeep_reg[11]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tkeep[11]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tkeep_reg[11]_i_1 
       (.I0(s3_HBM_READ_tkeep[11]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tkeep[11]),
        .I3(s1_HBM_READ_tkeep[11]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tkeep_reg[11]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tkeep_reg[12] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tkeep_reg[12]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tkeep[12]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tkeep_reg[12]_i_1 
       (.I0(s3_HBM_READ_tkeep[12]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tkeep[12]),
        .I3(s1_HBM_READ_tkeep[12]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tkeep_reg[12]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tkeep_reg[13] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tkeep_reg[13]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tkeep[13]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tkeep_reg[13]_i_1 
       (.I0(s3_HBM_READ_tkeep[13]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tkeep[13]),
        .I3(s1_HBM_READ_tkeep[13]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tkeep_reg[13]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tkeep_reg[14] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tkeep_reg[14]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tkeep[14]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tkeep_reg[14]_i_1 
       (.I0(s3_HBM_READ_tkeep[14]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tkeep[14]),
        .I3(s1_HBM_READ_tkeep[14]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tkeep_reg[14]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tkeep_reg[15] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tkeep_reg[15]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tkeep[15]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tkeep_reg[15]_i_1 
       (.I0(s3_HBM_READ_tkeep[15]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tkeep[15]),
        .I3(s1_HBM_READ_tkeep[15]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tkeep_reg[15]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tkeep_reg[16] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tkeep_reg[16]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tkeep[16]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tkeep_reg[16]_i_1 
       (.I0(s3_HBM_READ_tkeep[16]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tkeep[16]),
        .I3(s1_HBM_READ_tkeep[16]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tkeep_reg[16]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tkeep_reg[17] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tkeep_reg[17]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tkeep[17]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tkeep_reg[17]_i_1 
       (.I0(s3_HBM_READ_tkeep[17]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tkeep[17]),
        .I3(s1_HBM_READ_tkeep[17]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tkeep_reg[17]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tkeep_reg[18] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tkeep_reg[18]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tkeep[18]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tkeep_reg[18]_i_1 
       (.I0(s3_HBM_READ_tkeep[18]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tkeep[18]),
        .I3(s1_HBM_READ_tkeep[18]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tkeep_reg[18]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tkeep_reg[19] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tkeep_reg[19]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tkeep[19]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tkeep_reg[19]_i_1 
       (.I0(s3_HBM_READ_tkeep[19]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tkeep[19]),
        .I3(s1_HBM_READ_tkeep[19]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tkeep_reg[19]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tkeep_reg[1] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tkeep_reg[1]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tkeep[1]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tkeep_reg[1]_i_1 
       (.I0(s3_HBM_READ_tkeep[1]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tkeep[1]),
        .I3(s1_HBM_READ_tkeep[1]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tkeep_reg[1]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tkeep_reg[20] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tkeep_reg[20]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tkeep[20]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tkeep_reg[20]_i_1 
       (.I0(s3_HBM_READ_tkeep[20]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tkeep[20]),
        .I3(s1_HBM_READ_tkeep[20]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tkeep_reg[20]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tkeep_reg[21] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tkeep_reg[21]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tkeep[21]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tkeep_reg[21]_i_1 
       (.I0(s3_HBM_READ_tkeep[21]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tkeep[21]),
        .I3(s1_HBM_READ_tkeep[21]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tkeep_reg[21]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tkeep_reg[22] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tkeep_reg[22]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tkeep[22]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tkeep_reg[22]_i_1 
       (.I0(s3_HBM_READ_tkeep[22]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tkeep[22]),
        .I3(s1_HBM_READ_tkeep[22]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tkeep_reg[22]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tkeep_reg[23] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tkeep_reg[23]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tkeep[23]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tkeep_reg[23]_i_1 
       (.I0(s3_HBM_READ_tkeep[23]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tkeep[23]),
        .I3(s1_HBM_READ_tkeep[23]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tkeep_reg[23]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tkeep_reg[24] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tkeep_reg[24]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tkeep[24]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tkeep_reg[24]_i_1 
       (.I0(s3_HBM_READ_tkeep[24]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tkeep[24]),
        .I3(s1_HBM_READ_tkeep[24]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tkeep_reg[24]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tkeep_reg[25] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tkeep_reg[25]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tkeep[25]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tkeep_reg[25]_i_1 
       (.I0(s3_HBM_READ_tkeep[25]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tkeep[25]),
        .I3(s1_HBM_READ_tkeep[25]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tkeep_reg[25]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tkeep_reg[26] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tkeep_reg[26]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tkeep[26]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tkeep_reg[26]_i_1 
       (.I0(s3_HBM_READ_tkeep[26]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tkeep[26]),
        .I3(s1_HBM_READ_tkeep[26]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tkeep_reg[26]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tkeep_reg[27] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tkeep_reg[27]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tkeep[27]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tkeep_reg[27]_i_1 
       (.I0(s3_HBM_READ_tkeep[27]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tkeep[27]),
        .I3(s1_HBM_READ_tkeep[27]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tkeep_reg[27]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tkeep_reg[28] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tkeep_reg[28]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tkeep[28]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tkeep_reg[28]_i_1 
       (.I0(s3_HBM_READ_tkeep[28]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tkeep[28]),
        .I3(s1_HBM_READ_tkeep[28]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tkeep_reg[28]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tkeep_reg[29] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tkeep_reg[29]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tkeep[29]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tkeep_reg[29]_i_1 
       (.I0(s3_HBM_READ_tkeep[29]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tkeep[29]),
        .I3(s1_HBM_READ_tkeep[29]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tkeep_reg[29]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tkeep_reg[2] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tkeep_reg[2]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tkeep[2]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tkeep_reg[2]_i_1 
       (.I0(s3_HBM_READ_tkeep[2]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tkeep[2]),
        .I3(s1_HBM_READ_tkeep[2]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tkeep_reg[2]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tkeep_reg[30] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tkeep_reg[30]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tkeep[30]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tkeep_reg[30]_i_1 
       (.I0(s3_HBM_READ_tkeep[30]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tkeep[30]),
        .I3(s1_HBM_READ_tkeep[30]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tkeep_reg[30]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tkeep_reg[31] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tkeep_reg[31]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tkeep[31]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tkeep_reg[31]_i_1 
       (.I0(s3_HBM_READ_tkeep[31]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tkeep[31]),
        .I3(s1_HBM_READ_tkeep[31]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tkeep_reg[31]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tkeep_reg[3] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tkeep_reg[3]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tkeep[3]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tkeep_reg[3]_i_1 
       (.I0(s3_HBM_READ_tkeep[3]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tkeep[3]),
        .I3(s1_HBM_READ_tkeep[3]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tkeep_reg[3]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tkeep_reg[4] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tkeep_reg[4]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tkeep[4]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tkeep_reg[4]_i_1 
       (.I0(s3_HBM_READ_tkeep[4]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tkeep[4]),
        .I3(s1_HBM_READ_tkeep[4]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tkeep_reg[4]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tkeep_reg[5] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tkeep_reg[5]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tkeep[5]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tkeep_reg[5]_i_1 
       (.I0(s3_HBM_READ_tkeep[5]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tkeep[5]),
        .I3(s1_HBM_READ_tkeep[5]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tkeep_reg[5]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tkeep_reg[6] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tkeep_reg[6]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tkeep[6]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tkeep_reg[6]_i_1 
       (.I0(s3_HBM_READ_tkeep[6]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tkeep[6]),
        .I3(s1_HBM_READ_tkeep[6]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tkeep_reg[6]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tkeep_reg[7] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tkeep_reg[7]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tkeep[7]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tkeep_reg[7]_i_1 
       (.I0(s3_HBM_READ_tkeep[7]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tkeep[7]),
        .I3(s1_HBM_READ_tkeep[7]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tkeep_reg[7]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tkeep_reg[8] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tkeep_reg[8]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tkeep[8]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tkeep_reg[8]_i_1 
       (.I0(s3_HBM_READ_tkeep[8]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tkeep[8]),
        .I3(s1_HBM_READ_tkeep[8]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tkeep_reg[8]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \m_HBM_READ_tkeep_reg[9] 
       (.CLR(1'b0),
        .D(\m_HBM_READ_tkeep_reg[9]_i_1_n_0 ),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tkeep[9]));
  LUT5 #(
    .INIT(32'hB8B8FF00)) 
    \m_HBM_READ_tkeep_reg[9]_i_1 
       (.I0(s3_HBM_READ_tkeep[9]),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tkeep[9]),
        .I3(s1_HBM_READ_tkeep[9]),
        .I4(mux_select[1]),
        .O(\m_HBM_READ_tkeep_reg[9]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    m_HBM_READ_tlast__0
       (.I0(s3_HBM_READ_tlast),
        .I1(mux_select[0]),
        .I2(s2_HBM_READ_tlast),
        .I3(mux_select[1]),
        .I4(s1_HBM_READ_tlast),
        .O(m_HBM_READ_tlast__0_n_0));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    m_HBM_READ_tlast_reg
       (.CLR(1'b0),
        .D(m_HBM_READ_tlast__0_n_0),
        .G(\m_HBM_READ_tdata_reg[255]_i_2_n_0 ),
        .GE(1'b1),
        .Q(m_HBM_READ_tlast));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    m_HBM_READ_tvalid__0
       (.I0(s3_HBM_READ_tvalid),
        .I1(s1_HBM_READ_tvalid),
        .I2(mux_select[0]),
        .I3(mux_select[1]),
        .I4(s2_HBM_READ_tvalid),
        .O(m_HBM_READ_tvalid));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    m_HBM_WRITE_tready__0
       (.I0(s3_HBM_WRITE_tready),
        .I1(s1_HBM_WRITE_tready),
        .I2(mux_select[0]),
        .I3(mux_select[1]),
        .I4(s2_HBM_WRITE_tready),
        .O(m_HBM_WRITE_tready));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    m_busy__0
       (.I0(s3_busy),
        .I1(s1_busy),
        .I2(mux_select[0]),
        .I3(mux_select[1]),
        .I4(s2_busy),
        .O(m_busy));
  (* OPT_MODIFIED = "MLO" *) 
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0),
    .IS_G_INVERTED(1'b1)) 
    s1_HBM_READ_tready_reg
       (.CLR(1'b0),
        .D(s1_HBM_READ_tready_reg_i_1_n_0),
        .G(mux_select[1]),
        .GE(1'b1),
        .Q(s1_HBM_READ_tready));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'h8)) 
    s1_HBM_READ_tready_reg_i_1
       (.I0(mux_select[0]),
        .I1(m_HBM_READ_tready),
        .O(s1_HBM_READ_tready_reg_i_1_n_0));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[0] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[0]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[0]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[100] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[100]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[100]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[101] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[101]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[101]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[102] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[102]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[102]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[103] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[103]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[103]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[104] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[104]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[104]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[105] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[105]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[105]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[106] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[106]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[106]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[107] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[107]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[107]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[108] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[108]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[108]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[109] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[109]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[109]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[10] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[10]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[10]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[110] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[110]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[110]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[111] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[111]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[111]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[112] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[112]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[112]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[113] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[113]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[113]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[114] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[114]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[114]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[115] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[115]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[115]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[116] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[116]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[116]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[117] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[117]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[117]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[118] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[118]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[118]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[119] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[119]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[119]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[11] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[11]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[11]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[120] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[120]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[120]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[121] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[121]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[121]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[122] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[122]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[122]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[123] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[123]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[123]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[124] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[124]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[124]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[125] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[125]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[125]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[126] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[126]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[126]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[127] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[127]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[127]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[128] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[128]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[128]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[129] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[129]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[129]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[12] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[12]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[12]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[130] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[130]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[130]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[131] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[131]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[131]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[132] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[132]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[132]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[133] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[133]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[133]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[134] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[134]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[134]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[135] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[135]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[135]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[136] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[136]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[136]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[137] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[137]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[137]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[138] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[138]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[138]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[139] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[139]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[139]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[13] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[13]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[13]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[140] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[140]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[140]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[141] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[141]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[141]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[142] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[142]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[142]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[143] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[143]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[143]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[144] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[144]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[144]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[145] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[145]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[145]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[146] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[146]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[146]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[147] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[147]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[147]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[148] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[148]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[148]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[149] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[149]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[149]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[14] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[14]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[14]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[150] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[150]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[150]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[151] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[151]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[151]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[152] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[152]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[152]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[153] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[153]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[153]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[154] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[154]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[154]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[155] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[155]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[155]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[156] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[156]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[156]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[157] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[157]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[157]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[158] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[158]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[158]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[159] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[159]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[159]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[15] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[15]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[15]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[160] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[160]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[160]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[161] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[161]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[161]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[162] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[162]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[162]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[163] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[163]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[163]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[164] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[164]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[164]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[165] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[165]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[165]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[166] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[166]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[166]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[167] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[167]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[167]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[168] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[168]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[168]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[169] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[169]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[169]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[16] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[16]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[16]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[170] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[170]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[170]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[171] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[171]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[171]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[172] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[172]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[172]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[173] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[173]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[173]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[174] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[174]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[174]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[175] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[175]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[175]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[176] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[176]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[176]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[177] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[177]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[177]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[178] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[178]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[178]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[179] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[179]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[179]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[17] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[17]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[17]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[180] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[180]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[180]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[181] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[181]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[181]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[182] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[182]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[182]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[183] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[183]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[183]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[184] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[184]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[184]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[185] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[185]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[185]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[186] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[186]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[186]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[187] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[187]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[187]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[188] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[188]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[188]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[189] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[189]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[189]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[18] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[18]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[18]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[190] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[190]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[190]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[191] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[191]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[191]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[192] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[192]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[192]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[193] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[193]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[193]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[194] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[194]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[194]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[195] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[195]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[195]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[196] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[196]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[196]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[197] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[197]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[197]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[198] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[198]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[198]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[199] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[199]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[199]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[19] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[19]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[19]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[1] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[1]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[1]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[200] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[200]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[200]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[201] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[201]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[201]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[202] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[202]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[202]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[203] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[203]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[203]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[204] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[204]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[204]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[205] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[205]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[205]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[206] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[206]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[206]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[207] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[207]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[207]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[208] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[208]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[208]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[209] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[209]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[209]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[20] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[20]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[20]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[210] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[210]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[210]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[211] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[211]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[211]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[212] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[212]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[212]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[213] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[213]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[213]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[214] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[214]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[214]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[215] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[215]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[215]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[216] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[216]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[216]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[217] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[217]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[217]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[218] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[218]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[218]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[219] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[219]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[219]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[21] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[21]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[21]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[220] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[220]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[220]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[221] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[221]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[221]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[222] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[222]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[222]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[223] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[223]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[223]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[224] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[224]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[224]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[225] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[225]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[225]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[226] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[226]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[226]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[227] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[227]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[227]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[228] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[228]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[228]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[229] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[229]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[229]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[22] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[22]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[22]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[230] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[230]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[230]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[231] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[231]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[231]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[232] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[232]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[232]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[233] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[233]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[233]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[234] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[234]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[234]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[235] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[235]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[235]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[236] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[236]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[236]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[237] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[237]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[237]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[238] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[238]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[238]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[239] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[239]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[239]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[23] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[23]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[23]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[240] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[240]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[240]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[241] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[241]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[241]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[242] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[242]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[242]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[243] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[243]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[243]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[244] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[244]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[244]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[245] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[245]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[245]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[246] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[246]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[246]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[247] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[247]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[247]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[248] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[248]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[248]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[249] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[249]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[249]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[24] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[24]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[24]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[250] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[250]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[250]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[251] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[251]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[251]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[252] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[252]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[252]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[253] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[253]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[253]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[254] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[254]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[254]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[255] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[255]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[255]));
  LUT2 #(
    .INIT(4'h2)) 
    \s1_HBM_WRITE_tdata_reg[255]_i_1 
       (.I0(mux_select[0]),
        .I1(mux_select[1]),
        .O(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[25] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[25]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[25]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[26] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[26]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[26]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[27] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[27]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[27]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[28] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[28]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[28]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[29] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[29]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[29]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[2] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[2]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[2]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[30] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[30]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[30]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[31] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[31]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[31]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[32] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[32]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[32]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[33] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[33]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[33]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[34] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[34]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[34]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[35] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[35]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[35]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[36] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[36]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[36]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[37] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[37]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[37]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[38] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[38]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[38]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[39] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[39]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[39]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[3] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[3]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[3]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[40] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[40]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[40]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[41] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[41]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[41]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[42] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[42]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[42]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[43] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[43]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[43]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[44] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[44]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[44]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[45] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[45]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[45]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[46] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[46]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[46]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[47] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[47]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[47]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[48] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[48]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[48]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[49] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[49]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[49]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[4] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[4]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[4]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[50] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[50]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[50]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[51] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[51]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[51]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[52] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[52]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[52]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[53] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[53]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[53]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[54] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[54]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[54]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[55] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[55]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[55]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[56] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[56]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[56]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[57] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[57]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[57]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[58] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[58]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[58]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[59] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[59]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[59]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[5] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[5]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[5]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[60] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[60]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[60]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[61] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[61]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[61]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[62] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[62]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[62]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[63] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[63]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[63]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[64] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[64]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[64]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[65] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[65]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[65]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[66] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[66]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[66]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[67] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[67]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[67]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[68] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[68]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[68]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[69] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[69]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[69]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[6] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[6]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[6]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[70] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[70]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[70]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[71] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[71]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[71]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[72] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[72]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[72]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[73] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[73]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[73]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[74] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[74]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[74]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[75] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[75]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[75]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[76] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[76]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[76]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[77] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[77]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[77]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[78] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[78]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[78]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[79] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[79]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[79]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[7] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[7]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[7]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[80] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[80]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[80]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[81] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[81]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[81]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[82] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[82]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[82]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[83] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[83]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[83]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[84] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[84]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[84]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[85] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[85]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[85]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[86] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[86]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[86]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[87] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[87]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[87]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[88] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[88]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[88]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[89] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[89]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[89]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[8] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[8]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[8]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[90] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[90]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[90]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[91] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[91]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[91]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[92] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[92]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[92]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[93] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[93]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[93]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[94] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[94]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[94]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[95] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[95]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[95]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[96] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[96]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[96]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[97] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[97]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[97]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[98] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[98]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[98]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[99] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[99]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[99]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_HBM_WRITE_tdata_reg[9] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[9]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tdata[9]));
  (* OPT_MODIFIED = "MLO" *) 
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0),
    .IS_G_INVERTED(1'b1)) 
    s1_HBM_WRITE_tvalid_reg
       (.CLR(1'b0),
        .D(s1_HBM_WRITE_tvalid_reg_i_1_n_0),
        .G(mux_select[1]),
        .GE(1'b1),
        .Q(s1_HBM_WRITE_tvalid));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'h8)) 
    s1_HBM_WRITE_tvalid_reg_i_1
       (.I0(mux_select[0]),
        .I1(m_HBM_WRITE_tvalid),
        .O(s1_HBM_WRITE_tvalid_reg_i_1_n_0));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_blockamount_reg[0] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[0]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_blockamount[0]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_blockamount_reg[10] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[10]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_blockamount[10]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_blockamount_reg[11] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[11]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_blockamount[11]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_blockamount_reg[12] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[12]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_blockamount[12]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_blockamount_reg[13] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[13]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_blockamount[13]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_blockamount_reg[14] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[14]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_blockamount[14]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_blockamount_reg[15] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[15]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_blockamount[15]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_blockamount_reg[16] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[16]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_blockamount[16]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_blockamount_reg[17] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[17]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_blockamount[17]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_blockamount_reg[18] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[18]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_blockamount[18]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_blockamount_reg[19] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[19]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_blockamount[19]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_blockamount_reg[1] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[1]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_blockamount[1]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_blockamount_reg[20] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[20]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_blockamount[20]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_blockamount_reg[21] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[21]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_blockamount[21]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_blockamount_reg[22] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[22]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_blockamount[22]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_blockamount_reg[23] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[23]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_blockamount[23]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_blockamount_reg[24] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[24]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_blockamount[24]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_blockamount_reg[25] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[25]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_blockamount[25]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_blockamount_reg[26] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[26]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_blockamount[26]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_blockamount_reg[27] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[27]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_blockamount[27]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_blockamount_reg[28] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[28]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_blockamount[28]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_blockamount_reg[29] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[29]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_blockamount[29]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_blockamount_reg[2] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[2]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_blockamount[2]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_blockamount_reg[30] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[30]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_blockamount[30]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_blockamount_reg[31] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[31]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_blockamount[31]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_blockamount_reg[3] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[3]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_blockamount[3]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_blockamount_reg[4] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[4]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_blockamount[4]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_blockamount_reg[5] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[5]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_blockamount[5]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_blockamount_reg[6] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[6]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_blockamount[6]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_blockamount_reg[7] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[7]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_blockamount[7]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_blockamount_reg[8] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[8]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_blockamount[8]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_blockamount_reg[9] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[9]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_blockamount[9]));
  (* OPT_MODIFIED = "MLO" *) 
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0),
    .IS_G_INVERTED(1'b1)) 
    s1_rd_run_reg
       (.CLR(1'b0),
        .D(s1_rd_run_reg_i_1_n_0),
        .G(mux_select[1]),
        .GE(1'b1),
        .Q(s1_rd_run));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h8)) 
    s1_rd_run_reg_i_1
       (.I0(mux_select[0]),
        .I1(m_rd_run),
        .O(s1_rd_run_reg_i_1_n_0));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_startaddress_reg[0] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[0]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_startaddress[0]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_startaddress_reg[10] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[10]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_startaddress[10]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_startaddress_reg[11] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[11]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_startaddress[11]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_startaddress_reg[12] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[12]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_startaddress[12]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_startaddress_reg[13] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[13]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_startaddress[13]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_startaddress_reg[14] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[14]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_startaddress[14]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_startaddress_reg[15] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[15]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_startaddress[15]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_startaddress_reg[16] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[16]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_startaddress[16]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_startaddress_reg[17] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[17]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_startaddress[17]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_startaddress_reg[18] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[18]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_startaddress[18]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_startaddress_reg[19] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[19]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_startaddress[19]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_startaddress_reg[1] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[1]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_startaddress[1]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_startaddress_reg[20] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[20]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_startaddress[20]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_startaddress_reg[21] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[21]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_startaddress[21]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_startaddress_reg[22] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[22]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_startaddress[22]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_startaddress_reg[23] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[23]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_startaddress[23]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_startaddress_reg[24] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[24]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_startaddress[24]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_startaddress_reg[25] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[25]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_startaddress[25]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_startaddress_reg[26] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[26]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_startaddress[26]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_startaddress_reg[27] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[27]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_startaddress[27]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_startaddress_reg[28] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[28]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_startaddress[28]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_startaddress_reg[29] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[29]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_startaddress[29]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_startaddress_reg[2] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[2]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_startaddress[2]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_startaddress_reg[30] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[30]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_startaddress[30]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_startaddress_reg[31] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[31]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_startaddress[31]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_startaddress_reg[3] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[3]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_startaddress[3]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_startaddress_reg[4] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[4]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_startaddress[4]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_startaddress_reg[5] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[5]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_startaddress[5]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_startaddress_reg[6] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[6]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_startaddress[6]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_startaddress_reg[7] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[7]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_startaddress[7]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_startaddress_reg[8] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[8]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_startaddress[8]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_rd_startaddress_reg[9] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[9]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_rd_startaddress[9]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_blockamount_reg[0] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[0]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_blockamount[0]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_blockamount_reg[10] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[10]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_blockamount[10]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_blockamount_reg[11] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[11]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_blockamount[11]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_blockamount_reg[12] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[12]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_blockamount[12]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_blockamount_reg[13] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[13]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_blockamount[13]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_blockamount_reg[14] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[14]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_blockamount[14]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_blockamount_reg[15] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[15]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_blockamount[15]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_blockamount_reg[16] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[16]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_blockamount[16]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_blockamount_reg[17] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[17]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_blockamount[17]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_blockamount_reg[18] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[18]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_blockamount[18]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_blockamount_reg[19] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[19]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_blockamount[19]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_blockamount_reg[1] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[1]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_blockamount[1]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_blockamount_reg[20] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[20]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_blockamount[20]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_blockamount_reg[21] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[21]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_blockamount[21]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_blockamount_reg[22] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[22]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_blockamount[22]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_blockamount_reg[23] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[23]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_blockamount[23]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_blockamount_reg[24] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[24]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_blockamount[24]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_blockamount_reg[25] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[25]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_blockamount[25]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_blockamount_reg[26] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[26]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_blockamount[26]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_blockamount_reg[27] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[27]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_blockamount[27]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_blockamount_reg[28] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[28]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_blockamount[28]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_blockamount_reg[29] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[29]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_blockamount[29]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_blockamount_reg[2] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[2]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_blockamount[2]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_blockamount_reg[30] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[30]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_blockamount[30]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_blockamount_reg[31] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[31]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_blockamount[31]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_blockamount_reg[3] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[3]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_blockamount[3]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_blockamount_reg[4] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[4]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_blockamount[4]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_blockamount_reg[5] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[5]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_blockamount[5]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_blockamount_reg[6] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[6]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_blockamount[6]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_blockamount_reg[7] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[7]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_blockamount[7]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_blockamount_reg[8] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[8]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_blockamount[8]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_blockamount_reg[9] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[9]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_blockamount[9]));
  (* OPT_MODIFIED = "MLO" *) 
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0),
    .IS_G_INVERTED(1'b1)) 
    s1_wr_run_reg
       (.CLR(1'b0),
        .D(s1_wr_run_reg_i_1_n_0),
        .G(mux_select[1]),
        .GE(1'b1),
        .Q(s1_wr_run));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h8)) 
    s1_wr_run_reg_i_1
       (.I0(mux_select[0]),
        .I1(m_wr_run),
        .O(s1_wr_run_reg_i_1_n_0));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_startaddress_reg[0] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[0]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_startaddress[0]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_startaddress_reg[10] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[10]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_startaddress[10]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_startaddress_reg[11] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[11]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_startaddress[11]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_startaddress_reg[12] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[12]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_startaddress[12]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_startaddress_reg[13] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[13]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_startaddress[13]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_startaddress_reg[14] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[14]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_startaddress[14]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_startaddress_reg[15] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[15]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_startaddress[15]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_startaddress_reg[16] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[16]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_startaddress[16]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_startaddress_reg[17] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[17]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_startaddress[17]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_startaddress_reg[18] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[18]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_startaddress[18]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_startaddress_reg[19] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[19]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_startaddress[19]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_startaddress_reg[1] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[1]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_startaddress[1]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_startaddress_reg[20] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[20]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_startaddress[20]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_startaddress_reg[21] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[21]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_startaddress[21]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_startaddress_reg[22] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[22]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_startaddress[22]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_startaddress_reg[23] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[23]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_startaddress[23]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_startaddress_reg[24] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[24]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_startaddress[24]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_startaddress_reg[25] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[25]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_startaddress[25]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_startaddress_reg[26] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[26]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_startaddress[26]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_startaddress_reg[27] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[27]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_startaddress[27]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_startaddress_reg[28] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[28]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_startaddress[28]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_startaddress_reg[29] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[29]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_startaddress[29]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_startaddress_reg[2] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[2]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_startaddress[2]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_startaddress_reg[30] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[30]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_startaddress[30]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_startaddress_reg[31] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[31]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_startaddress[31]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_startaddress_reg[3] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[3]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_startaddress[3]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_startaddress_reg[4] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[4]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_startaddress[4]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_startaddress_reg[5] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[5]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_startaddress[5]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_startaddress_reg[6] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[6]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_startaddress[6]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_startaddress_reg[7] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[7]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_startaddress[7]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_startaddress_reg[8] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[8]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_startaddress[8]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s1_wr_startaddress_reg[9] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[9]),
        .G(\s1_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s1_wr_startaddress[9]));
  (* OPT_MODIFIED = "MLO" *) 
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0),
    .IS_G_INVERTED(1'b1)) 
    s2_HBM_READ_tready_reg
       (.CLR(1'b0),
        .D(s2_HBM_READ_tready_reg_i_1_n_0),
        .G(mux_select[0]),
        .GE(1'b1),
        .Q(s2_HBM_READ_tready));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h8)) 
    s2_HBM_READ_tready_reg_i_1
       (.I0(mux_select[1]),
        .I1(m_HBM_READ_tready),
        .O(s2_HBM_READ_tready_reg_i_1_n_0));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[0] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[0]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[0]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[100] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[100]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[100]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[101] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[101]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[101]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[102] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[102]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[102]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[103] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[103]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[103]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[104] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[104]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[104]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[105] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[105]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[105]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[106] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[106]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[106]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[107] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[107]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[107]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[108] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[108]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[108]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[109] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[109]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[109]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[10] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[10]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[10]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[110] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[110]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[110]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[111] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[111]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[111]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[112] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[112]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[112]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[113] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[113]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[113]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[114] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[114]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[114]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[115] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[115]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[115]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[116] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[116]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[116]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[117] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[117]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[117]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[118] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[118]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[118]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[119] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[119]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[119]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[11] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[11]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[11]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[120] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[120]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[120]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[121] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[121]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[121]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[122] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[122]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[122]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[123] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[123]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[123]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[124] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[124]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[124]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[125] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[125]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[125]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[126] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[126]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[126]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[127] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[127]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[127]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[128] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[128]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[128]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[129] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[129]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[129]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[12] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[12]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[12]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[130] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[130]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[130]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[131] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[131]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[131]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[132] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[132]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[132]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[133] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[133]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[133]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[134] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[134]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[134]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[135] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[135]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[135]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[136] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[136]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[136]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[137] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[137]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[137]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[138] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[138]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[138]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[139] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[139]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[139]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[13] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[13]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[13]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[140] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[140]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[140]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[141] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[141]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[141]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[142] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[142]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[142]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[143] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[143]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[143]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[144] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[144]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[144]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[145] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[145]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[145]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[146] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[146]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[146]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[147] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[147]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[147]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[148] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[148]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[148]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[149] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[149]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[149]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[14] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[14]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[14]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[150] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[150]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[150]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[151] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[151]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[151]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[152] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[152]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[152]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[153] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[153]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[153]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[154] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[154]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[154]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[155] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[155]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[155]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[156] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[156]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[156]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[157] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[157]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[157]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[158] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[158]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[158]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[159] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[159]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[159]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[15] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[15]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[15]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[160] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[160]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[160]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[161] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[161]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[161]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[162] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[162]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[162]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[163] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[163]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[163]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[164] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[164]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[164]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[165] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[165]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[165]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[166] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[166]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[166]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[167] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[167]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[167]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[168] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[168]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[168]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[169] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[169]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[169]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[16] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[16]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[16]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[170] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[170]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[170]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[171] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[171]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[171]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[172] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[172]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[172]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[173] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[173]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[173]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[174] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[174]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[174]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[175] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[175]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[175]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[176] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[176]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[176]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[177] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[177]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[177]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[178] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[178]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[178]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[179] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[179]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[179]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[17] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[17]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[17]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[180] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[180]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[180]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[181] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[181]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[181]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[182] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[182]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[182]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[183] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[183]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[183]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[184] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[184]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[184]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[185] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[185]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[185]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[186] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[186]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[186]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[187] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[187]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[187]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[188] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[188]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[188]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[189] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[189]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[189]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[18] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[18]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[18]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[190] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[190]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[190]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[191] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[191]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[191]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[192] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[192]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[192]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[193] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[193]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[193]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[194] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[194]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[194]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[195] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[195]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[195]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[196] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[196]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[196]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[197] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[197]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[197]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[198] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[198]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[198]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[199] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[199]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[199]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[19] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[19]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[19]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[1] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[1]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[1]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[200] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[200]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[200]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[201] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[201]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[201]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[202] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[202]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[202]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[203] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[203]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[203]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[204] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[204]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[204]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[205] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[205]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[205]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[206] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[206]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[206]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[207] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[207]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[207]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[208] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[208]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[208]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[209] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[209]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[209]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[20] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[20]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[20]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[210] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[210]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[210]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[211] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[211]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[211]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[212] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[212]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[212]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[213] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[213]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[213]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[214] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[214]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[214]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[215] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[215]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[215]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[216] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[216]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[216]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[217] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[217]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[217]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[218] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[218]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[218]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[219] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[219]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[219]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[21] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[21]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[21]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[220] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[220]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[220]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[221] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[221]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[221]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[222] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[222]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[222]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[223] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[223]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[223]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[224] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[224]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[224]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[225] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[225]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[225]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[226] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[226]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[226]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[227] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[227]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[227]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[228] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[228]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[228]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[229] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[229]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[229]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[22] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[22]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[22]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[230] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[230]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[230]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[231] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[231]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[231]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[232] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[232]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[232]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[233] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[233]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[233]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[234] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[234]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[234]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[235] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[235]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[235]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[236] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[236]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[236]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[237] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[237]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[237]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[238] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[238]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[238]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[239] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[239]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[239]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[23] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[23]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[23]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[240] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[240]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[240]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[241] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[241]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[241]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[242] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[242]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[242]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[243] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[243]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[243]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[244] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[244]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[244]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[245] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[245]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[245]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[246] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[246]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[246]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[247] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[247]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[247]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[248] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[248]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[248]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[249] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[249]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[249]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[24] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[24]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[24]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[250] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[250]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[250]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[251] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[251]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[251]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[252] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[252]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[252]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[253] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[253]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[253]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[254] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[254]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[254]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[255] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[255]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[255]));
  LUT2 #(
    .INIT(4'h2)) 
    \s2_HBM_WRITE_tdata_reg[255]_i_1 
       (.I0(mux_select[1]),
        .I1(mux_select[0]),
        .O(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[25] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[25]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[25]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[26] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[26]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[26]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[27] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[27]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[27]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[28] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[28]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[28]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[29] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[29]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[29]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[2] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[2]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[2]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[30] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[30]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[30]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[31] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[31]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[31]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[32] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[32]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[32]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[33] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[33]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[33]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[34] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[34]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[34]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[35] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[35]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[35]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[36] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[36]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[36]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[37] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[37]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[37]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[38] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[38]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[38]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[39] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[39]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[39]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[3] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[3]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[3]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[40] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[40]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[40]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[41] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[41]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[41]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[42] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[42]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[42]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[43] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[43]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[43]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[44] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[44]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[44]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[45] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[45]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[45]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[46] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[46]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[46]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[47] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[47]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[47]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[48] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[48]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[48]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[49] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[49]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[49]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[4] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[4]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[4]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[50] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[50]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[50]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[51] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[51]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[51]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[52] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[52]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[52]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[53] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[53]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[53]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[54] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[54]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[54]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[55] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[55]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[55]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[56] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[56]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[56]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[57] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[57]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[57]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[58] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[58]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[58]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[59] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[59]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[59]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[5] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[5]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[5]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[60] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[60]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[60]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[61] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[61]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[61]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[62] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[62]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[62]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[63] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[63]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[63]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[64] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[64]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[64]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[65] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[65]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[65]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[66] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[66]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[66]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[67] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[67]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[67]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[68] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[68]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[68]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[69] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[69]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[69]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[6] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[6]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[6]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[70] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[70]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[70]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[71] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[71]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[71]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[72] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[72]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[72]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[73] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[73]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[73]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[74] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[74]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[74]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[75] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[75]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[75]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[76] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[76]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[76]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[77] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[77]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[77]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[78] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[78]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[78]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[79] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[79]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[79]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[7] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[7]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[7]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[80] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[80]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[80]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[81] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[81]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[81]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[82] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[82]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[82]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[83] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[83]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[83]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[84] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[84]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[84]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[85] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[85]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[85]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[86] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[86]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[86]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[87] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[87]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[87]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[88] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[88]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[88]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[89] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[89]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[89]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[8] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[8]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[8]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[90] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[90]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[90]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[91] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[91]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[91]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[92] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[92]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[92]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[93] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[93]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[93]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[94] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[94]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[94]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[95] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[95]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[95]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[96] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[96]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[96]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[97] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[97]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[97]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[98] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[98]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[98]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[99] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[99]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[99]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_HBM_WRITE_tdata_reg[9] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[9]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tdata[9]));
  (* OPT_MODIFIED = "MLO" *) 
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0),
    .IS_G_INVERTED(1'b1)) 
    s2_HBM_WRITE_tvalid_reg
       (.CLR(1'b0),
        .D(s2_HBM_WRITE_tvalid_reg_i_1_n_0),
        .G(mux_select[0]),
        .GE(1'b1),
        .Q(s2_HBM_WRITE_tvalid));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h8)) 
    s2_HBM_WRITE_tvalid_reg_i_1
       (.I0(mux_select[1]),
        .I1(m_HBM_WRITE_tvalid),
        .O(s2_HBM_WRITE_tvalid_reg_i_1_n_0));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_blockamount_reg[0] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[0]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_blockamount[0]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_blockamount_reg[10] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[10]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_blockamount[10]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_blockamount_reg[11] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[11]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_blockamount[11]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_blockamount_reg[12] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[12]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_blockamount[12]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_blockamount_reg[13] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[13]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_blockamount[13]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_blockamount_reg[14] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[14]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_blockamount[14]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_blockamount_reg[15] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[15]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_blockamount[15]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_blockamount_reg[16] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[16]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_blockamount[16]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_blockamount_reg[17] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[17]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_blockamount[17]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_blockamount_reg[18] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[18]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_blockamount[18]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_blockamount_reg[19] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[19]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_blockamount[19]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_blockamount_reg[1] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[1]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_blockamount[1]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_blockamount_reg[20] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[20]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_blockamount[20]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_blockamount_reg[21] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[21]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_blockamount[21]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_blockamount_reg[22] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[22]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_blockamount[22]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_blockamount_reg[23] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[23]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_blockamount[23]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_blockamount_reg[24] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[24]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_blockamount[24]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_blockamount_reg[25] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[25]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_blockamount[25]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_blockamount_reg[26] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[26]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_blockamount[26]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_blockamount_reg[27] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[27]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_blockamount[27]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_blockamount_reg[28] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[28]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_blockamount[28]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_blockamount_reg[29] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[29]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_blockamount[29]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_blockamount_reg[2] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[2]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_blockamount[2]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_blockamount_reg[30] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[30]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_blockamount[30]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_blockamount_reg[31] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[31]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_blockamount[31]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_blockamount_reg[3] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[3]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_blockamount[3]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_blockamount_reg[4] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[4]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_blockamount[4]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_blockamount_reg[5] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[5]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_blockamount[5]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_blockamount_reg[6] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[6]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_blockamount[6]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_blockamount_reg[7] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[7]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_blockamount[7]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_blockamount_reg[8] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[8]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_blockamount[8]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_blockamount_reg[9] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[9]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_blockamount[9]));
  (* OPT_MODIFIED = "MLO" *) 
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0),
    .IS_G_INVERTED(1'b1)) 
    s2_rd_run_reg
       (.CLR(1'b0),
        .D(s2_rd_run_reg_i_1_n_0),
        .G(mux_select[0]),
        .GE(1'b1),
        .Q(s2_rd_run));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h8)) 
    s2_rd_run_reg_i_1
       (.I0(mux_select[1]),
        .I1(m_rd_run),
        .O(s2_rd_run_reg_i_1_n_0));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_startaddress_reg[0] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[0]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_startaddress[0]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_startaddress_reg[10] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[10]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_startaddress[10]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_startaddress_reg[11] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[11]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_startaddress[11]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_startaddress_reg[12] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[12]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_startaddress[12]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_startaddress_reg[13] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[13]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_startaddress[13]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_startaddress_reg[14] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[14]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_startaddress[14]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_startaddress_reg[15] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[15]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_startaddress[15]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_startaddress_reg[16] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[16]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_startaddress[16]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_startaddress_reg[17] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[17]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_startaddress[17]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_startaddress_reg[18] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[18]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_startaddress[18]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_startaddress_reg[19] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[19]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_startaddress[19]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_startaddress_reg[1] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[1]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_startaddress[1]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_startaddress_reg[20] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[20]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_startaddress[20]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_startaddress_reg[21] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[21]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_startaddress[21]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_startaddress_reg[22] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[22]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_startaddress[22]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_startaddress_reg[23] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[23]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_startaddress[23]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_startaddress_reg[24] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[24]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_startaddress[24]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_startaddress_reg[25] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[25]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_startaddress[25]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_startaddress_reg[26] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[26]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_startaddress[26]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_startaddress_reg[27] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[27]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_startaddress[27]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_startaddress_reg[28] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[28]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_startaddress[28]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_startaddress_reg[29] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[29]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_startaddress[29]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_startaddress_reg[2] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[2]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_startaddress[2]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_startaddress_reg[30] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[30]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_startaddress[30]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_startaddress_reg[31] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[31]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_startaddress[31]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_startaddress_reg[3] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[3]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_startaddress[3]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_startaddress_reg[4] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[4]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_startaddress[4]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_startaddress_reg[5] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[5]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_startaddress[5]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_startaddress_reg[6] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[6]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_startaddress[6]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_startaddress_reg[7] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[7]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_startaddress[7]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_startaddress_reg[8] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[8]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_startaddress[8]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_rd_startaddress_reg[9] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[9]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_rd_startaddress[9]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_blockamount_reg[0] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[0]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_blockamount[0]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_blockamount_reg[10] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[10]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_blockamount[10]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_blockamount_reg[11] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[11]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_blockamount[11]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_blockamount_reg[12] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[12]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_blockamount[12]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_blockamount_reg[13] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[13]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_blockamount[13]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_blockamount_reg[14] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[14]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_blockamount[14]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_blockamount_reg[15] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[15]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_blockamount[15]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_blockamount_reg[16] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[16]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_blockamount[16]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_blockamount_reg[17] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[17]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_blockamount[17]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_blockamount_reg[18] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[18]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_blockamount[18]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_blockamount_reg[19] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[19]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_blockamount[19]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_blockamount_reg[1] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[1]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_blockamount[1]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_blockamount_reg[20] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[20]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_blockamount[20]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_blockamount_reg[21] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[21]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_blockamount[21]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_blockamount_reg[22] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[22]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_blockamount[22]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_blockamount_reg[23] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[23]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_blockamount[23]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_blockamount_reg[24] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[24]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_blockamount[24]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_blockamount_reg[25] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[25]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_blockamount[25]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_blockamount_reg[26] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[26]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_blockamount[26]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_blockamount_reg[27] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[27]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_blockamount[27]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_blockamount_reg[28] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[28]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_blockamount[28]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_blockamount_reg[29] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[29]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_blockamount[29]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_blockamount_reg[2] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[2]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_blockamount[2]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_blockamount_reg[30] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[30]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_blockamount[30]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_blockamount_reg[31] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[31]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_blockamount[31]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_blockamount_reg[3] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[3]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_blockamount[3]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_blockamount_reg[4] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[4]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_blockamount[4]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_blockamount_reg[5] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[5]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_blockamount[5]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_blockamount_reg[6] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[6]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_blockamount[6]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_blockamount_reg[7] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[7]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_blockamount[7]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_blockamount_reg[8] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[8]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_blockamount[8]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_blockamount_reg[9] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[9]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_blockamount[9]));
  (* OPT_MODIFIED = "MLO" *) 
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0),
    .IS_G_INVERTED(1'b1)) 
    s2_wr_run_reg
       (.CLR(1'b0),
        .D(s2_wr_run_reg_i_1_n_0),
        .G(mux_select[0]),
        .GE(1'b1),
        .Q(s2_wr_run));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h8)) 
    s2_wr_run_reg_i_1
       (.I0(mux_select[1]),
        .I1(m_wr_run),
        .O(s2_wr_run_reg_i_1_n_0));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_startaddress_reg[0] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[0]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_startaddress[0]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_startaddress_reg[10] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[10]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_startaddress[10]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_startaddress_reg[11] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[11]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_startaddress[11]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_startaddress_reg[12] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[12]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_startaddress[12]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_startaddress_reg[13] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[13]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_startaddress[13]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_startaddress_reg[14] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[14]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_startaddress[14]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_startaddress_reg[15] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[15]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_startaddress[15]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_startaddress_reg[16] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[16]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_startaddress[16]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_startaddress_reg[17] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[17]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_startaddress[17]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_startaddress_reg[18] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[18]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_startaddress[18]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_startaddress_reg[19] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[19]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_startaddress[19]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_startaddress_reg[1] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[1]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_startaddress[1]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_startaddress_reg[20] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[20]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_startaddress[20]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_startaddress_reg[21] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[21]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_startaddress[21]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_startaddress_reg[22] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[22]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_startaddress[22]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_startaddress_reg[23] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[23]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_startaddress[23]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_startaddress_reg[24] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[24]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_startaddress[24]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_startaddress_reg[25] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[25]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_startaddress[25]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_startaddress_reg[26] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[26]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_startaddress[26]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_startaddress_reg[27] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[27]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_startaddress[27]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_startaddress_reg[28] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[28]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_startaddress[28]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_startaddress_reg[29] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[29]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_startaddress[29]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_startaddress_reg[2] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[2]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_startaddress[2]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_startaddress_reg[30] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[30]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_startaddress[30]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_startaddress_reg[31] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[31]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_startaddress[31]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_startaddress_reg[3] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[3]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_startaddress[3]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_startaddress_reg[4] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[4]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_startaddress[4]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_startaddress_reg[5] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[5]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_startaddress[5]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_startaddress_reg[6] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[6]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_startaddress[6]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_startaddress_reg[7] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[7]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_startaddress[7]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_startaddress_reg[8] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[8]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_startaddress[8]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s2_wr_startaddress_reg[9] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[9]),
        .G(\s2_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s2_wr_startaddress[9]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    s3_HBM_READ_tready_reg
       (.CLR(1'b0),
        .D(s2_HBM_READ_tready_reg_i_1_n_0),
        .G(s3_HBM_READ_tready_reg_i_1_n_0),
        .GE(1'b1),
        .Q(s3_HBM_READ_tready));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'h9)) 
    s3_HBM_READ_tready_reg_i_1
       (.I0(mux_select[0]),
        .I1(mux_select[1]),
        .O(s3_HBM_READ_tready_reg_i_1_n_0));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[0] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[0]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[0]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[100] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[100]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[100]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[101] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[101]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[101]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[102] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[102]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[102]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[103] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[103]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[103]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[104] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[104]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[104]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[105] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[105]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[105]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[106] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[106]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[106]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[107] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[107]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[107]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[108] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[108]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[108]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[109] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[109]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[109]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[10] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[10]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[10]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[110] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[110]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[110]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[111] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[111]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[111]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[112] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[112]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[112]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[113] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[113]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[113]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[114] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[114]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[114]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[115] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[115]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[115]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[116] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[116]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[116]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[117] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[117]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[117]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[118] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[118]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[118]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[119] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[119]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[119]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[11] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[11]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[11]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[120] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[120]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[120]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[121] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[121]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[121]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[122] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[122]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[122]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[123] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[123]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[123]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[124] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[124]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[124]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[125] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[125]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[125]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[126] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[126]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[126]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[127] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[127]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[127]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[128] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[128]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[128]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[129] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[129]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[129]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[12] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[12]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[12]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[130] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[130]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[130]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[131] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[131]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[131]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[132] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[132]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[132]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[133] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[133]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[133]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[134] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[134]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[134]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[135] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[135]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[135]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[136] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[136]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[136]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[137] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[137]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[137]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[138] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[138]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[138]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[139] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[139]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[139]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[13] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[13]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[13]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[140] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[140]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[140]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[141] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[141]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[141]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[142] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[142]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[142]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[143] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[143]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[143]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[144] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[144]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[144]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[145] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[145]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[145]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[146] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[146]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[146]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[147] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[147]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[147]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[148] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[148]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[148]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[149] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[149]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[149]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[14] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[14]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[14]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[150] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[150]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[150]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[151] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[151]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[151]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[152] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[152]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[152]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[153] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[153]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[153]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[154] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[154]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[154]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[155] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[155]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[155]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[156] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[156]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[156]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[157] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[157]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[157]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[158] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[158]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[158]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[159] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[159]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[159]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[15] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[15]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[15]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[160] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[160]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[160]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[161] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[161]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[161]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[162] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[162]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[162]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[163] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[163]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[163]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[164] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[164]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[164]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[165] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[165]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[165]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[166] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[166]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[166]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[167] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[167]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[167]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[168] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[168]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[168]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[169] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[169]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[169]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[16] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[16]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[16]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[170] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[170]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[170]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[171] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[171]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[171]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[172] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[172]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[172]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[173] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[173]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[173]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[174] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[174]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[174]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[175] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[175]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[175]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[176] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[176]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[176]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[177] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[177]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[177]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[178] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[178]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[178]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[179] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[179]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[179]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[17] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[17]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[17]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[180] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[180]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[180]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[181] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[181]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[181]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[182] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[182]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[182]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[183] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[183]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[183]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[184] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[184]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[184]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[185] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[185]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[185]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[186] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[186]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[186]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[187] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[187]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[187]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[188] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[188]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[188]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[189] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[189]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[189]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[18] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[18]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[18]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[190] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[190]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[190]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[191] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[191]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[191]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[192] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[192]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[192]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[193] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[193]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[193]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[194] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[194]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[194]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[195] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[195]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[195]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[196] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[196]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[196]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[197] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[197]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[197]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[198] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[198]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[198]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[199] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[199]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[199]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[19] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[19]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[19]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[1] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[1]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[1]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[200] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[200]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[200]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[201] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[201]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[201]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[202] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[202]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[202]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[203] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[203]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[203]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[204] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[204]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[204]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[205] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[205]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[205]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[206] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[206]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[206]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[207] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[207]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[207]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[208] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[208]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[208]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[209] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[209]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[209]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[20] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[20]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[20]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[210] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[210]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[210]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[211] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[211]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[211]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[212] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[212]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[212]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[213] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[213]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[213]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[214] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[214]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[214]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[215] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[215]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[215]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[216] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[216]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[216]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[217] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[217]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[217]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[218] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[218]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[218]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[219] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[219]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[219]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[21] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[21]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[21]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[220] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[220]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[220]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[221] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[221]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[221]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[222] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[222]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[222]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[223] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[223]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[223]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[224] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[224]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[224]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[225] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[225]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[225]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[226] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[226]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[226]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[227] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[227]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[227]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[228] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[228]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[228]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[229] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[229]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[229]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[22] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[22]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[22]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[230] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[230]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[230]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[231] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[231]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[231]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[232] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[232]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[232]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[233] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[233]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[233]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[234] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[234]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[234]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[235] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[235]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[235]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[236] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[236]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[236]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[237] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[237]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[237]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[238] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[238]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[238]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[239] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[239]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[239]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[23] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[23]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[23]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[240] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[240]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[240]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[241] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[241]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[241]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[242] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[242]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[242]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[243] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[243]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[243]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[244] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[244]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[244]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[245] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[245]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[245]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[246] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[246]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[246]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[247] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[247]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[247]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[248] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[248]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[248]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[249] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[249]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[249]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[24] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[24]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[24]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[250] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[250]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[250]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[251] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[251]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[251]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[252] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[252]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[252]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[253] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[253]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[253]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[254] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[254]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[254]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[255] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[255]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[255]));
  LUT2 #(
    .INIT(4'h8)) 
    \s3_HBM_WRITE_tdata_reg[255]_i_1 
       (.I0(mux_select[0]),
        .I1(mux_select[1]),
        .O(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[25] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[25]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[25]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[26] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[26]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[26]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[27] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[27]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[27]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[28] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[28]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[28]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[29] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[29]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[29]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[2] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[2]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[2]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[30] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[30]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[30]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[31] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[31]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[31]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[32] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[32]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[32]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[33] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[33]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[33]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[34] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[34]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[34]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[35] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[35]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[35]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[36] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[36]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[36]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[37] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[37]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[37]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[38] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[38]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[38]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[39] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[39]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[39]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[3] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[3]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[3]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[40] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[40]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[40]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[41] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[41]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[41]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[42] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[42]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[42]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[43] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[43]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[43]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[44] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[44]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[44]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[45] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[45]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[45]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[46] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[46]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[46]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[47] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[47]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[47]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[48] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[48]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[48]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[49] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[49]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[49]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[4] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[4]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[4]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[50] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[50]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[50]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[51] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[51]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[51]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[52] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[52]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[52]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[53] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[53]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[53]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[54] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[54]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[54]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[55] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[55]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[55]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[56] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[56]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[56]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[57] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[57]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[57]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[58] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[58]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[58]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[59] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[59]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[59]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[5] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[5]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[5]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[60] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[60]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[60]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[61] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[61]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[61]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[62] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[62]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[62]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[63] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[63]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[63]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[64] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[64]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[64]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[65] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[65]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[65]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[66] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[66]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[66]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[67] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[67]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[67]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[68] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[68]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[68]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[69] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[69]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[69]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[6] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[6]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[6]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[70] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[70]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[70]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[71] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[71]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[71]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[72] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[72]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[72]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[73] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[73]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[73]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[74] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[74]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[74]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[75] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[75]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[75]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[76] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[76]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[76]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[77] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[77]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[77]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[78] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[78]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[78]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[79] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[79]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[79]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[7] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[7]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[7]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[80] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[80]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[80]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[81] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[81]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[81]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[82] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[82]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[82]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[83] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[83]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[83]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[84] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[84]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[84]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[85] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[85]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[85]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[86] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[86]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[86]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[87] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[87]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[87]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[88] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[88]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[88]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[89] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[89]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[89]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[8] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[8]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[8]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[90] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[90]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[90]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[91] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[91]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[91]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[92] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[92]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[92]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[93] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[93]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[93]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[94] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[94]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[94]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[95] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[95]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[95]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[96] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[96]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[96]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[97] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[97]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[97]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[98] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[98]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[98]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[99] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[99]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[99]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_HBM_WRITE_tdata_reg[9] 
       (.CLR(1'b0),
        .D(m_HBM_WRITE_tdata[9]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tdata[9]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    s3_HBM_WRITE_tvalid_reg
       (.CLR(1'b0),
        .D(s2_HBM_WRITE_tvalid_reg_i_1_n_0),
        .G(s3_HBM_READ_tready_reg_i_1_n_0),
        .GE(1'b1),
        .Q(s3_HBM_WRITE_tvalid));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_blockamount_reg[0] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[0]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_blockamount[0]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_blockamount_reg[10] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[10]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_blockamount[10]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_blockamount_reg[11] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[11]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_blockamount[11]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_blockamount_reg[12] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[12]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_blockamount[12]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_blockamount_reg[13] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[13]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_blockamount[13]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_blockamount_reg[14] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[14]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_blockamount[14]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_blockamount_reg[15] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[15]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_blockamount[15]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_blockamount_reg[16] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[16]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_blockamount[16]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_blockamount_reg[17] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[17]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_blockamount[17]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_blockamount_reg[18] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[18]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_blockamount[18]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_blockamount_reg[19] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[19]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_blockamount[19]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_blockamount_reg[1] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[1]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_blockamount[1]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_blockamount_reg[20] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[20]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_blockamount[20]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_blockamount_reg[21] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[21]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_blockamount[21]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_blockamount_reg[22] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[22]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_blockamount[22]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_blockamount_reg[23] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[23]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_blockamount[23]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_blockamount_reg[24] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[24]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_blockamount[24]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_blockamount_reg[25] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[25]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_blockamount[25]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_blockamount_reg[26] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[26]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_blockamount[26]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_blockamount_reg[27] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[27]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_blockamount[27]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_blockamount_reg[28] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[28]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_blockamount[28]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_blockamount_reg[29] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[29]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_blockamount[29]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_blockamount_reg[2] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[2]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_blockamount[2]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_blockamount_reg[30] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[30]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_blockamount[30]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_blockamount_reg[31] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[31]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_blockamount[31]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_blockamount_reg[3] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[3]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_blockamount[3]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_blockamount_reg[4] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[4]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_blockamount[4]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_blockamount_reg[5] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[5]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_blockamount[5]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_blockamount_reg[6] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[6]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_blockamount[6]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_blockamount_reg[7] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[7]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_blockamount[7]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_blockamount_reg[8] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[8]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_blockamount[8]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_blockamount_reg[9] 
       (.CLR(1'b0),
        .D(m_rd_blockamount[9]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_blockamount[9]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    s3_rd_run_reg
       (.CLR(1'b0),
        .D(s2_rd_run_reg_i_1_n_0),
        .G(s3_HBM_READ_tready_reg_i_1_n_0),
        .GE(1'b1),
        .Q(s3_rd_run));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_startaddress_reg[0] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[0]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_startaddress[0]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_startaddress_reg[10] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[10]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_startaddress[10]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_startaddress_reg[11] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[11]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_startaddress[11]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_startaddress_reg[12] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[12]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_startaddress[12]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_startaddress_reg[13] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[13]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_startaddress[13]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_startaddress_reg[14] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[14]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_startaddress[14]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_startaddress_reg[15] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[15]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_startaddress[15]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_startaddress_reg[16] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[16]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_startaddress[16]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_startaddress_reg[17] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[17]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_startaddress[17]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_startaddress_reg[18] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[18]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_startaddress[18]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_startaddress_reg[19] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[19]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_startaddress[19]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_startaddress_reg[1] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[1]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_startaddress[1]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_startaddress_reg[20] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[20]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_startaddress[20]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_startaddress_reg[21] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[21]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_startaddress[21]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_startaddress_reg[22] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[22]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_startaddress[22]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_startaddress_reg[23] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[23]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_startaddress[23]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_startaddress_reg[24] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[24]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_startaddress[24]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_startaddress_reg[25] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[25]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_startaddress[25]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_startaddress_reg[26] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[26]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_startaddress[26]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_startaddress_reg[27] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[27]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_startaddress[27]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_startaddress_reg[28] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[28]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_startaddress[28]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_startaddress_reg[29] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[29]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_startaddress[29]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_startaddress_reg[2] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[2]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_startaddress[2]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_startaddress_reg[30] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[30]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_startaddress[30]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_startaddress_reg[31] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[31]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_startaddress[31]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_startaddress_reg[3] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[3]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_startaddress[3]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_startaddress_reg[4] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[4]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_startaddress[4]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_startaddress_reg[5] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[5]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_startaddress[5]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_startaddress_reg[6] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[6]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_startaddress[6]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_startaddress_reg[7] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[7]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_startaddress[7]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_startaddress_reg[8] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[8]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_startaddress[8]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_rd_startaddress_reg[9] 
       (.CLR(1'b0),
        .D(m_rd_startaddress[9]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_rd_startaddress[9]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_blockamount_reg[0] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[0]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_blockamount[0]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_blockamount_reg[10] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[10]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_blockamount[10]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_blockamount_reg[11] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[11]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_blockamount[11]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_blockamount_reg[12] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[12]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_blockamount[12]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_blockamount_reg[13] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[13]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_blockamount[13]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_blockamount_reg[14] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[14]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_blockamount[14]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_blockamount_reg[15] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[15]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_blockamount[15]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_blockamount_reg[16] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[16]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_blockamount[16]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_blockamount_reg[17] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[17]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_blockamount[17]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_blockamount_reg[18] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[18]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_blockamount[18]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_blockamount_reg[19] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[19]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_blockamount[19]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_blockamount_reg[1] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[1]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_blockamount[1]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_blockamount_reg[20] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[20]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_blockamount[20]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_blockamount_reg[21] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[21]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_blockamount[21]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_blockamount_reg[22] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[22]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_blockamount[22]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_blockamount_reg[23] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[23]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_blockamount[23]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_blockamount_reg[24] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[24]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_blockamount[24]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_blockamount_reg[25] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[25]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_blockamount[25]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_blockamount_reg[26] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[26]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_blockamount[26]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_blockamount_reg[27] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[27]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_blockamount[27]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_blockamount_reg[28] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[28]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_blockamount[28]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_blockamount_reg[29] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[29]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_blockamount[29]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_blockamount_reg[2] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[2]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_blockamount[2]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_blockamount_reg[30] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[30]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_blockamount[30]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_blockamount_reg[31] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[31]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_blockamount[31]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_blockamount_reg[3] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[3]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_blockamount[3]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_blockamount_reg[4] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[4]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_blockamount[4]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_blockamount_reg[5] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[5]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_blockamount[5]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_blockamount_reg[6] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[6]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_blockamount[6]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_blockamount_reg[7] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[7]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_blockamount[7]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_blockamount_reg[8] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[8]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_blockamount[8]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_blockamount_reg[9] 
       (.CLR(1'b0),
        .D(m_wr_blockamount[9]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_blockamount[9]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    s3_wr_run_reg
       (.CLR(1'b0),
        .D(s2_wr_run_reg_i_1_n_0),
        .G(s3_HBM_READ_tready_reg_i_1_n_0),
        .GE(1'b1),
        .Q(s3_wr_run));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_startaddress_reg[0] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[0]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_startaddress[0]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_startaddress_reg[10] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[10]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_startaddress[10]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_startaddress_reg[11] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[11]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_startaddress[11]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_startaddress_reg[12] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[12]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_startaddress[12]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_startaddress_reg[13] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[13]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_startaddress[13]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_startaddress_reg[14] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[14]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_startaddress[14]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_startaddress_reg[15] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[15]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_startaddress[15]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_startaddress_reg[16] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[16]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_startaddress[16]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_startaddress_reg[17] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[17]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_startaddress[17]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_startaddress_reg[18] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[18]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_startaddress[18]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_startaddress_reg[19] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[19]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_startaddress[19]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_startaddress_reg[1] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[1]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_startaddress[1]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_startaddress_reg[20] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[20]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_startaddress[20]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_startaddress_reg[21] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[21]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_startaddress[21]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_startaddress_reg[22] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[22]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_startaddress[22]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_startaddress_reg[23] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[23]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_startaddress[23]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_startaddress_reg[24] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[24]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_startaddress[24]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_startaddress_reg[25] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[25]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_startaddress[25]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_startaddress_reg[26] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[26]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_startaddress[26]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_startaddress_reg[27] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[27]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_startaddress[27]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_startaddress_reg[28] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[28]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_startaddress[28]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_startaddress_reg[29] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[29]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_startaddress[29]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_startaddress_reg[2] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[2]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_startaddress[2]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_startaddress_reg[30] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[30]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_startaddress[30]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_startaddress_reg[31] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[31]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_startaddress[31]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_startaddress_reg[3] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[3]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_startaddress[3]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_startaddress_reg[4] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[4]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_startaddress[4]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_startaddress_reg[5] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[5]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_startaddress[5]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_startaddress_reg[6] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[6]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_startaddress[6]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_startaddress_reg[7] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[7]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_startaddress[7]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_startaddress_reg[8] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[8]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_startaddress[8]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:GE GND:CLR" *) 
  LDCE #(
    .INIT(1'b0)) 
    \s3_wr_startaddress_reg[9] 
       (.CLR(1'b0),
        .D(m_wr_startaddress[9]),
        .G(\s3_HBM_WRITE_tdata_reg[255]_i_1_n_0 ),
        .GE(1'b1),
        .Q(s3_wr_startaddress[9]));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
