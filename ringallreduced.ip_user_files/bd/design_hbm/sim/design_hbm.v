//Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
//Date        : Tue Dec  6 16:10:09 2022
//Host        : dell-Vostro-3910-China-HDD-Protection running 64-bit Ubuntu 22.04.1 LTS
//Command     : generate_target design_hbm.bd
//Design      : design_hbm
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CORE_GENERATION_INFO = "design_hbm,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=design_hbm,x_ipVersion=1.00.a,x_ipLanguage=VERILOG,numBlks=24,numReposBlks=24,numNonXlnxBlks=0,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=12,numPkgbdBlks=0,bdsource=USER,da_clkrst_cnt=4,synth_mode=OOC_per_IP}" *) (* HW_HANDOFF = "design_hbm.hwdef" *) 
module design_hbm
   (HBM_READ_m_0_tdata,
    HBM_READ_m_0_tkeep,
    HBM_READ_m_0_tlast,
    HBM_READ_m_0_tready,
    HBM_READ_m_0_tvalid,
    HBM_READ_m_1_tdata,
    HBM_READ_m_1_tkeep,
    HBM_READ_m_1_tlast,
    HBM_READ_m_1_tready,
    HBM_READ_m_1_tvalid,
    HBM_READ_m_2_tdata,
    HBM_READ_m_2_tkeep,
    HBM_READ_m_2_tlast,
    HBM_READ_m_2_tready,
    HBM_READ_m_2_tvalid,
    HBM_STATUS,
    HBM_WRITE_m_0_tdata,
    HBM_WRITE_m_0_tready,
    HBM_WRITE_m_0_tvalid,
    HBM_WRITE_m_1_tdata,
    HBM_WRITE_m_1_tready,
    HBM_WRITE_m_1_tvalid,
    HBM_WRITE_m_2_tdata,
    HBM_WRITE_m_2_tready,
    HBM_WRITE_m_2_tvalid,
    clk_100MHz,
    clk_312MHz,
    m_busy_0,
    m_busy_1,
    m_busy_2,
    m_ctrl_ports_0_rd_blockamount,
    m_ctrl_ports_0_rd_run,
    m_ctrl_ports_0_rd_startaddress,
    m_ctrl_ports_0_wr_blockamount,
    m_ctrl_ports_0_wr_run,
    m_ctrl_ports_0_wr_startaddress,
    m_ctrl_ports_1_rd_blockamount,
    m_ctrl_ports_1_rd_run,
    m_ctrl_ports_1_rd_startaddress,
    m_ctrl_ports_1_wr_blockamount,
    m_ctrl_ports_1_wr_run,
    m_ctrl_ports_1_wr_startaddress,
    m_ctrl_ports_2_rd_blockamount,
    m_ctrl_ports_2_rd_run,
    m_ctrl_ports_2_rd_startaddress,
    m_ctrl_ports_2_wr_blockamount,
    m_ctrl_ports_2_wr_run,
    m_ctrl_ports_2_wr_startaddress,
    mux_select_0,
    mux_select_1,
    mux_select_2,
    reset);
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_READ_m_0 TDATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME HBM_READ_m_0, CLK_DOMAIN design_hbm_clk_312MHz, FREQ_HZ 100000000, HAS_TKEEP 1, HAS_TLAST 1, HAS_TREADY 1, HAS_TSTRB 0, INSERT_VIP 0, LAYERED_METADATA undef, PHASE 0.0, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0" *) output [255:0]HBM_READ_m_0_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_READ_m_0 TKEEP" *) output [31:0]HBM_READ_m_0_tkeep;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_READ_m_0 TLAST" *) output HBM_READ_m_0_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_READ_m_0 TREADY" *) input HBM_READ_m_0_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_READ_m_0 TVALID" *) output HBM_READ_m_0_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_READ_m_1 TDATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME HBM_READ_m_1, CLK_DOMAIN design_hbm_clk_312MHz, FREQ_HZ 100000000, HAS_TKEEP 1, HAS_TLAST 1, HAS_TREADY 1, HAS_TSTRB 0, INSERT_VIP 0, LAYERED_METADATA undef, PHASE 0.0, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0" *) output [255:0]HBM_READ_m_1_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_READ_m_1 TKEEP" *) output [31:0]HBM_READ_m_1_tkeep;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_READ_m_1 TLAST" *) output HBM_READ_m_1_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_READ_m_1 TREADY" *) input HBM_READ_m_1_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_READ_m_1 TVALID" *) output HBM_READ_m_1_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_READ_m_2 TDATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME HBM_READ_m_2, CLK_DOMAIN design_hbm_clk_312MHz, FREQ_HZ 100000000, HAS_TKEEP 1, HAS_TLAST 1, HAS_TREADY 1, HAS_TSTRB 0, INSERT_VIP 0, LAYERED_METADATA undef, PHASE 0.0, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0" *) output [255:0]HBM_READ_m_2_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_READ_m_2 TKEEP" *) output [31:0]HBM_READ_m_2_tkeep;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_READ_m_2 TLAST" *) output HBM_READ_m_2_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_READ_m_2 TREADY" *) input HBM_READ_m_2_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_READ_m_2 TVALID" *) output HBM_READ_m_2_tvalid;
  output [0:0]HBM_STATUS;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_WRITE_m_0 TDATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME HBM_WRITE_m_0, CLK_DOMAIN design_hbm_clk_312MHz, FREQ_HZ 100000000, HAS_TKEEP 0, HAS_TLAST 0, HAS_TREADY 1, HAS_TSTRB 0, INSERT_VIP 0, LAYERED_METADATA undef, PHASE 0.0, TDATA_NUM_BYTES 8, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0" *) input [63:0]HBM_WRITE_m_0_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_WRITE_m_0 TREADY" *) output HBM_WRITE_m_0_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_WRITE_m_0 TVALID" *) input HBM_WRITE_m_0_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_WRITE_m_1 TDATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME HBM_WRITE_m_1, CLK_DOMAIN design_hbm_clk_312MHz, FREQ_HZ 100000000, HAS_TKEEP 0, HAS_TLAST 0, HAS_TREADY 1, HAS_TSTRB 0, INSERT_VIP 0, LAYERED_METADATA undef, PHASE 0.0, TDATA_NUM_BYTES 8, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0" *) input [63:0]HBM_WRITE_m_1_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_WRITE_m_1 TREADY" *) output HBM_WRITE_m_1_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_WRITE_m_1 TVALID" *) input HBM_WRITE_m_1_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_WRITE_m_2 TDATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME HBM_WRITE_m_2, CLK_DOMAIN design_hbm_clk_312MHz, FREQ_HZ 100000000, HAS_TKEEP 0, HAS_TLAST 0, HAS_TREADY 1, HAS_TSTRB 0, INSERT_VIP 0, LAYERED_METADATA undef, PHASE 0.0, TDATA_NUM_BYTES 8, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0" *) input [63:0]HBM_WRITE_m_2_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_WRITE_m_2 TREADY" *) output HBM_WRITE_m_2_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_WRITE_m_2 TVALID" *) input HBM_WRITE_m_2_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 CLK.CLK_100MHZ CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME CLK.CLK_100MHZ, CLK_DOMAIN design_hbm_clk_100MHz, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, INSERT_VIP 0, PHASE 0.0" *) input clk_100MHz;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 CLK.CLK_312MHZ CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME CLK.CLK_312MHZ, ASSOCIATED_BUSIF HBM_WRITE_m_0:HBM_READ_m_0:HBM_WRITE_m_1:HBM_READ_m_1:HBM_WRITE_m_2:HBM_READ_m_2, CLK_DOMAIN design_hbm_clk_312MHz, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, INSERT_VIP 0, PHASE 0.0" *) input clk_312MHz;
  output m_busy_0;
  output m_busy_1;
  output m_busy_2;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 m_ctrl_ports_0 rd_blockamount" *) input [31:0]m_ctrl_ports_0_rd_blockamount;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 m_ctrl_ports_0 rd_run" *) input m_ctrl_ports_0_rd_run;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 m_ctrl_ports_0 rd_startaddress" *) input [31:0]m_ctrl_ports_0_rd_startaddress;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 m_ctrl_ports_0 wr_blockamount" *) input [31:0]m_ctrl_ports_0_wr_blockamount;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 m_ctrl_ports_0 wr_run" *) input m_ctrl_ports_0_wr_run;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 m_ctrl_ports_0 wr_startaddress" *) input [31:0]m_ctrl_ports_0_wr_startaddress;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 m_ctrl_ports_1 rd_blockamount" *) input [31:0]m_ctrl_ports_1_rd_blockamount;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 m_ctrl_ports_1 rd_run" *) input m_ctrl_ports_1_rd_run;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 m_ctrl_ports_1 rd_startaddress" *) input [31:0]m_ctrl_ports_1_rd_startaddress;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 m_ctrl_ports_1 wr_blockamount" *) input [31:0]m_ctrl_ports_1_wr_blockamount;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 m_ctrl_ports_1 wr_run" *) input m_ctrl_ports_1_wr_run;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 m_ctrl_ports_1 wr_startaddress" *) input [31:0]m_ctrl_ports_1_wr_startaddress;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 m_ctrl_ports_2 rd_blockamount" *) input [31:0]m_ctrl_ports_2_rd_blockamount;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 m_ctrl_ports_2 rd_run" *) input m_ctrl_ports_2_rd_run;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 m_ctrl_ports_2 rd_startaddress" *) input [31:0]m_ctrl_ports_2_rd_startaddress;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 m_ctrl_ports_2 wr_blockamount" *) input [31:0]m_ctrl_ports_2_wr_blockamount;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 m_ctrl_ports_2 wr_run" *) input m_ctrl_ports_2_wr_run;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 m_ctrl_ports_2 wr_startaddress" *) input [31:0]m_ctrl_ports_2_wr_startaddress;
  input [1:0]mux_select_0;
  input [1:0]mux_select_1;
  input [1:0]mux_select_2;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 RST.RESET RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME RST.RESET, INSERT_VIP 0, POLARITY ACTIVE_HIGH" *) input reset;

  wire [63:0]HBM_WRITE_m_0_1_TDATA;
  wire HBM_WRITE_m_0_1_TREADY;
  wire HBM_WRITE_m_0_1_TVALID;
  wire [63:0]HBM_WRITE_m_1_1_TDATA;
  wire HBM_WRITE_m_1_1_TREADY;
  wire HBM_WRITE_m_1_1_TVALID;
  wire [63:0]HBM_WRITE_m_2_1_TDATA;
  wire HBM_WRITE_m_2_1_TREADY;
  wire HBM_WRITE_m_2_1_TVALID;
  wire [0:0]Net;
  wire [0:0]Net1;
  wire Net2;
  wire [0:0]Net3;
  wire [63:0]axis_register_slice_0_M_AXIS_TDATA;
  wire axis_register_slice_0_M_AXIS_TREADY;
  wire axis_register_slice_0_M_AXIS_TVALID;
  wire [255:0]axis_register_slice_1_M_AXIS_TDATA;
  wire [31:0]axis_register_slice_1_M_AXIS_TKEEP;
  wire axis_register_slice_1_M_AXIS_TLAST;
  wire axis_register_slice_1_M_AXIS_TREADY;
  wire axis_register_slice_1_M_AXIS_TVALID;
  wire [63:0]axis_register_slice_2_M_AXIS_TDATA;
  wire axis_register_slice_2_M_AXIS_TREADY;
  wire axis_register_slice_2_M_AXIS_TVALID;
  wire [255:0]axis_register_slice_3_M_AXIS_TDATA;
  wire [31:0]axis_register_slice_3_M_AXIS_TKEEP;
  wire axis_register_slice_3_M_AXIS_TLAST;
  wire axis_register_slice_3_M_AXIS_TREADY;
  wire axis_register_slice_3_M_AXIS_TVALID;
  wire [63:0]axis_register_slice_4_M_AXIS_TDATA;
  wire axis_register_slice_4_M_AXIS_TREADY;
  wire axis_register_slice_4_M_AXIS_TVALID;
  wire [255:0]axis_register_slice_5_M_AXIS_TDATA;
  wire [31:0]axis_register_slice_5_M_AXIS_TKEEP;
  wire axis_register_slice_5_M_AXIS_TLAST;
  wire axis_register_slice_5_M_AXIS_TREADY;
  wire axis_register_slice_5_M_AXIS_TVALID;
  wire clk_in1_0_1;
  wire clk_wiz_1_clk_out1;
  wire clk_wiz_clk_out1;
  wire [32:0]design_hbm_channel_w_1_HBM_AXI_ARADDR;
  wire [1:0]design_hbm_channel_w_1_HBM_AXI_ARBURST;
  wire [5:0]design_hbm_channel_w_1_HBM_AXI_ARID;
  wire [3:0]design_hbm_channel_w_1_HBM_AXI_ARLEN;
  wire design_hbm_channel_w_1_HBM_AXI_ARREADY;
  wire [2:0]design_hbm_channel_w_1_HBM_AXI_ARSIZE;
  wire design_hbm_channel_w_1_HBM_AXI_ARVALID;
  wire [32:0]design_hbm_channel_w_1_HBM_AXI_AWADDR;
  wire [1:0]design_hbm_channel_w_1_HBM_AXI_AWBURST;
  wire [5:0]design_hbm_channel_w_1_HBM_AXI_AWID;
  wire [3:0]design_hbm_channel_w_1_HBM_AXI_AWLEN;
  wire design_hbm_channel_w_1_HBM_AXI_AWREADY;
  wire [2:0]design_hbm_channel_w_1_HBM_AXI_AWSIZE;
  wire design_hbm_channel_w_1_HBM_AXI_AWVALID;
  wire [5:0]design_hbm_channel_w_1_HBM_AXI_BID;
  wire design_hbm_channel_w_1_HBM_AXI_BREADY;
  wire [1:0]design_hbm_channel_w_1_HBM_AXI_BRESP;
  wire design_hbm_channel_w_1_HBM_AXI_BVALID;
  wire [255:0]design_hbm_channel_w_1_HBM_AXI_RDATA;
  wire [5:0]design_hbm_channel_w_1_HBM_AXI_RID;
  wire design_hbm_channel_w_1_HBM_AXI_RLAST;
  wire design_hbm_channel_w_1_HBM_AXI_RREADY;
  wire [1:0]design_hbm_channel_w_1_HBM_AXI_RRESP;
  wire design_hbm_channel_w_1_HBM_AXI_RVALID;
  wire [255:0]design_hbm_channel_w_1_HBM_AXI_WDATA;
  wire design_hbm_channel_w_1_HBM_AXI_WLAST;
  wire design_hbm_channel_w_1_HBM_AXI_WREADY;
  wire [31:0]design_hbm_channel_w_1_HBM_AXI_WSTRB;
  wire design_hbm_channel_w_1_HBM_AXI_WVALID;
  wire [255:0]design_hbm_channel_w_1_HBM_READ_TDATA;
  wire [31:0]design_hbm_channel_w_1_HBM_READ_TKEEP;
  wire design_hbm_channel_w_1_HBM_READ_TLAST;
  wire design_hbm_channel_w_1_HBM_READ_TREADY;
  wire design_hbm_channel_w_1_HBM_READ_TVALID;
  wire design_hbm_channel_w_1_busy;
  wire [32:0]design_hbm_channel_w_2_HBM_AXI_ARADDR;
  wire [1:0]design_hbm_channel_w_2_HBM_AXI_ARBURST;
  wire [5:0]design_hbm_channel_w_2_HBM_AXI_ARID;
  wire [3:0]design_hbm_channel_w_2_HBM_AXI_ARLEN;
  wire design_hbm_channel_w_2_HBM_AXI_ARREADY;
  wire [2:0]design_hbm_channel_w_2_HBM_AXI_ARSIZE;
  wire design_hbm_channel_w_2_HBM_AXI_ARVALID;
  wire [32:0]design_hbm_channel_w_2_HBM_AXI_AWADDR;
  wire [1:0]design_hbm_channel_w_2_HBM_AXI_AWBURST;
  wire [5:0]design_hbm_channel_w_2_HBM_AXI_AWID;
  wire [3:0]design_hbm_channel_w_2_HBM_AXI_AWLEN;
  wire design_hbm_channel_w_2_HBM_AXI_AWREADY;
  wire [2:0]design_hbm_channel_w_2_HBM_AXI_AWSIZE;
  wire design_hbm_channel_w_2_HBM_AXI_AWVALID;
  wire [5:0]design_hbm_channel_w_2_HBM_AXI_BID;
  wire design_hbm_channel_w_2_HBM_AXI_BREADY;
  wire [1:0]design_hbm_channel_w_2_HBM_AXI_BRESP;
  wire design_hbm_channel_w_2_HBM_AXI_BVALID;
  wire [255:0]design_hbm_channel_w_2_HBM_AXI_RDATA;
  wire [5:0]design_hbm_channel_w_2_HBM_AXI_RID;
  wire design_hbm_channel_w_2_HBM_AXI_RLAST;
  wire design_hbm_channel_w_2_HBM_AXI_RREADY;
  wire [1:0]design_hbm_channel_w_2_HBM_AXI_RRESP;
  wire design_hbm_channel_w_2_HBM_AXI_RVALID;
  wire [255:0]design_hbm_channel_w_2_HBM_AXI_WDATA;
  wire design_hbm_channel_w_2_HBM_AXI_WLAST;
  wire design_hbm_channel_w_2_HBM_AXI_WREADY;
  wire [31:0]design_hbm_channel_w_2_HBM_AXI_WSTRB;
  wire design_hbm_channel_w_2_HBM_AXI_WVALID;
  wire [255:0]design_hbm_channel_w_2_HBM_READ_TDATA;
  wire [31:0]design_hbm_channel_w_2_HBM_READ_TKEEP;
  wire design_hbm_channel_w_2_HBM_READ_TLAST;
  wire design_hbm_channel_w_2_HBM_READ_TREADY;
  wire design_hbm_channel_w_2_HBM_READ_TVALID;
  wire design_hbm_channel_w_2_busy;
  wire [32:0]design_hbm_channel_w_3_HBM_AXI_ARADDR;
  wire [1:0]design_hbm_channel_w_3_HBM_AXI_ARBURST;
  wire [5:0]design_hbm_channel_w_3_HBM_AXI_ARID;
  wire [3:0]design_hbm_channel_w_3_HBM_AXI_ARLEN;
  wire design_hbm_channel_w_3_HBM_AXI_ARREADY;
  wire [2:0]design_hbm_channel_w_3_HBM_AXI_ARSIZE;
  wire design_hbm_channel_w_3_HBM_AXI_ARVALID;
  wire [32:0]design_hbm_channel_w_3_HBM_AXI_AWADDR;
  wire [1:0]design_hbm_channel_w_3_HBM_AXI_AWBURST;
  wire [5:0]design_hbm_channel_w_3_HBM_AXI_AWID;
  wire [3:0]design_hbm_channel_w_3_HBM_AXI_AWLEN;
  wire design_hbm_channel_w_3_HBM_AXI_AWREADY;
  wire [2:0]design_hbm_channel_w_3_HBM_AXI_AWSIZE;
  wire design_hbm_channel_w_3_HBM_AXI_AWVALID;
  wire [5:0]design_hbm_channel_w_3_HBM_AXI_BID;
  wire design_hbm_channel_w_3_HBM_AXI_BREADY;
  wire [1:0]design_hbm_channel_w_3_HBM_AXI_BRESP;
  wire design_hbm_channel_w_3_HBM_AXI_BVALID;
  wire [255:0]design_hbm_channel_w_3_HBM_AXI_RDATA;
  wire [5:0]design_hbm_channel_w_3_HBM_AXI_RID;
  wire design_hbm_channel_w_3_HBM_AXI_RLAST;
  wire design_hbm_channel_w_3_HBM_AXI_RREADY;
  wire [1:0]design_hbm_channel_w_3_HBM_AXI_RRESP;
  wire design_hbm_channel_w_3_HBM_AXI_RVALID;
  wire [255:0]design_hbm_channel_w_3_HBM_AXI_WDATA;
  wire design_hbm_channel_w_3_HBM_AXI_WLAST;
  wire design_hbm_channel_w_3_HBM_AXI_WREADY;
  wire [31:0]design_hbm_channel_w_3_HBM_AXI_WSTRB;
  wire design_hbm_channel_w_3_HBM_AXI_WVALID;
  wire [255:0]design_hbm_channel_w_3_HBM_READ_TDATA;
  wire [31:0]design_hbm_channel_w_3_HBM_READ_TKEEP;
  wire design_hbm_channel_w_3_HBM_READ_TLAST;
  wire design_hbm_channel_w_3_HBM_READ_TREADY;
  wire design_hbm_channel_w_3_HBM_READ_TVALID;
  wire design_hbm_channel_w_3_busy;
  wire [32:0]design_hbm_channel_w_4_HBM_AXI_ARADDR;
  wire [1:0]design_hbm_channel_w_4_HBM_AXI_ARBURST;
  wire [5:0]design_hbm_channel_w_4_HBM_AXI_ARID;
  wire [3:0]design_hbm_channel_w_4_HBM_AXI_ARLEN;
  wire design_hbm_channel_w_4_HBM_AXI_ARREADY;
  wire [2:0]design_hbm_channel_w_4_HBM_AXI_ARSIZE;
  wire design_hbm_channel_w_4_HBM_AXI_ARVALID;
  wire [32:0]design_hbm_channel_w_4_HBM_AXI_AWADDR;
  wire [1:0]design_hbm_channel_w_4_HBM_AXI_AWBURST;
  wire [5:0]design_hbm_channel_w_4_HBM_AXI_AWID;
  wire [3:0]design_hbm_channel_w_4_HBM_AXI_AWLEN;
  wire design_hbm_channel_w_4_HBM_AXI_AWREADY;
  wire [2:0]design_hbm_channel_w_4_HBM_AXI_AWSIZE;
  wire design_hbm_channel_w_4_HBM_AXI_AWVALID;
  wire [5:0]design_hbm_channel_w_4_HBM_AXI_BID;
  wire design_hbm_channel_w_4_HBM_AXI_BREADY;
  wire [1:0]design_hbm_channel_w_4_HBM_AXI_BRESP;
  wire design_hbm_channel_w_4_HBM_AXI_BVALID;
  wire [255:0]design_hbm_channel_w_4_HBM_AXI_RDATA;
  wire [5:0]design_hbm_channel_w_4_HBM_AXI_RID;
  wire design_hbm_channel_w_4_HBM_AXI_RLAST;
  wire design_hbm_channel_w_4_HBM_AXI_RREADY;
  wire [1:0]design_hbm_channel_w_4_HBM_AXI_RRESP;
  wire design_hbm_channel_w_4_HBM_AXI_RVALID;
  wire [255:0]design_hbm_channel_w_4_HBM_AXI_WDATA;
  wire design_hbm_channel_w_4_HBM_AXI_WLAST;
  wire design_hbm_channel_w_4_HBM_AXI_WREADY;
  wire [31:0]design_hbm_channel_w_4_HBM_AXI_WSTRB;
  wire design_hbm_channel_w_4_HBM_AXI_WVALID;
  wire [255:0]design_hbm_channel_w_4_HBM_READ_TDATA;
  wire [31:0]design_hbm_channel_w_4_HBM_READ_TKEEP;
  wire design_hbm_channel_w_4_HBM_READ_TLAST;
  wire design_hbm_channel_w_4_HBM_READ_TREADY;
  wire design_hbm_channel_w_4_HBM_READ_TVALID;
  wire design_hbm_channel_w_4_busy;
  wire [32:0]design_hbm_channel_w_5_HBM_AXI_ARADDR;
  wire [1:0]design_hbm_channel_w_5_HBM_AXI_ARBURST;
  wire [5:0]design_hbm_channel_w_5_HBM_AXI_ARID;
  wire [3:0]design_hbm_channel_w_5_HBM_AXI_ARLEN;
  wire design_hbm_channel_w_5_HBM_AXI_ARREADY;
  wire [2:0]design_hbm_channel_w_5_HBM_AXI_ARSIZE;
  wire design_hbm_channel_w_5_HBM_AXI_ARVALID;
  wire [32:0]design_hbm_channel_w_5_HBM_AXI_AWADDR;
  wire [1:0]design_hbm_channel_w_5_HBM_AXI_AWBURST;
  wire [5:0]design_hbm_channel_w_5_HBM_AXI_AWID;
  wire [3:0]design_hbm_channel_w_5_HBM_AXI_AWLEN;
  wire design_hbm_channel_w_5_HBM_AXI_AWREADY;
  wire [2:0]design_hbm_channel_w_5_HBM_AXI_AWSIZE;
  wire design_hbm_channel_w_5_HBM_AXI_AWVALID;
  wire [5:0]design_hbm_channel_w_5_HBM_AXI_BID;
  wire design_hbm_channel_w_5_HBM_AXI_BREADY;
  wire [1:0]design_hbm_channel_w_5_HBM_AXI_BRESP;
  wire design_hbm_channel_w_5_HBM_AXI_BVALID;
  wire [255:0]design_hbm_channel_w_5_HBM_AXI_RDATA;
  wire [5:0]design_hbm_channel_w_5_HBM_AXI_RID;
  wire design_hbm_channel_w_5_HBM_AXI_RLAST;
  wire design_hbm_channel_w_5_HBM_AXI_RREADY;
  wire [1:0]design_hbm_channel_w_5_HBM_AXI_RRESP;
  wire design_hbm_channel_w_5_HBM_AXI_RVALID;
  wire [255:0]design_hbm_channel_w_5_HBM_AXI_WDATA;
  wire design_hbm_channel_w_5_HBM_AXI_WLAST;
  wire design_hbm_channel_w_5_HBM_AXI_WREADY;
  wire [31:0]design_hbm_channel_w_5_HBM_AXI_WSTRB;
  wire design_hbm_channel_w_5_HBM_AXI_WVALID;
  wire [255:0]design_hbm_channel_w_5_HBM_READ_TDATA;
  wire [31:0]design_hbm_channel_w_5_HBM_READ_TKEEP;
  wire design_hbm_channel_w_5_HBM_READ_TLAST;
  wire design_hbm_channel_w_5_HBM_READ_TREADY;
  wire design_hbm_channel_w_5_HBM_READ_TVALID;
  wire design_hbm_channel_w_5_busy;
  wire [32:0]design_hbm_channel_w_6_HBM_AXI_ARADDR;
  wire [1:0]design_hbm_channel_w_6_HBM_AXI_ARBURST;
  wire [5:0]design_hbm_channel_w_6_HBM_AXI_ARID;
  wire [3:0]design_hbm_channel_w_6_HBM_AXI_ARLEN;
  wire design_hbm_channel_w_6_HBM_AXI_ARREADY;
  wire [2:0]design_hbm_channel_w_6_HBM_AXI_ARSIZE;
  wire design_hbm_channel_w_6_HBM_AXI_ARVALID;
  wire [32:0]design_hbm_channel_w_6_HBM_AXI_AWADDR;
  wire [1:0]design_hbm_channel_w_6_HBM_AXI_AWBURST;
  wire [5:0]design_hbm_channel_w_6_HBM_AXI_AWID;
  wire [3:0]design_hbm_channel_w_6_HBM_AXI_AWLEN;
  wire design_hbm_channel_w_6_HBM_AXI_AWREADY;
  wire [2:0]design_hbm_channel_w_6_HBM_AXI_AWSIZE;
  wire design_hbm_channel_w_6_HBM_AXI_AWVALID;
  wire [5:0]design_hbm_channel_w_6_HBM_AXI_BID;
  wire design_hbm_channel_w_6_HBM_AXI_BREADY;
  wire [1:0]design_hbm_channel_w_6_HBM_AXI_BRESP;
  wire design_hbm_channel_w_6_HBM_AXI_BVALID;
  wire [255:0]design_hbm_channel_w_6_HBM_AXI_RDATA;
  wire [5:0]design_hbm_channel_w_6_HBM_AXI_RID;
  wire design_hbm_channel_w_6_HBM_AXI_RLAST;
  wire design_hbm_channel_w_6_HBM_AXI_RREADY;
  wire [1:0]design_hbm_channel_w_6_HBM_AXI_RRESP;
  wire design_hbm_channel_w_6_HBM_AXI_RVALID;
  wire [255:0]design_hbm_channel_w_6_HBM_AXI_WDATA;
  wire design_hbm_channel_w_6_HBM_AXI_WLAST;
  wire design_hbm_channel_w_6_HBM_AXI_WREADY;
  wire [31:0]design_hbm_channel_w_6_HBM_AXI_WSTRB;
  wire design_hbm_channel_w_6_HBM_AXI_WVALID;
  wire [255:0]design_hbm_channel_w_6_HBM_READ_TDATA;
  wire [31:0]design_hbm_channel_w_6_HBM_READ_TKEEP;
  wire design_hbm_channel_w_6_HBM_READ_TLAST;
  wire design_hbm_channel_w_6_HBM_READ_TREADY;
  wire design_hbm_channel_w_6_HBM_READ_TVALID;
  wire design_hbm_channel_w_6_busy;
  wire [32:0]design_hbm_channel_w_7_HBM_AXI_ARADDR;
  wire [1:0]design_hbm_channel_w_7_HBM_AXI_ARBURST;
  wire [5:0]design_hbm_channel_w_7_HBM_AXI_ARID;
  wire [3:0]design_hbm_channel_w_7_HBM_AXI_ARLEN;
  wire design_hbm_channel_w_7_HBM_AXI_ARREADY;
  wire [2:0]design_hbm_channel_w_7_HBM_AXI_ARSIZE;
  wire design_hbm_channel_w_7_HBM_AXI_ARVALID;
  wire [32:0]design_hbm_channel_w_7_HBM_AXI_AWADDR;
  wire [1:0]design_hbm_channel_w_7_HBM_AXI_AWBURST;
  wire [5:0]design_hbm_channel_w_7_HBM_AXI_AWID;
  wire [3:0]design_hbm_channel_w_7_HBM_AXI_AWLEN;
  wire design_hbm_channel_w_7_HBM_AXI_AWREADY;
  wire [2:0]design_hbm_channel_w_7_HBM_AXI_AWSIZE;
  wire design_hbm_channel_w_7_HBM_AXI_AWVALID;
  wire [5:0]design_hbm_channel_w_7_HBM_AXI_BID;
  wire design_hbm_channel_w_7_HBM_AXI_BREADY;
  wire [1:0]design_hbm_channel_w_7_HBM_AXI_BRESP;
  wire design_hbm_channel_w_7_HBM_AXI_BVALID;
  wire [255:0]design_hbm_channel_w_7_HBM_AXI_RDATA;
  wire [5:0]design_hbm_channel_w_7_HBM_AXI_RID;
  wire design_hbm_channel_w_7_HBM_AXI_RLAST;
  wire design_hbm_channel_w_7_HBM_AXI_RREADY;
  wire [1:0]design_hbm_channel_w_7_HBM_AXI_RRESP;
  wire design_hbm_channel_w_7_HBM_AXI_RVALID;
  wire [255:0]design_hbm_channel_w_7_HBM_AXI_WDATA;
  wire design_hbm_channel_w_7_HBM_AXI_WLAST;
  wire design_hbm_channel_w_7_HBM_AXI_WREADY;
  wire [31:0]design_hbm_channel_w_7_HBM_AXI_WSTRB;
  wire design_hbm_channel_w_7_HBM_AXI_WVALID;
  wire [255:0]design_hbm_channel_w_7_HBM_READ_TDATA;
  wire [31:0]design_hbm_channel_w_7_HBM_READ_TKEEP;
  wire design_hbm_channel_w_7_HBM_READ_TLAST;
  wire design_hbm_channel_w_7_HBM_READ_TREADY;
  wire design_hbm_channel_w_7_HBM_READ_TVALID;
  wire design_hbm_channel_w_7_busy;
  wire [32:0]design_hbm_channel_w_8_HBM_AXI_ARADDR;
  wire [1:0]design_hbm_channel_w_8_HBM_AXI_ARBURST;
  wire [5:0]design_hbm_channel_w_8_HBM_AXI_ARID;
  wire [3:0]design_hbm_channel_w_8_HBM_AXI_ARLEN;
  wire design_hbm_channel_w_8_HBM_AXI_ARREADY;
  wire [2:0]design_hbm_channel_w_8_HBM_AXI_ARSIZE;
  wire design_hbm_channel_w_8_HBM_AXI_ARVALID;
  wire [32:0]design_hbm_channel_w_8_HBM_AXI_AWADDR;
  wire [1:0]design_hbm_channel_w_8_HBM_AXI_AWBURST;
  wire [5:0]design_hbm_channel_w_8_HBM_AXI_AWID;
  wire [3:0]design_hbm_channel_w_8_HBM_AXI_AWLEN;
  wire design_hbm_channel_w_8_HBM_AXI_AWREADY;
  wire [2:0]design_hbm_channel_w_8_HBM_AXI_AWSIZE;
  wire design_hbm_channel_w_8_HBM_AXI_AWVALID;
  wire [5:0]design_hbm_channel_w_8_HBM_AXI_BID;
  wire design_hbm_channel_w_8_HBM_AXI_BREADY;
  wire [1:0]design_hbm_channel_w_8_HBM_AXI_BRESP;
  wire design_hbm_channel_w_8_HBM_AXI_BVALID;
  wire [255:0]design_hbm_channel_w_8_HBM_AXI_RDATA;
  wire [5:0]design_hbm_channel_w_8_HBM_AXI_RID;
  wire design_hbm_channel_w_8_HBM_AXI_RLAST;
  wire design_hbm_channel_w_8_HBM_AXI_RREADY;
  wire [1:0]design_hbm_channel_w_8_HBM_AXI_RRESP;
  wire design_hbm_channel_w_8_HBM_AXI_RVALID;
  wire [255:0]design_hbm_channel_w_8_HBM_AXI_WDATA;
  wire design_hbm_channel_w_8_HBM_AXI_WLAST;
  wire design_hbm_channel_w_8_HBM_AXI_WREADY;
  wire [31:0]design_hbm_channel_w_8_HBM_AXI_WSTRB;
  wire design_hbm_channel_w_8_HBM_AXI_WVALID;
  wire [255:0]design_hbm_channel_w_8_HBM_READ_TDATA;
  wire [31:0]design_hbm_channel_w_8_HBM_READ_TKEEP;
  wire design_hbm_channel_w_8_HBM_READ_TLAST;
  wire design_hbm_channel_w_8_HBM_READ_TREADY;
  wire design_hbm_channel_w_8_HBM_READ_TVALID;
  wire design_hbm_channel_w_8_busy;
  wire [32:0]design_hbm_channel_w_9_HBM_AXI_ARADDR;
  wire [1:0]design_hbm_channel_w_9_HBM_AXI_ARBURST;
  wire [5:0]design_hbm_channel_w_9_HBM_AXI_ARID;
  wire [3:0]design_hbm_channel_w_9_HBM_AXI_ARLEN;
  wire design_hbm_channel_w_9_HBM_AXI_ARREADY;
  wire [2:0]design_hbm_channel_w_9_HBM_AXI_ARSIZE;
  wire design_hbm_channel_w_9_HBM_AXI_ARVALID;
  wire [32:0]design_hbm_channel_w_9_HBM_AXI_AWADDR;
  wire [1:0]design_hbm_channel_w_9_HBM_AXI_AWBURST;
  wire [5:0]design_hbm_channel_w_9_HBM_AXI_AWID;
  wire [3:0]design_hbm_channel_w_9_HBM_AXI_AWLEN;
  wire design_hbm_channel_w_9_HBM_AXI_AWREADY;
  wire [2:0]design_hbm_channel_w_9_HBM_AXI_AWSIZE;
  wire design_hbm_channel_w_9_HBM_AXI_AWVALID;
  wire [5:0]design_hbm_channel_w_9_HBM_AXI_BID;
  wire design_hbm_channel_w_9_HBM_AXI_BREADY;
  wire [1:0]design_hbm_channel_w_9_HBM_AXI_BRESP;
  wire design_hbm_channel_w_9_HBM_AXI_BVALID;
  wire [255:0]design_hbm_channel_w_9_HBM_AXI_RDATA;
  wire [5:0]design_hbm_channel_w_9_HBM_AXI_RID;
  wire design_hbm_channel_w_9_HBM_AXI_RLAST;
  wire design_hbm_channel_w_9_HBM_AXI_RREADY;
  wire [1:0]design_hbm_channel_w_9_HBM_AXI_RRESP;
  wire design_hbm_channel_w_9_HBM_AXI_RVALID;
  wire [255:0]design_hbm_channel_w_9_HBM_AXI_WDATA;
  wire design_hbm_channel_w_9_HBM_AXI_WLAST;
  wire design_hbm_channel_w_9_HBM_AXI_WREADY;
  wire [31:0]design_hbm_channel_w_9_HBM_AXI_WSTRB;
  wire design_hbm_channel_w_9_HBM_AXI_WVALID;
  wire [255:0]design_hbm_channel_w_9_HBM_READ_TDATA;
  wire [31:0]design_hbm_channel_w_9_HBM_READ_TKEEP;
  wire design_hbm_channel_w_9_HBM_READ_TLAST;
  wire design_hbm_channel_w_9_HBM_READ_TREADY;
  wire design_hbm_channel_w_9_HBM_READ_TVALID;
  wire design_hbm_channel_w_9_busy;
  wire hbm_0_apb_complete_0;
  wire hbm_0_apb_complete_1;
  wire [255:0]hbmlane_mux31_0_m_HBM_READ_TDATA;
  wire [31:0]hbmlane_mux31_0_m_HBM_READ_TKEEP;
  wire hbmlane_mux31_0_m_HBM_READ_TLAST;
  wire hbmlane_mux31_0_m_HBM_READ_TREADY;
  wire hbmlane_mux31_0_m_HBM_READ_TVALID;
  wire hbmlane_mux31_0_m_busy;
  wire [255:0]hbmlane_mux31_0_s1_HBM_WRITE_TDATA;
  wire hbmlane_mux31_0_s1_HBM_WRITE_TREADY;
  wire hbmlane_mux31_0_s1_HBM_WRITE_TVALID;
  wire [31:0]hbmlane_mux31_0_s1_ctrl_ports_rd_blockamount;
  wire hbmlane_mux31_0_s1_ctrl_ports_rd_run;
  wire [31:0]hbmlane_mux31_0_s1_ctrl_ports_rd_startaddress;
  wire [31:0]hbmlane_mux31_0_s1_ctrl_ports_wr_blockamount;
  wire hbmlane_mux31_0_s1_ctrl_ports_wr_run;
  wire [31:0]hbmlane_mux31_0_s1_ctrl_ports_wr_startaddress;
  wire [255:0]hbmlane_mux31_0_s2_HBM_WRITE_TDATA;
  wire hbmlane_mux31_0_s2_HBM_WRITE_TREADY;
  wire hbmlane_mux31_0_s2_HBM_WRITE_TVALID;
  wire [31:0]hbmlane_mux31_0_s2_ctrl_ports_rd_blockamount;
  wire hbmlane_mux31_0_s2_ctrl_ports_rd_run;
  wire [31:0]hbmlane_mux31_0_s2_ctrl_ports_rd_startaddress;
  wire [31:0]hbmlane_mux31_0_s2_ctrl_ports_wr_blockamount;
  wire hbmlane_mux31_0_s2_ctrl_ports_wr_run;
  wire [31:0]hbmlane_mux31_0_s2_ctrl_ports_wr_startaddress;
  wire [255:0]hbmlane_mux31_0_s3_HBM_WRITE_TDATA;
  wire hbmlane_mux31_0_s3_HBM_WRITE_TREADY;
  wire hbmlane_mux31_0_s3_HBM_WRITE_TVALID;
  wire [31:0]hbmlane_mux31_0_s3_ctrl_ports_rd_blockamount;
  wire hbmlane_mux31_0_s3_ctrl_ports_rd_run;
  wire [31:0]hbmlane_mux31_0_s3_ctrl_ports_rd_startaddress;
  wire [31:0]hbmlane_mux31_0_s3_ctrl_ports_wr_blockamount;
  wire hbmlane_mux31_0_s3_ctrl_ports_wr_run;
  wire [31:0]hbmlane_mux31_0_s3_ctrl_ports_wr_startaddress;
  wire [255:0]hbmlane_mux31_1_m_HBM_READ_TDATA;
  wire [31:0]hbmlane_mux31_1_m_HBM_READ_TKEEP;
  wire hbmlane_mux31_1_m_HBM_READ_TLAST;
  wire hbmlane_mux31_1_m_HBM_READ_TREADY;
  wire hbmlane_mux31_1_m_HBM_READ_TVALID;
  wire hbmlane_mux31_1_m_busy;
  wire [255:0]hbmlane_mux31_1_s1_HBM_WRITE_TDATA;
  wire hbmlane_mux31_1_s1_HBM_WRITE_TREADY;
  wire hbmlane_mux31_1_s1_HBM_WRITE_TVALID;
  wire [31:0]hbmlane_mux31_1_s1_ctrl_ports_rd_blockamount;
  wire hbmlane_mux31_1_s1_ctrl_ports_rd_run;
  wire [31:0]hbmlane_mux31_1_s1_ctrl_ports_rd_startaddress;
  wire [31:0]hbmlane_mux31_1_s1_ctrl_ports_wr_blockamount;
  wire hbmlane_mux31_1_s1_ctrl_ports_wr_run;
  wire [31:0]hbmlane_mux31_1_s1_ctrl_ports_wr_startaddress;
  wire [255:0]hbmlane_mux31_1_s2_HBM_WRITE_TDATA;
  wire hbmlane_mux31_1_s2_HBM_WRITE_TREADY;
  wire hbmlane_mux31_1_s2_HBM_WRITE_TVALID;
  wire [31:0]hbmlane_mux31_1_s2_ctrl_ports_rd_blockamount;
  wire hbmlane_mux31_1_s2_ctrl_ports_rd_run;
  wire [31:0]hbmlane_mux31_1_s2_ctrl_ports_rd_startaddress;
  wire [31:0]hbmlane_mux31_1_s2_ctrl_ports_wr_blockamount;
  wire hbmlane_mux31_1_s2_ctrl_ports_wr_run;
  wire [31:0]hbmlane_mux31_1_s2_ctrl_ports_wr_startaddress;
  wire [255:0]hbmlane_mux31_1_s3_HBM_WRITE_TDATA;
  wire hbmlane_mux31_1_s3_HBM_WRITE_TREADY;
  wire hbmlane_mux31_1_s3_HBM_WRITE_TVALID;
  wire [31:0]hbmlane_mux31_1_s3_ctrl_ports_rd_blockamount;
  wire hbmlane_mux31_1_s3_ctrl_ports_rd_run;
  wire [31:0]hbmlane_mux31_1_s3_ctrl_ports_rd_startaddress;
  wire [31:0]hbmlane_mux31_1_s3_ctrl_ports_wr_blockamount;
  wire hbmlane_mux31_1_s3_ctrl_ports_wr_run;
  wire [31:0]hbmlane_mux31_1_s3_ctrl_ports_wr_startaddress;
  wire [255:0]hbmlane_mux31_2_m_HBM_READ_TDATA;
  wire [31:0]hbmlane_mux31_2_m_HBM_READ_TKEEP;
  wire hbmlane_mux31_2_m_HBM_READ_TLAST;
  wire hbmlane_mux31_2_m_HBM_READ_TREADY;
  wire hbmlane_mux31_2_m_HBM_READ_TVALID;
  wire hbmlane_mux31_2_m_busy;
  wire [255:0]hbmlane_mux31_2_s1_HBM_WRITE_TDATA;
  wire hbmlane_mux31_2_s1_HBM_WRITE_TREADY;
  wire hbmlane_mux31_2_s1_HBM_WRITE_TVALID;
  wire [31:0]hbmlane_mux31_2_s1_ctrl_ports_rd_blockamount;
  wire hbmlane_mux31_2_s1_ctrl_ports_rd_run;
  wire [31:0]hbmlane_mux31_2_s1_ctrl_ports_rd_startaddress;
  wire [31:0]hbmlane_mux31_2_s1_ctrl_ports_wr_blockamount;
  wire hbmlane_mux31_2_s1_ctrl_ports_wr_run;
  wire [31:0]hbmlane_mux31_2_s1_ctrl_ports_wr_startaddress;
  wire [255:0]hbmlane_mux31_2_s2_HBM_WRITE_TDATA;
  wire hbmlane_mux31_2_s2_HBM_WRITE_TREADY;
  wire hbmlane_mux31_2_s2_HBM_WRITE_TVALID;
  wire [31:0]hbmlane_mux31_2_s2_ctrl_ports_rd_blockamount;
  wire hbmlane_mux31_2_s2_ctrl_ports_rd_run;
  wire [31:0]hbmlane_mux31_2_s2_ctrl_ports_rd_startaddress;
  wire [31:0]hbmlane_mux31_2_s2_ctrl_ports_wr_blockamount;
  wire hbmlane_mux31_2_s2_ctrl_ports_wr_run;
  wire [31:0]hbmlane_mux31_2_s2_ctrl_ports_wr_startaddress;
  wire [255:0]hbmlane_mux31_2_s3_HBM_WRITE_TDATA;
  wire hbmlane_mux31_2_s3_HBM_WRITE_TREADY;
  wire hbmlane_mux31_2_s3_HBM_WRITE_TVALID;
  wire [31:0]hbmlane_mux31_2_s3_ctrl_ports_rd_blockamount;
  wire hbmlane_mux31_2_s3_ctrl_ports_rd_run;
  wire [31:0]hbmlane_mux31_2_s3_ctrl_ports_rd_startaddress;
  wire [31:0]hbmlane_mux31_2_s3_ctrl_ports_wr_blockamount;
  wire hbmlane_mux31_2_s3_ctrl_ports_wr_run;
  wire [31:0]hbmlane_mux31_2_s3_ctrl_ports_wr_startaddress;
  wire [31:0]m_ctrl_ports_0_1_rd_blockamount;
  wire m_ctrl_ports_0_1_rd_run;
  wire [31:0]m_ctrl_ports_0_1_rd_startaddress;
  wire [31:0]m_ctrl_ports_0_1_wr_blockamount;
  wire m_ctrl_ports_0_1_wr_run;
  wire [31:0]m_ctrl_ports_0_1_wr_startaddress;
  wire [31:0]m_ctrl_ports_1_1_rd_blockamount;
  wire m_ctrl_ports_1_1_rd_run;
  wire [31:0]m_ctrl_ports_1_1_rd_startaddress;
  wire [31:0]m_ctrl_ports_1_1_wr_blockamount;
  wire m_ctrl_ports_1_1_wr_run;
  wire [31:0]m_ctrl_ports_1_1_wr_startaddress;
  wire [31:0]m_ctrl_ports_2_1_rd_blockamount;
  wire m_ctrl_ports_2_1_rd_run;
  wire [31:0]m_ctrl_ports_2_1_rd_startaddress;
  wire [31:0]m_ctrl_ports_2_1_wr_blockamount;
  wire m_ctrl_ports_2_1_wr_run;
  wire [31:0]m_ctrl_ports_2_1_wr_startaddress;
  wire [1:0]mux_select_0_1;
  wire [1:0]mux_select_1_1;
  wire [1:0]mux_select_2_1;
  wire reset_0_1;
  wire [0:0]util_vector_logic_0_Res;
  wire NLW_clk_wiz_1_locked_UNCONNECTED;
  wire [5:0]NLW_design_hbm_channel_w_1_HBM_AXI_wid_UNCONNECTED;
  wire [5:0]NLW_design_hbm_channel_w_2_HBM_AXI_wid_UNCONNECTED;
  wire [5:0]NLW_design_hbm_channel_w_3_HBM_AXI_wid_UNCONNECTED;
  wire [5:0]NLW_design_hbm_channel_w_4_HBM_AXI_wid_UNCONNECTED;
  wire [5:0]NLW_design_hbm_channel_w_5_HBM_AXI_wid_UNCONNECTED;
  wire [5:0]NLW_design_hbm_channel_w_6_HBM_AXI_wid_UNCONNECTED;
  wire [5:0]NLW_design_hbm_channel_w_7_HBM_AXI_wid_UNCONNECTED;
  wire [5:0]NLW_design_hbm_channel_w_8_HBM_AXI_wid_UNCONNECTED;
  wire [5:0]NLW_design_hbm_channel_w_9_HBM_AXI_wid_UNCONNECTED;
  wire NLW_hbm_0_DRAM_0_STAT_CATTRIP_UNCONNECTED;
  wire NLW_hbm_0_DRAM_1_STAT_CATTRIP_UNCONNECTED;
  wire [31:0]NLW_hbm_0_AXI_00_RDATA_PARITY_UNCONNECTED;
  wire [31:0]NLW_hbm_0_AXI_01_RDATA_PARITY_UNCONNECTED;
  wire [31:0]NLW_hbm_0_AXI_02_RDATA_PARITY_UNCONNECTED;
  wire [31:0]NLW_hbm_0_AXI_03_RDATA_PARITY_UNCONNECTED;
  wire [31:0]NLW_hbm_0_AXI_04_RDATA_PARITY_UNCONNECTED;
  wire [31:0]NLW_hbm_0_AXI_05_RDATA_PARITY_UNCONNECTED;
  wire [31:0]NLW_hbm_0_AXI_06_RDATA_PARITY_UNCONNECTED;
  wire [31:0]NLW_hbm_0_AXI_07_RDATA_PARITY_UNCONNECTED;
  wire [31:0]NLW_hbm_0_AXI_08_RDATA_PARITY_UNCONNECTED;
  wire [6:0]NLW_hbm_0_DRAM_0_STAT_TEMP_UNCONNECTED;
  wire [6:0]NLW_hbm_0_DRAM_1_STAT_TEMP_UNCONNECTED;
  wire NLW_proc_sys_reset_0_mb_reset_UNCONNECTED;
  wire [0:0]NLW_proc_sys_reset_0_bus_struct_reset_UNCONNECTED;
  wire [0:0]NLW_proc_sys_reset_0_interconnect_aresetn_UNCONNECTED;
  wire [0:0]NLW_proc_sys_reset_0_peripheral_reset_UNCONNECTED;
  wire NLW_proc_sys_reset_1_mb_reset_UNCONNECTED;
  wire [0:0]NLW_proc_sys_reset_1_bus_struct_reset_UNCONNECTED;
  wire [0:0]NLW_proc_sys_reset_1_interconnect_aresetn_UNCONNECTED;
  wire [0:0]NLW_proc_sys_reset_1_peripheral_reset_UNCONNECTED;

  assign HBM_READ_m_0_tdata[255:0] = axis_register_slice_5_M_AXIS_TDATA;
  assign HBM_READ_m_0_tkeep[31:0] = axis_register_slice_5_M_AXIS_TKEEP;
  assign HBM_READ_m_0_tlast = axis_register_slice_5_M_AXIS_TLAST;
  assign HBM_READ_m_0_tvalid = axis_register_slice_5_M_AXIS_TVALID;
  assign HBM_READ_m_1_tdata[255:0] = axis_register_slice_1_M_AXIS_TDATA;
  assign HBM_READ_m_1_tkeep[31:0] = axis_register_slice_1_M_AXIS_TKEEP;
  assign HBM_READ_m_1_tlast = axis_register_slice_1_M_AXIS_TLAST;
  assign HBM_READ_m_1_tvalid = axis_register_slice_1_M_AXIS_TVALID;
  assign HBM_READ_m_2_tdata[255:0] = axis_register_slice_3_M_AXIS_TDATA;
  assign HBM_READ_m_2_tkeep[31:0] = axis_register_slice_3_M_AXIS_TKEEP;
  assign HBM_READ_m_2_tlast = axis_register_slice_3_M_AXIS_TLAST;
  assign HBM_READ_m_2_tvalid = axis_register_slice_3_M_AXIS_TVALID;
  assign HBM_STATUS[0] = util_vector_logic_0_Res;
  assign HBM_WRITE_m_0_1_TDATA = HBM_WRITE_m_0_tdata[63:0];
  assign HBM_WRITE_m_0_1_TVALID = HBM_WRITE_m_0_tvalid;
  assign HBM_WRITE_m_0_tready = HBM_WRITE_m_0_1_TREADY;
  assign HBM_WRITE_m_1_1_TDATA = HBM_WRITE_m_1_tdata[63:0];
  assign HBM_WRITE_m_1_1_TVALID = HBM_WRITE_m_1_tvalid;
  assign HBM_WRITE_m_1_tready = HBM_WRITE_m_1_1_TREADY;
  assign HBM_WRITE_m_2_1_TDATA = HBM_WRITE_m_2_tdata[63:0];
  assign HBM_WRITE_m_2_1_TVALID = HBM_WRITE_m_2_tvalid;
  assign HBM_WRITE_m_2_tready = HBM_WRITE_m_2_1_TREADY;
  assign Net2 = clk_312MHz;
  assign axis_register_slice_1_M_AXIS_TREADY = HBM_READ_m_1_tready;
  assign axis_register_slice_3_M_AXIS_TREADY = HBM_READ_m_2_tready;
  assign axis_register_slice_5_M_AXIS_TREADY = HBM_READ_m_0_tready;
  assign clk_in1_0_1 = clk_100MHz;
  assign m_busy_0 = hbmlane_mux31_0_m_busy;
  assign m_busy_1 = hbmlane_mux31_1_m_busy;
  assign m_busy_2 = hbmlane_mux31_2_m_busy;
  assign m_ctrl_ports_0_1_rd_blockamount = m_ctrl_ports_0_rd_blockamount[31:0];
  assign m_ctrl_ports_0_1_rd_run = m_ctrl_ports_0_rd_run;
  assign m_ctrl_ports_0_1_rd_startaddress = m_ctrl_ports_0_rd_startaddress[31:0];
  assign m_ctrl_ports_0_1_wr_blockamount = m_ctrl_ports_0_wr_blockamount[31:0];
  assign m_ctrl_ports_0_1_wr_run = m_ctrl_ports_0_wr_run;
  assign m_ctrl_ports_0_1_wr_startaddress = m_ctrl_ports_0_wr_startaddress[31:0];
  assign m_ctrl_ports_1_1_rd_blockamount = m_ctrl_ports_1_rd_blockamount[31:0];
  assign m_ctrl_ports_1_1_rd_run = m_ctrl_ports_1_rd_run;
  assign m_ctrl_ports_1_1_rd_startaddress = m_ctrl_ports_1_rd_startaddress[31:0];
  assign m_ctrl_ports_1_1_wr_blockamount = m_ctrl_ports_1_wr_blockamount[31:0];
  assign m_ctrl_ports_1_1_wr_run = m_ctrl_ports_1_wr_run;
  assign m_ctrl_ports_1_1_wr_startaddress = m_ctrl_ports_1_wr_startaddress[31:0];
  assign m_ctrl_ports_2_1_rd_blockamount = m_ctrl_ports_2_rd_blockamount[31:0];
  assign m_ctrl_ports_2_1_rd_run = m_ctrl_ports_2_rd_run;
  assign m_ctrl_ports_2_1_rd_startaddress = m_ctrl_ports_2_rd_startaddress[31:0];
  assign m_ctrl_ports_2_1_wr_blockamount = m_ctrl_ports_2_wr_blockamount[31:0];
  assign m_ctrl_ports_2_1_wr_run = m_ctrl_ports_2_wr_run;
  assign m_ctrl_ports_2_1_wr_startaddress = m_ctrl_ports_2_wr_startaddress[31:0];
  assign mux_select_0_1 = mux_select_0[1:0];
  assign mux_select_1_1 = mux_select_1[1:0];
  assign mux_select_2_1 = mux_select_2[1:0];
  assign reset_0_1 = reset;
  design_hbm_axis_register_slice_0_1 axis_register_slice_0
       (.aclk(Net2),
        .aresetn(Net3),
        .m_axis_tdata(axis_register_slice_0_M_AXIS_TDATA),
        .m_axis_tready(axis_register_slice_0_M_AXIS_TREADY),
        .m_axis_tvalid(axis_register_slice_0_M_AXIS_TVALID),
        .s_axis_tdata(HBM_WRITE_m_1_1_TDATA),
        .s_axis_tready(HBM_WRITE_m_1_1_TREADY),
        .s_axis_tvalid(HBM_WRITE_m_1_1_TVALID));
  design_hbm_axis_register_slice_0_2 axis_register_slice_1
       (.aclk(Net2),
        .aresetn(Net3),
        .m_axis_tdata(axis_register_slice_1_M_AXIS_TDATA),
        .m_axis_tkeep(axis_register_slice_1_M_AXIS_TKEEP),
        .m_axis_tlast(axis_register_slice_1_M_AXIS_TLAST),
        .m_axis_tready(axis_register_slice_1_M_AXIS_TREADY),
        .m_axis_tvalid(axis_register_slice_1_M_AXIS_TVALID),
        .s_axis_tdata(hbmlane_mux31_1_m_HBM_READ_TDATA),
        .s_axis_tkeep(hbmlane_mux31_1_m_HBM_READ_TKEEP),
        .s_axis_tlast(hbmlane_mux31_1_m_HBM_READ_TLAST),
        .s_axis_tready(hbmlane_mux31_1_m_HBM_READ_TREADY),
        .s_axis_tvalid(hbmlane_mux31_1_m_HBM_READ_TVALID));
  design_hbm_axis_register_slice_0_3 axis_register_slice_2
       (.aclk(Net2),
        .aresetn(Net3),
        .m_axis_tdata(axis_register_slice_2_M_AXIS_TDATA),
        .m_axis_tready(axis_register_slice_2_M_AXIS_TREADY),
        .m_axis_tvalid(axis_register_slice_2_M_AXIS_TVALID),
        .s_axis_tdata(HBM_WRITE_m_2_1_TDATA),
        .s_axis_tready(HBM_WRITE_m_2_1_TREADY),
        .s_axis_tvalid(HBM_WRITE_m_2_1_TVALID));
  design_hbm_axis_register_slice_0_4 axis_register_slice_3
       (.aclk(Net2),
        .aresetn(Net3),
        .m_axis_tdata(axis_register_slice_3_M_AXIS_TDATA),
        .m_axis_tkeep(axis_register_slice_3_M_AXIS_TKEEP),
        .m_axis_tlast(axis_register_slice_3_M_AXIS_TLAST),
        .m_axis_tready(axis_register_slice_3_M_AXIS_TREADY),
        .m_axis_tvalid(axis_register_slice_3_M_AXIS_TVALID),
        .s_axis_tdata(hbmlane_mux31_2_m_HBM_READ_TDATA),
        .s_axis_tkeep(hbmlane_mux31_2_m_HBM_READ_TKEEP),
        .s_axis_tlast(hbmlane_mux31_2_m_HBM_READ_TLAST),
        .s_axis_tready(hbmlane_mux31_2_m_HBM_READ_TREADY),
        .s_axis_tvalid(hbmlane_mux31_2_m_HBM_READ_TVALID));
  design_hbm_axis_register_slice_2_0 axis_register_slice_4
       (.aclk(Net2),
        .aresetn(Net3),
        .m_axis_tdata(axis_register_slice_4_M_AXIS_TDATA),
        .m_axis_tready(axis_register_slice_4_M_AXIS_TREADY),
        .m_axis_tvalid(axis_register_slice_4_M_AXIS_TVALID),
        .s_axis_tdata(HBM_WRITE_m_0_1_TDATA),
        .s_axis_tready(HBM_WRITE_m_0_1_TREADY),
        .s_axis_tvalid(HBM_WRITE_m_0_1_TVALID));
  design_hbm_axis_register_slice_2_1 axis_register_slice_5
       (.aclk(Net2),
        .aresetn(Net3),
        .m_axis_tdata(axis_register_slice_5_M_AXIS_TDATA),
        .m_axis_tkeep(axis_register_slice_5_M_AXIS_TKEEP),
        .m_axis_tlast(axis_register_slice_5_M_AXIS_TLAST),
        .m_axis_tready(axis_register_slice_5_M_AXIS_TREADY),
        .m_axis_tvalid(axis_register_slice_5_M_AXIS_TVALID),
        .s_axis_tdata(hbmlane_mux31_0_m_HBM_READ_TDATA),
        .s_axis_tkeep(hbmlane_mux31_0_m_HBM_READ_TKEEP),
        .s_axis_tlast(hbmlane_mux31_0_m_HBM_READ_TLAST),
        .s_axis_tready(hbmlane_mux31_0_m_HBM_READ_TREADY),
        .s_axis_tvalid(hbmlane_mux31_0_m_HBM_READ_TVALID));
  design_hbm_clk_wiz_1_0 clk_wiz_1
       (.clk_in1(clk_in1_0_1),
        .clk_out1(clk_wiz_1_clk_out1),
        .clk_out2(clk_wiz_clk_out1),
        .reset(reset_0_1));
  design_hbm_design_hbm_channel_w_0_1 design_hbm_channel_w_1
       (.HBM_AXI_araddr(design_hbm_channel_w_1_HBM_AXI_ARADDR),
        .HBM_AXI_arburst(design_hbm_channel_w_1_HBM_AXI_ARBURST),
        .HBM_AXI_arid(design_hbm_channel_w_1_HBM_AXI_ARID),
        .HBM_AXI_arlen(design_hbm_channel_w_1_HBM_AXI_ARLEN),
        .HBM_AXI_arready(design_hbm_channel_w_1_HBM_AXI_ARREADY),
        .HBM_AXI_arsize(design_hbm_channel_w_1_HBM_AXI_ARSIZE),
        .HBM_AXI_arvalid(design_hbm_channel_w_1_HBM_AXI_ARVALID),
        .HBM_AXI_awaddr(design_hbm_channel_w_1_HBM_AXI_AWADDR),
        .HBM_AXI_awburst(design_hbm_channel_w_1_HBM_AXI_AWBURST),
        .HBM_AXI_awid(design_hbm_channel_w_1_HBM_AXI_AWID),
        .HBM_AXI_awlen(design_hbm_channel_w_1_HBM_AXI_AWLEN),
        .HBM_AXI_awready(design_hbm_channel_w_1_HBM_AXI_AWREADY),
        .HBM_AXI_awsize(design_hbm_channel_w_1_HBM_AXI_AWSIZE),
        .HBM_AXI_awvalid(design_hbm_channel_w_1_HBM_AXI_AWVALID),
        .HBM_AXI_bid(design_hbm_channel_w_1_HBM_AXI_BID),
        .HBM_AXI_bready(design_hbm_channel_w_1_HBM_AXI_BREADY),
        .HBM_AXI_bresp(design_hbm_channel_w_1_HBM_AXI_BRESP),
        .HBM_AXI_bvalid(design_hbm_channel_w_1_HBM_AXI_BVALID),
        .HBM_AXI_rdata(design_hbm_channel_w_1_HBM_AXI_RDATA),
        .HBM_AXI_rid(design_hbm_channel_w_1_HBM_AXI_RID),
        .HBM_AXI_rlast(design_hbm_channel_w_1_HBM_AXI_RLAST),
        .HBM_AXI_rready(design_hbm_channel_w_1_HBM_AXI_RREADY),
        .HBM_AXI_rresp(design_hbm_channel_w_1_HBM_AXI_RRESP),
        .HBM_AXI_rvalid(design_hbm_channel_w_1_HBM_AXI_RVALID),
        .HBM_AXI_wdata(design_hbm_channel_w_1_HBM_AXI_WDATA),
        .HBM_AXI_wlast(design_hbm_channel_w_1_HBM_AXI_WLAST),
        .HBM_AXI_wready(design_hbm_channel_w_1_HBM_AXI_WREADY),
        .HBM_AXI_wstrb(design_hbm_channel_w_1_HBM_AXI_WSTRB),
        .HBM_AXI_wvalid(design_hbm_channel_w_1_HBM_AXI_WVALID),
        .HBM_READ_tdata(design_hbm_channel_w_1_HBM_READ_TDATA),
        .HBM_READ_tkeep(design_hbm_channel_w_1_HBM_READ_TKEEP),
        .HBM_READ_tlast(design_hbm_channel_w_1_HBM_READ_TLAST),
        .HBM_READ_tready(design_hbm_channel_w_1_HBM_READ_TREADY),
        .HBM_READ_tvalid(design_hbm_channel_w_1_HBM_READ_TVALID),
        .HBM_WRITE_tdata(hbmlane_mux31_0_s1_HBM_WRITE_TDATA),
        .HBM_WRITE_tready(hbmlane_mux31_0_s1_HBM_WRITE_TREADY),
        .HBM_WRITE_tvalid(hbmlane_mux31_0_s1_HBM_WRITE_TVALID),
        .clk_312(Net2),
        .clk_450(clk_wiz_1_clk_out1),
        .rd_running(hbmlane_mux31_0_s1_ctrl_ports_rd_run),
        .user_hbm_channel_ctrl_ports_busy(design_hbm_channel_w_1_busy),
        .user_hbm_channel_ctrl_ports_rd_blockamount(hbmlane_mux31_0_s1_ctrl_ports_rd_blockamount),
        .user_hbm_channel_ctrl_ports_rd_startaddress(hbmlane_mux31_0_s1_ctrl_ports_rd_startaddress),
        .user_hbm_channel_ctrl_ports_wr_blockamount(hbmlane_mux31_0_s1_ctrl_ports_wr_blockamount),
        .user_hbm_channel_ctrl_ports_wr_startaddress(hbmlane_mux31_0_s1_ctrl_ports_wr_startaddress),
        .wr_running(hbmlane_mux31_0_s1_ctrl_ports_wr_run));
  design_hbm_design_hbm_channel_w_0_2 design_hbm_channel_w_2
       (.HBM_AXI_araddr(design_hbm_channel_w_2_HBM_AXI_ARADDR),
        .HBM_AXI_arburst(design_hbm_channel_w_2_HBM_AXI_ARBURST),
        .HBM_AXI_arid(design_hbm_channel_w_2_HBM_AXI_ARID),
        .HBM_AXI_arlen(design_hbm_channel_w_2_HBM_AXI_ARLEN),
        .HBM_AXI_arready(design_hbm_channel_w_2_HBM_AXI_ARREADY),
        .HBM_AXI_arsize(design_hbm_channel_w_2_HBM_AXI_ARSIZE),
        .HBM_AXI_arvalid(design_hbm_channel_w_2_HBM_AXI_ARVALID),
        .HBM_AXI_awaddr(design_hbm_channel_w_2_HBM_AXI_AWADDR),
        .HBM_AXI_awburst(design_hbm_channel_w_2_HBM_AXI_AWBURST),
        .HBM_AXI_awid(design_hbm_channel_w_2_HBM_AXI_AWID),
        .HBM_AXI_awlen(design_hbm_channel_w_2_HBM_AXI_AWLEN),
        .HBM_AXI_awready(design_hbm_channel_w_2_HBM_AXI_AWREADY),
        .HBM_AXI_awsize(design_hbm_channel_w_2_HBM_AXI_AWSIZE),
        .HBM_AXI_awvalid(design_hbm_channel_w_2_HBM_AXI_AWVALID),
        .HBM_AXI_bid(design_hbm_channel_w_2_HBM_AXI_BID),
        .HBM_AXI_bready(design_hbm_channel_w_2_HBM_AXI_BREADY),
        .HBM_AXI_bresp(design_hbm_channel_w_2_HBM_AXI_BRESP),
        .HBM_AXI_bvalid(design_hbm_channel_w_2_HBM_AXI_BVALID),
        .HBM_AXI_rdata(design_hbm_channel_w_2_HBM_AXI_RDATA),
        .HBM_AXI_rid(design_hbm_channel_w_2_HBM_AXI_RID),
        .HBM_AXI_rlast(design_hbm_channel_w_2_HBM_AXI_RLAST),
        .HBM_AXI_rready(design_hbm_channel_w_2_HBM_AXI_RREADY),
        .HBM_AXI_rresp(design_hbm_channel_w_2_HBM_AXI_RRESP),
        .HBM_AXI_rvalid(design_hbm_channel_w_2_HBM_AXI_RVALID),
        .HBM_AXI_wdata(design_hbm_channel_w_2_HBM_AXI_WDATA),
        .HBM_AXI_wlast(design_hbm_channel_w_2_HBM_AXI_WLAST),
        .HBM_AXI_wready(design_hbm_channel_w_2_HBM_AXI_WREADY),
        .HBM_AXI_wstrb(design_hbm_channel_w_2_HBM_AXI_WSTRB),
        .HBM_AXI_wvalid(design_hbm_channel_w_2_HBM_AXI_WVALID),
        .HBM_READ_tdata(design_hbm_channel_w_2_HBM_READ_TDATA),
        .HBM_READ_tkeep(design_hbm_channel_w_2_HBM_READ_TKEEP),
        .HBM_READ_tlast(design_hbm_channel_w_2_HBM_READ_TLAST),
        .HBM_READ_tready(design_hbm_channel_w_2_HBM_READ_TREADY),
        .HBM_READ_tvalid(design_hbm_channel_w_2_HBM_READ_TVALID),
        .HBM_WRITE_tdata(hbmlane_mux31_0_s2_HBM_WRITE_TDATA),
        .HBM_WRITE_tready(hbmlane_mux31_0_s2_HBM_WRITE_TREADY),
        .HBM_WRITE_tvalid(hbmlane_mux31_0_s2_HBM_WRITE_TVALID),
        .clk_312(Net2),
        .clk_450(clk_wiz_1_clk_out1),
        .rd_running(hbmlane_mux31_0_s2_ctrl_ports_rd_run),
        .user_hbm_channel_ctrl_ports_busy(design_hbm_channel_w_2_busy),
        .user_hbm_channel_ctrl_ports_rd_blockamount(hbmlane_mux31_0_s2_ctrl_ports_rd_blockamount),
        .user_hbm_channel_ctrl_ports_rd_startaddress(hbmlane_mux31_0_s2_ctrl_ports_rd_startaddress),
        .user_hbm_channel_ctrl_ports_wr_blockamount(hbmlane_mux31_0_s2_ctrl_ports_wr_blockamount),
        .user_hbm_channel_ctrl_ports_wr_startaddress(hbmlane_mux31_0_s2_ctrl_ports_wr_startaddress),
        .wr_running(hbmlane_mux31_0_s2_ctrl_ports_wr_run));
  design_hbm_design_hbm_channel_w_0_3 design_hbm_channel_w_3
       (.HBM_AXI_araddr(design_hbm_channel_w_3_HBM_AXI_ARADDR),
        .HBM_AXI_arburst(design_hbm_channel_w_3_HBM_AXI_ARBURST),
        .HBM_AXI_arid(design_hbm_channel_w_3_HBM_AXI_ARID),
        .HBM_AXI_arlen(design_hbm_channel_w_3_HBM_AXI_ARLEN),
        .HBM_AXI_arready(design_hbm_channel_w_3_HBM_AXI_ARREADY),
        .HBM_AXI_arsize(design_hbm_channel_w_3_HBM_AXI_ARSIZE),
        .HBM_AXI_arvalid(design_hbm_channel_w_3_HBM_AXI_ARVALID),
        .HBM_AXI_awaddr(design_hbm_channel_w_3_HBM_AXI_AWADDR),
        .HBM_AXI_awburst(design_hbm_channel_w_3_HBM_AXI_AWBURST),
        .HBM_AXI_awid(design_hbm_channel_w_3_HBM_AXI_AWID),
        .HBM_AXI_awlen(design_hbm_channel_w_3_HBM_AXI_AWLEN),
        .HBM_AXI_awready(design_hbm_channel_w_3_HBM_AXI_AWREADY),
        .HBM_AXI_awsize(design_hbm_channel_w_3_HBM_AXI_AWSIZE),
        .HBM_AXI_awvalid(design_hbm_channel_w_3_HBM_AXI_AWVALID),
        .HBM_AXI_bid(design_hbm_channel_w_3_HBM_AXI_BID),
        .HBM_AXI_bready(design_hbm_channel_w_3_HBM_AXI_BREADY),
        .HBM_AXI_bresp(design_hbm_channel_w_3_HBM_AXI_BRESP),
        .HBM_AXI_bvalid(design_hbm_channel_w_3_HBM_AXI_BVALID),
        .HBM_AXI_rdata(design_hbm_channel_w_3_HBM_AXI_RDATA),
        .HBM_AXI_rid(design_hbm_channel_w_3_HBM_AXI_RID),
        .HBM_AXI_rlast(design_hbm_channel_w_3_HBM_AXI_RLAST),
        .HBM_AXI_rready(design_hbm_channel_w_3_HBM_AXI_RREADY),
        .HBM_AXI_rresp(design_hbm_channel_w_3_HBM_AXI_RRESP),
        .HBM_AXI_rvalid(design_hbm_channel_w_3_HBM_AXI_RVALID),
        .HBM_AXI_wdata(design_hbm_channel_w_3_HBM_AXI_WDATA),
        .HBM_AXI_wlast(design_hbm_channel_w_3_HBM_AXI_WLAST),
        .HBM_AXI_wready(design_hbm_channel_w_3_HBM_AXI_WREADY),
        .HBM_AXI_wstrb(design_hbm_channel_w_3_HBM_AXI_WSTRB),
        .HBM_AXI_wvalid(design_hbm_channel_w_3_HBM_AXI_WVALID),
        .HBM_READ_tdata(design_hbm_channel_w_3_HBM_READ_TDATA),
        .HBM_READ_tkeep(design_hbm_channel_w_3_HBM_READ_TKEEP),
        .HBM_READ_tlast(design_hbm_channel_w_3_HBM_READ_TLAST),
        .HBM_READ_tready(design_hbm_channel_w_3_HBM_READ_TREADY),
        .HBM_READ_tvalid(design_hbm_channel_w_3_HBM_READ_TVALID),
        .HBM_WRITE_tdata(hbmlane_mux31_0_s3_HBM_WRITE_TDATA),
        .HBM_WRITE_tready(hbmlane_mux31_0_s3_HBM_WRITE_TREADY),
        .HBM_WRITE_tvalid(hbmlane_mux31_0_s3_HBM_WRITE_TVALID),
        .clk_312(Net2),
        .clk_450(clk_wiz_1_clk_out1),
        .rd_running(hbmlane_mux31_0_s3_ctrl_ports_rd_run),
        .user_hbm_channel_ctrl_ports_busy(design_hbm_channel_w_3_busy),
        .user_hbm_channel_ctrl_ports_rd_blockamount(hbmlane_mux31_0_s3_ctrl_ports_rd_blockamount),
        .user_hbm_channel_ctrl_ports_rd_startaddress(hbmlane_mux31_0_s3_ctrl_ports_rd_startaddress),
        .user_hbm_channel_ctrl_ports_wr_blockamount(hbmlane_mux31_0_s3_ctrl_ports_wr_blockamount),
        .user_hbm_channel_ctrl_ports_wr_startaddress(hbmlane_mux31_0_s3_ctrl_ports_wr_startaddress),
        .wr_running(hbmlane_mux31_0_s3_ctrl_ports_wr_run));
  design_hbm_design_hbm_channel_w_1_0 design_hbm_channel_w_4
       (.HBM_AXI_araddr(design_hbm_channel_w_4_HBM_AXI_ARADDR),
        .HBM_AXI_arburst(design_hbm_channel_w_4_HBM_AXI_ARBURST),
        .HBM_AXI_arid(design_hbm_channel_w_4_HBM_AXI_ARID),
        .HBM_AXI_arlen(design_hbm_channel_w_4_HBM_AXI_ARLEN),
        .HBM_AXI_arready(design_hbm_channel_w_4_HBM_AXI_ARREADY),
        .HBM_AXI_arsize(design_hbm_channel_w_4_HBM_AXI_ARSIZE),
        .HBM_AXI_arvalid(design_hbm_channel_w_4_HBM_AXI_ARVALID),
        .HBM_AXI_awaddr(design_hbm_channel_w_4_HBM_AXI_AWADDR),
        .HBM_AXI_awburst(design_hbm_channel_w_4_HBM_AXI_AWBURST),
        .HBM_AXI_awid(design_hbm_channel_w_4_HBM_AXI_AWID),
        .HBM_AXI_awlen(design_hbm_channel_w_4_HBM_AXI_AWLEN),
        .HBM_AXI_awready(design_hbm_channel_w_4_HBM_AXI_AWREADY),
        .HBM_AXI_awsize(design_hbm_channel_w_4_HBM_AXI_AWSIZE),
        .HBM_AXI_awvalid(design_hbm_channel_w_4_HBM_AXI_AWVALID),
        .HBM_AXI_bid(design_hbm_channel_w_4_HBM_AXI_BID),
        .HBM_AXI_bready(design_hbm_channel_w_4_HBM_AXI_BREADY),
        .HBM_AXI_bresp(design_hbm_channel_w_4_HBM_AXI_BRESP),
        .HBM_AXI_bvalid(design_hbm_channel_w_4_HBM_AXI_BVALID),
        .HBM_AXI_rdata(design_hbm_channel_w_4_HBM_AXI_RDATA),
        .HBM_AXI_rid(design_hbm_channel_w_4_HBM_AXI_RID),
        .HBM_AXI_rlast(design_hbm_channel_w_4_HBM_AXI_RLAST),
        .HBM_AXI_rready(design_hbm_channel_w_4_HBM_AXI_RREADY),
        .HBM_AXI_rresp(design_hbm_channel_w_4_HBM_AXI_RRESP),
        .HBM_AXI_rvalid(design_hbm_channel_w_4_HBM_AXI_RVALID),
        .HBM_AXI_wdata(design_hbm_channel_w_4_HBM_AXI_WDATA),
        .HBM_AXI_wlast(design_hbm_channel_w_4_HBM_AXI_WLAST),
        .HBM_AXI_wready(design_hbm_channel_w_4_HBM_AXI_WREADY),
        .HBM_AXI_wstrb(design_hbm_channel_w_4_HBM_AXI_WSTRB),
        .HBM_AXI_wvalid(design_hbm_channel_w_4_HBM_AXI_WVALID),
        .HBM_READ_tdata(design_hbm_channel_w_4_HBM_READ_TDATA),
        .HBM_READ_tkeep(design_hbm_channel_w_4_HBM_READ_TKEEP),
        .HBM_READ_tlast(design_hbm_channel_w_4_HBM_READ_TLAST),
        .HBM_READ_tready(design_hbm_channel_w_4_HBM_READ_TREADY),
        .HBM_READ_tvalid(design_hbm_channel_w_4_HBM_READ_TVALID),
        .HBM_WRITE_tdata(hbmlane_mux31_1_s1_HBM_WRITE_TDATA),
        .HBM_WRITE_tready(hbmlane_mux31_1_s1_HBM_WRITE_TREADY),
        .HBM_WRITE_tvalid(hbmlane_mux31_1_s1_HBM_WRITE_TVALID),
        .clk_312(Net2),
        .clk_450(clk_wiz_1_clk_out1),
        .rd_running(hbmlane_mux31_1_s1_ctrl_ports_rd_run),
        .user_hbm_channel_ctrl_ports_busy(design_hbm_channel_w_4_busy),
        .user_hbm_channel_ctrl_ports_rd_blockamount(hbmlane_mux31_1_s1_ctrl_ports_rd_blockamount),
        .user_hbm_channel_ctrl_ports_rd_startaddress(hbmlane_mux31_1_s1_ctrl_ports_rd_startaddress),
        .user_hbm_channel_ctrl_ports_wr_blockamount(hbmlane_mux31_1_s1_ctrl_ports_wr_blockamount),
        .user_hbm_channel_ctrl_ports_wr_startaddress(hbmlane_mux31_1_s1_ctrl_ports_wr_startaddress),
        .wr_running(hbmlane_mux31_1_s1_ctrl_ports_wr_run));
  design_hbm_design_hbm_channel_w_2_0 design_hbm_channel_w_5
       (.HBM_AXI_araddr(design_hbm_channel_w_5_HBM_AXI_ARADDR),
        .HBM_AXI_arburst(design_hbm_channel_w_5_HBM_AXI_ARBURST),
        .HBM_AXI_arid(design_hbm_channel_w_5_HBM_AXI_ARID),
        .HBM_AXI_arlen(design_hbm_channel_w_5_HBM_AXI_ARLEN),
        .HBM_AXI_arready(design_hbm_channel_w_5_HBM_AXI_ARREADY),
        .HBM_AXI_arsize(design_hbm_channel_w_5_HBM_AXI_ARSIZE),
        .HBM_AXI_arvalid(design_hbm_channel_w_5_HBM_AXI_ARVALID),
        .HBM_AXI_awaddr(design_hbm_channel_w_5_HBM_AXI_AWADDR),
        .HBM_AXI_awburst(design_hbm_channel_w_5_HBM_AXI_AWBURST),
        .HBM_AXI_awid(design_hbm_channel_w_5_HBM_AXI_AWID),
        .HBM_AXI_awlen(design_hbm_channel_w_5_HBM_AXI_AWLEN),
        .HBM_AXI_awready(design_hbm_channel_w_5_HBM_AXI_AWREADY),
        .HBM_AXI_awsize(design_hbm_channel_w_5_HBM_AXI_AWSIZE),
        .HBM_AXI_awvalid(design_hbm_channel_w_5_HBM_AXI_AWVALID),
        .HBM_AXI_bid(design_hbm_channel_w_5_HBM_AXI_BID),
        .HBM_AXI_bready(design_hbm_channel_w_5_HBM_AXI_BREADY),
        .HBM_AXI_bresp(design_hbm_channel_w_5_HBM_AXI_BRESP),
        .HBM_AXI_bvalid(design_hbm_channel_w_5_HBM_AXI_BVALID),
        .HBM_AXI_rdata(design_hbm_channel_w_5_HBM_AXI_RDATA),
        .HBM_AXI_rid(design_hbm_channel_w_5_HBM_AXI_RID),
        .HBM_AXI_rlast(design_hbm_channel_w_5_HBM_AXI_RLAST),
        .HBM_AXI_rready(design_hbm_channel_w_5_HBM_AXI_RREADY),
        .HBM_AXI_rresp(design_hbm_channel_w_5_HBM_AXI_RRESP),
        .HBM_AXI_rvalid(design_hbm_channel_w_5_HBM_AXI_RVALID),
        .HBM_AXI_wdata(design_hbm_channel_w_5_HBM_AXI_WDATA),
        .HBM_AXI_wlast(design_hbm_channel_w_5_HBM_AXI_WLAST),
        .HBM_AXI_wready(design_hbm_channel_w_5_HBM_AXI_WREADY),
        .HBM_AXI_wstrb(design_hbm_channel_w_5_HBM_AXI_WSTRB),
        .HBM_AXI_wvalid(design_hbm_channel_w_5_HBM_AXI_WVALID),
        .HBM_READ_tdata(design_hbm_channel_w_5_HBM_READ_TDATA),
        .HBM_READ_tkeep(design_hbm_channel_w_5_HBM_READ_TKEEP),
        .HBM_READ_tlast(design_hbm_channel_w_5_HBM_READ_TLAST),
        .HBM_READ_tready(design_hbm_channel_w_5_HBM_READ_TREADY),
        .HBM_READ_tvalid(design_hbm_channel_w_5_HBM_READ_TVALID),
        .HBM_WRITE_tdata(hbmlane_mux31_1_s2_HBM_WRITE_TDATA),
        .HBM_WRITE_tready(hbmlane_mux31_1_s2_HBM_WRITE_TREADY),
        .HBM_WRITE_tvalid(hbmlane_mux31_1_s2_HBM_WRITE_TVALID),
        .clk_312(Net2),
        .clk_450(clk_wiz_1_clk_out1),
        .rd_running(hbmlane_mux31_1_s2_ctrl_ports_rd_run),
        .user_hbm_channel_ctrl_ports_busy(design_hbm_channel_w_5_busy),
        .user_hbm_channel_ctrl_ports_rd_blockamount(hbmlane_mux31_1_s2_ctrl_ports_rd_blockamount),
        .user_hbm_channel_ctrl_ports_rd_startaddress(hbmlane_mux31_1_s2_ctrl_ports_rd_startaddress),
        .user_hbm_channel_ctrl_ports_wr_blockamount(hbmlane_mux31_1_s2_ctrl_ports_wr_blockamount),
        .user_hbm_channel_ctrl_ports_wr_startaddress(hbmlane_mux31_1_s2_ctrl_ports_wr_startaddress),
        .wr_running(hbmlane_mux31_1_s2_ctrl_ports_wr_run));
  design_hbm_design_hbm_channel_w_0_4 design_hbm_channel_w_6
       (.HBM_AXI_araddr(design_hbm_channel_w_6_HBM_AXI_ARADDR),
        .HBM_AXI_arburst(design_hbm_channel_w_6_HBM_AXI_ARBURST),
        .HBM_AXI_arid(design_hbm_channel_w_6_HBM_AXI_ARID),
        .HBM_AXI_arlen(design_hbm_channel_w_6_HBM_AXI_ARLEN),
        .HBM_AXI_arready(design_hbm_channel_w_6_HBM_AXI_ARREADY),
        .HBM_AXI_arsize(design_hbm_channel_w_6_HBM_AXI_ARSIZE),
        .HBM_AXI_arvalid(design_hbm_channel_w_6_HBM_AXI_ARVALID),
        .HBM_AXI_awaddr(design_hbm_channel_w_6_HBM_AXI_AWADDR),
        .HBM_AXI_awburst(design_hbm_channel_w_6_HBM_AXI_AWBURST),
        .HBM_AXI_awid(design_hbm_channel_w_6_HBM_AXI_AWID),
        .HBM_AXI_awlen(design_hbm_channel_w_6_HBM_AXI_AWLEN),
        .HBM_AXI_awready(design_hbm_channel_w_6_HBM_AXI_AWREADY),
        .HBM_AXI_awsize(design_hbm_channel_w_6_HBM_AXI_AWSIZE),
        .HBM_AXI_awvalid(design_hbm_channel_w_6_HBM_AXI_AWVALID),
        .HBM_AXI_bid(design_hbm_channel_w_6_HBM_AXI_BID),
        .HBM_AXI_bready(design_hbm_channel_w_6_HBM_AXI_BREADY),
        .HBM_AXI_bresp(design_hbm_channel_w_6_HBM_AXI_BRESP),
        .HBM_AXI_bvalid(design_hbm_channel_w_6_HBM_AXI_BVALID),
        .HBM_AXI_rdata(design_hbm_channel_w_6_HBM_AXI_RDATA),
        .HBM_AXI_rid(design_hbm_channel_w_6_HBM_AXI_RID),
        .HBM_AXI_rlast(design_hbm_channel_w_6_HBM_AXI_RLAST),
        .HBM_AXI_rready(design_hbm_channel_w_6_HBM_AXI_RREADY),
        .HBM_AXI_rresp(design_hbm_channel_w_6_HBM_AXI_RRESP),
        .HBM_AXI_rvalid(design_hbm_channel_w_6_HBM_AXI_RVALID),
        .HBM_AXI_wdata(design_hbm_channel_w_6_HBM_AXI_WDATA),
        .HBM_AXI_wlast(design_hbm_channel_w_6_HBM_AXI_WLAST),
        .HBM_AXI_wready(design_hbm_channel_w_6_HBM_AXI_WREADY),
        .HBM_AXI_wstrb(design_hbm_channel_w_6_HBM_AXI_WSTRB),
        .HBM_AXI_wvalid(design_hbm_channel_w_6_HBM_AXI_WVALID),
        .HBM_READ_tdata(design_hbm_channel_w_6_HBM_READ_TDATA),
        .HBM_READ_tkeep(design_hbm_channel_w_6_HBM_READ_TKEEP),
        .HBM_READ_tlast(design_hbm_channel_w_6_HBM_READ_TLAST),
        .HBM_READ_tready(design_hbm_channel_w_6_HBM_READ_TREADY),
        .HBM_READ_tvalid(design_hbm_channel_w_6_HBM_READ_TVALID),
        .HBM_WRITE_tdata(hbmlane_mux31_1_s3_HBM_WRITE_TDATA),
        .HBM_WRITE_tready(hbmlane_mux31_1_s3_HBM_WRITE_TREADY),
        .HBM_WRITE_tvalid(hbmlane_mux31_1_s3_HBM_WRITE_TVALID),
        .clk_312(Net2),
        .clk_450(clk_wiz_1_clk_out1),
        .rd_running(hbmlane_mux31_1_s3_ctrl_ports_rd_run),
        .user_hbm_channel_ctrl_ports_busy(design_hbm_channel_w_6_busy),
        .user_hbm_channel_ctrl_ports_rd_blockamount(hbmlane_mux31_1_s3_ctrl_ports_rd_blockamount),
        .user_hbm_channel_ctrl_ports_rd_startaddress(hbmlane_mux31_1_s3_ctrl_ports_rd_startaddress),
        .user_hbm_channel_ctrl_ports_wr_blockamount(hbmlane_mux31_1_s3_ctrl_ports_wr_blockamount),
        .user_hbm_channel_ctrl_ports_wr_startaddress(hbmlane_mux31_1_s3_ctrl_ports_wr_startaddress),
        .wr_running(hbmlane_mux31_1_s3_ctrl_ports_wr_run));
  design_hbm_design_hbm_channel_w_1_1 design_hbm_channel_w_7
       (.HBM_AXI_araddr(design_hbm_channel_w_7_HBM_AXI_ARADDR),
        .HBM_AXI_arburst(design_hbm_channel_w_7_HBM_AXI_ARBURST),
        .HBM_AXI_arid(design_hbm_channel_w_7_HBM_AXI_ARID),
        .HBM_AXI_arlen(design_hbm_channel_w_7_HBM_AXI_ARLEN),
        .HBM_AXI_arready(design_hbm_channel_w_7_HBM_AXI_ARREADY),
        .HBM_AXI_arsize(design_hbm_channel_w_7_HBM_AXI_ARSIZE),
        .HBM_AXI_arvalid(design_hbm_channel_w_7_HBM_AXI_ARVALID),
        .HBM_AXI_awaddr(design_hbm_channel_w_7_HBM_AXI_AWADDR),
        .HBM_AXI_awburst(design_hbm_channel_w_7_HBM_AXI_AWBURST),
        .HBM_AXI_awid(design_hbm_channel_w_7_HBM_AXI_AWID),
        .HBM_AXI_awlen(design_hbm_channel_w_7_HBM_AXI_AWLEN),
        .HBM_AXI_awready(design_hbm_channel_w_7_HBM_AXI_AWREADY),
        .HBM_AXI_awsize(design_hbm_channel_w_7_HBM_AXI_AWSIZE),
        .HBM_AXI_awvalid(design_hbm_channel_w_7_HBM_AXI_AWVALID),
        .HBM_AXI_bid(design_hbm_channel_w_7_HBM_AXI_BID),
        .HBM_AXI_bready(design_hbm_channel_w_7_HBM_AXI_BREADY),
        .HBM_AXI_bresp(design_hbm_channel_w_7_HBM_AXI_BRESP),
        .HBM_AXI_bvalid(design_hbm_channel_w_7_HBM_AXI_BVALID),
        .HBM_AXI_rdata(design_hbm_channel_w_7_HBM_AXI_RDATA),
        .HBM_AXI_rid(design_hbm_channel_w_7_HBM_AXI_RID),
        .HBM_AXI_rlast(design_hbm_channel_w_7_HBM_AXI_RLAST),
        .HBM_AXI_rready(design_hbm_channel_w_7_HBM_AXI_RREADY),
        .HBM_AXI_rresp(design_hbm_channel_w_7_HBM_AXI_RRESP),
        .HBM_AXI_rvalid(design_hbm_channel_w_7_HBM_AXI_RVALID),
        .HBM_AXI_wdata(design_hbm_channel_w_7_HBM_AXI_WDATA),
        .HBM_AXI_wlast(design_hbm_channel_w_7_HBM_AXI_WLAST),
        .HBM_AXI_wready(design_hbm_channel_w_7_HBM_AXI_WREADY),
        .HBM_AXI_wstrb(design_hbm_channel_w_7_HBM_AXI_WSTRB),
        .HBM_AXI_wvalid(design_hbm_channel_w_7_HBM_AXI_WVALID),
        .HBM_READ_tdata(design_hbm_channel_w_7_HBM_READ_TDATA),
        .HBM_READ_tkeep(design_hbm_channel_w_7_HBM_READ_TKEEP),
        .HBM_READ_tlast(design_hbm_channel_w_7_HBM_READ_TLAST),
        .HBM_READ_tready(design_hbm_channel_w_7_HBM_READ_TREADY),
        .HBM_READ_tvalid(design_hbm_channel_w_7_HBM_READ_TVALID),
        .HBM_WRITE_tdata(hbmlane_mux31_2_s1_HBM_WRITE_TDATA),
        .HBM_WRITE_tready(hbmlane_mux31_2_s1_HBM_WRITE_TREADY),
        .HBM_WRITE_tvalid(hbmlane_mux31_2_s1_HBM_WRITE_TVALID),
        .clk_312(Net2),
        .clk_450(clk_wiz_1_clk_out1),
        .rd_running(hbmlane_mux31_2_s1_ctrl_ports_rd_run),
        .user_hbm_channel_ctrl_ports_busy(design_hbm_channel_w_7_busy),
        .user_hbm_channel_ctrl_ports_rd_blockamount(hbmlane_mux31_2_s1_ctrl_ports_rd_blockamount),
        .user_hbm_channel_ctrl_ports_rd_startaddress(hbmlane_mux31_2_s1_ctrl_ports_rd_startaddress),
        .user_hbm_channel_ctrl_ports_wr_blockamount(hbmlane_mux31_2_s1_ctrl_ports_wr_blockamount),
        .user_hbm_channel_ctrl_ports_wr_startaddress(hbmlane_mux31_2_s1_ctrl_ports_wr_startaddress),
        .wr_running(hbmlane_mux31_2_s1_ctrl_ports_wr_run));
  design_hbm_design_hbm_channel_w_2_1 design_hbm_channel_w_8
       (.HBM_AXI_araddr(design_hbm_channel_w_8_HBM_AXI_ARADDR),
        .HBM_AXI_arburst(design_hbm_channel_w_8_HBM_AXI_ARBURST),
        .HBM_AXI_arid(design_hbm_channel_w_8_HBM_AXI_ARID),
        .HBM_AXI_arlen(design_hbm_channel_w_8_HBM_AXI_ARLEN),
        .HBM_AXI_arready(design_hbm_channel_w_8_HBM_AXI_ARREADY),
        .HBM_AXI_arsize(design_hbm_channel_w_8_HBM_AXI_ARSIZE),
        .HBM_AXI_arvalid(design_hbm_channel_w_8_HBM_AXI_ARVALID),
        .HBM_AXI_awaddr(design_hbm_channel_w_8_HBM_AXI_AWADDR),
        .HBM_AXI_awburst(design_hbm_channel_w_8_HBM_AXI_AWBURST),
        .HBM_AXI_awid(design_hbm_channel_w_8_HBM_AXI_AWID),
        .HBM_AXI_awlen(design_hbm_channel_w_8_HBM_AXI_AWLEN),
        .HBM_AXI_awready(design_hbm_channel_w_8_HBM_AXI_AWREADY),
        .HBM_AXI_awsize(design_hbm_channel_w_8_HBM_AXI_AWSIZE),
        .HBM_AXI_awvalid(design_hbm_channel_w_8_HBM_AXI_AWVALID),
        .HBM_AXI_bid(design_hbm_channel_w_8_HBM_AXI_BID),
        .HBM_AXI_bready(design_hbm_channel_w_8_HBM_AXI_BREADY),
        .HBM_AXI_bresp(design_hbm_channel_w_8_HBM_AXI_BRESP),
        .HBM_AXI_bvalid(design_hbm_channel_w_8_HBM_AXI_BVALID),
        .HBM_AXI_rdata(design_hbm_channel_w_8_HBM_AXI_RDATA),
        .HBM_AXI_rid(design_hbm_channel_w_8_HBM_AXI_RID),
        .HBM_AXI_rlast(design_hbm_channel_w_8_HBM_AXI_RLAST),
        .HBM_AXI_rready(design_hbm_channel_w_8_HBM_AXI_RREADY),
        .HBM_AXI_rresp(design_hbm_channel_w_8_HBM_AXI_RRESP),
        .HBM_AXI_rvalid(design_hbm_channel_w_8_HBM_AXI_RVALID),
        .HBM_AXI_wdata(design_hbm_channel_w_8_HBM_AXI_WDATA),
        .HBM_AXI_wlast(design_hbm_channel_w_8_HBM_AXI_WLAST),
        .HBM_AXI_wready(design_hbm_channel_w_8_HBM_AXI_WREADY),
        .HBM_AXI_wstrb(design_hbm_channel_w_8_HBM_AXI_WSTRB),
        .HBM_AXI_wvalid(design_hbm_channel_w_8_HBM_AXI_WVALID),
        .HBM_READ_tdata(design_hbm_channel_w_8_HBM_READ_TDATA),
        .HBM_READ_tkeep(design_hbm_channel_w_8_HBM_READ_TKEEP),
        .HBM_READ_tlast(design_hbm_channel_w_8_HBM_READ_TLAST),
        .HBM_READ_tready(design_hbm_channel_w_8_HBM_READ_TREADY),
        .HBM_READ_tvalid(design_hbm_channel_w_8_HBM_READ_TVALID),
        .HBM_WRITE_tdata(hbmlane_mux31_2_s2_HBM_WRITE_TDATA),
        .HBM_WRITE_tready(hbmlane_mux31_2_s2_HBM_WRITE_TREADY),
        .HBM_WRITE_tvalid(hbmlane_mux31_2_s2_HBM_WRITE_TVALID),
        .clk_312(Net2),
        .clk_450(clk_wiz_1_clk_out1),
        .rd_running(hbmlane_mux31_2_s2_ctrl_ports_rd_run),
        .user_hbm_channel_ctrl_ports_busy(design_hbm_channel_w_8_busy),
        .user_hbm_channel_ctrl_ports_rd_blockamount(hbmlane_mux31_2_s2_ctrl_ports_rd_blockamount),
        .user_hbm_channel_ctrl_ports_rd_startaddress(hbmlane_mux31_2_s2_ctrl_ports_rd_startaddress),
        .user_hbm_channel_ctrl_ports_wr_blockamount(hbmlane_mux31_2_s2_ctrl_ports_wr_blockamount),
        .user_hbm_channel_ctrl_ports_wr_startaddress(hbmlane_mux31_2_s2_ctrl_ports_wr_startaddress),
        .wr_running(hbmlane_mux31_2_s2_ctrl_ports_wr_run));
  design_hbm_design_hbm_channel_w_3_0 design_hbm_channel_w_9
       (.HBM_AXI_araddr(design_hbm_channel_w_9_HBM_AXI_ARADDR),
        .HBM_AXI_arburst(design_hbm_channel_w_9_HBM_AXI_ARBURST),
        .HBM_AXI_arid(design_hbm_channel_w_9_HBM_AXI_ARID),
        .HBM_AXI_arlen(design_hbm_channel_w_9_HBM_AXI_ARLEN),
        .HBM_AXI_arready(design_hbm_channel_w_9_HBM_AXI_ARREADY),
        .HBM_AXI_arsize(design_hbm_channel_w_9_HBM_AXI_ARSIZE),
        .HBM_AXI_arvalid(design_hbm_channel_w_9_HBM_AXI_ARVALID),
        .HBM_AXI_awaddr(design_hbm_channel_w_9_HBM_AXI_AWADDR),
        .HBM_AXI_awburst(design_hbm_channel_w_9_HBM_AXI_AWBURST),
        .HBM_AXI_awid(design_hbm_channel_w_9_HBM_AXI_AWID),
        .HBM_AXI_awlen(design_hbm_channel_w_9_HBM_AXI_AWLEN),
        .HBM_AXI_awready(design_hbm_channel_w_9_HBM_AXI_AWREADY),
        .HBM_AXI_awsize(design_hbm_channel_w_9_HBM_AXI_AWSIZE),
        .HBM_AXI_awvalid(design_hbm_channel_w_9_HBM_AXI_AWVALID),
        .HBM_AXI_bid(design_hbm_channel_w_9_HBM_AXI_BID),
        .HBM_AXI_bready(design_hbm_channel_w_9_HBM_AXI_BREADY),
        .HBM_AXI_bresp(design_hbm_channel_w_9_HBM_AXI_BRESP),
        .HBM_AXI_bvalid(design_hbm_channel_w_9_HBM_AXI_BVALID),
        .HBM_AXI_rdata(design_hbm_channel_w_9_HBM_AXI_RDATA),
        .HBM_AXI_rid(design_hbm_channel_w_9_HBM_AXI_RID),
        .HBM_AXI_rlast(design_hbm_channel_w_9_HBM_AXI_RLAST),
        .HBM_AXI_rready(design_hbm_channel_w_9_HBM_AXI_RREADY),
        .HBM_AXI_rresp(design_hbm_channel_w_9_HBM_AXI_RRESP),
        .HBM_AXI_rvalid(design_hbm_channel_w_9_HBM_AXI_RVALID),
        .HBM_AXI_wdata(design_hbm_channel_w_9_HBM_AXI_WDATA),
        .HBM_AXI_wlast(design_hbm_channel_w_9_HBM_AXI_WLAST),
        .HBM_AXI_wready(design_hbm_channel_w_9_HBM_AXI_WREADY),
        .HBM_AXI_wstrb(design_hbm_channel_w_9_HBM_AXI_WSTRB),
        .HBM_AXI_wvalid(design_hbm_channel_w_9_HBM_AXI_WVALID),
        .HBM_READ_tdata(design_hbm_channel_w_9_HBM_READ_TDATA),
        .HBM_READ_tkeep(design_hbm_channel_w_9_HBM_READ_TKEEP),
        .HBM_READ_tlast(design_hbm_channel_w_9_HBM_READ_TLAST),
        .HBM_READ_tready(design_hbm_channel_w_9_HBM_READ_TREADY),
        .HBM_READ_tvalid(design_hbm_channel_w_9_HBM_READ_TVALID),
        .HBM_WRITE_tdata(hbmlane_mux31_2_s3_HBM_WRITE_TDATA),
        .HBM_WRITE_tready(hbmlane_mux31_2_s3_HBM_WRITE_TREADY),
        .HBM_WRITE_tvalid(hbmlane_mux31_2_s3_HBM_WRITE_TVALID),
        .clk_312(Net2),
        .clk_450(clk_wiz_1_clk_out1),
        .rd_running(hbmlane_mux31_2_s3_ctrl_ports_rd_run),
        .user_hbm_channel_ctrl_ports_busy(design_hbm_channel_w_9_busy),
        .user_hbm_channel_ctrl_ports_rd_blockamount(hbmlane_mux31_2_s3_ctrl_ports_rd_blockamount),
        .user_hbm_channel_ctrl_ports_rd_startaddress(hbmlane_mux31_2_s3_ctrl_ports_rd_startaddress),
        .user_hbm_channel_ctrl_ports_wr_blockamount(hbmlane_mux31_2_s3_ctrl_ports_wr_blockamount),
        .user_hbm_channel_ctrl_ports_wr_startaddress(hbmlane_mux31_2_s3_ctrl_ports_wr_startaddress),
        .wr_running(hbmlane_mux31_2_s3_ctrl_ports_wr_run));
  design_hbm_hbm_0_0 hbm_0
       (.APB_0_PCLK(clk_wiz_clk_out1),
        .APB_0_PRESET_N(Net1),
        .APB_1_PCLK(clk_wiz_clk_out1),
        .APB_1_PRESET_N(Net1),
        .AXI_00_ACLK(clk_wiz_1_clk_out1),
        .AXI_00_ARADDR(design_hbm_channel_w_1_HBM_AXI_ARADDR),
        .AXI_00_ARBURST(design_hbm_channel_w_1_HBM_AXI_ARBURST),
        .AXI_00_ARESET_N(Net),
        .AXI_00_ARID(design_hbm_channel_w_1_HBM_AXI_ARID),
        .AXI_00_ARLEN(design_hbm_channel_w_1_HBM_AXI_ARLEN),
        .AXI_00_ARREADY(design_hbm_channel_w_1_HBM_AXI_ARREADY),
        .AXI_00_ARSIZE(design_hbm_channel_w_1_HBM_AXI_ARSIZE),
        .AXI_00_ARVALID(design_hbm_channel_w_1_HBM_AXI_ARVALID),
        .AXI_00_AWADDR(design_hbm_channel_w_1_HBM_AXI_AWADDR),
        .AXI_00_AWBURST(design_hbm_channel_w_1_HBM_AXI_AWBURST),
        .AXI_00_AWID(design_hbm_channel_w_1_HBM_AXI_AWID),
        .AXI_00_AWLEN(design_hbm_channel_w_1_HBM_AXI_AWLEN),
        .AXI_00_AWREADY(design_hbm_channel_w_1_HBM_AXI_AWREADY),
        .AXI_00_AWSIZE(design_hbm_channel_w_1_HBM_AXI_AWSIZE),
        .AXI_00_AWVALID(design_hbm_channel_w_1_HBM_AXI_AWVALID),
        .AXI_00_BID(design_hbm_channel_w_1_HBM_AXI_BID),
        .AXI_00_BREADY(design_hbm_channel_w_1_HBM_AXI_BREADY),
        .AXI_00_BRESP(design_hbm_channel_w_1_HBM_AXI_BRESP),
        .AXI_00_BVALID(design_hbm_channel_w_1_HBM_AXI_BVALID),
        .AXI_00_RDATA(design_hbm_channel_w_1_HBM_AXI_RDATA),
        .AXI_00_RID(design_hbm_channel_w_1_HBM_AXI_RID),
        .AXI_00_RLAST(design_hbm_channel_w_1_HBM_AXI_RLAST),
        .AXI_00_RREADY(design_hbm_channel_w_1_HBM_AXI_RREADY),
        .AXI_00_RRESP(design_hbm_channel_w_1_HBM_AXI_RRESP),
        .AXI_00_RVALID(design_hbm_channel_w_1_HBM_AXI_RVALID),
        .AXI_00_WDATA(design_hbm_channel_w_1_HBM_AXI_WDATA),
        .AXI_00_WDATA_PARITY({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .AXI_00_WLAST(design_hbm_channel_w_1_HBM_AXI_WLAST),
        .AXI_00_WREADY(design_hbm_channel_w_1_HBM_AXI_WREADY),
        .AXI_00_WSTRB(design_hbm_channel_w_1_HBM_AXI_WSTRB),
        .AXI_00_WVALID(design_hbm_channel_w_1_HBM_AXI_WVALID),
        .AXI_01_ACLK(clk_wiz_1_clk_out1),
        .AXI_01_ARADDR(design_hbm_channel_w_2_HBM_AXI_ARADDR),
        .AXI_01_ARBURST(design_hbm_channel_w_2_HBM_AXI_ARBURST),
        .AXI_01_ARESET_N(Net),
        .AXI_01_ARID(design_hbm_channel_w_2_HBM_AXI_ARID),
        .AXI_01_ARLEN(design_hbm_channel_w_2_HBM_AXI_ARLEN),
        .AXI_01_ARREADY(design_hbm_channel_w_2_HBM_AXI_ARREADY),
        .AXI_01_ARSIZE(design_hbm_channel_w_2_HBM_AXI_ARSIZE),
        .AXI_01_ARVALID(design_hbm_channel_w_2_HBM_AXI_ARVALID),
        .AXI_01_AWADDR(design_hbm_channel_w_2_HBM_AXI_AWADDR),
        .AXI_01_AWBURST(design_hbm_channel_w_2_HBM_AXI_AWBURST),
        .AXI_01_AWID(design_hbm_channel_w_2_HBM_AXI_AWID),
        .AXI_01_AWLEN(design_hbm_channel_w_2_HBM_AXI_AWLEN),
        .AXI_01_AWREADY(design_hbm_channel_w_2_HBM_AXI_AWREADY),
        .AXI_01_AWSIZE(design_hbm_channel_w_2_HBM_AXI_AWSIZE),
        .AXI_01_AWVALID(design_hbm_channel_w_2_HBM_AXI_AWVALID),
        .AXI_01_BID(design_hbm_channel_w_2_HBM_AXI_BID),
        .AXI_01_BREADY(design_hbm_channel_w_2_HBM_AXI_BREADY),
        .AXI_01_BRESP(design_hbm_channel_w_2_HBM_AXI_BRESP),
        .AXI_01_BVALID(design_hbm_channel_w_2_HBM_AXI_BVALID),
        .AXI_01_RDATA(design_hbm_channel_w_2_HBM_AXI_RDATA),
        .AXI_01_RID(design_hbm_channel_w_2_HBM_AXI_RID),
        .AXI_01_RLAST(design_hbm_channel_w_2_HBM_AXI_RLAST),
        .AXI_01_RREADY(design_hbm_channel_w_2_HBM_AXI_RREADY),
        .AXI_01_RRESP(design_hbm_channel_w_2_HBM_AXI_RRESP),
        .AXI_01_RVALID(design_hbm_channel_w_2_HBM_AXI_RVALID),
        .AXI_01_WDATA(design_hbm_channel_w_2_HBM_AXI_WDATA),
        .AXI_01_WDATA_PARITY({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .AXI_01_WLAST(design_hbm_channel_w_2_HBM_AXI_WLAST),
        .AXI_01_WREADY(design_hbm_channel_w_2_HBM_AXI_WREADY),
        .AXI_01_WSTRB(design_hbm_channel_w_2_HBM_AXI_WSTRB),
        .AXI_01_WVALID(design_hbm_channel_w_2_HBM_AXI_WVALID),
        .AXI_02_ACLK(clk_wiz_1_clk_out1),
        .AXI_02_ARADDR(design_hbm_channel_w_3_HBM_AXI_ARADDR),
        .AXI_02_ARBURST(design_hbm_channel_w_3_HBM_AXI_ARBURST),
        .AXI_02_ARESET_N(Net),
        .AXI_02_ARID(design_hbm_channel_w_3_HBM_AXI_ARID),
        .AXI_02_ARLEN(design_hbm_channel_w_3_HBM_AXI_ARLEN),
        .AXI_02_ARREADY(design_hbm_channel_w_3_HBM_AXI_ARREADY),
        .AXI_02_ARSIZE(design_hbm_channel_w_3_HBM_AXI_ARSIZE),
        .AXI_02_ARVALID(design_hbm_channel_w_3_HBM_AXI_ARVALID),
        .AXI_02_AWADDR(design_hbm_channel_w_3_HBM_AXI_AWADDR),
        .AXI_02_AWBURST(design_hbm_channel_w_3_HBM_AXI_AWBURST),
        .AXI_02_AWID(design_hbm_channel_w_3_HBM_AXI_AWID),
        .AXI_02_AWLEN(design_hbm_channel_w_3_HBM_AXI_AWLEN),
        .AXI_02_AWREADY(design_hbm_channel_w_3_HBM_AXI_AWREADY),
        .AXI_02_AWSIZE(design_hbm_channel_w_3_HBM_AXI_AWSIZE),
        .AXI_02_AWVALID(design_hbm_channel_w_3_HBM_AXI_AWVALID),
        .AXI_02_BID(design_hbm_channel_w_3_HBM_AXI_BID),
        .AXI_02_BREADY(design_hbm_channel_w_3_HBM_AXI_BREADY),
        .AXI_02_BRESP(design_hbm_channel_w_3_HBM_AXI_BRESP),
        .AXI_02_BVALID(design_hbm_channel_w_3_HBM_AXI_BVALID),
        .AXI_02_RDATA(design_hbm_channel_w_3_HBM_AXI_RDATA),
        .AXI_02_RID(design_hbm_channel_w_3_HBM_AXI_RID),
        .AXI_02_RLAST(design_hbm_channel_w_3_HBM_AXI_RLAST),
        .AXI_02_RREADY(design_hbm_channel_w_3_HBM_AXI_RREADY),
        .AXI_02_RRESP(design_hbm_channel_w_3_HBM_AXI_RRESP),
        .AXI_02_RVALID(design_hbm_channel_w_3_HBM_AXI_RVALID),
        .AXI_02_WDATA(design_hbm_channel_w_3_HBM_AXI_WDATA),
        .AXI_02_WDATA_PARITY({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .AXI_02_WLAST(design_hbm_channel_w_3_HBM_AXI_WLAST),
        .AXI_02_WREADY(design_hbm_channel_w_3_HBM_AXI_WREADY),
        .AXI_02_WSTRB(design_hbm_channel_w_3_HBM_AXI_WSTRB),
        .AXI_02_WVALID(design_hbm_channel_w_3_HBM_AXI_WVALID),
        .AXI_03_ACLK(clk_wiz_1_clk_out1),
        .AXI_03_ARADDR(design_hbm_channel_w_4_HBM_AXI_ARADDR),
        .AXI_03_ARBURST(design_hbm_channel_w_4_HBM_AXI_ARBURST),
        .AXI_03_ARESET_N(Net),
        .AXI_03_ARID(design_hbm_channel_w_4_HBM_AXI_ARID),
        .AXI_03_ARLEN(design_hbm_channel_w_4_HBM_AXI_ARLEN),
        .AXI_03_ARREADY(design_hbm_channel_w_4_HBM_AXI_ARREADY),
        .AXI_03_ARSIZE(design_hbm_channel_w_4_HBM_AXI_ARSIZE),
        .AXI_03_ARVALID(design_hbm_channel_w_4_HBM_AXI_ARVALID),
        .AXI_03_AWADDR(design_hbm_channel_w_4_HBM_AXI_AWADDR),
        .AXI_03_AWBURST(design_hbm_channel_w_4_HBM_AXI_AWBURST),
        .AXI_03_AWID(design_hbm_channel_w_4_HBM_AXI_AWID),
        .AXI_03_AWLEN(design_hbm_channel_w_4_HBM_AXI_AWLEN),
        .AXI_03_AWREADY(design_hbm_channel_w_4_HBM_AXI_AWREADY),
        .AXI_03_AWSIZE(design_hbm_channel_w_4_HBM_AXI_AWSIZE),
        .AXI_03_AWVALID(design_hbm_channel_w_4_HBM_AXI_AWVALID),
        .AXI_03_BID(design_hbm_channel_w_4_HBM_AXI_BID),
        .AXI_03_BREADY(design_hbm_channel_w_4_HBM_AXI_BREADY),
        .AXI_03_BRESP(design_hbm_channel_w_4_HBM_AXI_BRESP),
        .AXI_03_BVALID(design_hbm_channel_w_4_HBM_AXI_BVALID),
        .AXI_03_RDATA(design_hbm_channel_w_4_HBM_AXI_RDATA),
        .AXI_03_RID(design_hbm_channel_w_4_HBM_AXI_RID),
        .AXI_03_RLAST(design_hbm_channel_w_4_HBM_AXI_RLAST),
        .AXI_03_RREADY(design_hbm_channel_w_4_HBM_AXI_RREADY),
        .AXI_03_RRESP(design_hbm_channel_w_4_HBM_AXI_RRESP),
        .AXI_03_RVALID(design_hbm_channel_w_4_HBM_AXI_RVALID),
        .AXI_03_WDATA(design_hbm_channel_w_4_HBM_AXI_WDATA),
        .AXI_03_WDATA_PARITY({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .AXI_03_WLAST(design_hbm_channel_w_4_HBM_AXI_WLAST),
        .AXI_03_WREADY(design_hbm_channel_w_4_HBM_AXI_WREADY),
        .AXI_03_WSTRB(design_hbm_channel_w_4_HBM_AXI_WSTRB),
        .AXI_03_WVALID(design_hbm_channel_w_4_HBM_AXI_WVALID),
        .AXI_04_ACLK(clk_wiz_1_clk_out1),
        .AXI_04_ARADDR(design_hbm_channel_w_5_HBM_AXI_ARADDR),
        .AXI_04_ARBURST(design_hbm_channel_w_5_HBM_AXI_ARBURST),
        .AXI_04_ARESET_N(Net),
        .AXI_04_ARID(design_hbm_channel_w_5_HBM_AXI_ARID),
        .AXI_04_ARLEN(design_hbm_channel_w_5_HBM_AXI_ARLEN),
        .AXI_04_ARREADY(design_hbm_channel_w_5_HBM_AXI_ARREADY),
        .AXI_04_ARSIZE(design_hbm_channel_w_5_HBM_AXI_ARSIZE),
        .AXI_04_ARVALID(design_hbm_channel_w_5_HBM_AXI_ARVALID),
        .AXI_04_AWADDR(design_hbm_channel_w_5_HBM_AXI_AWADDR),
        .AXI_04_AWBURST(design_hbm_channel_w_5_HBM_AXI_AWBURST),
        .AXI_04_AWID(design_hbm_channel_w_5_HBM_AXI_AWID),
        .AXI_04_AWLEN(design_hbm_channel_w_5_HBM_AXI_AWLEN),
        .AXI_04_AWREADY(design_hbm_channel_w_5_HBM_AXI_AWREADY),
        .AXI_04_AWSIZE(design_hbm_channel_w_5_HBM_AXI_AWSIZE),
        .AXI_04_AWVALID(design_hbm_channel_w_5_HBM_AXI_AWVALID),
        .AXI_04_BID(design_hbm_channel_w_5_HBM_AXI_BID),
        .AXI_04_BREADY(design_hbm_channel_w_5_HBM_AXI_BREADY),
        .AXI_04_BRESP(design_hbm_channel_w_5_HBM_AXI_BRESP),
        .AXI_04_BVALID(design_hbm_channel_w_5_HBM_AXI_BVALID),
        .AXI_04_RDATA(design_hbm_channel_w_5_HBM_AXI_RDATA),
        .AXI_04_RID(design_hbm_channel_w_5_HBM_AXI_RID),
        .AXI_04_RLAST(design_hbm_channel_w_5_HBM_AXI_RLAST),
        .AXI_04_RREADY(design_hbm_channel_w_5_HBM_AXI_RREADY),
        .AXI_04_RRESP(design_hbm_channel_w_5_HBM_AXI_RRESP),
        .AXI_04_RVALID(design_hbm_channel_w_5_HBM_AXI_RVALID),
        .AXI_04_WDATA(design_hbm_channel_w_5_HBM_AXI_WDATA),
        .AXI_04_WDATA_PARITY({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .AXI_04_WLAST(design_hbm_channel_w_5_HBM_AXI_WLAST),
        .AXI_04_WREADY(design_hbm_channel_w_5_HBM_AXI_WREADY),
        .AXI_04_WSTRB(design_hbm_channel_w_5_HBM_AXI_WSTRB),
        .AXI_04_WVALID(design_hbm_channel_w_5_HBM_AXI_WVALID),
        .AXI_05_ACLK(clk_wiz_1_clk_out1),
        .AXI_05_ARADDR(design_hbm_channel_w_6_HBM_AXI_ARADDR),
        .AXI_05_ARBURST(design_hbm_channel_w_6_HBM_AXI_ARBURST),
        .AXI_05_ARESET_N(Net),
        .AXI_05_ARID(design_hbm_channel_w_6_HBM_AXI_ARID),
        .AXI_05_ARLEN(design_hbm_channel_w_6_HBM_AXI_ARLEN),
        .AXI_05_ARREADY(design_hbm_channel_w_6_HBM_AXI_ARREADY),
        .AXI_05_ARSIZE(design_hbm_channel_w_6_HBM_AXI_ARSIZE),
        .AXI_05_ARVALID(design_hbm_channel_w_6_HBM_AXI_ARVALID),
        .AXI_05_AWADDR(design_hbm_channel_w_6_HBM_AXI_AWADDR),
        .AXI_05_AWBURST(design_hbm_channel_w_6_HBM_AXI_AWBURST),
        .AXI_05_AWID(design_hbm_channel_w_6_HBM_AXI_AWID),
        .AXI_05_AWLEN(design_hbm_channel_w_6_HBM_AXI_AWLEN),
        .AXI_05_AWREADY(design_hbm_channel_w_6_HBM_AXI_AWREADY),
        .AXI_05_AWSIZE(design_hbm_channel_w_6_HBM_AXI_AWSIZE),
        .AXI_05_AWVALID(design_hbm_channel_w_6_HBM_AXI_AWVALID),
        .AXI_05_BID(design_hbm_channel_w_6_HBM_AXI_BID),
        .AXI_05_BREADY(design_hbm_channel_w_6_HBM_AXI_BREADY),
        .AXI_05_BRESP(design_hbm_channel_w_6_HBM_AXI_BRESP),
        .AXI_05_BVALID(design_hbm_channel_w_6_HBM_AXI_BVALID),
        .AXI_05_RDATA(design_hbm_channel_w_6_HBM_AXI_RDATA),
        .AXI_05_RID(design_hbm_channel_w_6_HBM_AXI_RID),
        .AXI_05_RLAST(design_hbm_channel_w_6_HBM_AXI_RLAST),
        .AXI_05_RREADY(design_hbm_channel_w_6_HBM_AXI_RREADY),
        .AXI_05_RRESP(design_hbm_channel_w_6_HBM_AXI_RRESP),
        .AXI_05_RVALID(design_hbm_channel_w_6_HBM_AXI_RVALID),
        .AXI_05_WDATA(design_hbm_channel_w_6_HBM_AXI_WDATA),
        .AXI_05_WDATA_PARITY({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .AXI_05_WLAST(design_hbm_channel_w_6_HBM_AXI_WLAST),
        .AXI_05_WREADY(design_hbm_channel_w_6_HBM_AXI_WREADY),
        .AXI_05_WSTRB(design_hbm_channel_w_6_HBM_AXI_WSTRB),
        .AXI_05_WVALID(design_hbm_channel_w_6_HBM_AXI_WVALID),
        .AXI_06_ACLK(clk_wiz_1_clk_out1),
        .AXI_06_ARADDR(design_hbm_channel_w_7_HBM_AXI_ARADDR),
        .AXI_06_ARBURST(design_hbm_channel_w_7_HBM_AXI_ARBURST),
        .AXI_06_ARESET_N(Net),
        .AXI_06_ARID(design_hbm_channel_w_7_HBM_AXI_ARID),
        .AXI_06_ARLEN(design_hbm_channel_w_7_HBM_AXI_ARLEN),
        .AXI_06_ARREADY(design_hbm_channel_w_7_HBM_AXI_ARREADY),
        .AXI_06_ARSIZE(design_hbm_channel_w_7_HBM_AXI_ARSIZE),
        .AXI_06_ARVALID(design_hbm_channel_w_7_HBM_AXI_ARVALID),
        .AXI_06_AWADDR(design_hbm_channel_w_7_HBM_AXI_AWADDR),
        .AXI_06_AWBURST(design_hbm_channel_w_7_HBM_AXI_AWBURST),
        .AXI_06_AWID(design_hbm_channel_w_7_HBM_AXI_AWID),
        .AXI_06_AWLEN(design_hbm_channel_w_7_HBM_AXI_AWLEN),
        .AXI_06_AWREADY(design_hbm_channel_w_7_HBM_AXI_AWREADY),
        .AXI_06_AWSIZE(design_hbm_channel_w_7_HBM_AXI_AWSIZE),
        .AXI_06_AWVALID(design_hbm_channel_w_7_HBM_AXI_AWVALID),
        .AXI_06_BID(design_hbm_channel_w_7_HBM_AXI_BID),
        .AXI_06_BREADY(design_hbm_channel_w_7_HBM_AXI_BREADY),
        .AXI_06_BRESP(design_hbm_channel_w_7_HBM_AXI_BRESP),
        .AXI_06_BVALID(design_hbm_channel_w_7_HBM_AXI_BVALID),
        .AXI_06_RDATA(design_hbm_channel_w_7_HBM_AXI_RDATA),
        .AXI_06_RID(design_hbm_channel_w_7_HBM_AXI_RID),
        .AXI_06_RLAST(design_hbm_channel_w_7_HBM_AXI_RLAST),
        .AXI_06_RREADY(design_hbm_channel_w_7_HBM_AXI_RREADY),
        .AXI_06_RRESP(design_hbm_channel_w_7_HBM_AXI_RRESP),
        .AXI_06_RVALID(design_hbm_channel_w_7_HBM_AXI_RVALID),
        .AXI_06_WDATA(design_hbm_channel_w_7_HBM_AXI_WDATA),
        .AXI_06_WDATA_PARITY({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .AXI_06_WLAST(design_hbm_channel_w_7_HBM_AXI_WLAST),
        .AXI_06_WREADY(design_hbm_channel_w_7_HBM_AXI_WREADY),
        .AXI_06_WSTRB(design_hbm_channel_w_7_HBM_AXI_WSTRB),
        .AXI_06_WVALID(design_hbm_channel_w_7_HBM_AXI_WVALID),
        .AXI_07_ACLK(clk_wiz_1_clk_out1),
        .AXI_07_ARADDR(design_hbm_channel_w_8_HBM_AXI_ARADDR),
        .AXI_07_ARBURST(design_hbm_channel_w_8_HBM_AXI_ARBURST),
        .AXI_07_ARESET_N(Net),
        .AXI_07_ARID(design_hbm_channel_w_8_HBM_AXI_ARID),
        .AXI_07_ARLEN(design_hbm_channel_w_8_HBM_AXI_ARLEN),
        .AXI_07_ARREADY(design_hbm_channel_w_8_HBM_AXI_ARREADY),
        .AXI_07_ARSIZE(design_hbm_channel_w_8_HBM_AXI_ARSIZE),
        .AXI_07_ARVALID(design_hbm_channel_w_8_HBM_AXI_ARVALID),
        .AXI_07_AWADDR(design_hbm_channel_w_8_HBM_AXI_AWADDR),
        .AXI_07_AWBURST(design_hbm_channel_w_8_HBM_AXI_AWBURST),
        .AXI_07_AWID(design_hbm_channel_w_8_HBM_AXI_AWID),
        .AXI_07_AWLEN(design_hbm_channel_w_8_HBM_AXI_AWLEN),
        .AXI_07_AWREADY(design_hbm_channel_w_8_HBM_AXI_AWREADY),
        .AXI_07_AWSIZE(design_hbm_channel_w_8_HBM_AXI_AWSIZE),
        .AXI_07_AWVALID(design_hbm_channel_w_8_HBM_AXI_AWVALID),
        .AXI_07_BID(design_hbm_channel_w_8_HBM_AXI_BID),
        .AXI_07_BREADY(design_hbm_channel_w_8_HBM_AXI_BREADY),
        .AXI_07_BRESP(design_hbm_channel_w_8_HBM_AXI_BRESP),
        .AXI_07_BVALID(design_hbm_channel_w_8_HBM_AXI_BVALID),
        .AXI_07_RDATA(design_hbm_channel_w_8_HBM_AXI_RDATA),
        .AXI_07_RID(design_hbm_channel_w_8_HBM_AXI_RID),
        .AXI_07_RLAST(design_hbm_channel_w_8_HBM_AXI_RLAST),
        .AXI_07_RREADY(design_hbm_channel_w_8_HBM_AXI_RREADY),
        .AXI_07_RRESP(design_hbm_channel_w_8_HBM_AXI_RRESP),
        .AXI_07_RVALID(design_hbm_channel_w_8_HBM_AXI_RVALID),
        .AXI_07_WDATA(design_hbm_channel_w_8_HBM_AXI_WDATA),
        .AXI_07_WDATA_PARITY({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .AXI_07_WLAST(design_hbm_channel_w_8_HBM_AXI_WLAST),
        .AXI_07_WREADY(design_hbm_channel_w_8_HBM_AXI_WREADY),
        .AXI_07_WSTRB(design_hbm_channel_w_8_HBM_AXI_WSTRB),
        .AXI_07_WVALID(design_hbm_channel_w_8_HBM_AXI_WVALID),
        .AXI_08_ACLK(clk_wiz_1_clk_out1),
        .AXI_08_ARADDR(design_hbm_channel_w_9_HBM_AXI_ARADDR),
        .AXI_08_ARBURST(design_hbm_channel_w_9_HBM_AXI_ARBURST),
        .AXI_08_ARESET_N(Net),
        .AXI_08_ARID(design_hbm_channel_w_9_HBM_AXI_ARID),
        .AXI_08_ARLEN(design_hbm_channel_w_9_HBM_AXI_ARLEN),
        .AXI_08_ARREADY(design_hbm_channel_w_9_HBM_AXI_ARREADY),
        .AXI_08_ARSIZE(design_hbm_channel_w_9_HBM_AXI_ARSIZE),
        .AXI_08_ARVALID(design_hbm_channel_w_9_HBM_AXI_ARVALID),
        .AXI_08_AWADDR(design_hbm_channel_w_9_HBM_AXI_AWADDR),
        .AXI_08_AWBURST(design_hbm_channel_w_9_HBM_AXI_AWBURST),
        .AXI_08_AWID(design_hbm_channel_w_9_HBM_AXI_AWID),
        .AXI_08_AWLEN(design_hbm_channel_w_9_HBM_AXI_AWLEN),
        .AXI_08_AWREADY(design_hbm_channel_w_9_HBM_AXI_AWREADY),
        .AXI_08_AWSIZE(design_hbm_channel_w_9_HBM_AXI_AWSIZE),
        .AXI_08_AWVALID(design_hbm_channel_w_9_HBM_AXI_AWVALID),
        .AXI_08_BID(design_hbm_channel_w_9_HBM_AXI_BID),
        .AXI_08_BREADY(design_hbm_channel_w_9_HBM_AXI_BREADY),
        .AXI_08_BRESP(design_hbm_channel_w_9_HBM_AXI_BRESP),
        .AXI_08_BVALID(design_hbm_channel_w_9_HBM_AXI_BVALID),
        .AXI_08_RDATA(design_hbm_channel_w_9_HBM_AXI_RDATA),
        .AXI_08_RID(design_hbm_channel_w_9_HBM_AXI_RID),
        .AXI_08_RLAST(design_hbm_channel_w_9_HBM_AXI_RLAST),
        .AXI_08_RREADY(design_hbm_channel_w_9_HBM_AXI_RREADY),
        .AXI_08_RRESP(design_hbm_channel_w_9_HBM_AXI_RRESP),
        .AXI_08_RVALID(design_hbm_channel_w_9_HBM_AXI_RVALID),
        .AXI_08_WDATA(design_hbm_channel_w_9_HBM_AXI_WDATA),
        .AXI_08_WDATA_PARITY({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .AXI_08_WLAST(design_hbm_channel_w_9_HBM_AXI_WLAST),
        .AXI_08_WREADY(design_hbm_channel_w_9_HBM_AXI_WREADY),
        .AXI_08_WSTRB(design_hbm_channel_w_9_HBM_AXI_WSTRB),
        .AXI_08_WVALID(design_hbm_channel_w_9_HBM_AXI_WVALID),
        .HBM_REF_CLK_0(clk_wiz_clk_out1),
        .HBM_REF_CLK_1(clk_wiz_clk_out1),
        .apb_complete_0(hbm_0_apb_complete_0),
        .apb_complete_1(hbm_0_apb_complete_1));
  design_hbm_hbmlane_mux31_0_0 hbmlane_mux31_0
       (.clk(Net2),
        .m_HBM_READ_tdata(hbmlane_mux31_0_m_HBM_READ_TDATA),
        .m_HBM_READ_tkeep(hbmlane_mux31_0_m_HBM_READ_TKEEP),
        .m_HBM_READ_tlast(hbmlane_mux31_0_m_HBM_READ_TLAST),
        .m_HBM_READ_tready(hbmlane_mux31_0_m_HBM_READ_TREADY),
        .m_HBM_READ_tvalid(hbmlane_mux31_0_m_HBM_READ_TVALID),
        .m_HBM_WRITE_tdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,axis_register_slice_4_M_AXIS_TDATA}),
        .m_HBM_WRITE_tready(axis_register_slice_4_M_AXIS_TREADY),
        .m_HBM_WRITE_tvalid(axis_register_slice_4_M_AXIS_TVALID),
        .m_busy(hbmlane_mux31_0_m_busy),
        .m_rd_blockamount(m_ctrl_ports_0_1_rd_blockamount),
        .m_rd_run(m_ctrl_ports_0_1_rd_run),
        .m_rd_startaddress(m_ctrl_ports_0_1_rd_startaddress),
        .m_wr_blockamount(m_ctrl_ports_0_1_wr_blockamount),
        .m_wr_run(m_ctrl_ports_0_1_wr_run),
        .m_wr_startaddress(m_ctrl_ports_0_1_wr_startaddress),
        .mux_select(mux_select_0_1),
        .s1_HBM_READ_tdata(design_hbm_channel_w_1_HBM_READ_TDATA),
        .s1_HBM_READ_tkeep(design_hbm_channel_w_1_HBM_READ_TKEEP),
        .s1_HBM_READ_tlast(design_hbm_channel_w_1_HBM_READ_TLAST),
        .s1_HBM_READ_tready(design_hbm_channel_w_1_HBM_READ_TREADY),
        .s1_HBM_READ_tvalid(design_hbm_channel_w_1_HBM_READ_TVALID),
        .s1_HBM_WRITE_tdata(hbmlane_mux31_0_s1_HBM_WRITE_TDATA),
        .s1_HBM_WRITE_tready(hbmlane_mux31_0_s1_HBM_WRITE_TREADY),
        .s1_HBM_WRITE_tvalid(hbmlane_mux31_0_s1_HBM_WRITE_TVALID),
        .s1_busy(design_hbm_channel_w_1_busy),
        .s1_rd_blockamount(hbmlane_mux31_0_s1_ctrl_ports_rd_blockamount),
        .s1_rd_run(hbmlane_mux31_0_s1_ctrl_ports_rd_run),
        .s1_rd_startaddress(hbmlane_mux31_0_s1_ctrl_ports_rd_startaddress),
        .s1_wr_blockamount(hbmlane_mux31_0_s1_ctrl_ports_wr_blockamount),
        .s1_wr_run(hbmlane_mux31_0_s1_ctrl_ports_wr_run),
        .s1_wr_startaddress(hbmlane_mux31_0_s1_ctrl_ports_wr_startaddress),
        .s2_HBM_READ_tdata(design_hbm_channel_w_2_HBM_READ_TDATA),
        .s2_HBM_READ_tkeep(design_hbm_channel_w_2_HBM_READ_TKEEP),
        .s2_HBM_READ_tlast(design_hbm_channel_w_2_HBM_READ_TLAST),
        .s2_HBM_READ_tready(design_hbm_channel_w_2_HBM_READ_TREADY),
        .s2_HBM_READ_tvalid(design_hbm_channel_w_2_HBM_READ_TVALID),
        .s2_HBM_WRITE_tdata(hbmlane_mux31_0_s2_HBM_WRITE_TDATA),
        .s2_HBM_WRITE_tready(hbmlane_mux31_0_s2_HBM_WRITE_TREADY),
        .s2_HBM_WRITE_tvalid(hbmlane_mux31_0_s2_HBM_WRITE_TVALID),
        .s2_busy(design_hbm_channel_w_2_busy),
        .s2_rd_blockamount(hbmlane_mux31_0_s2_ctrl_ports_rd_blockamount),
        .s2_rd_run(hbmlane_mux31_0_s2_ctrl_ports_rd_run),
        .s2_rd_startaddress(hbmlane_mux31_0_s2_ctrl_ports_rd_startaddress),
        .s2_wr_blockamount(hbmlane_mux31_0_s2_ctrl_ports_wr_blockamount),
        .s2_wr_run(hbmlane_mux31_0_s2_ctrl_ports_wr_run),
        .s2_wr_startaddress(hbmlane_mux31_0_s2_ctrl_ports_wr_startaddress),
        .s3_HBM_READ_tdata(design_hbm_channel_w_3_HBM_READ_TDATA),
        .s3_HBM_READ_tkeep(design_hbm_channel_w_3_HBM_READ_TKEEP),
        .s3_HBM_READ_tlast(design_hbm_channel_w_3_HBM_READ_TLAST),
        .s3_HBM_READ_tready(design_hbm_channel_w_3_HBM_READ_TREADY),
        .s3_HBM_READ_tvalid(design_hbm_channel_w_3_HBM_READ_TVALID),
        .s3_HBM_WRITE_tdata(hbmlane_mux31_0_s3_HBM_WRITE_TDATA),
        .s3_HBM_WRITE_tready(hbmlane_mux31_0_s3_HBM_WRITE_TREADY),
        .s3_HBM_WRITE_tvalid(hbmlane_mux31_0_s3_HBM_WRITE_TVALID),
        .s3_busy(design_hbm_channel_w_3_busy),
        .s3_rd_blockamount(hbmlane_mux31_0_s3_ctrl_ports_rd_blockamount),
        .s3_rd_run(hbmlane_mux31_0_s3_ctrl_ports_rd_run),
        .s3_rd_startaddress(hbmlane_mux31_0_s3_ctrl_ports_rd_startaddress),
        .s3_wr_blockamount(hbmlane_mux31_0_s3_ctrl_ports_wr_blockamount),
        .s3_wr_run(hbmlane_mux31_0_s3_ctrl_ports_wr_run),
        .s3_wr_startaddress(hbmlane_mux31_0_s3_ctrl_ports_wr_startaddress));
  design_hbm_hbmlane_mux31_0_1 hbmlane_mux31_1
       (.clk(Net2),
        .m_HBM_READ_tdata(hbmlane_mux31_1_m_HBM_READ_TDATA),
        .m_HBM_READ_tkeep(hbmlane_mux31_1_m_HBM_READ_TKEEP),
        .m_HBM_READ_tlast(hbmlane_mux31_1_m_HBM_READ_TLAST),
        .m_HBM_READ_tready(hbmlane_mux31_1_m_HBM_READ_TREADY),
        .m_HBM_READ_tvalid(hbmlane_mux31_1_m_HBM_READ_TVALID),
        .m_HBM_WRITE_tdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,axis_register_slice_0_M_AXIS_TDATA}),
        .m_HBM_WRITE_tready(axis_register_slice_0_M_AXIS_TREADY),
        .m_HBM_WRITE_tvalid(axis_register_slice_0_M_AXIS_TVALID),
        .m_busy(hbmlane_mux31_1_m_busy),
        .m_rd_blockamount(m_ctrl_ports_1_1_rd_blockamount),
        .m_rd_run(m_ctrl_ports_1_1_rd_run),
        .m_rd_startaddress(m_ctrl_ports_1_1_rd_startaddress),
        .m_wr_blockamount(m_ctrl_ports_1_1_wr_blockamount),
        .m_wr_run(m_ctrl_ports_1_1_wr_run),
        .m_wr_startaddress(m_ctrl_ports_1_1_wr_startaddress),
        .mux_select(mux_select_1_1),
        .s1_HBM_READ_tdata(design_hbm_channel_w_4_HBM_READ_TDATA),
        .s1_HBM_READ_tkeep(design_hbm_channel_w_4_HBM_READ_TKEEP),
        .s1_HBM_READ_tlast(design_hbm_channel_w_4_HBM_READ_TLAST),
        .s1_HBM_READ_tready(design_hbm_channel_w_4_HBM_READ_TREADY),
        .s1_HBM_READ_tvalid(design_hbm_channel_w_4_HBM_READ_TVALID),
        .s1_HBM_WRITE_tdata(hbmlane_mux31_1_s1_HBM_WRITE_TDATA),
        .s1_HBM_WRITE_tready(hbmlane_mux31_1_s1_HBM_WRITE_TREADY),
        .s1_HBM_WRITE_tvalid(hbmlane_mux31_1_s1_HBM_WRITE_TVALID),
        .s1_busy(design_hbm_channel_w_4_busy),
        .s1_rd_blockamount(hbmlane_mux31_1_s1_ctrl_ports_rd_blockamount),
        .s1_rd_run(hbmlane_mux31_1_s1_ctrl_ports_rd_run),
        .s1_rd_startaddress(hbmlane_mux31_1_s1_ctrl_ports_rd_startaddress),
        .s1_wr_blockamount(hbmlane_mux31_1_s1_ctrl_ports_wr_blockamount),
        .s1_wr_run(hbmlane_mux31_1_s1_ctrl_ports_wr_run),
        .s1_wr_startaddress(hbmlane_mux31_1_s1_ctrl_ports_wr_startaddress),
        .s2_HBM_READ_tdata(design_hbm_channel_w_5_HBM_READ_TDATA),
        .s2_HBM_READ_tkeep(design_hbm_channel_w_5_HBM_READ_TKEEP),
        .s2_HBM_READ_tlast(design_hbm_channel_w_5_HBM_READ_TLAST),
        .s2_HBM_READ_tready(design_hbm_channel_w_5_HBM_READ_TREADY),
        .s2_HBM_READ_tvalid(design_hbm_channel_w_5_HBM_READ_TVALID),
        .s2_HBM_WRITE_tdata(hbmlane_mux31_1_s2_HBM_WRITE_TDATA),
        .s2_HBM_WRITE_tready(hbmlane_mux31_1_s2_HBM_WRITE_TREADY),
        .s2_HBM_WRITE_tvalid(hbmlane_mux31_1_s2_HBM_WRITE_TVALID),
        .s2_busy(design_hbm_channel_w_5_busy),
        .s2_rd_blockamount(hbmlane_mux31_1_s2_ctrl_ports_rd_blockamount),
        .s2_rd_run(hbmlane_mux31_1_s2_ctrl_ports_rd_run),
        .s2_rd_startaddress(hbmlane_mux31_1_s2_ctrl_ports_rd_startaddress),
        .s2_wr_blockamount(hbmlane_mux31_1_s2_ctrl_ports_wr_blockamount),
        .s2_wr_run(hbmlane_mux31_1_s2_ctrl_ports_wr_run),
        .s2_wr_startaddress(hbmlane_mux31_1_s2_ctrl_ports_wr_startaddress),
        .s3_HBM_READ_tdata(design_hbm_channel_w_6_HBM_READ_TDATA),
        .s3_HBM_READ_tkeep(design_hbm_channel_w_6_HBM_READ_TKEEP),
        .s3_HBM_READ_tlast(design_hbm_channel_w_6_HBM_READ_TLAST),
        .s3_HBM_READ_tready(design_hbm_channel_w_6_HBM_READ_TREADY),
        .s3_HBM_READ_tvalid(design_hbm_channel_w_6_HBM_READ_TVALID),
        .s3_HBM_WRITE_tdata(hbmlane_mux31_1_s3_HBM_WRITE_TDATA),
        .s3_HBM_WRITE_tready(hbmlane_mux31_1_s3_HBM_WRITE_TREADY),
        .s3_HBM_WRITE_tvalid(hbmlane_mux31_1_s3_HBM_WRITE_TVALID),
        .s3_busy(design_hbm_channel_w_6_busy),
        .s3_rd_blockamount(hbmlane_mux31_1_s3_ctrl_ports_rd_blockamount),
        .s3_rd_run(hbmlane_mux31_1_s3_ctrl_ports_rd_run),
        .s3_rd_startaddress(hbmlane_mux31_1_s3_ctrl_ports_rd_startaddress),
        .s3_wr_blockamount(hbmlane_mux31_1_s3_ctrl_ports_wr_blockamount),
        .s3_wr_run(hbmlane_mux31_1_s3_ctrl_ports_wr_run),
        .s3_wr_startaddress(hbmlane_mux31_1_s3_ctrl_ports_wr_startaddress));
  design_hbm_hbmlane_mux31_1_0 hbmlane_mux31_2
       (.clk(Net2),
        .m_HBM_READ_tdata(hbmlane_mux31_2_m_HBM_READ_TDATA),
        .m_HBM_READ_tkeep(hbmlane_mux31_2_m_HBM_READ_TKEEP),
        .m_HBM_READ_tlast(hbmlane_mux31_2_m_HBM_READ_TLAST),
        .m_HBM_READ_tready(hbmlane_mux31_2_m_HBM_READ_TREADY),
        .m_HBM_READ_tvalid(hbmlane_mux31_2_m_HBM_READ_TVALID),
        .m_HBM_WRITE_tdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,axis_register_slice_2_M_AXIS_TDATA}),
        .m_HBM_WRITE_tready(axis_register_slice_2_M_AXIS_TREADY),
        .m_HBM_WRITE_tvalid(axis_register_slice_2_M_AXIS_TVALID),
        .m_busy(hbmlane_mux31_2_m_busy),
        .m_rd_blockamount(m_ctrl_ports_2_1_rd_blockamount),
        .m_rd_run(m_ctrl_ports_2_1_rd_run),
        .m_rd_startaddress(m_ctrl_ports_2_1_rd_startaddress),
        .m_wr_blockamount(m_ctrl_ports_2_1_wr_blockamount),
        .m_wr_run(m_ctrl_ports_2_1_wr_run),
        .m_wr_startaddress(m_ctrl_ports_2_1_wr_startaddress),
        .mux_select(mux_select_2_1),
        .s1_HBM_READ_tdata(design_hbm_channel_w_7_HBM_READ_TDATA),
        .s1_HBM_READ_tkeep(design_hbm_channel_w_7_HBM_READ_TKEEP),
        .s1_HBM_READ_tlast(design_hbm_channel_w_7_HBM_READ_TLAST),
        .s1_HBM_READ_tready(design_hbm_channel_w_7_HBM_READ_TREADY),
        .s1_HBM_READ_tvalid(design_hbm_channel_w_7_HBM_READ_TVALID),
        .s1_HBM_WRITE_tdata(hbmlane_mux31_2_s1_HBM_WRITE_TDATA),
        .s1_HBM_WRITE_tready(hbmlane_mux31_2_s1_HBM_WRITE_TREADY),
        .s1_HBM_WRITE_tvalid(hbmlane_mux31_2_s1_HBM_WRITE_TVALID),
        .s1_busy(design_hbm_channel_w_7_busy),
        .s1_rd_blockamount(hbmlane_mux31_2_s1_ctrl_ports_rd_blockamount),
        .s1_rd_run(hbmlane_mux31_2_s1_ctrl_ports_rd_run),
        .s1_rd_startaddress(hbmlane_mux31_2_s1_ctrl_ports_rd_startaddress),
        .s1_wr_blockamount(hbmlane_mux31_2_s1_ctrl_ports_wr_blockamount),
        .s1_wr_run(hbmlane_mux31_2_s1_ctrl_ports_wr_run),
        .s1_wr_startaddress(hbmlane_mux31_2_s1_ctrl_ports_wr_startaddress),
        .s2_HBM_READ_tdata(design_hbm_channel_w_8_HBM_READ_TDATA),
        .s2_HBM_READ_tkeep(design_hbm_channel_w_8_HBM_READ_TKEEP),
        .s2_HBM_READ_tlast(design_hbm_channel_w_8_HBM_READ_TLAST),
        .s2_HBM_READ_tready(design_hbm_channel_w_8_HBM_READ_TREADY),
        .s2_HBM_READ_tvalid(design_hbm_channel_w_8_HBM_READ_TVALID),
        .s2_HBM_WRITE_tdata(hbmlane_mux31_2_s2_HBM_WRITE_TDATA),
        .s2_HBM_WRITE_tready(hbmlane_mux31_2_s2_HBM_WRITE_TREADY),
        .s2_HBM_WRITE_tvalid(hbmlane_mux31_2_s2_HBM_WRITE_TVALID),
        .s2_busy(design_hbm_channel_w_8_busy),
        .s2_rd_blockamount(hbmlane_mux31_2_s2_ctrl_ports_rd_blockamount),
        .s2_rd_run(hbmlane_mux31_2_s2_ctrl_ports_rd_run),
        .s2_rd_startaddress(hbmlane_mux31_2_s2_ctrl_ports_rd_startaddress),
        .s2_wr_blockamount(hbmlane_mux31_2_s2_ctrl_ports_wr_blockamount),
        .s2_wr_run(hbmlane_mux31_2_s2_ctrl_ports_wr_run),
        .s2_wr_startaddress(hbmlane_mux31_2_s2_ctrl_ports_wr_startaddress),
        .s3_HBM_READ_tdata(design_hbm_channel_w_9_HBM_READ_TDATA),
        .s3_HBM_READ_tkeep(design_hbm_channel_w_9_HBM_READ_TKEEP),
        .s3_HBM_READ_tlast(design_hbm_channel_w_9_HBM_READ_TLAST),
        .s3_HBM_READ_tready(design_hbm_channel_w_9_HBM_READ_TREADY),
        .s3_HBM_READ_tvalid(design_hbm_channel_w_9_HBM_READ_TVALID),
        .s3_HBM_WRITE_tdata(hbmlane_mux31_2_s3_HBM_WRITE_TDATA),
        .s3_HBM_WRITE_tready(hbmlane_mux31_2_s3_HBM_WRITE_TREADY),
        .s3_HBM_WRITE_tvalid(hbmlane_mux31_2_s3_HBM_WRITE_TVALID),
        .s3_busy(design_hbm_channel_w_9_busy),
        .s3_rd_blockamount(hbmlane_mux31_2_s3_ctrl_ports_rd_blockamount),
        .s3_rd_run(hbmlane_mux31_2_s3_ctrl_ports_rd_run),
        .s3_rd_startaddress(hbmlane_mux31_2_s3_ctrl_ports_rd_startaddress),
        .s3_wr_blockamount(hbmlane_mux31_2_s3_ctrl_ports_wr_blockamount),
        .s3_wr_run(hbmlane_mux31_2_s3_ctrl_ports_wr_run),
        .s3_wr_startaddress(hbmlane_mux31_2_s3_ctrl_ports_wr_startaddress));
  design_hbm_proc_sys_reset_0_0 proc_sys_reset_0
       (.aux_reset_in(1'b1),
        .dcm_locked(1'b1),
        .ext_reset_in(reset_0_1),
        .mb_debug_sys_rst(1'b0),
        .peripheral_aresetn(Net),
        .slowest_sync_clk(clk_wiz_1_clk_out1));
  design_hbm_proc_sys_reset_1_0 proc_sys_reset_1
       (.aux_reset_in(1'b1),
        .dcm_locked(1'b1),
        .ext_reset_in(reset_0_1),
        .mb_debug_sys_rst(1'b0),
        .peripheral_aresetn(Net1),
        .slowest_sync_clk(clk_wiz_clk_out1));
  design_hbm_util_vector_logic_0_0 util_vector_logic_0
       (.Op1(hbm_0_apb_complete_0),
        .Op2(hbm_0_apb_complete_1),
        .Res(util_vector_logic_0_Res));
  design_hbm_xlconstant_0_0 xlconstant_0
       (.dout(Net3));
endmodule
