// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
// Date        : Tue Dec  6 16:31:49 2022
// Host        : dell-Vostro-3910-China-HDD-Protection running 64-bit Ubuntu 22.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_hbm_design_hbm_channel_w_0_4_sim_netlist.v
// Design      : design_hbm_design_hbm_channel_w_0_4
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcvu37p-fsvh2892-2L-e
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* HW_HANDOFF = "design_hbm_channel.hwdef" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel
   (HBM_AXI_araddr,
    HBM_AXI_arburst,
    HBM_AXI_arcache,
    HBM_AXI_arid,
    HBM_AXI_arlen,
    HBM_AXI_arlock,
    HBM_AXI_arprot,
    HBM_AXI_arqos,
    HBM_AXI_arready,
    HBM_AXI_arsize,
    HBM_AXI_arvalid,
    HBM_AXI_awaddr,
    HBM_AXI_awburst,
    HBM_AXI_awcache,
    HBM_AXI_awid,
    HBM_AXI_awlen,
    HBM_AXI_awlock,
    HBM_AXI_awprot,
    HBM_AXI_awqos,
    HBM_AXI_awready,
    HBM_AXI_awsize,
    HBM_AXI_awvalid,
    HBM_AXI_bid,
    HBM_AXI_bready,
    HBM_AXI_bresp,
    HBM_AXI_bvalid,
    HBM_AXI_rdata,
    HBM_AXI_rid,
    HBM_AXI_rlast,
    HBM_AXI_rready,
    HBM_AXI_rresp,
    HBM_AXI_rvalid,
    HBM_AXI_wdata,
    HBM_AXI_wid,
    HBM_AXI_wlast,
    HBM_AXI_wready,
    HBM_AXI_wstrb,
    HBM_AXI_wvalid,
    HBM_READ_tdata,
    HBM_READ_tkeep,
    HBM_READ_tlast,
    HBM_READ_tready,
    HBM_READ_tvalid,
    HBM_WRITE_tdata,
    HBM_WRITE_tkeep,
    HBM_WRITE_tlast,
    HBM_WRITE_tready,
    HBM_WRITE_tvalid,
    clk_312,
    clk_450,
    rd_running,
    user_hbm_channel_ctrl_ports_busy,
    user_hbm_channel_ctrl_ports_rd_blockamount,
    user_hbm_channel_ctrl_ports_rd_startaddress,
    user_hbm_channel_ctrl_ports_wr_blockamount,
    user_hbm_channel_ctrl_ports_wr_startaddress,
    wr_running);
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI ARADDR" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME HBM_AXI, ADDR_WIDTH 33, ARUSER_WIDTH 0, AWUSER_WIDTH 0, BUSER_WIDTH 0, CLK_DOMAIN design_hbm_channel_clk_450, DATA_WIDTH 256, FREQ_HZ 450000000, HAS_BRESP 1, HAS_BURST 1, HAS_CACHE 0, HAS_LOCK 0, HAS_PROT 0, HAS_QOS 0, HAS_REGION 0, HAS_RRESP 1, HAS_WSTRB 1, ID_WIDTH 6, INSERT_VIP 0, MAX_BURST_LENGTH 16, NUM_READ_OUTSTANDING 2, NUM_READ_THREADS 1, NUM_WRITE_OUTSTANDING 2, NUM_WRITE_THREADS 1, PHASE 0.0, PROTOCOL AXI3, READ_WRITE_MODE READ_WRITE, RUSER_BITS_PER_BYTE 0, RUSER_WIDTH 0, SUPPORTS_NARROW_BURST 1, WUSER_BITS_PER_BYTE 0, WUSER_WIDTH 0" *) output [32:0]HBM_AXI_araddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI ARBURST" *) output [1:0]HBM_AXI_arburst;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI ARCACHE" *) output [3:0]HBM_AXI_arcache;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI ARID" *) output [5:0]HBM_AXI_arid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI ARLEN" *) output [3:0]HBM_AXI_arlen;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI ARLOCK" *) output [1:0]HBM_AXI_arlock;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI ARPROT" *) output [2:0]HBM_AXI_arprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI ARQOS" *) output [3:0]HBM_AXI_arqos;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI ARREADY" *) input HBM_AXI_arready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI ARSIZE" *) output [2:0]HBM_AXI_arsize;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI ARVALID" *) output HBM_AXI_arvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI AWADDR" *) output [32:0]HBM_AXI_awaddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI AWBURST" *) output [1:0]HBM_AXI_awburst;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI AWCACHE" *) output [3:0]HBM_AXI_awcache;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI AWID" *) output [5:0]HBM_AXI_awid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI AWLEN" *) output [3:0]HBM_AXI_awlen;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI AWLOCK" *) output [1:0]HBM_AXI_awlock;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI AWPROT" *) output [2:0]HBM_AXI_awprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI AWQOS" *) output [3:0]HBM_AXI_awqos;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI AWREADY" *) input HBM_AXI_awready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI AWSIZE" *) output [2:0]HBM_AXI_awsize;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI AWVALID" *) output HBM_AXI_awvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI BID" *) input [5:0]HBM_AXI_bid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI BREADY" *) output HBM_AXI_bready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI BRESP" *) input [1:0]HBM_AXI_bresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI BVALID" *) input HBM_AXI_bvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI RDATA" *) input [255:0]HBM_AXI_rdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI RID" *) input [5:0]HBM_AXI_rid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI RLAST" *) input HBM_AXI_rlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI RREADY" *) output HBM_AXI_rready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI RRESP" *) input [1:0]HBM_AXI_rresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI RVALID" *) input HBM_AXI_rvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI WDATA" *) output [255:0]HBM_AXI_wdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI WID" *) output [5:0]HBM_AXI_wid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI WLAST" *) output HBM_AXI_wlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI WREADY" *) input HBM_AXI_wready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI WSTRB" *) output [31:0]HBM_AXI_wstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI WVALID" *) output HBM_AXI_wvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_READ TDATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME HBM_READ, CLK_DOMAIN design_hbm_channel_clk_312, FREQ_HZ 312500000, HAS_TKEEP 1, HAS_TLAST 1, HAS_TREADY 1, HAS_TSTRB 0, INSERT_VIP 0, LAYERED_METADATA undef, PHASE 0.0, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0" *) output [255:0]HBM_READ_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_READ TKEEP" *) output [31:0]HBM_READ_tkeep;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_READ TLAST" *) output HBM_READ_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_READ TREADY" *) input HBM_READ_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_READ TVALID" *) output HBM_READ_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_WRITE TDATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME HBM_WRITE, CLK_DOMAIN design_hbm_channel_clk_312, FREQ_HZ 312500000, HAS_TKEEP 1, HAS_TLAST 1, HAS_TREADY 1, HAS_TSTRB 0, INSERT_VIP 0, LAYERED_METADATA undef, PHASE 0.0, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0" *) input [255:0]HBM_WRITE_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_WRITE TKEEP" *) input [31:0]HBM_WRITE_tkeep;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_WRITE TLAST" *) input HBM_WRITE_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_WRITE TREADY" *) output HBM_WRITE_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_WRITE TVALID" *) input HBM_WRITE_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 CLK.CLK_312 CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME CLK.CLK_312, ASSOCIATED_BUSIF HBM_READ:HBM_WRITE, CLK_DOMAIN design_hbm_channel_clk_312, FREQ_HZ 312500000, FREQ_TOLERANCE_HZ 0, INSERT_VIP 0, PHASE 0.0" *) input clk_312;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 CLK.CLK_450 CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME CLK.CLK_450, ASSOCIATED_BUSIF HBM_AXI, CLK_DOMAIN design_hbm_channel_clk_450, FREQ_HZ 450000000, FREQ_TOLERANCE_HZ 0, INSERT_VIP 0, PHASE 0.0" *) input clk_450;
  input rd_running;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 user_hbm_channel_ctrl_ports busy" *) output user_hbm_channel_ctrl_ports_busy;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 user_hbm_channel_ctrl_ports rd_blockamount" *) input [31:0]user_hbm_channel_ctrl_ports_rd_blockamount;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 user_hbm_channel_ctrl_ports rd_startaddress" *) input [31:0]user_hbm_channel_ctrl_ports_rd_startaddress;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 user_hbm_channel_ctrl_ports wr_blockamount" *) input [31:0]user_hbm_channel_ctrl_ports_wr_blockamount;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 user_hbm_channel_ctrl_ports wr_startaddress" *) input [31:0]user_hbm_channel_ctrl_ports_wr_startaddress;
  input wr_running;

  wire \<const0> ;
  wire [32:0]HBM_AXI_araddr;
  wire [1:0]HBM_AXI_arburst;
  wire [5:0]HBM_AXI_arid;
  wire [3:0]HBM_AXI_arlen;
  wire HBM_AXI_arready;
  wire [2:0]HBM_AXI_arsize;
  wire HBM_AXI_arvalid;
  wire [32:0]HBM_AXI_awaddr;
  wire [1:0]HBM_AXI_awburst;
  wire [5:0]HBM_AXI_awid;
  wire [3:0]HBM_AXI_awlen;
  wire HBM_AXI_awready;
  wire [2:0]HBM_AXI_awsize;
  wire HBM_AXI_awvalid;
  wire [5:0]HBM_AXI_bid;
  wire HBM_AXI_bready;
  wire [1:0]HBM_AXI_bresp;
  wire HBM_AXI_bvalid;
  wire [255:0]HBM_AXI_rdata;
  wire [5:0]HBM_AXI_rid;
  wire HBM_AXI_rlast;
  wire HBM_AXI_rready;
  wire [1:0]HBM_AXI_rresp;
  wire HBM_AXI_rvalid;
  wire [255:0]HBM_AXI_wdata;
  wire [5:0]HBM_AXI_wid;
  wire HBM_AXI_wlast;
  wire HBM_AXI_wready;
  wire [31:0]HBM_AXI_wstrb;
  wire HBM_AXI_wvalid;
  wire [255:0]HBM_READ_tdata;
  wire [31:0]HBM_READ_tkeep;
  wire HBM_READ_tlast;
  wire HBM_READ_tready;
  wire HBM_READ_tvalid;
  wire [255:0]HBM_WRITE_tdata;
  wire HBM_WRITE_tready;
  wire HBM_WRITE_tvalid;
  wire axi_register_slice_0_n_326;
  wire axi_register_slice_0_n_327;
  wire axi_register_slice_0_n_328;
  wire axi_register_slice_0_n_329;
  wire axi_register_slice_0_n_330;
  wire axi_register_slice_0_n_331;
  wire axi_register_slice_0_n_332;
  wire axi_register_slice_0_n_333;
  wire axi_register_slice_0_n_334;
  wire axi_register_slice_0_n_335;
  wire axi_register_slice_0_n_336;
  wire axi_register_slice_0_n_337;
  wire axi_register_slice_0_n_338;
  wire axi_register_slice_0_n_685;
  wire axi_register_slice_0_n_686;
  wire axi_register_slice_0_n_687;
  wire axi_register_slice_0_n_688;
  wire axi_register_slice_0_n_689;
  wire axi_register_slice_0_n_690;
  wire axi_register_slice_0_n_691;
  wire axi_register_slice_0_n_692;
  wire axi_register_slice_0_n_693;
  wire axi_register_slice_0_n_694;
  wire axi_register_slice_0_n_695;
  wire axi_register_slice_0_n_696;
  wire axi_register_slice_0_n_697;
  wire [255:0]axis_data_fifo_0_M_AXIS_TDATA;
  wire [31:0]axis_data_fifo_0_M_AXIS_TKEEP;
  wire axis_data_fifo_0_M_AXIS_TLAST;
  wire axis_data_fifo_0_M_AXIS_TREADY;
  wire axis_data_fifo_0_M_AXIS_TVALID;
  wire [255:0]axis_data_fifo_1_M_AXIS_TDATA;
  wire [31:0]axis_data_fifo_1_M_AXIS_TKEEP;
  wire axis_data_fifo_1_M_AXIS_TLAST;
  wire axis_data_fifo_1_M_AXIS_TREADY;
  wire axis_data_fifo_1_M_AXIS_TVALID;
  wire [255:0]axis_register_slice_0_M_AXIS_TDATA;
  wire [31:0]axis_register_slice_0_M_AXIS_TKEEP;
  wire axis_register_slice_0_M_AXIS_TLAST;
  wire axis_register_slice_0_M_AXIS_TREADY;
  wire axis_register_slice_0_M_AXIS_TVALID;
  wire [255:0]axis_register_slice_1_M_AXIS_TDATA;
  wire axis_register_slice_1_M_AXIS_TREADY;
  wire axis_register_slice_1_M_AXIS_TVALID;
  wire [255:0]axis_register_slice_3_M_AXIS_TDATA;
  wire [31:0]axis_register_slice_3_M_AXIS_TKEEP;
  wire axis_register_slice_3_M_AXIS_TLAST;
  wire axis_register_slice_3_M_AXIS_TREADY;
  wire axis_register_slice_3_M_AXIS_TVALID;
  wire [32:0]channel_ctrl_v5_0_AXI_ARADDR;
  wire [1:0]channel_ctrl_v5_0_AXI_ARBURST;
  wire [5:0]channel_ctrl_v5_0_AXI_ARID;
  wire [3:0]channel_ctrl_v5_0_AXI_ARLEN;
  wire channel_ctrl_v5_0_AXI_ARREADY;
  wire [2:0]channel_ctrl_v5_0_AXI_ARSIZE;
  wire channel_ctrl_v5_0_AXI_ARVALID;
  wire [32:0]channel_ctrl_v5_0_AXI_AWADDR;
  wire [1:0]channel_ctrl_v5_0_AXI_AWBURST;
  wire [5:0]channel_ctrl_v5_0_AXI_AWID;
  wire [3:0]channel_ctrl_v5_0_AXI_AWLEN;
  wire channel_ctrl_v5_0_AXI_AWREADY;
  wire [2:0]channel_ctrl_v5_0_AXI_AWSIZE;
  wire channel_ctrl_v5_0_AXI_AWVALID;
  wire [5:0]channel_ctrl_v5_0_AXI_BID;
  wire channel_ctrl_v5_0_AXI_BREADY;
  wire [1:0]channel_ctrl_v5_0_AXI_BRESP;
  wire channel_ctrl_v5_0_AXI_BVALID;
  wire [255:0]channel_ctrl_v5_0_AXI_RDATA;
  wire [5:0]channel_ctrl_v5_0_AXI_RID;
  wire channel_ctrl_v5_0_AXI_RLAST;
  wire channel_ctrl_v5_0_AXI_RREADY;
  wire [1:0]channel_ctrl_v5_0_AXI_RRESP;
  wire channel_ctrl_v5_0_AXI_RVALID;
  wire [255:0]channel_ctrl_v5_0_AXI_WDATA;
  wire [5:0]channel_ctrl_v5_0_AXI_WID;
  wire channel_ctrl_v5_0_AXI_WLAST;
  wire channel_ctrl_v5_0_AXI_WREADY;
  wire [31:0]channel_ctrl_v5_0_AXI_WSTRB;
  wire channel_ctrl_v5_0_AXI_WVALID;
  wire [255:0]channel_ctrl_v5_0_FIFO_WRITE_TDATA;
  wire [31:0]channel_ctrl_v5_0_FIFO_WRITE_TKEEP;
  wire channel_ctrl_v5_0_FIFO_WRITE_TLAST;
  wire channel_ctrl_v5_0_FIFO_WRITE_TREADY;
  wire channel_ctrl_v5_0_FIFO_WRITE_TVALID;
  wire clk_312;
  wire clk_450;
  wire rd_running;
  wire user_hbm_channel_ctrl_ports_busy;
  wire [31:0]user_hbm_channel_ctrl_ports_rd_blockamount;
  wire [31:0]user_hbm_channel_ctrl_ports_rd_startaddress;
  wire [31:0]user_hbm_channel_ctrl_ports_wr_blockamount;
  wire [31:0]user_hbm_channel_ctrl_ports_wr_startaddress;
  wire wr_running;
  wire NLW_axis_register_slice_1_m_axis_tlast_UNCONNECTED;
  wire [31:0]NLW_axis_register_slice_1_m_axis_tkeep_UNCONNECTED;
  wire [31:0]NLW_channel_ctrl_v5_0_AXI_WDATA_PARITY_UNCONNECTED;

  assign HBM_AXI_arcache[3] = \<const0> ;
  assign HBM_AXI_arcache[2] = \<const0> ;
  assign HBM_AXI_arcache[1] = \<const0> ;
  assign HBM_AXI_arcache[0] = \<const0> ;
  assign HBM_AXI_arlock[1] = \<const0> ;
  assign HBM_AXI_arlock[0] = \<const0> ;
  assign HBM_AXI_arprot[2] = \<const0> ;
  assign HBM_AXI_arprot[1] = \<const0> ;
  assign HBM_AXI_arprot[0] = \<const0> ;
  assign HBM_AXI_arqos[3] = \<const0> ;
  assign HBM_AXI_arqos[2] = \<const0> ;
  assign HBM_AXI_arqos[1] = \<const0> ;
  assign HBM_AXI_arqos[0] = \<const0> ;
  assign HBM_AXI_awcache[3] = \<const0> ;
  assign HBM_AXI_awcache[2] = \<const0> ;
  assign HBM_AXI_awcache[1] = \<const0> ;
  assign HBM_AXI_awcache[0] = \<const0> ;
  assign HBM_AXI_awlock[1] = \<const0> ;
  assign HBM_AXI_awlock[0] = \<const0> ;
  assign HBM_AXI_awprot[2] = \<const0> ;
  assign HBM_AXI_awprot[1] = \<const0> ;
  assign HBM_AXI_awprot[0] = \<const0> ;
  assign HBM_AXI_awqos[3] = \<const0> ;
  assign HBM_AXI_awqos[2] = \<const0> ;
  assign HBM_AXI_awqos[1] = \<const0> ;
  assign HBM_AXI_awqos[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  (* X_CORE_INFO = "axi_register_slice_v2_1_27_axi_register_slice,Vivado 2022.2" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_axi_register_slice_0_0 axi_register_slice_0
       (.aclk(clk_450),
        .aresetn(1'b1),
        .m_axi_araddr(HBM_AXI_araddr),
        .m_axi_arburst(HBM_AXI_arburst),
        .m_axi_arcache({axi_register_slice_0_n_687,axi_register_slice_0_n_688,axi_register_slice_0_n_689,axi_register_slice_0_n_690}),
        .m_axi_arid(HBM_AXI_arid),
        .m_axi_arlen(HBM_AXI_arlen),
        .m_axi_arlock({axi_register_slice_0_n_685,axi_register_slice_0_n_686}),
        .m_axi_arprot({axi_register_slice_0_n_691,axi_register_slice_0_n_692,axi_register_slice_0_n_693}),
        .m_axi_arqos({axi_register_slice_0_n_694,axi_register_slice_0_n_695,axi_register_slice_0_n_696,axi_register_slice_0_n_697}),
        .m_axi_arready(HBM_AXI_arready),
        .m_axi_arsize(HBM_AXI_arsize),
        .m_axi_arvalid(HBM_AXI_arvalid),
        .m_axi_awaddr(HBM_AXI_awaddr),
        .m_axi_awburst(HBM_AXI_awburst),
        .m_axi_awcache({axi_register_slice_0_n_328,axi_register_slice_0_n_329,axi_register_slice_0_n_330,axi_register_slice_0_n_331}),
        .m_axi_awid(HBM_AXI_awid),
        .m_axi_awlen(HBM_AXI_awlen),
        .m_axi_awlock({axi_register_slice_0_n_326,axi_register_slice_0_n_327}),
        .m_axi_awprot({axi_register_slice_0_n_332,axi_register_slice_0_n_333,axi_register_slice_0_n_334}),
        .m_axi_awqos({axi_register_slice_0_n_335,axi_register_slice_0_n_336,axi_register_slice_0_n_337,axi_register_slice_0_n_338}),
        .m_axi_awready(HBM_AXI_awready),
        .m_axi_awsize(HBM_AXI_awsize),
        .m_axi_awvalid(HBM_AXI_awvalid),
        .m_axi_bid(HBM_AXI_bid),
        .m_axi_bready(HBM_AXI_bready),
        .m_axi_bresp(HBM_AXI_bresp),
        .m_axi_bvalid(HBM_AXI_bvalid),
        .m_axi_rdata(HBM_AXI_rdata),
        .m_axi_rid(HBM_AXI_rid),
        .m_axi_rlast(HBM_AXI_rlast),
        .m_axi_rready(HBM_AXI_rready),
        .m_axi_rresp(HBM_AXI_rresp),
        .m_axi_rvalid(HBM_AXI_rvalid),
        .m_axi_wdata(HBM_AXI_wdata),
        .m_axi_wid(HBM_AXI_wid),
        .m_axi_wlast(HBM_AXI_wlast),
        .m_axi_wready(HBM_AXI_wready),
        .m_axi_wstrb(HBM_AXI_wstrb),
        .m_axi_wvalid(HBM_AXI_wvalid),
        .s_axi_araddr(channel_ctrl_v5_0_AXI_ARADDR),
        .s_axi_arburst(channel_ctrl_v5_0_AXI_ARBURST),
        .s_axi_arcache({1'b0,1'b0,1'b1,1'b1}),
        .s_axi_arid(channel_ctrl_v5_0_AXI_ARID),
        .s_axi_arlen(channel_ctrl_v5_0_AXI_ARLEN),
        .s_axi_arlock({1'b0,1'b0}),
        .s_axi_arprot({1'b0,1'b0,1'b0}),
        .s_axi_arqos({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(channel_ctrl_v5_0_AXI_ARREADY),
        .s_axi_arsize(channel_ctrl_v5_0_AXI_ARSIZE),
        .s_axi_arvalid(channel_ctrl_v5_0_AXI_ARVALID),
        .s_axi_awaddr(channel_ctrl_v5_0_AXI_AWADDR),
        .s_axi_awburst(channel_ctrl_v5_0_AXI_AWBURST),
        .s_axi_awcache({1'b0,1'b0,1'b1,1'b1}),
        .s_axi_awid(channel_ctrl_v5_0_AXI_AWID),
        .s_axi_awlen(channel_ctrl_v5_0_AXI_AWLEN),
        .s_axi_awlock({1'b0,1'b0}),
        .s_axi_awprot({1'b0,1'b0,1'b0}),
        .s_axi_awqos({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(channel_ctrl_v5_0_AXI_AWREADY),
        .s_axi_awsize(channel_ctrl_v5_0_AXI_AWSIZE),
        .s_axi_awvalid(channel_ctrl_v5_0_AXI_AWVALID),
        .s_axi_bid(channel_ctrl_v5_0_AXI_BID),
        .s_axi_bready(channel_ctrl_v5_0_AXI_BREADY),
        .s_axi_bresp(channel_ctrl_v5_0_AXI_BRESP),
        .s_axi_bvalid(channel_ctrl_v5_0_AXI_BVALID),
        .s_axi_rdata(channel_ctrl_v5_0_AXI_RDATA),
        .s_axi_rid(channel_ctrl_v5_0_AXI_RID),
        .s_axi_rlast(channel_ctrl_v5_0_AXI_RLAST),
        .s_axi_rready(channel_ctrl_v5_0_AXI_RREADY),
        .s_axi_rresp(channel_ctrl_v5_0_AXI_RRESP),
        .s_axi_rvalid(channel_ctrl_v5_0_AXI_RVALID),
        .s_axi_wdata(channel_ctrl_v5_0_AXI_WDATA),
        .s_axi_wid(channel_ctrl_v5_0_AXI_WID),
        .s_axi_wlast(channel_ctrl_v5_0_AXI_WLAST),
        .s_axi_wready(channel_ctrl_v5_0_AXI_WREADY),
        .s_axi_wstrb(channel_ctrl_v5_0_AXI_WSTRB),
        .s_axi_wvalid(channel_ctrl_v5_0_AXI_WVALID));
  (* X_CORE_INFO = "axis_data_fifo_v2_0_9_top,Vivado 2022.2" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_axis_data_fifo_0_0 axis_data_fifo_0
       (.m_axis_aclk(clk_450),
        .m_axis_tdata(axis_data_fifo_0_M_AXIS_TDATA),
        .m_axis_tkeep(axis_data_fifo_0_M_AXIS_TKEEP),
        .m_axis_tlast(axis_data_fifo_0_M_AXIS_TLAST),
        .m_axis_tready(axis_data_fifo_0_M_AXIS_TREADY),
        .m_axis_tvalid(axis_data_fifo_0_M_AXIS_TVALID),
        .s_axis_aclk(clk_312),
        .s_axis_aresetn(1'b1),
        .s_axis_tdata(axis_register_slice_3_M_AXIS_TDATA),
        .s_axis_tkeep(axis_register_slice_3_M_AXIS_TKEEP),
        .s_axis_tlast(axis_register_slice_3_M_AXIS_TLAST),
        .s_axis_tready(axis_register_slice_3_M_AXIS_TREADY),
        .s_axis_tvalid(axis_register_slice_3_M_AXIS_TVALID));
  (* X_CORE_INFO = "axis_data_fifo_v2_0_9_top,Vivado 2022.2" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_axis_data_fifo_0_1 axis_data_fifo_1
       (.m_axis_aclk(clk_312),
        .m_axis_tdata(axis_data_fifo_1_M_AXIS_TDATA),
        .m_axis_tkeep(axis_data_fifo_1_M_AXIS_TKEEP),
        .m_axis_tlast(axis_data_fifo_1_M_AXIS_TLAST),
        .m_axis_tready(axis_data_fifo_1_M_AXIS_TREADY),
        .m_axis_tvalid(axis_data_fifo_1_M_AXIS_TVALID),
        .s_axis_aclk(clk_450),
        .s_axis_aresetn(1'b1),
        .s_axis_tdata(axis_register_slice_0_M_AXIS_TDATA),
        .s_axis_tkeep(axis_register_slice_0_M_AXIS_TKEEP),
        .s_axis_tlast(axis_register_slice_0_M_AXIS_TLAST),
        .s_axis_tready(axis_register_slice_0_M_AXIS_TREADY),
        .s_axis_tvalid(axis_register_slice_0_M_AXIS_TVALID));
  (* X_CORE_INFO = "axis_register_slice_v1_1_27_axis_register_slice,Vivado 2022.2" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_axis_register_slice_0_0 axis_register_slice_0
       (.aclk(clk_450),
        .aresetn(1'b1),
        .m_axis_tdata(axis_register_slice_0_M_AXIS_TDATA),
        .m_axis_tkeep(axis_register_slice_0_M_AXIS_TKEEP),
        .m_axis_tlast(axis_register_slice_0_M_AXIS_TLAST),
        .m_axis_tready(axis_register_slice_0_M_AXIS_TREADY),
        .m_axis_tvalid(axis_register_slice_0_M_AXIS_TVALID),
        .s_axis_tdata(channel_ctrl_v5_0_FIFO_WRITE_TDATA),
        .s_axis_tkeep(channel_ctrl_v5_0_FIFO_WRITE_TKEEP),
        .s_axis_tlast(channel_ctrl_v5_0_FIFO_WRITE_TLAST),
        .s_axis_tready(channel_ctrl_v5_0_FIFO_WRITE_TREADY),
        .s_axis_tvalid(channel_ctrl_v5_0_FIFO_WRITE_TVALID));
  (* X_CORE_INFO = "axis_register_slice_v1_1_27_axis_register_slice,Vivado 2022.2" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_axis_register_slice_0_1 axis_register_slice_1
       (.aclk(clk_450),
        .aresetn(1'b1),
        .m_axis_tdata(axis_register_slice_1_M_AXIS_TDATA),
        .m_axis_tkeep(NLW_axis_register_slice_1_m_axis_tkeep_UNCONNECTED[31:0]),
        .m_axis_tlast(NLW_axis_register_slice_1_m_axis_tlast_UNCONNECTED),
        .m_axis_tready(axis_register_slice_1_M_AXIS_TREADY),
        .m_axis_tvalid(axis_register_slice_1_M_AXIS_TVALID),
        .s_axis_tdata(axis_data_fifo_0_M_AXIS_TDATA),
        .s_axis_tkeep(axis_data_fifo_0_M_AXIS_TKEEP),
        .s_axis_tlast(axis_data_fifo_0_M_AXIS_TLAST),
        .s_axis_tready(axis_data_fifo_0_M_AXIS_TREADY),
        .s_axis_tvalid(axis_data_fifo_0_M_AXIS_TVALID));
  (* X_CORE_INFO = "axis_register_slice_v1_1_27_axis_register_slice,Vivado 2022.2" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_axis_register_slice_2_0 axis_register_slice_2
       (.aclk(clk_312),
        .aresetn(1'b1),
        .m_axis_tdata(HBM_READ_tdata),
        .m_axis_tkeep(HBM_READ_tkeep),
        .m_axis_tlast(HBM_READ_tlast),
        .m_axis_tready(HBM_READ_tready),
        .m_axis_tvalid(HBM_READ_tvalid),
        .s_axis_tdata(axis_data_fifo_1_M_AXIS_TDATA),
        .s_axis_tkeep(axis_data_fifo_1_M_AXIS_TKEEP),
        .s_axis_tlast(axis_data_fifo_1_M_AXIS_TLAST),
        .s_axis_tready(axis_data_fifo_1_M_AXIS_TREADY),
        .s_axis_tvalid(axis_data_fifo_1_M_AXIS_TVALID));
  (* X_CORE_INFO = "axis_register_slice_v1_1_27_axis_register_slice,Vivado 2022.2" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_axis_register_slice_3_0 axis_register_slice_3
       (.aclk(clk_312),
        .aresetn(1'b1),
        .m_axis_tdata(axis_register_slice_3_M_AXIS_TDATA),
        .m_axis_tkeep(axis_register_slice_3_M_AXIS_TKEEP),
        .m_axis_tlast(axis_register_slice_3_M_AXIS_TLAST),
        .m_axis_tready(axis_register_slice_3_M_AXIS_TREADY),
        .m_axis_tvalid(axis_register_slice_3_M_AXIS_TVALID),
        .s_axis_tdata(HBM_WRITE_tdata),
        .s_axis_tkeep({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axis_tlast(1'b0),
        .s_axis_tready(HBM_WRITE_tready),
        .s_axis_tvalid(HBM_WRITE_tvalid));
  (* X_CORE_INFO = "channel_ctrl_v5,Vivado 2022.2" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_channel_ctrl_v5_0_0 channel_ctrl_v5_0
       (.AXI_ARADDR(channel_ctrl_v5_0_AXI_ARADDR),
        .AXI_ARBURST(channel_ctrl_v5_0_AXI_ARBURST),
        .AXI_ARID(channel_ctrl_v5_0_AXI_ARID),
        .AXI_ARLEN(channel_ctrl_v5_0_AXI_ARLEN),
        .AXI_ARREADY(channel_ctrl_v5_0_AXI_ARREADY),
        .AXI_ARSIZE(channel_ctrl_v5_0_AXI_ARSIZE),
        .AXI_ARVALID(channel_ctrl_v5_0_AXI_ARVALID),
        .AXI_AWADDR(channel_ctrl_v5_0_AXI_AWADDR),
        .AXI_AWBURST(channel_ctrl_v5_0_AXI_AWBURST),
        .AXI_AWID(channel_ctrl_v5_0_AXI_AWID),
        .AXI_AWLEN(channel_ctrl_v5_0_AXI_AWLEN),
        .AXI_AWREADY(channel_ctrl_v5_0_AXI_AWREADY),
        .AXI_AWSIZE(channel_ctrl_v5_0_AXI_AWSIZE),
        .AXI_AWVALID(channel_ctrl_v5_0_AXI_AWVALID),
        .AXI_BID(channel_ctrl_v5_0_AXI_BID),
        .AXI_BREADY(channel_ctrl_v5_0_AXI_BREADY),
        .AXI_BRESP(channel_ctrl_v5_0_AXI_BRESP),
        .AXI_BVALID(channel_ctrl_v5_0_AXI_BVALID),
        .AXI_RDATA(channel_ctrl_v5_0_AXI_RDATA),
        .AXI_RID(channel_ctrl_v5_0_AXI_RID),
        .AXI_RLAST(channel_ctrl_v5_0_AXI_RLAST),
        .AXI_RREADY(channel_ctrl_v5_0_AXI_RREADY),
        .AXI_RRESP(channel_ctrl_v5_0_AXI_RRESP),
        .AXI_RVALID(channel_ctrl_v5_0_AXI_RVALID),
        .AXI_WDATA(channel_ctrl_v5_0_AXI_WDATA),
        .AXI_WDATA_PARITY(NLW_channel_ctrl_v5_0_AXI_WDATA_PARITY_UNCONNECTED[31:0]),
        .AXI_WID(channel_ctrl_v5_0_AXI_WID),
        .AXI_WLAST(channel_ctrl_v5_0_AXI_WLAST),
        .AXI_WREADY(channel_ctrl_v5_0_AXI_WREADY),
        .AXI_WSTRB(channel_ctrl_v5_0_AXI_WSTRB),
        .AXI_WVALID(channel_ctrl_v5_0_AXI_WVALID),
        .FIFO_READ_tdata(axis_register_slice_1_M_AXIS_TDATA),
        .FIFO_READ_tready(axis_register_slice_1_M_AXIS_TREADY),
        .FIFO_READ_tvalid(axis_register_slice_1_M_AXIS_TVALID),
        .FIFO_WRITE_tdata(channel_ctrl_v5_0_FIFO_WRITE_TDATA),
        .FIFO_WRITE_tkeep(channel_ctrl_v5_0_FIFO_WRITE_TKEEP),
        .FIFO_WRITE_tlast(channel_ctrl_v5_0_FIFO_WRITE_TLAST),
        .FIFO_WRITE_tready(channel_ctrl_v5_0_FIFO_WRITE_TREADY),
        .FIFO_WRITE_tvalid(channel_ctrl_v5_0_FIFO_WRITE_TVALID),
        .busy(user_hbm_channel_ctrl_ports_busy),
        .clk(clk_450),
        .rd_blockamount(user_hbm_channel_ctrl_ports_rd_blockamount),
        .rd_running(rd_running),
        .rd_startaddress(user_hbm_channel_ctrl_ports_rd_startaddress),
        .wr_blockamount(user_hbm_channel_ctrl_ports_wr_blockamount),
        .wr_running(wr_running),
        .wr_startaddress(user_hbm_channel_ctrl_ports_wr_startaddress));
endmodule

(* X_CORE_INFO = "axi_register_slice_v2_1_27_axi_register_slice,Vivado 2022.2" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_axi_register_slice_0_0
   (aclk,
    aresetn,
    s_axi_awid,
    s_axi_awaddr,
    s_axi_awlen,
    s_axi_awsize,
    s_axi_awburst,
    s_axi_awlock,
    s_axi_awcache,
    s_axi_awprot,
    s_axi_awqos,
    s_axi_awvalid,
    s_axi_awready,
    s_axi_wid,
    s_axi_wdata,
    s_axi_wstrb,
    s_axi_wlast,
    s_axi_wvalid,
    s_axi_wready,
    s_axi_bid,
    s_axi_bresp,
    s_axi_bvalid,
    s_axi_bready,
    s_axi_arid,
    s_axi_araddr,
    s_axi_arlen,
    s_axi_arsize,
    s_axi_arburst,
    s_axi_arlock,
    s_axi_arcache,
    s_axi_arprot,
    s_axi_arqos,
    s_axi_arvalid,
    s_axi_arready,
    s_axi_rid,
    s_axi_rdata,
    s_axi_rresp,
    s_axi_rlast,
    s_axi_rvalid,
    s_axi_rready,
    m_axi_awid,
    m_axi_awaddr,
    m_axi_awlen,
    m_axi_awsize,
    m_axi_awburst,
    m_axi_awlock,
    m_axi_awcache,
    m_axi_awprot,
    m_axi_awqos,
    m_axi_awvalid,
    m_axi_awready,
    m_axi_wid,
    m_axi_wdata,
    m_axi_wstrb,
    m_axi_wlast,
    m_axi_wvalid,
    m_axi_wready,
    m_axi_bid,
    m_axi_bresp,
    m_axi_bvalid,
    m_axi_bready,
    m_axi_arid,
    m_axi_araddr,
    m_axi_arlen,
    m_axi_arsize,
    m_axi_arburst,
    m_axi_arlock,
    m_axi_arcache,
    m_axi_arprot,
    m_axi_arqos,
    m_axi_arvalid,
    m_axi_arready,
    m_axi_rid,
    m_axi_rdata,
    m_axi_rresp,
    m_axi_rlast,
    m_axi_rvalid,
    m_axi_rready);
  input aclk;
  input aresetn;
  input [5:0]s_axi_awid;
  input [32:0]s_axi_awaddr;
  input [3:0]s_axi_awlen;
  input [2:0]s_axi_awsize;
  input [1:0]s_axi_awburst;
  input [1:0]s_axi_awlock;
  input [3:0]s_axi_awcache;
  input [2:0]s_axi_awprot;
  input [3:0]s_axi_awqos;
  input s_axi_awvalid;
  output s_axi_awready;
  input [5:0]s_axi_wid;
  input [255:0]s_axi_wdata;
  input [31:0]s_axi_wstrb;
  input s_axi_wlast;
  input s_axi_wvalid;
  output s_axi_wready;
  output [5:0]s_axi_bid;
  output [1:0]s_axi_bresp;
  output s_axi_bvalid;
  input s_axi_bready;
  input [5:0]s_axi_arid;
  input [32:0]s_axi_araddr;
  input [3:0]s_axi_arlen;
  input [2:0]s_axi_arsize;
  input [1:0]s_axi_arburst;
  input [1:0]s_axi_arlock;
  input [3:0]s_axi_arcache;
  input [2:0]s_axi_arprot;
  input [3:0]s_axi_arqos;
  input s_axi_arvalid;
  output s_axi_arready;
  output [5:0]s_axi_rid;
  output [255:0]s_axi_rdata;
  output [1:0]s_axi_rresp;
  output s_axi_rlast;
  output s_axi_rvalid;
  input s_axi_rready;
  output [5:0]m_axi_awid;
  output [32:0]m_axi_awaddr;
  output [3:0]m_axi_awlen;
  output [2:0]m_axi_awsize;
  output [1:0]m_axi_awburst;
  output [1:0]m_axi_awlock;
  output [3:0]m_axi_awcache;
  output [2:0]m_axi_awprot;
  output [3:0]m_axi_awqos;
  output m_axi_awvalid;
  input m_axi_awready;
  output [5:0]m_axi_wid;
  output [255:0]m_axi_wdata;
  output [31:0]m_axi_wstrb;
  output m_axi_wlast;
  output m_axi_wvalid;
  input m_axi_wready;
  input [5:0]m_axi_bid;
  input [1:0]m_axi_bresp;
  input m_axi_bvalid;
  output m_axi_bready;
  output [5:0]m_axi_arid;
  output [32:0]m_axi_araddr;
  output [3:0]m_axi_arlen;
  output [2:0]m_axi_arsize;
  output [1:0]m_axi_arburst;
  output [1:0]m_axi_arlock;
  output [3:0]m_axi_arcache;
  output [2:0]m_axi_arprot;
  output [3:0]m_axi_arqos;
  output m_axi_arvalid;
  input m_axi_arready;
  input [5:0]m_axi_rid;
  input [255:0]m_axi_rdata;
  input [1:0]m_axi_rresp;
  input m_axi_rlast;
  input m_axi_rvalid;
  output m_axi_rready;


endmodule

(* X_CORE_INFO = "axis_data_fifo_v2_0_9_top,Vivado 2022.2" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_axis_data_fifo_0_0
   (s_axis_aresetn,
    s_axis_aclk,
    s_axis_tvalid,
    s_axis_tready,
    s_axis_tdata,
    s_axis_tkeep,
    s_axis_tlast,
    m_axis_aclk,
    m_axis_tvalid,
    m_axis_tready,
    m_axis_tdata,
    m_axis_tkeep,
    m_axis_tlast);
  input s_axis_aresetn;
  input s_axis_aclk;
  input s_axis_tvalid;
  output s_axis_tready;
  input [255:0]s_axis_tdata;
  input [31:0]s_axis_tkeep;
  input s_axis_tlast;
  input m_axis_aclk;
  output m_axis_tvalid;
  input m_axis_tready;
  output [255:0]m_axis_tdata;
  output [31:0]m_axis_tkeep;
  output m_axis_tlast;


endmodule

(* X_CORE_INFO = "axis_data_fifo_v2_0_9_top,Vivado 2022.2" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_axis_data_fifo_0_1
   (s_axis_aresetn,
    s_axis_aclk,
    s_axis_tvalid,
    s_axis_tready,
    s_axis_tdata,
    s_axis_tkeep,
    s_axis_tlast,
    m_axis_aclk,
    m_axis_tvalid,
    m_axis_tready,
    m_axis_tdata,
    m_axis_tkeep,
    m_axis_tlast);
  input s_axis_aresetn;
  input s_axis_aclk;
  input s_axis_tvalid;
  output s_axis_tready;
  input [255:0]s_axis_tdata;
  input [31:0]s_axis_tkeep;
  input s_axis_tlast;
  input m_axis_aclk;
  output m_axis_tvalid;
  input m_axis_tready;
  output [255:0]m_axis_tdata;
  output [31:0]m_axis_tkeep;
  output m_axis_tlast;


endmodule

(* X_CORE_INFO = "axis_register_slice_v1_1_27_axis_register_slice,Vivado 2022.2" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_axis_register_slice_0_0
   (aclk,
    aresetn,
    s_axis_tvalid,
    s_axis_tready,
    s_axis_tdata,
    s_axis_tkeep,
    s_axis_tlast,
    m_axis_tvalid,
    m_axis_tready,
    m_axis_tdata,
    m_axis_tkeep,
    m_axis_tlast);
  input aclk;
  input aresetn;
  input s_axis_tvalid;
  output s_axis_tready;
  input [255:0]s_axis_tdata;
  input [31:0]s_axis_tkeep;
  input s_axis_tlast;
  output m_axis_tvalid;
  input m_axis_tready;
  output [255:0]m_axis_tdata;
  output [31:0]m_axis_tkeep;
  output m_axis_tlast;


endmodule

(* X_CORE_INFO = "axis_register_slice_v1_1_27_axis_register_slice,Vivado 2022.2" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_axis_register_slice_0_1
   (aclk,
    aresetn,
    s_axis_tvalid,
    s_axis_tready,
    s_axis_tdata,
    s_axis_tkeep,
    s_axis_tlast,
    m_axis_tvalid,
    m_axis_tready,
    m_axis_tdata,
    m_axis_tkeep,
    m_axis_tlast);
  input aclk;
  input aresetn;
  input s_axis_tvalid;
  output s_axis_tready;
  input [255:0]s_axis_tdata;
  input [31:0]s_axis_tkeep;
  input s_axis_tlast;
  output m_axis_tvalid;
  input m_axis_tready;
  output [255:0]m_axis_tdata;
  output [31:0]m_axis_tkeep;
  output m_axis_tlast;


endmodule

(* X_CORE_INFO = "axis_register_slice_v1_1_27_axis_register_slice,Vivado 2022.2" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_axis_register_slice_2_0
   (aclk,
    aresetn,
    s_axis_tvalid,
    s_axis_tready,
    s_axis_tdata,
    s_axis_tkeep,
    s_axis_tlast,
    m_axis_tvalid,
    m_axis_tready,
    m_axis_tdata,
    m_axis_tkeep,
    m_axis_tlast);
  input aclk;
  input aresetn;
  input s_axis_tvalid;
  output s_axis_tready;
  input [255:0]s_axis_tdata;
  input [31:0]s_axis_tkeep;
  input s_axis_tlast;
  output m_axis_tvalid;
  input m_axis_tready;
  output [255:0]m_axis_tdata;
  output [31:0]m_axis_tkeep;
  output m_axis_tlast;


endmodule

(* X_CORE_INFO = "axis_register_slice_v1_1_27_axis_register_slice,Vivado 2022.2" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_axis_register_slice_3_0
   (aclk,
    aresetn,
    s_axis_tvalid,
    s_axis_tready,
    s_axis_tdata,
    s_axis_tkeep,
    s_axis_tlast,
    m_axis_tvalid,
    m_axis_tready,
    m_axis_tdata,
    m_axis_tkeep,
    m_axis_tlast);
  input aclk;
  input aresetn;
  input s_axis_tvalid;
  output s_axis_tready;
  input [255:0]s_axis_tdata;
  input [31:0]s_axis_tkeep;
  input s_axis_tlast;
  output m_axis_tvalid;
  input m_axis_tready;
  output [255:0]m_axis_tdata;
  output [31:0]m_axis_tkeep;
  output m_axis_tlast;


endmodule

(* X_CORE_INFO = "channel_ctrl_v5,Vivado 2022.2" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_channel_ctrl_v5_0_0
   (AXI_ARADDR,
    AXI_ARBURST,
    AXI_ARID,
    AXI_ARLEN,
    AXI_ARREADY,
    AXI_ARSIZE,
    AXI_ARVALID,
    AXI_AWADDR,
    AXI_AWBURST,
    AXI_AWID,
    AXI_AWLEN,
    AXI_AWREADY,
    AXI_AWSIZE,
    AXI_AWVALID,
    AXI_BID,
    AXI_BREADY,
    AXI_BRESP,
    AXI_BVALID,
    AXI_RDATA,
    AXI_RID,
    AXI_RLAST,
    AXI_RREADY,
    AXI_RRESP,
    AXI_RVALID,
    AXI_WID,
    AXI_WDATA,
    AXI_WLAST,
    AXI_WREADY,
    AXI_WSTRB,
    AXI_WVALID,
    AXI_WDATA_PARITY,
    FIFO_WRITE_tready,
    FIFO_WRITE_tdata,
    FIFO_WRITE_tvalid,
    FIFO_WRITE_tkeep,
    FIFO_WRITE_tlast,
    FIFO_READ_tvalid,
    FIFO_READ_tdata,
    FIFO_READ_tready,
    rd_running,
    rd_startaddress,
    rd_blockamount,
    wr_running,
    wr_startaddress,
    wr_blockamount,
    busy,
    clk);
  output [32:0]AXI_ARADDR;
  output [1:0]AXI_ARBURST;
  output [5:0]AXI_ARID;
  output [3:0]AXI_ARLEN;
  input AXI_ARREADY;
  output [2:0]AXI_ARSIZE;
  output AXI_ARVALID;
  output [32:0]AXI_AWADDR;
  output [1:0]AXI_AWBURST;
  output [5:0]AXI_AWID;
  output [3:0]AXI_AWLEN;
  input AXI_AWREADY;
  output [2:0]AXI_AWSIZE;
  output AXI_AWVALID;
  input [5:0]AXI_BID;
  output AXI_BREADY;
  input [1:0]AXI_BRESP;
  input AXI_BVALID;
  input [255:0]AXI_RDATA;
  input [5:0]AXI_RID;
  input AXI_RLAST;
  output AXI_RREADY;
  input [1:0]AXI_RRESP;
  input AXI_RVALID;
  output [5:0]AXI_WID;
  output [255:0]AXI_WDATA;
  output AXI_WLAST;
  input AXI_WREADY;
  output [31:0]AXI_WSTRB;
  output AXI_WVALID;
  output [31:0]AXI_WDATA_PARITY;
  input FIFO_WRITE_tready;
  output [255:0]FIFO_WRITE_tdata;
  output FIFO_WRITE_tvalid;
  output [31:0]FIFO_WRITE_tkeep;
  output FIFO_WRITE_tlast;
  input FIFO_READ_tvalid;
  input [255:0]FIFO_READ_tdata;
  output FIFO_READ_tready;
  input rd_running;
  input [31:0]rd_startaddress;
  input [31:0]rd_blockamount;
  input wr_running;
  input [31:0]wr_startaddress;
  input [31:0]wr_blockamount;
  output busy;
  input clk;


endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_wrapper
   (HBM_AXI_araddr,
    HBM_AXI_arburst,
    HBM_AXI_arid,
    HBM_AXI_arlen,
    HBM_AXI_arsize,
    HBM_AXI_arvalid,
    HBM_AXI_awaddr,
    HBM_AXI_awburst,
    HBM_AXI_awid,
    HBM_AXI_awlen,
    HBM_AXI_awsize,
    HBM_AXI_awvalid,
    HBM_AXI_bready,
    HBM_AXI_rready,
    HBM_AXI_wdata,
    HBM_AXI_wid,
    HBM_AXI_wlast,
    HBM_AXI_wstrb,
    HBM_AXI_wvalid,
    HBM_READ_tdata,
    HBM_READ_tkeep,
    HBM_READ_tlast,
    HBM_READ_tvalid,
    HBM_WRITE_tready,
    user_hbm_channel_ctrl_ports_busy,
    HBM_AXI_arready,
    HBM_AXI_awready,
    HBM_AXI_bid,
    HBM_AXI_bresp,
    HBM_AXI_bvalid,
    HBM_AXI_rdata,
    HBM_AXI_rid,
    HBM_AXI_rlast,
    HBM_AXI_rresp,
    HBM_AXI_rvalid,
    HBM_AXI_wready,
    HBM_READ_tready,
    HBM_WRITE_tdata,
    HBM_WRITE_tvalid,
    clk_312,
    clk_450,
    rd_running,
    user_hbm_channel_ctrl_ports_rd_blockamount,
    user_hbm_channel_ctrl_ports_rd_startaddress,
    user_hbm_channel_ctrl_ports_wr_blockamount,
    user_hbm_channel_ctrl_ports_wr_startaddress,
    wr_running);
  output [32:0]HBM_AXI_araddr;
  output [1:0]HBM_AXI_arburst;
  output [5:0]HBM_AXI_arid;
  output [3:0]HBM_AXI_arlen;
  output [2:0]HBM_AXI_arsize;
  output HBM_AXI_arvalid;
  output [32:0]HBM_AXI_awaddr;
  output [1:0]HBM_AXI_awburst;
  output [5:0]HBM_AXI_awid;
  output [3:0]HBM_AXI_awlen;
  output [2:0]HBM_AXI_awsize;
  output HBM_AXI_awvalid;
  output HBM_AXI_bready;
  output HBM_AXI_rready;
  output [255:0]HBM_AXI_wdata;
  output [5:0]HBM_AXI_wid;
  output HBM_AXI_wlast;
  output [31:0]HBM_AXI_wstrb;
  output HBM_AXI_wvalid;
  output [255:0]HBM_READ_tdata;
  output [31:0]HBM_READ_tkeep;
  output HBM_READ_tlast;
  output HBM_READ_tvalid;
  output HBM_WRITE_tready;
  output user_hbm_channel_ctrl_ports_busy;
  input HBM_AXI_arready;
  input HBM_AXI_awready;
  input [5:0]HBM_AXI_bid;
  input [1:0]HBM_AXI_bresp;
  input HBM_AXI_bvalid;
  input [255:0]HBM_AXI_rdata;
  input [5:0]HBM_AXI_rid;
  input HBM_AXI_rlast;
  input [1:0]HBM_AXI_rresp;
  input HBM_AXI_rvalid;
  input HBM_AXI_wready;
  input HBM_READ_tready;
  input [255:0]HBM_WRITE_tdata;
  input HBM_WRITE_tvalid;
  input clk_312;
  input clk_450;
  input rd_running;
  input [31:0]user_hbm_channel_ctrl_ports_rd_blockamount;
  input [31:0]user_hbm_channel_ctrl_ports_rd_startaddress;
  input [31:0]user_hbm_channel_ctrl_ports_wr_blockamount;
  input [31:0]user_hbm_channel_ctrl_ports_wr_startaddress;
  input wr_running;

  wire [32:0]HBM_AXI_araddr;
  wire [1:0]HBM_AXI_arburst;
  wire [5:0]HBM_AXI_arid;
  wire [3:0]HBM_AXI_arlen;
  wire HBM_AXI_arready;
  wire [2:0]HBM_AXI_arsize;
  wire HBM_AXI_arvalid;
  wire [32:0]HBM_AXI_awaddr;
  wire [1:0]HBM_AXI_awburst;
  wire [5:0]HBM_AXI_awid;
  wire [3:0]HBM_AXI_awlen;
  wire HBM_AXI_awready;
  wire [2:0]HBM_AXI_awsize;
  wire HBM_AXI_awvalid;
  wire [5:0]HBM_AXI_bid;
  wire HBM_AXI_bready;
  wire [1:0]HBM_AXI_bresp;
  wire HBM_AXI_bvalid;
  wire [255:0]HBM_AXI_rdata;
  wire [5:0]HBM_AXI_rid;
  wire HBM_AXI_rlast;
  wire HBM_AXI_rready;
  wire [1:0]HBM_AXI_rresp;
  wire HBM_AXI_rvalid;
  wire [255:0]HBM_AXI_wdata;
  wire [5:0]HBM_AXI_wid;
  wire HBM_AXI_wlast;
  wire HBM_AXI_wready;
  wire [31:0]HBM_AXI_wstrb;
  wire HBM_AXI_wvalid;
  wire [255:0]HBM_READ_tdata;
  wire [31:0]HBM_READ_tkeep;
  wire HBM_READ_tlast;
  wire HBM_READ_tready;
  wire HBM_READ_tvalid;
  wire [255:0]HBM_WRITE_tdata;
  wire HBM_WRITE_tready;
  wire HBM_WRITE_tvalid;
  wire clk_312;
  wire clk_450;
  wire rd_running;
  wire user_hbm_channel_ctrl_ports_busy;
  wire [31:0]user_hbm_channel_ctrl_ports_rd_blockamount;
  wire [31:0]user_hbm_channel_ctrl_ports_rd_startaddress;
  wire [31:0]user_hbm_channel_ctrl_ports_wr_blockamount;
  wire [31:0]user_hbm_channel_ctrl_ports_wr_startaddress;
  wire wr_running;
  wire [3:0]NLW_design_hbm_channel_i_HBM_AXI_arcache_UNCONNECTED;
  wire [1:0]NLW_design_hbm_channel_i_HBM_AXI_arlock_UNCONNECTED;
  wire [2:0]NLW_design_hbm_channel_i_HBM_AXI_arprot_UNCONNECTED;
  wire [3:0]NLW_design_hbm_channel_i_HBM_AXI_arqos_UNCONNECTED;
  wire [3:0]NLW_design_hbm_channel_i_HBM_AXI_awcache_UNCONNECTED;
  wire [1:0]NLW_design_hbm_channel_i_HBM_AXI_awlock_UNCONNECTED;
  wire [2:0]NLW_design_hbm_channel_i_HBM_AXI_awprot_UNCONNECTED;
  wire [3:0]NLW_design_hbm_channel_i_HBM_AXI_awqos_UNCONNECTED;

  (* HW_HANDOFF = "design_hbm_channel.hwdef" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel design_hbm_channel_i
       (.HBM_AXI_araddr(HBM_AXI_araddr),
        .HBM_AXI_arburst(HBM_AXI_arburst),
        .HBM_AXI_arcache(NLW_design_hbm_channel_i_HBM_AXI_arcache_UNCONNECTED[3:0]),
        .HBM_AXI_arid(HBM_AXI_arid),
        .HBM_AXI_arlen(HBM_AXI_arlen),
        .HBM_AXI_arlock(NLW_design_hbm_channel_i_HBM_AXI_arlock_UNCONNECTED[1:0]),
        .HBM_AXI_arprot(NLW_design_hbm_channel_i_HBM_AXI_arprot_UNCONNECTED[2:0]),
        .HBM_AXI_arqos(NLW_design_hbm_channel_i_HBM_AXI_arqos_UNCONNECTED[3:0]),
        .HBM_AXI_arready(HBM_AXI_arready),
        .HBM_AXI_arsize(HBM_AXI_arsize),
        .HBM_AXI_arvalid(HBM_AXI_arvalid),
        .HBM_AXI_awaddr(HBM_AXI_awaddr),
        .HBM_AXI_awburst(HBM_AXI_awburst),
        .HBM_AXI_awcache(NLW_design_hbm_channel_i_HBM_AXI_awcache_UNCONNECTED[3:0]),
        .HBM_AXI_awid(HBM_AXI_awid),
        .HBM_AXI_awlen(HBM_AXI_awlen),
        .HBM_AXI_awlock(NLW_design_hbm_channel_i_HBM_AXI_awlock_UNCONNECTED[1:0]),
        .HBM_AXI_awprot(NLW_design_hbm_channel_i_HBM_AXI_awprot_UNCONNECTED[2:0]),
        .HBM_AXI_awqos(NLW_design_hbm_channel_i_HBM_AXI_awqos_UNCONNECTED[3:0]),
        .HBM_AXI_awready(HBM_AXI_awready),
        .HBM_AXI_awsize(HBM_AXI_awsize),
        .HBM_AXI_awvalid(HBM_AXI_awvalid),
        .HBM_AXI_bid(HBM_AXI_bid),
        .HBM_AXI_bready(HBM_AXI_bready),
        .HBM_AXI_bresp(HBM_AXI_bresp),
        .HBM_AXI_bvalid(HBM_AXI_bvalid),
        .HBM_AXI_rdata(HBM_AXI_rdata),
        .HBM_AXI_rid(HBM_AXI_rid),
        .HBM_AXI_rlast(HBM_AXI_rlast),
        .HBM_AXI_rready(HBM_AXI_rready),
        .HBM_AXI_rresp(HBM_AXI_rresp),
        .HBM_AXI_rvalid(HBM_AXI_rvalid),
        .HBM_AXI_wdata(HBM_AXI_wdata),
        .HBM_AXI_wid(HBM_AXI_wid),
        .HBM_AXI_wlast(HBM_AXI_wlast),
        .HBM_AXI_wready(HBM_AXI_wready),
        .HBM_AXI_wstrb(HBM_AXI_wstrb),
        .HBM_AXI_wvalid(HBM_AXI_wvalid),
        .HBM_READ_tdata(HBM_READ_tdata),
        .HBM_READ_tkeep(HBM_READ_tkeep),
        .HBM_READ_tlast(HBM_READ_tlast),
        .HBM_READ_tready(HBM_READ_tready),
        .HBM_READ_tvalid(HBM_READ_tvalid),
        .HBM_WRITE_tdata(HBM_WRITE_tdata),
        .HBM_WRITE_tkeep({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .HBM_WRITE_tlast(1'b0),
        .HBM_WRITE_tready(HBM_WRITE_tready),
        .HBM_WRITE_tvalid(HBM_WRITE_tvalid),
        .clk_312(clk_312),
        .clk_450(clk_450),
        .rd_running(rd_running),
        .user_hbm_channel_ctrl_ports_busy(user_hbm_channel_ctrl_ports_busy),
        .user_hbm_channel_ctrl_ports_rd_blockamount(user_hbm_channel_ctrl_ports_rd_blockamount),
        .user_hbm_channel_ctrl_ports_rd_startaddress(user_hbm_channel_ctrl_ports_rd_startaddress),
        .user_hbm_channel_ctrl_ports_wr_blockamount(user_hbm_channel_ctrl_ports_wr_blockamount),
        .user_hbm_channel_ctrl_ports_wr_startaddress(user_hbm_channel_ctrl_ports_wr_startaddress),
        .wr_running(wr_running));
endmodule

(* CHECK_LICENSE_TYPE = "design_hbm_design_hbm_channel_w_0_4,design_hbm_channel_wrapper,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* IP_DEFINITION_SOURCE = "module_ref" *) 
(* X_CORE_INFO = "design_hbm_channel_wrapper,Vivado 2022.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (HBM_AXI_araddr,
    HBM_AXI_arburst,
    HBM_AXI_arid,
    HBM_AXI_arlen,
    HBM_AXI_arready,
    HBM_AXI_arsize,
    HBM_AXI_arvalid,
    HBM_AXI_awaddr,
    HBM_AXI_awburst,
    HBM_AXI_awid,
    HBM_AXI_awlen,
    HBM_AXI_awready,
    HBM_AXI_awsize,
    HBM_AXI_awvalid,
    HBM_AXI_bid,
    HBM_AXI_bready,
    HBM_AXI_bresp,
    HBM_AXI_bvalid,
    HBM_AXI_rdata,
    HBM_AXI_rid,
    HBM_AXI_rlast,
    HBM_AXI_rready,
    HBM_AXI_rresp,
    HBM_AXI_rvalid,
    HBM_AXI_wdata,
    HBM_AXI_wid,
    HBM_AXI_wlast,
    HBM_AXI_wready,
    HBM_AXI_wstrb,
    HBM_AXI_wvalid,
    HBM_READ_tdata,
    HBM_READ_tkeep,
    HBM_READ_tlast,
    HBM_READ_tready,
    HBM_READ_tvalid,
    HBM_WRITE_tdata,
    HBM_WRITE_tready,
    HBM_WRITE_tvalid,
    clk_312,
    clk_450,
    rd_running,
    user_hbm_channel_ctrl_ports_busy,
    user_hbm_channel_ctrl_ports_rd_blockamount,
    user_hbm_channel_ctrl_ports_rd_startaddress,
    user_hbm_channel_ctrl_ports_wr_blockamount,
    user_hbm_channel_ctrl_ports_wr_startaddress,
    wr_running);
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI ARADDR" *) output [32:0]HBM_AXI_araddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI ARBURST" *) output [1:0]HBM_AXI_arburst;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI ARID" *) output [5:0]HBM_AXI_arid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI ARLEN" *) output [3:0]HBM_AXI_arlen;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI ARREADY" *) input HBM_AXI_arready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI ARSIZE" *) output [2:0]HBM_AXI_arsize;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI ARVALID" *) output HBM_AXI_arvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI AWADDR" *) output [32:0]HBM_AXI_awaddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI AWBURST" *) output [1:0]HBM_AXI_awburst;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI AWID" *) output [5:0]HBM_AXI_awid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI AWLEN" *) output [3:0]HBM_AXI_awlen;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI AWREADY" *) input HBM_AXI_awready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI AWSIZE" *) output [2:0]HBM_AXI_awsize;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI AWVALID" *) output HBM_AXI_awvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI BID" *) input [5:0]HBM_AXI_bid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI BREADY" *) output HBM_AXI_bready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI BRESP" *) input [1:0]HBM_AXI_bresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI BVALID" *) input HBM_AXI_bvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI RDATA" *) input [255:0]HBM_AXI_rdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI RID" *) input [5:0]HBM_AXI_rid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI RLAST" *) input HBM_AXI_rlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI RREADY" *) output HBM_AXI_rready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI RRESP" *) input [1:0]HBM_AXI_rresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI RVALID" *) input HBM_AXI_rvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI WDATA" *) output [255:0]HBM_AXI_wdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI WID" *) output [5:0]HBM_AXI_wid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI WLAST" *) output HBM_AXI_wlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI WREADY" *) input HBM_AXI_wready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI WSTRB" *) output [31:0]HBM_AXI_wstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 HBM_AXI WVALID" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME HBM_AXI, DATA_WIDTH 256, PROTOCOL AXI3, FREQ_HZ 450000000, ID_WIDTH 6, ADDR_WIDTH 33, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 1, HAS_LOCK 0, HAS_PROT 0, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 16, PHASE 0.0, CLK_DOMAIN design_hbm_clk_wiz_1_0_clk_out1, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) output HBM_AXI_wvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_READ TDATA" *) output [255:0]HBM_READ_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_READ TKEEP" *) output [31:0]HBM_READ_tkeep;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_READ TLAST" *) output HBM_READ_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_READ TREADY" *) input HBM_READ_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_READ TVALID" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME HBM_READ, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN design_hbm_clk_312MHz, LAYERED_METADATA undef, INSERT_VIP 0" *) output HBM_READ_tvalid;
  (* CLK_DOMAIN = "design_hbm_clk_312MHz" *) (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_WRITE TDATA" *) input [255:0]HBM_WRITE_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_WRITE TREADY" *) output HBM_WRITE_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 HBM_WRITE TVALID" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME HBM_WRITE, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.0, LAYERED_METADATA undef, INSERT_VIP 0" *) input HBM_WRITE_tvalid;
  input clk_312;
  input clk_450;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 ctrl_ports rd_run" *) input rd_running;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 ctrl_ports busy" *) output user_hbm_channel_ctrl_ports_busy;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 ctrl_ports rd_blockamount" *) (* X_INTERFACE_MODE = "slave" *) input [31:0]user_hbm_channel_ctrl_ports_rd_blockamount;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 ctrl_ports rd_startaddress" *) input [31:0]user_hbm_channel_ctrl_ports_rd_startaddress;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 ctrl_ports wr_blockamount" *) input [31:0]user_hbm_channel_ctrl_ports_wr_blockamount;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 ctrl_ports wr_startaddress" *) input [31:0]user_hbm_channel_ctrl_ports_wr_startaddress;
  (* X_INTERFACE_INFO = "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 ctrl_ports wr_run" *) input wr_running;

  wire [32:0]HBM_AXI_araddr;
  wire [1:0]HBM_AXI_arburst;
  wire [5:0]HBM_AXI_arid;
  wire [3:0]HBM_AXI_arlen;
  wire HBM_AXI_arready;
  wire [2:0]HBM_AXI_arsize;
  wire HBM_AXI_arvalid;
  wire [32:0]HBM_AXI_awaddr;
  wire [1:0]HBM_AXI_awburst;
  wire [5:0]HBM_AXI_awid;
  wire [3:0]HBM_AXI_awlen;
  wire HBM_AXI_awready;
  wire [2:0]HBM_AXI_awsize;
  wire HBM_AXI_awvalid;
  wire [5:0]HBM_AXI_bid;
  wire HBM_AXI_bready;
  wire [1:0]HBM_AXI_bresp;
  wire HBM_AXI_bvalid;
  wire [255:0]HBM_AXI_rdata;
  wire [5:0]HBM_AXI_rid;
  wire HBM_AXI_rlast;
  wire HBM_AXI_rready;
  wire [1:0]HBM_AXI_rresp;
  wire HBM_AXI_rvalid;
  wire [255:0]HBM_AXI_wdata;
  wire [5:0]HBM_AXI_wid;
  wire HBM_AXI_wlast;
  wire HBM_AXI_wready;
  wire [31:0]HBM_AXI_wstrb;
  wire HBM_AXI_wvalid;
  wire [255:0]HBM_READ_tdata;
  wire [31:0]HBM_READ_tkeep;
  wire HBM_READ_tlast;
  wire HBM_READ_tready;
  wire HBM_READ_tvalid;
  wire [255:0]HBM_WRITE_tdata;
  wire HBM_WRITE_tready;
  wire HBM_WRITE_tvalid;
  wire clk_312;
  wire clk_450;
  wire rd_running;
  wire user_hbm_channel_ctrl_ports_busy;
  wire [31:0]user_hbm_channel_ctrl_ports_rd_blockamount;
  wire [31:0]user_hbm_channel_ctrl_ports_rd_startaddress;
  wire [31:0]user_hbm_channel_ctrl_ports_wr_blockamount;
  wire [31:0]user_hbm_channel_ctrl_ports_wr_startaddress;
  wire wr_running;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_wrapper inst
       (.HBM_AXI_araddr(HBM_AXI_araddr),
        .HBM_AXI_arburst(HBM_AXI_arburst),
        .HBM_AXI_arid(HBM_AXI_arid),
        .HBM_AXI_arlen(HBM_AXI_arlen),
        .HBM_AXI_arready(HBM_AXI_arready),
        .HBM_AXI_arsize(HBM_AXI_arsize),
        .HBM_AXI_arvalid(HBM_AXI_arvalid),
        .HBM_AXI_awaddr(HBM_AXI_awaddr),
        .HBM_AXI_awburst(HBM_AXI_awburst),
        .HBM_AXI_awid(HBM_AXI_awid),
        .HBM_AXI_awlen(HBM_AXI_awlen),
        .HBM_AXI_awready(HBM_AXI_awready),
        .HBM_AXI_awsize(HBM_AXI_awsize),
        .HBM_AXI_awvalid(HBM_AXI_awvalid),
        .HBM_AXI_bid(HBM_AXI_bid),
        .HBM_AXI_bready(HBM_AXI_bready),
        .HBM_AXI_bresp(HBM_AXI_bresp),
        .HBM_AXI_bvalid(HBM_AXI_bvalid),
        .HBM_AXI_rdata(HBM_AXI_rdata),
        .HBM_AXI_rid(HBM_AXI_rid),
        .HBM_AXI_rlast(HBM_AXI_rlast),
        .HBM_AXI_rready(HBM_AXI_rready),
        .HBM_AXI_rresp(HBM_AXI_rresp),
        .HBM_AXI_rvalid(HBM_AXI_rvalid),
        .HBM_AXI_wdata(HBM_AXI_wdata),
        .HBM_AXI_wid(HBM_AXI_wid),
        .HBM_AXI_wlast(HBM_AXI_wlast),
        .HBM_AXI_wready(HBM_AXI_wready),
        .HBM_AXI_wstrb(HBM_AXI_wstrb),
        .HBM_AXI_wvalid(HBM_AXI_wvalid),
        .HBM_READ_tdata(HBM_READ_tdata),
        .HBM_READ_tkeep(HBM_READ_tkeep),
        .HBM_READ_tlast(HBM_READ_tlast),
        .HBM_READ_tready(HBM_READ_tready),
        .HBM_READ_tvalid(HBM_READ_tvalid),
        .HBM_WRITE_tdata(HBM_WRITE_tdata),
        .HBM_WRITE_tready(HBM_WRITE_tready),
        .HBM_WRITE_tvalid(HBM_WRITE_tvalid),
        .clk_312(clk_312),
        .clk_450(clk_450),
        .rd_running(rd_running),
        .user_hbm_channel_ctrl_ports_busy(user_hbm_channel_ctrl_ports_busy),
        .user_hbm_channel_ctrl_ports_rd_blockamount(user_hbm_channel_ctrl_ports_rd_blockamount),
        .user_hbm_channel_ctrl_ports_rd_startaddress(user_hbm_channel_ctrl_ports_rd_startaddress),
        .user_hbm_channel_ctrl_ports_wr_blockamount(user_hbm_channel_ctrl_ports_wr_blockamount),
        .user_hbm_channel_ctrl_ports_wr_startaddress(user_hbm_channel_ctrl_ports_wr_startaddress),
        .wr_running(wr_running));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
