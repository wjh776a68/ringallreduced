-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
-- Date        : Tue Dec  6 16:31:49 2022
-- Host        : dell-Vostro-3910-China-HDD-Protection running 64-bit Ubuntu 22.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_hbm_design_hbm_channel_w_0_1_sim_netlist.vhdl
-- Design      : design_hbm_design_hbm_channel_w_0_1
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xcvu37p-fsvh2892-2L-e
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel is
  port (
    HBM_AXI_araddr : out STD_LOGIC_VECTOR ( 32 downto 0 );
    HBM_AXI_arburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    HBM_AXI_arcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
    HBM_AXI_arid : out STD_LOGIC_VECTOR ( 5 downto 0 );
    HBM_AXI_arlen : out STD_LOGIC_VECTOR ( 3 downto 0 );
    HBM_AXI_arlock : out STD_LOGIC_VECTOR ( 1 downto 0 );
    HBM_AXI_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    HBM_AXI_arqos : out STD_LOGIC_VECTOR ( 3 downto 0 );
    HBM_AXI_arready : in STD_LOGIC;
    HBM_AXI_arsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    HBM_AXI_arvalid : out STD_LOGIC;
    HBM_AXI_awaddr : out STD_LOGIC_VECTOR ( 32 downto 0 );
    HBM_AXI_awburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    HBM_AXI_awcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
    HBM_AXI_awid : out STD_LOGIC_VECTOR ( 5 downto 0 );
    HBM_AXI_awlen : out STD_LOGIC_VECTOR ( 3 downto 0 );
    HBM_AXI_awlock : out STD_LOGIC_VECTOR ( 1 downto 0 );
    HBM_AXI_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    HBM_AXI_awqos : out STD_LOGIC_VECTOR ( 3 downto 0 );
    HBM_AXI_awready : in STD_LOGIC;
    HBM_AXI_awsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    HBM_AXI_awvalid : out STD_LOGIC;
    HBM_AXI_bid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    HBM_AXI_bready : out STD_LOGIC;
    HBM_AXI_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    HBM_AXI_bvalid : in STD_LOGIC;
    HBM_AXI_rdata : in STD_LOGIC_VECTOR ( 255 downto 0 );
    HBM_AXI_rid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    HBM_AXI_rlast : in STD_LOGIC;
    HBM_AXI_rready : out STD_LOGIC;
    HBM_AXI_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    HBM_AXI_rvalid : in STD_LOGIC;
    HBM_AXI_wdata : out STD_LOGIC_VECTOR ( 255 downto 0 );
    HBM_AXI_wid : out STD_LOGIC_VECTOR ( 5 downto 0 );
    HBM_AXI_wlast : out STD_LOGIC;
    HBM_AXI_wready : in STD_LOGIC;
    HBM_AXI_wstrb : out STD_LOGIC_VECTOR ( 31 downto 0 );
    HBM_AXI_wvalid : out STD_LOGIC;
    HBM_READ_tdata : out STD_LOGIC_VECTOR ( 255 downto 0 );
    HBM_READ_tkeep : out STD_LOGIC_VECTOR ( 31 downto 0 );
    HBM_READ_tlast : out STD_LOGIC;
    HBM_READ_tready : in STD_LOGIC;
    HBM_READ_tvalid : out STD_LOGIC;
    HBM_WRITE_tdata : in STD_LOGIC_VECTOR ( 255 downto 0 );
    HBM_WRITE_tkeep : in STD_LOGIC_VECTOR ( 31 downto 0 );
    HBM_WRITE_tlast : in STD_LOGIC;
    HBM_WRITE_tready : out STD_LOGIC;
    HBM_WRITE_tvalid : in STD_LOGIC;
    clk_312 : in STD_LOGIC;
    clk_450 : in STD_LOGIC;
    rd_running : in STD_LOGIC;
    user_hbm_channel_ctrl_ports_busy : out STD_LOGIC;
    user_hbm_channel_ctrl_ports_rd_blockamount : in STD_LOGIC_VECTOR ( 31 downto 0 );
    user_hbm_channel_ctrl_ports_rd_startaddress : in STD_LOGIC_VECTOR ( 31 downto 0 );
    user_hbm_channel_ctrl_ports_wr_blockamount : in STD_LOGIC_VECTOR ( 31 downto 0 );
    user_hbm_channel_ctrl_ports_wr_startaddress : in STD_LOGIC_VECTOR ( 31 downto 0 );
    wr_running : in STD_LOGIC
  );
  attribute HW_HANDOFF : string;
  attribute HW_HANDOFF of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel : entity is "design_hbm_channel.hwdef";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel is
  component decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_axi_register_slice_0_0 is
  port (
    aclk : in STD_LOGIC;
    aresetn : in STD_LOGIC;
    s_axi_awid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 32 downto 0 );
    s_axi_awlen : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_awlock : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_awvalid : in STD_LOGIC;
    s_axi_awready : out STD_LOGIC;
    s_axi_wid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    s_axi_wdata : in STD_LOGIC_VECTOR ( 255 downto 0 );
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_wlast : in STD_LOGIC;
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_bid : out STD_LOGIC_VECTOR ( 5 downto 0 );
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bvalid : out STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_arid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    s_axi_araddr : in STD_LOGIC_VECTOR ( 32 downto 0 );
    s_axi_arlen : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_arlock : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_arvalid : in STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    s_axi_rid : out STD_LOGIC_VECTOR ( 5 downto 0 );
    s_axi_rdata : out STD_LOGIC_VECTOR ( 255 downto 0 );
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rlast : out STD_LOGIC;
    s_axi_rvalid : out STD_LOGIC;
    s_axi_rready : in STD_LOGIC;
    m_axi_awid : out STD_LOGIC_VECTOR ( 5 downto 0 );
    m_axi_awaddr : out STD_LOGIC_VECTOR ( 32 downto 0 );
    m_axi_awlen : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_awsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_axi_awburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_awlock : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_awcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_axi_awqos : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_awvalid : out STD_LOGIC;
    m_axi_awready : in STD_LOGIC;
    m_axi_wid : out STD_LOGIC_VECTOR ( 5 downto 0 );
    m_axi_wdata : out STD_LOGIC_VECTOR ( 255 downto 0 );
    m_axi_wstrb : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axi_wlast : out STD_LOGIC;
    m_axi_wvalid : out STD_LOGIC;
    m_axi_wready : in STD_LOGIC;
    m_axi_bid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    m_axi_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_bvalid : in STD_LOGIC;
    m_axi_bready : out STD_LOGIC;
    m_axi_arid : out STD_LOGIC_VECTOR ( 5 downto 0 );
    m_axi_araddr : out STD_LOGIC_VECTOR ( 32 downto 0 );
    m_axi_arlen : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_arsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_axi_arburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_arlock : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_arcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_axi_arqos : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_arvalid : out STD_LOGIC;
    m_axi_arready : in STD_LOGIC;
    m_axi_rid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    m_axi_rdata : in STD_LOGIC_VECTOR ( 255 downto 0 );
    m_axi_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_rlast : in STD_LOGIC;
    m_axi_rvalid : in STD_LOGIC;
    m_axi_rready : out STD_LOGIC
  );
  end component decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_axi_register_slice_0_0;
  component decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_axis_data_fifo_0_0 is
  port (
    s_axis_aresetn : in STD_LOGIC;
    s_axis_aclk : in STD_LOGIC;
    s_axis_tvalid : in STD_LOGIC;
    s_axis_tready : out STD_LOGIC;
    s_axis_tdata : in STD_LOGIC_VECTOR ( 255 downto 0 );
    s_axis_tkeep : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axis_tlast : in STD_LOGIC;
    m_axis_aclk : in STD_LOGIC;
    m_axis_tvalid : out STD_LOGIC;
    m_axis_tready : in STD_LOGIC;
    m_axis_tdata : out STD_LOGIC_VECTOR ( 255 downto 0 );
    m_axis_tkeep : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axis_tlast : out STD_LOGIC
  );
  end component decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_axis_data_fifo_0_0;
  component decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_axis_data_fifo_0_1 is
  port (
    s_axis_aresetn : in STD_LOGIC;
    s_axis_aclk : in STD_LOGIC;
    s_axis_tvalid : in STD_LOGIC;
    s_axis_tready : out STD_LOGIC;
    s_axis_tdata : in STD_LOGIC_VECTOR ( 255 downto 0 );
    s_axis_tkeep : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axis_tlast : in STD_LOGIC;
    m_axis_aclk : in STD_LOGIC;
    m_axis_tvalid : out STD_LOGIC;
    m_axis_tready : in STD_LOGIC;
    m_axis_tdata : out STD_LOGIC_VECTOR ( 255 downto 0 );
    m_axis_tkeep : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axis_tlast : out STD_LOGIC
  );
  end component decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_axis_data_fifo_0_1;
  component decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_axis_register_slice_0_0 is
  port (
    aclk : in STD_LOGIC;
    aresetn : in STD_LOGIC;
    s_axis_tvalid : in STD_LOGIC;
    s_axis_tready : out STD_LOGIC;
    s_axis_tdata : in STD_LOGIC_VECTOR ( 255 downto 0 );
    s_axis_tkeep : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axis_tlast : in STD_LOGIC;
    m_axis_tvalid : out STD_LOGIC;
    m_axis_tready : in STD_LOGIC;
    m_axis_tdata : out STD_LOGIC_VECTOR ( 255 downto 0 );
    m_axis_tkeep : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axis_tlast : out STD_LOGIC
  );
  end component decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_axis_register_slice_0_0;
  component decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_axis_register_slice_0_1 is
  port (
    aclk : in STD_LOGIC;
    aresetn : in STD_LOGIC;
    s_axis_tvalid : in STD_LOGIC;
    s_axis_tready : out STD_LOGIC;
    s_axis_tdata : in STD_LOGIC_VECTOR ( 255 downto 0 );
    s_axis_tkeep : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axis_tlast : in STD_LOGIC;
    m_axis_tvalid : out STD_LOGIC;
    m_axis_tready : in STD_LOGIC;
    m_axis_tdata : out STD_LOGIC_VECTOR ( 255 downto 0 );
    m_axis_tkeep : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axis_tlast : out STD_LOGIC
  );
  end component decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_axis_register_slice_0_1;
  component decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_axis_register_slice_2_0 is
  port (
    aclk : in STD_LOGIC;
    aresetn : in STD_LOGIC;
    s_axis_tvalid : in STD_LOGIC;
    s_axis_tready : out STD_LOGIC;
    s_axis_tdata : in STD_LOGIC_VECTOR ( 255 downto 0 );
    s_axis_tkeep : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axis_tlast : in STD_LOGIC;
    m_axis_tvalid : out STD_LOGIC;
    m_axis_tready : in STD_LOGIC;
    m_axis_tdata : out STD_LOGIC_VECTOR ( 255 downto 0 );
    m_axis_tkeep : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axis_tlast : out STD_LOGIC
  );
  end component decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_axis_register_slice_2_0;
  component decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_axis_register_slice_3_0 is
  port (
    aclk : in STD_LOGIC;
    aresetn : in STD_LOGIC;
    s_axis_tvalid : in STD_LOGIC;
    s_axis_tready : out STD_LOGIC;
    s_axis_tdata : in STD_LOGIC_VECTOR ( 255 downto 0 );
    s_axis_tkeep : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axis_tlast : in STD_LOGIC;
    m_axis_tvalid : out STD_LOGIC;
    m_axis_tready : in STD_LOGIC;
    m_axis_tdata : out STD_LOGIC_VECTOR ( 255 downto 0 );
    m_axis_tkeep : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axis_tlast : out STD_LOGIC
  );
  end component decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_axis_register_slice_3_0;
  component decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_channel_ctrl_v5_0_0 is
  port (
    AXI_ARADDR : out STD_LOGIC_VECTOR ( 32 downto 0 );
    AXI_ARBURST : out STD_LOGIC_VECTOR ( 1 downto 0 );
    AXI_ARID : out STD_LOGIC_VECTOR ( 5 downto 0 );
    AXI_ARLEN : out STD_LOGIC_VECTOR ( 3 downto 0 );
    AXI_ARREADY : in STD_LOGIC;
    AXI_ARSIZE : out STD_LOGIC_VECTOR ( 2 downto 0 );
    AXI_ARVALID : out STD_LOGIC;
    AXI_AWADDR : out STD_LOGIC_VECTOR ( 32 downto 0 );
    AXI_AWBURST : out STD_LOGIC_VECTOR ( 1 downto 0 );
    AXI_AWID : out STD_LOGIC_VECTOR ( 5 downto 0 );
    AXI_AWLEN : out STD_LOGIC_VECTOR ( 3 downto 0 );
    AXI_AWREADY : in STD_LOGIC;
    AXI_AWSIZE : out STD_LOGIC_VECTOR ( 2 downto 0 );
    AXI_AWVALID : out STD_LOGIC;
    AXI_BID : in STD_LOGIC_VECTOR ( 5 downto 0 );
    AXI_BREADY : out STD_LOGIC;
    AXI_BRESP : in STD_LOGIC_VECTOR ( 1 downto 0 );
    AXI_BVALID : in STD_LOGIC;
    AXI_RDATA : in STD_LOGIC_VECTOR ( 255 downto 0 );
    AXI_RID : in STD_LOGIC_VECTOR ( 5 downto 0 );
    AXI_RLAST : in STD_LOGIC;
    AXI_RREADY : out STD_LOGIC;
    AXI_RRESP : in STD_LOGIC_VECTOR ( 1 downto 0 );
    AXI_RVALID : in STD_LOGIC;
    AXI_WID : out STD_LOGIC_VECTOR ( 5 downto 0 );
    AXI_WDATA : out STD_LOGIC_VECTOR ( 255 downto 0 );
    AXI_WLAST : out STD_LOGIC;
    AXI_WREADY : in STD_LOGIC;
    AXI_WSTRB : out STD_LOGIC_VECTOR ( 31 downto 0 );
    AXI_WVALID : out STD_LOGIC;
    AXI_WDATA_PARITY : out STD_LOGIC_VECTOR ( 31 downto 0 );
    FIFO_WRITE_tready : in STD_LOGIC;
    FIFO_WRITE_tdata : out STD_LOGIC_VECTOR ( 255 downto 0 );
    FIFO_WRITE_tvalid : out STD_LOGIC;
    FIFO_WRITE_tkeep : out STD_LOGIC_VECTOR ( 31 downto 0 );
    FIFO_WRITE_tlast : out STD_LOGIC;
    FIFO_READ_tvalid : in STD_LOGIC;
    FIFO_READ_tdata : in STD_LOGIC_VECTOR ( 255 downto 0 );
    FIFO_READ_tready : out STD_LOGIC;
    rd_running : in STD_LOGIC;
    rd_startaddress : in STD_LOGIC_VECTOR ( 31 downto 0 );
    rd_blockamount : in STD_LOGIC_VECTOR ( 31 downto 0 );
    wr_running : in STD_LOGIC;
    wr_startaddress : in STD_LOGIC_VECTOR ( 31 downto 0 );
    wr_blockamount : in STD_LOGIC_VECTOR ( 31 downto 0 );
    busy : out STD_LOGIC;
    clk : in STD_LOGIC
  );
  end component decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_channel_ctrl_v5_0_0;
  signal \<const0>\ : STD_LOGIC;
  signal axi_register_slice_0_n_326 : STD_LOGIC;
  signal axi_register_slice_0_n_327 : STD_LOGIC;
  signal axi_register_slice_0_n_328 : STD_LOGIC;
  signal axi_register_slice_0_n_329 : STD_LOGIC;
  signal axi_register_slice_0_n_330 : STD_LOGIC;
  signal axi_register_slice_0_n_331 : STD_LOGIC;
  signal axi_register_slice_0_n_332 : STD_LOGIC;
  signal axi_register_slice_0_n_333 : STD_LOGIC;
  signal axi_register_slice_0_n_334 : STD_LOGIC;
  signal axi_register_slice_0_n_335 : STD_LOGIC;
  signal axi_register_slice_0_n_336 : STD_LOGIC;
  signal axi_register_slice_0_n_337 : STD_LOGIC;
  signal axi_register_slice_0_n_338 : STD_LOGIC;
  signal axi_register_slice_0_n_685 : STD_LOGIC;
  signal axi_register_slice_0_n_686 : STD_LOGIC;
  signal axi_register_slice_0_n_687 : STD_LOGIC;
  signal axi_register_slice_0_n_688 : STD_LOGIC;
  signal axi_register_slice_0_n_689 : STD_LOGIC;
  signal axi_register_slice_0_n_690 : STD_LOGIC;
  signal axi_register_slice_0_n_691 : STD_LOGIC;
  signal axi_register_slice_0_n_692 : STD_LOGIC;
  signal axi_register_slice_0_n_693 : STD_LOGIC;
  signal axi_register_slice_0_n_694 : STD_LOGIC;
  signal axi_register_slice_0_n_695 : STD_LOGIC;
  signal axi_register_slice_0_n_696 : STD_LOGIC;
  signal axi_register_slice_0_n_697 : STD_LOGIC;
  signal axis_data_fifo_0_M_AXIS_TDATA : STD_LOGIC_VECTOR ( 255 downto 0 );
  signal axis_data_fifo_0_M_AXIS_TKEEP : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal axis_data_fifo_0_M_AXIS_TLAST : STD_LOGIC;
  signal axis_data_fifo_0_M_AXIS_TREADY : STD_LOGIC;
  signal axis_data_fifo_0_M_AXIS_TVALID : STD_LOGIC;
  signal axis_data_fifo_1_M_AXIS_TDATA : STD_LOGIC_VECTOR ( 255 downto 0 );
  signal axis_data_fifo_1_M_AXIS_TKEEP : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal axis_data_fifo_1_M_AXIS_TLAST : STD_LOGIC;
  signal axis_data_fifo_1_M_AXIS_TREADY : STD_LOGIC;
  signal axis_data_fifo_1_M_AXIS_TVALID : STD_LOGIC;
  signal axis_register_slice_0_M_AXIS_TDATA : STD_LOGIC_VECTOR ( 255 downto 0 );
  signal axis_register_slice_0_M_AXIS_TKEEP : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal axis_register_slice_0_M_AXIS_TLAST : STD_LOGIC;
  signal axis_register_slice_0_M_AXIS_TREADY : STD_LOGIC;
  signal axis_register_slice_0_M_AXIS_TVALID : STD_LOGIC;
  signal axis_register_slice_1_M_AXIS_TDATA : STD_LOGIC_VECTOR ( 255 downto 0 );
  signal axis_register_slice_1_M_AXIS_TREADY : STD_LOGIC;
  signal axis_register_slice_1_M_AXIS_TVALID : STD_LOGIC;
  signal axis_register_slice_3_M_AXIS_TDATA : STD_LOGIC_VECTOR ( 255 downto 0 );
  signal axis_register_slice_3_M_AXIS_TKEEP : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal axis_register_slice_3_M_AXIS_TLAST : STD_LOGIC;
  signal axis_register_slice_3_M_AXIS_TREADY : STD_LOGIC;
  signal axis_register_slice_3_M_AXIS_TVALID : STD_LOGIC;
  signal channel_ctrl_v5_0_AXI_ARADDR : STD_LOGIC_VECTOR ( 32 downto 0 );
  signal channel_ctrl_v5_0_AXI_ARBURST : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal channel_ctrl_v5_0_AXI_ARID : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal channel_ctrl_v5_0_AXI_ARLEN : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal channel_ctrl_v5_0_AXI_ARREADY : STD_LOGIC;
  signal channel_ctrl_v5_0_AXI_ARSIZE : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal channel_ctrl_v5_0_AXI_ARVALID : STD_LOGIC;
  signal channel_ctrl_v5_0_AXI_AWADDR : STD_LOGIC_VECTOR ( 32 downto 0 );
  signal channel_ctrl_v5_0_AXI_AWBURST : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal channel_ctrl_v5_0_AXI_AWID : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal channel_ctrl_v5_0_AXI_AWLEN : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal channel_ctrl_v5_0_AXI_AWREADY : STD_LOGIC;
  signal channel_ctrl_v5_0_AXI_AWSIZE : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal channel_ctrl_v5_0_AXI_AWVALID : STD_LOGIC;
  signal channel_ctrl_v5_0_AXI_BID : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal channel_ctrl_v5_0_AXI_BREADY : STD_LOGIC;
  signal channel_ctrl_v5_0_AXI_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal channel_ctrl_v5_0_AXI_BVALID : STD_LOGIC;
  signal channel_ctrl_v5_0_AXI_RDATA : STD_LOGIC_VECTOR ( 255 downto 0 );
  signal channel_ctrl_v5_0_AXI_RID : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal channel_ctrl_v5_0_AXI_RLAST : STD_LOGIC;
  signal channel_ctrl_v5_0_AXI_RREADY : STD_LOGIC;
  signal channel_ctrl_v5_0_AXI_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal channel_ctrl_v5_0_AXI_RVALID : STD_LOGIC;
  signal channel_ctrl_v5_0_AXI_WDATA : STD_LOGIC_VECTOR ( 255 downto 0 );
  signal channel_ctrl_v5_0_AXI_WID : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal channel_ctrl_v5_0_AXI_WLAST : STD_LOGIC;
  signal channel_ctrl_v5_0_AXI_WREADY : STD_LOGIC;
  signal channel_ctrl_v5_0_AXI_WSTRB : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal channel_ctrl_v5_0_AXI_WVALID : STD_LOGIC;
  signal channel_ctrl_v5_0_FIFO_WRITE_TDATA : STD_LOGIC_VECTOR ( 255 downto 0 );
  signal channel_ctrl_v5_0_FIFO_WRITE_TKEEP : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal channel_ctrl_v5_0_FIFO_WRITE_TLAST : STD_LOGIC;
  signal channel_ctrl_v5_0_FIFO_WRITE_TREADY : STD_LOGIC;
  signal channel_ctrl_v5_0_FIFO_WRITE_TVALID : STD_LOGIC;
  signal NLW_axis_register_slice_1_m_axis_tlast_UNCONNECTED : STD_LOGIC;
  signal NLW_axis_register_slice_1_m_axis_tkeep_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_channel_ctrl_v5_0_AXI_WDATA_PARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of axi_register_slice_0 : label is "axi_register_slice_v2_1_27_axi_register_slice,Vivado 2022.2";
  attribute X_CORE_INFO of axis_data_fifo_0 : label is "axis_data_fifo_v2_0_9_top,Vivado 2022.2";
  attribute X_CORE_INFO of axis_data_fifo_1 : label is "axis_data_fifo_v2_0_9_top,Vivado 2022.2";
  attribute X_CORE_INFO of axis_register_slice_0 : label is "axis_register_slice_v1_1_27_axis_register_slice,Vivado 2022.2";
  attribute X_CORE_INFO of axis_register_slice_1 : label is "axis_register_slice_v1_1_27_axis_register_slice,Vivado 2022.2";
  attribute X_CORE_INFO of axis_register_slice_2 : label is "axis_register_slice_v1_1_27_axis_register_slice,Vivado 2022.2";
  attribute X_CORE_INFO of axis_register_slice_3 : label is "axis_register_slice_v1_1_27_axis_register_slice,Vivado 2022.2";
  attribute X_CORE_INFO of channel_ctrl_v5_0 : label is "channel_ctrl_v5,Vivado 2022.2";
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of HBM_AXI_arready : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI ARREADY";
  attribute X_INTERFACE_INFO of HBM_AXI_arvalid : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI ARVALID";
  attribute X_INTERFACE_INFO of HBM_AXI_awready : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI AWREADY";
  attribute X_INTERFACE_INFO of HBM_AXI_awvalid : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI AWVALID";
  attribute X_INTERFACE_INFO of HBM_AXI_bready : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI BREADY";
  attribute X_INTERFACE_INFO of HBM_AXI_bvalid : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI BVALID";
  attribute X_INTERFACE_INFO of HBM_AXI_rlast : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI RLAST";
  attribute X_INTERFACE_INFO of HBM_AXI_rready : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI RREADY";
  attribute X_INTERFACE_INFO of HBM_AXI_rvalid : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI RVALID";
  attribute X_INTERFACE_INFO of HBM_AXI_wlast : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI WLAST";
  attribute X_INTERFACE_INFO of HBM_AXI_wready : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI WREADY";
  attribute X_INTERFACE_INFO of HBM_AXI_wvalid : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI WVALID";
  attribute X_INTERFACE_INFO of HBM_READ_tlast : signal is "xilinx.com:interface:axis:1.0 HBM_READ TLAST";
  attribute X_INTERFACE_INFO of HBM_READ_tready : signal is "xilinx.com:interface:axis:1.0 HBM_READ TREADY";
  attribute X_INTERFACE_INFO of HBM_READ_tvalid : signal is "xilinx.com:interface:axis:1.0 HBM_READ TVALID";
  attribute X_INTERFACE_INFO of HBM_WRITE_tlast : signal is "xilinx.com:interface:axis:1.0 HBM_WRITE TLAST";
  attribute X_INTERFACE_INFO of HBM_WRITE_tready : signal is "xilinx.com:interface:axis:1.0 HBM_WRITE TREADY";
  attribute X_INTERFACE_INFO of HBM_WRITE_tvalid : signal is "xilinx.com:interface:axis:1.0 HBM_WRITE TVALID";
  attribute X_INTERFACE_INFO of clk_312 : signal is "xilinx.com:signal:clock:1.0 CLK.CLK_312 CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of clk_312 : signal is "XIL_INTERFACENAME CLK.CLK_312, ASSOCIATED_BUSIF HBM_READ:HBM_WRITE, CLK_DOMAIN design_hbm_channel_clk_312, FREQ_HZ 312500000, FREQ_TOLERANCE_HZ 0, INSERT_VIP 0, PHASE 0.0";
  attribute X_INTERFACE_INFO of clk_450 : signal is "xilinx.com:signal:clock:1.0 CLK.CLK_450 CLK";
  attribute X_INTERFACE_PARAMETER of clk_450 : signal is "XIL_INTERFACENAME CLK.CLK_450, ASSOCIATED_BUSIF HBM_AXI, CLK_DOMAIN design_hbm_channel_clk_450, FREQ_HZ 450000000, FREQ_TOLERANCE_HZ 0, INSERT_VIP 0, PHASE 0.0";
  attribute X_INTERFACE_INFO of user_hbm_channel_ctrl_ports_busy : signal is "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 user_hbm_channel_ctrl_ports busy";
  attribute X_INTERFACE_INFO of HBM_AXI_araddr : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI ARADDR";
  attribute X_INTERFACE_PARAMETER of HBM_AXI_araddr : signal is "XIL_INTERFACENAME HBM_AXI, ADDR_WIDTH 33, ARUSER_WIDTH 0, AWUSER_WIDTH 0, BUSER_WIDTH 0, CLK_DOMAIN design_hbm_channel_clk_450, DATA_WIDTH 256, FREQ_HZ 450000000, HAS_BRESP 1, HAS_BURST 1, HAS_CACHE 0, HAS_LOCK 0, HAS_PROT 0, HAS_QOS 0, HAS_REGION 0, HAS_RRESP 1, HAS_WSTRB 1, ID_WIDTH 6, INSERT_VIP 0, MAX_BURST_LENGTH 16, NUM_READ_OUTSTANDING 2, NUM_READ_THREADS 1, NUM_WRITE_OUTSTANDING 2, NUM_WRITE_THREADS 1, PHASE 0.0, PROTOCOL AXI3, READ_WRITE_MODE READ_WRITE, RUSER_BITS_PER_BYTE 0, RUSER_WIDTH 0, SUPPORTS_NARROW_BURST 1, WUSER_BITS_PER_BYTE 0, WUSER_WIDTH 0";
  attribute X_INTERFACE_INFO of HBM_AXI_arburst : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI ARBURST";
  attribute X_INTERFACE_INFO of HBM_AXI_arcache : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI ARCACHE";
  attribute X_INTERFACE_INFO of HBM_AXI_arid : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI ARID";
  attribute X_INTERFACE_INFO of HBM_AXI_arlen : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI ARLEN";
  attribute X_INTERFACE_INFO of HBM_AXI_arlock : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI ARLOCK";
  attribute X_INTERFACE_INFO of HBM_AXI_arprot : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI ARPROT";
  attribute X_INTERFACE_INFO of HBM_AXI_arqos : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI ARQOS";
  attribute X_INTERFACE_INFO of HBM_AXI_arsize : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI ARSIZE";
  attribute X_INTERFACE_INFO of HBM_AXI_awaddr : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI AWADDR";
  attribute X_INTERFACE_INFO of HBM_AXI_awburst : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI AWBURST";
  attribute X_INTERFACE_INFO of HBM_AXI_awcache : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI AWCACHE";
  attribute X_INTERFACE_INFO of HBM_AXI_awid : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI AWID";
  attribute X_INTERFACE_INFO of HBM_AXI_awlen : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI AWLEN";
  attribute X_INTERFACE_INFO of HBM_AXI_awlock : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI AWLOCK";
  attribute X_INTERFACE_INFO of HBM_AXI_awprot : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI AWPROT";
  attribute X_INTERFACE_INFO of HBM_AXI_awqos : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI AWQOS";
  attribute X_INTERFACE_INFO of HBM_AXI_awsize : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI AWSIZE";
  attribute X_INTERFACE_INFO of HBM_AXI_bid : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI BID";
  attribute X_INTERFACE_INFO of HBM_AXI_bresp : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI BRESP";
  attribute X_INTERFACE_INFO of HBM_AXI_rdata : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI RDATA";
  attribute X_INTERFACE_INFO of HBM_AXI_rid : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI RID";
  attribute X_INTERFACE_INFO of HBM_AXI_rresp : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI RRESP";
  attribute X_INTERFACE_INFO of HBM_AXI_wdata : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI WDATA";
  attribute X_INTERFACE_INFO of HBM_AXI_wid : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI WID";
  attribute X_INTERFACE_INFO of HBM_AXI_wstrb : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI WSTRB";
  attribute X_INTERFACE_INFO of HBM_READ_tdata : signal is "xilinx.com:interface:axis:1.0 HBM_READ TDATA";
  attribute X_INTERFACE_PARAMETER of HBM_READ_tdata : signal is "XIL_INTERFACENAME HBM_READ, CLK_DOMAIN design_hbm_channel_clk_312, FREQ_HZ 312500000, HAS_TKEEP 1, HAS_TLAST 1, HAS_TREADY 1, HAS_TSTRB 0, INSERT_VIP 0, LAYERED_METADATA undef, PHASE 0.0, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0";
  attribute X_INTERFACE_INFO of HBM_READ_tkeep : signal is "xilinx.com:interface:axis:1.0 HBM_READ TKEEP";
  attribute X_INTERFACE_INFO of HBM_WRITE_tdata : signal is "xilinx.com:interface:axis:1.0 HBM_WRITE TDATA";
  attribute X_INTERFACE_PARAMETER of HBM_WRITE_tdata : signal is "XIL_INTERFACENAME HBM_WRITE, CLK_DOMAIN design_hbm_channel_clk_312, FREQ_HZ 312500000, HAS_TKEEP 1, HAS_TLAST 1, HAS_TREADY 1, HAS_TSTRB 0, INSERT_VIP 0, LAYERED_METADATA undef, PHASE 0.0, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0";
  attribute X_INTERFACE_INFO of HBM_WRITE_tkeep : signal is "xilinx.com:interface:axis:1.0 HBM_WRITE TKEEP";
  attribute X_INTERFACE_INFO of user_hbm_channel_ctrl_ports_rd_blockamount : signal is "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 user_hbm_channel_ctrl_ports rd_blockamount";
  attribute X_INTERFACE_INFO of user_hbm_channel_ctrl_ports_rd_startaddress : signal is "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 user_hbm_channel_ctrl_ports rd_startaddress";
  attribute X_INTERFACE_INFO of user_hbm_channel_ctrl_ports_wr_blockamount : signal is "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 user_hbm_channel_ctrl_ports wr_blockamount";
  attribute X_INTERFACE_INFO of user_hbm_channel_ctrl_ports_wr_startaddress : signal is "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 user_hbm_channel_ctrl_ports wr_startaddress";
begin
  HBM_AXI_arcache(3) <= \<const0>\;
  HBM_AXI_arcache(2) <= \<const0>\;
  HBM_AXI_arcache(1) <= \<const0>\;
  HBM_AXI_arcache(0) <= \<const0>\;
  HBM_AXI_arlock(1) <= \<const0>\;
  HBM_AXI_arlock(0) <= \<const0>\;
  HBM_AXI_arprot(2) <= \<const0>\;
  HBM_AXI_arprot(1) <= \<const0>\;
  HBM_AXI_arprot(0) <= \<const0>\;
  HBM_AXI_arqos(3) <= \<const0>\;
  HBM_AXI_arqos(2) <= \<const0>\;
  HBM_AXI_arqos(1) <= \<const0>\;
  HBM_AXI_arqos(0) <= \<const0>\;
  HBM_AXI_awcache(3) <= \<const0>\;
  HBM_AXI_awcache(2) <= \<const0>\;
  HBM_AXI_awcache(1) <= \<const0>\;
  HBM_AXI_awcache(0) <= \<const0>\;
  HBM_AXI_awlock(1) <= \<const0>\;
  HBM_AXI_awlock(0) <= \<const0>\;
  HBM_AXI_awprot(2) <= \<const0>\;
  HBM_AXI_awprot(1) <= \<const0>\;
  HBM_AXI_awprot(0) <= \<const0>\;
  HBM_AXI_awqos(3) <= \<const0>\;
  HBM_AXI_awqos(2) <= \<const0>\;
  HBM_AXI_awqos(1) <= \<const0>\;
  HBM_AXI_awqos(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
axi_register_slice_0: component decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_axi_register_slice_0_0
     port map (
      aclk => clk_450,
      aresetn => '1',
      m_axi_araddr(32 downto 0) => HBM_AXI_araddr(32 downto 0),
      m_axi_arburst(1 downto 0) => HBM_AXI_arburst(1 downto 0),
      m_axi_arcache(3) => axi_register_slice_0_n_687,
      m_axi_arcache(2) => axi_register_slice_0_n_688,
      m_axi_arcache(1) => axi_register_slice_0_n_689,
      m_axi_arcache(0) => axi_register_slice_0_n_690,
      m_axi_arid(5 downto 0) => HBM_AXI_arid(5 downto 0),
      m_axi_arlen(3 downto 0) => HBM_AXI_arlen(3 downto 0),
      m_axi_arlock(1) => axi_register_slice_0_n_685,
      m_axi_arlock(0) => axi_register_slice_0_n_686,
      m_axi_arprot(2) => axi_register_slice_0_n_691,
      m_axi_arprot(1) => axi_register_slice_0_n_692,
      m_axi_arprot(0) => axi_register_slice_0_n_693,
      m_axi_arqos(3) => axi_register_slice_0_n_694,
      m_axi_arqos(2) => axi_register_slice_0_n_695,
      m_axi_arqos(1) => axi_register_slice_0_n_696,
      m_axi_arqos(0) => axi_register_slice_0_n_697,
      m_axi_arready => HBM_AXI_arready,
      m_axi_arsize(2 downto 0) => HBM_AXI_arsize(2 downto 0),
      m_axi_arvalid => HBM_AXI_arvalid,
      m_axi_awaddr(32 downto 0) => HBM_AXI_awaddr(32 downto 0),
      m_axi_awburst(1 downto 0) => HBM_AXI_awburst(1 downto 0),
      m_axi_awcache(3) => axi_register_slice_0_n_328,
      m_axi_awcache(2) => axi_register_slice_0_n_329,
      m_axi_awcache(1) => axi_register_slice_0_n_330,
      m_axi_awcache(0) => axi_register_slice_0_n_331,
      m_axi_awid(5 downto 0) => HBM_AXI_awid(5 downto 0),
      m_axi_awlen(3 downto 0) => HBM_AXI_awlen(3 downto 0),
      m_axi_awlock(1) => axi_register_slice_0_n_326,
      m_axi_awlock(0) => axi_register_slice_0_n_327,
      m_axi_awprot(2) => axi_register_slice_0_n_332,
      m_axi_awprot(1) => axi_register_slice_0_n_333,
      m_axi_awprot(0) => axi_register_slice_0_n_334,
      m_axi_awqos(3) => axi_register_slice_0_n_335,
      m_axi_awqos(2) => axi_register_slice_0_n_336,
      m_axi_awqos(1) => axi_register_slice_0_n_337,
      m_axi_awqos(0) => axi_register_slice_0_n_338,
      m_axi_awready => HBM_AXI_awready,
      m_axi_awsize(2 downto 0) => HBM_AXI_awsize(2 downto 0),
      m_axi_awvalid => HBM_AXI_awvalid,
      m_axi_bid(5 downto 0) => HBM_AXI_bid(5 downto 0),
      m_axi_bready => HBM_AXI_bready,
      m_axi_bresp(1 downto 0) => HBM_AXI_bresp(1 downto 0),
      m_axi_bvalid => HBM_AXI_bvalid,
      m_axi_rdata(255 downto 0) => HBM_AXI_rdata(255 downto 0),
      m_axi_rid(5 downto 0) => HBM_AXI_rid(5 downto 0),
      m_axi_rlast => HBM_AXI_rlast,
      m_axi_rready => HBM_AXI_rready,
      m_axi_rresp(1 downto 0) => HBM_AXI_rresp(1 downto 0),
      m_axi_rvalid => HBM_AXI_rvalid,
      m_axi_wdata(255 downto 0) => HBM_AXI_wdata(255 downto 0),
      m_axi_wid(5 downto 0) => HBM_AXI_wid(5 downto 0),
      m_axi_wlast => HBM_AXI_wlast,
      m_axi_wready => HBM_AXI_wready,
      m_axi_wstrb(31 downto 0) => HBM_AXI_wstrb(31 downto 0),
      m_axi_wvalid => HBM_AXI_wvalid,
      s_axi_araddr(32 downto 0) => channel_ctrl_v5_0_AXI_ARADDR(32 downto 0),
      s_axi_arburst(1 downto 0) => channel_ctrl_v5_0_AXI_ARBURST(1 downto 0),
      s_axi_arcache(3 downto 0) => B"0011",
      s_axi_arid(5 downto 0) => channel_ctrl_v5_0_AXI_ARID(5 downto 0),
      s_axi_arlen(3 downto 0) => channel_ctrl_v5_0_AXI_ARLEN(3 downto 0),
      s_axi_arlock(1 downto 0) => B"00",
      s_axi_arprot(2 downto 0) => B"000",
      s_axi_arqos(3 downto 0) => B"0000",
      s_axi_arready => channel_ctrl_v5_0_AXI_ARREADY,
      s_axi_arsize(2 downto 0) => channel_ctrl_v5_0_AXI_ARSIZE(2 downto 0),
      s_axi_arvalid => channel_ctrl_v5_0_AXI_ARVALID,
      s_axi_awaddr(32 downto 0) => channel_ctrl_v5_0_AXI_AWADDR(32 downto 0),
      s_axi_awburst(1 downto 0) => channel_ctrl_v5_0_AXI_AWBURST(1 downto 0),
      s_axi_awcache(3 downto 0) => B"0011",
      s_axi_awid(5 downto 0) => channel_ctrl_v5_0_AXI_AWID(5 downto 0),
      s_axi_awlen(3 downto 0) => channel_ctrl_v5_0_AXI_AWLEN(3 downto 0),
      s_axi_awlock(1 downto 0) => B"00",
      s_axi_awprot(2 downto 0) => B"000",
      s_axi_awqos(3 downto 0) => B"0000",
      s_axi_awready => channel_ctrl_v5_0_AXI_AWREADY,
      s_axi_awsize(2 downto 0) => channel_ctrl_v5_0_AXI_AWSIZE(2 downto 0),
      s_axi_awvalid => channel_ctrl_v5_0_AXI_AWVALID,
      s_axi_bid(5 downto 0) => channel_ctrl_v5_0_AXI_BID(5 downto 0),
      s_axi_bready => channel_ctrl_v5_0_AXI_BREADY,
      s_axi_bresp(1 downto 0) => channel_ctrl_v5_0_AXI_BRESP(1 downto 0),
      s_axi_bvalid => channel_ctrl_v5_0_AXI_BVALID,
      s_axi_rdata(255 downto 0) => channel_ctrl_v5_0_AXI_RDATA(255 downto 0),
      s_axi_rid(5 downto 0) => channel_ctrl_v5_0_AXI_RID(5 downto 0),
      s_axi_rlast => channel_ctrl_v5_0_AXI_RLAST,
      s_axi_rready => channel_ctrl_v5_0_AXI_RREADY,
      s_axi_rresp(1 downto 0) => channel_ctrl_v5_0_AXI_RRESP(1 downto 0),
      s_axi_rvalid => channel_ctrl_v5_0_AXI_RVALID,
      s_axi_wdata(255 downto 0) => channel_ctrl_v5_0_AXI_WDATA(255 downto 0),
      s_axi_wid(5 downto 0) => channel_ctrl_v5_0_AXI_WID(5 downto 0),
      s_axi_wlast => channel_ctrl_v5_0_AXI_WLAST,
      s_axi_wready => channel_ctrl_v5_0_AXI_WREADY,
      s_axi_wstrb(31 downto 0) => channel_ctrl_v5_0_AXI_WSTRB(31 downto 0),
      s_axi_wvalid => channel_ctrl_v5_0_AXI_WVALID
    );
axis_data_fifo_0: component decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_axis_data_fifo_0_0
     port map (
      m_axis_aclk => clk_450,
      m_axis_tdata(255 downto 0) => axis_data_fifo_0_M_AXIS_TDATA(255 downto 0),
      m_axis_tkeep(31 downto 0) => axis_data_fifo_0_M_AXIS_TKEEP(31 downto 0),
      m_axis_tlast => axis_data_fifo_0_M_AXIS_TLAST,
      m_axis_tready => axis_data_fifo_0_M_AXIS_TREADY,
      m_axis_tvalid => axis_data_fifo_0_M_AXIS_TVALID,
      s_axis_aclk => clk_312,
      s_axis_aresetn => '1',
      s_axis_tdata(255 downto 0) => axis_register_slice_3_M_AXIS_TDATA(255 downto 0),
      s_axis_tkeep(31 downto 0) => axis_register_slice_3_M_AXIS_TKEEP(31 downto 0),
      s_axis_tlast => axis_register_slice_3_M_AXIS_TLAST,
      s_axis_tready => axis_register_slice_3_M_AXIS_TREADY,
      s_axis_tvalid => axis_register_slice_3_M_AXIS_TVALID
    );
axis_data_fifo_1: component decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_axis_data_fifo_0_1
     port map (
      m_axis_aclk => clk_312,
      m_axis_tdata(255 downto 0) => axis_data_fifo_1_M_AXIS_TDATA(255 downto 0),
      m_axis_tkeep(31 downto 0) => axis_data_fifo_1_M_AXIS_TKEEP(31 downto 0),
      m_axis_tlast => axis_data_fifo_1_M_AXIS_TLAST,
      m_axis_tready => axis_data_fifo_1_M_AXIS_TREADY,
      m_axis_tvalid => axis_data_fifo_1_M_AXIS_TVALID,
      s_axis_aclk => clk_450,
      s_axis_aresetn => '1',
      s_axis_tdata(255 downto 0) => axis_register_slice_0_M_AXIS_TDATA(255 downto 0),
      s_axis_tkeep(31 downto 0) => axis_register_slice_0_M_AXIS_TKEEP(31 downto 0),
      s_axis_tlast => axis_register_slice_0_M_AXIS_TLAST,
      s_axis_tready => axis_register_slice_0_M_AXIS_TREADY,
      s_axis_tvalid => axis_register_slice_0_M_AXIS_TVALID
    );
axis_register_slice_0: component decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_axis_register_slice_0_0
     port map (
      aclk => clk_450,
      aresetn => '1',
      m_axis_tdata(255 downto 0) => axis_register_slice_0_M_AXIS_TDATA(255 downto 0),
      m_axis_tkeep(31 downto 0) => axis_register_slice_0_M_AXIS_TKEEP(31 downto 0),
      m_axis_tlast => axis_register_slice_0_M_AXIS_TLAST,
      m_axis_tready => axis_register_slice_0_M_AXIS_TREADY,
      m_axis_tvalid => axis_register_slice_0_M_AXIS_TVALID,
      s_axis_tdata(255 downto 0) => channel_ctrl_v5_0_FIFO_WRITE_TDATA(255 downto 0),
      s_axis_tkeep(31 downto 0) => channel_ctrl_v5_0_FIFO_WRITE_TKEEP(31 downto 0),
      s_axis_tlast => channel_ctrl_v5_0_FIFO_WRITE_TLAST,
      s_axis_tready => channel_ctrl_v5_0_FIFO_WRITE_TREADY,
      s_axis_tvalid => channel_ctrl_v5_0_FIFO_WRITE_TVALID
    );
axis_register_slice_1: component decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_axis_register_slice_0_1
     port map (
      aclk => clk_450,
      aresetn => '1',
      m_axis_tdata(255 downto 0) => axis_register_slice_1_M_AXIS_TDATA(255 downto 0),
      m_axis_tkeep(31 downto 0) => NLW_axis_register_slice_1_m_axis_tkeep_UNCONNECTED(31 downto 0),
      m_axis_tlast => NLW_axis_register_slice_1_m_axis_tlast_UNCONNECTED,
      m_axis_tready => axis_register_slice_1_M_AXIS_TREADY,
      m_axis_tvalid => axis_register_slice_1_M_AXIS_TVALID,
      s_axis_tdata(255 downto 0) => axis_data_fifo_0_M_AXIS_TDATA(255 downto 0),
      s_axis_tkeep(31 downto 0) => axis_data_fifo_0_M_AXIS_TKEEP(31 downto 0),
      s_axis_tlast => axis_data_fifo_0_M_AXIS_TLAST,
      s_axis_tready => axis_data_fifo_0_M_AXIS_TREADY,
      s_axis_tvalid => axis_data_fifo_0_M_AXIS_TVALID
    );
axis_register_slice_2: component decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_axis_register_slice_2_0
     port map (
      aclk => clk_312,
      aresetn => '1',
      m_axis_tdata(255 downto 0) => HBM_READ_tdata(255 downto 0),
      m_axis_tkeep(31 downto 0) => HBM_READ_tkeep(31 downto 0),
      m_axis_tlast => HBM_READ_tlast,
      m_axis_tready => HBM_READ_tready,
      m_axis_tvalid => HBM_READ_tvalid,
      s_axis_tdata(255 downto 0) => axis_data_fifo_1_M_AXIS_TDATA(255 downto 0),
      s_axis_tkeep(31 downto 0) => axis_data_fifo_1_M_AXIS_TKEEP(31 downto 0),
      s_axis_tlast => axis_data_fifo_1_M_AXIS_TLAST,
      s_axis_tready => axis_data_fifo_1_M_AXIS_TREADY,
      s_axis_tvalid => axis_data_fifo_1_M_AXIS_TVALID
    );
axis_register_slice_3: component decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_axis_register_slice_3_0
     port map (
      aclk => clk_312,
      aresetn => '1',
      m_axis_tdata(255 downto 0) => axis_register_slice_3_M_AXIS_TDATA(255 downto 0),
      m_axis_tkeep(31 downto 0) => axis_register_slice_3_M_AXIS_TKEEP(31 downto 0),
      m_axis_tlast => axis_register_slice_3_M_AXIS_TLAST,
      m_axis_tready => axis_register_slice_3_M_AXIS_TREADY,
      m_axis_tvalid => axis_register_slice_3_M_AXIS_TVALID,
      s_axis_tdata(255 downto 0) => HBM_WRITE_tdata(255 downto 0),
      s_axis_tkeep(31 downto 0) => B"00000000000000000000000000000000",
      s_axis_tlast => '0',
      s_axis_tready => HBM_WRITE_tready,
      s_axis_tvalid => HBM_WRITE_tvalid
    );
channel_ctrl_v5_0: component decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_channel_ctrl_v5_0_0
     port map (
      AXI_ARADDR(32 downto 0) => channel_ctrl_v5_0_AXI_ARADDR(32 downto 0),
      AXI_ARBURST(1 downto 0) => channel_ctrl_v5_0_AXI_ARBURST(1 downto 0),
      AXI_ARID(5 downto 0) => channel_ctrl_v5_0_AXI_ARID(5 downto 0),
      AXI_ARLEN(3 downto 0) => channel_ctrl_v5_0_AXI_ARLEN(3 downto 0),
      AXI_ARREADY => channel_ctrl_v5_0_AXI_ARREADY,
      AXI_ARSIZE(2 downto 0) => channel_ctrl_v5_0_AXI_ARSIZE(2 downto 0),
      AXI_ARVALID => channel_ctrl_v5_0_AXI_ARVALID,
      AXI_AWADDR(32 downto 0) => channel_ctrl_v5_0_AXI_AWADDR(32 downto 0),
      AXI_AWBURST(1 downto 0) => channel_ctrl_v5_0_AXI_AWBURST(1 downto 0),
      AXI_AWID(5 downto 0) => channel_ctrl_v5_0_AXI_AWID(5 downto 0),
      AXI_AWLEN(3 downto 0) => channel_ctrl_v5_0_AXI_AWLEN(3 downto 0),
      AXI_AWREADY => channel_ctrl_v5_0_AXI_AWREADY,
      AXI_AWSIZE(2 downto 0) => channel_ctrl_v5_0_AXI_AWSIZE(2 downto 0),
      AXI_AWVALID => channel_ctrl_v5_0_AXI_AWVALID,
      AXI_BID(5 downto 0) => channel_ctrl_v5_0_AXI_BID(5 downto 0),
      AXI_BREADY => channel_ctrl_v5_0_AXI_BREADY,
      AXI_BRESP(1 downto 0) => channel_ctrl_v5_0_AXI_BRESP(1 downto 0),
      AXI_BVALID => channel_ctrl_v5_0_AXI_BVALID,
      AXI_RDATA(255 downto 0) => channel_ctrl_v5_0_AXI_RDATA(255 downto 0),
      AXI_RID(5 downto 0) => channel_ctrl_v5_0_AXI_RID(5 downto 0),
      AXI_RLAST => channel_ctrl_v5_0_AXI_RLAST,
      AXI_RREADY => channel_ctrl_v5_0_AXI_RREADY,
      AXI_RRESP(1 downto 0) => channel_ctrl_v5_0_AXI_RRESP(1 downto 0),
      AXI_RVALID => channel_ctrl_v5_0_AXI_RVALID,
      AXI_WDATA(255 downto 0) => channel_ctrl_v5_0_AXI_WDATA(255 downto 0),
      AXI_WDATA_PARITY(31 downto 0) => NLW_channel_ctrl_v5_0_AXI_WDATA_PARITY_UNCONNECTED(31 downto 0),
      AXI_WID(5 downto 0) => channel_ctrl_v5_0_AXI_WID(5 downto 0),
      AXI_WLAST => channel_ctrl_v5_0_AXI_WLAST,
      AXI_WREADY => channel_ctrl_v5_0_AXI_WREADY,
      AXI_WSTRB(31 downto 0) => channel_ctrl_v5_0_AXI_WSTRB(31 downto 0),
      AXI_WVALID => channel_ctrl_v5_0_AXI_WVALID,
      FIFO_READ_tdata(255 downto 0) => axis_register_slice_1_M_AXIS_TDATA(255 downto 0),
      FIFO_READ_tready => axis_register_slice_1_M_AXIS_TREADY,
      FIFO_READ_tvalid => axis_register_slice_1_M_AXIS_TVALID,
      FIFO_WRITE_tdata(255 downto 0) => channel_ctrl_v5_0_FIFO_WRITE_TDATA(255 downto 0),
      FIFO_WRITE_tkeep(31 downto 0) => channel_ctrl_v5_0_FIFO_WRITE_TKEEP(31 downto 0),
      FIFO_WRITE_tlast => channel_ctrl_v5_0_FIFO_WRITE_TLAST,
      FIFO_WRITE_tready => channel_ctrl_v5_0_FIFO_WRITE_TREADY,
      FIFO_WRITE_tvalid => channel_ctrl_v5_0_FIFO_WRITE_TVALID,
      busy => user_hbm_channel_ctrl_ports_busy,
      clk => clk_450,
      rd_blockamount(31 downto 0) => user_hbm_channel_ctrl_ports_rd_blockamount(31 downto 0),
      rd_running => rd_running,
      rd_startaddress(31 downto 0) => user_hbm_channel_ctrl_ports_rd_startaddress(31 downto 0),
      wr_blockamount(31 downto 0) => user_hbm_channel_ctrl_ports_wr_blockamount(31 downto 0),
      wr_running => wr_running,
      wr_startaddress(31 downto 0) => user_hbm_channel_ctrl_ports_wr_startaddress(31 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_wrapper is
  port (
    HBM_AXI_araddr : out STD_LOGIC_VECTOR ( 32 downto 0 );
    HBM_AXI_arburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    HBM_AXI_arid : out STD_LOGIC_VECTOR ( 5 downto 0 );
    HBM_AXI_arlen : out STD_LOGIC_VECTOR ( 3 downto 0 );
    HBM_AXI_arsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    HBM_AXI_arvalid : out STD_LOGIC;
    HBM_AXI_awaddr : out STD_LOGIC_VECTOR ( 32 downto 0 );
    HBM_AXI_awburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    HBM_AXI_awid : out STD_LOGIC_VECTOR ( 5 downto 0 );
    HBM_AXI_awlen : out STD_LOGIC_VECTOR ( 3 downto 0 );
    HBM_AXI_awsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    HBM_AXI_awvalid : out STD_LOGIC;
    HBM_AXI_bready : out STD_LOGIC;
    HBM_AXI_rready : out STD_LOGIC;
    HBM_AXI_wdata : out STD_LOGIC_VECTOR ( 255 downto 0 );
    HBM_AXI_wid : out STD_LOGIC_VECTOR ( 5 downto 0 );
    HBM_AXI_wlast : out STD_LOGIC;
    HBM_AXI_wstrb : out STD_LOGIC_VECTOR ( 31 downto 0 );
    HBM_AXI_wvalid : out STD_LOGIC;
    HBM_READ_tdata : out STD_LOGIC_VECTOR ( 255 downto 0 );
    HBM_READ_tkeep : out STD_LOGIC_VECTOR ( 31 downto 0 );
    HBM_READ_tlast : out STD_LOGIC;
    HBM_READ_tvalid : out STD_LOGIC;
    HBM_WRITE_tready : out STD_LOGIC;
    user_hbm_channel_ctrl_ports_busy : out STD_LOGIC;
    HBM_AXI_arready : in STD_LOGIC;
    HBM_AXI_awready : in STD_LOGIC;
    HBM_AXI_bid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    HBM_AXI_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    HBM_AXI_bvalid : in STD_LOGIC;
    HBM_AXI_rdata : in STD_LOGIC_VECTOR ( 255 downto 0 );
    HBM_AXI_rid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    HBM_AXI_rlast : in STD_LOGIC;
    HBM_AXI_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    HBM_AXI_rvalid : in STD_LOGIC;
    HBM_AXI_wready : in STD_LOGIC;
    HBM_READ_tready : in STD_LOGIC;
    HBM_WRITE_tdata : in STD_LOGIC_VECTOR ( 255 downto 0 );
    HBM_WRITE_tvalid : in STD_LOGIC;
    clk_312 : in STD_LOGIC;
    clk_450 : in STD_LOGIC;
    rd_running : in STD_LOGIC;
    user_hbm_channel_ctrl_ports_rd_blockamount : in STD_LOGIC_VECTOR ( 31 downto 0 );
    user_hbm_channel_ctrl_ports_rd_startaddress : in STD_LOGIC_VECTOR ( 31 downto 0 );
    user_hbm_channel_ctrl_ports_wr_blockamount : in STD_LOGIC_VECTOR ( 31 downto 0 );
    user_hbm_channel_ctrl_ports_wr_startaddress : in STD_LOGIC_VECTOR ( 31 downto 0 );
    wr_running : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_wrapper;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_wrapper is
  signal NLW_design_hbm_channel_i_HBM_AXI_arcache_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_design_hbm_channel_i_HBM_AXI_arlock_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_design_hbm_channel_i_HBM_AXI_arprot_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_design_hbm_channel_i_HBM_AXI_arqos_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_design_hbm_channel_i_HBM_AXI_awcache_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_design_hbm_channel_i_HBM_AXI_awlock_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_design_hbm_channel_i_HBM_AXI_awprot_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_design_hbm_channel_i_HBM_AXI_awqos_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute HW_HANDOFF : string;
  attribute HW_HANDOFF of design_hbm_channel_i : label is "design_hbm_channel.hwdef";
begin
design_hbm_channel_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel
     port map (
      HBM_AXI_araddr(32 downto 0) => HBM_AXI_araddr(32 downto 0),
      HBM_AXI_arburst(1 downto 0) => HBM_AXI_arburst(1 downto 0),
      HBM_AXI_arcache(3 downto 0) => NLW_design_hbm_channel_i_HBM_AXI_arcache_UNCONNECTED(3 downto 0),
      HBM_AXI_arid(5 downto 0) => HBM_AXI_arid(5 downto 0),
      HBM_AXI_arlen(3 downto 0) => HBM_AXI_arlen(3 downto 0),
      HBM_AXI_arlock(1 downto 0) => NLW_design_hbm_channel_i_HBM_AXI_arlock_UNCONNECTED(1 downto 0),
      HBM_AXI_arprot(2 downto 0) => NLW_design_hbm_channel_i_HBM_AXI_arprot_UNCONNECTED(2 downto 0),
      HBM_AXI_arqos(3 downto 0) => NLW_design_hbm_channel_i_HBM_AXI_arqos_UNCONNECTED(3 downto 0),
      HBM_AXI_arready => HBM_AXI_arready,
      HBM_AXI_arsize(2 downto 0) => HBM_AXI_arsize(2 downto 0),
      HBM_AXI_arvalid => HBM_AXI_arvalid,
      HBM_AXI_awaddr(32 downto 0) => HBM_AXI_awaddr(32 downto 0),
      HBM_AXI_awburst(1 downto 0) => HBM_AXI_awburst(1 downto 0),
      HBM_AXI_awcache(3 downto 0) => NLW_design_hbm_channel_i_HBM_AXI_awcache_UNCONNECTED(3 downto 0),
      HBM_AXI_awid(5 downto 0) => HBM_AXI_awid(5 downto 0),
      HBM_AXI_awlen(3 downto 0) => HBM_AXI_awlen(3 downto 0),
      HBM_AXI_awlock(1 downto 0) => NLW_design_hbm_channel_i_HBM_AXI_awlock_UNCONNECTED(1 downto 0),
      HBM_AXI_awprot(2 downto 0) => NLW_design_hbm_channel_i_HBM_AXI_awprot_UNCONNECTED(2 downto 0),
      HBM_AXI_awqos(3 downto 0) => NLW_design_hbm_channel_i_HBM_AXI_awqos_UNCONNECTED(3 downto 0),
      HBM_AXI_awready => HBM_AXI_awready,
      HBM_AXI_awsize(2 downto 0) => HBM_AXI_awsize(2 downto 0),
      HBM_AXI_awvalid => HBM_AXI_awvalid,
      HBM_AXI_bid(5 downto 0) => HBM_AXI_bid(5 downto 0),
      HBM_AXI_bready => HBM_AXI_bready,
      HBM_AXI_bresp(1 downto 0) => HBM_AXI_bresp(1 downto 0),
      HBM_AXI_bvalid => HBM_AXI_bvalid,
      HBM_AXI_rdata(255 downto 0) => HBM_AXI_rdata(255 downto 0),
      HBM_AXI_rid(5 downto 0) => HBM_AXI_rid(5 downto 0),
      HBM_AXI_rlast => HBM_AXI_rlast,
      HBM_AXI_rready => HBM_AXI_rready,
      HBM_AXI_rresp(1 downto 0) => HBM_AXI_rresp(1 downto 0),
      HBM_AXI_rvalid => HBM_AXI_rvalid,
      HBM_AXI_wdata(255 downto 0) => HBM_AXI_wdata(255 downto 0),
      HBM_AXI_wid(5 downto 0) => HBM_AXI_wid(5 downto 0),
      HBM_AXI_wlast => HBM_AXI_wlast,
      HBM_AXI_wready => HBM_AXI_wready,
      HBM_AXI_wstrb(31 downto 0) => HBM_AXI_wstrb(31 downto 0),
      HBM_AXI_wvalid => HBM_AXI_wvalid,
      HBM_READ_tdata(255 downto 0) => HBM_READ_tdata(255 downto 0),
      HBM_READ_tkeep(31 downto 0) => HBM_READ_tkeep(31 downto 0),
      HBM_READ_tlast => HBM_READ_tlast,
      HBM_READ_tready => HBM_READ_tready,
      HBM_READ_tvalid => HBM_READ_tvalid,
      HBM_WRITE_tdata(255 downto 0) => HBM_WRITE_tdata(255 downto 0),
      HBM_WRITE_tkeep(31 downto 0) => B"00000000000000000000000000000000",
      HBM_WRITE_tlast => '0',
      HBM_WRITE_tready => HBM_WRITE_tready,
      HBM_WRITE_tvalid => HBM_WRITE_tvalid,
      clk_312 => clk_312,
      clk_450 => clk_450,
      rd_running => rd_running,
      user_hbm_channel_ctrl_ports_busy => user_hbm_channel_ctrl_ports_busy,
      user_hbm_channel_ctrl_ports_rd_blockamount(31 downto 0) => user_hbm_channel_ctrl_ports_rd_blockamount(31 downto 0),
      user_hbm_channel_ctrl_ports_rd_startaddress(31 downto 0) => user_hbm_channel_ctrl_ports_rd_startaddress(31 downto 0),
      user_hbm_channel_ctrl_ports_wr_blockamount(31 downto 0) => user_hbm_channel_ctrl_ports_wr_blockamount(31 downto 0),
      user_hbm_channel_ctrl_ports_wr_startaddress(31 downto 0) => user_hbm_channel_ctrl_ports_wr_startaddress(31 downto 0),
      wr_running => wr_running
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    HBM_AXI_araddr : out STD_LOGIC_VECTOR ( 32 downto 0 );
    HBM_AXI_arburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    HBM_AXI_arid : out STD_LOGIC_VECTOR ( 5 downto 0 );
    HBM_AXI_arlen : out STD_LOGIC_VECTOR ( 3 downto 0 );
    HBM_AXI_arready : in STD_LOGIC;
    HBM_AXI_arsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    HBM_AXI_arvalid : out STD_LOGIC;
    HBM_AXI_awaddr : out STD_LOGIC_VECTOR ( 32 downto 0 );
    HBM_AXI_awburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    HBM_AXI_awid : out STD_LOGIC_VECTOR ( 5 downto 0 );
    HBM_AXI_awlen : out STD_LOGIC_VECTOR ( 3 downto 0 );
    HBM_AXI_awready : in STD_LOGIC;
    HBM_AXI_awsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    HBM_AXI_awvalid : out STD_LOGIC;
    HBM_AXI_bid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    HBM_AXI_bready : out STD_LOGIC;
    HBM_AXI_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    HBM_AXI_bvalid : in STD_LOGIC;
    HBM_AXI_rdata : in STD_LOGIC_VECTOR ( 255 downto 0 );
    HBM_AXI_rid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    HBM_AXI_rlast : in STD_LOGIC;
    HBM_AXI_rready : out STD_LOGIC;
    HBM_AXI_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    HBM_AXI_rvalid : in STD_LOGIC;
    HBM_AXI_wdata : out STD_LOGIC_VECTOR ( 255 downto 0 );
    HBM_AXI_wid : out STD_LOGIC_VECTOR ( 5 downto 0 );
    HBM_AXI_wlast : out STD_LOGIC;
    HBM_AXI_wready : in STD_LOGIC;
    HBM_AXI_wstrb : out STD_LOGIC_VECTOR ( 31 downto 0 );
    HBM_AXI_wvalid : out STD_LOGIC;
    HBM_READ_tdata : out STD_LOGIC_VECTOR ( 255 downto 0 );
    HBM_READ_tkeep : out STD_LOGIC_VECTOR ( 31 downto 0 );
    HBM_READ_tlast : out STD_LOGIC;
    HBM_READ_tready : in STD_LOGIC;
    HBM_READ_tvalid : out STD_LOGIC;
    HBM_WRITE_tdata : in STD_LOGIC_VECTOR ( 255 downto 0 );
    HBM_WRITE_tready : out STD_LOGIC;
    HBM_WRITE_tvalid : in STD_LOGIC;
    clk_312 : in STD_LOGIC;
    clk_450 : in STD_LOGIC;
    rd_running : in STD_LOGIC;
    user_hbm_channel_ctrl_ports_busy : out STD_LOGIC;
    user_hbm_channel_ctrl_ports_rd_blockamount : in STD_LOGIC_VECTOR ( 31 downto 0 );
    user_hbm_channel_ctrl_ports_rd_startaddress : in STD_LOGIC_VECTOR ( 31 downto 0 );
    user_hbm_channel_ctrl_ports_wr_blockamount : in STD_LOGIC_VECTOR ( 31 downto 0 );
    user_hbm_channel_ctrl_ports_wr_startaddress : in STD_LOGIC_VECTOR ( 31 downto 0 );
    wr_running : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_hbm_design_hbm_channel_w_0_1,design_hbm_channel_wrapper,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute IP_DEFINITION_SOURCE : string;
  attribute IP_DEFINITION_SOURCE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "module_ref";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_hbm_channel_wrapper,Vivado 2022.2";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of HBM_AXI_arready : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI ARREADY";
  attribute X_INTERFACE_INFO of HBM_AXI_arvalid : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI ARVALID";
  attribute X_INTERFACE_INFO of HBM_AXI_awready : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI AWREADY";
  attribute X_INTERFACE_INFO of HBM_AXI_awvalid : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI AWVALID";
  attribute X_INTERFACE_INFO of HBM_AXI_bready : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI BREADY";
  attribute X_INTERFACE_INFO of HBM_AXI_bvalid : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI BVALID";
  attribute X_INTERFACE_INFO of HBM_AXI_rlast : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI RLAST";
  attribute X_INTERFACE_INFO of HBM_AXI_rready : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI RREADY";
  attribute X_INTERFACE_INFO of HBM_AXI_rvalid : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI RVALID";
  attribute X_INTERFACE_INFO of HBM_AXI_wlast : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI WLAST";
  attribute X_INTERFACE_INFO of HBM_AXI_wready : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI WREADY";
  attribute X_INTERFACE_INFO of HBM_AXI_wvalid : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI WVALID";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of HBM_AXI_wvalid : signal is "XIL_INTERFACENAME HBM_AXI, DATA_WIDTH 256, PROTOCOL AXI3, FREQ_HZ 450000000, ID_WIDTH 6, ADDR_WIDTH 33, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 1, HAS_LOCK 0, HAS_PROT 0, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 16, PHASE 0.0, CLK_DOMAIN design_hbm_clk_wiz_1_0_clk_out1, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of HBM_READ_tlast : signal is "xilinx.com:interface:axis:1.0 HBM_READ TLAST";
  attribute X_INTERFACE_INFO of HBM_READ_tready : signal is "xilinx.com:interface:axis:1.0 HBM_READ TREADY";
  attribute X_INTERFACE_INFO of HBM_READ_tvalid : signal is "xilinx.com:interface:axis:1.0 HBM_READ TVALID";
  attribute X_INTERFACE_PARAMETER of HBM_READ_tvalid : signal is "XIL_INTERFACENAME HBM_READ, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN design_hbm_clk_312MHz, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of HBM_WRITE_tready : signal is "xilinx.com:interface:axis:1.0 HBM_WRITE TREADY";
  attribute X_INTERFACE_INFO of HBM_WRITE_tvalid : signal is "xilinx.com:interface:axis:1.0 HBM_WRITE TVALID";
  attribute X_INTERFACE_PARAMETER of HBM_WRITE_tvalid : signal is "XIL_INTERFACENAME HBM_WRITE, TDATA_NUM_BYTES 32, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.0, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of rd_running : signal is "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 ctrl_ports rd_run";
  attribute X_INTERFACE_INFO of user_hbm_channel_ctrl_ports_busy : signal is "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 ctrl_ports busy";
  attribute X_INTERFACE_INFO of wr_running : signal is "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 ctrl_ports wr_run";
  attribute X_INTERFACE_INFO of HBM_AXI_araddr : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI ARADDR";
  attribute X_INTERFACE_INFO of HBM_AXI_arburst : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI ARBURST";
  attribute X_INTERFACE_INFO of HBM_AXI_arid : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI ARID";
  attribute X_INTERFACE_INFO of HBM_AXI_arlen : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI ARLEN";
  attribute X_INTERFACE_INFO of HBM_AXI_arsize : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI ARSIZE";
  attribute X_INTERFACE_INFO of HBM_AXI_awaddr : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI AWADDR";
  attribute X_INTERFACE_INFO of HBM_AXI_awburst : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI AWBURST";
  attribute X_INTERFACE_INFO of HBM_AXI_awid : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI AWID";
  attribute X_INTERFACE_INFO of HBM_AXI_awlen : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI AWLEN";
  attribute X_INTERFACE_INFO of HBM_AXI_awsize : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI AWSIZE";
  attribute X_INTERFACE_INFO of HBM_AXI_bid : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI BID";
  attribute X_INTERFACE_INFO of HBM_AXI_bresp : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI BRESP";
  attribute X_INTERFACE_INFO of HBM_AXI_rdata : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI RDATA";
  attribute X_INTERFACE_INFO of HBM_AXI_rid : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI RID";
  attribute X_INTERFACE_INFO of HBM_AXI_rresp : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI RRESP";
  attribute X_INTERFACE_INFO of HBM_AXI_wdata : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI WDATA";
  attribute X_INTERFACE_INFO of HBM_AXI_wid : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI WID";
  attribute X_INTERFACE_INFO of HBM_AXI_wstrb : signal is "xilinx.com:interface:aximm:1.0 HBM_AXI WSTRB";
  attribute X_INTERFACE_INFO of HBM_READ_tdata : signal is "xilinx.com:interface:axis:1.0 HBM_READ TDATA";
  attribute X_INTERFACE_INFO of HBM_READ_tkeep : signal is "xilinx.com:interface:axis:1.0 HBM_READ TKEEP";
  attribute CLK_DOMAIN : string;
  attribute CLK_DOMAIN of HBM_WRITE_tdata : signal is "design_hbm_clk_312MHz";
  attribute X_INTERFACE_INFO of HBM_WRITE_tdata : signal is "xilinx.com:interface:axis:1.0 HBM_WRITE TDATA";
  attribute X_INTERFACE_INFO of user_hbm_channel_ctrl_ports_rd_blockamount : signal is "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 ctrl_ports rd_blockamount";
  attribute X_INTERFACE_MODE : string;
  attribute X_INTERFACE_MODE of user_hbm_channel_ctrl_ports_rd_blockamount : signal is "slave";
  attribute X_INTERFACE_INFO of user_hbm_channel_ctrl_ports_rd_startaddress : signal is "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 ctrl_ports rd_startaddress";
  attribute X_INTERFACE_INFO of user_hbm_channel_ctrl_ports_wr_blockamount : signal is "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 ctrl_ports wr_blockamount";
  attribute X_INTERFACE_INFO of user_hbm_channel_ctrl_ports_wr_startaddress : signal is "hbm_channel_ctrl_ports:user:hbm_channel_ctrl_ports:1.0 ctrl_ports wr_startaddress";
begin
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_design_hbm_channel_wrapper
     port map (
      HBM_AXI_araddr(32 downto 0) => HBM_AXI_araddr(32 downto 0),
      HBM_AXI_arburst(1 downto 0) => HBM_AXI_arburst(1 downto 0),
      HBM_AXI_arid(5 downto 0) => HBM_AXI_arid(5 downto 0),
      HBM_AXI_arlen(3 downto 0) => HBM_AXI_arlen(3 downto 0),
      HBM_AXI_arready => HBM_AXI_arready,
      HBM_AXI_arsize(2 downto 0) => HBM_AXI_arsize(2 downto 0),
      HBM_AXI_arvalid => HBM_AXI_arvalid,
      HBM_AXI_awaddr(32 downto 0) => HBM_AXI_awaddr(32 downto 0),
      HBM_AXI_awburst(1 downto 0) => HBM_AXI_awburst(1 downto 0),
      HBM_AXI_awid(5 downto 0) => HBM_AXI_awid(5 downto 0),
      HBM_AXI_awlen(3 downto 0) => HBM_AXI_awlen(3 downto 0),
      HBM_AXI_awready => HBM_AXI_awready,
      HBM_AXI_awsize(2 downto 0) => HBM_AXI_awsize(2 downto 0),
      HBM_AXI_awvalid => HBM_AXI_awvalid,
      HBM_AXI_bid(5 downto 0) => HBM_AXI_bid(5 downto 0),
      HBM_AXI_bready => HBM_AXI_bready,
      HBM_AXI_bresp(1 downto 0) => HBM_AXI_bresp(1 downto 0),
      HBM_AXI_bvalid => HBM_AXI_bvalid,
      HBM_AXI_rdata(255 downto 0) => HBM_AXI_rdata(255 downto 0),
      HBM_AXI_rid(5 downto 0) => HBM_AXI_rid(5 downto 0),
      HBM_AXI_rlast => HBM_AXI_rlast,
      HBM_AXI_rready => HBM_AXI_rready,
      HBM_AXI_rresp(1 downto 0) => HBM_AXI_rresp(1 downto 0),
      HBM_AXI_rvalid => HBM_AXI_rvalid,
      HBM_AXI_wdata(255 downto 0) => HBM_AXI_wdata(255 downto 0),
      HBM_AXI_wid(5 downto 0) => HBM_AXI_wid(5 downto 0),
      HBM_AXI_wlast => HBM_AXI_wlast,
      HBM_AXI_wready => HBM_AXI_wready,
      HBM_AXI_wstrb(31 downto 0) => HBM_AXI_wstrb(31 downto 0),
      HBM_AXI_wvalid => HBM_AXI_wvalid,
      HBM_READ_tdata(255 downto 0) => HBM_READ_tdata(255 downto 0),
      HBM_READ_tkeep(31 downto 0) => HBM_READ_tkeep(31 downto 0),
      HBM_READ_tlast => HBM_READ_tlast,
      HBM_READ_tready => HBM_READ_tready,
      HBM_READ_tvalid => HBM_READ_tvalid,
      HBM_WRITE_tdata(255 downto 0) => HBM_WRITE_tdata(255 downto 0),
      HBM_WRITE_tready => HBM_WRITE_tready,
      HBM_WRITE_tvalid => HBM_WRITE_tvalid,
      clk_312 => clk_312,
      clk_450 => clk_450,
      rd_running => rd_running,
      user_hbm_channel_ctrl_ports_busy => user_hbm_channel_ctrl_ports_busy,
      user_hbm_channel_ctrl_ports_rd_blockamount(31 downto 0) => user_hbm_channel_ctrl_ports_rd_blockamount(31 downto 0),
      user_hbm_channel_ctrl_ports_rd_startaddress(31 downto 0) => user_hbm_channel_ctrl_ports_rd_startaddress(31 downto 0),
      user_hbm_channel_ctrl_ports_wr_blockamount(31 downto 0) => user_hbm_channel_ctrl_ports_wr_blockamount(31 downto 0),
      user_hbm_channel_ctrl_ports_wr_startaddress(31 downto 0) => user_hbm_channel_ctrl_ports_wr_startaddress(31 downto 0),
      wr_running => wr_running
    );
end STRUCTURE;
